/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.io;

/**
 * This exception contains all data necessary to locate an error in vnf data. It is intended to be thrown
 * all the way up to the calling module level and then handled by displaying FileErrorFrame
 * and silent finalizing the module execution with null outputs. Since the FileErrorFrame allows
 * to edit offending files the user can fix them and reread the .vnf file.
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */

public class VNIOException extends Exception
{
    public final String text;
    public final String fname;
    public final int lineNumber;
    public final String vnfName;
    public final int vnfLineNumber;
    public final Exception ex;

/**
 * The constructor to be used if the error has been detected when parsing .vnf or when reading a binary file
 * @param text        error description
 * @param fname       name of the offending file (.vnf)
 * @param lineNumber  location of the error (-1 when error has been detected in a binary file)
 */
    public VNIOException(String text, String fname, int lineNumber)
    {
        this(text, fname, lineNumber, "", -1);
    }

/**
 * The constructor to be used if the error has been detected when parsing .vnf or when reading a binary file
 * @param text        error description
 * @param fname       name of the offending file (.vnf)
 * @param lineNumber  location of the error (-1 when error has been detected in a binary file)
 */
    public VNIOException(String text, String fname, int lineNumber, Exception ex)
    {
        this(text, fname, lineNumber, "", -1, ex);
    }

/**
 * The constructor to be used if the error has been detected when reading a text file
 * @param text             error description
 * @param fname            name of the offending file
 * @param lineNumber       location of the error
 * @param vnfName          name of the .vnf file
 * @param vnfLineNumber    line containing the offending file section description
 */
    public VNIOException(String text, String fname, int lineNumber, String vnfName, int vnfLineNumber)
    {
        this(text, fname, lineNumber, vnfName, vnfLineNumber, null);
    }


/**
 * The constructor to be used if the error has been detected when reading a text file
 * @param text             error description
 * @param fname            name of the offending file
 * @param lineNumber       location of the error
 * @param vnfName          name of the .vnf file
 * @param vnfLineNumber    line containing the offending file section description
 * @param ex               if the VNIOException is raised because of some exception ex, ex is passed
 */
    public VNIOException(String text, String fname, int lineNumber, String vnfName, int vnfLineNumber, Exception ex)
    {
        this.text = text;
        this.fname = fname;
        this.lineNumber = lineNumber;
        this.vnfName = vnfName;
        this.vnfLineNumber = vnfLineNumber;
        this.ex = ex;
    }

    public void debug()
    {
        System.out.println("text = " + text);
        System.out.println("file = " + fname);
        System.out.println("line = " + lineNumber);
        System.out.println("vnf file = " + vnfName);
        System.out.println("vnf line = " + vnfLineNumber);
        if (ex != null)
            ex.printStackTrace();
    }
}
