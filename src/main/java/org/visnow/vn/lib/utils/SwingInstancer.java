/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import javax.swing.SwingUtilities;
import org.apache.log4j.Logger;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class SwingInstancer
{
    private static final Logger LOGGER = Logger.getLogger(SwingInstancer.class);

    /**
     * Synchronously runs <code>r</code> in Swing dispatch thread.
     * <p>
     * @param r Runnable to be called
     */
    public static void swingRunAndWait(Runnable r)
    {
        if (SwingUtilities.isEventDispatchThread()) {
            r.run();
        } else {
            try {
                SwingUtilities.invokeAndWait(r);
            } catch (InterruptedException | InvocationTargetException ex) {
                LOGGER.warn("", ex);
            }
        }
    }

    /**
     * Asynchronously runs <code>r</code> in Swing dispatch thread.
     * <p>
     * @param r Runnable to be called
     */
    public static void swingRunLater(Runnable r)
    {
        if (SwingUtilities.isEventDispatchThread()) {
            r.run();
        } else {
            SwingUtilities.invokeLater(r);
        }

    }

    /**
     * Asynchronously runs <code>r</code> in Swing dispatch thread if condition is true,
     * otherwise run in-place.
     * <p>
     * @param r Runnable to be called
     * @param condition for running asynchronously
     */
    public static void swingRunLater(Runnable r, boolean condition)
    {
        if (SwingUtilities.isEventDispatchThread() || !condition) {
            r.run();
        } else {
            SwingUtilities.invokeLater(r);
        }

    }   
    
    private SwingInstancer()
    {
    }
}
