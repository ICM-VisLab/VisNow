/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.geometries.parameters.RegularField3DParams;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class RegularFieldProjection
{
    /**
     * Creates a new instance of CropRegularField
     */
    public RegularFieldProjection()
    {
    }
    protected static class ComputeProjection implements Runnable
    {
        long[] dims;
        long[] outDims;
        LargeArrayType type;
        int axis;
        int nThreads;
        int iThread;
        LargeArray inData;
        LargeArray outData;
        int operation;
        public ComputeProjection(long[] dims, long[] outDims, int axis, int nThreads, int iThread,
                LargeArray inData, LargeArray outData, int operation)
        {
            type = inData.getType();
            this.dims = dims;
            this.outDims = outDims;
            this.axis = axis;
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.inData = inData;
            this.outData = outData;
            this.operation = operation;
        }
        public void run()
        {
            switch (operation) {
            case RegularField3DParams.AVG:
                switch (type) {
                case LOGIC:
                    switch (axis) {
                    case 0:
                        for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                            int f = 0;
                            for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++)
                                f += inData.getByte(k) & 0xff;
                            outData.setBoolean(i, ((byte) (f / dims[0] & 0xff)) != 0);
                        }
                        break;
                    case 1:
                        for (long i = iThread; i < dims[2]; i += nThreads)
                            for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                int f = 0;
                                for (long k = 0, kk = i * dims[0] * dims[1] + j; 
                                     k < dims[1]; 
                                     k++, kk += dims[0])
                                    f += inData.getByte(kk) & 0xff;
                                outData.setBoolean(ii, ((byte) (f / dims[1] & 0xff)) != 0);
                            }
                        break;
                    case 2:
                        for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                            int f = 0;
                            for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1])
                                f += inData.getByte(k) & 0xff;
                            outData.setBoolean(i, ((byte) (f / dims[2] & 0xff)) != 0);
                        }
                        break;
                    }
                    break;
                case BYTE:
                    switch (axis) {
                    case 0:
                        for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                            int f = 0;
                            for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++)
                                f += inData.getByte(k) & 0xff;
                            outData.setByte(i, (byte) (f / dims[0] & 0xff));
                        }
                        break;
                    case 1:
                        for (long i = iThread; i < dims[2]; i += nThreads)
                            for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                int f = 0;
                                for (long k = 0, kk = i * dims[0] * dims[1] + j; 
                                     k < dims[1]; 
                                     k++, kk += dims[0])
                                    f += inData.getByte(kk) & 0xff;
                                outData.setByte(ii, (byte) (f / dims[1] & 0xff));
                            }
                        break;
                    case 2:
                        for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                            int f = 0;
                            for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1])
                                f += inData.getByte(k) & 0xff;
                            outData.setByte(i, (byte) (f / dims[2] & 0xff));
                        }
                        break;
                    }
                    break;
                case SHORT:
                case INT:
                case FLOAT:
                case DOUBLE:
                    switch (axis) {
                    case 0:
                        for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                            double f = 0;
                            for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++)
                                f += inData.getDouble(k);
                            outData.setDouble(i, f / dims[0]);
                        }
                        break;
                    case 1:
                        for (long i = iThread; i < dims[2]; i += nThreads)
                            for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                double f = 0;
                                for (long k = 0, kk = i * dims[0] * dims[1] + j; 
                                     k < dims[1]; 
                                     k++, kk += dims[0])
                                    f += inData.getDouble(kk);
                                outData.setDouble(ii, f / dims[1]);
                            }
                        break;
                    case 2:
                        for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                            double f = 0;
                            for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1])
                                f += inData.getDouble(k);
                            outData.setDouble(i, f / dims[2]);
                        }
                        break;
                    }
                    break;
                case COMPLEX_FLOAT: {
                    FloatLargeArray realData = ((ComplexFloatLargeArray) inData).getRealArray();
                    FloatLargeArray imData = ((ComplexFloatLargeArray) inData).getImaginaryArray();
                    double[] cplx = new double[2];
                    switch (axis) {
                    case 0:
                        for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                            cplx[0] = 0;
                            cplx[1] = 0;
                            for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++) {
                                cplx[0] += realData.getDouble(k);
                                cplx[1] += imData.getDouble(k);
                            }
                            cplx[0] /= dims[0];
                            cplx[1] /= dims[0];
                            ((ComplexFloatLargeArray) outData).setComplexDouble(i, cplx);
                        }
                        break;
                    case 1:
                        for (long i = iThread; i < dims[2]; i += nThreads)
                            for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                cplx[0] = 0;
                                cplx[1] = 0;
                                for (long k = 0, kk = i * dims[0] * dims[1] + j; 
                                     k < dims[1]; 
                                     k++, kk += dims[0]) {
                                    cplx[0] += realData.getDouble(kk);
                                    cplx[1] += imData.getDouble(kk);
                                }
                                cplx[0] /= dims[1];
                                cplx[1] /= dims[1];
                                ((ComplexFloatLargeArray) outData).setComplexDouble(ii, cplx);
                            }
                        break;
                    case 2:
                        for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                            cplx[0] = 0;
                            cplx[1] = 0;
                            for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1]) {
                                cplx[0] += realData.getDouble(k);
                                cplx[1] += imData.getDouble(k);
                            }
                            cplx[0] /= dims[2];
                            cplx[1] /= dims[2];
                            ((ComplexFloatLargeArray) outData).setComplexDouble(i, cplx);
                        }
                        break;
                    }
                    break;
                    }
                }
                break;
            case RegularField3DParams.MAX:
                switch (type) {
                case LOGIC:
                    switch (axis) {
                    case 0:
                        for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                            int f = 0;
                            for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++)
                                if (f < (inData.getByte(k) & 0xff))
                                    f = inData.getByte(k) & 0xff;
                            outData.setBoolean(i, ((byte) (f & 0xff)) != 0);
                        }
                        break;
                    case 1:
                        for (long i = iThread; i < dims[2]; i += nThreads)
                            for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                int f = 0;
                                for (long k = 0, kk = i * dims[0] * dims[1] + j; 
                                     k < dims[1]; 
                                     k++, kk += dims[0])
                                    if (f < (inData.getByte(kk) & 0xff))
                                        f = inData.getByte(kk) & 0xff;
                                outData.setBoolean(ii, ((byte) (f & 0xff)) != 0);
                            }
                        break;
                    case 2:
                        for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                            int f = 0;
                            for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1])
                                if (f < (inData.getByte(k) & 0xff))
                                    f = inData.getByte(k) & 0xff;
                            outData.setBoolean(i, ((byte) (f & 0xff)) != 0);
                        }
                        break;
                    }
                    break;
                case BYTE:
                    switch (axis) {
                    case 0:
                        for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                            int f = 0;
                            for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++)
                                if (f < (inData.getByte(k) & 0xff))
                                    f = inData.getByte(k) & 0xff;
                            outData.setByte(i, (byte) (f & 0xff));
                        }
                        break;
                    case 1:
                        for (long i = iThread; i < dims[2]; i += nThreads)
                            for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                int f = 0;
                                for (long k = 0, kk = i * dims[0] * dims[1] + j; 
                                     k < dims[1]; k++, 
                                     kk += dims[0])
                                    if (f < (inData.getByte(kk) & 0xff))
                                        f = inData.getByte(kk) & 0xff;
                                outData.setByte(ii, (byte) (f & 0xff));
                            }
                        break;
                    case 2:
                        for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                            int f = 0;
                            for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1])
                                if (f < (inData.getByte(k) & 0xff))
                                    f = inData.getByte(k) & 0xff;
                            outData.setByte(i, (byte) (f & 0xff));
                        }
                        break;
                    }
                    break;
                case SHORT:
                case INT:
                case FLOAT:
                case DOUBLE:
                    switch (axis) {
                    case 0:
                        for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                            double f = Double.NEGATIVE_INFINITY;
                            for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++)
                                if (f < inData.getDouble(k))
                                    f = inData.getDouble(k);
                            outData.setDouble(i, f);
                        }
                        break;
                    case 1:
                        for (long i = iThread; i < dims[2]; i += nThreads)
                            for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                double f = 0;
                                for (long k = 0, kk = i * dims[0] * dims[1] + j; 
                                     k < dims[1]; 
                                     k++, kk += dims[0])
                                    if (f < inData.getDouble(kk))
                                        f = inData.getDouble(kk);
                                outData.setDouble(ii, f);
                            }
                        break;
                    case 2:
                        for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                            double f = 0;
                            for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1])
                                if (f < inData.getDouble(k))
                                    f = inData.getDouble(k);
                            outData.setDouble(i, f);
                        }
                        break;
                    }
                    break;
                case COMPLEX_FLOAT: {
                    FloatLargeArray absData = ((ComplexFloatLargeArray) inData).getAbsArray();
                    switch (axis) {
                    case 0:
                        for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                            double f = Double.NEGATIVE_INFINITY;
                            for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++)
                                if (f < absData.getDouble(k))
                                    f = absData.getDouble(k);
                            outData.setDouble(i, f);
                        }
                        break;
                    case 1:
                        for (long i = iThread; i < dims[2]; i += nThreads)
                            for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                double f = Double.NEGATIVE_INFINITY;
                                for (long k = 0, kk = i * dims[0] * dims[1] + j; 
                                     k < dims[1]; k++, 
                                     kk += dims[0])
                                    if (f < absData.getDouble(kk))
                                        f = absData.getDouble(kk);
                                outData.setDouble(ii, f);
                            }
                        break;
                    case 2:
                        for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                            double f = Double.NEGATIVE_INFINITY;
                            for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1])
                                if (f < absData.getDouble(k))
                                    f = absData.getDouble(k);
                            outData.setDouble(i, f);
                        }
                        break;
                    }
                    break;
                    }
                }
                break;
            case RegularField3DParams.STDDEV:
                switch (type) {
                case LOGIC:
                    switch (axis) {
                    case 0:
                        for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                            int f = 0, s = 0;
                            for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++) {
                                int v = inData.getByte(k) & 0xff;
                                f += v;
                                s += v * v;
                            }
                            outData.setBoolean(i, ((byte) (0xff & 
                                    (int) (sqrt((float) s / dims[axis] - 
                                           ((float) f / dims[axis]) * ((float) f / dims[axis]))))) != 0);
                        }
                        break;
                    case 1:
                        for (long i = iThread; i < dims[2]; i += nThreads)
                            for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                int f = 0, s = 0;
                                for (long k = 0, kk = i * dims[0] * dims[1] + j; k < dims[1]; k++, kk += dims[0]) {
                                    int v = inData.getByte(kk) & 0xff;
                                    f += v;
                                    s += v * v;
                                }
                                outData.setBoolean(ii, ((byte) (0xff & 
                                   (int) (sqrt((float) s / dims[axis] - 
                                         ((float) f / dims[axis]) * ((float) f / dims[axis]))))) != 0);
                            }
                        break;
                    case 2:
                        for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                            int f = 0, s = 0;
                            for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1]) {
                                int v = inData.getByte(k) & 0xff;
                                f += v;
                                s += v * v;
                            }
                            outData.setBoolean(i, ((byte) (0xff & 
                                    (int) (sqrt((float) s / dims[axis] - 
                                          ((float) f / dims[axis]) * ((float) f / dims[axis]))))) != 0);
                        }
                        break;
                    }
                    break;
                case BYTE:
                    switch (axis) {
                    case 0:
                        for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                            int f = 0, s = 0;
                            for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++) {
                                int v = inData.getByte(k) & 0xff;
                                f += v;
                                s += v * v;
                            }
                            outData.setByte(i, (byte) (0xff & 
                                    (int) (sqrt((float) s / dims[axis] - 
                                          ((float) f / dims[axis]) * ((float) f / dims[axis])))));
                        }
                        break;
                    case 1:
                        for (long i = iThread; i < dims[2]; i += nThreads)
                            for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                int f = 0, s = 0;
                                for (long k = 0, kk = i * dims[0] * dims[1] + j; k < dims[1]; k++, kk += dims[0]) {
                                    int v = inData.getByte(kk) & 0xff;
                                    f += v;
                                    s += v * v;
                                }
                                outData.setByte(ii, (byte) (0xff & 
                                        (int) (sqrt((float) s / dims[axis] - 
                                              ((float) f / dims[axis]) * ((float) f / dims[axis])))));
                            }
                        break;
                    case 2:
                        for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                            int f = 0, s = 0;
                            for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1]) {
                                int v = inData.getByte(k) & 0xff;
                                f += v;
                                s += v * v;
                            }
                            outData.setByte(i, (byte) (0xff & 
                                    (int) (sqrt((float) s / dims[axis] - 
                                          ((float) f / dims[axis]) * ((float) f / dims[axis])))));
                        }
                        break;
                    }
                    break;
                case SHORT:
                case INT:
                case FLOAT:
                case DOUBLE:
                    switch (axis) {
                    case 0:
                        for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                            double f = 0, s = 0;
                            for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++) {
                                double v = inData.getDouble(k);
                                f += v;
                                s += v * v;
                            }
                            outData.setDouble(i, sqrt(s / dims[axis] - (f / dims[axis]) * (f / dims[axis])));
                        }
                        break;
                    case 1:
                        for (long i = iThread; i < dims[2]; i += nThreads)
                            for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                double f = 0, s = 0;
                                for (long k = 0, kk = i * dims[0] * dims[1] + j; k < dims[1]; k++, kk += dims[0]) {
                                    double v = inData.getDouble(kk);
                                    f += v;
                                    s += v * v;
                                }
                                outData.setDouble(ii, sqrt(s / dims[axis] - (f / dims[axis]) * (f / dims[axis])));
                            }
                        break;
                    case 2:
                        for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                            double f = 0, s = 0;
                            for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1]) {
                                double v = inData.getDouble(k);
                                f += v;
                                s += v * v;
                            }
                            outData.setDouble(i, sqrt(s / dims[axis] - (f / dims[axis]) * (f / dims[axis])));
                        }
                        break;
                    }
                    break;
                case COMPLEX_FLOAT: {
                    FloatLargeArray absData = ((ComplexFloatLargeArray) inData).getAbsArray();
                    switch (axis) {
                    case 0:
                        for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                            float f = 0, s = 0;
                            for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++) {
                                double v = absData.getDouble(k);
                                f += v;
                                s += v * v;
                            }
                            outData.setDouble(i, (float) (sqrt(s / dims[axis] - (f / dims[axis]) * (f / dims[axis]))));
                        }
                        break;
                    case 1:
                        for (long i = iThread; i < dims[2]; i += nThreads)
                            for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                float f = 0, s = 0;
                                for (long k = 0, kk = i * dims[0] * dims[1] + j; k < dims[1]; k++, kk += dims[0]) {
                                    double v = absData.getDouble(kk);
                                    f += v;
                                    s += v * v;
                                }
                                outData.setDouble(ii, (float) (sqrt(s / dims[axis] - (f / dims[axis]) * (f / dims[axis]))));
                            }
                        break;
                    case 2:
                        for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                            float f = 0, s = 0;
                            for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1]) {
                                double v = absData.getDouble(k);
                                f += v;
                                s += v * v;
                            }
                            outData.setDouble(i, (float) (sqrt(s / dims[axis] - (f / dims[axis]) * (f / dims[axis]))));
                        }
                        break;
                    }
                    break;
                    }
                }
            }
        }
    }
    public static final DataArray dataArrayProjection(DataArray dataArr, long[] dims, int axis, int function)
    {
        long nInData = dims[0] * dims[1] * dims[2];
        long[] outDims = new long[2];
        int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        switch (axis) {
            case 0:
                outDims[0] = dims[1];
                outDims[1] = dims[2];
                break;
            case 1:
                outDims[0] = dims[0];
                outDims[1] = dims[2];
                break;
            case 2:
                outDims[0] = dims[0];
                outDims[1] = dims[1];
                break;
        }
        if (!dataArr.isNumeric())
            return null;
        DataArray outDA = null;
        int veclen = dataArr.getVectorLength();
        Thread[] workThreads = new Thread[nThreads];
        if (veclen == 1) {
            switch (dataArr.getType()) {
                case FIELD_DATA_LOGIC:
                case FIELD_DATA_BYTE:
                case FIELD_DATA_SHORT:
                case FIELD_DATA_INT:
                case FIELD_DATA_FLOAT:
                case FIELD_DATA_DOUBLE: {
                    LargeArray inData = dataArr.getRawArray();
                    LargeArray outData = LargeArrayUtils.create(inData.getType(), nInData / dims[axis], false);
                    for (int i = 0; i < workThreads.length; i++) {
                        workThreads[i] = new Thread(new ComputeProjection(dims, outDims, axis, nThreads, i, inData, outData, function));
                        workThreads[i].start();
                    }
                    for (Thread workThread : workThreads)
                        try {
                            workThread.join();
                        } catch (Exception e) {
                        }
                    outDA = DataArray.create(outData, 1, dataArr.getName());
                    break;
                }
                case FIELD_DATA_COMPLEX: {
                    LargeArray inData = dataArr.getRawArray();
                    LargeArray outData;
                    if (function == RegularField3DParams.AVG)
                        outData = LargeArrayUtils.create(LargeArrayType.COMPLEX_FLOAT, nInData / dims[axis], false);
                    else
                        outData = LargeArrayUtils.create(LargeArrayType.FLOAT, nInData / dims[axis], false);
                    for (int i = 0; i < workThreads.length; i++) {
                        workThreads[i] = new Thread(new ComputeProjection(dims, outDims, axis, nThreads, i, inData, outData, function));
                        workThreads[i].start();
                    }
                    for (Thread workThread : workThreads)
                        try {
                            workThread.join();
                        } catch (Exception e) {
                        }
                    outDA = DataArray.create(outData, 1, dataArr.getName());
                    break;
                }
                default:
                    throw new UnsupportedOperationException("DataArray of type " + DataArrayType.getType(dataArr.getType().getValue()) + " is not supported.");
            }
            outDA.setPreferredRange(dataArr.getPreferredMinValue(), dataArr.getPreferredMaxValue());
        } else {
            FloatLargeArray infData = dataArr.getVectorNorms();
            FloatLargeArray outfData = new FloatLargeArray(nInData / dims[axis], false);
            for (int i = 0; i < workThreads.length; i++) {
                workThreads[i] = new Thread(new ComputeProjection(dims, outDims, axis, nThreads, i, infData, outfData, function));
                workThreads[i].start();
            }
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (Exception e) {
                }
            outDA = DataArray.create(outfData, 1, dataArr.getName());
        }
        return outDA;
    }
    static class ComputeProjectionWithMask implements Runnable
    {
        long[] dims;
        long[] outDims;
        LargeArrayType type;
        int axis;
        int nThreads;
        int iThread;
        LargeArray inData;
        LargeArray outData;
        LogicLargeArray mask = null;
        int operation;
        public ComputeProjectionWithMask(long[] dims, long[] outDims,
                int axis, int nThreads, int iThread,
                LargeArray inData, LogicLargeArray mask,
                LargeArray outData, int operation)
        {
            type = inData.getType();
            this.dims = dims;
            this.outDims = outDims;
            this.axis = axis;
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.inData = inData;
            this.mask = mask;
            this.outData = outData;
            this.operation = operation;
        }
        public void run()
        {
            switch (operation) {
                case RegularField3DParams.AVG:
                    switch (type) {
                        case LOGIC:
                            switch (axis) {
                                case 0:
                                    for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                                        long f = 0;
                                        long count = 0;
                                        for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++)
                                            if (mask.getBoolean(k)) {
                                                f += inData.getByte(k) & 0xff;
                                                count += 1;
                                            }
                                        if (count > 0)
                                            outData.setBoolean(i, ((byte) (f / count & 0xff)) != 0);
                                    }
                                    break;
                                case 1:
                                    for (long i = iThread; i < dims[2]; i += nThreads)
                                        for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                            long f = 0;
                                            long count = 0;
                                            for (long k = 0, kk = i * dims[0] * dims[1] + j; k < dims[1]; k++, kk += dims[0])
                                                if (mask.getBoolean(kk)) {
                                                    f += inData.getByte(kk) & 0xff;
                                                    count += 1;
                                                }
                                            if (count > 0)
                                                outData.setBoolean(ii, ((byte) (f / count & 0xff)) != 0);
                                        }
                                    break;
                                case 2:
                                    for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                                        long f = 0;
                                        long count = 0;
                                        for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1])
                                            if (mask.getBoolean(k)) {
                                                f += inData.getByte(k) & 0xff;
                                                count += 1;
                                            }
                                        if (count > 0)
                                            outData.setBoolean(i, ((byte) (f / count & 0xff)) != 0);
                                    }
                                    break;
                            }
                            break;
                        case BYTE:
                            switch (axis) {
                                case 0:
                                    for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                                        long f = 0;
                                        long count = 0;
                                        for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++)
                                            if (mask.getBoolean(k)) {
                                                f += inData.getByte(k) & 0xff;
                                                count += 1;
                                            }
                                        if (count > 0)
                                            outData.setByte(i, (byte) (f / count & 0xff));
                                    }
                                    break;
                                case 1:
                                    for (long i = iThread; i < dims[2]; i += nThreads)
                                        for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                            long f = 0;
                                            long count = 0;
                                            for (long k = 0, kk = i * dims[0] * dims[1] + j; k < dims[1]; k++, kk += dims[0])
                                                if (mask.getBoolean(kk)) {
                                                    f += inData.getByte(kk) & 0xff;
                                                    count += 1;
                                                }
                                            if (count > 0)
                                                outData.setByte(ii, (byte) (f / count & 0xff));
                                        }
                                    break;
                                case 2:
                                    for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                                        long f = 0;
                                        long count = 0;
                                        for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1])
                                            if (mask.getBoolean(k)) {
                                                f += inData.getByte(k) & 0xff;
                                                count += 1;
                                            }
                                        if (count > 0)
                                            outData.setByte(i, (byte) (f / count & 0xff));
                                    }
                                    break;
                            }
                            break;
                        case SHORT:
                        case INT:
                        case FLOAT:
                        case DOUBLE:
                            switch (axis) {
                                case 0:
                                    for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                                        long count = 0;
                                        double f = 0;
                                        for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++)
                                            if (mask.getBoolean(k)) {
                                                f += inData.getDouble(k);
                                                count += 1;
                                            }
                                        if (count > 0)
                                            outData.setDouble(i, f / count);
                                    }
                                    break;
                                case 1:
                                    for (long i = iThread; i < dims[2]; i += nThreads)
                                        for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                            long count = 0;
                                            double f = 0;
                                            for (long k = 0, kk = i * dims[0] * dims[1] + j; k < dims[1]; k++, kk += dims[0])
                                                if (mask.getBoolean(kk)) {
                                                    f += inData.getDouble(kk);
                                                    count += 1;
                                                }
                                            if (count > 0)
                                                outData.setDouble(ii, f / dims[1]);
                                        }
                                    break;
                                case 2:
                                    for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                                        long count = 0;
                                        double f = 0;
                                        for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1])
                                            if (mask.getBoolean(k)) {
                                                f += inData.getDouble(k);
                                                count += 1;
                                            }
                                        if (count > 0)
                                            outData.setDouble(i, f / count);
                                    }
                                    break;
                            }
                            break;
                        case COMPLEX_FLOAT: {
                            FloatLargeArray realData = ((ComplexFloatLargeArray) inData).getRealArray();
                            FloatLargeArray imData = ((ComplexFloatLargeArray) inData).getImaginaryArray();
                            double[] cplx = new double[2];
                            switch (axis) {
                                case 0:
                                    for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                                        long count = 0;
                                        cplx[0] = 0;
                                        cplx[1] = 0;
                                        for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++)
                                            if (mask.getBoolean(k)) {
                                                cplx[0] += realData.getDouble(k);
                                                cplx[1] += imData.getDouble(k);
                                                count += 1;
                                            }
                                        if (count > 0) {
                                            cplx[0] /= count;
                                            cplx[1] /= count;
                                            ((ComplexFloatLargeArray) outData).setComplexDouble(i, cplx);
                                        }
                                    }
                                    break;
                                case 1:
                                    for (long i = iThread; i < dims[2]; i += nThreads)
                                        for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                            long count = 0;
                                            cplx[0] = 0;
                                            cplx[1] = 0;
                                            for (long k = 0, kk = i * dims[0] * dims[1] + j; k < dims[1]; k++, kk += dims[0])
                                                if (mask.getBoolean(kk)) {
                                                    cplx[0] += realData.getDouble(kk);
                                                    cplx[1] += imData.getDouble(kk);
                                                    count += 1;
                                                }
                                            if (count > 0) {
                                                cplx[0] /= count;
                                                cplx[1] /= count;
                                                ((ComplexFloatLargeArray) outData).setComplexDouble(ii, cplx);
                                            }
                                        }
                                    break;
                                case 2:
                                    for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                                        long count = 0;
                                        cplx[0] = 0;
                                        cplx[1] = 0;
                                        for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1])
                                            if (mask.getBoolean(k)) {
                                                cplx[0] += realData.getDouble(k);
                                                cplx[1] += imData.getDouble(k);
                                                count += 1;
                                            }
                                        if (count > 0) {
                                            cplx[0] /= count;
                                            cplx[1] /= count;
                                            ((ComplexFloatLargeArray) outData).setComplexDouble(i, cplx);
                                        }
                                    }
                                    break;
                            }
                            break;
                        }
                    }
                    break;
                case RegularField3DParams.MAX:
                    switch (type) {
                        case LOGIC:
                            switch (axis) {
                                case 0:
                                    for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                                        int f = 0;
                                        for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++)
                                            if (mask.getBoolean(k) && f < (inData.getByte(k) & 0xff))
                                                f = inData.getByte(k) & 0xff;
                                        outData.setBoolean(i, ((byte) (f & 0xff)) != 0);
                                    }
                                    break;
                                case 1:
                                    for (long i = iThread; i < dims[2]; i += nThreads)
                                        for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                            int f = 0;
                                            for (long k = 0, kk = i * dims[0] * dims[1] + j; k < dims[1]; k++, kk += dims[0])
                                                if (mask.getBoolean(kk) && f < (inData.getByte(kk) & 0xff))
                                                    f = inData.getByte(kk) & 0xff;
                                            outData.setBoolean(ii, ((byte) (f & 0xff)) != 0);
                                        }
                                    break;
                                case 2:
                                    for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                                        int f = 0;
                                        for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1])
                                            if (mask.getBoolean(k) && f < (inData.getByte(k) & 0xff))
                                                f = inData.getByte(k) & 0xff;
                                        outData.setBoolean(i, ((byte) (f & 0xff)) != 0);
                                    }
                                    break;
                            }
                            break;
                        case BYTE:
                            switch (axis) {
                                case 0:
                                    for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                                        int f = 0;
                                        for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++)
                                            if (mask.getBoolean(k) && f < (inData.getByte(k) & 0xff))
                                                f = inData.getByte(k) & 0xff;
                                        outData.setByte(i, (byte) (f & 0xff));
                                    }
                                    break;
                                case 1:
                                    for (long i = iThread; i < dims[2]; i += nThreads)
                                        for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                            int f = 0;
                                            for (long k = 0, kk = i * dims[0] * dims[1] + j; k < dims[1]; k++, kk += dims[0])
                                                if (mask.getBoolean(kk) && f < (inData.getByte(kk) & 0xff))
                                                    f = inData.getByte(kk) & 0xff;
                                            outData.setByte(ii, (byte) (f & 0xff));
                                        }
                                    break;
                                case 2:
                                    for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                                        int f = 0;
                                        for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1])
                                            if (mask.getBoolean(k) && f < (inData.getByte(k) & 0xff))
                                                f = inData.getByte(k) & 0xff;
                                        outData.setByte(i, (byte) (f & 0xff));
                                    }
                                    break;
                            }
                            break;
                        case SHORT:
                        case INT:
                        case FLOAT:
                        case DOUBLE:
                            switch (axis) {
                                case 0:
                                    for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                                        double f = -Double.MAX_VALUE;
                                        for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++)
                                            if (mask.getBoolean(k) && f < inData.getDouble(k))
                                                f = inData.getDouble(k);
                                        outData.setDouble(i, f);
                                    }
                                    break;
                                case 1:
                                    for (long i = iThread; i < dims[2]; i += nThreads)
                                        for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                            double f = -Double.MAX_VALUE;
                                            for (long k = 0, kk = i * dims[0] * dims[1] + j; k < dims[1]; k++, kk += dims[0])
                                                if (mask.getBoolean(kk) && f < inData.getDouble(kk))
                                                    f = inData.getDouble(kk);
                                            outData.setDouble(ii, f);
                                        }
                                    break;
                                case 2:
                                    for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                                        double f = -Double.MAX_VALUE;
                                        for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1])
                                            if (mask.getBoolean(k) && f < inData.getDouble(k))
                                                f = inData.getDouble(k);
                                        outData.setDouble(i, f);
                                    }
                                    break;
                            }
                            break;
                        case COMPLEX_FLOAT: {
                            FloatLargeArray absData = ((ComplexFloatLargeArray) inData).getAbsArray();
                            switch (axis) {
                                case 0:
                                    for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                                        double f = -Double.MAX_VALUE;
                                        for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++)
                                            if (mask.getBoolean(k) && f < absData.getDouble(k))
                                                f = absData.getDouble(k);
                                        outData.setDouble(i, f);
                                    }
                                    break;
                                case 1:
                                    for (long i = iThread; i < dims[2]; i += nThreads)
                                        for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                            double f = -Double.MAX_VALUE;
                                            for (long k = 0, kk = i * dims[0] * dims[1] + j; k < dims[1]; k++, kk += dims[0])
                                                if (mask.getBoolean(kk) && f < absData.getDouble(kk))
                                                    f = absData.getDouble(kk);
                                            outData.setDouble(ii, f);
                                        }
                                    break;
                                case 2:
                                    for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                                        double f = -Double.MAX_VALUE;
                                        for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1])
                                            if (mask.getBoolean(k) && f < absData.getDouble(k))
                                                f = absData.getDouble(k);
                                        outData.setDouble(i, f);
                                    }
                                    break;
                            }
                            break;
                        }
                    }
                    break;
                case RegularField3DParams.STDDEV:
                    switch (type) {
                        case LOGIC:
                            switch (axis) {
                                case 0:
                                    for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                                        int f = 0, s = 0;
                                        for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++) {
                                            int v = inData.getByte(k) & 0xff;
                                            f += v;
                                            s += v * v;
                                        }
                                        outData.setBoolean(i, ((byte) (0xff & (int) (sqrt((float) s / dims[axis] - ((float) f / dims[axis]) * ((float) f / dims[axis]))))) != 0);
                                    }
                                    break;
                                case 1:
                                    for (long i = iThread; i < dims[2]; i += nThreads)
                                        for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                            int f = 0, s = 0;
                                            for (long k = 0, kk = i * dims[0] * dims[1] + j; k < dims[1]; k++, kk += dims[0]) {
                                                int v = inData.getByte(kk) & 0xff;
                                                f += v;
                                                s += v * v;
                                            }
                                            outData.setBoolean(ii, ((byte) (0xff & (int) (sqrt((float) s / dims[axis] - ((float) f / dims[axis]) * ((float) f / dims[axis]))))) != 0);
                                        }
                                    break;
                                case 2:
                                    for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                                        int f = 0, s = 0;
                                        for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1]) {
                                            int v = inData.getByte(k) & 0xff;
                                            f += v;
                                            s += v * v;
                                        }
                                        outData.setBoolean(i, ((byte) (0xff & (int) (sqrt((float) s / dims[axis] - ((float) f / dims[axis]) * ((float) f / dims[axis]))))) != 0);
                                    }
                                    break;
                            }
                            break;
                        case BYTE:
                            switch (axis) {
                                case 0:
                                    for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                                        int f = 0, s = 0;
                                        for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++) {
                                            int v = inData.getByte(k) & 0xff;
                                            f += v;
                                            s += v * v;
                                        }
                                        outData.setByte(i, (byte) (0xff & (int) (sqrt((float) s / dims[axis] - ((float) f / dims[axis]) * ((float) f / dims[axis])))));
                                    }
                                    break;
                                case 1:
                                    for (long i = iThread; i < dims[2]; i += nThreads)
                                        for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                            int f = 0, s = 0;
                                            for (long k = 0, kk = i * dims[0] * dims[1] + j; k < dims[1]; k++, kk += dims[0]) {
                                                int v = inData.getByte(kk) & 0xff;
                                                f += v;
                                                s += v * v;
                                            }
                                            outData.setByte(ii, (byte) (0xff & (int) (sqrt((float) s / dims[axis] - ((float) f / dims[axis]) * ((float) f / dims[axis])))));
                                        }
                                    break;
                                case 2:
                                    for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                                        int f = 0, s = 0;
                                        for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1]) {
                                            int v = inData.getByte(k) & 0xff;
                                            f += v;
                                            s += v * v;
                                        }
                                        outData.setByte(i, (byte) (0xff & (int) (sqrt((float) s / dims[axis] - ((float) f / dims[axis]) * ((float) f / dims[axis])))));
                                    }
                                    break;
                            }
                            break;
                        case SHORT:
                        case INT:
                        case FLOAT:
                        case DOUBLE:
                            switch (axis) {
                                case 0:
                                    for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                                        double f = 0, s = 0;
                                        for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++) {
                                            double v = inData.getDouble(k);
                                            f += v;
                                            s += v * v;
                                        }
                                        outData.setDouble(i, sqrt(s / dims[axis] - (f / dims[axis]) * (f / dims[axis])));
                                    }
                                    break;
                                case 1:
                                    for (long i = iThread; i < dims[2]; i += nThreads)
                                        for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                            double f = 0, s = 0;
                                            for (long k = 0, kk = i * dims[0] * dims[1] + j; k < dims[1]; k++, kk += dims[0]) {
                                                double v = inData.getDouble(kk);
                                                f += v;
                                                s += v * v;
                                            }
                                            outData.setDouble(ii, sqrt(s / dims[axis] - (f / dims[axis]) * (f / dims[axis])));
                                        }
                                    break;
                                case 2:
                                    for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                                        double f = 0, s = 0;
                                        for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1]) {
                                            double v = inData.getDouble(k);
                                            f += v;
                                            s += v * v;
                                        }
                                        outData.setDouble(i, sqrt(s / dims[axis] - (f / dims[axis]) * (f / dims[axis])));
                                    }
                                    break;
                            }
                            break;
                        case COMPLEX_FLOAT: {
                            FloatLargeArray absData = ((ComplexFloatLargeArray) inData).getAbsArray();
                            switch (axis) {
                                case 0:
                                    for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                                        float f = 0, s = 0;
                                        for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++) {
                                            double v = absData.getDouble(k);
                                            f += v;
                                            s += v * v;
                                        }
                                        outData.setDouble(i, (float) (sqrt(s / dims[axis] - (f / dims[axis]) * (f / dims[axis]))));
                                    }
                                    break;
                                case 1:
                                    for (long i = iThread; i < dims[2]; i += nThreads)
                                        for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                                            float f = 0, s = 0;
                                            for (long k = 0, kk = i * dims[0] * dims[1] + j; k < dims[1]; k++, kk += dims[0]) {
                                                double v = absData.getDouble(kk);
                                                f += v;
                                                s += v * v;
                                            }
                                            outData.setDouble(ii, (float) (sqrt(s / dims[axis] - (f / dims[axis]) * (f / dims[axis]))));
                                        }
                                    break;
                                case 2:
                                    for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                                        float f = 0, s = 0;
                                        for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1]) {
                                            double v = absData.getDouble(k);
                                            f += v;
                                            s += v * v;
                                        }
                                        outData.setDouble(i, (float) (sqrt(s / dims[axis] - (f / dims[axis]) * (f / dims[axis]))));
                                    }
                                    break;
                            }
                            break;
                        }
                    }
            }
        }
    }
    public static final DataArray dataArrayProjection(DataArray dataArr, LogicLargeArray mask, long[] dims, int axis, int function)
    {
        long nInData = dims[0] * dims[1] * dims[2];
        long[] outDims = new long[2];
        int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        switch (axis) {
            case 0:
                outDims[0] = dims[1];
                outDims[1] = dims[2];
                break;
            case 1:
                outDims[0] = dims[0];
                outDims[1] = dims[2];
                break;
            case 2:
                outDims[0] = dims[0];
                outDims[1] = dims[1];
                break;
        }
        if (!dataArr.isNumeric())
            return null;
        DataArray outDA = null;
        int veclen = dataArr.getVectorLength();
        Thread[] workThreads = new Thread[nThreads];
        if (veclen == 1) {
            switch (dataArr.getType()) {
                case FIELD_DATA_LOGIC:
                case FIELD_DATA_BYTE:
                case FIELD_DATA_SHORT:
                case FIELD_DATA_INT:
                case FIELD_DATA_FLOAT:
                case FIELD_DATA_DOUBLE: {
                    LargeArray inData = dataArr.getRawArray();
                    LargeArray outData = LargeArrayUtils.create(inData.getType(), nInData / dims[axis], true);
                    for (int i = 0; i < workThreads.length; i++) {
                        workThreads[i] = new Thread(new ComputeProjectionWithMask(dims, outDims, axis, nThreads, i, inData, mask, outData, function));
                        workThreads[i].start();
                    }
                    for (Thread workThread : workThreads)
                        try {
                            workThread.join();
                        } catch (Exception e) {
                        }
                    outDA = DataArray.create(outData, 1, dataArr.getName());
                    break;
                }
                case FIELD_DATA_COMPLEX: {
                    LargeArray inData = dataArr.getRawArray();
                    LargeArray outData;
                    if (function == RegularField3DParams.AVG)
                        outData = LargeArrayUtils.create(LargeArrayType.COMPLEX_FLOAT, nInData / dims[axis], true);
                    else
                        outData = LargeArrayUtils.create(LargeArrayType.FLOAT, nInData / dims[axis], true);
                    for (int i = 0; i < workThreads.length; i++) {
                        workThreads[i] = new Thread(new ComputeProjectionWithMask(dims, outDims, axis, nThreads, i, inData, mask, outData, function));
                        workThreads[i].start();
                    }
                    for (Thread workThread : workThreads)
                        try {
                            workThread.join();
                        } catch (Exception e) {
                        }
                    outDA = DataArray.create(outData, 1, dataArr.getName());
                    break;
                }
                default:
                    throw new UnsupportedOperationException("DataArray of type " + DataArrayType.getType(dataArr.getType().getValue()) + " is not supported.");
            }
            outDA.setPreferredRange(dataArr.getPreferredMinValue(), dataArr.getPreferredMaxValue());
        } else {
            FloatLargeArray infData = dataArr.getVectorNorms();
            FloatLargeArray outfData = new FloatLargeArray(nInData / dims[axis], true);
            for (int i = 0; i < workThreads.length; i++) {
                workThreads[i] = new Thread(new ComputeProjectionWithMask(dims, outDims, axis, nThreads, i, infData, mask, outfData, function));
                workThreads[i].start();
            }
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (Exception e) {
                }
            outDA = DataArray.create(outfData, 1, dataArr.getName());
        }
        return outDA;
    }
    static class ComputeMaskProjection implements Runnable
    {
        long[] dims;
        long[] outDims;
        int axis;
        int nThreads;
        int iThread;
        LogicLargeArray mask = null;
        LogicLargeArray outMask = null;
        public ComputeMaskProjection(long[] dims, long[] outDims,
                int axis, int nThreads, int iThread,
                LogicLargeArray mask, LogicLargeArray outMask)
        {
            this.dims = dims;
            this.outDims = outDims;
            this.axis = axis;
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.mask = mask;
            this.outMask = outMask;
        }
        @Override
        public void run()
        {
            switch (axis) {
                case 0:
                    for (long i = iThread; i < dims[1] * dims[2]; i += nThreads) {
                        outMask.setBoolean(i, false);
                        for (long j = 0, k = i * dims[0]; j < dims[0]; j++, k++)
                            if (mask.getBoolean(k)) {
                                outMask.setBoolean(i, true);
                                break;
                            }
                    }
                    break;
                case 1:
                    for (long i = iThread; i < dims[2]; i += nThreads)
                        for (long j = 0, ii = i * dims[0]; j < dims[0]; j++, ii++) {
                            outMask.setBoolean(ii, false);
                            for (long k = 0, kk = i * dims[0] * dims[1] + j; k < dims[1]; k++, kk += dims[0])
                                if (mask.getBoolean(kk)) {
                                    outMask.setBoolean(ii, true);
                                    break;
                                }
                        }
                    break;
                case 2:
                    for (long i = iThread; i < dims[0] * dims[1]; i += nThreads) {
                        outMask.setBoolean(i, false);
                        for (long j = 0, k = i; j < dims[2]; j++, k += dims[0] * dims[1])
                            if (mask.getBoolean(k)) {
                                outMask.setBoolean(i, true);
                                break;
                            }
                    }
                    break;
            }
        }
    }
    public static LogicLargeArray computeMaskProjection(RegularField inField, int axis)
    {
        if (inField == null || inField.getDims() == null || inField.getDims().length != 3
                || axis < 0 || axis >= 3)
            return null;
        int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        if (nThreads > 3)
            nThreads -= 1;
        long[] dims = inField.getLDims();
        long[] outDims = new long[2];
        switch (axis) {
            case 0:
                outDims[0] = dims[1];
                outDims[1] = dims[2];
                break;
            case 1:
                outDims[0] = dims[0];
                outDims[1] = dims[2];
                break;
            case 2:
                outDims[0] = dims[0];
                outDims[1] = dims[1];
        }
        LogicLargeArray mask = inField.getCurrentMask();
        LogicLargeArray outMask = new LogicLargeArray(outDims[0] * outDims[1], false);
        Thread[] workThreads = new Thread[nThreads];
        for (int i = 0; i < workThreads.length; i++) {
            workThreads[i] = new Thread(new ComputeMaskProjection(dims, outDims, axis, nThreads, i, mask, outMask));
            workThreads[i].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (Exception e) {
            }
        return outMask;
    }
    public static final RegularField fieldProjection(RegularField inField, int axis, int function, int slice)
    {
        if (inField == null || inField.getDims() == null || inField.getDims().length != 3
                || axis < 0 || axis >= 3
                || function < RegularField3DParams.AVG || function > RegularField3DParams.STDDEV)
            return null;
        int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        if (nThreads > 3)
            nThreads -= 1;
        long[] dims = inField.getLDims();
        long[] outDims = new long[2];
        float[][] inAffine = inField.getAffine();
        float[][] outAffine = null;
        FloatLargeArray inCoords = null;
        FloatLargeArray outCoords = null;
        if (inField.getCurrentCoords() == null)
            outAffine = new float[4][3];
        else
            inCoords = inField.getCurrentCoords();
        switch (axis) {
            case 0:
                outDims[0] = dims[1];
                outDims[1] = dims[2];
                if (inField.getCurrentCoords() == null)
                    for (int i = 0; i < 3; i++) {
                        outAffine[3][i] = inAffine[3][i] + slice * inAffine[0][i];
                        outAffine[0][i] = inAffine[1][i];
                        outAffine[1][i] = inAffine[2][i];
                        outAffine[2][i] = 0;
                    }
                else {
                    outCoords = new FloatLargeArray(3 * outDims[0] * outDims[1], false);
                    for (long i = 0, k = slice, l = 0; i < outDims[0] * outDims[1]; i++, k += dims[0])
                        for (long j = 0; j < 3; j++, l++)
                            outCoords.setFloat(l, inCoords.getFloat(3 * k + j));
                }
                break;
            case 1:
                outDims[0] = dims[0];
                outDims[1] = dims[2];
                if (inField.getCurrentCoords() == null)
                    for (int i = 0; i < 3; i++) {
                        outAffine[3][i] = inAffine[3][i] + slice * inAffine[1][i];
                        outAffine[0][i] = inAffine[0][i];
                        outAffine[1][i] = inAffine[2][i];
                        outAffine[2][i] = 0;
                    }
                else {
                    outCoords = new FloatLargeArray(3 * outDims[0] * outDims[1], false);
                    for (long i = 0, k = 0; i < outDims[1]; i++)
                        for (long j = 0, l = 3 * ((i * dims[1] + slice) * dims[0]); j < 3 * outDims[0]; j++, k++, l++)
                            outCoords.setFloat(k, inCoords.getFloat(l));
                }
                break;
            case 2:
                outDims[0] = dims[0];
                outDims[1] = dims[1];
                if (inField.getCurrentCoords() == null)
                    for (int i = 0; i < 3; i++) {
                        outAffine[3][i] = inAffine[3][i] + slice * inAffine[2][i];
                        outAffine[0][i] = inAffine[0][i];
                        outAffine[1][i] = inAffine[1][i];
                        outAffine[2][i] = 0;
                    }
                else {
                    outCoords = new FloatLargeArray(3 * outDims[0] * outDims[1], false);
                    for (long i = 0, j = 3 * slice * dims[0] * dims[1]; i < outCoords.length(); i++, j++)
                        outCoords.setFloat(i, inCoords.getFloat(j));
                }
                break;
        }
        RegularField outField = new RegularField(outDims);
        if (inField.getCurrentCoords() == null)
            outField.setAffine(outAffine);
        else
            outField.setCurrentCoords(outCoords);

        if (inField.hasMask()) {
            outField.setCurrentMask(computeMaskProjection(inField, axis));
            for (int n = 0; n < inField.getNComponents(); n++) {
                DataArray dataArr = inField.getComponent(n);
                if (dataArr.isNumeric())
                    outField.addComponent(dataArrayProjection(dataArr, inField.getCurrentMask(), dims, axis, function));
            }
        } else
            for (int n = 0; n < inField.getNComponents(); n++) {
                DataArray dataArr = inField.getComponent(n);
                if (dataArr.isNumeric())
                    outField.addComponent(dataArrayProjection(dataArr, dims, axis, function));
            }
        return outField;
    }
}
