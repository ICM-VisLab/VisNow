/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field.subset;

import java.util.ArrayList;
import java.util.Arrays;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import static org.visnow.jscic.utils.CropDownUtils.cropDownArray;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling)
 */
public class FieldSample
{
    private FieldSample()
    {

    }

    public static final TimeData sample(TimeData in, int sampleSize, int veclen, LogicLargeArray mask)
    {
        TimeData out = new TimeData(in.getType());
        for (int it = 0; it < in.getNSteps(); it++) {
            LargeArray inVals = in.getValues().get(it);
            LargeArray outVals = LargeArrayUtils.create(inVals.getType(), sampleSize * veclen);
            for (long i = 0, l = 0; i < mask.length(); i++)
                if (mask.getBoolean(i)) {
                    LargeArrayUtils.arraycopy(inVals, veclen * i, outVals, veclen * l, veclen);
                    l += 1;
                }
            out.setValue(outVals, in.getTime(it));
        }
        return out;
    }

    public static final DataArray sample(DataArray in, int sampleSize, LogicLargeArray mask)
    {
        return DataArray.create(sample(in.getTimeData(), sampleSize, in.getVectorLength(), mask),
                                in.getVectorLength(), in.getName(), in.getUnit(), in.getUserData());
    }



    public static final Field randomSample(Field in, int sampleSize, boolean sampleComponents)
    {
        if (in == null)
            return null;
        if (sampleSize >= in.getNNodes())
            return in.cloneShallow();
        long nNodes = in.getNNodes();
        long nEffNodes = nNodes;
        LogicLargeArray mask = new LogicLargeArray(nNodes, true);
        LogicLargeArray inMask = null;
        if (in.hasMask()) {
            inMask = in.getMask(0);
            nEffNodes = 0;
            for (long i = 0; i < nNodes; i++)
                if (inMask.getBoolean(i))
                    nEffNodes += 1;
            int done = 0;
            if (sampleSize < nEffNodes / 2)
                while (done < sampleSize) {
                    long i = (long) (Math.random() * nNodes);
                    if (!mask.getBoolean(i) &&  inMask.getBoolean(i)) {
                        mask.setBoolean(i, true);
                        done += 1;
                    }
                }
            else {
                for (long i = 0; i < nNodes; i++)
                    mask.setBoolean(i, inMask.getBoolean(i));
                while (done < nEffNodes - sampleSize) {
                    long i = (long) (Math.random() * nNodes);
                    if (mask.getBoolean(i)) {
                        mask.setBoolean(i, false);
                        done += 1;
                    }
                }
            }
        }
        else {
            int done = 0;
            if (sampleSize < nNodes / 2)
                while (done < sampleSize) {
                    long i = (long) (Math.random() * nNodes);
                    if (!mask.getBoolean(i)) {
                        mask.setBoolean(i, true);
                        done += 1;
                    }
                }
            else {
                for (long i = 0; i < nNodes; i++)
                    mask.setBoolean(i, true);
                while (done < nEffNodes - sampleSize) {
                    long i = (long) (Math.random() * nNodes);
                    if (mask.getBoolean(i)) {
                        mask.setBoolean(i, false);
                        done += 1;
                    }
                }
            }
        }

        IrregularField out = new IrregularField(sampleSize);
        if (in.hasCoords())
            out.setCoords(sample(in.getCoords(), sampleSize, 3, mask));
        else if (!(in instanceof RegularField))
            return null;
        else {
            RegularField rIn = (RegularField) in;
            long[] dims = rIn.getLDims();
            float[][] aff = rIn.getAffine();
            FloatLargeArray outCrds = new FloatLargeArray(3 * sampleSize);
            for (long i = 0, l = 0; i < nNodes; i++)
                if (mask.get(i) != 0) {
                    float[] p = new float[3];
                    System.arraycopy(aff[3], 0, p, 0, 3);
                    long ii = i;
                    for (int j = 0; j < dims.length; j++) {
                        long ind = ii % dims[j];
                        ii /= dims[j];
                        for (int k = 0; k < 3; k++)
                            p[k] += ind * aff[j][k];
                    }
                    for (int k = 0; k < p.length; k++)
                        outCrds.setFloat(3 * l + k, p[k]);
                }
            out.setCoords(outCrds, 0);
        }
        if (sampleComponents)
            for (DataArray component : in.getComponents())
                out.addComponent(sample(component, sampleSize, mask));
        else {
            FloatLargeArray d = new FloatLargeArray(sampleSize);
            for (long i = 0; i < sampleSize; i++)
                d.setFloat(i, i);
            out.addComponent(DataArray.create(d, 1, "dummy"));
        }
        return out;
    }

    public static final RegularField regularSample(RegularField in, int[] down, int[] low, int[] up, boolean sampleComponents)
    {

        int[] dims = in.getDims();
        int[] out = new int[dims.length];
        for (int i = 0; i < out.length; i++)
            out[i] = (up[i] - low[i] - 1) / down[i] + 1;
        RegularField outField = new RegularField(out);

        if (in.getCoords() != null) {
            ArrayList<LargeArray> oldTimeCoordsSeries = in.getCoords().getValues();
            float[] oldTimeCoordsTimes = in.getCoords().getTimesAsArray();
            for (int i = 0; i < oldTimeCoordsSeries.size(); i++) {
                outField.setCoords((FloatLargeArray)cropDownArray(oldTimeCoordsSeries.get(i), 3, dims, low, up, down),
                                   oldTimeCoordsTimes[i]);
            }
            outField.setCurrentCoords((FloatLargeArray)cropDownArray(in.getCurrentCoords(), 3, dims, low, up, down));
        } else {
            float[][] outAffine = new float[4][3];
            float[][] affine = in.getAffine();
            System.arraycopy(affine[3], 0, outAffine[3], 0, 3);
            for (int i = 0; i < out.length; i++) {
                for (int j = 0; j < 3; j++) {
                    outAffine[3][j] += low[i] * affine[i][j];
                    outAffine[i][j] = affine[i][j] * down[i];
                }
            }
            outField.setAffine(outAffine);
        }
        if (in.hasMask()) {
            outField.setCurrentMask((LogicLargeArray)cropDownArray(in.getCurrentMask(), 1, dims, low, up, down));
        }

        for (int i = 0; i < in.getNComponents(); i++) {
            DataArray dta = in.getComponent(i);
            if (!dta.isNumeric()) {
                continue;
            }
            DataArray outDta = DataArray.create(dta.getType(), outField.getNNodes(), dta.getVectorLength(), dta.getName(), dta.getUnit(), dta.getUserData());
            outDta.setPreferredRanges(dta.getPreferredMinValue(), dta.getPreferredMaxValue(), dta.getPreferredPhysMinValue(),dta.getPreferredPhysMaxValue());
            outDta.setTimeData(dta.getTimeData().cropDown(dta.getVectorLength(), dims, low, up, down));
            outField.addComponent(outDta);
        }
        return outField;
    }

    public static final RegularField regularSample(RegularField in, int[] low, int[] up)
    {
        int[] down = new int[in.getDimNum()];
        Arrays.fill(down, 1);
        return regularSample(in, down, low, up, true);
    }

    public static final RegularField regularSample(RegularField in, int[] down, boolean centered, boolean sampleComponents)
    {
        if (in == null)
            return null;
        int[] dims = in.getDims();
        if (down.length < dims.length)
            return null;
        for (int i = 0; i < dims.length; i++)
           if (dims[i] < down[i])
               return null;
        int[] low = new int[dims.length];
        int[] up = new int[dims.length];
        for (int i = 0; i < dims.length; i++) {
            int d = (dims[i] - 1) / down[i];
            low[i] = centered ? (dims[i] - down[i] * d) / 2  : 0;
            up[i]  = low[i] + down[i] * d + 1;
        }
        return regularSample(in, down, low, up, sampleComponents);
    }

}
