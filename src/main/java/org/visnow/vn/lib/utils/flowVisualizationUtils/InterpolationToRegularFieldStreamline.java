/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.flowVisualizationUtils;

import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class InterpolationToRegularFieldStreamline {
    
    public static class InterpolateTo3DLines implements Runnable
    {
        protected int iThread;
        protected int nThreads;
        protected int[] dims;
        protected int nSrc;
        protected int nBackward, nForward;
        protected int nSteps;
        protected float[][] inCoords; 
        protected int nData;
        protected int[] indices;
        protected FloatLargeArray[] inData;
        protected int[] inVlens;
        protected FloatLargeArray[] outData;

        public InterpolateTo3DLines(int iThread, int nThreads, int[] dims, 
                                    int nSrc, int nSteps, 
                                    float[][] inCoords, int[] indices,
                                    FloatLargeArray[] inData, int[] inVlens, FloatLargeArray[] outData) {
            this.iThread   = iThread;
            this.nThreads  = nThreads;
            this.dims      = dims;
            this.nSrc      = nSrc;
            this.nSteps    = nSteps;
            this.inCoords  = inCoords;
            this.indices   = indices;
            this.inData    = inData;
            this.inVlens   = inVlens;
            this.outData   = outData;
            nData = inData.length;
        }

        @Override
        public void run() {
            for (int n = (iThread * nSrc) / nThreads; n < ((iThread + 1) * nSrc) / nThreads; n++) {
                int nn = n;
                for (int k = 0; k < nSteps; k++, nn += nSrc) {
                    computeAtPoint(inCoords[n][3 * k], inCoords[n][3 * k + 1], inCoords[n][3 * k + 2], nn);
                    indices[nn] = k;
                }
            }
        }
        
        protected void computeAtPoint(float u, float v, float w, long nn)
        {
            int n0 = dims[0];
            int n1 = dims[0] * dims[1];
            int inexact = 0;
            if (u < 0)
                u = 0;
            if (u >= dims[0])
                u = dims[0] - 1;
            if (v < 0)
                v = 0;
            if (v >= dims[1])
                v = dims[1] - 1;
            if (w < 0)
                w = 0;
            if (w >= dims[2])
                w = dims[2] - 1;
            int i = (int) u;
            u -= i;
            if (u != 0 && i < dims[0] - 1)
                inexact += 1;
            int j = (int) v;
            v -= j;
            if (v != 0 && j < dims[1] - 1)
                inexact += 2;
            int k = (int) w;
            w -= k;
            if (w != 0 && k < dims[2] - 1)
                inexact += 4;
            int m = (dims[1] * k + j) * dims[0] + i;            
            switch (inexact) {
                case 0:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++) 
                            outData[var].set(vlen * nn + cmp, inData[var].get(vlen * m + cmp));
                    }
                    break;
                case 1:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++) 
                            outData[var].set(vlen * nn + cmp,
                                    u *       inData[var].get(vlen * (m + 1) + cmp) +
                                    (1 - u) * inData[var].get(vlen *  m +      cmp));
                    }
                    break;
                case 2:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++) 
                            outData[var].set(vlen * nn + cmp, 
                                    v *       inData[var].get(vlen * (m + n0) + cmp) +
                                    (1 - v) * inData[var].get(vlen *  m +       cmp));
                    }
                    break;
                case 3:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++) 
                            outData[var].set(vlen * nn + cmp, 
                                    v *       (u *      inData[var].get(vlen * (m + n0 + 1) + cmp) +
                                              (1 - u) * inData[var].get(vlen * (m + n0) +     cmp)) + 
                                    (1 - v) * (u *      inData[var].get(vlen * (m +      1) + cmp) +
                                              (1 - u) * inData[var].get(vlen *  m +           cmp)));
                    }
                    break;
                case 4:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++) 
                            outData[var].set(vlen * nn + cmp, 
                                    w *       inData[var].get(vlen * (m + n1) + cmp) +
                                    (1 - w) * inData[var].get(vlen *  m +       cmp));
                    }
                    break;
                case 5:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++) 
                            outData[var].set(vlen * nn + cmp, 
                                    w *       (u *      inData[var].get(vlen * (m + n1 + 1) + cmp) +
                                              (1 - u) * inData[var].get(vlen * (m + n1) +     cmp)) + 
                                    (1 - w) * (u *      inData[var].get(vlen * (m +      1) + cmp) +
                                              (1 - u) * inData[var].get(vlen *  m +           cmp)));
                    }
                    break;
                case 6:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++) 
                            outData[var].set(vlen * nn + cmp, 
                                    w *       (v *      inData[var].get(vlen * (m + n1 + n0) + cmp) +
                                              (1 - v) * inData[var].get(vlen * (m + n1) +      cmp)) + 
                                    (1 - w) * (v *      inData[var].get(vlen * (m +      n0) + cmp) +
                                              (1 - v) * inData[var].get(vlen *  m +            cmp)));
                    }
                    break;
                case 7:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++) 
                            outData[var].set(vlen * nn + cmp, 
                                    w *       (v *      (u *      inData[var].get(vlen * (m + n1 + n0 + 1) + cmp) +
                                                        (1 - u) * inData[var].get(vlen * (m + n1 + n0) +     cmp)) +
                                              (1 - v) * (u *      inData[var].get(vlen * (m + n1 +      1) + cmp) +
                                                        (1 - u) * inData[var].get(vlen * (m + n1) +          cmp))) +
                                    (1 - w) * (v *      (u *      inData[var].get(vlen * (m +      n0 + 1) + cmp) +
                                                        (1 - u) * inData[var].get(vlen * (m +      n0) +     cmp)) +
                                              (1 - v) * (u *      inData[var].get(vlen * (m +           1) + cmp) +
                                                        (1 - u) * inData[var].get(vlen *  m +                cmp))));
                    }
                    break;
            }  
        }
    }
    
    protected static class InterpolateTo2DLines implements Runnable
    {
        protected int iThread;
        protected int nThreads;
        protected int[] dims;
        protected int nSrc;
        protected int nSteps;
        protected float[][] inCoords; 
        protected int nData;
        protected int[] indices;
        protected FloatLargeArray[] inData;
        protected int[] inVlens;
        protected FloatLargeArray[] outData;

        public InterpolateTo2DLines(int iThread, int nThreads, int[] dims, 
                                    int nSrc, int nSteps, 
                                    float[][] inCoords, int[] indices,
                                    FloatLargeArray[] inData, int[] inVlens, FloatLargeArray[] outData) {
            this.iThread   = iThread;
            this.nThreads  = nThreads;
            this.dims      = dims;
            this.nSrc      = nSrc;
            this.nSteps    = nSteps;
            this.inCoords  = inCoords;
            this.indices   = indices;
            this.inData    = inData;
            this.inVlens   = inVlens;
            this.outData   = outData;
            nData = inData.length;
        }

        @Override
        public void run() {
            for (int n = (iThread * nSrc) / nThreads; n < ((iThread + 1) * nSrc) / nThreads; n++) {
                int nn = n;
                for (int k = 0; k < nSteps; k++, nn += nSrc) {
                    computeAtPoint(inCoords[n][2 * k], inCoords[n][2 * k + 1], nn);
                    indices[nn] = k;
                }
            }
        }
        
        protected void computeAtPoint(float u, float v, int nn)
        {
            int n0 = dims[0];
            int inexact = 0;
            if (u < 0)
                u = 0;
            if (u >= dims[0])
                u = dims[0] - 1;
            if (v < 0)
                v = 0;
            if (v >= dims[1])
                v = dims[1] - 1;
            int i = (int) u;
            u -= i;
            if (u != 0 && i < dims[0] - 1)
                inexact += 1;
            int j = (int) v;
            v -= j;
            if (v != 0 && j < dims[1] - 1)
                inexact += 2;
            int m = j * dims[0] + i;            
            switch (inexact) {
                case 0:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++) 
                            try {
                            outData[var].set(vlen * nn + cmp, inData[var].get(vlen * m + cmp));
                            } catch (Exception e) {
                                System.out.printf("var=%d vlen=%d m=%d cmp=%d nn=%d%n", var,vlen,(vlen * m + cmp),cmp,vlen * nn + cmp);
                            }
                    }
                    break;
                case 1:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++) 
                            outData[var].set(vlen * nn + cmp, 
                                    u *       inData[var].get(vlen * (m + 1) + cmp) +
                                    (1 - u) * inData[var].get(vlen *  m +      cmp));
                    }
                    break;
                case 2:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++) 
                            try {
                            outData[var].set(vlen * nn + cmp, 
                                    v *       inData[var].get(vlen * (m + n0) + cmp) +
                                    (1 - v) * inData[var].get(vlen *  m +       cmp));
                            } catch (Exception e) {
                                System.out.printf("var=%d vlen=%d m=%d cmp=%d onn=%d%n", var,vlen,(vlen * (m + n0 + 1) + cmp),cmp,vlen * nn + cmp);
                            }
                    }
                    break;
                case 3:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++) 
                            try {
                            outData[var].set(vlen * nn + cmp, 
                                    v *       (u *      inData[var].get(vlen * (m + n0 + 1) + cmp) +
                                              (1 - u) * inData[var].get(vlen * (m + n0) +     cmp)) + 
                                    (1 - v) * (u *      inData[var].get(vlen * (m +      1) + cmp) +
                                              (1 - u) * inData[var].get(vlen *  m +           cmp)));
                            } catch (Exception e) {
                                System.out.printf("var=%d vlen=%d m=%d cmp=%d nn=%d%n", var,vlen,(vlen * (m + n0 + 1) + cmp),cmp,vlen * nn + cmp);
                            }
                    }
                    break;
            }  
        }
    }
    
    protected static void remapAffine(int trueDim, float[][] affine, 
                               float[] inCoords, FloatLargeArray outCoords, 
                               int in, int out)
    {
        if (trueDim * in >= inCoords.length || 3 * out >= outCoords.length())
            return;
        float[] v = new float[trueDim];
        float[] q = new float[3];
        System.arraycopy(inCoords, trueDim * in, v, 0, trueDim);
        System.arraycopy(affine[3], 0, q, 0, trueDim);
        for (int l = 0; l < trueDim; l++)
            for (int m = 0; m < trueDim; m++)
                q[l] += affine[m][l] * v[m];
        for (int i = 0; i < trueDim; i++) 
            outCoords.set(3 * out + i, q[i]);
    }

    public static IrregularField 
        createOutputStreamlines(RegularField inField, String vfName,
                                int nSrc, int nSteps, 
                                float[][] inCoords, float[][] inTime)
    {
        int[] dims = inField.getDims();
        int trueDim = inField.getTrueNSpace();
        FloatLargeArray fldCoords = inField.getCoords(0);
        float[][] affine = inField.getAffine();
        int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        
        int nOutputNodes = nSrc * nSteps;
        int[] lines = new int[2 * nSrc * (nSteps - 1)];
        FloatLargeArray coords = new FloatLargeArray(3 * nOutputNodes);
        int[] indices = new int[nOutputNodes];
        int nComp = inField.getNComponents();
        DataArray[] interpolatedComponents = new DataArray[nComp + 1];
        FloatLargeArray[] inData = new FloatLargeArray[nComp + 1];
        int[] vLens = new int[nComp + 1];
        FloatLargeArray[] outData = new FloatLargeArray[nComp + 1];
        int nInterpolated = 0, nInterpolatedComponents = 0;
        for (int i = 0; i < inField.getNComponents(); i++) 
            if (inField.getComponent(i).isNumeric()) {
                interpolatedComponents[nInterpolated] = inField.getComponent(i);
                vLens[nInterpolated] = inField.getComponent(i).getVectorLength();
                if(inField.getComponent(i).getType() == DataArrayType.FIELD_DATA_FLOAT) {
                    inData[nInterpolated] = (FloatLargeArray)inField.getComponent(i).getRawArray();
                }
                else {
                    inData[nInterpolated] = inField.getComponent(i).getRawFloatArray();
                }
                outData[nInterpolated] = new FloatLargeArray(vLens[nInterpolated] * nOutputNodes);
                nInterpolated += 1;
            }
        nInterpolatedComponents = nInterpolated;
        if (fldCoords == null) {
            for (int iSrc = 0; iSrc < nSrc; iSrc++) {
                int iout = iSrc;
                for (int iin = 0; iin < nSteps; iin++, iout += nSrc) {
                    remapAffine(trueDim, affine, inCoords[iSrc], coords, iin, iout);
                    indices[iout] = iin;
                }
            }
            Thread[] workThreads = new Thread[nThreads];
            for (int n = 0; n < nThreads; n++) {
                if (dims.length == 3)
                    workThreads[n] = new Thread(new InterpolateTo3DLines(n, nThreads, dims, nSrc, nSteps, 
                                                                         inCoords, indices, inData, vLens, outData));
                else
                    workThreads[n] = new Thread(new InterpolateTo2DLines(n, nThreads, dims, nSrc, nSteps, 
                                                                         inCoords, indices, inData, vLens, outData));
                workThreads[n].start();
            }
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (InterruptedException e) {
                }
        } else {
            inData[nInterpolated] = fldCoords;
            outData[nInterpolated] = coords;
            vLens[nInterpolated] = 3;
            nInterpolated += 1;
            
            Thread[] workThreads = new Thread[nThreads];
            for (int n = 0; n < nThreads; n++) {
                if (dims.length == 3)
                    workThreads[n] = new Thread(new InterpolateTo3DLines(n, nThreads, dims, nSrc, nSteps, 
                                                                         inCoords, indices, inData, vLens, outData));
                else
                    workThreads[n] = new Thread(new InterpolateTo2DLines(n, nThreads, dims, nSrc, nSteps, 
                                                                         inCoords, indices, inData, vLens, outData));
                workThreads[n].start();
            }
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (InterruptedException e) {
                }
        }
        inCoords = null;
        for (int i = 0, j = 0; i < nSrc; i++)
            for (int k = 0; k < nSteps - 1; k++, j += 2) {
                lines[j] = i + k * nSrc;
                lines[j + 1] = lines[j] + nSrc;
            }
        byte[] edgeOrientations = new byte[lines.length / 2];
        for (int i = 0; i < edgeOrientations.length; i++)
            edgeOrientations[i] = 1;
        IrregularField outField = new IrregularField(nOutputNodes);
        outField.setCurrentCoords(coords);
        DataArray da = DataArray.create(indices, 1, "steps");
        da.recomputeStatistics(true);
        outField.addComponent(da);
        CellArray streamLines = new CellArray(CellType.SEGMENT, lines, edgeOrientations, null);
        CellSet cellSet = new CellSet(inField.getName() + " " + vfName + " streamlines");
        cellSet.setBoundaryCellArray(streamLines);
        cellSet.setCellArray(streamLines);
        outField.addCellSet(cellSet);
        if (inTime != null)
        {
            float[] times = new float[nOutputNodes];
            int l = inTime[0].length;
            for (int i = 0, j = 0; i < nSrc; i++, j += l)
                System.arraycopy(inTime[i], 0, times, j, l);
            outField.addComponent(DataArray.create(times, 1, "time"));
        }
        
        for (int i = 0; i < nInterpolatedComponents; i++) 
            outField.addComponent(DataArray.create(outData[i], vLens[i], 
                                                   interpolatedComponents[i].getName(), 
                                                   interpolatedComponents[i].getUnit(),
                                                   interpolatedComponents[i].getUserData()).
                                                   preferredRanges(interpolatedComponents[i].getPreferredMinValue(), 
                                                                   interpolatedComponents[i].getPreferredMaxValue(), 
                                                                   interpolatedComponents[i].getPreferredPhysMinValue(), 
                                                                   interpolatedComponents[i].getPreferredPhysMaxValue()));
        return outField;
    }

    public static IrregularField 
        createOutputStreamlines(RegularField inField, String vfName,
                                int nSrc, int nSteps, 
                                float[][] inCoords)
    {
        return createOutputStreamlines(inField, vfName, nSrc, nSteps, inCoords, null);
    }
}
