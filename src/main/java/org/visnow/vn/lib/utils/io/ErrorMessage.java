/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.io;

import org.apache.log4j.Logger;
import org.visnow.vn.gui.widgets.FileErrorFrame;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;

/**
 *
 * @author know
 */
public class ErrorMessage
{
    private static final Logger LOGGER = Logger.getLogger(ErrorMessage.class);

    private ErrorMessage()
    {
    }

    public static void outputError(String text, String fname, int lineNumber, Exception e)
    {
        FileErrorFrame.display(text, fname, lineNumber, e);
        String message = "ERROR: " + text + "\nin file " + fname + "\nline " + lineNumber + "\nSee log for details\n";
        VisNow.get().userMessageSend(null, "Error",
                 message, Level.ERROR);
        LOGGER.warn(message, e);
    }
}
