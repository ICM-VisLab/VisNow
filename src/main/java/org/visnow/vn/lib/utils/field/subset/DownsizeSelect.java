/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field.subset;

import java.util.Arrays;
import org.apache.commons.lang3.ArrayUtils;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.vn.lib.gui.FieldBasedUI.DownsizeUI.DownsizeParams;

/**
 *
 * @author know
 */


public class DownsizeSelect {

    public static long[] select(Field inField, DownsizeParams params, int maxNGlyphs,
                                NodeSelector nodeSelector,
                                int[] tLow, int[] tUp)
    {
        boolean random =      params.isRandom();
        boolean centered =    params.isCentered();
        int[] ind =           params.getShuffledIndices();
        long[] lInd =         params.getSampledIndices();
        int start =           params.getStartIndex();
        int nGlyphs;
        long[] gl = new long[maxNGlyphs];
        nGlyphs = 0;
        if (inField instanceof RegularField && !random) {
            RegularField inRegularField = (RegularField) inField;
            int[] dims = inRegularField.getDims();
            int[] low  = ArrayUtils.clone(tLow);
            int[] up   = ArrayUtils.clone(tUp);
            int[] down = params.getDown();
            for (int i = 0; i < dims.length; i++) {
                maxNGlyphs *= (up[i] - low[i]) / down[i] + 1;
                if (centered){
                    int shift = (up[i] - low[i]) % down[i];
                    low[i] += shift / 2;
                }
            }
            Arrays.fill(gl, -1);
            long l;
            switch (dims.length) {
                case 3:
                    for (long i = low[2]; i < up[2]; i += down[2])
                        for (long j = low[1]; j < up[1]; j += down[1]) {
                            l = (dims[1] * i + j) * dims[0] + low[0];
                            for (long k = low[0]; k < up[0]; k += down[0], l += down[0])
                                if (nodeSelector.isValid(l)) {
                                    gl[nGlyphs] = l;
                                    nGlyphs += 1;
                                }
                        }
                    break;
                case 2:
                    for (long j = low[1]; j < up[1]; j += down[1]) {
                        l = j * dims[0] + low[0];
                        for (long k = low[0]; k < up[0]; k += down[0], l += down[0])
                            if (nodeSelector.isValid(l)) {
                                gl[nGlyphs] = l;
                                nGlyphs += 1;
                            }
                    }
                    break;
                case 1:
                    l = low[0];
                    for (long k = low[0]; k < up[0]; k += down[0], l += down[0])
                        if (nodeSelector.isValid(l)) {
                            gl[nGlyphs] = l;
                            nGlyphs += 1;
                        }
                    break;
            }
        } else {
            Arrays.fill(gl, -1);
            if (maxNGlyphs >= inField.getNNodes()) {
                for (long i = 0; i < inField.getNNodes(); i ++)
                    if (nodeSelector.isValid(i)) {
                        gl[nGlyphs] = i;
                        nGlyphs += 1;
                    }
            }
            else if (ind != null) {
                for (int i = 0; i < ind.length && nGlyphs < maxNGlyphs; i++) {
                    int k = (i + start) % ind.length;
                    if (nodeSelector.isValid(ind[k])) {
                        gl[nGlyphs] = ind[k];
                        nGlyphs += 1;
                    }
                }
            }
            else if (lInd != null) {
                int nValidPoints = 0;
                for (long i = 0; i < inField.getNNodes(); i ++)
                    if (nodeSelector.isValid(i))
                        nValidPoints += 1;
                if (nValidPoints < maxNGlyphs) {
                    for (long i = 0; i < inField.getNNodes(); i ++)
                        if (nodeSelector.isValid(i) && nGlyphs < gl.length) {
                            gl[nGlyphs] = i;
                            nGlyphs += 1;
                        }
                }
                else
                    for (int i = 0; i < lInd.length && nGlyphs < maxNGlyphs; i++) {
                        int k = (i + start) % lInd.length;
                        if (nodeSelector.isValid(lInd[k])) {
                            gl[nGlyphs] = lInd[k];
                            nGlyphs += 1;
                        }
                    }
            }
        }
        long[] glyphIn = new long[nGlyphs];
        System.arraycopy(gl, 0, glyphIn, 0, nGlyphs);
        return glyphIn;
    }

}
