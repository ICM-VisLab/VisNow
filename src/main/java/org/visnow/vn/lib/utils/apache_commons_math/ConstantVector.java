/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.apache_commons_math;
import java.util.Arrays;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.NotPositiveException;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.linear.RealVector;

/**
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * University of Warsaw, ICM
 */
public class ConstantVector extends RealVector 
{
    private int n;
    private float v;
    private int nThreads = Runtime.getRuntime().availableProcessors();
    
    public ConstantVector(float v, int n)
    {
        this.v = v;
        this.n = n;
    }

    @Override
    public int getDimension()
    {
        return n;
    }

    @Override
    public double getEntry(int i) throws OutOfRangeException
    {
        if (i < 0 || i >= n)
            throw new OutOfRangeException(i, 0, n);
        return v;
    }

    @Override
    public void setEntry(int i, double d) throws OutOfRangeException
    {
        throw new OutOfRangeException(i, 0, n);
    }

    @Override
    public RealVector append(RealVector rv)
    {
        return new ConstantVector(v, n + rv.getDimension());
    }

    @Override
    public RealVector append(double d)
    {
        return new ConstantVector(v, n + 1);
    }

    @Override
    public RealVector getSubVector(int i, int k) throws NotPositiveException, OutOfRangeException
    {
        return new ConstantVector(v, k);
    }

    @Override
    public void setSubVector(int i, RealVector rv) throws OutOfRangeException
    {
            throw new OutOfRangeException(i, 0, n);
    }

    @Override
    public boolean isNaN()
    {
        return Float.isNaN(v);
    }

    @Override
    public boolean isInfinite()
    {
        return Float.isInfinite(v);
    }
    
    private class DotProductPart implements Runnable
    {
        private int iThread; 
        private float[] rv;
        private double[] dpParts;

        public DotProductPart(int iThread, float[] rv, double[] dpParts)
        {
            this.iThread = iThread;
            this.rv = rv;
            this.dpParts = dpParts;
        }
        
        @Override
        public void run()
        {
            double partDotProduct = 0;
            for (int i = (iThread * n) / nThreads; i < ((iThread + 1) * n) / nThreads; i++)
                partDotProduct += v * rv[i];
            dpParts[iThread] = partDotProduct;
        }

    }
    
    @Override
    public double dotProduct(RealVector rv) throws DimensionMismatchException
    {
        if (!(rv instanceof ConstantVector))
            return super.dotProduct(rv);
        if (n != rv.getDimension())
            throw new DimensionMismatchException(n, rv.getDimension());
        double dp = 0;
        double[] dpParts = new double[nThreads];
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new DotProductPart(iThread, ((ConstantVector)rv).getDataArray(), dpParts));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try
            {
                workThread.join();
            }catch (InterruptedException e) {
            }
        for (int i = 0; i < workThreads.length; i++)
            dp += dpParts[i];
        return dp;
    }


    @Override
    public RealVector copy()
    {
        return new ConstantVector(v, n);
    }

    @Override
    public RealVector ebeDivide(RealVector rv) throws DimensionMismatchException
    {
        if (n != rv.getDimension())
            throw new DimensionMismatchException(rv.getDimension(), n);
        float[] tmpV = new float[n];
        for (int i = 0; i < n; i++) 
            tmpV[i] = (float)(v / rv.getEntry(i));
        return new FloatArrayVector(tmpV);
    }

    @Override
    public RealVector ebeMultiply(RealVector rv) throws DimensionMismatchException
    {
        if (n != rv.getDimension())
            throw new DimensionMismatchException(rv.getDimension(), n);
        float[] tmpV = new float[n];
        for (int i = 0; i < n; i++) 
            tmpV[i] = (float)(v * rv.getEntry(i));
        return new FloatArrayVector(tmpV);
        
    }
    
    public float[] getDataArray()
    { 
       float[] vals = new float[n];
       Arrays.fill(vals, v);
       return vals;
    }
    
}
