/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.io;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.visnow.jscic.Field;

/**
 * @author Piotr Wendykier ICM, University of Warsaw
 */
public class SerializedFieldWriter
{
    private static final Logger LOGGER = Logger.getLogger(SerializedFieldWriter.class);

    public static boolean writeField(Field inField, String path, boolean overwrite)
    {
        String baseName = FilenameUtils.removeExtension(path);
        try {
            File dataFile = new File(baseName + "." + "vns");
            if (dataFile != null) {
                if (dataFile.isDirectory()) {
                    LOGGER.error("Expected file instead of directory.");
                    return false;
                }
                if (!dataFile.getParentFile().canWrite()) {
                    LOGGER.error("cannot write to file " + dataFile.getAbsolutePath());
                    return false;
                }
                if (!overwrite && dataFile.exists()) {
                    LOGGER.error("cannot overwrite file " + dataFile.getAbsolutePath());
                }
            }
            BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream(dataFile));
            ObjectOutputStream objOut = new ObjectOutputStream(bout);
            objOut.writeObject(inField);
            objOut.close();
            return true;
        } catch (IOException ex) {
            return false;
        }
    }
}
