/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import java.util.Arrays;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.PointField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.lib.utils.field.subset.FieldSubset;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class PointFldToIrregularFld
{
    private PointFldToIrregularFld()
    {
    }

    public static IrregularField convert(PointField inField)
    {
        if (inField.getNNodes() > 2L << 31)
            throw new IllegalArgumentException("large point field can not be converted to irregular field");
        int nNodes = (int) inField.getNNodes();
        boolean isMask = inField.hasMask();
        int nOutNodes = 0;
        IrregularField outIrregularField;
        if (isMask) {
            LogicLargeArray mask = inField.getCurrentMask();
            for (int i = 0; i < nNodes; i++)
                if (mask.getBoolean(i))
                    nOutNodes += 1;
            outIrregularField = new IrregularField(nOutNodes);
            outIrregularField.setCoords(FieldSubset.select(inField.getCoords(), 3, nOutNodes, mask));
            for (DataArray inDa : inField.getComponents())
                outIrregularField.addComponent(FieldSubset.select(inDa, nOutNodes, inField.getCurrentMask()));
        }
        else {
            nOutNodes = nNodes;
            outIrregularField = new IrregularField(nOutNodes);
            outIrregularField.setCoords(inField.getCoords().cloneShallow());
            for (DataArray inDa : inField.getComponents())
                outIrregularField.addComponent(inDa.cloneShallow());
        }
        if (outIrregularField.getComponent("subsets") != null &&
            outIrregularField.getComponent("subsets").getType() == DataArrayType.FIELD_DATA_BYTE) {
            String[] names = outIrregularField.getComponent("subsets").getUserData();
            UnsignedByteLargeArray sets = outIrregularField.getComponent("subsets").getRawByteArray();
            int[] nodesInSet = new int[255];
            Arrays.fill(nodesInSet, 0);
            for (long i = 0; i < nOutNodes; i++)
                nodesInSet[sets.getInt(i)] += 1;
            int[][] pts = new int[255][];
            for (int i = 0; i < nodesInSet.length; i++)
                if (nodesInSet[i] > 0) {
                    pts[i] = new int[nodesInSet[i]];
                    nodesInSet[i] = 0;
                }
            for (long i = 0; i < nOutNodes; i++) {
                int k = sets.getInt(i);
                pts[k][nodesInSet[k]] = (int)i;
                nodesInSet[k] += 1;
            }
            for (int i = 0; i < nodesInSet.length; i++)
                if (nodesInSet[i] > 0) {
                    CellArray ptsArr = new CellArray(CellType.POINT, pts[i], null, null);
                    CellSet cs = new CellSet(names != null && names.length >= i ?
                                             names[i] :
                                             "subset" + i);
                    cs.addCells(ptsArr);
                    outIrregularField.addCellSet(cs);
                }
        }
        else {
            int[] pts = new int[nOutNodes];
            for (int i = 0; i < pts.length; i++)
                pts[i] = i;
            CellArray ptsArr = new CellArray(CellType.POINT, pts, null, null);
            CellSet cs = new CellSet("cells");
            cs.addCells(ptsArr);
            outIrregularField.addCellSet(cs);
        }
        return outIrregularField;
    }
}
