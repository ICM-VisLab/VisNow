/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.probeInterfaces;

import org.visnow.vn.lib.utils.graphing.*;
import java.awt.Color;
import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class NumericDisplayParams extends Parameters
{
    public static final  Color[] DEFAULT_COLORS = new Color[]{
        new Color(1.f, .5f, .5f), new Color(1.f, 1.f, .5f), new Color(.5f, 1.f, .5f),
        new Color(.5f, 1.f, 1.f), new Color(.5f, .5f, 1.f), new Color(1.f, .5f, 1.f),
        new Color(.8f, .8f, .8f)};
    
    public static final String EXTENTS = "extents";
    
    public static final String X_POS = "x position";
    public static final String Y_POS = "y position";
    public static final String LEGEND_X_POS = "legend x position";
    public static final String LEGEND_Y_POS = "legend y position";
    public static final String COLOR = "color";
    public static final String COLOR_AUTO = "color auto";
    public static final String FONT_SIZE = "font size";
    public static final String LINE_WIDTH = "line width";
    public static final String DATA = "data";
    public static final String REFRESH = "refresh";
    public static final String COORDINATES = "coordinates";
    public static final String INDEX = "index";    
    public static final String DRAW_FRAME = "draw frame";
    public static final String FILL_RECT = "fill rectangle";
    public static final String WINDOW_BGR_COLOR = "window background color";
    public static final String USER_BGR_COLOR = "user background color";
    public static final String EFFECTIVE_BGR_COLOR = "effective background color";
    public static final String BGR_TRANSP = "background transparency";
    public static final String AUTO_BACKGROUND = "background color from window";
    public static final String TIME = "time";
    
    private static final float DEFAULT_X =  .1f;
    private static final float DEFAULT_Y =  .95f;
    private static final float DEFAULT_T =  0.f;

    protected static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Float>(X_POS,                 ParameterType.dependent, DEFAULT_X),
        new ParameterEgg<Float>(Y_POS,                 ParameterType.dependent, DEFAULT_Y),
        new ParameterEgg<Float>(LEGEND_X_POS,          ParameterType.dependent, 1f),
        new ParameterEgg<Float>(LEGEND_Y_POS,          ParameterType.dependent, 1f),
        new ParameterEgg<Color>(COLOR,                 ParameterType.independent, Color.WHITE),
        new ParameterEgg<Boolean>(COLOR_AUTO,          ParameterType.independent, true),
        new ParameterEgg<Float>(BGR_TRANSP,            ParameterType.dependent, .5f),
        new ParameterEgg<Color>(WINDOW_BGR_COLOR,      ParameterType.independent, Color.BLACK),
        new ParameterEgg<Color>(USER_BGR_COLOR,        ParameterType.independent, Color.BLACK),
        new ParameterEgg<Boolean>(AUTO_BACKGROUND,     ParameterType.independent, true),
        new ParameterEgg<Float>(FONT_SIZE,             ParameterType.independent, .01f),
        new ParameterEgg<Float>(LINE_WIDTH,            ParameterType.independent, 1.f),
        new ParameterEgg<float[]>(COORDINATES,         ParameterType.independent, new float[]{0, 0, 0}),
        new ParameterEgg<float[]>(INDEX,               ParameterType.dependent,   new float[]{0, 0, 0}),
        new ParameterEgg<DisplayedData[]>(DATA,        ParameterType.independent, null),
        new ParameterEgg<Float>(TIME,                  ParameterType.dependent,   DEFAULT_T),
        new ParameterEgg<Boolean>(REFRESH,             ParameterType.independent, false),
        new ParameterEgg<Boolean>(DRAW_FRAME,          ParameterType.independent, false),
        new ParameterEgg<Boolean>(FILL_RECT,           ParameterType.independent, true),
    };

    public NumericDisplayParams()
    {
        super(eggs);
        setValue(DATA, new DisplayedData[]{new DisplayedData(0, Color.WHITE)});
    }
    
/**
 * x coordinate of lower left graph area corner relative to the window 
 * @return x coordinate of lower left graph area corner
 */
    public float getXPosition()
    {
        return (Float) getValue(X_POS);
    }
    
/**
 * Setter for graph area position
 * @param x new x coordinate of lower left graph area corner relative to the window
 */
    public void setXPosition(float x)
    {
        setValue(X_POS, x);
        fireParameterChanged(EXTENTS);
    }
    
/**
 * y coordinate of lower left graph area corner relative to the window 
 * @return y coordinate of lower left graph area corner
 */
    public float getYPosition()
    {
        return (Float) getValue(Y_POS);
    }

/**
 * Setter for graph area position
 * @param y new y coordinate of lower left graph area corner relative to the window
 */
    public void setYPosition(float y)
    {
        setValue(Y_POS, y);
        fireParameterChanged(EXTENTS);
    }
    
    
/**
 * x coordinate of lower left graph legend area corner relative to the window 
 * @return x coordinate of lower left graph legend area corner
 */
    public float getLegendXPosition()
    {
        return (Float) getValue(LEGEND_X_POS);
    }
    
/**
 * Setter for graph legend area position
 * @param x new x coordinate of lower left graph legend area corner relative to the window
 */
    public void setLegendXPosition(float x)
    {
        setValue(LEGEND_X_POS, x);
        fireParameterChanged(EXTENTS);
    }
    
/**
 * y coordinate of lower left graph  legend area corner relative to the window 
 * @return y coordinate of lower left graph legend area corner
 */
    public float getLegendYPosition()
    {
        return (Float) getValue(LEGEND_Y_POS);
    }

/**
 * Setter for graph legend area position
 * @param y new y coordinate of lower left legend graph area corner relative to the window
 */
    public void setLegendYPosition(float y)
    {
        setValue(LEGEND_Y_POS, y);
        fireParameterChanged(EXTENTS);
    }
    
    public void setBottomLeftCorner(float x, float y)
    {
        setValue(X_POS, x);
        setValue(Y_POS, y);
        fireParameterChanged(EXTENTS);
    }
    
 /**
  * resets graph area to default rectangle near lower left window area
  */   
    public void resetTextArea()
    {
        setValue(X_POS, DEFAULT_X);
        setValue(Y_POS, DEFAULT_Y);
        fireParameterChanged(EXTENTS);
    }
    
    public Color getColor()
    {
        return (Color) getValue(COLOR);
    }

    public void setColor(Color color)
    {
        setValue(COLOR, color);
        fireParameterChanged(COLOR);
    }

    public Color getUserBgrColor()
    {
        return (Color) getValue(USER_BGR_COLOR);
    }

    public void setUserBgrColor(Color color)
    {
        setValue(USER_BGR_COLOR, color);
        updateEffectiveBgrColor();
        fireParameterChanged(EFFECTIVE_BGR_COLOR);
    }

    public Color getWindowBgrColor()
    {
        return (Color) getValue(WINDOW_BGR_COLOR);
    }

    public void setWindowBgrColor(Color color)
    {
        setValue(WINDOW_BGR_COLOR, color);
        updateEffectiveBgrColor();
        fireParameterChanged(EFFECTIVE_BGR_COLOR);
    }

    public Float getBgrTransparency()
    {
        return (Float) getValue(BGR_TRANSP);
    }

    public void setBgrTransparency(float val)
    {
        setValue(BGR_TRANSP, val);
        updateEffectiveBgrColor();
        fireParameterChanged(EFFECTIVE_BGR_COLOR);
    }

    public boolean isAutoBgr()
    {
        return (Boolean) getValue(AUTO_BACKGROUND);
    }

    public void setAutoBgr(boolean cl)
    {
        setValue(AUTO_BACKGROUND, cl);
        updateEffectiveBgrColor();
        fireParameterChanged(EFFECTIVE_BGR_COLOR);
    }

    public boolean fillRect()
    {
        boolean r = (Boolean) getValue(FILL_RECT);
        return r;
    }

    public void setFillRect(boolean r)
    {
        setValue(FILL_RECT, r);
        updateEffectiveBgrColor();
        fireParameterChanged(EFFECTIVE_BGR_COLOR);
    }
    
    private Color effectiweBgrColor = Color.BLACK;
    
    private void updateEffectiveBgrColor()
    {
        if (isAutoBgr() || !fillRect())
            effectiweBgrColor = getWindowBgrColor();
        else {
            float[] windowBgr = new float[3];
            getWindowBgrColor().getRGBColorComponents(windowBgr);
            float[] userBgr   = new float[3];
            getUserBgrColor().getRGBColorComponents(userBgr);
            float t = getBgrTransparency();
            effectiweBgrColor = 
                    new Color(t * userBgr[0] + (1 - t) * windowBgr[0],
                              t * userBgr[1] + (1 - t) * windowBgr[1],
                              t * userBgr[2] + (1 - t) * windowBgr[2]);
        }
    }

    public Color getEffectiweBgrColor()
    {
        return effectiweBgrColor;
    }
    
    
    public DisplayedData[] getDisplayedData()
    {
        return (DisplayedData[]) getValue(DATA);
    }

    public void setDisplayedData(DisplayedData[] displayedData)
    {
        setValue(DATA, displayedData);
        fireParameterChanged(DATA);
    }

    public float getFontSize()
    {
        return (Float) getValue(FONT_SIZE);
    }

    public void setFontSize(float size)
    {
        setValue(FONT_SIZE, size);
        fireParameterChanged(FONT_SIZE);
    }

    public float getLineWidth()
    {
        return (Float) getValue(LINE_WIDTH);
    }

    public void setLineWidth(float width)
    {
        setValue(LINE_WIDTH, width);
        fireParameterChanged(LINE_WIDTH);
    }

    public float[] getCoordinates()
    {
        return (float[]) getValue(COORDINATES);
    }

    public void setCoordinates(float[] arg)
    {
        setValue(COORDINATES, arg);
        fireParameterChanged(COORDINATES);
    }

    public float[] getIndices()
    {
        return (float[]) getValue(INDEX);
    }

    public void setIndices(float[] args)
    {
        setValue(INDEX, args);
        fireParameterChanged(INDEX);
    }
    
    public void updateTable()
    {
        fireParameterChanged(DATA);
    }

    public boolean refresh()
    {
        boolean r = (Boolean) getValue(REFRESH);
        setValue(REFRESH, false);
        return r;
    }

    public void setRefresh(boolean r)
    {
        setValue(REFRESH, false);
        fireParameterChanged(EXTENTS);
    }
    
    public boolean drawFrame()
    {
        boolean r = (Boolean) getValue(DRAW_FRAME);
        return r;
    }

    public void setDrawFrame(boolean r)
    {
        setValue(DRAW_FRAME, r);
        fireParameterChanged(DRAW_FRAME);
    }

    public float getTime()
    {
        return (Float)getValue(TIME);
    }
    
    public void setTime(float val)
    {
        setValue(TIME, val);
        fireParameterChanged(DATA);
    }
    
    public boolean colorAuto()
    {
        return (Boolean)getValue(COLOR_AUTO);
    }
    
    public void setColorAuto(boolean auto)
    {
        setValue(COLOR_AUTO, auto);
    }
    
    
}
