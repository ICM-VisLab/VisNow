/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.graph3d;

import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class GenericGraph3DShared
{
    public static final String COMPONENT_STRING =     "component";
    public static final String SCALE_STRING =         "scale";
    public static final String ZERO_BASED_STRING =    "zeroBased";
    public static final String GLOBAL_RANGE_STRING =  "global";
    public static final String UPDATING_STRING =      "updating";
    public static final String ADJUSTING_STRING =     "adjusting";

    public static final ParameterName<ComponentFeature> COMPONENT =   new ParameterName<>(COMPONENT_STRING);
    public static final ParameterName<Float> SCALE =                  new ParameterName<>(SCALE_STRING);
    public static final ParameterName<Boolean> GLOBAL_RANGE =         new ParameterName<>(GLOBAL_RANGE_STRING);
    public static final ParameterName<Boolean> ZERO_BASED =           new ParameterName<>(ZERO_BASED_STRING);
    public static final ParameterName<Boolean> ADJUSTING =            new ParameterName<>(ADJUSTING_STRING);
    public static final ParameterName<Boolean> UPDATING =             new ParameterName<>(UPDATING_STRING);


    public static final Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(COMPONENT,    new ComponentFeature(sch -> sch.isNumeric() && sch.getType() != DataArrayType.FIELD_DATA_COMPLEX)),
            new Parameter<>(SCALE,        .3f),
            new Parameter<>(GLOBAL_RANGE, false),
            new Parameter<>(ZERO_BASED,   true),
            new Parameter<>(ADJUSTING,    false),
            new Parameter<>(UPDATING,     false),
        };
    }

}
