/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.RegularField;
import org.visnow.jlargearrays.FloatLargeArray;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.utils.ConvertUtils;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class FieldSmoothDown
{

    public static FloatLargeArray smoothDownArray(FloatLargeArray in, int[] inDims, int[] down, float sigma, int nThreads)
    {
        int dim = inDims.length;
        FloatLargeArray cIn = null, cOut = null;
        cIn = in;
        int[] dims = new int[dim];
        int[] outDims = new int[dim];
        System.arraycopy(inDims, 0, dims, 0, dim);
        System.arraycopy(inDims, 0, outDims, 0, dim);
        long len = 1;
        for (int i = 0; i < dim; i++) {
            len *= dims[i];
        }
        int vlen = (int) (in.length() / len);

        for (int direction = dim - 1; direction >= 0; direction--) {
            int d = down[direction];
            outDims[direction] = (dims[direction] + d - 1) / d;
            int nOut = 1;
            for (int i = 0; i < outDims.length; i++)
                nOut *= outDims[i];
            cOut = new FloatLargeArray(nOut * vlen);
            if (nThreads < 2) {
                DirectionalLargeFieldSmoothing smoother = new DirectionalLargeFieldSmoothing(direction, dims, down[direction],
                                                                                             cIn, cOut, sigma, 1, 0);
                smoother.run();
            } else {
                Thread[] workThreads = new Thread[nThreads];
                DirectionalLargeFieldSmoothing[] smoothers = new DirectionalLargeFieldSmoothing[nThreads];
                for (int i = 0; i < workThreads.length; i++) {
                    smoothers[i] = new DirectionalLargeFieldSmoothing(direction, dims, down[direction],
                                                                      cIn, cOut, sigma, nThreads, i);
                    workThreads[i] = new Thread(smoothers[i]);
                    workThreads[i].start();
                }
                for (int i = 0; i < workThreads.length; i++)
                    try {
                        workThreads[i].join();
                    } catch (Exception e) {
                    }
            }
            dims[direction] = outDims[direction];
            cIn = cOut;
        }
        return cOut;
    }

    public static RegularField smoothDownToFloat(RegularField inField, int[] down, float sigma, int nThreads)
    {
        if (inField == null || inField.getDims() == null)
            return null;
        int[] inDims = inField.getDims();
        int dim = inDims.length;
        int[] outDims = new int[dim];
        for (int i = 0; i < dim; i++)
            outDims[i] = (inDims[i] + down[i] - 1) / down[i];
        RegularField outField = new RegularField(outDims);
        float[][] inAffine = inField.getAffine();
        float[][] outAffine = new float[4][3];
        for (int i = 0; i < 3; i++) {
            outAffine[3][i] = inAffine[3][i];
            for (int j = 0; j < dim; j++)
                outAffine[j][i] = inAffine[j][i] * down[i];
        }
        outField.setAffine(outAffine);
        if (inField.getCurrentCoords() != null)
            outField.setCurrentCoords(smoothDownArray(inField.getCurrentCoords(), inDims, down, sigma, nThreads));
        for (int component = 0; component < inField.getNComponents(); component++) {
            if (inField.getComponent(component).isNumeric())
                outField.addComponent(DataArray.create(smoothDownArray(inField.getComponent(component).getRawFloatArray(), inDims, down, sigma, nThreads),
                                                       inField.getComponent(component).getVectorLength(), inField.getComponent(component).getName()));
        }
        return outField;
    }

    public static RegularField smoothDownToFloat(RegularField inField, int down, float sigma, int nThreads)
    {
        return smoothDownToFloat(inField, new int[]{down, down, down}, sigma, nThreads);
    }

    public static RegularField smoothDownToFloat(RegularField inField, int down, int nThreads)
    {
        return smoothDownToFloat(inField, new int[]{down, down, down}, (float) down, nThreads);
    }

    public static RegularField smoothDown(RegularField inField, int[] down, float sigma, boolean retainMinMax, int nThreads)
    {
        if (inField == null || inField.getDims() == null || inField.getDims().length < 2)
            return null;
        int[] inDims = inField.getDims();
        LargeArray outData = null;
        int dim = inDims.length;
        int[] outDims = new int[dim];
        for (int i = 0; i < dim; i++)
            outDims[i] = (inDims[i] + down[i] - 1) / down[i];
        RegularField outField = new RegularField(outDims);
        float[][] inAffine = inField.getAffine();
        float[][] outAffine = new float[4][3];
        for (int i = 0; i < 3; i++) {
            outAffine[3][i] = inAffine[3][i];
            for (int j = 0; j < 3; j++)
                outAffine[j][i] = inAffine[j][i] * down[i];
        }
        outField.setAffine(outAffine);
        if (inField.getCurrentCoords() != null)
            outField.setCurrentCoords(smoothDownArray(inField.getCurrentCoords(), inDims, down, sigma, nThreads));
        for (int component = 0; component < inField.getNComponents(); component++) {
            DataArray da = inField.getComponent(component);
            FloatLargeArray outFData;
            if (da.getType() == DataArrayType.FIELD_DATA_FLOAT) {
                outFData = smoothDownArray((FloatLargeArray) da.getRawArray(), inDims, down, sigma, nThreads);
            } else {
                outFData = smoothDownArray(da.getRawFloatArray(), inDims, down, sigma, nThreads);
            }
            if (inField.getComponent(component).isNumeric())
                switch (inField.getComponent(component).getType()) {
                    case FIELD_DATA_BYTE:
                        outData = new UnsignedByteLargeArray(outFData.length(), false);
                        for (long i = 0; i < outFData.length(); i++)
                            outData.setByte(i, (byte) ((int) (max(0, min(255, (int) outFData.getFloat(i)))) & 0xff));
                        break;
                    case FIELD_DATA_LOGIC:
                        outData = new LogicLargeArray(outFData.length(), false);
                        for (long i = 0; i < outFData.length(); i++)
                            outData.setBoolean(i, outFData.getFloat(i) != 0.0);
                        break;
                    case FIELD_DATA_SHORT:
                    case FIELD_DATA_INT:
                    case FIELD_DATA_DOUBLE:
                    case FIELD_DATA_COMPLEX:
                    case FIELD_DATA_STRING:
                        outData = LargeArrayUtils.create(inField.getComponent(component).getType().toLargeArrayType(), outFData.length(), false);
                        for (long i = 0; i < outFData.length(); i++)
                            outData.setFloat(i, outFData.getFloat(i));
                        break;
                    case FIELD_DATA_FLOAT:
                        outData = outFData;
                        break;
                    default:
                        throw new IllegalArgumentException("Unsupported data type");
                }
            DataArray outDA = DataArray.create(outData, da.getVectorLength(), da.getName()).unit(da.getUnit()).userData(da.getUserData());
            if (retainMinMax)
                outDA.setPreferredRanges(da.getPreferredMinValue(), da.getPreferredMaxValue(), da.getPreferredPhysMinValue(), da.getPreferredPhysMaxValue());
            outField.addComponent(outDA);
        }
        return outField;
    }

    public static RegularField smoothDown(RegularField inField, int down, float sigma, boolean retainMinMax, int nThreads)
    {
        return smoothDown(inField, new int[]{down, down, down}, sigma, retainMinMax, nThreads);
    }

    public static RegularField smoothDown(RegularField inField, int down, boolean retainMinMax, int nThreads)
    {
        return smoothDown(inField, new int[]{down, down, down}, (float) down, retainMinMax, nThreads);
    }

    public static RegularField smoothDown(RegularField inField, int[] down, float sigma, int nThreads)
    {
        return smoothDown(inField, down, sigma, true, nThreads);
    }

    public static RegularField smoothDown(RegularField inField, int down, float sigma, int nThreads)
    {
        return smoothDown(inField, new int[]{down, down, down}, sigma, true, nThreads);
    }

    public static RegularField smoothDown(RegularField inField, int down, int nThreads)
    {
        return smoothDown(inField, new int[]{down, down, down}, (float) down, true, nThreads);
    }

    private FieldSmoothDown()
    {
    }

}
