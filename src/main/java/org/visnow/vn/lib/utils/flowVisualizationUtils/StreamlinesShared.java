/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.flowVisualizationUtils;

import org.visnow.vn.engine.core.ParameterName;

import org.visnow.vn.gui.widgets.RunButton;

/**
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl),
 * Warsaw University, Interdisciplinary Centre for Mathematical and Computational Modelling
 */

public class StreamlinesShared
{
    
    public static final int NO_ANIMATION     = 0;
    public static final int STREAM_ANIMATION = 1;
    public static final int GLYPH_ANIMATION  = 2;
    // Specifications for parameters:
    
    // Value from META_VECTOR_COMPONENT_NAMES
    public static final ParameterName<String> COMPONENT = new ParameterName<>("Component (vector)");
    // ???
    public static final ParameterName<Float> STEP = new ParameterName<>("Step");
    // Non-negative integer.
    public static final ParameterName<Integer> NUM_STEPS_FORWARD = new ParameterName<>("Steps forward");
    // Non-negative integer.
    public static final ParameterName<Integer> NUM_STEPS_BACKWARD = new ParameterName<>("Steps backward");
    public static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>("Running message");
    public static final ParameterName<Integer> ANIMATION_TYPE = new ParameterName<>("animation type");
    public static final ParameterName<Boolean> COMPUTE_PERIODICITY = new ParameterName<>("compute periodicity index");
    public static final ParameterName<Boolean> SETTING_SEED_POINTS = new ParameterName<>("setting seed points");

    // Meta-parameters:
    
    public static final ParameterName<Boolean> META_IS_START_POINT_FIELD_REGULAR = new ParameterName<>("(META) Is input field regular?");
    //not-empty array of unique, non-empty strings
    public static final ParameterName<String[]> META_VECTOR_COMPONENT_NAMES = new ParameterName<>("(META) Vector component names");
    public static final ParameterName<float[]> META_PREFERRED_STEPS = new ParameterName<>("(META) Preferred steps");
}
