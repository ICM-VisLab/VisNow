/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.geometry2D;

import org.jogamp.vecmath.*;
import java.awt.*;
import org.visnow.vn.geometries.parameters.FontParams;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class CXYZString extends CXYString
{

    private float x = 0;
    private float y = 0;
    private float z = 0;

    /**
     * Creates a new instance of CXYZString
     */
    public CXYZString(String s, Color c, float x, float y, float z, Font font, float relativeHeight)
    {
        super(s, c, font, relativeHeight);
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public CXYZString(String s, float x, float y, float z, FontParams params)
    {
        this(s, params.getColor(), x, y, z, params.getFont2D(), params.getSize());
    }

    public CXYZString(String s, Color c)
    {
        super(s, c);
    }

    public CXYZString(String s)
    {
        this(s, Color.WHITE);
    }

    public void setCoords(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void update(org.visnow.vn.geometries.utils.transform.LocalToWindow ltw)
    {
        if (ltw == null)
            return;
        depth = ltw.transformPt(new Point3d((double) x, (double) y, (double) z), sCoords);
        if (depth > 1.f)
            depth = 1.f;
        if (depth < .01f)
            depth = .01f;
    }
}
