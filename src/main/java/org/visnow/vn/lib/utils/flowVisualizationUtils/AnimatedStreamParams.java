/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.flowVisualizationUtils;

import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class AnimatedStreamParams extends Parameters
{

    public static enum ANIMATION {FORWARD, STOP, BACK};
    
    public static final String SEGMENT_COUNT = "segment count";
    public static final String SEGMENT_GAP    = "segment gap";
    public static final String LINE_WIDTH     = "line width";
    public static final String DELAY          = "delay";
    public static final String ANIMATE        = "animate";

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Integer>(SEGMENT_COUNT, ParameterType.dependent, 20),
        new ParameterEgg<Integer>(SEGMENT_GAP, ParameterType.dependent, 1),
        new ParameterEgg<Float>(LINE_WIDTH, ParameterType.dependent, 1.f),
        new ParameterEgg<Integer>(DELAY, ParameterType.independent, 0),
        new ParameterEgg<ANIMATION>(ANIMATE, ParameterType.independent, ANIMATION.STOP)
    };

    public AnimatedStreamParams()
    {
        super(eggs);
    }

    public int getGap()
    {
        return (Integer) getValue(SEGMENT_GAP);
    }

    public void setGap(int val)
    {
        if (val < 0)
            return;
        setValue(SEGMENT_GAP, val);
        fireParameterChanged(SEGMENT_GAP);
    }

    public ANIMATION getAnimate()
    {
        return (ANIMATION) getValue(ANIMATE);
    }

    public void setAnimate(ANIMATION animate)
    {
        setValue(ANIMATE, animate);
        fireParameterChanged(ANIMATE);
    }

    public int getDelay()
    {
        return (Integer) getValue(DELAY);
    }

    public void setDelay(int delay)
    {
        setValue(DELAY, delay);
        fireParameterChanged(DELAY);
    }

    public int getSegmentCount()
    {
        return (Integer) getValue(SEGMENT_COUNT);
    }

    public void setSegmentCount(int segmentLength)
    {
        if (segmentLength < 5)
            return;
        setValue(SEGMENT_COUNT, segmentLength);
        fireParameterChanged(SEGMENT_COUNT);
    }
    
    public float getLineWidth()
    {
        return (Float)getValue(LINE_WIDTH);
    }
    
    public void setLineWidth(float val)
    {
        setValue(LINE_WIDTH, val);
        fireParameterChanged(LINE_WIDTH);
    }
    
}
