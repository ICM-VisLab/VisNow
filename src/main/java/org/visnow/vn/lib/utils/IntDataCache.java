/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils;

import java.util.ArrayList;

public class IntDataCache
{

    private final int dataCacheSize;
    private final int cacheSize;
    private final int veclen;
    private int nElements = 0;
    private ArrayList<int[]> elements = new ArrayList<int[]>();
    private int[] currentArr;
    private int currentIndex;

    public IntDataCache(int cacheSize, int veclen)
    {
        super();
        this.cacheSize = cacheSize;
        dataCacheSize = cacheSize * veclen;
        this.veclen = veclen;
        currentArr = new int[dataCacheSize];
        elements.add(currentArr);
        currentIndex = 0;
    }

    /**
     *
     * @param values
     *               <p>
     * @return Index of inserted element
     */
    public int put(int... values)
    {
        assert values.length == veclen;
        return put(values, 0);
    }

    public int put(int[] data, int offset)
    {
        if (currentIndex == dataCacheSize) {
            currentArr = new int[dataCacheSize];
            elements.add(currentArr);
            currentIndex = 0;
        }
        System.arraycopy(data, offset, currentArr, currentIndex, veclen);
        currentIndex += veclen;
        return nElements++;
    }

    public int[] getContigous()
    {
        if (nElements > 0) {
            int[] outData = new int[veclen * nElements];
            int outIndex = 0;
            int lastEBlock = elements.size() - 1;

            //rewrite full blocks
            for (int i = 0; i < lastEBlock; ++i) {
                System.arraycopy(elements.get(i), 0, outData, outIndex, dataCacheSize);
                outIndex += dataCacheSize;
            }

            //rewrite last block
            int lastNElements = (nElements % cacheSize) * veclen;
            if (lastNElements == 0)
                lastNElements = dataCacheSize;
            System.arraycopy(elements.get(lastEBlock), 0, outData, outIndex, lastNElements);

            return outData;
        }
        return new int[0];
    }

    public int nElements()
    {
        return nElements;
    }
}
