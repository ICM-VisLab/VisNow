/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.numeric.integration;

/**
 *
 * @author babor
 */
public class GeometricVolumes
{

    public static double hexahedronVolume(float[] coords)
    {
        if (coords == null || coords.length != 24)
            return 0;

        double[] x70 = new double[3];
        double[] x10 = new double[3];
        double[] x35 = new double[3];
        double[] x40 = new double[3];
        double[] x56 = new double[3];
        double[] x20 = new double[3];
        double[] x63 = new double[3];

        for (int i = 0; i < 3; i++) {
            x70[i] = coords[21 + i] - coords[i]; //(x7-x0)
            x10[i] = coords[3 + i] - coords[i]; //(x1-x0)
            x35[i] = coords[9 + i] - coords[15 + i]; //(x3-x5)
            x40[i] = coords[12 + i] - coords[i]; //(x4-x0)
            x56[i] = coords[15 + i] - coords[18 + i]; //(x5-x6)
            x20[i] = coords[6 + i] - coords[i]; //(x2-x0)
            x63[i] = coords[18 + i] - coords[9 + i]; //(x6-x3)
        }
        return (tripleProduct(x70, x10, x35) + tripleProduct(x70, x40, x56) + tripleProduct(x70, x20, x63)) / 6.0;
    }

    public static double hexahedronVolume(double[] coords)
    {
        if (coords == null || coords.length != 24)
            return 0;

        double[] x70 = new double[3];
        double[] x10 = new double[3];
        double[] x35 = new double[3];
        double[] x40 = new double[3];
        double[] x56 = new double[3];
        double[] x20 = new double[3];
        double[] x63 = new double[3];

        for (int i = 0; i < 3; i++) {
            x70[i] = coords[21 + i] - coords[i]; //(x7-x0)
            x10[i] = coords[3 + i] - coords[i]; //(x1-x0)
            x35[i] = coords[9 + i] - coords[15 + i]; //(x3-x5)
            x40[i] = coords[12 + i] - coords[i]; //(x4-x0)
            x56[i] = coords[15 + i] - coords[18 + i]; //(x5-x6)
            x20[i] = coords[6 + i] - coords[i]; //(x2-x0)
            x63[i] = coords[18 + i] - coords[9 + i]; //(x6-x3)
        }
        return (tripleProduct(x70, x10, x35) + tripleProduct(x70, x40, x56) + tripleProduct(x70, x20, x63)) / 6.0;
    }

    private static double tripleProduct(double[] A, double[] B, double[] C)
    {
        return A[0] * B[1] * C[2] + B[0] * C[1] * A[2] + C[0] * A[1] * B[2] - C[0] * B[1] * A[2] - B[0] * A[1] * C[2] - A[0] * C[1] * B[2];
    }

    private GeometricVolumes()
    {
    }

}
