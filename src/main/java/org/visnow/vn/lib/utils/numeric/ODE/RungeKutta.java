/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.numeric.ODE;

import java.util.Arrays;
import org.visnow.vn.lib.utils.interpolation.FieldPosition;

public class RungeKutta
{
    
    private static int compute(int nSpace, Deriv g, 
                               float[] x0, float dt, 
                               int step0, int dir, 
                               float[] trajectory)
    {
        float[] v1 = new float[nSpace];
        float[] v2 = new float[nSpace];
        float[] v3 = new float[nSpace];
        float[] v4 = new float[nSpace];
        float[] x = new float[nSpace];
        float[] xcurrent = new float[nSpace];
        float[] last_dx_dt = new float[nSpace];
        float[] dx_dt;
        float h = dir * dt;
        int iStep = 0;
        int nSteps = trajectory.length / nSpace;
        System.arraycopy(x0, 0, x, 0, nSpace);
        System.arraycopy(x0, 0, trajectory, step0 * nSpace, nSpace);
        try {
            dx_dt = g.derivn(x0);
            System.arraycopy(dx_dt, 0, last_dx_dt, 0, nSpace);

        // iteration over allowed steps: iStep is current step number
            for (iStep = step0 + dir;  iStep < nSteps && iStep >= 0; iStep += dir) {
                dx_dt = g.derivn(x);
                if (dx_dt == null)
                    break;
                for (int i = 0; i < nSpace; i++) {
                    v1[i] = h * dx_dt[i];
                    xcurrent[i] = x[i] + v1[i] / 2;
                }
                dx_dt = g.derivn(xcurrent);
                if (dx_dt == null)
                    break;
                for (int i = 0; i < nSpace; i++) {
                    v2[i] = h * dx_dt[i];
                    xcurrent[i] = x[i] + v2[i] / 2;
                }
                dx_dt = g.derivn(xcurrent);
                if (dx_dt == null)
                    break;
                for (int i = 0; i < nSpace; i++) {
                    v3[i] = h * dx_dt[i];
                    xcurrent[i] = x[i] + v3[i];
                }
                dx_dt = g.derivn(xcurrent);
                if (dx_dt == null)
                    break;
                for (int i = 0; i < nSpace; i++) {
                    v4[i] = h * dx_dt[i];
                    x[i] += v1[i] / 6 + v2[i] / 3 + v3[i] / 3 + v4[i] / 6;
                }
                System.arraycopy(x, 0, trajectory, iStep * nSpace, nSpace);
            }
            for (int i = iStep; i < nSteps && i >= 0; i+= dir) {
                System.arraycopy(x, 0, trajectory, i * nSpace, nSpace);
            }
        } catch (Exception e) {
        } 
        return iStep;
    }

/**
 * Fourth order Runge-Kutta for n (nSpace) ordinary differential equations (ODE)
 * @param g      Deriv object providing RHS for the dx/dt = v(x) equation
 * @param x0     initial conditions (start point)
 * @param dt     timestep
 * @param step0  position of initial point in trajectory array
 * @param trajectory preallocated trajectory array 
 * @return  number of effectively computed steps (when trajectory exits vector field domain earlier)
 */
    public static int[] fourthOrderRK(Deriv g, float[] x0, float dt, int step0, 
                                      float[] trajectory)
    {
        int nSpace = x0.length;
        int nsteps = trajectory.length / nSpace;
        for (int i = 0; i < nsteps; i++) 
            System.arraycopy(x0, 0, trajectory, i * nSpace, nSpace);
        int[] bdrySteps = new int[] {0, nsteps - 1};
        try {
            if (step0 > 0)
                bdrySteps[0] = compute(nSpace, g, x0, dt, step0, -1, trajectory);
            if (step0 < nsteps - 1)
                bdrySteps[1] = compute(nSpace, g, x0, dt, step0, 1, trajectory);
            
        } catch (Exception e) {
        }
        return bdrySteps;
    }

    
    private static int compute(int nSpace, Deriv g, 
                               float[] x0, float dt, 
                               int step0, int dir, 
                               float[] trajectory, FieldPosition[] positions)
    {
        float[] v1 = new float[nSpace];
        float[] v2 = new float[nSpace];
        float[] v3 = new float[nSpace];
        float[] v4 = new float[nSpace];
        float[] x = new float[nSpace];
        float[] xcurrent = new float[nSpace];
        float[] dx_dt;
        FieldPosition lastPos;
        float h = dir * dt;
        int iStep = 0;
        int nSteps = trajectory.length / nSpace;
        System.arraycopy(x0, 0, x, 0, nSpace);
        System.arraycopy(x0, 0, trajectory, step0 * nSpace, nSpace);
        try {
            lastPos = positions[step0] = g.getPosition(step0);

        // iteration over allowed steps: iStep is current step number
            for (iStep = step0 + dir;  iStep < nSteps && iStep >= 0; iStep += dir) {
                dx_dt = g.derivn(x);
                if (dx_dt == null)
                    break;
                double r = 0;
                for (int i = 0; i < nSpace; i++)
                    r += dx_dt[i] * dx_dt[i];
                if (r == 0)
                    break;
                lastPos = positions[iStep] = g.getPosition(iStep);
                for (int i = 0; i < nSpace; i++) {
                    v1[i] = h * dx_dt[i];
                    xcurrent[i] = x[i] + v1[i] / 2;
                }
                dx_dt = g.derivn(xcurrent);
                if (dx_dt == null)
                    break;
                for (int i = 0; i < nSpace; i++) {
                    v2[i] = h * dx_dt[i];
                    xcurrent[i] = x[i] + v2[i] / 2;
                }
                dx_dt = g.derivn(xcurrent);
                if (dx_dt == null)
                    break;
                for (int i = 0; i < nSpace; i++) {
                    v3[i] = h * dx_dt[i];
                    xcurrent[i] = x[i] + v3[i];
                }
                dx_dt = g.derivn(xcurrent);
                if (dx_dt == null)
                    break;
                for (int i = 0; i < nSpace; i++) {
                    v4[i] = h * dx_dt[i];
                    x[i] += v1[i] / 6 + v2[i] / 3 + v3[i] / 3 + v4[i] / 6;
                }
                System.arraycopy(x, 0, trajectory, iStep * nSpace, nSpace);
            }
            for (int i = iStep; i < nSteps && i >= 0; i+= dir) {
                positions[i] = lastPos;
                System.arraycopy(x, 0, trajectory, i * nSpace, nSpace);
            }
        } catch (Exception e) {
        } 
        return iStep;
    }

/**
 * Fourth order Runge-Kutta for n (nSpace) ordinary differential equations (ODE)
 * @param g      Deriv object providing RHS for the dx/dt = v(x) equation
 * @param x0     initial conditions (start point)
 * @param dt     timestep
 * @param step0  position of initial point in trajectory array
 * @param trajectory preallocated trajectory array 
 * @param  positions   preallocated array of FieldPosition (field nodes + weights of trajectory points)
 * @return  number of effectively computed steps (when trajectory exits vector field domain earlier)
 */
    public static int[] fourthOrderRK(Deriv g, float[] x0, float dt, int step0, 
                               float[] trajectory, FieldPosition[] positions)
    {
        int nSpace = x0.length;
        int nsteps = trajectory.length / nSpace;
        int[] bdrySteps = new int[] {0, nsteps - 1};
        for (int i = 0; i < nsteps; i++) 
            System.arraycopy(x0, 0, trajectory, i * nSpace, nSpace);
        try {
            float[] v =g.derivn(x0);
            if (v == null)
                return new int[] {-1, - 1};
            if (step0 > 0)
                bdrySteps[0] = compute(nSpace, g, x0, dt, step0, -1, trajectory, positions);
            if (step0 < nsteps - 1)
                bdrySteps[1] = compute(nSpace, g, x0, dt, step0, 1, trajectory, positions);
        } catch (Exception e) {
            return new int[] {-1, - 1};
        }
        return bdrySteps;
    }

    private RungeKutta()
    {
    }
}
