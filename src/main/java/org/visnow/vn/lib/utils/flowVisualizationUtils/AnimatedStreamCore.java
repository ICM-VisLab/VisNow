/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.flowVisualizationUtils;

import java.awt.Color;
import java.util.Arrays;
import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.LineStripArray;
import org.jogamp.java3d.TransparencyAttributes;
import org.jogamp.vecmath.Color3f;
import org.visnow.jscic.Field;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.datamaps.ColorMapManager;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenLineAttributes;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;
import org.visnow.vn.geometries.objects.generics.OpenTransparencyAttributes;
import org.visnow.vn.geometries.parameters.ComponentColorMap;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.lib.utils.flowVisualizationUtils.AnimatedStreamParams.ANIMATION;

/**
 *
 * @author Krzysztof S. Nowinski
 */


public class AnimatedStreamCore
{
    private final OpenBranchGroup outGroup = new OpenBranchGroup();
    private OpenBranchGroup streamlineGroup;
    
    private final AnimatedStreamParams params ;
    
    private Field inField = null;
    private ComponentColorMap colorMap;
    
    private int nFrames = 1;
    private int segmentLength = 2;
    private int frame = 0;
    private int nStreamlines = 0;
    private int nSegments = 1;
    private int segSkip = 1;
    private int dir = 0;
    private boolean animating = false;
    private boolean upGeometry = false;
    private boolean renderDone = true;
    private int outNNodes;
    
    private float[] inCoordVals;
    private float[] outCoords;
    
    private float[] inColorVals;
    int vlen = 1;
    private byte[] cMap;
    private final float[] bgr = new float[3];
    private byte[] outColors = null;
    
    private final DataMappingParams dataMappingParams;
    
    private LineStripArray lineStrips = null;
    private OpenShape3D lineShape = new OpenShape3D();
    private final OpenAppearance appearance = new OpenAppearance();
    private final OpenLineAttributes lineAttributes = new OpenLineAttributes(1.f, OpenLineAttributes.PATTERN_SOLID, true);
    private final OpenTransparencyAttributes trAttributes = new OpenTransparencyAttributes(TransparencyAttributes.NONE, 1.f);
    
    
    public AnimatedStreamCore(Field inField, AnimatedStreamParams params, DataMappingParams dataMappingParams)
    {
        this.inField = inField;
        String[] items = inField.getUserData()[0].split(" +");
        nStreamlines = Integer.parseInt(items[2]);
        nFrames = (int)(inField.getNNodes() / nStreamlines);
        this.params = params;
        this.dataMappingParams = dataMappingParams;
        params.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                animatedStreamParameterChanged(name);
            }
        });
        updateGeometry();
    }
    
    public void animatedStreamParameterChanged(String name)
    {
        switch (name) {
        case AnimatedStreamParams.SEGMENT_COUNT:
        case AnimatedStreamParams.SEGMENT_GAP:
            if (animating)
                upGeometry = true;
            else
                updateGeometry();
            break;
        case AnimatedStreamParams.LINE_WIDTH:
            lineAttributes.setLineWidth(params.getLineWidth());
            break;
        case AnimatedStreamParams.DELAY:
            break;
        case AnimatedStreamParams.ANIMATE:
            switch (params.getAnimate()) {
            case STOP:
                dir = 0;
                break;
            case FORWARD:
                dir = 1;
                if (!animating)
                    new Thread(new Animation()).start();
                break;
            case BACK:
                dir = -1;
                if (!animating)
                    new Thread(new Animation()).start();
                break;
            }
            break;
        }
    }
    
    private class Animation implements Runnable
    {
        @Override
        public synchronized void run()
        {
            if (animating)
                return;
            animating = true;
            while (dir != 0) {
                renderDone = false;
                frame = (frame + dir) % segSkip;
                if (upGeometry)
                    updateGeometry();
                else {
                    updateValues();
                }
                try {
                    while (!renderDone) {
                        wait(10);
                    }
                    wait(params.getDelay() + 1);
                } catch (InterruptedException c) {
                    Thread.currentThread().interrupt();
                }
            }
            animating = false;
        }
    }
    
    public void stopAnimation()
    {
        params.setAnimate(ANIMATION.STOP);
    }
    
    public void setRenderDone(boolean renderDone)
    {
        this.renderDone = renderDone;
    }
    
    public void setBgrColor(Color c)
    {
        c.getRGBColorComponents(bgr);
    }
    
    private void updateValues()
    {
        updateCooords();
        updateColors();
        lineAttributes.setLineWidth(params.getLineWidth());         
    }
    
    public void updateCooords()
    {
        if (outCoords == null || outCoords.length != 3 * outNNodes)
            outCoords = new float[3 * outNNodes];
        for (int iStreamline = 0, l = 0; iStreamline < nStreamlines; iStreamline++) 
            for (int iSegment = 0; iSegment < nSegments; iSegment++) {
                int m = iStreamline + nStreamlines * (frame + iSegment * segSkip - segmentLength);
                for (int i = 0; i <= segmentLength; i++, m += nStreamlines, l += 3) {
                    int mm = m;
                    if (mm < 0)
                        mm = iStreamline;
                    if (m < 0)
                        System.arraycopy(inCoordVals, 3 * mm, outCoords, l, 3);
                    else
                        System.arraycopy(inCoordVals, 3 * mm, outCoords, l, 3);
                }
            }
        lineStrips.setCoordRefFloat(outCoords);
    }
    
    public void updateColors()
    {
//        VisNowCallTrace.trace();
        colorMap = dataMappingParams.getColorMap0();
        cMap = colorMap.getRGBByteColorTable();
        if (outColors == null || outColors.length != 4 * outNNodes) 
            outColors = new byte[4 * outNNodes];
        DataArray mapArray = inField.getComponent(colorMap.getDataComponentName());
        if (mapArray != null) {
            int colorCmpVlen = mapArray.getVectorLength();
            float minV = colorMap.getDataMin();
            float dv =  cMap.length / (3 * (colorMap.getDataMax() - minV));
            inColorVals = mapArray.getRawFloatArray().getData();
            for (int iStreamline = 0, l = 0; iStreamline < nStreamlines; iStreamline++) 
                for (int iSegment = 0; iSegment < nSegments; iSegment++) {
                    int m = iStreamline + nStreamlines * (frame + iSegment * segSkip - segmentLength);
                    for (int i = 0; i <= segmentLength; i++, m += nStreamlines, l += 4) {
                        int mm = m;
                        if (mm < 0)
                            mm = iStreamline;
                        float v = 0;
                        if (colorCmpVlen == 1)
                            v = inColorVals[mm];
                        else {
                            for (int j = 0; j < colorCmpVlen; j++) 
                                v += inColorVals[colorCmpVlen * mm + j] * inColorVals[colorCmpVlen * mm + j];
                            v = (float)Math.sqrt(v);
                        }
                        int cmapInd = (int)((v - minV) * dv);
                        if (cmapInd < 0)    cmapInd = 0; 
                        if (cmapInd >= ColorMapManager.SAMPLING_TABLE)    
                            cmapInd =  ColorMapManager.SAMPLING_TABLE - 1; 
                        System.arraycopy(cMap, 3 * cmapInd, outColors, l, 3);
                    }
                }
        } else {
            Color3f defaultColor = colorMap.getDefaultColor();
            float[] c = new float[3];
            defaultColor.get(c);
            byte[] defColor = new byte[]{(byte)(0xff & (int)(c[0] * 255)), 
                                         (byte)(0xff & (int)(c[1] * 255)), 
                                         (byte)(0xff & (int)(c[2] * 255))};
            for (int l = 0; l < defColor.length; l += 4)
                        System.arraycopy(defColor, 0, outColors, l, 3);
        }
        for (int iStreamline = 0, l = 3; iStreamline < nStreamlines; iStreamline++) 
            for (int iSegment = 0; iSegment < nSegments; iSegment++) 
                for (int i = 0; i <= segmentLength; i++, l += 4) 
                    outColors[l] = (byte)(0xff & (int)(255 * i / (float)segmentLength));
        lineStrips.setColorRefByte(outColors);
    }
    
    public final void updateGeometry()
    {
        outGroup.removeAllChildren();
        streamlineGroup = null;
        streamlineGroup = new OpenBranchGroup();
        nSegments = params.getSegmentCount();
        segSkip = nFrames / nSegments;
        segmentLength = Math.max(2, (int)(segSkip * (1 - params.getGap() / 100.)));
        outNNodes = nStreamlines * (segmentLength + 1) * nSegments;
        frame = 0;   
        inCoordVals = inField.getCoords(0).getData();
        outCoords = new float[3 * outNNodes];
        outColors =new byte[4 * outNNodes];
        int[] lineLengths = new int[nStreamlines * nSegments];
        Arrays.fill(lineLengths, segmentLength + 1);
        lineShape  = new OpenShape3D();
        lineStrips = new LineStripArray(outNNodes,
                                        GeometryArray.COORDINATES | GeometryArray.COLOR_4 | GeometryArray.BY_REFERENCE,
                                        lineLengths);    
        lineStrips.setCapability(GeometryArray.ALLOW_REF_DATA_READ);
        lineStrips.setCapability(GeometryArray.ALLOW_REF_DATA_WRITE);
        dataMappingParams.setInDataSchemas(inField.getSchema(), null);
        lineShape.addGeometry(lineStrips);
        lineAttributes.setLineWidth(params.getLineWidth());
        trAttributes.setTransparencyMode(TransparencyAttributes.NICEST);    
        trAttributes.setTransparency(0);
        appearance.setLineAttributes(lineAttributes);
        appearance.setTransparencyAttributes(trAttributes);
        lineShape.setAppearance(appearance);
        streamlineGroup.addChild(lineShape);
        outGroup.addChild(streamlineGroup);
        updateValues();
        upGeometry = false;
    }

    public OpenBranchGroup getGeometry()
    {
        return outGroup;
    }
}
