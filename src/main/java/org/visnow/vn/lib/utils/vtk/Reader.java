/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.vtk;

import java.io.File;
import org.visnow.jscic.Field;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.CellArray;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jlargearrays.FloatLargeArray;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
abstract public class Reader
{

    class Polydata
    {

        public int type;
        public int nItems;
        public int[] verts;

        public Polydata(int type, int nItems, int[] verts)
        {
            this.type = type;
            this.nItems = nItems;
            this.verts = verts;
        }
    }

    protected static final int STRUCTURED_POINTS = 0;
    protected static final int STRUCTURED_GRID = 1;
    protected static final int RECTILINEAR_GRID = 2;
    protected static final int UNSTRUCTURED_GRID = 3;
    protected static final int POLYDATA = 4;
    protected static final int VERTICES = 0;
    protected static final int LINES = 1;
    protected static final int POLYGONS = 2;
    protected static final int TRIANGLE_STRIPS = 3;
    protected static boolean initialized = false;
    protected static Map<String, Integer> VTKFieldTypeMap = new HashMap<String, Integer>();
    protected static Map<String, DataArrayType> VTKDataTypeMap = new HashMap<String, DataArrayType>();
    protected static Map<String, Integer> VTKPolyTypeMap = new HashMap<String, Integer>();
    protected static final CellType[] VTKcellTypes = {
        CellType.EMPTY,
        CellType.POINT, CellType.POINT,
        CellType.SEGMENT, CellType.SEGMENT,
        CellType.TRIANGLE, CellType.TRIANGLE, CellType.TRIANGLE, CellType.QUAD, CellType.QUAD,
        CellType.TETRA, CellType.HEXAHEDRON, CellType.HEXAHEDRON
    };
    protected static final int[][] VTKnodeOrders = {
        {0},
        {0},
        {0, 1},
        {0, 1},
        {0, 1, 2},
        {0, 1, 2},
        {0, 1, 2},
        {0, 1, 2, 3},
        {0, 1, 3, 2},
        {0, 1, 2, 3},
        {4, 0, 3, 2, 1},
        {0, 1, 3, 2, 4, 5, 7, 6},
        {0, 1, 2, 3, 4, 5, 6, 7}
    };
    protected static final int[] VTKcellSets = {
        -999, 1, 0, 1, -1, 1, -2, -1, 1, 1, 1, 1, 1
    };
    protected static Cell[] stdCells = new Cell[Cell.getNProperCellTypes()];
    protected int VTKFieldType = -1;
    protected int nNodes = 0;
    protected int nCellsItems = 0;
    protected int nCells = 0;
    protected int nCellData = 0;
    protected int nNodeData = 0;
    protected String outFieldName = "";

    public Reader()
    {
        if (!initialized) {
            initialized = true;
            for (int i = 0; i < stdCells.length; i++) {
                stdCells[i] = Cell.createCell(CellType.getType(i), 3, new int[CellType.getType(i).getNVertices()], (byte)1);
            }
            VTKDataTypeMap.put("bit", DataArrayType.FIELD_DATA_LOGIC);
            VTKDataTypeMap.put("char", DataArrayType.FIELD_DATA_BYTE);
            VTKDataTypeMap.put("unsigned_char", DataArrayType.FIELD_DATA_BYTE);
            VTKDataTypeMap.put("short", DataArrayType.FIELD_DATA_SHORT);
            VTKDataTypeMap.put("int", DataArrayType.FIELD_DATA_INT);
            VTKDataTypeMap.put("integer", DataArrayType.FIELD_DATA_INT);
            VTKDataTypeMap.put("float", DataArrayType.FIELD_DATA_FLOAT);
            VTKDataTypeMap.put("double", DataArrayType.FIELD_DATA_DOUBLE);

            VTKFieldTypeMap.put("STRUCTURED_POINTS", STRUCTURED_POINTS);
            VTKFieldTypeMap.put("STRUCTURED_GRID", STRUCTURED_GRID);
            VTKFieldTypeMap.put("RECTILINEAR_GRID", RECTILINEAR_GRID);
            VTKFieldTypeMap.put("UNSTRUCTURED_GRID", UNSTRUCTURED_GRID);
            VTKFieldTypeMap.put("POLYDATA", POLYDATA);

            VTKPolyTypeMap.put("VERTICES", VERTICES);
            VTKPolyTypeMap.put("LINES", LINES);
            VTKPolyTypeMap.put("POLYGONS", POLYGONS);
            VTKPolyTypeMap.put("TRIANGLE_STRIPS", TRIANGLE_STRIPS);
        }
    }

    protected void addRectilinearCoords(RegularField outField)
    {
        int[] dims = outField.getDims();
        DataArrayType type;
        String line = nextLine(new String[]{"X_COORDINATES"});
        String[] tokens = line.split("\\s+");
        int n = Integer.parseInt(tokens[1]);
        if (n != dims[0]) {
            return;
        }
        if (VTKDataTypeMap.containsKey(tokens[2].toLowerCase())) {
            type = VTKDataTypeMap.get(tokens[2].toLowerCase());
        } else {
            System.out.println("invalid vtk data type in " + line);
            return;
        }
        float[] cx = new float[n];
        readFloatArrayFrom(cx, type);
        outField.setRectilinearCoords(0, cx);
        line = nextLine(new String[]{"Y_COORDINATES"});
        tokens = line.split("\\s+");
        n = Integer.parseInt(tokens[1]);
        if (n != dims[1]) {
            return;
        }
        float[] cy = new float[n];
        readFloatArrayFrom(cy, type);
        outField.setRectilinearCoords(1, cy);

        if (dims.length > 2) {
            line = nextLine(new String[]{"Z_COORDINATES"});
            tokens = line.split("\\s+");
            n = Integer.parseInt(tokens[1]);
            if (n != dims[2]) {
                return;
            }
            float[] cz = new float[n];
            readFloatArrayFrom(cz, type);
            outField.setRectilinearCoords(2, cz);
        }
    }

    protected float[] readCoords(int n)
    {
        String line = nextLine(new String[]{"POINTS"});
        String[] tokens = line.split("\\s+");
        nNodes = Integer.parseInt(tokens[1]);
        DataArrayType type;
        float[] coords = new float[3 * nNodes];
        if (n > 0 && nNodes != n) {
            return null;
        }
        if (VTKDataTypeMap.containsKey(tokens[2].toLowerCase())) {
            type = VTKDataTypeMap.get(tokens[2].toLowerCase());
        } else {
            System.out.println("invalid vtk data type in " + line);
            return null;
        }
        readFloatArrayFrom(coords, type);
        return coords;
    }

    protected void buildCells(CellSet cs, int nVerts, int nSegments, int nTriangles, int nQuads, List<Polydata> polys)
    {
        int[] ptNodes = new int[nVerts];
        int[] ptIndices = new int[nVerts];
        int[] segNodes = new int[2 * nSegments];
        int[] segIndices = new int[nSegments];
        int[] triNodes = new int[3 * nTriangles];
        int[] triIndices = new int[nTriangles];
        int[] quadNodes = new int[4 * nQuads];
        int[] quadIndices = new int[nQuads];

        int index = 0;
        int kvert = 0;
        int kline = 0;
        int ktri = 0;
        int kquad = 0;
        for (int i = 0; i < polys.size(); i++, index++) {
            int type = polys.get(i).type;
            int nItems = polys.get(i).nItems;
            int[] data = polys.get(i).verts;

            switch (type) {
                case VERTICES:
                    for (int j = 0; j < data.length;) {
                        int n = data[j];
                        j += 1;
                        for (int l = 0; l < n; l++, j++, kvert++) {
                            ptNodes[kvert] = data[j];
                            ptIndices[kvert] = index;
                        }
                    }
                    break;
                case LINES:
                    for (int j = 0; j < data.length; j++) {
                        int n = data[j];
                        j += 1;
                        for (int l = 0; l < n - 1; l++, j++, kline += 2) {
                            segNodes[kline] = data[j];
                            segNodes[kline + 1] = data[j + 1];
                            segIndices[kline / 2] = index;
                        }
                    }
                    break;
                case POLYGONS:
                    for (int j = 0; j < data.length;) {
                        int n = data[j];
                        j += 1;
                        switch (n) {
                            case 3:
                                for (int l = 0; l < 3; l++, j++, ktri++) {
                                    triNodes[ktri] = data[j];
                                    triIndices[ktri / 3] = index;
                                }
                                break;
                            case 4:
                                for (int l = 0; l < 4; l++, j++, kquad++) {
                                    quadNodes[kquad] = data[j];
                                    quadIndices[kquad / 4] = index;
                                }
                                break;
                            default:
                                int l0 = 0;
                                int l1 = n - 1;
                                int l2 = 1;
                                for (int l = 0; l < n - 2; l++, ktri += 3) {
                                    triNodes[ktri] = data[j + l0];
                                    triNodes[ktri + 1] = data[j + l1];
                                    triNodes[ktri + 2] = data[j + l2];
                                    triIndices[ktri / 3] = index;
                                    if (l % 2 == 0) {
                                        l0 = l2;
                                        l2 = l1 - 1;
                                    } else {
                                        l1 = l2;
                                        l2 = l0 + 1;
                                    }
                                }
                                j += n;
                        }
                        index += 1;
                    }
                    break;
                case TRIANGLE_STRIPS:
                    for (int j = 0; j < data.length - 3; j++) {
                        int n = data[j];
                        j += 1;
                        for (int l = 0; l < n - 2; l++, j++, ktri += 3) {
                            triNodes[ktri] = data[j];
                            triNodes[ktri + 1] = data[j + 1];
                            triNodes[ktri + 2] = data[j + 2];
                            triIndices[ktri / 3] = index;
                        }
                        j += 1;
                    }
            }
        }

        if (nVerts > 0) {
            byte[] ptOrients = new byte[nVerts];
            for (int i = 0; i < ptOrients.length; i++) {
                ptOrients[i] = 1;
            }
            CellArray pointArray = new CellArray(CellType.POINT, ptNodes, ptOrients, ptIndices);
            cs.setCellArray(pointArray);
        }

        if (nSegments > 0) {
            byte[] segOrients = new byte[nSegments];
            for (int i = 0; i < segOrients.length; i++) {
                segOrients[i] = 1;
            }
            CellArray segArray = new CellArray(CellType.SEGMENT, segNodes, segOrients, segIndices);
            cs.setCellArray(segArray);
        }

        if (nTriangles > 0) {
            byte[] triOrients = new byte[nTriangles];
            for (int i = 0; i < triOrients.length; i++) {
                triOrients[i] = 1;
            }
            CellArray triArray = new CellArray(CellType.TRIANGLE, triNodes, triOrients, triIndices);
            cs.setCellArray(triArray);
        }

        if (nQuads > 0) {
            byte[] quadOrients = new byte[nQuads];
            for (int i = 0; i < quadOrients.length; i++) {
                quadOrients[i] = 1;
            }
            CellArray quadArray = new CellArray(CellType.QUAD, quadNodes, quadOrients, quadIndices);
            cs.setCellArray(quadArray);
        }
    }

    protected void addVNFFieldData(DataContainer container, String l)
    {
        String name = "";
        int vlen = 1;
        int nData = 0;
        DataArrayType type = DataArrayType.FIELD_DATA_UNKNOWN;
        String line = l;
        String[] tokens = line.split("\\s+");
        int nDataInField = Integer.parseInt(tokens[2]);
        for (int iData = 0; iData < nDataInField; iData++) {
            line = nextLine();
            tokens = line.split("\\s+");
            if (tokens.length < 4) {
                return;
            }
            name = tokens[0];
            vlen = Integer.parseInt(tokens[1]);
            nData = Integer.parseInt(tokens[2]);
            if (VTKDataTypeMap.containsKey(tokens[3].toLowerCase())) {
                type = VTKDataTypeMap.get(tokens[3].toLowerCase());
            } else {
                System.out.println("invalid vtk data type in " + line);
                return;
            }
            container.addComponent(readData(type, vlen, nData, name));
        }
    }

    protected RegularField readRegularFile()
    {
        RegularField outField = null;
        String line = nextLine();
        String[] tokens = line.split("\\s+");
        if (tokens.length < 4 || !tokens[0].equalsIgnoreCase("DIMENSIONS")) {
            return null;
        }
        try {
            int[] dims;
            int length = 1;
            if (Integer.parseInt(tokens[3]) <= 1) {
                dims = new int[2];
                for (int i = 0; i < dims.length; i++) {
                    dims[i] = Integer.parseInt(tokens[i + 1]);
                    length *= dims[i];
                }
                outField = new RegularField(dims);
            } else {
                dims = new int[3];
                for (int i = 0; i < dims.length; i++) {
                    dims[i] = Integer.parseInt(tokens[i + 1]);
                    length *= dims[i];
                }
                outField = new RegularField(dims);
            }
            outField.setName(outFieldName);
            switch (VTKFieldType) {
                case STRUCTURED_POINTS:
                    float[][] affine = new float[4][3];
                    for (int k = 0; k < 2; k++) {
                        line = nextLine(new String[]{"ORIGIN", "SPACING", "ASPECT_RATIO"});
                        tokens = line.split("\\s+");
                        if (tokens[0].equalsIgnoreCase("ORIGIN")) {
                            for (int i = 0; i < affine[3].length; i++) {
                                affine[3][i] = Float.parseFloat(tokens[i + 1]);
                            }
                        } else {
                            for (int i = 0; i < affine[3].length; i++) {
                                affine[i][i] = Float.parseFloat(tokens[i + 1]);
                            }
                        }
                        outField.setAffine(affine);
                    }
                    break;
                case STRUCTURED_GRID:
                    float[] coords = readCoords(length);
                    outField.setCurrentCoords(new FloatLargeArray(coords));
                    break;
                case RECTILINEAR_GRID:
                    addRectilinearCoords(outField);
                    break;
            }

            boolean nodeData = false;
            line = nextLine(new String[]{"POINT_DATA", "CELL_DATA"});
            for (int dataLoc = 0; dataLoc < 2; dataLoc++) {
                tokens = line.split("\\s+");
                if (tokens.length < 2) {
                    System.out.println("bad DATA line " + line);
                    return outField;
                }
                nodeData = tokens[0].equalsIgnoreCase("POINT_DATA");
                int nData = Integer.parseInt(tokens[1]);
                while ((line = nextLine(new String[]{"POINT_DATA", "CELL_DATA",
                                                     "FIELD",
                                                     "SCALARS", "COLOR_SCALARS", "LOOKUP_TABLE",
                                                     "VECTORS", "NORMALS", "TEXTURE_COORDINATES", "TENSORS"})) != null) {
                    if (line.toUpperCase().startsWith("POINT_DATA") || line.toUpperCase().startsWith("CELL_DATA")) {
                        break;
                    }
                    if (line.toUpperCase().startsWith("FIELD")) {
                        addVNFFieldData(outField, line);
                    } else {
                        DataArray[] da = readDataArray(nData, line);
                        if (da != null && nodeData) {
                            for (int i = 0; i < da.length; i++) {
                                outField.addComponent(da[i]);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
        return outField;
    }

    protected IrregularField readIrregularFileFromVTKGrid()
    {
        float[] coords = (readCoords(-1));
        IrregularField outField = new IrregularField(nNodes);
        outField.setCurrentCoords(new FloatLargeArray(coords));
        String line = nextLine(new String[]{"CELL"});
        String[] tokens = line.split(" +");
        int[] tmpCells = null;
        int[][] nodes = null;
        int[] nCellsOfType = new int[Cell.getNProperCellTypes()];
        for (int i = 0; i < nCellsOfType.length; i++) {
            nCellsOfType[i] = 0;
        }

        if (tokens[0].equalsIgnoreCase("CELLS")) {
            try {
                nCellsItems = Integer.parseInt(tokens[2]);
            } catch (Exception e) {
                return null;
            }
            tmpCells = new int[nCellsItems];
            for (int i = 0; i < tmpCells.length; i++) {
                tmpCells[i] = getInt();
            }
            line = nextLine(new String[]{"CELL_TYPES"});
            tokens = line.split(" +");
            if (!tokens[0].equalsIgnoreCase("CELL_TYPES")) {
                return null;
            }
            try {
                nCells = Integer.parseInt(tokens[1]);
            } catch (Exception e) {
                return null;
            }
            int[] cellTypes = new int[nCells];
            int[] iCellOfType = new int[Cell.getNProperCellTypes()];
            for (int i = 0; i < nCellsOfType.length; i++) {
                nCellsOfType[i] = iCellOfType[i] = 0;
            }
            for (int i = 0, tmpCellNodesIndex = 0; i < cellTypes.length; i++, tmpCellNodesIndex += 1) {
                int vtkCellType = getInt();
                CellType vnCellType = VTKcellTypes[vtkCellType];
                int vnCellCount = VTKcellSets[vtkCellType];
                if (vnCellType == CellType.EMPTY) {
                    System.out.println("" + vtkCellType);
                    return null;
                }
                cellTypes[i] = vtkCellType;
                if (vnCellCount == 1) {
                    nCellsOfType[vnCellType.getValue()] += 1;
                } else {
                    int n = tmpCells[tmpCellNodesIndex];
                    nCellsOfType[vnCellType.getValue()] += n + vnCellCount;
                    tmpCellNodesIndex += n;
                }
            }
            nodes = new int[Cell.getNProperCellTypes()][];
            for (int i = 0; i < Cell.getNProperCellTypes(); i++) {
                nodes[i] = new int[nCellsOfType[i] * CellType.getType(i).getNVertices()];
            }
            for (int i = 0, j = 0; i < nCells; i++) {
                int vtkCellType = cellTypes[i];
                CellType vnCellType = VTKcellTypes[vtkCellType];
                int k = vnCellType.getNVertices();
                int k1 = tmpCells[j];
                j += 1;
                if (VTKcellSets[vtkCellType] == 1) {
                    for (int l = 0; l < k; l++, j++, iCellOfType[vnCellType.getValue()] += 1) {
                        nodes[vnCellType.getValue()][iCellOfType[vnCellType.getValue()]] = tmpCells[j];
                    }
                } else {
                    for (int l = 0; l < k1 + VTKcellSets[vtkCellType]; l++) {
                        for (int m = 0; m < k; m++, iCellOfType[vnCellType.getValue()] += 1) {
                            nodes[vnCellType.getValue()][iCellOfType[vnCellType.getValue()]] = tmpCells[j + l + k];
                        }
                    }
                    j += k1;
                }
            }
        }
        if (tokens[0].equalsIgnoreCase("CELL_TYPES")) {
            try {
                nCells = Integer.parseInt(tokens[1]);
                nodes = new int[Cell.getNProperCellTypes()][];
                nodes[CellType.POINT.getValue()] = new int[nCells];
                for (int i = 0; i < nCells; i++) {
                    int k = getInt();
                    if (k == 1) {
                        nodes[CellType.POINT.getValue()][i] = i;
                    }
                    nCellsOfType[CellType.POINT.getValue()] = nCells;
                }
            } catch (Exception e) {
                return null;
            }
        }
        CellSet cs = new CellSet();
        for (int i = 0; i < nodes.length; i++) {
            if (nodes[i] == null || nodes[i].length == 0) {
                continue;
            }
            byte[] orient = new byte[nCellsOfType[i]];
            int[] indices = new int[nCellsOfType[i]];
            for (int j = 0; j < orient.length; j++) {
                indices[i] = i;
                orient[j] = 1;
            }
            cs.addCells(new CellArray(CellType.getType(i), nodes[i], orient, indices));
        }
        cs.generateDisplayData(new FloatLargeArray(coords));
        outField.addCellSet(cs);
        line = nextLine(new String[]{"CELL_DATA", "POINT_DATA"});
        for (int iDataType = 0; iDataType < 2; iDataType++) {
            if (line == null) {
                break;
            }
            //System.out.println(line);
            tokens = line.split(" +");
            if (tokens.length < 2) {
                return outField;
            }
            DataContainer dataCont = outField;
            if (tokens[0].equalsIgnoreCase("CELL_DATA")) {
                dataCont = cs;
            }
            int nData = Integer.parseInt(tokens[1]);
            while ((line = nextLine(new String[]{"CELL_DATA", "POINT_DATA",
                                                 "FIELD",
                                                 "SCALARS", "COLOR_SCALARS", "LOOKUP_TABLE",
                                                 "VECTORS", "NORMALS", "TEXTURE_COORDINATES", "TENSORS"})) != null) {
                if (line.toUpperCase().startsWith("CELL_DATA") || line.toUpperCase().startsWith("POINT_DATA")) {
                    break;
                }
                if (line.toUpperCase().startsWith("FIELD")) {
                    addVNFFieldData(dataCont, line);
                } else {
                    DataArray[] da = readDataArray(nData, line);
                    if (da != null) {
                        for (int i = 0; i < da.length; i++) {
                            dataCont.addComponent(da[i]);
                        }
                    }
                }
            }
        }
        return outField;
    }

    protected IrregularField readIrregularFileFromVTKPolys()
    {
        String line;
        String[] tokens;
        float[] coords = (readCoords(-1));
        IrregularField outField = new IrregularField(nNodes);
        outField.setCurrentCoords(new FloatLargeArray(coords));
        CellSet cs = new CellSet();

        int nVerts = 0;
        int nSegments = 0;
        int nTriangles = 0;
        int nQuads = 0;
        List<Polydata> polys = new ArrayList<Polydata>();
        while ((line = nextLine(new String[]{"VERTICES", "LINES", "POLYGONS", "TRIANGLE_STRIPS",
                                             "CELL_DATA", "POINT_DATA"})) != null) {
            tokens = line.split(" +");
            if (tokens[0].equalsIgnoreCase("CELL_DATA") || tokens[0].equalsIgnoreCase("POINT_DATA")) {
                break;
            }
            if (tokens.length < 3) {
                System.out.println("error in line " + line);
                return outField;
            }
            int type = VTKPolyTypeMap.get(tokens[0].toUpperCase());
            int nItems = Integer.parseInt(tokens[1]);
            int nData = Integer.parseInt(tokens[2]);
            int[] data = new int[nData];
            readArray(data);
            polys.add(new Polydata(type, nItems, data));
            switch (type) {
                case VERTICES:
                    nVerts += nData - nItems;
                    break;
                case LINES:
                    nSegments += nData - 2 * nItems;
                    break;
                case POLYGONS:
                    for (int i = 0, j = 0; i < nItems; i++, j++) {
                        if (data[j] == 3) {
                            nTriangles += 1;
                        } else if (data[j] == 4) {
                            nQuads += 1;
                        } else {
                            nTriangles += (data[j] - 2);
                        }
                        j += data[j];
                    }
                    break;
                case TRIANGLE_STRIPS:
                    nTriangles += nData - 3 * nItems;
            }
        }
        buildCells(cs, nVerts, nSegments, nTriangles, nQuads, polys);
        cs.generateDisplayData(new FloatLargeArray(coords));
        outField.addCellSet(cs);
        for (int iDataType = 0; iDataType < 2; iDataType++) {
            if (line == null) {
                break;
            }
            //System.out.println(line);
            tokens = line.split(" +");
            if (tokens.length < 2) {
                return outField;
            }
            DataContainer dataCont = outField;
            if (tokens[0].equalsIgnoreCase("CELL_DATA")) {
                dataCont = cs;
            }
            int nData = Integer.parseInt(tokens[1]);
            while ((line = nextLine(new String[]{"CELL_DATA", "POINT_DATA",
                                                 "FIELD",
                                                 "SCALARS", "COLOR_SCALARS", "LOOKUP_TABLE",
                                                 "VECTORS", "NORMALS", "TEXTURE_COORDINATES", "TENSORS"})) != null) {
                if (line.toUpperCase().startsWith("CELL_DATA") || line.toUpperCase().startsWith("POINT_DATA")) {
                    break;
                }
                if (line.toUpperCase().startsWith("FIELD")) {
                    addVNFFieldData(dataCont, line);
                } else {
                    DataArray[] da = readDataArray(nData, line);
                    if (da != null) {
                        for (int i = 0; i < da.length; i++) {
                            dataCont.addComponent(da[i]);
                        }
                    }
                }
            }
        }
        return outField;
    }

    abstract int getInt();

    abstract void readArray(float[] a);

    abstract void readArray(int[] a);

    abstract void readFloatArrayFrom(float[] a, DataArrayType type);

    abstract DataArray[] readDataArray(int nData, String l);

    abstract String nextLine();

    abstract String nextLine(String[] begin);

    abstract DataArray readData(DataArrayType type, int vlen, int nData, String name);

    abstract public Field readVTK(File file, ByteOrder order);
}
