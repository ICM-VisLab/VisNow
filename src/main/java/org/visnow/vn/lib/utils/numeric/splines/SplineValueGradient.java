/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.numeric.splines;

import org.visnow.jscic.dataarrays.DataArray;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class SplineValueGradient
{

    public static float[] getSplineValue(float[] tArr, int veclen, int[] dims, float[] origin, float[][] invAffine, float[] coords)
    {
        int d0 = dims[0], d1 = dims[1], d2 = dims[2];
        for (int i = 0; i < coords.length; i++)
            coords[i] -= origin[i];

        float u = 0, v = 0, w = 0;
        for (int m = 0; m < 3; m++) {
            u += invAffine[0][m] * coords[m];
            v += invAffine[1][m] * coords[m];
            w += invAffine[2][m] * coords[m];
        }
        float[] val = new float[veclen];
        int off1 = d0;
        int off2 = d0 * d1;
        int i = min(max((int) u, 0), d0 - 1);
        u -= i;
        int j = min(max((int) v, 0), d1 - 1);
        v -= j;
        int k = min(max((int) w, 0), d2 - 1);
        w -= k;
        int il = -1;// if (i == 0) il = 0;
        int jl = -1;// if (j == 0) jl = 0;
        int kl = -1;// if (k == 0) kl = 0;
        int iu = 3;// if (i + iu > tDims[0])  iu = tDims[0] - i;
        int ju = 3;// if (j + ju > tDims[1])  ju = tDims[1] - j;
        int ku = 3;// if (k + ku > tDims[2])  ku = tDims[2] - k;
        float vu = 0, vv = 0, vw = 0;
        for (int k1 = kl; k1 < ku; k1++) {
            switch (k1) {
                case -1:
                    vw = ((-.5f * w + 1.0f) * w - .5f) * w;
                    break;
                case 0:
                    vw = (1.5f * w - 2.5f) * w * w + 1.f;
                    break;
                case 1:
                    vw = ((-1.5f * w + 2.f) * w + .5f) * w;
                    break;
                case 2:
                    vw = (.5f * w - .5f) * w * w;
                    break;
            }
            for (int j1 = jl; j1 < ju; j1++) {
                switch (j1) {
                    case -1:
                        vv = ((-.5f * v + 1.0f) * v - .5f) * v;
                        break;
                    case 0:
                        vv = (1.5f * v - 2.5f) * v * v + 1.f;
                        break;
                    case 1:
                        vv = ((-1.5f * v + 2.f) * v + .5f) * v;
                        break;
                    case 2:
                        vv = (.5f * v - .5f) * v * v;
                        break;
                }
                for (int i1 = il; i1 < iu; i1++) {
                    switch (i1) {
                        case -1:
                            vu = ((-.5f * u + 1.0f) * u - .5f) * u;
                            break;
                        case 0:
                            vu = (1.5f * u - 2.5f) * u * u + 1.f;
                            break;
                        case 1:
                            vu = ((-1.5f * u + 2.f) * u + .5f) * u;
                            break;
                        case 2:
                            vu = (.5f * u - .5f) * u * u;
                            break;
                    }
                    int idx = max(0, min(k + k1, d2 - 1)) * off2 + max(0, min(j + j1, d1 - 1)) * off1 + max(0, min(i + i1, d0 - 1));
                    for (int iv = 0; iv < veclen; iv++)
                        val[iv] += tArr[veclen * idx + iv] * vu * vv * vw;
                }
            }
        }
        return val;
    }

    public static class Interpolate implements Runnable
    {

        private int nThreads = 1;
        private int iThread = 0;
        private int[] dims;
        private float[] data = null;
        private float[] vals = null;
        private float[][] invAffine = null;
        private float[] origin = null;
        private int veclen = 1;
        private float[] coords;
        int n = 0;

        public Interpolate(int nThreads, int iThread, int[] dims, float[] origin, float[][] invAffine, float[] data, int veclen, float[] vals, float[] coords)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.dims = dims;
            this.origin = origin;
            this.invAffine = invAffine;
            this.data = data;
            this.vals = vals;
            this.veclen = veclen;
            this.coords = coords;
            n = coords.length / 3;
        }

        @Override
        public void run()
        {
            for (int i = (iThread * n) / nThreads; i < ((iThread + 1) * n) / nThreads; i++) {
                float[] x = new float[3];
                System.arraycopy(coords, 3 * i, x, 0, 3);
                System.arraycopy(getSplineValue(data, veclen, dims, origin, invAffine, x), 0, vals, veclen * i, veclen);
            }
        }
    }

    public static float[] getSplineValues(float[] tArr, int[] dims, float[] origin, float[][] invAffine, int veclen, float[] coords)
    {
        float[] vals = new float[coords.length / 3];
        int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new Interpolate(nThreads, iThread, dims, origin, invAffine, tArr, veclen, vals, coords));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            }catch (InterruptedException e) {
            }
        return vals;
    }
    
    public static float[] getSplineValues(DataArray tArr, int[] dims, float[] origin, float[][] invAffine, int veclen, float[] coords)
    {
        return getSplineValues(tArr.getRawFloatArray().getData(), dims, origin, invAffine, veclen, coords);
    }

    public static double getSplineValueGradient(DataArray tArr, int d0, int d1, int d2, float[] coords, float[] gradient)
    {
        float u = coords[0];
        float v = coords[1];
        float w = coords[2];
        double val = 0;
        for (int i = 0; i < gradient.length; i++)
            gradient[i] = 0;
        int off1 = d0;
        int off2 = d0 * d1;
        int i = min(max((int) u, 0), d0 - 1);
        u -= i;
        int j = min(max((int) v, 0), d1 - 1);
        v -= j;
        int k = min(max((int) w, 0), d2 - 1);
        w -= k;
        int il = -1;// if (i == 0) il = 0;
        int jl = -1;// if (j == 0) jl = 0;
        int kl = -1;// if (k == 0) kl = 0;
        int iu = 3;// if (i + iu > tDims[0])  iu = tDims[0] - i;
        int ju = 3;// if (j + ju > tDims[1])  ju = tDims[1] - j;
        int ku = 3;// if (k + ku > tDims[2])  ku = tDims[2] - k;
        float vu = 0, vv = 0, vw = 0;
        float du = 0, dv = 0, dw = 0;
        for (int k1 = kl; k1 < ku; k1++) {
            switch (k1) {
                case -1:
                    vw = ((-.5f * w + 1.0f) * w - .5f) * w;
                    dw = (-1.5f * w + 2.0f) * w - .5f;
                    break;
                case 0:
                    vw = (1.5f * w - 2.5f) * w * w + 1.f;
                    dw = (4.5f * w - 5.0f) * w;
                    break;
                case 1:
                    vw = ((-1.5f * w + 2.f) * w + .5f) * w;
                    dw = (-4.5f * w + 4.f) * w + .5f;
                    break;
                case 2:
                    vw = (.5f * w - .5f) * w * w;
                    dw = (1.5f * w - 1.f) * w;
                    break;
            }
            for (int j1 = jl; j1 < ju; j1++) {
                switch (j1) {
                    case -1:
                        vv = ((-.5f * v + 1.0f) * v - .5f) * v;
                        dv = (-1.5f * v + 2.0f) * v - .5f;
                        break;
                    case 0:
                        vv = (1.5f * v - 2.5f) * v * v + 1.f;
                        dv = (4.5f * v - 5.0f) * v;
                        break;
                    case 1:
                        vv = ((-1.5f * v + 2.f) * v + .5f) * v;
                        dv = (-4.5f * v + 4.f) * v + .5f;
                        break;
                    case 2:
                        vv = (.5f * v - .5f) * v * v;
                        dv = (1.5f * v - 1.f) * v;
                        break;
                }
                for (int i1 = il; i1 < iu; i1++) {
                    switch (i1) {
                        case -1:
                            vu = ((-.5f * u + 1.0f) * u - .5f) * u;
                            du = (-1.5f * u + 2.0f) * u - .5f;
                            break;
                        case 0:
                            vu = (1.5f * u - 2.5f) * u * u + 1.f;
                            du = (4.5f * u - 5.0f) * u;
                            break;
                        case 1:
                            vu = ((-1.5f * u + 2.f) * u + .5f) * u;
                            du = (-4.5f * u + 4.f) * u + .5f;
                            break;
                        case 2:
                            vu = (.5f * u - .5f) * u * u;
                            du = (1.5f * u - 1.f) * u;
                            break;
                    }
                    float t = tArr.getFloatElement(max(0, min(k + k1, d2 - 1)) * off2 + max(0, min(j + j1, d1 - 1)) * off1 + max(0, min(i + i1, d0 - 1)))[0];
                    val += t * vu * vv * vw;
                    gradient[0] += 2 * t * du * vv * vw;
                    gradient[1] += 2 * t * vu * dv * vw;
                    gradient[2] += 2 * t * vu * vv * dw;
                }
            }
        }
        return val;
    }

    private SplineValueGradient()
    {
    }

}
