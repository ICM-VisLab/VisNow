/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.painting;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import org.visnow.vn.lib.utils.ImageUtils;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public abstract class PaintingDevice
{

    protected PaintingDeviceUI ui;
    protected Cursor cursor;
    protected Color color = Color.RED;
    protected Color cursorColor = Color.WHITE;
    protected Color bgTransparencyColor = Color.MAGENTA;

    public PaintingDeviceUI getUI()
    {
        return ui;
    }

    public Cursor getCursor()
    {
        return cursor;
    }

    public void setColor(Color color)
    {
        this.color = color;
    }

    public Color getColor()
    {
        return color;
    }

    public abstract BufferedImage onMouseClicked(int x, int y, int button, int nClicks, BufferedImage cursorLayer);

    public abstract void onMousePressed(int x, int y, int button, BufferedImage cursorLayer);

    public abstract BufferedImage onMouseReleased(int x, int y, int button, BufferedImage cursorLayer);

    public abstract void onMouseDragged(int x, int y, int button, BufferedImage cursorLayer);

    public abstract void onMouseMoved(int x, int y, BufferedImage cursorLayer);

    public void onMouseWheelMoved(int x, int y, BufferedImage cursorLayer)
    {
        onMouseMoved(x, y, cursorLayer);
    }

    protected abstract void paintOn(int x, int y, BufferedImage layer, boolean inverse);

    protected abstract void paintCursorOn(int x, int y, BufferedImage layer);

    public Color getBgTransparencyColor()
    {
        return bgTransparencyColor;
    }

    public void setBgTransparencyColor(Color bgTransparencyColor)
    {
        this.bgTransparencyColor = bgTransparencyColor;
    }

    protected void paintBackground(BufferedImage layer)
    {
        Graphics g = layer.getGraphics();
        Color oldColor = g.getColor();
        g.setColor(bgTransparencyColor);
        g.fillRect(0, 0, layer.getWidth(), layer.getHeight());
        g.setColor(oldColor);
        ImageUtils.makeTransparent(layer, bgTransparencyColor);
    }

    public Color getCursorColor()
    {
        return cursorColor;
    }

    public void setCursorColor(Color cursorColor)
    {
        this.cursorColor = cursorColor;
    }

}
