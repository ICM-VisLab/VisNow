/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.utils.SliceUtils;
import org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams;
import static org.visnow.vn.lib.utils.interpolation.SubsetGeometryComponents.*;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */


public class IndexSlice1D  extends IndexSlice
{
    private IndexSlice1D(RegularField field, IndexSliceParams params) throws Exception
    {
        if (field == null)
            throw new IllegalArgumentException("null inField");
        this.params = params;
        inField = field;
        inDims = inField.getDims();
        fSlice = new float[inDims.length - 1];
        low = new int[1];
        up = new int[1];
        outDims = new int[1];
        nOutNodes = 1;
        for (int i = 0, j = 0; i < inDims.length; i++) 
            if (params.getFixed()[i]) {
                fSlice[j] = params.getPosition()[i];
                j += 1;
            }
            else {
                axis = i;
                low[0] = params.getLow()[i];
                up[0] =  params.getUp()[i];
                nOutNodes = outDims[0] = up[0] - low[0];
            }
        collectData(params.isAdjusting());
    }
    
    void computeSlicedData()
    {
        int[] slice = new int[fSlice.length];
        for (int i = 0; i < slice.length; i++) 
            slice[i] = (int)fSlice[i];
        long start = 0, n = outDims[0], step = 1;
        int offset0 = 0;
        int offset1 = 0;
        if (inDims.length == 3) {
            switch (axis) {
                case 2:
                    start = low[0] * inDims[1] * inDims[0] + slice[1] * inDims[0] + slice[0];
                    step = inDims[0] * inDims[1];
                    offset0 = 1;
                    offset1 = inDims[0];
                    break;
                case 1:
                    start = slice[1] * inDims[1] * inDims[0] + low[0] * inDims[0] + slice[0];
                    step = inDims[0];
                    offset0 = 1;
                    offset1 = inDims[0] * inDims[1];
                    break;
                case 0:
                    start = slice[1] * inDims[1] * inDims[0] + slice[0] * inDims[0] + low[0];
                    step = 1;
                    offset0 = inDims[0];
                    offset1 = inDims[0] * inDims[1];
                    break;
            }
            for (int idata =  0; idata < inData.length; idata++) {
                long veclen = (long)vlens[idata];
                if (slice[0] == fSlice[0] && slice[1] == fSlice[1]) {
                    if (inData[idata] instanceof FloatLargeArray)
                        outData[idata] = SliceUtils.get1DSlice(inData[idata], start, n, step, veclen);
                    else {
                        LargeArray tmp = SliceUtils.get1DSlice(inData[idata], start, n, step, veclen);
                        outData[idata] = new FloatLargeArray(tmp.length());
                        for (long i = 0; i < tmp.length(); i++) 
                            outData[idata].setFloat(i, tmp.getFloat(i));
                    }
                }
                else if (slice[0] == fSlice[0] && slice[1] != fSlice[1]){
                    float t = fSlice[1] - slice[1];
                    LargeArray d0 = SliceUtils.get1DSlice(inData[idata], start, n, step, veclen);
                    LargeArray d1 = SliceUtils.get1DSlice(inData[idata], start + offset1, n, step, veclen);
                    for (long i = 0; i < outData[idata].length(); i++)
                        outData[idata].setFloat(i, t * d1.getFloat(i) + (1 - t) * d0.getFloat(i));
                }
                else if (slice[0] != fSlice[0] && slice[1] == fSlice[1]){
                    float t = fSlice[0] - slice[0];
                    LargeArray d0 = SliceUtils.get1DSlice(inData[idata], start, n, step, veclen);
                    LargeArray d1 = SliceUtils.get1DSlice(inData[idata], start + offset0, n, step, veclen);
                    for (long i = 0; i < outData[idata].length(); i++)
                        outData[idata].setFloat(i, t * d1.getFloat(i) + (1 - t) * d0.getFloat(i));
                }
                else {
                    float t0 = fSlice[0] - slice[0];
                    float t1 = fSlice[0] - slice[0];
                    LargeArray d00 = SliceUtils.get1DSlice(inData[idata], start, n, step, veclen);
                    LargeArray d01 = SliceUtils.get1DSlice(inData[idata], start + offset0, n, step, veclen);
                    LargeArray d10 = SliceUtils.get1DSlice(inData[idata], start + offset1, n, step, veclen);
                    LargeArray d11 = SliceUtils.get1DSlice(inData[idata], start + offset1 + offset0, n, step, veclen);
                    for (long i = 0; i < outData[idata].length(); i++)
                        outData[idata].setFloat(i, (1 - t1) * ((1 - t0) * d00.getFloat(i) + t0 * d01.getFloat(i)) + 
                                                        t1  * ((1 - t0) * d10.getFloat(i) + t0 * d11.getFloat(i)));
                }
            }
        }
        else {
            switch (axis) {
                case 1:
                    start = low[0] * inDims[0] + slice[0];
                    step = inDims[0];
                    offset0 = 1;
                    break;
                case 0:
                    start = slice[0] * inDims[0] + low[0];
                    step = 1;
                    offset0 = inDims[0];
                    break;
            }
            for (int idata =  0; idata < inData.length; idata++) {
                long veclen = (long)vlens[idata];
                if (slice[0] == fSlice[0]) {
                    if (inData[idata] instanceof FloatLargeArray)
                        outData[idata] = SliceUtils.get1DSlice(inData[idata], start, n, step, veclen);
                    else {
                        LargeArray tmp = SliceUtils.get1DSlice(inData[idata], start, n, step, veclen);
                        outData[idata] = new FloatLargeArray(tmp.length());
                        for (long i = 0; i < tmp.length(); i++) 
                            outData[idata].setFloat(i, tmp.getFloat(i));
                    }
                }
                else {
                    float t = fSlice[0] - slice[0];
                    LargeArray d0 = SliceUtils.get1DSlice(inData[idata], start, n, step, veclen);
                    LargeArray d1 = SliceUtils.get1DSlice(inData[idata], start + offset0, n, step, veclen);
                    for (long i = 0; i < outData[idata].length(); i++)
                        outData[idata].setFloat(i, t * d1.getFloat(i) + (1 - t) * d0.getFloat(i));
                }
            }
        }
    }
    
    void addIndexCoords()
    {
        float[] indexCoords= new float[inDims.length * nOutNodes];
        switch (inDims.length) {
        case 3:
            for (int j = 0, l = 0; j < outDims[0]; j++, l += 3) 
                switch (axis) {
                    case 0:
                        indexCoords[l] =     j + low[0];
                        indexCoords[l + 1] = fSlice[0];
                        indexCoords[l + 2] = fSlice[1];
                        break;
                    case 1:
                        indexCoords[l] =     fSlice[0];
                        indexCoords[l + 1] = j + low[0];
                        indexCoords[l + 2] = fSlice[1];
                        break;
                    case 2:
                        indexCoords[l] =     fSlice[0];
                        indexCoords[l + 1] = fSlice[1];
                        indexCoords[l + 2] = j + low[0];
                        break;
                }
            break;
        case 2:
            for (int j = 0, l = 0; j < outDims[0]; j++, l += 2) 
                switch (axis) {
                    case 0:
                        indexCoords[l] =     j + low[0];
                        indexCoords[l + 1] = fSlice[0];
                        break;
                    case 1:
                        indexCoords[l] =     fSlice[0];
                        indexCoords[l + 1] = j + low[0];
                        break;
                }
        }
        if (regularSlice.getComponent(INDEX_COORDS) != null)
            regularSlice.removeComponent(INDEX_COORDS);
        regularSlice.addComponent(DataArray.create(indexCoords, inDims.length, INDEX_COORDS));
        float[] outT = new float[nOutNodes];
        outT[0] = 0;
        if (regularSlice.hasCoords()) {
            float[] outC = regularSlice.getCurrentCoords().getData();
            for (int i = 1; i < nOutNodes; i++) 
                outT[i] = (float)Math.sqrt((outC[3 * i]     - outC[3 * i - 3]) * (outC[3 * i]     - outC[3 * i - 3]) +
                                           (outC[3 * i + 1] - outC[3 * i - 2]) * (outC[3 * i + 1] - outC[3 * i - 2]) +
                                           (outC[3 * i + 2] - outC[3 * i - 1]) * (outC[3 * i + 2] - outC[3 * i - 1])) +
                                            outT[i - 1];
        }
        else {
            float r = (float)Math.sqrt(sliceAffine[0][0] * sliceAffine[0][0] +
                                       sliceAffine[0][1] * sliceAffine[0][1] +
                                       sliceAffine[0][2] * sliceAffine[0][2]);
            for (int i = 1; i < nOutNodes; i++) 
                outT[i] = i * r;
        }
            
        regularSlice.addComponent(DataArray.create(outT, 1, LINE_SLICE_COORDS));
    }
    
    
    public static RegularField slice(RegularField inField, IndexSliceParams params)
    {
        try {
            return (RegularField)(new IndexSlice1D(inField, params).slice(params.isAdjusting()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
