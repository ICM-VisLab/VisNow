/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils;

/**
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 */


public class MantissaRange {
    private MantissaRange()
    {
        
    }
    
    public static float[] mantissaRange(double low, double up)
    {
        if (low > up) {
            double t = low;
            low = up;
            up = t;
        }
        double lowMantissa = 0, upMantissa = 0;
        int lowExp = 0, upExp = 0;
        if (low != 0) 
            lowExp = (int)Math.floor(Math.log10(Math.abs(low)));
        if (up != 0) 
            upExp  = (int)Math.floor(Math.log10(Math.abs(up)));
        int exp = Math.max(lowExp, upExp);
            lowMantissa = low / Math.pow(10, exp);
        if (up != 0)
            upMantissa  = up  / Math.pow(10, exp);
        if (lowMantissa > upMantissa) {
            double t = lowMantissa;
            lowMantissa = upMantissa;
            upMantissa = t;
        }
        return new float[] {(float)lowMantissa, (float)upMantissa, (float)exp};
    }
    
}
