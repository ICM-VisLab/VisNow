/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.io;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Vector;
import org.apache.commons.lang3.StringUtils;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.StringLargeArray;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.Field;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import static org.visnow.jscic.dataarrays.DataArrayType.*;
import org.apache.log4j.Logger;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */


public class WriteContainer
{
    private static class TimeStepSeries
    {
        Vector<String> content = new Vector<>();
        final float start;
        int nSteps = 1;
        float last;

        public TimeStepSeries(float t)
        {
            last = start = t;
        }

        public void addItem(String s)
        {
            content.add(s);
        }

        public boolean tryAdd(TimeStepSeries newStep)
        {
            if (content.size() != newStep.content.size())
                return false;
            for (int i = 0; i < content.size(); i++)
                if (!content.get(i).equals(newStep.content.get(i)))
                        return false;
            if (nSteps > 1) {
                float dt = (last - start) / (nSteps - 1);
                if (Math.abs(newStep.start - last -dt) > .0001 * dt)
                    return false;
            }
            last = newStep.start;
            nSteps += 1;
            return true;
        }

        public void print(PrintWriter headerWriter, boolean ascii)
        {
            headerWriter.print("timestep " + start);
            if (nSteps == 1)
                headerWriter.println();
            else
                headerWriter.println(" " + ((last - start) / (nSteps - 1)));
            if (ascii) {
                headerWriter.println("skip 1");
                for (int i = 0; i < content.size(); i++)
                    headerWriter.print((i == 0 ? "" : ", ") + content.get(i));
                headerWriter.println();
            }
            else
                for (int i = 0; i < content.size(); i++)
                    headerWriter.println(content.get(i));
            if (nSteps == 1)
                headerWriter.println("end");
            else
                headerWriter.println("repeat " + nSteps);
        }

        public void printSingle(PrintWriter headerWriter, boolean ascii)
        {
            if (ascii) {
                headerWriter.println("skip 1");
                for (int i = 0; i < content.size(); i++)
                    headerWriter.print((i == 0 ? "" : ", ") + content.get(i));
                headerWriter.println();
            }
            else
                for (int i = 0; i < content.size(); i++)
                    headerWriter.println(content.get(i));
        }
    }

    protected static final Logger LOGGER = Logger.getLogger(WriteContainer.class);

    public static final void writeHeader(DataContainer container, PrintWriter headerWriter)
    {
        try {
            int maxCmpNameLen = 0;
            int maxUnitLen = 1;
            int maxVectorDesLen = 1;
            for (DataArray da: container.getComponents())
                if (da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_STRING) {
                    if (da.getName().length() > maxCmpNameLen)
                        maxCmpNameLen = da.getName().length();
                    if (da.getUnit() != null && !da.getUnit().isEmpty() && !da.getUnit().equals("1") &&
                        da.getUnit().length() > maxUnitLen)
                        maxUnitLen = da.getUnit().length();
                    int vDesLen = 0;
                    if (da.getVectorLength() > 1) {
                        if (da.getMatrixDims()[0] != da.getVectorLength()) {
                            vDesLen = 13;
                            if (da.isSymmetric())
                                vDesLen += 4;
                            else
                                vDesLen += 2;
                        } else
                           vDesLen = 10;
                    }
                    if (vDesLen > maxVectorDesLen)
                        maxVectorDesLen = vDesLen;
            }
            for (int i = 0; i < container.getNComponents(); i++) {
                DataArray da = container.getComponent(i);
                if (da.getType() == DataArrayType.FIELD_DATA_OBJECT || da.getType() == DataArrayType.FIELD_DATA_UNKNOWN)
                    continue;
                headerWriter.printf(Locale.US, "component %" + maxCmpNameLen + "s %7s",
                                    da.getName().replace(' ', '_').replace('.', '_'), (da.getType().toString()));
                if (da.getVectorLength() > 1) {
                    if (da.getMatrixDims()[0] != da.getVectorLength()) {
                        headerWriter.print(", array " + da.getMatrixDims()[0]);
                        if (da.isSymmetric())
                            headerWriter.printf(Locale.US, " %" + (maxVectorDesLen - 9) + "s", "sym");
                        else
                            headerWriter.printf(Locale.US, " " + da.getMatrixDims()[1] + " %" + (maxVectorDesLen - 7) + "s", "");
                    } else
                        headerWriter.printf(Locale.US, ",%" + maxVectorDesLen  + "s", "vector " + da.getVectorLength());
                }
                else
                    headerWriter.printf(Locale.US, " %" + (maxVectorDesLen) + "s", "");
                if (maxUnitLen > 1) { // nontrivial units present
                    if (da.getUnit() != null && !da.getUnit().isEmpty() && !da.getUnit().equals("1"))
                        headerWriter.printf(Locale.US, ", unit %" + maxUnitLen + "s ", da.getUnit());
                    else
                        headerWriter.printf(Locale.US, "%" + (maxUnitLen + 7) + "s ", "");
                }
                boolean canSkipExtents;
                if (!da.getType().isNumericType())
                    canSkipExtents = true;
                else if (da.getType() == FIELD_DATA_BYTE &&
                         da.getPreferredMinValue() == 0 &&
                         da.getPreferredMaxValue() == 255 &&
                         da.getPhysicalMappingCoefficients()[0] == 1 &&
                         da.getPhysicalMappingCoefficients()[1] == 0)
                    canSkipExtents = true;
                else if (da.getMinValue() == da.getMaxValue())
                    canSkipExtents = true;
                else {
                    canSkipExtents = da.getPhysicalMappingCoefficients()[0] == 1 &&
                                     da.getPhysicalMappingCoefficients()[1] == 0 &&
                                     da.getMaxValue() == da.getPreferredMaxValue();
                    if (da.getVectorLength() == 1)
                        canSkipExtents &=
                                     da.getMinValue() == da.getPreferredMinValue();
                }
               if (!canSkipExtents) {
                    double v = Math.abs(da.getPreferredMinValue());
                    String fMin = v > 1e-9 && v < 1e9 || v == 0 ? "%10.5f" : "%10.5e";
                    v = Math.abs(da.getPreferredMaxValue());
                    String fMax = v > 1e-9 && v < 1e9 || v == 0 ? "%10.5f" : "%10.5e";
                    v = Math.abs(da.getPreferredPhysMinValue());
                    String fpMin = v > 1e-9 && v < 1e9 || v == 0 ? "%10.5f" : "%10.5e";
                    v = Math.abs(da.getPreferredPhysMaxValue());
                    String fpMax = v > 1e-9 && v < 1e9 || v == 0 ? "%10.5f" : "%10.5e";
                    headerWriter.printf(Locale.US, ", min " + fMin + ", max " + fMax + ", phys_min " + fpMin + ", phys_max " + fpMax,
                                         da.getPreferredMinValue(),     da.getPreferredMaxValue(),
                                         da.getPreferredPhysMinValue(), da.getPreferredPhysMaxValue());
                }
                if (da.getUserData() != null) {
                    headerWriter.print(", user:");
                    String[] udata = da.getUserData();
                    for (int j = 0; j < udata.length; j++) {
                        if (j > 0)
                            headerWriter.print(";");
                        headerWriter.print("\"" + udata[j] + "\"");
                    }
                }
                headerWriter.println();
            }
        } catch (Exception e) {
        }
    }

    public static final boolean writeBinary(DataContainer container, String prefix,
                                            PrintWriter headerWriter,
                                            MemoryMappedFileWriter largeContentOutput)
    {
        try {
            if (prefix != null && !prefix.isEmpty())
                prefix = prefix + ":";
            else
                prefix = "";
            float[] timeSteps = container.getTimesteps();
            TimeStepSeries currentSeries = null, newSeries;
            for (int step = 0; step < timeSteps.length; step++) {
                float t = timeSteps[step];
                newSeries = new TimeStepSeries(t);
                if (container instanceof Field) {
                    Field fld = (Field) container;
                    if (fld.isMaskTimestep(t)) {
                        newSeries.addItem("mask");
                        LogicLargeArray mask = fld.getMask(t);
                        largeContentOutput.writeLogicLargeArray(mask, 0, mask.length());
                    }
                    if (fld.isCoordTimestep(t)) {
                        newSeries.addItem("coords");
                        FloatLargeArray coords = fld.getCoords(t);
                        largeContentOutput.writeFloatLargeArray(coords, 0, coords.length());
                    }
                }
                for (int i = 0; i < container.getNComponents(); i++) {
                    DataArray da = container.getComponent(i);
                    if (!da.isNumeric() || !da.isTimestep(t))
                        continue;
                    newSeries.addItem(prefix + da.getName().replace(' ', '_').replace('.', '_'));
                    long length = da.getNElements() * da.getVectorLength();
                    switch (da.getType()) {
                        case FIELD_DATA_OBJECT:
                            LOGGER.warn("Object data type not supported, skipping ");
                            break;
                        case FIELD_DATA_UNKNOWN:
                            LOGGER.warn("Unknown data type, skipping ");
                            break;
                        case FIELD_DATA_STRING:
                            break;
                        case FIELD_DATA_COMPLEX:
                            largeContentOutput.writeLargeArray(((ComplexFloatLargeArray) da.getRawArray(t)).getRealArray(), 0, length);
                            largeContentOutput.writeLargeArray(((ComplexFloatLargeArray) da.getRawArray(t)).getImaginaryArray(), 0, length);
                            break;
                        default:
                            largeContentOutput.writeLargeArray(da.getRawArray(t), 0, length);
                    }
                }
                if (currentSeries == null)
                    currentSeries = newSeries;
                else if (!currentSeries.tryAdd(newSeries)) {
                        currentSeries.print(headerWriter, false);
                        currentSeries = newSeries;
                }
            }
            if (timeSteps.length > 1)
                currentSeries.print(headerWriter, false);
            else
                currentSeries.printSingle(headerWriter, false);
        } catch (IOException e) {
            if (largeContentOutput != null)
                try {
                    largeContentOutput.close();
                } catch (IOException ex) {
                }
            LOGGER.error("Error writing field", e);
            return false;
        }
        return true;
    }

    public static final boolean writeStrings(DataContainer container, String prefix,
                                             PrintWriter headerWriter,
                                             PrintWriter stringOutput)
    {
        TimeStepSeries currentSeries = null, newSeries;
        if (prefix != null && !prefix.isEmpty())
            prefix = prefix + ":";
        float[] timeSteps = container.getTimesteps();
        for (int step = 0; step < timeSteps.length; step++) {
            float t = timeSteps[step];
            newSeries = new TimeStepSeries(t);
            for (int i = 0; i < container.getNComponents(); i++) {
                DataArray da = container.getComponent(i);
                if (da.getType() == FIELD_DATA_STRING && da.isTimestep(t)) {
                    newSeries.addItem(prefix + da.getName().replace(' ', '_').replace('.', '_'));
                    long length = da.getNElements() * da.getVectorLength();
                    for (long j = 0; j < length; j++)
                        stringOutput.println(((StringLargeArray) da.getRawArray()).get(j));
                }
            }
            if (currentSeries == null)
                currentSeries = newSeries;
            else if (!currentSeries.tryAdd(newSeries)) {
                    currentSeries.print(headerWriter, false);
                    currentSeries = newSeries;
            }
        }
        if (timeSteps.length > 1)
            currentSeries.print(headerWriter, false);
        else
            currentSeries.printSingle(headerWriter, false);
        return true;
    }

    public static final boolean writeASCII(DataContainer container, String prefix,
                                           PrintWriter headerWriter,
                                           PrintWriter contentWriter)
    {
        try {
            if (prefix != null && !prefix.isEmpty())
                prefix = prefix + ":";
            else
                prefix = "";
            float[] timeSteps = container.getTimesteps();
            ArrayList<LargeArray> dataArrs = new ArrayList<>();
            ArrayList<DataArrayType> dataTypes = new ArrayList<>();
            ArrayList<Integer> dataVlens = new ArrayList<>();
            ArrayList<Integer> dataStrLengths = new ArrayList<>();
            TimeStepSeries currentSeries = null, newSeries;
            for (int step = 0; step < timeSteps.length; step++) {
                dataArrs.clear();
                dataVlens.clear();
                dataTypes.clear();
                dataStrLengths.clear();
                float t = timeSteps[step];
                newSeries = new TimeStepSeries(t);
                if (container instanceof Field) {
                    Field fld = (Field)container;
                    if (fld.getMask() != null && fld.getMask().isTimestep(t)) {
                        newSeries.addItem("mask");
                        contentWriter.printf(Locale.US, "mask ");
                        dataArrs.add(fld.getMask(t));
                        dataTypes.add(FIELD_DATA_LOGIC);
                        dataVlens.add(1);
                        dataStrLengths.add(1);
                    }
                    if (fld.getCoords() != null && fld.getCoords().isTimestep(t)) {
                        newSeries.addItem("coords");
                        contentWriter.printf(Locale.US, "           coordinates          ");
                        dataArrs.add(fld.getCoords(t));
                        dataTypes.add(FIELD_DATA_FLOAT);
                        dataVlens.add(3);
                        dataStrLengths.add(9);
                    }
                }
                for (int iData = 0; iData < container.getNComponents(); iData++) {
                    DataArray da = container.getComponent(iData);
                    if (da.getType() == DataArrayType.FIELD_DATA_OBJECT ||
                        da.getType() == DataArrayType.FIELD_DATA_UNKNOWN ||
                        !da.isTimestep(t))
                        continue;
                    newSeries.addItem(prefix + da.getName().replace(' ', '_').replace('.', '_'));
                    int vl = da.getVectorLength();
                    int dataPosLength = 0;
                    switch (da.getType()) {
                        case FIELD_DATA_LOGIC:
                            dataPosLength = 1;
                            break;
                        case FIELD_DATA_BYTE:
                            dataPosLength = 3;
                            break;
                        case FIELD_DATA_SHORT:
                            dataPosLength = 5;
                            break;
                        case FIELD_DATA_INT:
                            dataPosLength = 8;
                            break;
                        case FIELD_DATA_LONG:
                            dataPosLength = 12;
                            break;
                        case FIELD_DATA_FLOAT:
                            dataPosLength = 9;
                            break;
                        case FIELD_DATA_DOUBLE:
                            dataPosLength = 14;
                            break;
                        case FIELD_DATA_COMPLEX:
                            dataPosLength = 9;
                            vl *= 2;
                            break;
                        case FIELD_DATA_STRING:
                                dataPosLength = (int)da.getMaxValue() + 1;
                            break;
                        default:
                            contentWriter.close();
                            LOGGER.error("Error writing field: invalid field type");
                            return false;
                        }
                    int fullPosLen = Math.max(vl * (dataPosLength + 1), da.getName().length() + 1);
                    if (fullPosLen % vl > 0) {
                        dataPosLength = fullPosLen / vl + 1;
                        fullPosLen = vl * dataPosLength;
                    }
                    else
                        dataPosLength = fullPosLen / vl;
                    fullPosLen += vl;
                    dataArrs.add(da.getRawArray(t));
                    dataTypes.add(da.getType());
                    dataVlens.add(da.getVectorLength());
                    dataStrLengths.add(dataPosLength);
                    contentWriter.printf("%" + fullPosLen + "s", da.getName().replace(' ', '_'));
                }
                if (currentSeries == null)
                    currentSeries = newSeries;
                else if (!currentSeries.tryAdd(newSeries)) {
                        currentSeries.print(headerWriter, true);
                        currentSeries = newSeries;
                    }
                contentWriter.println();
                int nData = dataArrs.size();

                for (int k = 0; k < container.getNElements(); k++) {
                    contentWriter.print(" ");
                    for (int l = 0; l < nData; l++) {
                        int vl = dataVlens.get(l);
                        int fl = dataStrLengths.get(l);
                        switch (dataTypes.get(l)) {
                        case FIELD_DATA_LOGIC:
                            for (int i = 0; i < vl; i++)
                                contentWriter.printf("%" + fl + "d ", dataArrs.get(l).getBoolean(k * vl + i) ? 1 : 0);
                            break;
                        case FIELD_DATA_BYTE:
                            for (int i = 0; i < vl; i++)
                                contentWriter.printf(Locale.US, "%" + fl + "d ", dataArrs.get(l).getUnsignedByte(k * vl + i) & 0xff);
                            break;
                        case FIELD_DATA_SHORT:
                            for (int i = 0; i < vl; i++)
                                contentWriter.printf(Locale.US, "%" + fl + "d ", dataArrs.get(l).getShort(k * vl + i));
                            break;
                        case FIELD_DATA_INT:
                            for (int i = 0; i < vl; i++)
                                contentWriter.printf(Locale.US, "%" + fl + "d ", dataArrs.get(l).getInt(k * vl + i));
                            break;
                        case FIELD_DATA_LONG:
                            for (int i = 0; i < vl; i++)
                                contentWriter.printf(Locale.US, "%" + fl + "d ", dataArrs.get(l).getLong(k * vl + i));
                            break;
                        case FIELD_DATA_FLOAT:
                            for (int i = 0; i < vl; i++)
                                contentWriter.printf(Locale.US, "%" + fl + ".4f ", dataArrs.get(l).getFloat(k * vl + i));
                            break;
                        case FIELD_DATA_DOUBLE:
                            for (int i = 0; i < vl; i++)
                                contentWriter.printf(Locale.US, "%" + fl + ".6f ", dataArrs.get(l).getDouble(k * vl + i));
                            break;
                        case FIELD_DATA_COMPLEX:
                            float[] v = ((ComplexFloatLargeArray)dataArrs.get(l)).getComplexData(null, k * vl, (k + 1) * vl, 1);
                            for (int i = 0; i < vl; i++) {
                                contentWriter.printf(Locale.US, "%" + fl + ".4f ", v[2 * i]);
                                contentWriter.printf(Locale.US, "%" + fl + ".4f ", v[2 * i + 1]);
                            }
                            break;
                        case FIELD_DATA_STRING:
                            for (int i = 0; i < vl; i++) {
                                String str = ((StringLargeArray)dataArrs.get(l)).get(k * vl + i);
                                if (StringUtils.containsWhitespace(str))
                                    contentWriter.printf("%" + fl + "s " ,  "\"" + str + "\"");
                                else
                                    contentWriter.printf("%" + fl + "s " ,  str);
                            }
                            break;
                        default:
                            contentWriter.close();
                            LOGGER.error("Error writing field: invalid field type");
                            return false;
                        }
                    }
                    contentWriter.println();
                }
            }
            if (timeSteps.length > 1)
                currentSeries.print(headerWriter, true);
            else
                currentSeries.printSingle(headerWriter, true);
        } catch (Exception e) {
            if (contentWriter != null) {
                contentWriter.close();
            }
            LOGGER.error("Error writing field", e);
            return false;
        }
        return true;
    }
}
