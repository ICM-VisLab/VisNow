/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field.subset.subvolume;

import java.util.ArrayList;
import org.visnow.jlargearrays.ComplexDoubleLargeArray;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.system.main.VisNow;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 */


public class LinearInterpolation
{
    private static class InterpolatePart implements Runnable
    {
        private final int n;
        private final int from;
        private final int to;
        private final ArrayList<TimeData>inVals;
        private final ArrayList<TimeData>outVals;
        private final int[] vLens;
        private final NewNode[] newNodes;

        public InterpolatePart(int iThread, int nThreads,
                               ArrayList<TimeData> inVals, ArrayList<TimeData> outVals, int[] vLens,
                               NewNode[] newNodes)
        {
            n = newNodes.length;
            from = (int)(n * iThread / nThreads);
            to = (int)(n * (iThread + 1) / nThreads);
            this.inVals = inVals;
            this.outVals = outVals;
            this.vLens = vLens;
            this.newNodes = newNodes;
        }

        @Override
        public void run()
        {
            for (int iData = 0; iData < inVals.size(); iData++) {
                boolean interpolable;
                int vLen = vLens[iData];
                TimeData inTD  = inVals.get(iData);
                switch (inTD.getType()) {
                    case FIELD_DATA_BYTE:
                    case FIELD_DATA_SHORT:
                    case FIELD_DATA_INT:
                    case FIELD_DATA_FLOAT:
                    case FIELD_DATA_DOUBLE:
                        interpolable = true;
                        break;
                    default:
                        interpolable = false;
                }
                TimeData outTD = outVals.get(iData);
                for (int iTime = 0; iTime < outTD.getNSteps(); iTime++) {
                    LargeArray in = inTD.getValue(outTD.getTime(iTime));
                    LargeArray out = outTD.getValue(outTD.getTime(iTime));
                    if (inTD.getType() == DataArrayType.FIELD_DATA_COMPLEX)
                        if (in instanceof ComplexFloatLargeArray)
                            for (int iNode = from, l = from * vLen; iNode < to; iNode++) {
                                NewNode node = newNodes[iNode];
                                for (int i = 0; i < vLen; i++, l++) {
                                    float[] cIn1 = ((ComplexFloatLargeArray) in).get(node.p1 * vLen + i);
                                    float[] cIn0 = ((ComplexFloatLargeArray) in).get(node.p0 * vLen + i);
                                    float[] cOut = new float[2];
                                    for (int j = 0; j < 2; j++)
                                        cOut[j] = node.ratio * cIn1[j] + (1 - node.ratio) * cIn0[j];
                                    ((ComplexFloatLargeArray) out).setComplexFloat(l, cOut);
                                }
                            }
                        else
                            for (int iNode = from, l = from * vLen; iNode < to; iNode++) {
                                NewNode node = newNodes[iNode];
                                for (int i = 0; i < vLen; i++, l++) {
                                    double[] cIn1 = ((ComplexDoubleLargeArray) in).get(node.p1 * vLen + i);
                                    double[] cIn0 = ((ComplexDoubleLargeArray) in).get(node.p0 * vLen + i);
                                    double[] cOut = new double[2];
                                    for (int j = 0; j < 2; j++)
                                        cOut[j] = node.ratio * cIn1[j] + (1 - node.ratio) * cIn0[j];
                                    ((ComplexDoubleLargeArray) out).setComplexDouble(l, cOut);
                                }
                            }
                    else if (interpolable)
                        for (int iNode = from, l = from * vLen; iNode < to; iNode++) {
                            NewNode node = newNodes[iNode];
                            for (int i = 0; i < vLen; i++, l++)
                                out.setFloat(l, node.ratio * in.getFloat(node.p1 * vLen + i)
                                        + (1 - node.ratio) * in.getFloat(node.p0 * vLen + i));
                        }
                    else
                        for (int iNode = from, l = from * vLen; iNode < to; iNode++) {
                            NewNode node = newNodes[iNode];
                            if (node.ratio > .5)
                                for (int i = 0; i < vLen; i++, l++)
                                    LargeArrayUtils.arraycopy(in, node.p1 * vLen, out, l, vLen);
                            else
                                for (int i = 0; i < vLen; i++, l++)
                                    LargeArrayUtils.arraycopy(in, node.p0 * vLen, out, l, vLen);
                        }
                }
            }
        }
    }

    public static ArrayList<TimeData> interpolateToNewNodesSet(int nNodes, ArrayList<TimeData>inVals, int[] vLens,
                                                            NewNode[] newNodes,
                                                            boolean singleTimeMoment)
    {
        int nThreads = VisNow.availableProcessors();
        ArrayList<TimeData> outVals = new ArrayList<>();
        for (int i = 0; i < inVals.size(); i++) {
            TimeData inVal = inVals.get(i);
            TimeData outVal = new TimeData(inVal.getType());
            if (singleTimeMoment)
                outVal.setValue(LargeArrayUtils.create(inVal.getType().toLargeArrayType(), nNodes * vLens[i]), inVal.getCurrentTime());
            else
                for (int j = 0; j < inVal.getNSteps(); j++)
                    outVal.setValue(LargeArrayUtils.create(inVal.getType().toLargeArrayType(), nNodes * vLens[i]), inVal.getTime(j));
            outVals.add(outVal);
        }
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++)
            workThreads[iThread] = new Thread(new InterpolatePart(iThread, nThreads,
                                             inVals, outVals, vLens, newNodes));
        for (int iThread = 0; iThread < nThreads; iThread++)
            workThreads[iThread].start();
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        return outVals;
    }

    public static void interpolateFieldToNewNodesSet(Field inField, Field outField,
                                                     NewNode[] newNodes, boolean singleTimeMoment)
    {
        if (newNodes.length != outField.getNNodes())
            return;
        int nOutNodes = newNodes.length;
        int nInterpolable = inField.getNComponents();
        if (inField.hasCoords())
            nInterpolable += 1;
        if (inField.hasMask())
            nInterpolable += 1;
        ArrayList<TimeData> interpolableData = new ArrayList<>();
        ArrayList<DataArray> components = new ArrayList<>();
        int[] vLens = new int[nInterpolable];
        int m = 0;
        for (int i = 0; i < inField.getNComponents(); i++) {
            DataArray cmp = inField.getComponent(i);
            interpolableData.add(cmp.getTimeData());
            components.add(cmp);
            vLens[m] = cmp.getVectorLength();
            m += 1;
        }
        if (inField.hasCoords()) {
            interpolableData.add(inField.getCoords());
            vLens[m] = 3;
            m += 1;
        }
        if (inField.hasMask()) {
            interpolableData.add(inField.getMask());
            vLens[m] = 1;
        }
        ArrayList<TimeData> interpolatedData =
                LinearInterpolation.interpolateToNewNodesSet(nOutNodes,interpolableData, vLens,
                                                             newNodes, singleTimeMoment);

        for (m = 0; m < inField.getNComponents(); m++) {
            DataArray cmp = components.get(m);
            outField.addComponent(
                    DataArray.create(interpolatedData.get(m), vLens[m], cmp.getName(),
                                     cmp.getUnit(), cmp.getUserData()).
                              preferredRanges(cmp.getPreferredMinValue(),
                                              cmp.getPreferredMaxValue(),
                                              cmp.getPreferredPhysMinValue(),
                                              cmp.getPreferredPhysMaxValue()));
        }
        if (inField.hasCoords())
            outField.setCoords(interpolatedData.get(m));
        else {
            float[][] affine = ((RegularField)inField).getAffine();
            int[] inDims = ((RegularField)inField).getDims();
            float[] crds = new float[3];
            FloatLargeArray outCoords = new FloatLargeArray(3 * nOutNodes, false);
            for (int i = 0; i < nOutNodes; i++) {
                NewNode p = newNodes[i];
                int[] i0 = new int[inDims.length];
                int[] i1 = new int[inDims.length];
                long p0 = p.p0;
                long p1 = p.p1;
                float t = p.ratio;
                for (int j = 0; j < inDims.length; j++) {
                    i0[j] = (int)(p0 % inDims[j]);
                    p0 /= inDims[j];
                    i1[j] = (int)(p1 % inDims[j]);
                    p1 /= inDims[j];
                }
                for (int j = 0; j < 3; j++) {
                    crds[j] = affine[3][j];
                    for (int k = 0; k < inDims.length; k++) {
                        crds[j] += ((1 - t) * i0[k] + t * i1[k]) * affine[k][j];
                    }
                }
                LargeArrayUtils.arraycopy(crds, 0, outCoords, 3 * (long)i, 3);
            }
            outField.setCoords(outCoords, 0);
        }
        if (inField.hasMask())
            outField.setMask(interpolatedData.get(m));
    }
}
