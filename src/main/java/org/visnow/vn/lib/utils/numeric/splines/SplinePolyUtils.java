/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.numeric.splines;


/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class SplinePolyUtils
{
    /**
     * spline base polynomials evaluated in Horner form
     * @param x from 0 : 1 interval argument
     * @param k from -1 : 2 interval segment index
     * @return spline base polynomial value
     */
    public static double splinePolyVal(double x, int k)
    {
        switch (k) {
            case -1:
                return ((-.5 * x + 1.0) * x - .5) * x;
            case 0:
                return (1.5 * x - 2.5) * x * x + 1.;
            case 1:
                return ((-1.5 * x + 2.) * x + .5) * x;
            case 2:
                return (.5 * x - .5) * x * x;
        }
        return 0;
    }
    
    /**
     * spline base polynomial derivativess evaluated in Horner form
     * @param x from 0 : 1 interval argument
     * @param k from -1 : 2 interval segment index
     * @return  spline base polynomial derivative
     */
    public static double splinePolyDif(double x, int k)
    {
        switch (k) {
            case -1:
                return (-1.5 * x + 2.0) * x - .5;
            case 0:
                return (4.5 * x - 5.0) * x;
            case 1:
                return (-4.5 * x + 4.) * x + .5;
            case 2:
                return (1.5 * x - 1.) * x;
        }
        return 0;
    }
    
    /**
     * spline base polynomials evaluated in Horner form
     * @param x from 0 : 1 interval argument
     * @param k from -1 : 2 interval segment index
     * @return  spline base polynomial value
     */
    public static float splinePolyValf(float x, int k)
    {
        switch (k) {
            case -1:
                return (float)((-.5 * x + 1.0) * x - .5) * x;
            case 0:
                return (float)(1.5 * x - 2.5) * x * x + 1;
            case 1:
                return (float)((-1.5 * x + 2.) * x + .5) * x;
            case 2:
                return (float)(.5 * x - .5) * x * x;
        }
        return 0;
    }
    
    /**
     * spline base polynomial derivativess evaluated in Horner form
     * @param x from 0 : 1 interval argument
     * @param k from -1 : 2 interval segment index
     * @return  spline base polynomial derivative
     */
    public static float splinePolydDiff(float x, int k)
    {
        switch (k) {
            case -1:
                return (float)(-1.5 * x + 2.0) * x - .5f;
            case 0:
                return (float)(4.5 * x - 5.0) * x;
            case 1:
                return (float)(-4.5 * x + 4.) * x + .5f;
            case 2:
                return (float)(1.5 * x - 1.) * x;
        }
        return 0;
    }
    
    /**
     * spline base piecewise polynomial evaluated in Horner form
     * @param t from -1 : 2 spline argument
     * @return  spline base piecewise polynomial value
     */
    public static float splinePolyValf(float t)
    {
        float x = 1 - t + (float)Math.floor(t);
        switch ((int)Math.floor(t)) {
            case -1:
                return (float)((-.5 * x + 1.0) * x - .5) * x;
            case 0:
                return (float)(1.5 * x - 2.5) * x * x + 1;
            case 1:
                return (float)((-1.5 * x + 2.) * x + .5) * x;
            case 2:
                return (float)(.5 * x - .5) * x * x;
        }
        return 0;
    }
    
    /**
     * spline base piecewise polynomial derivative evaluated in Horner form
     * @param t from -1 : 2 spline argument
     * @return spline base piecewise polynomial derivative 
     */
    public static float splinePolydDiff(float t)
    {
        float x = 1 - t + (float)Math.floor(t);
        switch ((int)Math.floor(t)) {
            case -1:
                return (float)(-1.5 * x + 2.0) * x - .5f;
            case 0:
                return (float)(4.5 * x - 5.0) * x;
            case 1:
                return (float)(-4.5 * x + 4.) * x + .5f;
            case 2:
                return (float)(1.5 * x - 1.) * x;
        }
        return 0;
    } 
    
    public static void main(String[] args)
    {
        for (int i = 0; i < 401; i++) {
            float x = (i / 100.f) - 1;
            System.out.printf("%8.4f %8.4f %8.4f %n", x, splinePolyValf(x), splinePolydDiff(x));
        }
    }
    
}
