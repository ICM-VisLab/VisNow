/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import org.visnow.jscic.Field;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.lib.utils.MantissaRange;

/**
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 */
public class ValueRanges {
    private ValueRanges()
    {

    }

    public static final void updateValueRanges(Field inField,
                                               float[] minima,  float[] maxima,
                                               float[] mMinima, float[] mMaxima,
                                               float[] pMinima, float[] physMinima,
                                               float[] coeffs,  boolean keepPreferredRanges)
    {
        int nNodes = (int)inField.getNNodes();
        for (int i = 0; i < inField.getNComponents(); i++) {
            DataArray da = inField.getComponent(i);
            float ct = da.getCurrentTime();
            if (da.isNumeric()) {

                if (keepPreferredRanges) {
                    maxima[i] = (float)da.getPreferredMaxValue();
                    minima[i] = (float)da.getPreferredMinValue();
                }
                else {
                    for (float time : da.getTimeSeries()) {
                        da.setCurrentTime(time);
                        if (da.getVectorLength() == 1)
                            for (int j = 0; j < nNodes; j++) {
                                float v = da.getFloatElement(j)[0];
                                maxima[i] = Math.max(v, maxima[i]);
                                minima[i] = Math.min(v, minima[i]);
                            }
                        else {
                            minima[i] = mMinima[i] = 0;
                            float[] vns = da.getVectorNorms().getData();
                            for (int j = 0; j < nNodes; j++) {
                                float v = vns[j];
                                maxima[i] = Math.max(v, maxima[i]);
                            }
                        }
                    }
                }
                pMinima[i]    = (float)da.getPreferredMinValue();
                physMinima[i] = (float)da.getPreferredPhysMinValue();
                coeffs[i]     = (float)((da.getPreferredPhysMaxValue() - physMinima[i]) /
                                        (da.getPreferredMaxValue() - pMinima[i]));
                da.setCurrentTime(ct);
            }
        }
    }

    public static final void updateExponentRanges(float[] minima,  float[] maxima,
                                                  float[] mMinima, float[] mMaxima,
                                                  float[] pMinima, float[] physMinima,
                                                  float[] coeffs,  int[] exponents)
    {
        for (int i = 0; i < minima.length; i++) {
            float pMin  = pMinima[i];
            float phMin = physMinima[i];
            float coeff = coeffs[i];
            minima[i]     = (phMin + coeff * (minima[i] - pMin));
            maxima[i]     = (phMin + coeff * (maxima[i] - pMin));
            float[] mR = MantissaRange.mantissaRange(minima[i], maxima[i]);
            mMinima[i] = mR[0];
            mMaxima[i] = mR[1];
            exponents[i] = (int)mR[2];
        }
    }
}
