/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.numeric.ODE;

import java.util.Arrays;

public class RandomizedRungeKutta
{

    // Fourth order Runge-Kutta for n (nSpace) ordinary differential equations (ODE)
    public static int fourthOrderRK(Deriv g, float[] y0, float randomAmount, float h, int nsteps, float[] trajectory, float[] vectors)
    {
        int nSpace = y0.length;
        float[] y1 = new float[nSpace];
        float[] y2 = new float[nSpace];
        float[] y3 = new float[nSpace];
        float[] y4 = new float[nSpace];
        float[] y = new float[nSpace];
        float[] yd = new float[nSpace];
        float[] dy;
        int iStep = 0;
        Arrays.fill(vectors, 0);
        Arrays.fill(trajectory, 0);
        System.arraycopy(y0, 0, y, 0, nSpace);
        System.arraycopy(y0, 0, trajectory, 0, nSpace);

        // iteration over allowed steps: iStep is current step number, k is stored step number
        try {
            for (iStep = 0; iStep < nsteps; iStep++) {
                dy = g.derivn(y);
                if (dy == null)
                    break;
                System.arraycopy(dy, 0, vectors, iStep * 3, 3);
                for (int i = 0; i < nSpace; i++) {
                    y1[i] = h * dy[i];
                    yd[i] = y[i] + y1[i] / 2;
                }
                dy = g.derivn(yd);
                if (dy == null)
                    break;
                for (int i = 0; i < nSpace; i++) {
                    y2[i] = h * dy[i];
                    yd[i] = y[i] + y2[i] / 2;
                }
                dy = g.derivn(yd);
                if (dy == null)
                    break;
                for (int i = 0; i < nSpace; i++) {
                    y3[i] = h * dy[i];
                    yd[i] = y[i] + y3[i];
                }
                dy = g.derivn(yd);
                if (dy == null)
                    break;
                for (int i = 0; i < nSpace; i++) {
                    y4[i] = h * dy[i];
                    y[i] += y1[i] / 6 + y2[i] / 3 + y3[i] / 3 + y4[i] / 6;
                }
                for (int i = 0; i < y.length; i++)
                    y[i] += (float) (randomAmount * (Math.random() - .5));
                System.arraycopy(y, 0, trajectory, iStep * nSpace, nSpace);
            }
            if (iStep < nsteps)
                for (int i = Math.max(iStep, 1); i < nsteps; i++) {
                    System.arraycopy(trajectory, nSpace * (i - 1), trajectory, nSpace * i, nSpace);
                    System.arraycopy(vectors, 3 * (i - 1), vectors, 3 * i, 3);
                }
        } catch (Exception e) {

        }
        return iStep;
    }

    private RandomizedRungeKutta()
    {
    }
}
