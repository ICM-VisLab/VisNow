/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.interpolation;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
import static org.visnow.vn.lib.utils.slicing.SliceLookupTable.*;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.cells.CellType;
import org.visnow.jlargearrays.LogicLargeArray;

public class GridPoitionsInSimplex {
    
    protected float[] recomputedCoords;
    protected int[] dims;
    protected int nNodes;
    protected FieldPosition[] result;
    protected LogicLargeArray valid;
/**
 * finds output field nodes inside given simplex and interpolates data
 * @param dims                dimensions of the interpolated field
 * @param recomputedCoords    node coordinates remapped to output field grid coordinates
 * @param result              on output positions of grid nodes with respect to the processed simplex are added
 * @param valid               mask array filled with true when an output node is within input field area
 */
    public GridPoitionsInSimplex(int[] dims, float[] recomputedCoords, 
                                 FieldPosition[] result, LogicLargeArray valid)
    {
        this.recomputedCoords = recomputedCoords;
        this.dims = dims;
        nNodes = 1;
        for (int dim : dims) 
            nNodes *= dim;
        this.result = result;
        this.valid = valid;
    }
    

    /**
     * finds grid points inside tetra and stores positions of grid points inside tetrahedron
     * @param nodes nodes of processed tetrahedron
     * 
     */    
    public void processTetra(int[] nodes, int index)
    {
        // tetraCoords - coordinates of tetra vertices (recomputed to the outField index coordinate system)
        float[][] tetraCoords = new float[4][3];

        float[][] sliceVertsBaryCoords = new float[4][4];
        float[][] sliceVertsPlaneCoords = new float[4][2];

        float[][] segmentVertsTriangleBaryCoords = new float[2][3];
        float[][] segmentVertsTetraBaryCoords = new float[2][4];
        float[] segmentVertsLineCoords = new float[2];

        for (int i = 0; i < nodes.length; i++)
            System.arraycopy(recomputedCoords, 3 * nodes[i], tetraCoords[i], 0, 3);
        float zMin = dims[2] - .9999f, zMax = -.0001f;
        float yMin = dims[1] - .9999f, yMax = -.0001f;
        float xMin = dims[0] - .9999f, xMax = -.0001f;
        for (float[] tetraCoord : tetraCoords) {
            if (tetraCoord[2] < zMin)
                zMin = tetraCoord[2];
            if (tetraCoord[2] > zMax)
                zMax = tetraCoord[2];
            if (tetraCoord[1] < yMin)
                yMin = tetraCoord[1];
            if (tetraCoord[1] > yMax)
                yMax = tetraCoord[1];
            if (tetraCoord[0] < xMin)
                xMin = tetraCoord[0];
            if (tetraCoord[0] > xMax)
                xMax = tetraCoord[0];
        }
        // xMin:xMax, yMin:yMax, zMin:zMax - range of tetra
        if (zMin > dims[2] - 1 || zMax < 0 || yMin > dims[1] - 1 || yMax < 0 || xMin > dims[0] - 1 || xMax < 0)
            return;
        if ((int) zMin == (int) zMax && zMin != (int) zMin ||
            (int) yMin == (int) yMax && yMin != (int) yMin ||
            (int) xMin == (int) xMax && xMin != (int) xMin)
            return;
        int k0 = max((int) zMin, 0);
        if (k0 < zMin)
            k0 += 1;
        zMax = min(dims[2] - 1, zMax);
        float[] vals = new float[4];
        // loop over possible z-indices in outfield (planes z = i cutting tetra)
        zLoop:
        for (int iz = k0; iz <= zMax; iz++) {
            for (int p = 0; p < 4; p++)
                vals[p] = tetraCoords[p][2] - iz;
            CellType[] sliceNodes = slice[4][simplexCode(vals)];
            if (sliceNodes == null || sliceNodes.length < 3)
                continue;
            // computing triangle (or triangles) forming slice of the tetra and z=i plane
            // slice nodes are vertices of the triangle or quad, 
            // sliceVertsBaryCoords[i] is a vector of coefficients of the i-th node as linear combinations of original coords
            // sliceVertsPlaneCoords[i] contains xy coordinates of the i-th node
            int nTri = 1;
            if (sliceNodes.length == 4)
                nTri = 2;
            for (int i = 0; i < sliceNodes.length; i++) {
                CellType is = sliceNodes[i];
                for (int j = 0; j < sliceVertsBaryCoords[i].length; j++)
                    sliceVertsBaryCoords[i][j] = 0;
                if (is.getValue() < 4) {
                    sliceVertsBaryCoords[i][is.getValue()] = 1;
                    sliceVertsPlaneCoords[i][0] = tetraCoords[is.getValue()][0];
                    sliceVertsPlaneCoords[i][1] = tetraCoords[is.getValue()][1];
                } else {
                    int p0 = ADD_NODES[4][is.getValue()][0];
                    int p1 = ADD_NODES[4][is.getValue()][1];
                    float u = vals[p1] / (vals[p1] - vals[p0]);
                    if (Float.isNaN(u) || u == Float.NEGATIVE_INFINITY || u == Float.POSITIVE_INFINITY)
                        continue zLoop;
                    sliceVertsBaryCoords[i][p0] = u;
                    sliceVertsBaryCoords[i][p1] = 1 - u;
                    sliceVertsPlaneCoords[i][0] = u * tetraCoords[p0][0] + (1 - u) * tetraCoords[p1][0];
                    sliceVertsPlaneCoords[i][1] = u * tetraCoords[p0][1] + (1 - u) * tetraCoords[p1][1];
                }
            }

            for (int iTri = 0; iTri < nTri; iTri++) {
                int[] triNodes;
                if (iTri == 0)
                    triNodes = new int[]{0, 1, 2};
                else
                    triNodes = new int[]{0, 2, 3};
                float[][] triXYCoords = new float[3][];
                // slicing z-slice triangle along along y axis 
                for (int i = 0; i < 3; i++)
                    triXYCoords[i] = sliceVertsPlaneCoords[triNodes[i]];

                yMin = dims[1] - .9999f;
                yMax = -.0001f;
                xMin = dims[0] - .9999f;
                xMax = -.0001f;
                for (int i = 0; i < 3; i++) {
                    if (triXYCoords[i][1] < yMin)
                        yMin = triXYCoords[i][1];
                    if (triXYCoords[i][1] > yMax)
                        yMax = triXYCoords[i][1];
                    if (triXYCoords[i][0] < xMin)
                        xMin = triXYCoords[i][0];
                    if (triXYCoords[i][0] > xMax)
                        xMax = triXYCoords[i][0];
                }
                if (yMin > dims[1] - 1 || yMax < 0 || xMin > dims[0] - 1 || xMax < 0)
                    continue;
                if ((int) yMin == (int) yMax && yMin != (int) yMin ||
                    (int) xMin == (int) xMax && xMin != (int) xMin)
                    continue;
                int l0 = max((int) yMin, 0);
                if (l0 < yMin)
                    l0 += 1;
                yMax = min(dims[1] - 1, yMax);
                float[] triVals = new float[3];
                // loop over possible y-indices in outfield (segments y = iy cutting triangle)
                yLoop:
                for (int iy = l0; iy <= yMax; iy++) {
                    for (int p = 0; p < 3; p++)
                        triVals[p] = triXYCoords[p][1] - iy;
                    CellType[] segmentNodes = slice[2][simplexCode(triVals)];
                    if (segmentNodes == null || segmentNodes.length < 2)
                        continue;

                    for (int i = 0; i < 2; i++) {
                        CellType is = segmentNodes[i];
                        for (int j = 0; j < segmentVertsTriangleBaryCoords[i].length; j++)
                            segmentVertsTriangleBaryCoords[i][j] = 0;
                        if (is.getValue() < 3) {
                            segmentVertsTriangleBaryCoords[i][is.getValue()] = 1;
                            segmentVertsLineCoords[i] = triXYCoords[is.getValue()][0];
                            System.arraycopy(sliceVertsBaryCoords[triNodes[is.getValue()]], 0, segmentVertsTetraBaryCoords[i], 0, 4);
                        } else {
                            int p0 = ADD_NODES[3][is.getValue()][0];
                            int p1 = ADD_NODES[3][is.getValue()][1];
                            float u = triVals[p1] / (triVals[p1] - triVals[p0]);
                            if (u == Float.NaN || u == Float.NEGATIVE_INFINITY || u == Float.POSITIVE_INFINITY)
                                continue yLoop;
                            segmentVertsTriangleBaryCoords[i][p0] = u;
                            segmentVertsTriangleBaryCoords[i][p1] = 1 - u;
                            segmentVertsLineCoords[i] = u * triXYCoords[p0][0] + (1 - u) * triXYCoords[p1][0];
                            for (int j = 0; j < 4; j++)
                                segmentVertsTetraBaryCoords[i][j] = u * sliceVertsBaryCoords[triNodes[p0]][j] +
                                    (1 - u) * sliceVertsBaryCoords[triNodes[p1]][j];
                        }
                    }

                    xMin = segmentVertsLineCoords[0];
                    xMax = segmentVertsLineCoords[1];
                    if (segmentVertsLineCoords[1] < segmentVertsLineCoords[0]) {
                        xMin = segmentVertsLineCoords[1];
                        xMax = segmentVertsLineCoords[0];
                    }
                    if (xMin > dims[0] - 1 || xMax < 0)
                        continue;
                    int m0 = max((int) xMin, 0);
                    if (m0 < xMin)
                        m0 += 1;
                    float d = 1 / (segmentVertsLineCoords[1] - segmentVertsLineCoords[0]);
                    if (d == Float.NaN || d == Float.NEGATIVE_INFINITY || d == Float.POSITIVE_INFINITY)
                        continue;
                    for (int ix = m0, iData = (dims[1] * iz + iy) * dims[0] + m0; ix <= min(xMax, dims[0] - 1); ix++, iData++) {
                        if (iData < 0 || iData >= nNodes)
                        {
                            System.out.printf("%3d %3d %3d   %d %n", ix, iy, iz, iData);
                            continue;
                        }
                        if (valid.getBoolean(iData))
                            continue;
                        float t = d * (segmentVertsLineCoords[1] - ix);
                        float[] baryCoords = new float[4];
                        for (int i = 0; i < 4; i++)
                            baryCoords[i] = t * segmentVertsTetraBaryCoords[0][i] +
                                (1 - t) * segmentVertsTetraBaryCoords[1][i];
                        valid.setByte(iData, (byte) 1);
                        result[iData] = new FieldPosition(index, baryCoords[0], nodes, baryCoords);
                    }
                }
            }
        }
    }

    /**
     * finds grid points inside triangle and interpolates inData to these points storing results in outData
     * @param nodes nodes of processed triangle
     * @param index index to cell data valid when cell data are interpolated
     */
    public void processTriangle(int[] nodes, int index)
    {
/*
                     * triCoords[0]             ----- yMax                                           
                    / \                                                                   
                   /+  \.   .    .                                                         
                  /     \                                                               
   triCoords[2]  *       \                                                               
                   \+   + \ .    .   ---- iy                                                      
                     \     \                                                                
                       \    \                                                                       
                    .   .\  +\   .                                                             
                           \  \                                                               
                             \ \                                                               
                    .   .   .  \\. 
                                * triCoords[1]  ----- yMinx  
        
                                                 
                   \+   + \ .    .   ---- ix     
*/        
        // triCoords - coordinates of triangle vertices (recomputed to the outField index coordinate system)
        float[][] triCoords = new float[3][2];
        for (int i = 0; i < 3; i++)
            System.arraycopy(recomputedCoords, 2 * nodes[i], triCoords[i], 0, 2);
        float yMin = dims[1] - .9999f, yMax = -.0001f;
        float xMin = dims[0] - .9999f, xMax = -.0001f;
        for (float[] triCoord : triCoords) {
            if (triCoord[1] < yMin)
                yMin = triCoord[1];
            if (triCoord[1] > yMax)
                yMax = triCoord[1];
            if (triCoord[0] < xMin)
                xMin = triCoord[0];
            if (triCoord[0] > xMax)
                xMax = triCoord[0];
        }
        float[][] segmentVertsBaryCoords = new float[2][3];
        float[] segmentVertsLineCoords = new float[2];

        // xMin:xMax, yMin:yMax - range of triangle
        if (yMin > dims[1] - 1 || yMax < 0 || xMin > dims[0] - 1 || xMax < 0)
            return;
        if ((int) yMin == (int) yMax && yMin != (int) yMin ||
            (int) xMin == (int) xMax && xMin != (int) xMin)
            return;
        int y0 = max((int) yMin, 0);
        if (y0 < yMin)
            y0 += 1;
        yMax = min(dims[1] - 1, yMax);
        float[] triVals = new float[3];
        // loop over possible y-indices in outfield (segments y = iy cutting triangle)
        yLoop:
        for (int iy = y0; iy <= yMax; iy++) {
            for (int p = 0; p < 3; p++)
                triVals[p] = triCoords[p][1] - iy;
            CellType[] segmentNodes = slice[2][simplexCode(triVals)];
            if (segmentNodes == null || segmentNodes.length < 2)
                continue;

            for (int i = 0; i < 2; i++) {
                CellType is = segmentNodes[i];
                for (int j = 0; j < segmentVertsBaryCoords[i].length; j++)
                    segmentVertsBaryCoords[i][j] = 0;
                if (is.getValue() < 3) {
                    segmentVertsBaryCoords[i][is.getValue()] = 1;
                    segmentVertsLineCoords[i] = triCoords[is.getValue()][0];
                } else {
                    int p0 = ADD_NODES[3][is.getValue()][0];
                    int p1 = ADD_NODES[3][is.getValue()][1];
                    float u = triVals[p1] / (triVals[p1] - triVals[p0]);
                    if (Float.isNaN(u) || u == Float.NEGATIVE_INFINITY || u == Float.POSITIVE_INFINITY)
                        continue yLoop;
                    segmentVertsBaryCoords[i][p0] = u;
                    segmentVertsBaryCoords[i][p1] = 1 - u;
                    segmentVertsLineCoords[i] = u * triCoords[p0][0] + (1 - u) * triCoords[p1][0];
                }
            }

            xMin = segmentVertsLineCoords[0];
            xMax = segmentVertsLineCoords[1];
            if (segmentVertsLineCoords[1] < segmentVertsLineCoords[0]) {
                xMin = segmentVertsLineCoords[1];
                xMax = segmentVertsLineCoords[0];
            }
            if (xMin > dims[0] - 1 || xMax < 0)
                continue;
            int m0 = max((int) xMin, 0);
            if (m0 < xMin)
                m0 += 1;
            float d = 1 / (segmentVertsLineCoords[1] - segmentVertsLineCoords[0]);
            if (d == Float.NaN || d == Float.NEGATIVE_INFINITY || d == Float.POSITIVE_INFINITY)
                continue;
            for (int ix = m0, iData = iy * dims[0] + m0; ix <= min(xMax, dims[0] - 1); ix++, iData++) {
                if (valid.getBoolean(iData))
                    continue;
                float t = d * (segmentVertsLineCoords[1] - ix);
                float[] baryCoords = new float[3];
                for (int i = 0; i < 3; i++)
                    baryCoords[i] = t * segmentVertsBaryCoords[0][i] +
                        (1 - t) * segmentVertsBaryCoords[1][i];
                valid.setByte(iData, (byte) 1);
                result[iData] = new FieldPosition(index, baryCoords[0], nodes, baryCoords);
            }
        }
    }
}
