/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import java.util.Arrays;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;

/**
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * University of Warsaw, ICM
 */
public class IrregularFieldGeometryStatistics {
    
    protected double avgSegmentLength;
    protected double segmentLengthStdDev;
    protected double minSegmentLength;
    protected double maxSegmentLength;
    protected double[] csAvgSegmentLengths;
    protected double[] csSegmentLengthStdDev;
    protected double[] csMinSegmentLengths;
    protected double[] csMaxSegmentLengths;
    
    protected double avgMinimalAngle;
    protected double minimalAngleStdDev;
    protected double minMinimalAngle;
    protected double maxMinimalAngle;
    protected double[] csAvgMinimalAngles;
    protected double[] csMinimalAngleStdDev;
    protected double[] csMinMinimalAngles;
    protected double[] csMaxMinimalAngles;
    
    protected double minGlassmeierIndex;
    protected double minRobertRouxIndex;
    protected double maxGlassmeierIndex;
    protected double maxRobertRouxIndex;
    protected double avgGlassmeierIndex;
    protected double avgRobertRouxIndex;
    protected double GlassmeierIndexStdDev;    
    protected double RobertRouxIndexStdDev;
    protected double[] csMinGlassmeierIndices;
    protected double[] csMinRobertRouxIndices;
    protected double[] csMaxGlassmeierIndices;
    protected double[] csMaxRobertRouxIndices;
    protected double[] csAvgGlassmeierIndices;
    protected double[] csAvgRobertRouxIndices;
    protected double[] csGlassmeierIndicesStdDev;    
    protected double[] csRobertRouxIndicesStdDev;
    
    public final void computeSegmentsStatistics(IrregularField inField) {
        double segmentLengthSum = 0;
        double segmentLengthSqSum = 0;
        float[] coords = inField.getCurrentCoords().getData();
        minSegmentLength = Double.MAX_VALUE;
        maxSegmentLength = 0;
        int nSegments = 0;
        
        csAvgSegmentLengths = new double[inField.getNCellSets()];
        csSegmentLengthStdDev = new double[inField.getNCellSets()];
        csMinSegmentLengths = new double[inField.getNCellSets()];
        csMaxSegmentLengths = new double[inField.getNCellSets()];
        Arrays.fill(csAvgSegmentLengths, 0);
        Arrays.fill(csSegmentLengthStdDev, 0);
        Arrays.fill(csMinSegmentLengths, 0);
        Arrays.fill(csMaxSegmentLengths, 0);
        
        for (int iSet = 0; iSet < inField.getNCellSets(); iSet++) {
            CellSet cs = inField.getCellSet(iSet);
            double csSumLength = 0;
            double csSumSqLength = 0;
            double csMinLength = Double.MAX_VALUE;
            double csMaxLength = 0;
            if (cs.getCellArray(CellType.SEGMENT) == null)
                continue;
            int csNSegments = cs.getCellArray(CellType.SEGMENT).getNCells();
            if (csNSegments < 1)
                continue;
            int[] segments = cs.getCellArray(CellType.SEGMENT).getNodes();
            for (int i = 0; i < segments.length; i += 2) {
                int k = 3 * segments[i];
                int l = 3 * segments[i + 1];
                double len = 0;
                for (int j = 0; j < 3; j++) 
                    len += (coords[k + j] - coords[l + j]) * (coords[k + j] - coords[l + j]);
                csSumSqLength += len;
                len = Math.sqrt(len);
                csSumLength += len;
                if (csMinLength > len)
                    csMinLength = len;
                if (csMaxLength < len)
                    csMaxLength = len;
            }
            nSegments += csNSegments;
            if (nSegments == 0)
                continue;
            double csAvgSegmentLength = csSumLength / csNSegments;
            csAvgSegmentLengths[iSet] = csAvgSegmentLength;
            csSegmentLengthStdDev[iSet] = Math.sqrt(csSumSqLength / csNSegments - 
                                                    csAvgSegmentLength * csAvgSegmentLength);
            csMinSegmentLengths[iSet] = csMinLength;
            csMaxSegmentLengths[iSet] = csMaxLength;
            
            segmentLengthSum += csSumLength;
            segmentLengthSqSum += csSumSqLength;
            if (minSegmentLength > csMinLength)
                minSegmentLength = csMinLength;
            if (maxSegmentLength < csMaxLength)
                maxSegmentLength = csMaxLength;
        }
        if (nSegments == 0)
            return;
        avgSegmentLength = segmentLengthSum / nSegments;
        segmentLengthStdDev = Math.sqrt(segmentLengthSqSum / nSegments - avgSegmentLength * avgSegmentLength);
        
    }

    
    public final void computeTrianglesStatistics(IrregularField inField) {
        double minimalAngleSum = 0;
        double minimalAngleSqSum = 0;
        float[] coords = inField.getCurrentCoords().getData();
        minMinimalAngle = Double.MAX_VALUE;
        maxMinimalAngle = 0;
        int nTriangles = 0;
        
        csAvgMinimalAngles = new double[inField.getNCellSets()];
        csMinimalAngleStdDev = new double[inField.getNCellSets()];
        csMinMinimalAngles = new double[inField.getNCellSets()];
        csMaxMinimalAngles = new double[inField.getNCellSets()];
        Arrays.fill(csAvgMinimalAngles, 0);
        Arrays.fill(csMinimalAngleStdDev, 0);
        Arrays.fill(csMinMinimalAngles, 0);
        Arrays.fill(csMaxMinimalAngles, 0);
        
        for (int iSet = 0; iSet < inField.getNCellSets(); iSet++) {
            CellSet cs = inField.getCellSet(iSet);
            double csSumMinAngle = 0;
            double csSumSqMinAngle = 0;
            double csMinMinAngle = Double.MAX_VALUE;
            double csMaxMinAngle = 0;
            
            int csNTriangles = cs.getCellArray(CellType.TRIANGLE).getNCells();
            if (cs.getCellArray(CellType.TRIANGLE) == null)
                continue;
            if (csNTriangles < 1)
                continue;
            int[] triangles = cs.getCellArray(CellType.TRIANGLE).getNodes();
            for (int i = 0; i < triangles.length; i += 3) {
                int k = 3 * triangles[i];
                int l = 3 * triangles[i + 1];
                int m = 3 * triangles[i + 2];
                double l0 = 0, l1 = 0, l2 = 0;
                for (int j = 0; j < 3; j++) {
                    l2 += (coords[k + j] - coords[l + j]) * (coords[k + j] - coords[l + j]);
                    l1 += (coords[k + j] - coords[m + j]) * (coords[k + j] - coords[m + j]);
                    l0 += (coords[l + j] - coords[m + j]) * (coords[l + j] - coords[m + j]);
                }
                
                double a0 = Math.acos((l1 + l2 - l0) / (2 * Math.sqrt(l1 * l2)));
                double a1 = Math.acos((l0 + l2 - l1) / (2 * Math.sqrt(l0 * l2)));
                double a2 = Math.acos((l1 + l0 - l2) / (2 * Math.sqrt(l1 * l0)));
                double minAngle = a0;
                if (a1 < minAngle) minAngle = a1;
                if (a2 < minAngle) minAngle = a2;
                
                csSumSqMinAngle += minAngle * minAngle;
                csSumMinAngle += minAngle;
                if (csMinMinAngle > minAngle)
                    csMinMinAngle = minAngle;
                if (csMaxMinAngle < minAngle)
                    csMaxMinAngle = minAngle;
            }
            nTriangles += csNTriangles;
            double csAvgMinimalAngle = csSumMinAngle / csNTriangles;
            csAvgMinimalAngles[iSet] = csAvgMinimalAngle;
            csMinimalAngleStdDev[iSet] = Math.sqrt(csSumSqMinAngle / csNTriangles - 
                                                    csAvgMinimalAngle * csAvgMinimalAngle);
            csMinMinimalAngles[iSet] = csMinMinAngle;
            csMaxMinimalAngles[iSet] = csMaxMinAngle;
            
            minimalAngleSum += csSumMinAngle;
            minimalAngleSqSum += csSumSqMinAngle;
            if (minMinimalAngle > csMinMinAngle)
                minMinimalAngle = csMinMinAngle;
            if (maxMinimalAngle < csMaxMinAngle)
                maxMinimalAngle = csMaxMinAngle;
        }
        if (nTriangles == 0)
            return;
        avgMinimalAngle = minimalAngleSum / nTriangles;
        minimalAngleStdDev = Math.sqrt(minimalAngleSqSum / nTriangles - avgMinimalAngle * avgMinimalAngle);
        
    }

    public final void computeTetrasStatistics(IrregularField inField) {
        
        minGlassmeierIndex = Double.MAX_VALUE;
        minRobertRouxIndex = Double.MAX_VALUE;
        maxGlassmeierIndex = 0;
        maxRobertRouxIndex = 0;
        avgGlassmeierIndex = 0;
        avgRobertRouxIndex = 0;
        GlassmeierIndexStdDev = 0;
        RobertRouxIndexStdDev = 0;
        
        double GlassmeierIndexSum = 0;
        double GlassmeierIndexSqSum = 0;
        double RobertRouxIndexSum = 0;
        double RobertRouxIndexSqSum = 0;
        
        float[] coords = inField.getCurrentCoords().getData();
        int nTetras = 0;
        
        
        
        csMinGlassmeierIndices = new double[inField.getNCellSets()];
        csMinRobertRouxIndices = new double[inField.getNCellSets()];
        csMaxGlassmeierIndices = new double[inField.getNCellSets()];
        csMaxRobertRouxIndices = new double[inField.getNCellSets()];
        csAvgGlassmeierIndices = new double[inField.getNCellSets()];
        csAvgRobertRouxIndices = new double[inField.getNCellSets()];
        csGlassmeierIndicesStdDev = new double[inField.getNCellSets()];
        csRobertRouxIndicesStdDev = new double[inField.getNCellSets()];
        Arrays.fill(csMinGlassmeierIndices, Double.MAX_VALUE);
        Arrays.fill(csMinRobertRouxIndices, Double.MAX_VALUE);
        Arrays.fill(csMaxGlassmeierIndices, 0);
        Arrays.fill(csMaxRobertRouxIndices, 0);
        Arrays.fill(csAvgGlassmeierIndices, 0);
        Arrays.fill(csAvgRobertRouxIndices, 0);
        Arrays.fill(csGlassmeierIndicesStdDev, 0);
        Arrays.fill(csRobertRouxIndicesStdDev, 0);
        
        for (int iSet = 0; iSet < inField.getNCellSets(); iSet++) {
            CellSet cs = inField.getCellSet(iSet);
            double csSumGlassmeierIndex = 0;
            double csSumSqGlassmeierIndex = 0;
            double csMinGlassmeierIndex = Double.MAX_VALUE;
            double csMaxGlassmeierIndex = 0;
            double csSumSqRobertRouxIndex = 0;
            double csSumRobertRouxIndex = 0;
            double csMinRobertRouxIndex = Double.MAX_VALUE;
            double csMaxRobertRouxIndex = 0;
            
            int csNTetras = cs.getCellArray(CellType.TETRA).getNCells();
            if (cs.getCellArray(CellType.TETRA) == null)
                continue;
            if (csNTetras < 1)
                continue;
            int[] tetras = cs.getCellArray(CellType.TETRA).getNodes();
            for (int i = 0; i < tetras.length; i += 4) {
                float[] q = tetraQualityIndicators(coords, 
                               tetras[i], tetras[i + 1], tetras[i + 2], tetras[i + 3]);
                
                csSumSqGlassmeierIndex += q[0] * q[0];
                csSumGlassmeierIndex += q[0];
                if (csMinGlassmeierIndex > q[0])
                    csMinGlassmeierIndex = q[0];
                if (csMaxGlassmeierIndex < q[0])
                    csMaxGlassmeierIndex = q[0];
                csSumSqRobertRouxIndex += q[1] * q[1];
                csSumRobertRouxIndex += q[1];
                if (csMinRobertRouxIndex > q[1])
                    csMinRobertRouxIndex = q[1];
                if (csMaxRobertRouxIndex < q[1])
                    csMaxRobertRouxIndex = q[1];
            }
            nTetras += csNTetras;
            double csAvgGlassmeierIndex = csSumGlassmeierIndex / csNTetras;
            csAvgGlassmeierIndices[iSet] = csAvgGlassmeierIndex;
            csGlassmeierIndicesStdDev[iSet] = Math.sqrt(csSumSqGlassmeierIndex /csNTetras  - 
                                                    csAvgGlassmeierIndex * csAvgGlassmeierIndex);
            csMinGlassmeierIndices[iSet] = csMinGlassmeierIndex;
            csMaxGlassmeierIndices[iSet] = csMaxGlassmeierIndex;
            double csAvgRobertRouxIndex = csSumRobertRouxIndex / csNTetras;
            csAvgRobertRouxIndices[iSet] = csAvgRobertRouxIndex;
            csRobertRouxIndicesStdDev[iSet] = Math.sqrt(csSumSqRobertRouxIndex /csNTetras  - 
                                                    csAvgRobertRouxIndex * csAvgRobertRouxIndex);
            csMinRobertRouxIndices[iSet] = csMinRobertRouxIndex;
            csMaxRobertRouxIndices[iSet] = csMaxRobertRouxIndex;
            
            GlassmeierIndexSum += csSumGlassmeierIndex;
            GlassmeierIndexSqSum += csSumSqGlassmeierIndex;
            if (minGlassmeierIndex > csMinGlassmeierIndex)
                minGlassmeierIndex = csMinGlassmeierIndex;
            if (maxGlassmeierIndex < csMaxGlassmeierIndex)
                maxGlassmeierIndex = csMaxGlassmeierIndex;
            
            RobertRouxIndexSum += csSumRobertRouxIndex;
            RobertRouxIndexSqSum += csSumSqRobertRouxIndex;
            if (minRobertRouxIndex > csMinRobertRouxIndex)
                minRobertRouxIndex = csMinRobertRouxIndex;
            if (maxRobertRouxIndex < csMaxRobertRouxIndex)
                maxRobertRouxIndex = csMaxRobertRouxIndex;
        }
        if (nTetras == 0)
            return;
        avgGlassmeierIndex = GlassmeierIndexSum / nTetras;
        GlassmeierIndexStdDev = Math.sqrt(GlassmeierIndexSqSum / nTetras - avgGlassmeierIndex * avgGlassmeierIndex);
        avgRobertRouxIndex = RobertRouxIndexSum / nTetras;
        RobertRouxIndexStdDev = Math.sqrt(RobertRouxIndexSqSum / nTetras - avgRobertRouxIndex * avgRobertRouxIndex);
    }

    public double getAvgSegmentLength()
    {
        return avgSegmentLength;
    }

    public double getSegmentLengthStdDev()
    {
        return segmentLengthStdDev;
    }

    public double getMinSegmentLength()
    {
        return minSegmentLength;
    }

    public double getMaxSegmentLength()
    {
        return maxSegmentLength;
    }

    public double[] getCsAvgSegmentLengths()
    {
        return csAvgSegmentLengths;
    }

    public double[] getCsSegmentLengthStdDev()
    {
        return csSegmentLengthStdDev;
    }

    public double[] getCsMinSegmentLengths()
    {
        return csMinSegmentLengths;
    }

    public double[] getCsMaxSegmentLengths()
    {
        return csMaxSegmentLengths;
    }

    public double getAvgMinimalAngle()
    {
        return avgMinimalAngle;
    }

    public double getMinimalAngleStdDev()
    {
        return minimalAngleStdDev;
    }

    public double getMinMinimalAngle()
    {
        return minMinimalAngle;
    }

    public double getMaxMinimalAngle()
    {
        return maxMinimalAngle;
    }

    public double[] getCsAvgMinimalAngles()
    {
        return csAvgMinimalAngles;
    }

    public double[] getCsMinimalAngleStdDev()
    {
        return csMinimalAngleStdDev;
    }

    public double[] getCsMinMinimalAngles()
    {
        return csMinMinimalAngles;
    }

    public double[] getCsMaxMinimalAngles()
    {
        return csMaxMinimalAngles;
    }

    public double getMinGlassmeierIndex()
    {
        return minGlassmeierIndex;
    }

    public double getMinRobertRouxIndex()
    {
        return minRobertRouxIndex;
    }

    public double getMaxGlassmeierIndex()
    {
        return maxGlassmeierIndex;
    }

    public double getMaxRobertRouxIndex()
    {
        return maxRobertRouxIndex;
    }

    public double getAvgGlassmeierIndex()
    {
        return avgGlassmeierIndex;
    }

    public double getAvgRobertRouxIndex()
    {
        return avgRobertRouxIndex;
    }

    public double getGlassmeierIndexStdDev()
    {
        return GlassmeierIndexStdDev;
    }

    public double getRobertRouxIndexStdDev()
    {
        return RobertRouxIndexStdDev;
    }

    public double[] getCsMinGlassmeierIndices()
    {
        return csMinGlassmeierIndices;
    }

    public double[] getCsMinRobertRouxIndices()
    {
        return csMinRobertRouxIndices;
    }

    public double[] getCsMaxGlassmeierIndices()
    {
        return csMaxGlassmeierIndices;
    }

    public double[] getCsMaxRobertRouxIndices()
    {
        return csMaxRobertRouxIndices;
    }

    public double[] getCsAvgGlassmeierIndices()
    {
        return csAvgGlassmeierIndices;
    }

    public double[] getCsAvgRobertRouxIndices()
    {
        return csAvgRobertRouxIndices;
    }

    public double[] getCsGlassmeierIndicesStdDev()
    {
        return csGlassmeierIndicesStdDev;
    }

    public double[] getCsRobertRouxIndicesStdDev()
    {
        return csRobertRouxIndicesStdDev;
    }
    
    
    public static float[] tetraQualityIndicators(float[] coords, int i0, int i1, int i2, int i3)
    {
        float[] q = new float[2];
        float[][] d = new float[3][3];
        float[][] c = new float[3][3];
        double[] lv = new double[3];
        for (int i = 0; i < 3; i++) {
            float w = coords[3 * i0 + i];
            d[0][i] = coords[3 * i1 + i] - w;
            d[1][i] = coords[3 * i2 + i] - w;
            d[2][i] = coords[3 * i3 + i] - w;
        }
        double l = 0;
        for (int i = 0; i < 3; i++)
            l += Math.sqrt(d[i][0] * d[i][0] + d[i][1] * d[i][1] + d[i][2] * d[i][2]);
        for (int i = 0; i < 3; i++) {
            int k1 = (i + 1) % 3;
            int k2 = (i + 2) % 3;
            float[] d1 = d[k1];
            float[] d2 = d[k2];
            lv[i] = Math.sqrt((d1[0] - d2[0]) * (d1[0] - d2[0]) +
                              (d1[1] - d2[1]) * (d1[1] - d2[1]) + 
                              (d1[2] - d2[2]) * (d1[2] - d2[2]));
            l += lv[i];
            c[i][0] = d1[1] * d2[2] - d1[2] * d2[1];
            c[i][1] = d1[2] * d2[0] - d1[0] * d2[2];
            c[i][2] = d1[0] * d2[1] - d1[1] * d2[0];
        }
        double avgL = l / 6;
        double s = 0;
        for (int i = 0; i < 3; i++)
            s += .5 * Math.sqrt(c[i][0] * c[i][0] + c[i][1] * c[i][1] + c[i][2] * c[i][2]);
        double s0 = 0;
        for (int i = 0; i < 3; i++) 
            s0 += (c[0][i] + c[1][i] + c[2][i]) * (c[0][i] + c[1][i] + c[2][i]);
        s += Math.sqrt(s0) / 2;
        double vol = Math.abs(c[0][0] * d[0][0] + c[0][1] * d[0][1] + c[0][2] * d[0][2]) / 6;
        double vmean = Math.sqrt(2.) * avgL * avgL * avgL / 12;
        double smean = Math.sqrt(3.) * avgL * avgL;
//        System.out.printf("%8.3f %8.3f %8.3f %8.3f%n", vol, vmean, s, smean);
//      Glassmeier factor        
        q[0] = (float)(vol / vmean + s / smean + 1);
        
//     Calculate radius of the circumscribed sphere   
        if (vol < 100 * Double.MIN_VALUE)
            q[1] = 0;
        else {
            double w = 1 / (12 * vol);
            double[] v = new double[3];
            v[0] = w * (c[0][0] * lv[0] * lv[0] +
                        c[1][0] * lv[1] * lv[1] +
                        c[2][0] * lv[2] * lv[2]);
            v[1] = w * (c[0][1] * lv[0] * lv[0] +
                        c[1][1] * lv[1] * lv[1] +
                        c[2][1] * lv[2] * lv[2]);
            v[2] = w * (c[0][2] * lv[0] * lv[0] +
                        c[1][2] * lv[1] * lv[1] +
                        c[2][2] * lv[2] * lv[2]); 
            double rc = Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
    //     Calculate the Robert/Roux factor
            w = 9 * Math.sqrt(3) / 8;
            w = Math.pow(w * vol, 1./3) / rc;
            q[1] = (float)w;
        }
        
        return q;
    }
 
    
    public static final void main(String[] args)
    {
        float[] c0 = {0,0,0, 1,1,0, 1,0,1, 0,1,1};
        float[] c1 = {0,0,0, 1,0,0, 0,0,1, 0,1,0};
        float[] c2 = {0,0,0, 1,0,0, 1,1,0, 0,1,0.0001f};
        float[] c3 = {0,0,0, 1,0,0, 1,1,0, 0,1,0};
        float[] c4 = {0,0,0, 1,0,0, 1,0,1, 1,1,1};
        float[] c5 = {0,0,0, 0,1,1, 1,0,1, 1,1,1};
        System.out.println("" + tetraQualityIndicators(c0, 0, 1, 2, 3)[0] + "   " +
                                tetraQualityIndicators(c0, 0, 1, 2, 3)[1]);
        System.out.println("" + tetraQualityIndicators(c1, 0, 1, 2, 3)[0] + "   " +
                                tetraQualityIndicators(c1, 0, 1, 2, 3)[1]);
        System.out.println("" + tetraQualityIndicators(c2, 0, 1, 2, 3)[0] + "   " +
                                tetraQualityIndicators(c2, 0, 1, 2, 3)[1]);
        System.out.println("" + tetraQualityIndicators(c3, 0, 1, 2, 3)[0] + "   " +
                                tetraQualityIndicators(c3, 0, 1, 2, 3)[1]);
        System.out.println("" + tetraQualityIndicators(c4, 0, 1, 2, 3)[0] + "   " +
                                tetraQualityIndicators(c4, 0, 1, 2, 3)[1]);
        System.out.println("" + tetraQualityIndicators(c5, 0, 1, 2, 3)[0] + "   " +
                                tetraQualityIndicators(c5, 0, 1, 2, 3)[1]);
    }
    
}
