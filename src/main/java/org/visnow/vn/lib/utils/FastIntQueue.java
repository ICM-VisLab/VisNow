/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class FastIntQueue
{

    private static final int INIT_LEN = 64;

    private int length = 0, first = 0, last = -1, arrLength = INIT_LEN;
    private int[] queue = new int[INIT_LEN];
    
    private int stepMark = 0;

    public FastIntQueue()
    {

    }

    public synchronized final void reset()
    {
        queue = new int[INIT_LEN];
        arrLength = INIT_LEN;
        length = 0;
        first = 0;
        last = -1;
    }

    public synchronized boolean isEmpty()
    {
        return length <= 0;
    }

    @Override
    public String toString()
    {
        StringBuilder b = new StringBuilder();
        b.append(String.format("   %3d %3d: ", first, last));
        for (int i = 0; i < length; i++) {
            int j = (first + i) % queue.length;
            b.append(String.format("%3d ", queue[j]));
        }
        return b.toString();
    }
    
    public synchronized void push(int val)
    {
        insert(val);
    }

    public synchronized void insert(int val)
    {
        length += 1;
        if (length > arrLength) {
            arrLength *= 2;
            int[] tmp = new int[arrLength];
            System.arraycopy(queue, first, tmp, 0, queue.length - first);
            if (first > 0)
                System.arraycopy(queue, 0, tmp, queue.length - first, first);
            queue = tmp;
            first = 0;
            last = length - 1;
        } else
            last = (last + 1) % arrLength;
        queue[last] = val;
    }
    
    public synchronized void init(int[] vals)
    {
        if (vals.length < INIT_LEN / 2)
            reset();
        else {
            arrLength = 2 * vals.length;
            queue = new int[arrLength];
        }
        
        System.arraycopy(vals, 0, queue, 0, vals.length);
        length = vals.length;
        first = 0;
        last = vals.length - 1;
        stepMark = vals.length - 1;
    }

    public synchronized int get() throws IndexOutOfBoundsException
    {
        if (length == 0)
            throw new IndexOutOfBoundsException();
        length -= 1;
        int val = queue[first];
        first = (first + 1) % arrLength;
        return val;
    }

    public int setStepMark()
    {
        this.stepMark = last;
        if (stepMark > first)
            return stepMark - first;
        else
            return stepMark - first + arrLength;
    }
    
    public int size()
    {
        return length;
    }

}
