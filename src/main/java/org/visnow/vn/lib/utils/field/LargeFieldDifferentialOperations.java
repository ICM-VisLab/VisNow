/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static java.lang.Math.*;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import static org.visnow.jscic.dataarrays.DataArrayType.*;
import org.visnow.vn.engine.core.ProgressAgent;
import org.visnow.vn.lib.basic.filters.DifferentialOperations.DifferentialOperationsShared.*;
import static org.visnow.vn.lib.basic.filters.DifferentialOperations.DifferentialOperationsShared.ScalarOperation.*;
import static org.visnow.vn.lib.basic.filters.DifferentialOperations.DifferentialOperationsShared.VectorOperation.*;
import static org.visnow.vn.lib.basic.filters.DifferentialOperations.DifferentialOperationsShared.TimeOperation.*;
import static org.visnow.vn.lib.utils.numeric.FiniteDifferences.Derivatives.*;
import org.visnow.vn.lib.utils.numeric.FiniteDifferences.InvertedJacobian;
import org.visnow.vn.lib.utils.numeric.PointwiseLinearAlgebra2D;
import org.visnow.vn.lib.utils.numeric.PointwiseLinearAlgebra3D;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class LargeFieldDifferentialOperations
{

    private static final String[] COORD_NAMES = {"x", "y", "z"};

    private static void computeD_Dt(float[] times, LargeArray[] f, FloatLargeArray[] df_dt)
    {
        int nTimesteps = f.length;
        for (int timestep = 0; timestep < nTimesteps; timestep++) {
            int tstep0 = timestep;
            int tstep1 = timestep > 0 ? timestep - 1: timestep + 2;
            int tstep2 = timestep < nTimesteps - 1 ? timestep + 1 : timestep - 2;
            float t0 = times[tstep0];
            float t1 = times[tstep1];
            float t2 = times[tstep2];
            LargeArray v0 = f[tstep0];
            LargeArray v1 = f[tstep1];
            LargeArray v2 = f[tstep2];
            LargeArray dv = df_dt[timestep];
            float a1 = 1 / ((t2 - t1) * (t1 - t0));
            float a2 = 1 / ((t2 - t1) * (t2 - t0));
            float b1 = (t2 + t0) / ((t1 - t2) * (t1 - t0));
            float b2 = (t1 + t0) / ((t1 - t2) * (t2 - t0));
            for (long j = 0; j < dv.length(); j++) {
                float u1 = v1.getFloat(j) - v0.getFloat(j);
                float u2 = v2.getFloat(j) - v0.getFloat(j);
                float a = u2 * a2 - u1 * a1;
                float b = u2 * b2 - u1 * b1;
                dv.setFloat(j, 2 * a * t0 + b);
            }
        }
    }

    public static Field computeDifferentialOperators(Field inField,
            HashMap<String, ScalarOperation[]> scalarOps,
            HashMap<String, VectorOperation[]> vectorOps,
            HashMap<String, TimeOperation[]> timeOps,
            boolean computeFully, ProgressAgent progressAgent, int nThreads)
    {
        Field outField = inField.cloneShallow();
        long nData = inField.getNNodes();
        String tUnit = inField.getTimeUnit();
        String lUnit = inField.getCoordsUnit(0);
        String dUnit = "", d2Unit = "";
        if (lUnit.isEmpty() || lUnit.equals("1"))
            lUnit = "";
        for (int i = 1; i < 3; i++) {
            String s = inField.getCoordsUnit(i);
            if (s != null && !s.isEmpty() && !s.equals("1") && !s.equals(lUnit))
                lUnit = "";
        }
        if (inField instanceof RegularField) {
            RegularField regInField = (RegularField)inField;
            int[]dims = regInField.getDims();
            int dim = dims.length;
            FloatLargeArray invJacobian = InvertedJacobian.computeLargeInvertedJacobian(VisNow.availableProcessors(), regInField);
            if (scalarOps != null) {
                for (Map.Entry<String, ScalarOperation[]> entry : scalarOps.entrySet()) {
                    DataArray outGrad = null, outGradNorm = null, outNormalizedGradient = null,
                            outHessian = null, outLaplacian = null;
                    DataArray[] outGradCmp       = new DataArray[dim];
                    DataArray[] outHessianEigval = new DataArray[dim];
                    DataArray[] outHessianEigvec = new DataArray[dim];
                    List<ScalarOperation> scalarOperations = Arrays.asList((ScalarOperation[]) entry.getValue());
                    if (scalarOperations.size() > 0) {
                        DataArray da = regInField.getComponent(entry.getKey());
                        String unit = da.getUnit();
                        if (!lUnit.isEmpty() && !unit.isEmpty() && !unit.equals("1")) {
                            dUnit  = unit + "/" + lUnit;
                            d2Unit = unit + "/" + lUnit + "^2";
                        }
                        if (scalarOperations.contains(GRADIENT))
                            outGrad = DataArray.create(FIELD_DATA_FLOAT, nData, dim,
                                    da.getName() + "_grad", dUnit,
                                    da.getUserData());
                        if (scalarOperations.contains(GRADIENT_NORM))
                            outGradNorm = DataArray.create(FIELD_DATA_FLOAT, nData, 1,
                                    da.getName() + "_gradNorm", dUnit,
                                    da.getUserData());
                        if (scalarOperations.contains(NORMALIZED_GRADIENT))
                            outNormalizedGradient = DataArray.create(FIELD_DATA_FLOAT, nData, dim,
                                    da.getName() + "_normGrad", "",
                                    da.getUserData());
                        if (scalarOperations.contains(GRADIENT_COMPONENTS))
                            for (int coord = 0; coord < dim; coord++)
                                outGradCmp[coord] = DataArray.create(FIELD_DATA_FLOAT, nData, 1,
                                        da.getName() + "_d/d" + COORD_NAMES[coord], dUnit,
                                        da.getUserData());
                        if (scalarOperations.contains(LAPLACIAN))
                            outLaplacian = DataArray.create(FIELD_DATA_FLOAT, nData, 1,
                                    da.getName() + "_lap", d2Unit,
                                    da.getUserData());
                        if (scalarOperations.contains(HESSIAN))
                            outHessian = DataArray.create(FIELD_DATA_FLOAT, nData,
                                    (dim * (dim + 1)) / 2, da.getName() + "_hessian",
                                    d2Unit, da.getUserData());
                        if (scalarOperations.contains(HESSIAN_EIGEN))
                            for (int coord = 0; coord < dim; coord++) {
                                outHessianEigval[coord] = DataArray.create(FIELD_DATA_FLOAT, nData, 1,
                                        da.getName() + "_eigval" + COORD_NAMES[coord], d2Unit,
                                        da.getUserData());
                                outHessianEigvec[coord] = DataArray.create(FIELD_DATA_FLOAT, nData, dim,
                                        da.getName() + "_eigvec" + COORD_NAMES[coord], "",
                                        da.getUserData());
                            }

                        int nTimesteps = computeFully ? da.getNFrames() : 1;
                        for (int timestep = 0; timestep < nTimesteps; timestep++) {
                            float time = da.getTime(timestep);
                            LargeArray fda = computeFully ? da.getRawArray(time): da.getRawArray();

                            //first derivative must be computed and stored
                            FloatLargeArray grad = computeLargeDerivatives(nThreads, regInField.getDims(), fda, invJacobian);
                            if (progressAgent != null)
                                progressAgent.increase();
                            if (outGrad != null)
                                outGrad.addRawArray(grad, time);

                            if (outGradNorm != null) {
                                FloatLargeArray gnorm = new FloatLargeArray(nData);
                                for (long k = 0, l = 0; k < nData; k++) {
                                    double s = 0;
                                    for (int m = 0; m < dim; m++, l++)
                                        s += grad.getFloat(l) * grad.getFloat(l);
                                    gnorm.setFloat(k,(float) sqrt(s));
                                }
                                outGradNorm.addRawArray(gnorm, time);
                                if (progressAgent != null)
                                    progressAgent.increase();
                            }

                            if (outNormalizedGradient != null) {
                                FloatLargeArray normg = new FloatLargeArray(dim * nData);
                                for (long k = 0, l = 0, n = 0; k < nData; k++) {
                                    float s = 0;
                                    for (int m = 0; m < dim; m++, l++)
                                        s += grad.getFloat(l) * grad.getFloat(l);
                                    if (s == 0)
                                        for (int m = 0; m < dim; m++, n++)
                                            normg.setFloat(n, 0);
                                    else {
                                        s = 1 / (float) sqrt(s);
                                        for (int m = 0; m < dim; m++, n++)
                                            normg.setFloat(n, grad.getFloat(n) * s);
                                    }
                                }
                                outNormalizedGradient.addRawArray(normg, time);
                                if (progressAgent != null)
                                    progressAgent.increase();
                            }

                            if (scalarOperations.contains(GRADIENT_COMPONENTS))
                                for (int coord = 0; coord < dim; coord++) {
                                    FloatLargeArray cmp = new FloatLargeArray(nData);
                                    for (long k = 0, l = coord; k < nData; k++, l += dim)
                                        cmp.setFloat(k, grad.getFloat(l));
                                    if (outGradCmp[coord] != null)
                                        outGradCmp[coord].addRawArray(cmp, time);
                                    if (progressAgent != null)
                                        progressAgent.increase();
                                }

                            //second derivatives
                            if (scalarOperations.contains(LAPLACIAN) ||
                                scalarOperations.contains(HESSIAN) ||
                                scalarOperations.contains(HESSIAN_EIGEN)) {
                                FloatLargeArray d2 = computeLargeDerivatives(nThreads, regInField.getDims(), grad, invJacobian);
                                FloatLargeArray h  = symmetrize(dim, d2);

                                if (outLaplacian != null) {
                                    FloatLargeArray lapl = new FloatLargeArray(nData);
                                    if (dim == 3)
                                        for (long k = 0, l = 0; k < nData; k++, l += 6)
                                            lapl.setFloat(k, h.getFloat(l) + h.getFloat(l + 3) + h.getFloat(l + 5));
                                    else if (dim == 2)
                                        for (long k = 0, l = 0; k < nData; k++, l += 3)
                                            lapl.setFloat(k, h.getFloat(l) + h.getFloat(l + 2));
                                    outLaplacian.addRawArray(lapl, time);
                                    if (progressAgent != null)
                                        progressAgent.increase();
                                }
                                if (outHessian != null) {
                                    outHessian.addRawArray(h, time);
                                    if (progressAgent != null)
                                        progressAgent.increase();
                                }
                                if (scalarOperations.contains(HESSIAN_EIGEN)) {
                                    FloatLargeArray[] hEigV = new FloatLargeArray[dim];
                                    FloatLargeArray[] hEigR = new FloatLargeArray[dim];
                                    for (int i = 0; i < dim; i++) {
                                        hEigV[i] = new FloatLargeArray(nData);
                                        hEigR[i] = new FloatLargeArray(nData * dim);
                                    }
                                    if (dim == 3)
                                        PointwiseLinearAlgebra3D.symEigen(nThreads, h, hEigV, hEigR);
                                    else
                                        PointwiseLinearAlgebra2D.symEigen(nThreads, h, hEigV, hEigR);
                                    for (int coord = 0; coord < dim; coord++) {
                                        if (outHessianEigval[coord] != null) {
                                            outHessianEigval[coord].addRawArray(hEigV[coord], time);
                                            outHessianEigvec[coord].addRawArray(hEigR[coord], time);
                                        }
                                        if (progressAgent != null)
                                            progressAgent.increase();
                                    }
                                }
                            }
                        }

                        if (outGrad != null)
                            outField.addComponent(outGrad);
                        if (outGradNorm != null)
                            outField.addComponent(outGradNorm);
                        if (outNormalizedGradient != null)
                            outField.addComponent(outNormalizedGradient);
                        if (scalarOperations.contains(GRADIENT_COMPONENTS))
                            for (int coord = 0; coord < dim; coord++)
                                outField.addComponent(outGradCmp[coord]);
                        if (outLaplacian != null)
                            outField.addComponent(outLaplacian);
                        if (outHessian != null)
                            outField.addComponent(outHessian);
                        if (scalarOperations.contains(HESSIAN_EIGEN))
                            for (int coord = 0; coord < dim; coord++) {
                                outField.addComponent(outHessianEigval[coord]);
                                outField.addComponent(outHessianEigvec[coord]);
                            }
                    }
                }
            }

            if (vectorOps != null)
                for (Map.Entry<String, VectorOperation[]> entry : vectorOps.entrySet()) {
                    DataArray outDiv = null, outRot = null;
                    DataArray[] outDerivCmp = new DataArray[dim];
                    List<VectorOperation> vectorOperations = Arrays.asList((VectorOperation[]) entry.getValue());
                    if (vectorOperations.size() > 0) {
                        DataArray da = regInField.getComponent(entry.getKey());
                        String unit = da.getUnit();
                        dUnit = "";
                        if (!lUnit.isEmpty() && !unit.isEmpty() && !unit.equals("1")) {
                            dUnit = unit + "/" + lUnit;
                            dUnit = unit + "/" + lUnit + "^2";
                        }
                        int vLen = da.getVectorLength();
                        if (vLen != dim)
                            continue;
                        if (vectorOperations.contains(DERIV))
                            for (int coord = 0; coord < dim; coord++)
                                outDerivCmp[coord] = DataArray.create(FIELD_DATA_FLOAT, nData, dim,
                                        da.getName() + "_d" + coord, dUnit,
                                        da.getUserData());
                        if (vectorOperations.contains(DIV))
                            outDiv = DataArray.create(FIELD_DATA_FLOAT, nData, 1,
                                    da.getName() + "_div", dUnit,
                                    da.getUserData());
                        if (vectorOperations.contains(ROT))
                            outRot = DataArray.create(FIELD_DATA_FLOAT, nData, dim == 3 ? 3 : 1,
                                    da.getName() + "_rot", dUnit,
                                    da.getUserData());

                        int nTimesteps = computeFully ? da.getNFrames() : 1;
                        for (int timestep = 0; timestep < nTimesteps; timestep++) {
                            float time = da.getTime(timestep);
                            LargeArray fda = computeFully ? da.getRawArray(time) : da.getRawArray();
                            FloatLargeArray h = computeLargeDerivatives(nThreads, regInField.getDims(), fda, invJacobian);
                            if (vectorOperations.contains(DERIV)) {
                                for (int k = 0; k < dim; k++) {
                                    FloatLargeArray dta = new FloatLargeArray(dim * nData);
                                    for (int l = 0, m = 0; l < nData; l++)
                                        for (long p = 0, n = dim * (dim * l + k); p < dim; p++, m++, n++)
                                            dta.setFloat(m, h.getFloat(n));
                                    outDerivCmp[k].addRawArray(dta, time);
                                }
                                if (progressAgent != null)
                                    progressAgent.increase();
                            }
                            if (outDiv != null) {
                                FloatLargeArray div = new FloatLargeArray(nData);
                                for (long k = 0, l = 0; k < nData; k++, l += dim * dim) {
                                    float s = 0;
                                    for (int m = 0; m < dim; m++)
                                        s += h.getFloat(l + m * (dim + 1));
                                    div.setFloat(k, s);
                                }
                                outDiv.addRawArray(div, time);
                                if (progressAgent != null)
                                    progressAgent.increase();
                            }
                            if (outRot != null) {
                                FloatLargeArray rot;
                                if (dim == 3) {
                                    rot = new FloatLargeArray(dim * nData);
                                    for (long k = 0, l = 0, n = 0; k < nData; k++, l += 3, n += 9) {
                                        rot.setFloat(l,     h.getFloat(n + 7) - h.getFloat(n + 5));
                                        rot.setFloat(l + 1, h.getFloat(n + 2) - h.getFloat(n + 6));
                                        rot.setFloat(l + 2, h.getFloat(n + 3) - h.getFloat(n + 1));
                                    }
                                } else {
                                    rot = new FloatLargeArray(nData);
                                    for (long k = 0, n = 0; k < nData; k++, n += 4)
                                        rot.setFloat(k, h.getFloat(n + 2) - h.getFloat(n + 1));
                                }
                                outRot.addRawArray(rot, time);
                                if (progressAgent != null)
                                    progressAgent.increase();
                            }
                        }

                        if (vectorOperations.contains(DERIV))
                            for (int coord = 0; coord < dim; coord++)
                                outField.addComponent(outDerivCmp[coord]);
                        if (vectorOperations.contains(DIV))
                            outField.addComponent(outDiv);
                        if (vectorOperations.contains(ROT))
                            outField.addComponent(outRot);
                    }
                }
        }

        if (timeOps != null) {
            for (Map.Entry<String, TimeOperation[]> entry : timeOps.entrySet()) {
                List<TimeOperation> timeOperations = Arrays.asList((TimeOperation[]) entry.getValue());
                String name = entry.getKey();
                int vLen, nTimesteps;
                TimeData td;
                dUnit = d2Unit = "";
                if (timeOperations.size() > 0) {
                    if (name.equals("coords")) {
                        td = inField.getCoords();
                        if (!lUnit.isEmpty() && !tUnit.isEmpty()) {
                            dUnit  = lUnit + "/" + tUnit;
                            d2Unit = lUnit + "/" + tUnit + "^2";
                        }
                        vLen = 3;
                        nTimesteps = td.getNSteps();
                    }
                    else {
                        DataArray da = inField.getComponent(entry.getKey());
                        String unit = da.getUnit();
                        if (!unit.isEmpty() && !tUnit.isEmpty()) {
                            dUnit  = unit + "/" + tUnit;
                            d2Unit = unit + "/" + tUnit + "^2";
                        }
                        td = da.getTimeData();
                        vLen = da.getVectorLength();
                        nTimesteps = da.getNFrames();
                    }

                    if (nTimesteps < 2)
                        continue;
                    //first derivative must be computed and stored
                    LargeArray[] dIn  = new LargeArray[nTimesteps];
                    float[] times  = td.getTimesAsArray();
                    for (int i = 0; i < nTimesteps; i++) {
                        float time = times[i];
                        dIn[i] = td.getValue(time);
                    }
                    FloatLargeArray[] dOut = new FloatLargeArray[nTimesteps];
                    for (int i = 0; i < dOut.length; i++)
                        dOut[i] = new FloatLargeArray(nData * vLen);
                    if (times.length > 2) // if three or more time moments are available, quadratic interpolation is used
                        computeD_Dt(times, dIn, dOut);
                    else {
                        float dt = times[1] - times[0];
                        for (long i = 0; i < dOut[0].length(); i++) {
                            float d = (dIn[1].getFloat(i) - dIn[0].getFloat(i)) / dt;
                            dOut[0].setFloat(i, d);
                            dOut[1].setFloat(i, d);
                        }
                    }
                    if (timeOperations.contains(D_DT)) {
                        DataArray d_dt = DataArray.create(FIELD_DATA_FLOAT, nData, vLen,
                                "d_" + name + "_dt", dUnit, new String[] {});
                        for (int i = 0; i < dOut.length; i++)
                            d_dt.addRawArray(dOut[i], times[i]);
                        outField.addComponent(d_dt);
                    }
                    if (timeOperations.contains(D2_DT2)) {
                        DataArray d2_dt2 = DataArray.create(FIELD_DATA_FLOAT, nData, vLen,
                                "d2_" + name + "_dt2", d2Unit, new String[] {});
                        FloatLargeArray[] dOut2 = new FloatLargeArray[nTimesteps];
                        for (int i = 0; i < dOut2.length; i++)
                            dOut2[i] = new FloatLargeArray(nData * vLen);
                        if (times.length > 2)
                            computeD_Dt(times, dOut, dOut2);
                        else {
                            float dt = times[1] - times[0];
                            for (long i = 0; i < dOut[0].length(); i++) {
                                float d = (dOut[1].getFloat(i) - dOut[0].getFloat(i)) / dt;
                                dOut2[0].setFloat(i, d);
                                dOut2[1].setFloat(i, d);
                            }
                        }
                        for (int i = 0; i < dOut2.length; i++)
                            d2_dt2.addRawArray(dOut2[i], times[i]);
                        outField.addComponent(d2_dt2);
                    }
                }
            }
        }
        return outField;
    }

}
