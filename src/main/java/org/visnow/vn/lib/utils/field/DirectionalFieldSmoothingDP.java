/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class DirectionalFieldSmoothingDP implements Runnable
{
    private final int[] dims;
    private final int[] outDims;
    private final int down = 1;
    private final double[] inData;
    private final double[] outData;
    private final double[] kernel;
    private final double[] kernelDistribution;
    private final int vlen;
    private int direction = 0;
    private int nThreads = 1;
    private int iThread = 0;
    private double[] inSlice;
    private double[] outSlice;

    public DirectionalFieldSmoothingDP(int direction, int[] dims, double[] data, double[] kernel, double[] kernelDistribution, int nThreads, int iThread)
    {
        this.dims = dims;
        this.inData = this.outData = data;
        this.kernel = kernel;
        this.kernelDistribution = kernelDistribution;
        this.direction = direction;
        this.nThreads = nThreads;
        this.iThread = iThread;
        int nData = dims[0];
        for (int i = 1; i < dims.length; i++)
            nData *= dims[i];
        vlen = data.length / nData;
        outDims = dims;
    }

    private void smooth(int down)
    {
        int radius = kernel.length / 2;
        int n = inSlice.length / vlen;
        for (int m = 0; m < vlen; m++)
            for (int i = 0, ii = 0; i < n; i += down, ii++) {
                double s = 0;
                int radiusLower = Math.min(i, radius);
                int radiusUpper = Math.min(n - 1 - i, radius);
                int kernelPosition = radius - radiusLower;

                for (int j = i - radiusLower; j <= i + radiusUpper; j++)
                    s += inSlice[j * vlen + m] * kernel[kernelPosition++];
                if (radiusLower == radius)
                    outSlice[ii * vlen + m] = s / (kernelDistribution[radius + radiusUpper]);
                else outSlice[ii * vlen + m] = s / (kernelDistribution[radius + radiusUpper] - kernelDistribution[radius - radiusLower - 1]);
            }
    }

    @Override
    public void run()
    {
        int nDims = dims.length;
        int start, step;
        int nSlices = 1;

        switch (direction) {
            case 0:
                inSlice = new double[dims[0] * vlen];
                outSlice = new double[outDims[0] * vlen];
                if (nDims > 1)
                    nSlices *= dims[1];
                if (nDims > 2)
                    nSlices *= dims[2];
                for (int i = iThread; i < nSlices; i += nThreads) {
                    System.arraycopy(inData, i * dims[0] * vlen, inSlice, 0, inSlice.length);
                    smooth(down);
                    System.arraycopy(outSlice, 0, outData, i * outDims[0] * vlen, outSlice.length);
                }
                break;
            case 1:
                step = dims[0];
                inSlice = new double[dims[1] * vlen];
                outSlice = new double[outDims[1] * vlen];
                nSlices = dims[0];
                if (nDims == 3)
                    nSlices *= dims[2];
                for (int i = iThread; i < nSlices; i += nThreads) {
                    int p = i / dims[0];
                    int q = i % dims[0];
                    if (nDims == 3)
                        start = p * dims[0] * dims[1] + q;
                    else
                        start = i;
                    for (int j = 0, l = 0; j < dims[1]; j++)
                        for (int k = 0; k < vlen; k++, l++)
                            inSlice[l] = inData[(start + j * step) * vlen + k];
                    smooth(down);
                    if (nDims == 3)
                        start = p * dims[0] * outDims[1] + q;
                    for (int j = 0, l = 0; j < outDims[1]; j++)
                        for (int k = 0; k < vlen; k++, l++)
                            try {
                                outData[(start + j * step) * vlen + k] = outSlice[l];
                            } catch (Exception e) {
                                System.out.println("1 " + start + " " + j + " " + step + " " + (start + j * step) + " " + (start + j * step) * vlen + k);
                            }
                }
                break;
            case 2:
                step = dims[0] * dims[1];
                inSlice = new double[dims[2] * vlen];
                outSlice = new double[outDims[2] * vlen];
                nSlices = dims[0] * dims[1];
                for (int i = iThread; i < nSlices; i += nThreads) {
                    if (i >= nSlices)
                        continue;
                    if (iThread == 0)
                        fireStatusChanged((float) i / nSlices);
                    for (int j = 0, l = 0; j < dims[2]; j++)
                        for (int k = 0; k < vlen; k++, l++)
                            inSlice[l] = inData[(i + j * step) * vlen + k];
                    smooth(down);
                    for (int j = 0, l = 0; j < outDims[2]; j++)
                        for (int k = 0; k < vlen; k++, l++)
                            try {
                                outData[(i + j * step) * vlen + k] = outSlice[l];
                            } catch (Exception e) {
                                System.out.println("2 " + i + " " + j + " " + step + " " + (i + j * step) + " " + ((i + j * step) * vlen + k) + " " + l);
                            }

                }
                break;
        }
    }
    private transient FloatValueModificationListener statusListener = null;

    public void addFloatValueModificationListener(FloatValueModificationListener listener)
    {
        if (statusListener == null)
            this.statusListener = listener;
        else
            System.out.println("" + this + ": only one status listener can be added");
    }

    private void fireStatusChanged(float status)
    {
        FloatValueModificationEvent e = new FloatValueModificationEvent(this, status, true);
        if (statusListener != null)
            statusListener.floatValueChanged(e);
    }
}
