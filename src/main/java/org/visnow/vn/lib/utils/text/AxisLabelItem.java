/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.text;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class AxisLabelItem
{

    /**
     * Creates a new instance of AxisLabelItem
     */
    public AxisLabelItem(float pos, String l)
    {
        position = pos;
        label = l;
    }

    /**
     * Holds value of property position.
     */
    private float position;

    /**
     * Getter for property position.
     * <p>
     * @return Value of property position.
     */
    public float getPosition()
    {

        return this.position;
    }

    /**
     * Setter for property position.
     * <p>
     * @param position New value of property position.
     */
    public void setPosition(float position)
    {

        this.position = position;
    }

    /**
     * Holds value of property label.
     */
    private String label;

    /**
     * Getter for property label.
     * <p>
     * @return Value of property label.
     */
    public String getLabel()
    {

        return this.label;
    }

    /**
     * Setter for property label.
     * <p>
     * @param label New value of property label.
     */
    public void setLabel(String label)
    {

        this.label = label;
    }

}
