/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field.subset;

import org.visnow.jlargearrays.LargeArray;
import org.visnow.jscic.dataarrays.DataArray;

/**
 *
 * @author know
 */


public class ThresholdSelector extends NodeSelector
{
    private final LargeArray thresholdArray;
    private final int vLen;
    private final float thrLow, thrUp;
    private final boolean inside;

    public ThresholdSelector(DataArray thresholdDataArray, float thrLow, float thrUp, boolean inside)
    {
        vLen = thresholdDataArray.getVectorLength();
        thresholdArray = vLen == 1 ? thresholdDataArray.getRawArray() : thresholdDataArray.getVectorNorms();
        this.thrLow = thrLow;
        this.thrUp = thrUp;
        this.inside = inside;
    }
    
    @Override
    public boolean isValid(long i)
    {
        if (thresholdArray == null)
            return true;
        double t = thresholdArray.getFloat(i);
        return inside == (t >= thrLow && t <= thrUp);
    }
}
