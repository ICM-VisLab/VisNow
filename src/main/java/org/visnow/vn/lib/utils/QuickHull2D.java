/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils;
/**
 *
 * @author know
 */
    //This is a java program to find a points in convex hull using quick hull method
    //source: Alexander Hrishov's website
    //URL: http://www.ahristov.com/tutorial/geometry-games/convex-hull.html
     
    import java.util.ArrayList;
    import java.util.Arrays;
     
    public class QuickHull2D
    {
        private QuickHull2D()
        {
        }
        
        public static float[] quickHull(float[] coords)
        {
            ArrayList<double[]> points = new ArrayList<>();
            for (int i = 0; i < coords.length; i += 3)
                points.add(new double[] {coords[i], coords[i + 1]});
            ArrayList<double[]> convexHull = new ArrayList<>();
            if (points.size() < 3)
                return coords;
     
            int minPoint = -1, maxPoint = -1;
            double minX = Integer.MAX_VALUE;
            double maxX = Integer.MIN_VALUE;
            for (int i = 0; i < points.size(); i++)
            {
                if (points.get(i)[0] < minX)
                {
                    minX = points.get(i)[0];
                    minPoint = i;
                }
                if (points.get(i)[0] > maxX)
                {
                    maxX = points.get(i)[0];
                    maxPoint = i;
                }
            }
            double[] A = points.get(minPoint);
            double[] B = points.get(maxPoint);
            convexHull.add(A);
            convexHull.add(B);
            points.remove(A);
            points.remove(B);
     
            ArrayList<double[]> leftSet = new ArrayList<>();
            ArrayList<double[]> rightSet = new ArrayList<>();
     
            for (int i = 0; i < points.size(); i++)
            {
                double[] p = points.get(i);
                if (pointLocation(A, B, p) == -1)
                    leftSet.add(p);
                else if (pointLocation(A, B, p) == 1)
                    rightSet.add(p);
            }
            hullSet(A, B, rightSet, convexHull);
            hullSet(B, A, leftSet, convexHull);
            int n = convexHull.size();
            float[] hull = new float[3 * n];
            Arrays.fill(hull, 0);
            for (int i = 0; i < n; i++) {
                double[] c = convexHull.get(i);
                hull[3 * i] = (float)c[0];
                hull[3 * i + 1] = (float)c[1];
            }
            return hull;
        }
     
        private static double distance(double[] A, double[] B, double[] C)
        {
            double ABx = B[0] - A[0];
            double ABy = B[1] - A[1];
            double num = ABx * (A[1] - C[1]) - ABy * (A[0] - C[0]);
            if (num < 0)
                num = -num;
            return num;
        }
     
        private static  void hullSet(double[] A, double[] B, ArrayList<double[]> set,
                ArrayList<double[]> hull)
        {
            int insertPosition = hull.indexOf(B);
            if (set.isEmpty())
                return;
            if (set.size() == 1)
            {
                double[] p = set.get(0);
                set.remove(p);
                hull.add(insertPosition, p);
                return;
            }
            double dist = Float.MIN_VALUE;
            int furthestPoint = -1;
            for (int i = 0; i < set.size(); i++)
            {
                double[] p = set.get(i);
                double distance = distance(A, B, p);
                if (distance > dist)
                {
                    dist = distance;
                    furthestPoint = i;
                }
            }
            double[] P = set.get(furthestPoint);
            set.remove(furthestPoint);
            hull.add(insertPosition, P);
     
            // Determine who's to the left of AP
            ArrayList<double[]> leftSetAP = new ArrayList<>();
            for (int i = 0; i < set.size(); i++)
            {
                double[] M = set.get(i);
                if (pointLocation(A, P, M) == 1)
                {
                    leftSetAP.add(M);
                }
            }
     
            // Determine who's to the left of PB
            ArrayList<double[]> leftSetPB = new ArrayList<>();
            for (int i = 0; i < set.size(); i++)
            {
                double[] M = set.get(i);
                if (pointLocation(P, B, M) == 1)
                {
                    leftSetPB.add(M);
                }
            }
            hullSet(A, P, leftSetAP, hull);
            hullSet(P, B, leftSetPB, hull);
     
        }
     
        private static  int pointLocation(double[] A, double[] B, double[] P)
        {
            double cp1 = (B[0] - A[0]) * (P[1] - A[1]) - (B[1] - A[1]) * (P[0] - A[0]);
            if (cp1 > 0)
                return 1;
            else if (cp1 == 0)
                return 0;
            else
                return -1;
        }
    }
