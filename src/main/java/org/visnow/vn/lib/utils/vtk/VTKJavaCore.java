/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.vtk;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteOrder;
import java.util.Scanner;
import org.apache.log4j.Logger;
import org.visnow.jscic.Field;

/**
 *
 * @author Piotr Wendykier (piotrw@icm.edu.pl)
 */
public class VTKJavaCore extends VTKCore
{

    private static final Logger LOGGER = Logger.getLogger(VTKJavaCore.class);

    /**
     * Creates a new
     * <code>VTKJavaCore</code> object.
     */
    public VTKJavaCore()
    {
    }

    @Override
    public Field readVTK(String filename, ByteOrder order)
    {
        return ReadVTKFile(filename, order);
    }

    @Override
    public Field readVTK(File filename, ByteOrder order)
    {
        return ReadVTKFile(filename, order);
    }

    static public Field ReadVTKFile(String filename, ByteOrder order)
    {
        return ReadVTKFile(new File(filename), order);
    }

    static public Field ReadVTKFile(File file, ByteOrder order)
    {
        Field field;
        Scanner scanner;
        boolean binary;
        try {
            scanner = new Scanner(new FileReader(file));
            String line = scanner.nextLine();
            if (line == null || !line.startsWith("# vtk DataFile")) {
                return null;
            }
            do {
                line = scanner.nextLine().trim();
                if (line != null && (line.toLowerCase().startsWith("binary") || line.toLowerCase().startsWith("ascii"))) {
                    break;
                }
            } while (line != null);
            binary = line.equalsIgnoreCase("binary");
            scanner.close();
        } catch (IOException iOException) {
            return null;
        }
        if (binary) {
            field = new BinaryReader().readVTK(file, order);
        } else {
            field = new ASCIIReader().readVTK(file, order);
        }
        return field;
    }
}
