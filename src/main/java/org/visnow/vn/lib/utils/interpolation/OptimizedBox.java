/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.interpolation;

import java.util.Arrays;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams;

/**
 *
 * @author know
 */


public class OptimizedBox
{
        
    private static float boxVolume(float[] c, float[] v0, float[] v1, float[] v2, FloatLargeArray coords)
    {
        float[] tmax = new float[3], tmin = new float[3];
        Arrays.fill(tmax, -Float.MAX_VALUE);
        Arrays.fill(tmin,  Float.MAX_VALUE);
        for (long i = 0; i < coords.length(); i += 3) {
            float[] t = new float[3];
            for (int j = 0; j < 3; j++) {
                float crd = coords.getFloat(i + j) - c[j];
                t[0] += v0[j] * crd;
                t[1] += v1[j] * crd;
                t[2] += v2[j] * crd;
            }
            for (int j = 0; j < 3; j++) {
                if (t[j] > tmax[j])
                    tmax[j] = t[j];
                if (t[j] < tmin[j])
                    tmin[j] = t[j];
            }
        }
        return (tmax[0] - tmin[0]) * (tmax[1] - tmin[1]) * (tmax[2] - tmin[2]);
    }
    
    private static float[][] boxAffine(float[] c, float[] v0, float[] v1, float[] v2, FloatLargeArray coords)
    {
        float[] tmax = new float[3], tmin = new float[3];
        Arrays.fill(tmax, -Float.MAX_VALUE);
        Arrays.fill(tmin,  Float.MAX_VALUE);
        for (long i = 0; i < coords.length(); i += 3) {
            float[] t = new float[3];
            for (int j = 0; j < 3; j++) {
                float crd = coords.getFloat(i + j) - c[j];
                t[0] += v0[j] * crd;
                t[1] += v1[j] * crd;
                t[2] += v2[j] * crd;
            }
            for (int j = 0; j < 3; j++) {
                if (t[j] > tmax[j])
                    tmax[j] = t[j];
                if (t[j] < tmin[j])
                    tmin[j] = t[j];
            }
        }
        float[][] boxAffine = new float[4][3];
        System.arraycopy(c, 0, boxAffine[3], 0, 3);
        for (int i = 0; i < 3; i++) {
            boxAffine[3][i] += tmin[0] * v0[i] + tmin[1] * v1[i] + tmin[2] * v2[i];
            boxAffine[0][i] =  (tmax[0] - tmin[0]) * v0[i];
            boxAffine[1][i] =  (tmax[1] - tmin[1]) * v1[i];
            boxAffine[2][i] =  (tmax[2] - tmin[2]) * v2[i];
        }
        return boxAffine;
    }
    
    private static float boxVolume(float[] c, float[] v0, float[] v1, FloatLargeArray coords)
    {
        float[] tmax = new float[3], tmin = new float[3];
        Arrays.fill(tmax, -Float.MAX_VALUE);
        Arrays.fill(tmin,  Float.MAX_VALUE);
        for (long i = 0; i < coords.length(); i += 3) {
            float[] t = new float[2];
            for (int j = 0; j < 2; j++) {
                float crd = coords.getFloat(i + j) - c[j];
                t[0] += v0[j] * crd;
                t[1] += v1[j] * crd;
            }
            for (int j = 0; j < 2; j++) {
                if (t[j] > tmax[j])
                    tmax[j] = t[j];
                if (t[j] < tmin[j])
                    tmin[j] = t[j];
            }
        }
        return (tmax[0] - tmin[0]) * (tmax[1] - tmin[1]);
    }

    private static float[][] boxAffine (float[] c, float[] v0, float[] v1, FloatLargeArray coords)
    {
        float[] tmax = new float[3], tmin = new float[3];
        Arrays.fill(tmax, -Float.MAX_VALUE);
        Arrays.fill(tmin,  Float.MAX_VALUE);
        for (long i = 0; i < coords.length(); i += 3) {
            float[] t = new float[2];
            for (int j = 0; j < 2; j++) {
                float crd = coords.getFloat(i + j) - c[j];
                t[0] += v0[j] * crd;
                t[1] += v1[j] * crd;
            }
            for (int j = 0; j < 2; j++) {
                if (t[j] > tmax[j])
                    tmax[j] = t[j];
                if (t[j] < tmin[j])
                    tmin[j] = t[j];
            }
        }
        float[][] boxAffine = new float[4][3];
        System.arraycopy(c, 0, boxAffine[3], 0, 2);
        for (int i = 0; i < 2; i++) {
            boxAffine[3][i] += tmin[0] * v0[i] + tmin[1] * v1[i];
            boxAffine[0][i] =  (tmax[0] - tmin[0]) * v0[i];
            boxAffine[1][i] =  (tmax[1] - tmin[1]) * v1[i];
            boxAffine[2][i] =  0;
        }
        return boxAffine;
    }

    public static float[][] optimizeInitBox(FloatLargeArray inCoords, int trueDim, InteractiveGlyphParams glyphParams)
    {
        long nCoords = inCoords.length() / 3;
        float[][] reper, bestReper = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
        float bestVol = Float.MAX_VALUE;
        float[] c = new float[3];
        Arrays.fill(c, 0);
        for (long i = 0; i < nCoords; i++) {
            for (int j = 0; j < c.length; j++) {
                c[j] += inCoords.getFloat(3 * i + j);
            }
        }
        for (int i = 0; i < c.length; i++) {
            c[i] /= nCoords;
        }
        switch (trueDim) {
        case 2:
            for (int i = 0; i < 900; i++) {
                double phi = Math.PI * i / 1800.;
                float cos = (float) Math.cos(phi);
                float sin = (float) Math.sin(phi);
                reper = new float[][]{{cos, sin}, {-sin, cos}};
                float vol = boxVolume(c, reper[0], reper[1], inCoords);
                if (vol < bestVol) {
                    bestReper = new float[][]{Arrays.copyOf(reper[0], 2), Arrays.copyOf(reper[1], 2)};
                    bestVol = vol;
                }
            }
            if (glyphParams != null) {
                glyphParams.setReper(c, new float[]{bestReper[0][0], bestReper[0][1], 0},
                                        new float[]{bestReper[1][0], bestReper[1][1], 0},
                                        new float[]{0, 0, 1});
                glyphParams.updateAxesExtents();
            }
            return boxAffine(c, bestReper[0], bestReper[1], inCoords);
        case 3:
            reper =     new float[][]{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
            bestReper = new float[][]{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
            for (int istep = 0; istep < 6; istep++) {
                int i1 = (istep + 1) % 3;
                int i2 = (istep + 2) % 3;
                reper[istep % 3] = Arrays.copyOf(bestReper[istep % 3], 3);
                reper[i1] = Arrays.copyOf(bestReper[i1], 3);
                reper[i2] = Arrays.copyOf(bestReper[i2], 3);
                float[] r1 = Arrays.copyOf(bestReper[i1], 3);
                float[] r2 = Arrays.copyOf(bestReper[i2], 3);
                for (int i = 0; i < 90; i++) {
                    double phi = Math.PI * i / 180.;
                    float cos = (float) Math.cos(phi);
                    float sin = (float) Math.sin(phi);
                    for (int j = 0; j < 3; j++) {
                        reper[i1][j] = cos * r1[j] + sin * r2[j];
                        reper[i2][j] = -sin * r1[j] + cos * r2[j];
                    }
                    float vol = boxVolume(c, reper[0], reper[1], reper[2], inCoords);
                    if (vol < bestVol) {
                        bestReper = new float[][]{Arrays.copyOf(reper[0], 3),
                                                  Arrays.copyOf(reper[1], 3),
                                                  Arrays.copyOf(reper[2], 3)};
                        bestVol = vol;
                    }
                }
            }
            if (glyphParams != null) {
                glyphParams.setReper(c, bestReper[0], bestReper[1], bestReper[2]);
                glyphParams.updateAxesExtents();
                
            }
            return boxAffine(c, bestReper[0], bestReper[1], bestReper[2], inCoords);
        }
        return null;
    }


}
