/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.io;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.util.zip.GZIPOutputStream;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class VolumeWriterUtils
{

    private VolumeWriterUtils()
    {
    }

    public static void writeVolume(DataOutputStream out, int[] dims, byte[] data)
    {
        try {
            if (dims.length == 3) {
                if (dims[0] < 256 && dims[1] < 256 && dims[2] < 256) {
                    out.writeByte(dims[0]);
                    out.writeByte(dims[1]);
                    out.writeByte(dims[2]);
                } else {
                    out.writeByte(0);
                    out.writeInt(dims[0]);
                    out.writeInt(dims[1]);
                    out.writeInt(dims[2]);
                }
            } else if (dims.length == 2) {
                out.writeInt(dims[0]);
                out.writeInt(dims[1]);
            } else if (dims.length == 1)
                out.writeInt(dims[0]);
            out.write(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void writeVolume(String filePath, RegularField inField)
    {
        if (inField == null)
            return;
        DataOutputStream out = null;
        try {
            if (filePath.endsWith("_gz") || filePath.endsWith("_GZ"))
                out = new DataOutputStream(new GZIPOutputStream(new FileOutputStream(filePath)));
            else
                out = new DataOutputStream(new FileOutputStream(filePath));
            if (inField.getComponent(0).getType() == DataArrayType.FIELD_DATA_BYTE) {
                writeVolume(out, inField.getDims(), (byte[]) inField.getComponent(0).getRawArray().getData());
            } else {
                writeVolume(out, inField.getDims(), inField.getComponent(0).getRawByteArray().getData());
            }
            float[][] affine = inField.getAffine();
            for (int i = 0; i < affine.length; i++)
                for (int j = 0; j < affine[i].length; j++)
                    out.writeFloat(affine[i][j]);
            out.writeUTF(inField.getComponent(0).getName());
            if (inField.getComponent(0).getUserData() != null)
                for (int i = 0; i < inField.getComponent(0).getUserData().length; i++)
                    out.writeUTF(inField.getComponent(0).getUserData()[i]);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
