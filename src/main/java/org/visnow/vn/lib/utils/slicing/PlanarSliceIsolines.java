/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.slicing;

import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.util.Arrays;
import java.util.ArrayList;
import org.jogamp.java3d.J3DGraphics2D;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class PlanarSliceIsolines
{
    private RegularField inField;
    private String inComponent;
    private int[] dims;
    private float[] fData;
    private float[] thresholds;
    private float threshold;
    private int npoint, midpoint;
    private ArrayList<float[]>[] allIsolines = null;
    private float[] t_coords;
    private int width;
    private int height;
    private DataArray inData = null;

    @SuppressWarnings("unchecked")
    public PlanarSliceIsolines(RegularField inField, IsolinesParams params)
    {
        this.inField = inField;
        inComponent = params.getComponent();
        thresholds = params.getThresholds();
        if (inField == null || inField.getDims() == null || inField.getDims().length != 2 ||
            thresholds == null || thresholds.length < 1)
            return;
        dims = inField.getDims();
        width = dims[0];
        height = dims[1];
        inData = inField.getComponent(inComponent);
        if (inData.getVectorLength() == 1)
            fData = inField.getComponent(inComponent).getRawFloatArray().getData();
        else
            fData = inField.getComponent(inComponent).getVectorNorms().getData();
        float fMin = fData[0];
        float fMax = fData[0];
        for (int i = 1; i < fData.length; i++) {
            if (fMin > fData[i])
                fMin = fData[i];
            if (fMax < fData[i])
                fMax = fData[i];
        }
        float u, v0, v1, v2;
        int endln;
        allIsolines = (ArrayList<float[]>[]) new ArrayList[thresholds.length];
        for (int iThreshold = 0; iThreshold < thresholds.length; iThreshold++) {
            threshold = thresholds[iThreshold];
            if (threshold < fMin || threshold > fMax)
                continue;
            byte[] done = new byte[height * width];
            allIsolines[iThreshold] = new ArrayList<>();
            Arrays.fill(done, (byte)0);
            if (inField.hasMask()) {
                byte[] mask = inField.getCurrentMask().getByteData();
                for (int k = 0; k < mask.length; k++)
                    if (mask[k] == 0)
                        done[k] = 1;
            }
            t_coords = new float[2 * width * height];

            for (int iy = 0; iy < height - 1; iy++) {
                for (int ix = 0, l = width * iy; ix < width - 1; ix++, l++) {
                    if ((done[l] & 1) != 0)
                        continue;
                    done[l] |= 1;
                    v0 = fData[iy * width + ix] - threshold;
                    v1 = fData[iy * width + ix + 1] - threshold;
                    v2 = fData[(iy + 1) * width + ix] - threshold;
                    if (v0 == 0.)
                        v0 = 1.e-10f;
                    if (v1 == 0.)
                        v1 = 1.e-10f;
                    if (v2 == 0.)
                        v2 = 1.e-10f;
//                    if (Math.signum(v0) == Math.signum(v1) && Math.signum(v0) == Math.signum(v2))
//                        continue;
                    npoint = midpoint = 0;
                    if (v0 * v1 < 0) {
                        u = 1.f / (v1 - v0);
                        t_coords[2 * npoint] = ix - u * v0;
                        t_coords[2 * npoint + 1] = iy;
                        npoint += 1;
                        Triangle tstart = new Triangle(ix, iy, 0, 2);
                        endln = 0;
                        while (endln == 0)
                            endln = tstart.traverse(done);
                        midpoint = npoint;
                        if (endln == 2 && iy > 0) {
                            tstart = new Triangle(ix, iy - 1, 1, 2);
                            while (tstart.traverse(done) == 0) {
                            }
                        }
                    } else if (v0 * v2 < 0) {
                        u = 1.f / (v2 - v0);
                        t_coords[2 * npoint] = ix;
                        t_coords[2 * npoint + 1] = iy - u * v0;
                        npoint += 1;
                        Triangle tstart = new Triangle(ix, iy, 0, 1);
                        endln = 0;
                        while (endln == 0)
                            endln = tstart.traverse(done);
                        midpoint = npoint;
                        if (endln == 2 && ix > 0) {
                            tstart = new Triangle(ix - 1, iy, 1, 1);
                            while (tstart.traverse(done) == 0) {
                            }
                        }
                    }
                    int i = 0;
                    if (npoint > 2)
                        try {
                            float[] iCoords = new float[2 * npoint];
                            for (int ii = midpoint - 1; ii >= 0; ii--, i++)
                                for (int j = 0; j < 2; j++)
                                    iCoords[2 * i + j] = t_coords[2 * ii + j];
                            for (int ii = midpoint; ii < npoint; ii++, i++)
                                for (int j = 0; j < 2; j++)
                                    iCoords[2 * i + j] = t_coords[2 * ii + j];
                            allIsolines[iThreshold].add(iCoords);
                        } catch (Exception e) {
//                            System.out.println("i=" + i + " ii=" + ii + " j=" + j);
                        }
                }
            }
        }
        //now remove all unnecessary links:
        fData = null;
        t_coords = null;
    }

    // TRIANGLE *****************************************
    private class Triangle
    {
        private int ix, iy;
        private int is_up;
        private float[][] coords;
        private float[] vals;
        private int edge_in;

        public Triangle(int inx, int iny, int inIs_up, int inEdge_in)
        {
            if (inx < 0 || inx >= width || iny < 0 || iny >= height) {
                ix = iy = -999;
                throw new IllegalArgumentException("Triangle out of range.");
            }
            ix = inx;
            iy = iny;
            is_up = inIs_up;
            coords = new float[3][2];
            vals = new float[3];
            loadValues();
            edge_in = inEdge_in;
        }

        private void loadValues()
        {
            if (is_up == 1) {
                coords[0][0] = ix + 1;
                coords[0][1] = iy + 1;
                vals[0] = fData[(iy + 1) * width + ix + 1] - threshold;
                coords[1][0] = ix;
                coords[1][1] = iy + 1;
                vals[1] = fData[(iy + 1) * width + ix] - threshold;
                coords[2][0] = ix + 1;
                coords[2][1] = iy;
                vals[2] = fData[iy * width + ix + 1] - threshold;
            }
            else {
                coords[0][0] = ix;
                coords[0][1] = iy;
                vals[0] = fData[iy * width + ix] - threshold;
                coords[1][0] = ix + 1;
                coords[1][1] = iy;
                vals[1] = fData[iy * width + ix + 1] - threshold;
                coords[2][0] = ix;
                coords[2][1] = iy + 1;
                vals[2] = fData[(iy + 1) * width + ix] - threshold;
            }
            for (int i = 0; i < 3; i++)
                if (vals[i] == 0.)
                    vals[i] = 1.e-10f;
        }

        public int traverse(byte[] done)
        {
            int edge_out = -1;
            int is_up_out = 1 - is_up;
            int dirx, diry;
            done[iy * width + ix] |= 1 << is_up;
            float u;
            dirx = diry = 0;
            switch (edge_in) {
                case 0:
                    if (vals[0] * vals[1] >= 0.)
                        edge_out = 1;
                    else
                        edge_out = 2;
                    break;
                case 1:
                    if (vals[1] * vals[2] >= 0.)
                        edge_out = 2;
                    else
                        edge_out = 0;
                    break;
                case 2:
                    if (vals[2] * vals[0] >= 0.)
                        edge_out = 0;
                    else
                        edge_out = 1;
                    break;
            }
            switch (edge_out) {
                case 0:
                    break;
                case 1:
                    u = 1.f / (vals[2] - vals[0]);
                    for (int i = 0; i < 2; i++)
                        t_coords[2 * npoint + i] = u * (vals[2] * coords[0][i] - vals[0] * coords[2][i]);
                    npoint++;
                    if (is_up == 1)
                        dirx = 1;
                    else
                        dirx = -1;
                    if (ix + dirx < 0 || ix + dirx >= width - 1)
                        return 2;
                    if ((done[iy * width + ix + dirx] & (1 << is_up_out)) != 0)
                        return (1);
                    break;
                case 2:
                    u = 1.f / (vals[0] - vals[1]);
                    for (int i = 0; i < 2; i++)
                        t_coords[2 * npoint + i] = u * (vals[0] * coords[1][i] - vals[1] * coords[0][i]);
                    npoint++;
                    if (is_up == 1)
                        diry = 1;
                    else
                        diry = -1;
                    if (iy + diry < 0 || iy + diry >= height - 1)
                        return 2;
                    if ((done[iy * width + ix + dirx] & (1 << is_up_out)) != 0)
                        return (1);
                    break;
            }
            ix += dirx;
            iy += diry;
            is_up = is_up_out;
            edge_in = edge_out;
            loadValues();
            return 0;
        }
    }
    
    public ArrayList<float[]>[] getLines()
    {
        return (allIsolines);
    }
    
    public void draw(J3DGraphics2D gr, int ix, int iy, float scale)
    {
        AffineTransform tr = gr.getTransform();
        gr.scale(scale, scale);
        gr.translate(ix, iy);
        gr.setColor(Color.WHITE);
        for (ArrayList<float[]> isoline : allIsolines) {
            for (float[] component : isoline) 
                if (component.length > 2) {
                    GeneralPath isolinePath = new GeneralPath();
                    isolinePath.moveTo(component[0], component[1]);
                    for (int i = 2; i < component.length; i += 2)
                        isolinePath.lineTo(component[i], component[i + 1]);
                    gr.draw(isolinePath);
                }
        }
        gr.setTransform(tr);
    }
}
