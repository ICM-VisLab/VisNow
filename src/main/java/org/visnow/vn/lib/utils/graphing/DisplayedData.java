/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.graphing;

import java.awt.BasicStroke;
import static java.awt.BasicStroke.*;
import java.awt.Color;
import java.awt.Stroke;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class DisplayedData
{
    private static final float[][] STROKES = 
    {   {1}, 
        {9, 6}, 
        {2, 3}, 
        {9, 6, 2, 6}, 
        {9, 6, 2, 6, 2, 6},
        {9, 6, 9, 6, 2, 6}, 
    };
    private int index = 0;
    
    private Color color = Color.WHITE;
    
    private int strokeIndex = 0;
    private float width = 1;
    private boolean displayed = false;
    private int exponent = 0;

    public DisplayedData()
    {
    }

    public DisplayedData(int index, Color color)
    {
        this.index = index;
        this.color = color;
    }

    public Color getColor()
    {
        return color;
    }

    public void setColor(Color color)
    {
        this.color = color;
    }

    public Stroke getStroke()
    {
        return strokeIndex > 0 ? new BasicStroke(width, CAP_ROUND, JOIN_ROUND, 0, 
                                                 STROKES[strokeIndex], 0) :
                                 new BasicStroke(width);
    }

    public void setStroke(int index)
    {
        strokeIndex = index;
    }
    
    public int getStrokeIndex()
    {
        return strokeIndex;
    }

    public float getWidth()
    {
        return width;
    }

    public void setWidth(float width)
    {
        this.width = width;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public boolean isDisplayed()
    {
        return displayed;
    }

    public void setDisplayed(boolean displayed)
    {
        this.displayed = displayed;
    }

    public int getExponent() {
        return exponent;
    }

    public void setExponent(int exponent) {
        this.exponent = exponent;
    }
}
