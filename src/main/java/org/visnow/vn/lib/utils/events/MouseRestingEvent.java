/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.events;

import javax.swing.event.ChangeEvent;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class MouseRestingEvent extends ChangeEvent
{

    private int ix = -1, iy = -1, screenX = -1, screenY = -1;

    /**
     * Creates a new instance of DynamicsModificationEvent
     * @param source source JComponent 
     * @param ix x mouse position in component
     * @param iy y mouse position in component
     * @param screenX x mouse position in screen
     * @param screenY y mouse position in screen
     */
    public MouseRestingEvent(Object source, int ix, int iy, int screenX, int screenY)
    {
        super(source);
        this.ix = ix;
        this.iy = iy;
        this.screenX = screenX;
        this.screenY = screenY;
    }

    @Override
    public String toString()
    {
        return "Mouse resting at (" + ix + "," + iy + ")";
    }

    public int getX()
    {
        return ix;
    }

    public int getY()
    {
        return iy;
    }

    public int getScreenX()
    {
        return screenX;
    }

    public int getScreenY()
    {
        return screenY;
    }

}
