/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.numeric.splines;

import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class SplineUtils
{

    private static final float[] c0 = {-.5f, 1.f, -.5f, 0.f};
    private static final float[] c1 = {1.5f, -2.5f, 0.f, 1.f};
    private static final float[] c2 = {-1.5f, 2.f, .5f, 0.f};
    private static final float[] c3 = {.5f, -.5f, 0.f, 0.f};
    private static final float[][] c = {c0, c1, c2, c3};
    private int[] dims;
    private int vlen;
    private FloatLargeArray inData;
    private int[] outDims;
    private FloatLargeArray outData;
    private long nData;
    private int nThreads = 1;
    private int[] iPoints;
    private int[] jPoints;
    private int[] kPoints;
    private int[] iLimits;
    private int[] jLimits;
    private int[] kLimits;
    private float[][] iCoeffs;
    private float[][] jCoeffs;
    private float[][] kCoeffs;

    class ComputeSplines implements Runnable
    {

        int nThreads = 1;
        int iThread = 0;

        public ComputeSplines(int nThreads, int iThread)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
        }

        public void run()
        {
            int ii, jj, kk;
            switch (dims.length) {
                case 3:
                    for (int i = (iThread * dims[2]) / nThreads; i < ((iThread + 1) * dims[2]) / nThreads; i++) {
                        if (iThread == 0)
                            fireStatusChanged(i * nThreads/ (dims[2] - 1.f));
                        for (int j = 0; j < dims[1] - 1; j++)
                            for (int k = 0; k < dims[0] - 1; k++) {
                                int pl = iLimits[i];
                                int pu = iLimits[i + 1];
                                int ql = jLimits[j];
                                int qu = jLimits[j + 1];
                                int rl = kLimits[k];
                                int ru = kLimits[k + 1];
                                for (ii = 0; ii < 4; ii++) {
                                    int ix = i + ii - 1;
                                    if (ix < 0)
                                        ix = 0;
                                    if (ix >= dims[2])
                                        ix = dims[2] - 1;
                                    for (jj = 0; jj < 4; jj++) {
                                        int jx = j + jj - 1;
                                        if (jx < 0)
                                            jx = 0;
                                        if (jx >= dims[1])
                                            jx = dims[1] - 1;
                                        for (kk = 0; kk < 4; kk++) {
                                            int kx = k + kk - 1;
                                            if (kx < 0)
                                                kx = 0;
                                            if (kx >= dims[0])
                                                kx = dims[0] - 1;
                                            for (int v = 0; v < vlen; v++) {
                                                float u = inData.getFloat(vlen * ((ix * dims[1] + jx) * dims[0] + kx) + v);
                                                for (long p = pl; p < pu; p++) {
                                                    float ui = u * iCoeffs[(int)p][ii];
                                                    for (int q = ql; q < qu; q++) {
                                                        float uij = ui * jCoeffs[q][jj];
                                                        long l = (p * outDims[1] + q) * outDims[0] + rl;
                                                        for (int r = rl; r < ru; r++, l++)
                                                            outData.setFloat(vlen * l + v, outData.getFloat(vlen * l + v) + uij * kCoeffs[r][kk]);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                    }
                    break;
                case 2:
                    for (int j = (iThread * dims[1]) / nThreads; j < ((iThread + 1) * dims[1]) / nThreads; j++)
                        for (int k = 0; k < dims[0] - 1; k++) {
                            int ql = jLimits[j];
                            int qu = jLimits[j + 1];
                            int rl = kLimits[k];
                            int ru = kLimits[k + 1];
                            for (jj = 0; jj < 4; jj++) {
                                int jx = j + jj - 1;
                                if (jx < 0)
                                    jx = 0;
                                if (jx >= dims[1])
                                    jx = dims[1] - 1;
                                for (kk = 0; kk < 4; kk++) {
                                    int kx = k + kk - 1;
                                    if (kx < 0)
                                        kx = 0;
                                    if (kx >= dims[0])
                                        kx = dims[0] - 1;
                                    for (int v = 0; v < vlen; v++) {
                                        float u = inData.getFloat(vlen * (jx * dims[0] + kx) + v);
                                        for (long q = ql; q < qu; q++) {
                                            long l = q * outDims[0] + rl;
                                            for (int r = rl; r < ru; r++, l++)
                                                outData.setFloat(vlen * l + v, outData.getFloat(vlen * l + v) + u * jCoeffs[(int)q][jj] * kCoeffs[r][kk]);
                                        }
                                    }
                                }
                            }
                        }
                    break;
                case 1:
                    for (int k = iThread; k < dims[0] - 1; k += nThreads) {
                        int rl = kLimits[k];
                        int ru = kLimits[k + 1];
                        for (kk = 0; kk < 4; kk++) {
                            int kx = k + kk - 1;
                            if (kx < 0)
                                kx = 0;
                            if (kx >= dims[0])
                                kx = dims[0] - 1;
                            for (int v = 0; v < vlen; v++) {
                                float u = inData.getFloat(vlen * kx + v);
                                for (int r = rl, l = rl; r < ru; r++, l++)
                                     outData.setFloat(vlen * l + v, outData.getFloat(vlen * l + v) + u * kCoeffs[r][kk]);
                            }
                        }
                    }
                    break;
            }
        }
    }

    public SplineUtils(int[] dims, int vlen, int density, FloatLargeArray inData, int nThreads)
    {
        this.dims = dims;
        this.vlen = vlen;
        this.inData = inData;
        outDims = new int[dims.length];
        nData = 1;
        for (int i = 0; i < dims.length; i++) {
            outDims[i] = density * (dims[i] - 1) + 1;
            nData *= outDims[i];
        }
        kPoints = new int[outDims[0]];
        kLimits = new int[dims[0]];
        kLimits[0] = 0;
        kCoeffs = new float[outDims[0]][4];
        for (int i = 0; i < outDims[0]; i++) {
            float f = (float) i / density;
            kPoints[i] = (int) f;
            if (kPoints[i] >= dims[0] - 1)
                kPoints[i] = dims[0] - 2;
            for (int j = kPoints[i] + 1; j < kLimits.length; j++)
                kLimits[j] = i + 1;
            f -= kPoints[i];
            for (int j = 0; j < 4; j++)
                kCoeffs[i][j] = ((c[j][0] * f + c[j][1]) * f + c[j][2]) * f + c[j][3];
        }
        if (outDims.length > 1) {
            jPoints = new int[outDims[1]];
            jLimits = new int[dims[1] + 1];
            jLimits[0] = 0;
            jCoeffs = new float[outDims[1]][4];
            for (int i = 0; i < outDims[1]; i++) {
                float f = (float) i / density;
                jPoints[i] = (int) f;
                if (jPoints[i] >= dims[1] - 1)
                    jPoints[i] = dims[1] - 2;
                for (int j = jPoints[i] + 1; j < jLimits.length; j++)
                    jLimits[j] = i + 1;
                f -= jPoints[i];
                for (int j = 0; j < 4; j++)
                    jCoeffs[i][j] = ((c[j][0] * f + c[j][1]) * f + c[j][2]) * f + c[j][3];
            }
        }
        if (outDims.length > 2) {
            iPoints = new int[outDims[2]];
            iLimits = new int[dims[2] + 1];
            iLimits[0] = 0;
            iCoeffs = new float[outDims[2]][4];
            for (int i = 0; i < outDims[2]; i++) {
                float f = (float) i / density;
                iPoints[i] = (int) f;
                if (iPoints[i] >= dims[2] - 1)
                    iPoints[i] = dims[2] - 2;
                for (int j = iPoints[i] + 1; j < iLimits.length; j++)
                    iLimits[j] = i + 1;
                f -= iPoints[i];
                for (int j = 0; j < 4; j++)
                    iCoeffs[i][j] = ((c[j][0] * f + c[j][1]) * f + c[j][2]) * f + c[j][3];
            }
        }
        outData = new FloatLargeArray(vlen * nData);
        for (long i = 0; i < outData.length(); i++)
            outData.setFloat(i, 0);

        this.nThreads = nThreads;
    }

    public SplineUtils(int[] dims, int vlen, int[] outDims, FloatLargeArray inData, int nThreads)
    {
        this.dims = dims;
        this.vlen = vlen;
        this.inData = inData;
        this.outDims = outDims;
        nData = 1;
        for (int i = 0; i < dims.length; i++)
            nData *= outDims[i];
        kPoints = new int[outDims[0]];
        kLimits = new int[dims[0] + 1];
        kLimits[0] = 0;
        kCoeffs = new float[outDims[0]][4];
        float d = (float) (outDims[0] - 1) / (dims[0] - 1);
        for (int i = 0; i < outDims[0]; i++) {
            float f = i / d;
            kPoints[i] = (int) f;
            if (kPoints[i] >= dims[0] - 1)
                kPoints[i] = dims[0] - 2;
            for (int j = kPoints[i] + 1; j < kLimits.length; j++)
                kLimits[j] = i + 1;
            f -= kPoints[i];
            for (int j = 0; j < 4; j++)
                kCoeffs[i][j] = ((c[j][0] * f + c[j][1]) * f + c[j][2]) * f + c[j][3];
        }
        if (outDims.length > 1) {
            jPoints = new int[outDims[1]];
            jLimits = new int[dims[1] + 1];
            jLimits[0] = 0;
            jCoeffs = new float[outDims[1]][4];
            d = (float) (outDims[1] - 1) / (dims[1] - 1);
            for (int i = 0; i < outDims[1]; i++) {
                float f = i / d;
                jPoints[i] = (int) f;
                if (jPoints[i] >= dims[1] - 1)
                    jPoints[i] = dims[1] - 2;
                for (int j = jPoints[i] + 1; j < jLimits.length; j++)
                    jLimits[j] = i + 1;
                f -= jPoints[i];
                for (int j = 0; j < 4; j++)
                    jCoeffs[i][j] = ((c[j][0] * f + c[j][1]) * f + c[j][2]) * f + c[j][3];
            }
        }
        if (outDims.length > 2) {
            iPoints = new int[outDims[2]];
            iLimits = new int[dims[2] + 1];
            iLimits[0] = 0;
            iCoeffs = new float[outDims[2]][4];
            d = (float) (outDims[2] - 1) / (dims[2] - 1);
            for (int i = 0; i < outDims[2]; i++) {
                float f = i / d;
                iPoints[i] = (int) f;
                if (iPoints[i] >= dims[2] - 1)
                    iPoints[i] = dims[2] - 2;
                for (int j = iPoints[i] + 1; j < iLimits.length; j++)
                    iLimits[j] = i + 1;
                f -= iPoints[i];
                for (int j = 0; j < 4; j++)
                    iCoeffs[i][j] = ((c[j][0] * f + c[j][1]) * f + c[j][2]) * f + c[j][3];
            }
        }

        outData = new FloatLargeArray(vlen * nData);
        for (long i = 0; i < outData.length(); i++)
            outData.setFloat(i, 0);

        this.nThreads = nThreads;
    }

    public SplineUtils(int[] dims, int vlen, float[][] affine, float cellSize, FloatLargeArray inData, int nThreads)
    {
        float xExp, yExp, zExp;
        this.dims = dims;
        this.vlen = vlen;
        this.inData = inData;
        outDims = new int[dims.length];
        nData = 1;
        xExp = affine[0][0] / cellSize;
        outDims[0] = (int) ((dims[0] - 1) * xExp) + 1;
        kPoints = new int[outDims[0]];
        kLimits = new int[dims[0] + 1];
        kLimits[0] = 0;
        kCoeffs = new float[outDims[0]][4];
        for (int i = 0; i < outDims[0]; i++) {
            float f = i / xExp;
            kPoints[i] = (int) f;
            if (kPoints[i] >= dims[0] - 1)
                kPoints[i] = dims[0] - 2;
            for (int j = kPoints[i] + 1; j < kLimits.length; j++)
                kLimits[j] = i + 1;
            f -= kPoints[i];
            for (int j = 0; j < 4; j++)
                kCoeffs[i][j] = ((c[j][0] * f + c[j][1]) * f + c[j][2]) * f + c[j][3];
        }
        if (outDims.length > 1) {
            yExp = affine[1][1] / cellSize;
            outDims[1] = (int) ((dims[1] - 1) * yExp) + 1;
            jPoints = new int[outDims[1]];
            jLimits = new int[dims[1] + 1];
            jLimits[0] = 0;
            jCoeffs = new float[outDims[1]][4];
            for (int i = 0; i < outDims[1]; i++) {
                float f = i / yExp;
                jPoints[i] = (int) f;
                if (jPoints[i] >= dims[1] - 1)
                    jPoints[i] = dims[1] - 2;
                for (int j = jPoints[i] + 1; j < jLimits.length; j++)
                    jLimits[j] = i + 1;
                f -= jPoints[i];
                for (int j = 0; j < 4; j++)
                    jCoeffs[i][j] = ((c[j][0] * f + c[j][1]) * f + c[j][2]) * f + c[j][3];
            }
        }
        if (outDims.length > 2) {
            zExp = affine[2][2] / cellSize;
            outDims[2] = (int) ((dims[2] - 1) * zExp) + 1;
            iPoints = new int[outDims[2]];
            iLimits = new int[dims[2] + 1];
            iLimits[0] = 0;
            iCoeffs = new float[outDims[2]][4];
            for (int i = 0; i < outDims[2]; i++) {
                float f = i / zExp;
                iPoints[i] = (int) f;
                if (iPoints[i] >= dims[2] - 1)
                    iPoints[i] = dims[2] - 2;
                for (int j = iPoints[i] + 1; j < iLimits.length; j++)
                    iLimits[j] = i + 1;
                f -= iPoints[i];
                for (int j = 0; j < 4; j++)
                    iCoeffs[i][j] = ((c[j][0] * f + c[j][1]) * f + c[j][2]) * f + c[j][3];
            }
        }

        for (int i = 0; i < dims.length; i++)
            nData *= outDims[i];
        outData = new FloatLargeArray(vlen * nData);
        for (long i = 0; i < outData.length(); i++)
            outData.setFloat(i, 0);
        this.nThreads = nThreads;
    }

    public FloatLargeArray splineInterpolate()
    {
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new ComputeSplines(nThreads, iThread));
            workThreads[iThread].start();
        }
        for (int i = 0; i < workThreads.length; i++)
            try {
                workThreads[i].join();
            } catch (Exception e) {
            }
        return outData;
    }

    private transient FloatValueModificationListener statusListener = null;

    public void addFloatValueModificationListener(FloatValueModificationListener listener)
    {
        if (statusListener == null)
            this.statusListener = listener;
        else
            System.out.println("" + this + ": only one status listener can be added");
    }

    private void fireStatusChanged(float status)
    {
        FloatValueModificationEvent e = new FloatValueModificationEvent(this, status, true);
        if (statusListener != null)
            statusListener.floatValueChanged(e);
    }

}
