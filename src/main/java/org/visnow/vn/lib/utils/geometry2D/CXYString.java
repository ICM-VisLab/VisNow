/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.geometry2D;

import java.awt.*;
import org.jogamp.java3d.*;
import org.visnow.vn.geometries.parameters.FontParams;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class CXYString
{

    protected String s;
    protected Color c = null;
    protected int fontSize = 12;
    protected Font font = new Font("Dialog", Font.PLAIN, fontSize);
    protected float[] ccomps = new float[3];
    protected int[] sCoords = new int[2];
    protected float depth = 1.f;
    protected float relativeHeight = .01f;

    /**
     * Creates a new instance of CXYString
     */
    public CXYString(String s, Color c, Font font, float relativeHeight)
    {
        this.font = font;
        this.relativeHeight = relativeHeight;
        this.s = s;
        this.c = c;
        ccomps = c.getRGBColorComponents(ccomps);
    }

    public CXYString(String s, FontParams params)
    {
        this.s = s;
        c = params.getColor();
        font = params.getFont2D();
        relativeHeight = params.getSize();
        ccomps = c.getRGBColorComponents(ccomps);
    }

    public CXYString(String s, Color c)
    {
        this.s = s;
        this.c = c;
        ccomps = c.getRGBColorComponents(ccomps);
    }

    @Override
    public String toString()
    {
        return s + " at(" + sCoords[0] + "," + sCoords[1] + ")";
    }

    public String getString()
    {
        return s;
    }

    public void setColor(Color c)
    {
        if (c == null)
            return;
        this.c = c;
        ccomps = c.getRGBColorComponents(ccomps);
    }

    /**
     *
     * @param vGraphics
     */
    public void draw(J3DGraphics2D vGraphics, int w, int h)
    {
        if (s == null || s.length() < 1)
            return;
        Font f = vGraphics.getFont();
        Font ft = new Font(font.getFontName(), font.getStyle(), (int) (h * relativeHeight));
        vGraphics.setFont(ft);
        vGraphics.setColor(new Color(depth * ccomps[0], depth * ccomps[1], depth * ccomps[2]));
        vGraphics.drawString(s, sCoords[0], sCoords[1]);
        vGraphics.setFont(f);
    }

    public void draw(J3DGraphics2D vGraphics, int xpos, int ypos, int w, int h)
    {
        if (s == null || s.length() < 1)
            return;
        Font f = vGraphics.getFont();
        Font ft = new Font(font.getFontName(), font.getStyle(), (int) (h * relativeHeight));
        vGraphics.setFont(ft);
        vGraphics.setColor(new Color(depth * ccomps[0], depth * ccomps[1], depth * ccomps[2]));
        vGraphics.drawString(s, xpos, ypos);
        vGraphics.setFont(f);
    }
}
