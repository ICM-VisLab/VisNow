/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.numeric;

/**
 * Sorts in ascending order
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class HeapSort
{

    public static void sort(int[] a0, boolean ascending)
    {
        heapsortInt(a0.length, 1, a0);
        if (ascending)
            return;
        int n = a0.length - 1;
        for (int i = 0; i < a0.length / 2; i++) {
            int j = a0[i];
            a0[i] = a0[n - i];
            a0[n - i] = j;
        }
    }

    public static void sort(int[] a0, int vl, boolean ascending)
    {
        heapsortInt(a0.length / vl, vl, a0);
        if (ascending)
            return;
        int n = a0.length - vl;
        for (int i = 0; i < a0.length / 2; i += vl)
            for (int j = 0; j < vl; j++) {
                int k = a0[i + j];
                a0[i + j] = a0[n - i + j];
                a0[n - i + j] = k;
            }
    }

    public static void sort(int[] a0, int n, int vl)
    {
        heapsortInt(n, vl, a0);
    }

    public static void sort(float[] a0)
    {
        heapsortFloat(a0.length, 1, a0);
    }

    public static void sort(float[] a0, int vl)
    {
        heapsortFloat(a0.length / vl, vl, a0);
    }

    public static void sort(float[] a0, int n, int vl)
    {
        heapsortFloat(n, vl, a0);
    }

    public static void sort(double[] a0)
    {
        heapsortDouble(a0.length, 1, a0);
    }

    public static void sort(double[] a0, int vl)
    {
        heapsortDouble(a0.length / vl, vl, a0);
    }

    public static void sort(double[] a0, int n, int vl)
    {
        heapsortDouble(n, vl, a0);
    }

    public static void heapsortInt(int n, int vlen, int a[])
    {
        int k, l, m, v, w, t;
        //buildheap();
        for (v = n / 2 - 1; v >= 0; v--) //downheap (v);
        {
            w = 2 * v + 1;
            while (w < n) {
                if (w + 1 < n && a[vlen * (w + 1)] > a[vlen * w])
                    w++;
                if (a[vlen * v] >= a[vlen * w])
                    break;
                for (k = 0, l = v * vlen, m = w * vlen; k < vlen; k++, l++, m++) {
                    t = a[l];
                    a[l] = a[m];
                    a[m] = t;
                }
                v = w;
                w = 2 * v + 1;
            }
        }
        while (n > 1) {
            n--;
            for (k = 0, l = 0, m = n * vlen; k < vlen; k++, l++, m++) {
                t = a[l];
                a[l] = a[m];
                a[m] = t;
            }
            //downheap (0);
            v = 0;
            w = 1;
            while (w < n) {
                if (w + 1 < n && a[vlen * (w + 1)] > a[vlen * w])
                    w++;
                if (a[vlen * v] >= a[vlen * w])
                    break;
                for (k = 0, l = v * vlen, m = w * vlen; k < vlen; k++, l++, m++) {
                    t = a[l];
                    a[l] = a[m];
                    a[m] = t;
                }
                v = w;
                w = 2 * v + 1;
            }
        }
    }

    public static void heapsortLong(int n, int vlen, long a[])
    {
        int k, l, m, v, w;
        long t;
        //buildheap();
        for (v = n / 2 - 1; v >= 0; v--) //downheap (v);
        {
            w = 2 * v + 1;
            while (w < n) {
                if (w + 1 < n && a[vlen * (w + 1)] > a[vlen * w])
                    w++;
                if (a[vlen * v] >= a[vlen * w])
                    break;
                for (k = 0, l = v * vlen, m = w * vlen; k < vlen; k++, l++, m++) {
                    t = a[l];
                    a[l] = a[m];
                    a[m] = t;
                }
                v = w;
                w = 2 * v + 1;
            }
        }
        while (n > 1) {
            n--;
            for (k = 0, l = 0, m = n * vlen; k < vlen; k++, l++, m++) {
                t = a[l];
                a[l] = a[m];
                a[m] = t;
            }
            //downheap (0);
            v = 0;
            w = 1;
            while (w < n) {
                if (w + 1 < n && a[vlen * (w + 1)] > a[vlen * w])
                    w++;
                if (a[vlen * v] >= a[vlen * w])
                    break;
                for (k = 0, l = v * vlen, m = w * vlen; k < vlen; k++, l++, m++) {
                    t = a[l];
                    a[l] = a[m];
                    a[m] = t;
                }
                v = w;
                w = 2 * v + 1;
            }
        }
    }

    /**
     * Sort arrays of vectors by all columns starting from first.
     *
     * @param n
     * @param vlen
     * @param a
     * @param indexes Indexes permutation
     */
    public static void heapsortFloatFull(int n, int vlen, float a[], int indexes[])
    {
        heapsortFloat(n, vlen, a, indexes, 0, 0);
        int len;
        int ind;
        int varNum = 1;
        while (varNum < vlen) {
            len = 1;
            ind = 1;
            while (ind < n) {
                if (a[ind * vlen + varNum - 1] != a[(ind - 1) * vlen + varNum - 1]) {
                    if (len > 1)
                        heapsortFloat(len, vlen, a, indexes, varNum, ind - len);
                    len = 0;
                }
                ++ind;
                ++len;
            }
            if (len > 1)
                heapsortFloat(len, vlen, a, indexes, varNum, ind - len);
            ++varNum;
        }
    }

    /**
     * Sort arrays of vectors by all columns starting from first.
     *
     * @param n
     * @param vlen
     * @param a
     * @param indexes Indexes permutation
     */
    public static void heapsortIntFull(int n, int vlen, int a[], int indexes[])
    {
        heapsortInt(n, vlen, a, indexes, 0, 0);
        int len;
        int ind;
        int varNum = 1;
        while (varNum < vlen) {
            len = 1;
            ind = 1;
            while (ind < n) {
                if (a[ind * vlen + varNum - 1] != a[(ind - 1) * vlen + varNum - 1]) {
                    if (len > 1)
                        heapsortInt(len, vlen, a, indexes, varNum, ind - len);
                    len = 0;
                }
                ++ind;
                ++len;
            }
            if (len > 1)
                heapsortInt(len, vlen, a, indexes, varNum, ind - len);
            ++varNum;
        }
    }

    /**
     * Sorting by varNum. Retrieves also index permutation.
     *
     * @param n            Number of vectors to sort
     * @param vlen         Vector length
     * @param a            Array to sort
     * @param indexes      Array of indexes [length = a.length/vlen]
     * @param varNum       Sort by varNum
     * @param vectorOffset Offset as vector index
     */
    public static void heapsortFloat(int n, int vlen, float a[], int indexes[], int varNum, int vectorOffset)
    {
        int arrOffset = vectorOffset * vlen;
        assert (varNum < vlen);
        int k, l, m, v, w, ti;
        float t;
        //buildheap();
        for (v = n / 2 - 1; v >= 0; v--) //downheap (v);
        {
            w = 2 * v + 1;
            while (w < n) {
                if (w + 1 < n && a[vlen * (w + 1) + varNum + arrOffset] > a[vlen * w + varNum + arrOffset])
                    w++;
                if (a[vlen * v + varNum + arrOffset] >= a[vlen * w + varNum + arrOffset])
                    break;
                for (k = 0, l = v * vlen, m = w * vlen; k < vlen; k++, l++, m++) {
                    t = a[l + arrOffset];
                    a[l + arrOffset] = a[m + arrOffset];
                    a[m + arrOffset] = t;
                }
                ti = indexes[v + vectorOffset];
                indexes[v + vectorOffset] = indexes[w + vectorOffset];
                indexes[w + vectorOffset] = ti;
                v = w;
                w = 2 * v + 1;
            }
        }
        while (n > 1) {
            n--;
            for (k = 0, l = 0, m = n * vlen; k < vlen; k++, l++, m++) {
                t = a[l + arrOffset];
                a[l + arrOffset] = a[m + arrOffset];
                a[m + arrOffset] = t;
            }
            ti = indexes[vectorOffset];
            indexes[vectorOffset] = indexes[n + vectorOffset];
            indexes[n + vectorOffset] = ti;

            //downheap (0);
            v = 0;
            w = 1;
            while (w < n) {
                if (w + 1 < n && a[vlen * (w + 1) + varNum + arrOffset] > a[vlen * w + varNum + arrOffset])
                    w++;
                if (a[vlen * v + varNum + arrOffset] >= a[vlen * w + varNum + arrOffset])
                    break;
                for (k = 0, l = v * vlen, m = w * vlen; k < vlen; k++, l++, m++) {
                    t = a[l + arrOffset];
                    a[l + arrOffset] = a[m + arrOffset];
                    a[m + arrOffset] = t;
                }
                ti = indexes[v + vectorOffset];
                indexes[v + vectorOffset] = indexes[w + vectorOffset];
                indexes[w + vectorOffset] = ti;
                v = w;
                w = 2 * v + 1;
            }
        }
    }

    /**
     * Sorting by varNum. Retrieves also index permutation.
     *
     * @param n            Number of vectors to sort
     * @param vlen         Vector length
     * @param a            Array to sort
     * @param indexes      Array of indexes [length = a.length/vlen]
     * @param varNum       Sort by varNum
     * @param vectorOffset Offset as vector index
     */
    public static void heapsortInt(int n, int vlen, int a[], int indexes[], int varNum, int vectorOffset)
    {
        int arrOffset = vectorOffset * vlen;
        assert (varNum < vlen);
        int k, l, m, v, w, ti;
        int t;
        //buildheap();
        for (v = n / 2 - 1; v >= 0; v--) //downheap (v);
        {
            w = 2 * v + 1;
            while (w < n) {
                if (w + 1 < n && a[vlen * (w + 1) + varNum + arrOffset] > a[vlen * w + varNum + arrOffset])
                    w++;
                if (a[vlen * v + varNum + arrOffset] >= a[vlen * w + varNum + arrOffset])
                    break;
                for (k = 0, l = v * vlen, m = w * vlen; k < vlen; k++, l++, m++) {
                    t = a[l + arrOffset];
                    a[l + arrOffset] = a[m + arrOffset];
                    a[m + arrOffset] = t;
                }
                ti = indexes[v + vectorOffset];
                indexes[v + vectorOffset] = indexes[w + vectorOffset];
                indexes[w + vectorOffset] = ti;
                v = w;
                w = 2 * v + 1;
            }
        }
        while (n > 1) {
            n--;
            for (k = 0, l = 0, m = n * vlen; k < vlen; k++, l++, m++) {
                t = a[l + arrOffset];
                a[l + arrOffset] = a[m + arrOffset];
                a[m + arrOffset] = t;
            }
            ti = indexes[vectorOffset];
            indexes[vectorOffset] = indexes[n + vectorOffset];
            indexes[n + vectorOffset] = ti;

            //downheap (0);
            v = 0;
            w = 1;
            while (w < n) {
                if (w + 1 < n && a[vlen * (w + 1) + varNum + arrOffset] > a[vlen * w + varNum + arrOffset])
                    w++;
                if (a[vlen * v + varNum + arrOffset] >= a[vlen * w + varNum + arrOffset])
                    break;
                for (k = 0, l = v * vlen, m = w * vlen; k < vlen; k++, l++, m++) {
                    t = a[l + arrOffset];
                    a[l + arrOffset] = a[m + arrOffset];
                    a[m + arrOffset] = t;
                }
                ti = indexes[v + vectorOffset];
                indexes[v + vectorOffset] = indexes[w + vectorOffset];
                indexes[w + vectorOffset] = ti;
                v = w;
                w = 2 * v + 1;
            }
        }
    }

    /**
     * Sorting by first element of a vector. Retrieves also index permutation.
     *
     * @param n       Number of vectors to sort
     * @param vlen    Vector length
     * @param a       Array to sort
     * @param indexes Array of indexes [length = a.length/vlen]
     */
    public static void heapsortFloat(int n, int vlen, float a[], int indexes[])
    {
        int k, l, m, v, w, ti;
        float t;
        //buildheap();
        for (v = n / 2 - 1; v >= 0; v--) //downheap (v);
        {
            w = 2 * v + 1;
            while (w < n) {
                if (w + 1 < n && a[vlen * (w + 1)] > a[vlen * w])
                    w++;
                if (a[vlen * v] >= a[vlen * w])
                    break;
                for (k = 0, l = v * vlen, m = w * vlen; k < vlen; k++, l++, m++) {
                    t = a[l];
                    a[l] = a[m];
                    a[m] = t;
                }
                ti = indexes[v];
                indexes[v] = indexes[w];
                indexes[w] = ti;
                v = w;
                w = 2 * v + 1;
            }
        }
        while (n > 1) {
            n--;
            for (k = 0, l = 0, m = n * vlen; k < vlen; k++, l++, m++) {
                t = a[l];
                a[l] = a[m];
                a[m] = t;
            }
            ti = indexes[0];
            indexes[0] = indexes[n];
            indexes[n] = ti;

            //downheap (0);
            v = 0;
            w = 1;
            while (w < n) {
                if (w + 1 < n && a[vlen * (w + 1)] > a[vlen * w])
                    w++;
                if (a[vlen * v] >= a[vlen * w])
                    break;
                for (k = 0, l = v * vlen, m = w * vlen; k < vlen; k++, l++, m++) {
                    t = a[l];
                    a[l] = a[m];
                    a[m] = t;
                }
                ti = indexes[v];
                indexes[v] = indexes[w];
                indexes[w] = ti;
                v = w;
                w = 2 * v + 1;
            }
        }
    }

    public static void heapsortFloat(int n, int vlen, float a[])
    {
        int k, l, m, v, w;
        float t;
        //buildheap();
        for (v = n / 2 - 1; v >= 0; v--) //downheap (v);
        {
            w = 2 * v + 1;
            while (w < n) {
                if (w + 1 < n && a[vlen * (w + 1)] > a[vlen * w])
                    w++;
                if (a[vlen * v] >= a[vlen * w])
                    break;
                for (k = 0, l = v * vlen, m = w * vlen; k < vlen; k++, l++, m++) {
                    t = a[l];
                    a[l] = a[m];
                    a[m] = t;
                }
                v = w;
                w = 2 * v + 1;
            }
        }
        while (n > 1) {
            n--;
            for (k = 0, l = 0, m = n * vlen; k < vlen; k++, l++, m++) {
                t = a[l];
                a[l] = a[m];
                a[m] = t;
            }
            //downheap (0);
            v = 0;
            w = 1;
            while (w < n) {
                if (w + 1 < n && a[vlen * (w + 1)] > a[vlen * w])
                    w++;
                if (a[vlen * v] >= a[vlen * w])
                    break;
                for (k = 0, l = v * vlen, m = w * vlen; k < vlen; k++, l++, m++) {
                    t = a[l];
                    a[l] = a[m];
                    a[m] = t;
                }
                v = w;
                w = 2 * v + 1;
            }
        }
    }

    public static void heapsortDouble(int n, int vlen, double a[])
    {
        int k, l, m, v, w;
        double t;
        //buildheap();
        for (v = n / 2 - 1; v >= 0; v--) //downheap (v);
        {
            w = 2 * v + 1;
            while (w < n) {
                if (w + 1 < n && a[vlen * (w + 1)] > a[vlen * w])
                    w++;
                if (a[vlen * v] >= a[vlen * w])
                    break;
                for (k = 0, l = v * vlen, m = w * vlen; k < vlen; k++, l++, m++) {
                    t = a[l];
                    a[l] = a[m];
                    a[m] = t;
                }
                v = w;
                w = 2 * v + 1;
            }
        }
        while (n > 1) {
            n--;
            for (k = 0, l = 0, m = n * vlen; k < vlen; k++, l++, m++) {
                t = a[l];
                a[l] = a[m];
                a[m] = t;
            }
            //downheap (0);
            v = 0;
            w = 1;
            while (w < n) {
                if (w + 1 < n && a[vlen * (w + 1)] > a[vlen * w])
                    w++;
                if (a[vlen * v] >= a[vlen * w])
                    break;
                for (k = 0, l = v * vlen, m = w * vlen; k < vlen; k++, l++, m++) {
                    t = a[l];
                    a[l] = a[m];
                    a[m] = t;
                }
                v = w;
                w = 2 * v + 1;
            }
        }
    }

    public static void main(String args[])
    {
        int n = 5;
        int d = 2 * n + 1;
        int[] s = new int[d * d];
        int[] indexes = new int[d * d];
        for (int i = 0, l = 0; i < d; i++) {
            for (int j = 0; j < d; j++, l++) {
                s[l] = (i - n) * (i - n) + (j - n) * (j - n);
                indexes[l] = l;
                System.out.printf("%4d %3d  ", s[l], indexes[l]);
            }
            System.out.println("");
        }
        heapsortInt(d * d, 1, s, indexes, 0, 0);
        for (int i = 0, l = 0; i < d; i++) {
            for (int j = 0; j < d; j++, l++) {
                System.out.printf("%3d  ", indexes[l]);
            }
            System.out.println("");
        }
    }

    private HeapSort()
    {
    }
}
