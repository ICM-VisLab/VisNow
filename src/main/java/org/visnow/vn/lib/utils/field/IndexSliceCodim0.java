/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams;
import static org.visnow.vn.lib.utils.interpolation.SubsetGeometryComponents.*;

/**
 *
 * @author know
 */


public class IndexSliceCodim0 extends IndexSlice
{
    private final int nDim;

    private IndexSliceCodim0(RegularField inField, IndexSliceParams params) throws Exception
    {
        this.inField = inField;
        this.params = params;
        inDims = inField.getDims();
        lDims =  inField.getLDims();
        nDim = inDims.length;
        low = params.getLow();
        up  = params.getUp();
        outDims = new int[nDim];
        nOutNodes = 1;
        for (int i = 0; i < nDim; i++) {
            outDims[i] = up[i] - low[i];
            nOutNodes *= outDims[i];
        }
        collectData(params.isAdjusting());
    }


    void computeSlicedData()
    {
        int[] xInDims   = new int[nDim];
        int[] xOutDims  = new int[nDim];
        int[] xlow      = new int[nDim];
        int[] dest      = new int[nDim];
        for (int i = 0; i < nDim; i++) {
            xInDims[i]  = inDims[i];
            xlow[i]     = low[i];
            xOutDims[i] = outDims[i];
            dest[i]     = 0;
        }
        for (int idata =  0; idata < inData.length; idata++) {
            int veclen = vlens[idata];
            xInDims[0] = veclen * inDims[0];
            xOutDims[0] = veclen * outDims[0];
            xlow[0] = veclen * low[0];
            if (inData[idata].getType() == LargeArrayType.FLOAT)
                LargeArrayUtils.subarraycopy(inData[idata], xInDims, xlow, outData[idata], xOutDims, dest, xOutDims);
            else {
                LargeArray tmp = LargeArrayUtils.create(inData[idata].getType(), outData[idata].length());
                LargeArrayUtils.subarraycopy(inData[idata], xInDims, xlow, tmp, xOutDims, dest, xOutDims);
                for (long i = 0; i < tmp.length(); i++)
                    inData[idata].setFloat(i, tmp.getFloat(i));
            }

        }
    }

    void addIndexCoords()
    {
        FloatLargeArray indexCoords= new  FloatLargeArray(nDim * nOutNodes);
        for (long i = 0, l = 0; i < nOutNodes; i++) {
            long ii = i;
            for (int k = 0; k < nDim; k++, l ++) {
                indexCoords.setFloat(l, (float)(low[k] + ii % outDims[k]));
                ii /= outDims[k];
            }
        }
        if (regularSlice.getComponent(INDEX_COORDS) != null)
            regularSlice.removeComponent(INDEX_COORDS);
        regularSlice.addComponent(DataArray.create(indexCoords, nDim, INDEX_COORDS));
    }


    public static RegularField slice(RegularField inField, IndexSliceParams params)
    {
        try {
            return (RegularField)(new IndexSliceCodim0(inField, params).slice(params.isAdjusting()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
