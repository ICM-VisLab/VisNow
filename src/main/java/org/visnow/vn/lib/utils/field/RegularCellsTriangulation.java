/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class RegularCellsTriangulation
{

    public static int[] triangulateRegularHex(int i0, int i1, int i2, int i3, int i4, int i5, int i6, int i7, boolean even)
    {
        if (even)
            return new int[]{
                i0, i2, i7, i5,
                i1, i2, i0, i5,
                i3, i0, i2, i7,
                i6, i5, i7, i2,
                i5, i4, i7, i0
            };
        else
            return new int[]{
                i1, i3, i4, i6,
                i0, i1, i3, i4,
                i2, i3, i1, i6,
                i4, i5, i1, i6,
                i7, i6, i4, i3
            };
    }

    public static int[] triangulateRegularQuad(int i0, int i1, int i2, int i3)
    {
        return new int[]{
            i0, i1, i2,
            i0, i2, i3
        };
    }

    private RegularCellsTriangulation()
    {
    }

}
