/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.fft;

import org.jtransforms.fft.FloatFFT_1D;
import org.jtransforms.fft.FloatFFT_2D;
import org.jtransforms.fft.FloatFFT_3D;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.utils.ScalarMath;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class FftJ1Core extends FftCore
{

    private FloatFFT_1D fft1;
    private FloatFFT_2D fft2;
    private FloatFFT_3D fft3;

    /**
     * Creates a new <code>FftJ1Core</code> object.
     */
    public FftJ1Core()
    {
    }

    @Override
    public void fft_r2c(FloatLargeArray real, FloatLargeArray imag)
    {
        long length = real.length();
        FloatLargeArray a = new FloatLargeArray(2l * length, false);
        LargeArrayUtils.arraycopy(real, 0, a, 0, length);
        fft1 = null;
        fft1 = new FloatFFT_1D(length);
        fft1.realForwardFull(a);
        for (long i = 0; i < length; i++) {
            real.setFloat(i, a.getFloat(2 * i));
            imag.setFloat(i, a.getFloat(2 * i + 1));
        }
    }

    @Override
    public void fft_c2c(FloatLargeArray real, FloatLargeArray imag)
    {
        long length = real.length();
        FloatLargeArray a = new FloatLargeArray(2 * length);
        for (long i = 0; i < length; i++) {
            a.setFloat(2 * i, real.getFloat(i));
            a.setFloat(2 * i + 1, imag.getFloat(i));
        }
        fft1 = null;
        fft1 = new FloatFFT_1D(length);
        fft1.complexForward(a);
        for (long i = 0; i < length; i++) {
            real.setFloat(i, a.getFloat(2 * i));
            imag.setFloat(i, a.getFloat(2 * i + 1));
        }
    }

    @Override
    public void ifft(FloatLargeArray real, FloatLargeArray imag)
    {
        long length = real.length();
        FloatLargeArray a = new FloatLargeArray(2 * length);
        for (long i = 0; i < length; i++) {
            a.setFloat(2 * i, real.getFloat(i));
            a.setFloat(2 * i + 1, imag.getFloat(i));
        }
        fft1 = null;
        fft1 = new FloatFFT_1D(length);
        fft1.complexInverse(a, true);
        for (long i = 0; i < length; i++) {
            real.setFloat(i, a.getFloat(2 * i));
            imag.setFloat(i, a.getFloat(2 * i + 1));
        }
    }

    @Override
    public void fft2_r2c(FloatLargeArray real, FloatLargeArray imag, long nx, long ny)
    {
        long length = nx * ny;
        FloatLargeArray a = new FloatLargeArray(2 * length);
        LargeArrayUtils.arraycopy(real, 0, a, 0, length);
        fft2 = null;
        fft2 = new FloatFFT_2D(ny, nx);
        fft2.realForwardFull(a);
        for (long i = 0; i < length; i++) {
            real.setFloat(i, a.getFloat(2 * i));
            imag.setFloat(i, a.getFloat(2 * i + 1));
        }

    }

    @Override
    public void fft2_c2c(FloatLargeArray real, FloatLargeArray imag, long nx, long ny)
    {
        long length = nx * ny;
        FloatLargeArray a = new FloatLargeArray(2 * length);
        for (long i = 0; i < length; i++) {
            a.setFloat(2 * i, real.getFloat(i));
            a.setFloat(2 * i + 1, imag.getFloat(i));
        }
        fft2 = null;
        fft2 = new FloatFFT_2D(ny, nx);
        fft2.complexForward(a);
        for (long i = 0; i < length; i++) {
            real.setFloat(i, a.getFloat(2 * i));
            imag.setFloat(i, a.getFloat(2 * i + 1));
        }
    }

    @Override
    public void ifft2(FloatLargeArray real, FloatLargeArray imag, long nx, long ny)
    {
        long length = nx * ny;
        FloatLargeArray a = new FloatLargeArray(2 * length);
        for (long i = 0; i < length; i++) {
            a.setFloat(2 * i, real.getFloat(i));
            a.setFloat(2 * i + 1, imag.getFloat(i));
        }
        fft2 = null;
        fft2 = new FloatFFT_2D(ny, nx);
        fft2.complexInverse(a, true);
        for (long i = 0; i < length; i++) {
            real.setFloat(i, a.getFloat(2 * i));
            imag.setFloat(i, a.getFloat(2 * i + 1));
        }
    }

    @Override
    public void fft3_r2c(FloatLargeArray real, FloatLargeArray imag, long nx, long ny, long nz)
    {
        long length = nx * ny * nz;
        FloatLargeArray a = new FloatLargeArray(2 * length);
        LargeArrayUtils.arraycopy(real, 0, a, 0, length);
        fft3 = null;
        fft3 = new FloatFFT_3D(ny, nx, nz);
        fft3.realForwardFull(a);
        for (long i = 0; i < length; i++) {
            real.setFloat(i, a.getFloat(2 * i));
            imag.setFloat(i, a.getFloat(2 * i + 1));
        }

    }

    @Override
    public void fft3_c2c(FloatLargeArray real, FloatLargeArray imag, long nx, long ny, long nz)
    {
        long length = nx * ny * nz;
        FloatLargeArray a = new FloatLargeArray(2 * length);
        for (long i = 0; i < length; i++) {
            a.setFloat(2 * i, real.getFloat(i));
            a.setFloat(2 * i + 1, imag.getFloat(i));
        }
        fft3 = null;
        fft3 = new FloatFFT_3D(ny, nx, nz);
        fft3.complexForward(a);
        for (long i = 0; i < length; i++) {
            real.setFloat(i, a.getFloat(2 * i));
            imag.setFloat(i, a.getFloat(2 * i + 1));
        }
    }

    @Override
    public void ifft3(FloatLargeArray real, FloatLargeArray imag, long nx, long ny, long nz)
    {
        long length = nx * ny * nz;
        FloatLargeArray a = new FloatLargeArray(2 * length);
        for (long i = 0; i < length; i++) {
            a.setFloat(2 * i, real.getFloat(i));
            a.setFloat(2 * i + 1, imag.getFloat(i));
        }
        fft3 = null;
        fft3 = new FloatFFT_3D(ny, nx, nz);
        fft3.complexInverse(a, true);
        for (long i = 0; i < length; i++) {
            real.setFloat(i, a.getFloat(2 * i));
            imag.setFloat(i, a.getFloat(2 * i + 1));
        }
    }

}
