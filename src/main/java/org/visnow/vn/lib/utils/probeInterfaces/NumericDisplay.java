/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.probeInterfaces;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.util.Vector;
import org.jogamp.java3d.J3DGraphics2D;

/**
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */

public class NumericDisplay {
    
    public static boolean isDark(Color c)
    {
        float[] v = new float[3];
        c.getRGBColorComponents(v);
        return 0.299 * v[0] + 0.587 * v[1] + 0.114 * v[2] < .5;
    }
    
    /**
     * 
     * @param gr graphics context of the display
     * @param xPos x position of the anchor point (left side of the rectangle if possible, right side if too close to the window right border)
     * @param yPos y position of the anchor point (bottom of the rectangle if possible, top if too close to the window upper border)
     * @param names component names
     * @param vals  formatted component values
     * @param width display window width
     * @param height display window height
     * @param fontHeight display font size 
     * @param bgr background color
     * @param bgrOpacity background opacity
     * @param colors colors of the display of components
     * @return position, width and height of drawn rectangle
     */
    public  static int[] displayPointStrings(J3DGraphics2D gr, int xPos, int yPos, 
                                             Vector<String> names, Vector<String> vals, 
                                             int width, int height, int fontHeight,
                                             Color bgr, float bgrOpacity, Vector<Color> colors)
    {
        gr.setFont(new java.awt.Font(Font.DIALOG_INPUT, 0, fontHeight));
        FontMetrics fm = gr.getFontMetrics();
        int nameWidth = 0;
        for (String name : names) {
            int w = fm.stringWidth(name);
            if (w > nameWidth)
                nameWidth = w;
        }
        for (String val : vals) {
            int w = fm.stringWidth(val);
            if (w > nameWidth)
                nameWidth = w;
        }
        nameWidth += 8;
        int itemWidth = fm.stringWidth("000000.000");
        int rectWidth = itemWidth + nameWidth + 15;
        int rectHeight = names.size() * (fontHeight + 2) + 8;
        if (yPos < rectHeight)
            yPos += rectHeight;
        if (xPos + rectWidth > width)
            xPos -= rectWidth;
        float[] colCmp = new float[3];
        bgr.getRGBColorComponents(colCmp);
        gr.setColor(new Color(colCmp[0], colCmp[1], colCmp[2], .5f + bgrOpacity / 2));
        gr.fillRect(xPos, yPos - rectHeight, rectWidth, rectHeight);
        if (isDark(bgr))
            gr.setColor(Color.LIGHT_GRAY);
        else
            gr.setColor(Color.DARK_GRAY);
        gr.drawRect(xPos, yPos - rectHeight, rectWidth, rectHeight);
        int y = yPos - rectHeight + 3 + fontHeight;
        for (int i = 0; i <  names.size(); i++) {
            if (colors != null)
                gr.setColor(colors.get(Math.min(i, colors.size() - 1)));
            gr.drawString(names.get(i), xPos + 3, y);
            gr.drawString(vals.get(i),  xPos + 3+ nameWidth, y);
            y += fontHeight + 2;
        }
        return new int[] {xPos, yPos, rectWidth, rectHeight};
    }
    
    /**
     * 
     * @param gr graphics context of the display
     * @param xPos x position of the anchor point (left side of the rectangle if possible, right side if too close to the window right border)
     * @param yPos y position of the anchor point (bottom of the rectangle if possible, top if too close to the window upper border)
     * @param names component names
     * @param vals  component values
     * @param width display window width
     * @param height display window height
     * @param fontHeight display font size 
     * @param bgr background color
     * @param bgrOpacity background opacity
     * @param colors colors of the display of components
     */
    public  static int[] displayPointValues(J3DGraphics2D gr, int xPos, int yPos, 
                                           Vector<String> names, Vector<float[]> vals, 
                                           int width, int height, int fontHeight,
                                           Color bgr, float bgrOpacity, Vector<Color> colors)
    {
        Vector<String> items = new Vector<>();
        for (float[] val : vals) {
            StringBuilder b = new StringBuilder();
            int vLen = val.length;
            for (int j = 0; j < vLen; j++) 
                b.append(String.format("%10.3f  ",val[j]));
            items.add(b.toString());
        }
        return displayPointStrings(gr, xPos, yPos, names, items, width, height, fontHeight, bgr, bgrOpacity, colors);
    }
    
    /**
     * Convenience method creating values display with default relative font size and color
     * @param gr graphics context of the display
     * @param xPos x position of the anchor point (left side of the rectangle if possible, right side if too close to the window right border)
     * @param yPos y position of the anchor point (bottom of the rectangle if possible, top if too close to the window upper border)
     * @param names component names
     * @param vals  component values
     * @param width display window width
     * @param height display window height
     * @param fontRelativeHeight
     * @param fgr foreground (texts and frame color
     * @param bgr background color
     * @param bgrOpacity opacity of label background
     */
    public  static int[] displayPointValues(J3DGraphics2D gr, int xPos, int yPos, 
                                           Vector<String> names, Vector<float[]> vals, 
                                           int width, int height, 
                                           float fontRelativeHeight, Color fgr, 
                                           Color bgr, float bgrOpacity)
    {
        Vector<Color> colors = new Vector<>();
        colors.add(fgr);
        return displayPointValues(gr, xPos, yPos,  names, vals, width, height, (int)(fontRelativeHeight * height),
                           bgr, .5f, colors);
    }
    
    /**
     * Convenience method creating values display with default relative font size and color
     * @param gr graphics context of the display
     * @param xPos x position of the anchor point (left side of the rectangle if possible, right side if too close to the window right border)
     * @param yPos y position of the anchor point (bottom of the rectangle if possible, top if too close to the window upper border)
     * @param names component names
     * @param vals  component values
     * @param width display window width
     * @param height display window height
     * @param bgr background color
     */
    public  static int[] displayPointValues(J3DGraphics2D gr, int xPos, int yPos, 
                                           Vector<String> names, Vector<float[]> vals, 
                                           int width, int height, Color bgr)
    {
        return displayPointValues(gr, xPos, yPos, names, vals, width, height, .013f, 
                           isDark(bgr) ? Color.LIGHT_GRAY : Color.DARK_GRAY, bgr, .5f);
    }
    /**
     * Convenience method creating values display with default relative font size and color
     * @param gr graphics context of the display
     * @param xPos x position of the anchor point (left side of the rectangle if possible, right side if too close to the window right border)
     * @param yPos y position of the anchor point (bottom of the rectangle if possible, top if too close to the window upper border)
     * @param names component names
     * @param vals  component values
     * @param width display window width
     * @param height display window height
     * @param fontRelativeHeight
     * @param fgr foreground (texts and frame color
     * @param bgr background color
     * @param bgrOpacity opacity of label background
     */
    public  static int[] displayPointStrings(J3DGraphics2D gr, int xPos, int yPos, 
                                           Vector<String> names, Vector<String> vals, 
                                           int width, int height, 
                                           float fontRelativeHeight, Color fgr, 
                                           Color bgr, float bgrOpacity)
    {
        Vector<Color> colors = new Vector<>();
        colors.add(fgr);
        return displayPointStrings(gr, xPos, yPos,  names, vals, width, height, (int)(fontRelativeHeight * height),
                           bgr, .5f, colors);
    }
    
    /**
     * Convenience method creating values display with default relative font size and color
     * @param gr graphics context of the display
     * @param xPos x position of the anchor point (left side of the rectangle if possible, right side if too close to the window right border)
     * @param yPos y position of the anchor point (bottom of the rectangle if possible, top if too close to the window upper border)
     * @param names component names
     * @param vals  component values
     * @param width display window width
     * @param height display window height
     * @param bgr background color
     */
    public  static int[] displayPointStrings(J3DGraphics2D gr, int xPos, int yPos, 
                                           Vector<String> names, Vector<String> vals, 
                                           int width, int height, Color bgr)
    {
        return displayPointStrings(gr, xPos, yPos, names, vals, width, height, .013f, 
                           isDark(bgr) ? Color.LIGHT_GRAY : Color.DARK_GRAY, bgr, .5f);
    }
}
