/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class PullBackArray1D
{

    private PullBackArray1D()
    {
    }
    
    /**
     * computes pull-back of data array from a trivially regular field by a mapping
     * defined by node coordinates using nearest neighbor interpolation
     * <p>
     * @param targetArray data array pulled back
     * @param targetDims  coordinates of mapped nodes (in index space of mapping target field)
     * @param coords      coordinates of mapped nodes
     * <p>
     * @return
     */
    public static DataArray pullBack3D(DataArray targetArray, int[] targetDims, float[] coords)
    {
        int n = coords.length / 3;
        LargeArray tData = targetArray.getRawArray();
        LargeArray outBData = LargeArrayUtils.create(tData.getType(), n, false);
        for (int i = 0; i < n; i++) {
            int i0 = (int) (.5 + coords[3 * i]);
            if (i0 < 0)
                i0 = 0;
            if (i0 >= targetDims[0])
                i0 = targetDims[0] - 1;
            outBData.set(i, tData.get(i0));
        }
        return (DataArray.create(outBData, 1, "" + targetArray.getName() + " pullback"));
    }

    /**
     * computes pull-back of data array from a trivially regular field by a mapping
     * defined by node coordinates using nearest neighbor interpolation
     * <p>
     * @param targetArray data array pulled back
     * @param targetDims  coordinates of mapped nodes (in index space of mapping target field)
     * @param coords      coordinates of mapped nodes
     * <p>
     * @return
     */
    public static DataArray pullBack2D(DataArray targetArray, int[] targetDims, float[] coords)
    {
        int n = coords.length / 2;
        LargeArray tData = targetArray.getRawArray();
        LargeArray outBData = LargeArrayUtils.create(tData.getType(), n, false);
        for (int i = 0; i < n; i++) {
            int i0 = (int) (.5 + coords[2 * i]);
            if (i0 < 0)
                i0 = 0;
            if (i0 >= targetDims[0])
                i0 = targetDims[0] - 1;
            outBData.set(i, tData.get(i0));
        }
        return (DataArray.create(outBData, 1, "" + targetArray.getName() + " pullback"));
    }

    /**
     * computes pull-back of data array from a trivially regular field by a mapping
     * defined by node coordinates using nearest neighbor interpolation
     * <p>
     * @param targetArray data array pulled back
     * @param targetDims  coordinates of mapped nodes (in index space of mapping target field)
     * @param coords      coordinates of mapped nodes
     * <p>
     * @return
     */
    public static DataArray pullBack1D(DataArray targetArray, int[] targetDims, float[] coords)
    {
        int n = coords.length;
        LargeArray tData = targetArray.getRawArray();
        LargeArray outBData = LargeArrayUtils.create(tData.getType(), n, false);
        for (int i = 0; i < n; i++) {
            int i0 = (int) (.5 + coords[i]);
            if (i0 < 0)
                i0 = 0;
            if (i0 >= targetDims[0])
                i0 = targetDims[0] - 1;
            outBData.set(i, tData.get(i0));
        }
        return (DataArray.create(outBData, 1, "" + targetArray.getName() + " pullback"));
    }
    
}
