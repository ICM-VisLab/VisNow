/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils;

import java.util.Arrays;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.vn.lib.utils.numeric.SGapproximation.PolynomialApproximation;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class InterpolateToMesh
{

    private InterpolateToMesh()
    {
    }

    public static class Interpolate implements Runnable
    {

        private int nThreads = 1;
        private int iThread = 0;
        private int[] inDims;
        private int[] outDims;
        private float[] inData = null;
        private float[] outData = null;
        private float[] coords;

        public Interpolate(int nThreads, int iThread, int[] inDims, int[] outDims, float[] inData, float[] outData, float[] coords)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.inDims = inDims;
            this.outDims = outDims;
            this.inData = inData;
            this.outData = outData;
            this.coords = coords;
        }

        @Override
        public void run()
        {
            for (int i2 = (iThread * outDims[2]) / nThreads, l = i2 * outDims[1] * outDims[0]; i2 < ((iThread + 1) * outDims[2]) / nThreads; i2++) {
                for (int i1 = 0; i1 < outDims[1]; i1++)
                    for (int i0 = 0; i0 < outDims[0]; i0++, l++) {
                        float[] x = new float[3];
                        System.arraycopy(coords, 3 * l, x, 0, 3);
                        outData[l] = PolynomialApproximation.coeffs(inData, null, inDims, x, 2, 1, 2)[0];
                    }
            }
        }
    }

    public static void updateOutField(RegularField inField, Field outField)
    {
        int trueNSpace = inField.getTrueNSpace();
        if (trueNSpace < 0) {
            System.out.println("input field has improper true space field");
            return;
        }
            
        LogicLargeArray valid = new LogicLargeArray((int) outField.getNNodes());
        float[] coords = outField.getCurrentCoords() == null ? null : outField.getCurrentCoords().getData();
        if (coords == null) {
            if (outField instanceof RegularField)
                coords = ((RegularField) outField).getCoordsFromAffine().getData();
            else
                return;
        }
        float[] p = new float[3], q = new float[3];
        Arrays.fill(p, 0);
        Arrays.fill(q, 0);
        if (inField.getCurrentCoords() == null) {
            float[][] affine = inField.getAffine();
            float[] origin = affine[3];
            float[][] invAffine = inField.getInvAffine();
            for (DataArray da : inField.getComponents()) {
                if (!da.isNumeric())
                    continue;
                int vlen = da.getVectorLength();
                LargeArray inBD = da.getRawArray();
                LargeArray outBD = LargeArrayUtils.create(inBD.getType(), vlen * (int) outField.getNNodes(), false);
                for (int i = 0; i < outField.getNNodes(); i++) {
                    for (int j = 0; j < trueNSpace; j++)
                        p[j] = coords[3 * i + j] - origin[j];
                    for (int j = 0; j < trueNSpace; j++) {
                        q[j] = 0;
                        for (int k = 0; k < trueNSpace; k++)
                            q[j] += invAffine[k][j] * p[k];
                    }
                    Object bd = inField.getInterpolatedData(inBD, q[0], q[1], q[2]);
                    LargeArrayUtils.arraycopy(bd, 0, outBD, vlen * i, vlen);
                }
                outField.addComponent(DataArray.create(outBD, vlen, da.getName()));
            }
            int[] dims = inField.getDims();
            for (int i = 0; i < outField.getNNodes(); i++) {
                valid.setBoolean(i, true);
                for (int j = 0; j < trueNSpace; j++)
                    p[j] = coords[3 * i + j] - origin[j];
                for (int j = 0; j < trueNSpace; j++) {
                    q[j] = 0;
                    for (int k = 0; k < trueNSpace; k++)
                        q[j] += invAffine[k][j] * p[k];
                }
                for (int j = 0; j < trueNSpace; j++)
                    if (q[j] < 0 || q[j] >= dims[j])
                        valid.setBoolean(i, false);
            }
            outField.setCurrentMask(valid);
        }
    }
}
