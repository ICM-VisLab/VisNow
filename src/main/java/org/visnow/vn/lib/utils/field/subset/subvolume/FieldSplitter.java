/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field.subset.subvolume;

import java.util.Arrays;
import java.util.HashMap;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.Field;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;
import static org.visnow.jscic.cells.CellType.QUAD;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 */
public class FieldSplitter
{
    public static final int SLICE_VOID = -2;
    public static final int SLICE_NONTRIVIAL = -1;
    protected enum Subset {VOID, NONTRIVIAL, TRIANGULATED, WHOLE};
    
    public static class PreservedNodes 
    {
        private int nPreservedNodes = 0;
        private final int[] indices;

        public PreservedNodes(int nNodes)
        {
            indices = new int[nNodes];
            Arrays.fill(indices, -1);
        }
        
        public int addNode(int n)
        {
            if (n < 0 || n >= indices.length) {
                System.out.println("bad new index "+n+"   "+indices.length);
                return -1;
            }
            if (indices[n] < 0) {
                indices[n] = nPreservedNodes;
                nPreservedNodes += 1;
            }
            return indices[n];
        }

        public int getnPreservedNodes()
        {
            return nPreservedNodes;
        }

        public int[] getIndices()
        {
            return indices;
        }
    }

    public static class NewCell
    {
        final CellType type;
        final int[] verts;
        final NewNode[] newVerts;
        final float[] normal;
        final int dataIndex;
        boolean orientation;

    /**
     * creates new cell in a field
     * @param type      new cell type
     * @param inVerts   vertices of the new cell
     * @param addVerts  new vertices (located on field edges - to be interpolated); inVerts at corresponding positions will be set to -1
     * @param normal    for 2D cells, a versor used for determining new cell orientation (original cell normal or normal at the first vertex)
     * @param dataIndex index to data in the cell set
     * @param oldNodes
     */
        public NewCell(CellType type, int[] inVerts, NewNode[] addVerts, float[] normal, int dataIndex, PreservedNodes oldNodes)
        {
            this.type = type;
            verts = new int[inVerts.length];
            newVerts = new NewNode[addVerts.length];
            for (int i = 0; i < verts.length; i++) 
                if (addVerts[i] != null) {
                    newVerts[i] = addVerts[i];
                    verts[i] = -1;
                }
                else {
                    newVerts[i] = null;
                    verts[i] = oldNodes.addNode(inVerts[i]);
                }
            this.normal = normal;
            this.dataIndex = dataIndex;
        }
    }
    
    protected static Subset subset(CellType type, float[] vals)
    {
        boolean allOver = true, isStrictlyOver = false;
        for (int i = 0; i < vals.length; i++) {
            if (vals[i] < 0)
                allOver = false;
            if (vals[i] > 0)
                isStrictlyOver = true;
        }
        if (allOver) 
            return (type.isSimplex() || type == QUAD) ? Subset.WHOLE : Subset.TRIANGULATED;
        else if (isStrictlyOver)     
            return Subset.NONTRIVIAL;
        else
            return Subset.VOID;
    }
  
    protected static Subset slice(CellType type, float[] vals)
    {
        int strictlyOver = 0, strictlyUnder = 0;
        for (int i = 0; i < vals.length; i++) {
            if (vals[i] < 0)
                strictlyUnder += 1;
            if (vals[i] > 0)
                strictlyOver += 1;
        }
        if (strictlyOver > 0 && strictlyUnder > 0)                    // cell is cut
            return Subset.NONTRIVIAL;
        if (strictlyOver + strictlyUnder <= vals.length - type.getDim()) {  // some face can be part of the slice 
            return Subset.NONTRIVIAL;
        }
        return Subset.VOID;
    }
        
    protected static FloatLargeArray interpolateCoords(Field in, int nOutNodes, int[] retainedNodeIndices, HashMap<Long, NewNode> newNodes)
    {
        FloatLargeArray coords = in.getCurrentCoords();
        FloatLargeArray outCoords = new FloatLargeArray(3 * nOutNodes);
        for (int i = 0; i < retainedNodeIndices.length; i++)
            if (retainedNodeIndices[i] >= 0) 
                LargeArrayUtils.arraycopy(coords, 3 * i, outCoords, 3 * retainedNodeIndices[i], 3);
        for (NewNode node : newNodes.values()) {
            long k0 = node.p0;
            long k1 = node.p1;
            int k = 3 * node.getIndex();
            float r = node.ratio;
            for (int j = 0; j < 3; j++, k++)
                outCoords.setFloat(k, r * coords.getFloat(3 * k0 + j) + (1 - r) * coords.getFloat(3 * k1 + j));
        }
        return outCoords;
    }
    
    protected static Cell outputCell(NewCell cell, FloatLargeArray coords)
    {
        if (cell.verts.length != cell.type.getNVertices())
            return null;
        CellType type = cell.type;
        int[] origVerts = cell.verts;
        int[] finalVerts = Arrays.copyOf(origVerts, origVerts.length);
        for (int i = 0; i < finalVerts.length; i++) {
            if (origVerts[i] < 0)
                finalVerts[i] = cell.newVerts[i].getIndex();
            if (finalVerts[i] < 0 || finalVerts[i] >= coords.length() / 3)
                System.out.println("????");
        }
        Cell outCell = Cell.createCell(type, finalVerts, (byte)0);
        float[] g = outCell.generalizedGeomOrientation(coords);
        switch (type.getDim()) {
        case 3:
            outCell.setOrientation(g[0] > 0 ? (byte)1 : (byte)0);
            break;
        case 2:
            float s = g[0] * cell.normal[0] + g[1] * cell.normal[1] + g[2] * cell.normal[2];
            outCell.setOrientation(s > 0 ? (byte)1 : (byte)0);
            break;
        default:
        }
        return outCell;
    }
    
    protected static void interpolateData(Field inField, Field outField, int nOutNodes, int[] retainedNodeIndices, HashMap<Long, NewNode> newNodes)
    {
    for (int iComponent = 0; iComponent < inField.getNComponents(); iComponent++)
        if (inField.getComponent(iComponent).isNumeric()) {
            DataArray da = inField.getComponent(iComponent);
            int veclen = da.getVectorLength();
            DataArrayType type = da.getType();
            LargeArrayType laType = type.toLargeArrayType();
            TimeData inTimeData = da.getTimeData();
            TimeData outTimeData = new TimeData(type);
            DataArray outDa = DataArray.create(type, nOutNodes, veclen, da.getName(), 
                                               da.getUnit(), da.getUserData());
            float[] timeSteps = inTimeData.getTimesAsArray();
            for (int iStep = 0; iStep < da.getNFrames(); iStep++) {
                LargeArray inTD = inTimeData.getValue(timeSteps[iStep]);
                LargeArray outTD = LargeArrayUtils.create(laType, veclen * nOutNodes);

                for (int i = 0; i < retainedNodeIndices.length; i++)
                    if (retainedNodeIndices[i] >= 0) 
                        LargeArrayUtils.arraycopy(inTD, veclen * i, outTD, veclen * retainedNodeIndices[i], veclen);
                for (NewNode node : newNodes.values()) {
                    long k0 = veclen * node.p0;
                    long k1 = veclen * node.p1;
                    int k = veclen * node.getIndex();
                    float r = node.ratio;
                    for (int j = 0; j < veclen; j++, k++)
                        outTD.setFloat(k, r * inTD.getFloat(k0 + j) + (1 - r) * inTD.getFloat(k1 + j));
                }
                outTimeData.setValue(outTD, timeSteps[iStep]);
            }
            outDa.setPreferredRanges(da.getPreferredMinValue(),     da.getPreferredMaxValue(), 
                                     da.getPreferredPhysMinValue(), da.getPreferredPhysMaxValue());
            outDa.setTimeData(outTimeData);
            outField.addComponent(outDa);
        }
    }
    
}
