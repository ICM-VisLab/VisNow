/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import java.util.Arrays;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */


abstract public class IndexSlice 
{
    protected RegularField inField = null;
    protected RegularField regularSlice = null;
    protected int[] inDims;
    protected long[] lDims;
    protected IndexSliceParams params;
    protected int[] low, up, outDims;
    protected int axis;
    protected float[] fSlice;
    protected int nOutNodes = 0;
    protected float[][] sliceAffine = new float[4][3];
    protected FloatLargeArray outCoords;
    protected int[] off;
    protected int nData;
    protected int nDataItems = 0;
    protected LargeArray[] inData = null;
    protected DataArray[] outDataArrs;
    protected LargeArray[] outData;
    protected int[] vlens;
    
    protected int lastNOutNodes = -1;
    
    protected void collectData(boolean singleVal)
    {
        nData = inField.getNComponents();
        outDataArrs = new DataArray[nData];
        nDataItems = 0;
        for (int i =  0; i < nData; i++) {
            DataArray data = inField.getComponent(i);
            if (data == null || !data.isNumeric())
                continue;
            if (singleVal) 
                nDataItems += 1;
            else
                nDataItems += data.getTimeData().getNSteps();
        }
        if (inField.hasCoords()) {
            if (singleVal) 
                nDataItems += 1;
            else
                nDataItems += inField.getCoords().getNSteps();
        }
        inData  = new LargeArray[nDataItems];
        outData = new FloatLargeArray[nDataItems];
        vlens   = new int[nDataItems];
        int iDataItem = 0;
        for (int idata =  0;  idata < nData; idata++) {
            DataArray data = inField.getComponent(idata);
            if (data == null || !data.isNumeric())
                continue;
            int vlen = data.getVectorLength();
            if (singleVal) {
                vlens[iDataItem]  = vlen;
                inData[iDataItem] = data.getRawArray();
                iDataItem += 1;
            }
            else {
                TimeData timeData = data.getTimeData();
                for (int i = 0; i < timeData.getNSteps(); i++) {
                    vlens[iDataItem]  = vlen;
                    inData[iDataItem] = timeData.getValue(i);
                    iDataItem += 1;
                }
            }
        }
        if (inField.hasCoords()) {
            if (singleVal) {
                vlens[iDataItem]  = 3;
                inData[iDataItem] = inField.getCurrentCoords();
                iDataItem += 1;
            }
            else {
                TimeData timeData = inField.getCoords();
                for (int i = 0; i < timeData.getNSteps(); i++) {
                    vlens[iDataItem]  = 3;
                    inData[iDataItem] = timeData.getValue(i);
                    iDataItem += 1;
                }
            }
        }
    }
    
    
    abstract void computeSlicedData();
    
    abstract void addIndexCoords();
    
    protected  Field slice(boolean singleVal) 
    {
        for (int iDataItem = 0; iDataItem < nDataItems; iDataItem++)
            outData[iDataItem] = new FloatLargeArray(vlens[iDataItem] * nOutNodes);
        for (int i =  0, idata =  0 ; i < nData; i++) {
            DataArray data = inField.getComponent(i);
            if (data == null || !data.isNumeric())
                continue;
            outDataArrs[idata] = DataArray.create(DataArrayType.FIELD_DATA_FLOAT, nOutNodes, data.getVectorLength(), 
                                                  data.getName(), data.getUnit(), 
                                                  data.getUserData());
            idata += 1;
        }
        computeSlicedData();
        regularSlice = new RegularField(outDims);
        int idata = 0;
        for (int i =  0; i < nData; i++) {
            DataArray data = inField.getComponent(i);
            if (data == null || !data.isNumeric())
                continue;
            
            if (singleVal) {
                regularSlice.addComponent(DataArray.create(outData[idata], 
                                                           data.getVectorLength(), 
                                                           data.getName(), 
                                                           data.getUnit(), 
                                                           data.getUserData()).
                                           preferredRanges(data.getPreferredMinValue(), 
                                                           data.getPreferredMaxValue(), 
                                                           data.getPreferredPhysMinValue(), 
                                                           data.getPreferredPhysMaxValue()));
                idata += 1;
            }
            else {
                TimeData timeData = data.getTimeData();
                TimeData outTimeData = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
                int nTimeSteps = timeData.getNSteps();
                for (int j = 0; j < nTimeSteps; j++, idata++) 
                    outTimeData.setValue(outData[idata], data.getTime(j));
                regularSlice.addComponent(DataArray.create(outTimeData, 
                                                           data.getVectorLength(), 
                                                           data.getName(), 
                                                           data.getUnit(), 
                                                           data.getUserData()).
                                           preferredRanges(data.getPreferredMinValue(), 
                                                           data.getPreferredMaxValue(), 
                                                           data.getPreferredPhysMinValue(), 
                                                           data.getPreferredPhysMaxValue()));
            }
        }
        if (inField.hasCoords()) {
            if (singleVal) 
                regularSlice.setCurrentCoords((FloatLargeArray)outData[idata]);
            else {
                TimeData outTimeCoords = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
                for (int i = 0; i < inField.getCoords().getNSteps(); i++, idata++) 
                    outTimeCoords.setValue(outData[idata], inField.getCoords().getTime(i));
                regularSlice.setCoords(outTimeCoords);
            }
        }
        else {
            float[][] affine = inField.getAffine();
            float[] pos = params.getPosition();
            boolean[] fixed = params.getFixed();
            int[] plow = params.getLow();
            for (float[] fs : sliceAffine) 
                Arrays.fill(fs, 0);
            for (int i = 0; i < 3; i++) {
                sliceAffine[3][i] = affine[3][i];
                for (int j = 0, k = 0; j < inField.getDimNum(); j++)
                    if (fixed[j]) 
                        sliceAffine[3][i] += pos[j] * affine[j][i];
                    else {
                        sliceAffine[3][i] += plow[j] * affine[j][i];
                        sliceAffine[k][i] = affine[j][i];
                        k += 1;
                    }
            }
            regularSlice.setAffine(sliceAffine);
        }
        addIndexCoords();
        return regularSlice;
    }
    
}
