/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.flowVisualizationUtils;


import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.lib.gui.FieldBasedUI.DownsizeUI.DownsizeParams;
import org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class SeedPointParams extends Parameters
{

    public enum SelectedFieldType {NULL, FIELD, REGULAR_FIELD};

    public enum SeedPointsSelection {WHOLE,
                                     PLANE_SLICE, LINE_SLICE,
                                     POINT,
                                     LINEAR_SLICE,
                                     PLANAR_SLICE};

    public static final int PREFERRED_NUMBER_OF_START_POINTS = 100;

    public static final String SOURCE         = "source";
    public static final String DOWNSIZE       = "downsize";
    public static final String TYPE           = "type";
    public static final String TRUE_DIM       = "true dim";
    public static final String COORDS         = "coords";
    public static final String PICKED_NODE    = "picked node";
    public static final String PREFERRED_SIZE = "preferred size";

    protected static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Boolean>(SOURCE,           ParameterType.dependent, true),
        new ParameterEgg<SeedPointsSelection>(TYPE, ParameterType.dependent, SeedPointsSelection.WHOLE),
        new ParameterEgg<Integer>(TRUE_DIM,         ParameterType.independent, 3),
        new ParameterEgg<Integer>(PREFERRED_SIZE,   ParameterType.independent, PREFERRED_NUMBER_OF_START_POINTS),
        new ParameterEgg<float[]>(COORDS,           ParameterType.dependent, new float[] {0, 0, 0})};

    protected Field inputField = null;
    protected Field auxField = null;
    protected SelectedFieldType selectedFieldType = SelectedFieldType.NULL;
    protected int selectedFieldTrueDim            = -1;
    protected int[] inputDims;
    protected int[] auxDims;
    protected int[] selDims;
    protected int[] rawSeedDims;
    protected long inputNNodes, auxNNodes, selNNodes, rawSeedNNodes;
    protected Field seedField = null;
    protected IndexSliceParams indexSliceParams = new IndexSliceParams();
    protected DownsizeParams downsizeParams = new DownsizeParams(1, 10000, 100, true);
    protected SeedPointsGUI gui = null;

    public  IndexSliceParams.Type getIndexedType()
    {
        if (selectedFieldTrueDim < 0)
            return IndexSliceParams.Type.UNDEFINED;
        switch (getSelectionType()) {
        case WHOLE:
            return IndexSliceParams.Type.WHOLE;
        case PLANE_SLICE:
            return IndexSliceParams.Type.PLANE;
        case LINE_SLICE:
            return IndexSliceParams.Type.LINE;
        case POINT:
            return IndexSliceParams.Type.POINT;
        }
        return IndexSliceParams.Type.UNDEFINED;
    }

    public SeedPointParams()
    {
        super(eggs);
        setValue(COORDS, new float[]{0, 0, 0});
    }

    private void updateCrdsParams()
    {
        float[] crds = new float[selDims.length];
        float[] lcrds = new float[selDims.length];
        for (int i = 0; i < selDims.length; i++)
            lcrds[i] = crds[i] = .5f * selDims[i];
        setParameterActive(false);
        setValue(COORDS, crds);
        updateSelectionParams();
        if (gui != null)
            gui.updateFromParams();
        setParameterActive(true);
    }

    public void setInputField(Field field)
    {
        active = false;
        parameterActive = false;
        inputField = field;
        if (inputField == null) {
            selectedFieldType = SelectedFieldType.NULL;
            selectedFieldTrueDim = -1;
            return;
        }
        if (inputField instanceof RegularField)
            inputDims = ((RegularField)inputField).getDims();
        else
            inputDims = null;
        inputNNodes = inputField.getNNodes();
        if (field !=null && (fromInputField() || auxField == null)) {
            setFromInputField(true);
            selectedFieldType = inputDims == null ? SelectedFieldType.FIELD : SelectedFieldType.REGULAR_FIELD;
            selectedFieldTrueDim = inputField.getTrueNSpace();
            setValue(TRUE_DIM, selectedFieldTrueDim);
            selNNodes = inputNNodes;
            if (inputField instanceof RegularField) {
                selDims   = inputDims;
                updateCrdsParams();
            }
            else
                updateSelectionParams();
        }
        active = true;
        parameterActive = true;
    }

    public void setAuxField(Field field)
    {
        active = false;
        auxField = field;
        if (auxField == null) {
            if (inputField == null) {
                selectedFieldType = SelectedFieldType.NULL;
                selectedFieldTrueDim = -1;
                return;
            }
            setFromInputField(true);
            selectedFieldType = inputDims == null ? SelectedFieldType.FIELD : SelectedFieldType.REGULAR_FIELD;
            selectedFieldTrueDim = inputField.getTrueNSpace();
            setValue(TRUE_DIM, selectedFieldTrueDim);
            selNNodes = inputNNodes;
            if (inputField instanceof RegularField) {
                selDims   = inputDims;
                updateCrdsParams();
            }
            else
                updateSelectionParams();
        }
        else {
            if (auxField instanceof RegularField)
                auxDims = ((RegularField)auxField).getDims();
            else
                auxDims = null;
            auxNNodes = auxField.getNNodes();
            if (auxField instanceof RegularField)
                auxDims = ((RegularField)auxField).getDims();
            else
                auxDims = null;
            auxNNodes = auxField.getNNodes();
            if (field !=null && !fromInputField())  {
                selectedFieldType = selDims == null ? SelectedFieldType.FIELD : SelectedFieldType.REGULAR_FIELD;
                selectedFieldTrueDim = auxField.getTrueNSpace();
                setValue(TRUE_DIM, selectedFieldTrueDim);
                selNNodes = auxNNodes;
                if (auxField instanceof RegularField) {
                    selDims   = auxDims;
                    updateCrdsParams();
                }
                else
                    updateSelectionParams();
            }
        }
        if (gui != null)
            gui.updateFromParams();
        active = true;
    }

    protected void updateSelectionParams() {
        if (selectedFieldType == SelectedFieldType.REGULAR_FIELD) {
            if (getSelectionType() == SeedPointsSelection.PLANE_SLICE && selDims.length < 3)
                setSeedPointsType(SeedPointsSelection.WHOLE);
            if (getSelectionType() == SeedPointsSelection.LINE_SLICE && selDims.length < 2)
                setSeedPointsType(SeedPointsSelection.WHOLE);
            for (int i = 0; i < selDims.length; i++) {
                getCoords()[i] = Math.min(getCoords()[i], selDims[i] - 1);
            }
        }
        else if (selectedFieldType == SelectedFieldType.FIELD) {
            if (getSelectionType() != SeedPointsSelection.POINT)
                setSeedPointsType(SeedPointsSelection.WHOLE);
        }
        updateRawSet();
        fireParameterChanged(DOWNSIZE);
    }

    protected void updateRawSet()
    {
        switch (getSelectionType()) {
        case WHOLE:
            rawSeedDims = selDims;
            rawSeedNNodes = selNNodes;
            break;
        case PLANE_SLICE:
            rawSeedDims = new int[2];
            rawSeedNNodes = 1;
            for (int i = 0, j = 0; i < 3; i++)
                if (!indexSliceParams.getFixed()[i]) {
                    rawSeedDims[j] = selDims[i];
                    rawSeedNNodes *= selDims[i];
                    j += 1;
                }
            break;
        case LINE_SLICE:
            rawSeedDims = new int[1];
            for (int i = 0, j = 0; i < 3; i++)
                if (!indexSliceParams.getFixed()[i]){
                    rawSeedDims[j] = selDims[i];
                    rawSeedNNodes *= selDims[i];
                    j += 1;
                }
            break;
        case POINT:
            rawSeedDims = null;
            rawSeedNNodes = 1;
        }
        initDownsize();
    }

    private void initDownsize()
    {
        downsizeParams.setFieldData(rawSeedNNodes, rawSeedDims);
        if (gui != null)
            gui.updateFromParams();
    }

    public DownsizeParams getDownsizeParams()
    {
        return downsizeParams;
    }

    public IndexSliceParams getIndexSliceParams()
    {
        return indexSliceParams;
    }

    public boolean isAuxField()
    {
        return auxField != null;
    }

    public void setPick(int pickCoords)
    {
        if (pickCoords == -1 || !(seedField instanceof RegularField))
            return;
        active = false;
        int[] dims = ((RegularField)seedField).getDims();
        float[] coords = new float[] {pickCoords % dims[0], pickCoords / dims[0]};setParameterActive(false);
        if (dims.length == 2) {
            setValue(TYPE, SeedPointsSelection.POINT);
            float[] crds = getCoords();
            System.arraycopy(coords, 0, crds, 0, coords.length);
            setValue(COORDS, crds);
        }
        else {
            setValue(TYPE, SeedPointsSelection.LINE_SLICE);
        }
        updateRawSet();
        if (gui != null)
            gui.updateFromParams();
        active = true;
    }

    /**
     * indicates if seed points are defined by the input field or auxiliary
     * field
     *
     * @return true if seed points are defined by the input field, false if by
     * auxiliary field
     */
    public boolean fromInputField()
    {
        return seedField == inputField;
    }

    /**
     * sets seed points source
     *
     * @param val true if seed points are defined by the input field, false if
     * by auxiliary field
     */
    public void setFromInputField(boolean val)
    {
        active = false;
        parameterActive = false;
        boolean oldVal = (Boolean)getValue(SOURCE);
        boolean newVal = val || auxField == null;
        setValue(SOURCE, newVal);
        seedField = val || auxField == null ? inputField : auxField;
        if (oldVal != newVal)
            setValue(TYPE, SeedPointsSelection.WHOLE);
        selNNodes = seedField.getNNodes();
        selectedFieldTrueDim = seedField.getTrueNSpace();
        setValue(TRUE_DIM, selectedFieldTrueDim);
        if (seedField instanceof RegularField) {
            selDims = ((RegularField)seedField).getDims();
            indexSliceParams.setFieldSchema(((RegularField)seedField).getSchema());
        }
        else
            selDims = null;
        updateRawSet();
        active = true;
        parameterActive = true;
        fireStateChanged();
    }

    public int seedFieldType()
    {
        if (seedField != null && seedField instanceof RegularField)
            return ((RegularField)seedField).getDimNum();
        else if (seedField == null)
            return -1;
        else
            return 0;
    }

    public int[] seedFieldDims()
    {
        if (seedField != null && seedField instanceof RegularField)
            return ((RegularField)seedField).getDims();
        return null;
    }

    /**
     * defines seed points sampling: WHOLE for whole field, PLANE_SLICE,
     * LINE_SLICE, POINT if corresponding subsamples are requested
     *
     * @return enum item indicating requested sampling
     */
    public SeedPointsSelection getSelectionType()
    {
        return (SeedPointsSelection)getValue(TYPE);
    }

    /**
     * sets seed points sampling: WHOLE for whole field, PLANE_SLICE,
     * LINE_SLICE, POINT if corresponding subsamples are requested
     *
     * @param type requested sampling type
     */
    public void setSeedPointsType(SeedPointsSelection type)
    {
        boolean oldActive = active;
        parameterActive = false;
        active = false;
        if (seedField == null ||
            seedField instanceof IrregularField &&
            type != SeedPointsSelection.WHOLE &&
            type != SeedPointsSelection.LINEAR_SLICE &&
            type != SeedPointsSelection.PLANAR_SLICE &&
            type != SeedPointsSelection.POINT) {
            active = oldActive;
            return;
        }
        setValue(TYPE, type);
        if (seedField instanceof RegularField &&
                (type == SeedPointsSelection.LINE_SLICE ||
                 type == SeedPointsSelection.PLANE_SLICE ||
                 type == SeedPointsSelection.POINT ||
                 type == SeedPointsSelection.WHOLE))
            indexSliceParams.setType(getIndexedType());
        active = oldActive;
        parameterActive = true;
        fireParameterChanged(TYPE);
    }
    /**
     * preferred number of seed points (default 100)
     * @return preferred number of seed points
     */
    public int getPreferredSize()
    {
        return (Integer)getValue(PREFERRED_SIZE);
    }

    /**
     * set preferred number of seed points (default 100)
     * @param val preferred number of seed points
     */
    public void setPreferredSize(int val)
    {
        setValue(PREFERRED_SIZE, val);
    }


    /**
     * set preferred number of seed points (default 100)
     * @param val preferred number of seed points
     * @param min minimum allowed number of seed points
     * @param max maximum allowed number of seed points
     */
    public void setPreferredSizes(int min, int max, int val)
    {
        boolean oldActive = active;
        downsizeParams.setSizes(min, max, val);
        initDownsize();
        active = oldActive;
    }


    /**
     * indices of seed point;
     * @return array indices of seed point
     */
    public float[] getCoords()
    {
        return (float[])getValue(COORDS);
    }


    public void updatePointCoords()
    {
        fireParameterChanged(COORDS);
    }

    public void setGui(SeedPointsGUI gui)
    {
        this.gui = gui;
    }

    public Field getInputField()
    {
        return inputField;
    }

    public Field getAuxField()
    {
        return auxField;
    }

    public boolean isAdjusting()
    {
        return gui != null && gui.isAdjusting() &&
              (getSelectionType() != SeedPointsSelection.POINT ||
               getSelectionType() != SeedPointsSelection.LINE_SLICE);
    }

}
