/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.isolines;

import java.util.ArrayList;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.TimeData;
import org.visnow.vn.lib.utils.field.subset.subvolume.LinearInterpolation;
import org.visnow.vn.lib.utils.field.subset.subvolume.NewNode;
import org.visnow.vn.lib.utils.sort.IndirectComparator;
import org.visnow.vn.lib.utils.sort.IndirectSort;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class IrregularFieldIsolines
{
/**
 *
 * @param cellSet
 * @param inDA
 * @param thresholds
 * @return
 */
    private static ArrayList<NewNode>[] process(CellSet cellSet, DataArray inDA, float[] thresholds, float time, boolean useBoundaryCells)
    {
        if (cellSet == null ||
            thresholds == null || thresholds.length < 1 ||
            inDA == null || inDA.isLarge())
            return null;
        ArrayList<NewNode>[] newNodes = new ArrayList[thresholds.length];
        for (int i = 0; i < newNodes.length; i++)
            newNodes[i] = new ArrayList<>();

        float[] fData;
        if (inDA.getVectorLength() == 1)
            fData = inDA.getRawArray(time).getFloatData();
        else
            fData = inDA.getVectorNorms().getData();
        CellArray triangleCellArray = useBoundaryCells ?
                                      cellSet.getBoundaryCellArray(CellType.TRIANGLE) :
                cellSet.getCellArray(CellType.TRIANGLE);
        if (triangleCellArray != null && triangleCellArray.getNodes() != null) {
            int[] triangles = triangleCellArray.getNodes();
            for (int i = 0; i < triangles.length; i += 3)
                processTriangle(fData, thresholds, triangles[i], triangles[i + 1], triangles[i + 2], newNodes);
        }
        CellArray quadCellArray = useBoundaryCells ?
                                  cellSet.getBoundaryCellArray(CellType.QUAD) :
                                  cellSet.getCellArray(CellType.QUAD);
        if (quadCellArray != null && quadCellArray.getNodes() != null) {
            int[] quads = quadCellArray.getNodes();
            for (int i = 0; i < quads.length; i += 4) {
                processTriangle(fData, thresholds, quads[i], quads[i + 1], quads[i + 2], newNodes);
                processTriangle(fData, thresholds, quads[i], quads[i + 2], quads[i + 3], newNodes);
            }
        }
        return newNodes;
    }

    private static void processTriangle(float[] fData, float[] thresholds,
                                        int p0, int p1, int p2, ArrayList<NewNode>[] newNodes)
    {
        float v0 = fData[p0];
        float v1 = fData[p1];
        float v2 = fData[p2];
        if (v0 > v1) {
            int p = p0;
            p0 = p1;
            p1 = p;
            v0 = fData[p0];
            v1 = fData[p1];
        }
        if (v0 > v2) {
            int p = p0;
            p0 = p2;
            p2 = p1;
            p1 = p;
        } else if (v1 > v2) {
            int p = p1;
            p1 = p2;
            p2 = p;
        }
        v0 = fData[p0];
        v1 = fData[p1];
        v2 = fData[p2];

        for (int j = 0; j < thresholds.length; j++) {
            float t = thresholds[j];
            if (t <= v0 || t >= v2)
                continue;
            if (t <= v1 && t > v0) {
                newNodes[j].add(new NewNode(p0, p1, (t - v0) / (v1 - v0)));
                newNodes[j].add(new NewNode(p0, p2, (t - v0) / (v2 - v0)));
            } else if (t > v1 && t < v2) {
                newNodes[j].add(new NewNode(p2, p1, (t - v2) / (v1 - v2)));
                newNodes[j].add(new NewNode(p2, p0, (t - v2) / (v0 - v2)));
            }
        }
    }



    public static IrregularField create(IrregularField inField,
                                        DataArray component, float[] thresholds,
                                        float time, boolean[] activeCellSets,
                                        boolean drawOnBoundary)
    {
        if (inField == null || !inField.getComponents().contains(component))
            return null;
        int nIsolines = thresholds.length;
        boolean timeDependentIsoComponent = component.isTimeDependant();
        ArrayList<NewNode>[] outNodes = new ArrayList[thresholds.length];
        for (int i = 0; i < thresholds.length; i++)
            outNodes[i] = new ArrayList<>();
        for (int i = 0; i < inField.getNCellSets(); i++)
            if (activeCellSets[i]) {
                ArrayList<NewNode>[] cellSetIsolines = process(inField.getCellSet(i), component,
                                                            thresholds, time, drawOnBoundary);
                for (int j = 0; j < cellSetIsolines.length; j++)
                    if (!cellSetIsolines[j].isEmpty())
                        outNodes[j].addAll(cellSetIsolines[j]);
             }
        int[][] globalNodes = new int[nIsolines][];
        ArrayList<NewNode>[] globalNewNodes = new ArrayList[nIsolines];
        int nOutNodes = 0;
        int outSegmentsLength = 0;
        for (int iIsoline = 0; iIsoline < nIsolines; iIsoline++) {
            ArrayList<NewNode> isolineNodes = outNodes[iIsoline];
            ArrayList<NewNode> cleanedNodes = new ArrayList<>();
            if (isolineNodes == null || isolineNodes.isEmpty())
                continue;
            int nIsolineNodes = isolineNodes.size();
            outSegmentsLength +=  nIsolineNodes;
            IndirectComparator cmp = new IndirectComparator()
            {
                @Override
                public int compare(int i, int j)
                {
                    NewNode n0 = isolineNodes.get(i);
                    NewNode n1 = isolineNodes.get(j);
                    if (n0.p0 < n1.p0) return -1;
                    if (n0.p0 > n1.p0) return 1;
                    return (n0.p1 < n1.p1 ? -1 : n0.p1 == n1.p1 ? 0 :  1);
                }
            };
            int[] iso = IndirectSort.indirectSort(nIsolineNodes, cmp);
            int[] nodes = new int[nIsolineNodes];
            for (int i = 0; i < nodes.length; i++)
                nodes[iso[i]] = i;
            int iLast = 0;
            int nNoDuplicates = 1;
            cleanedNodes.add(isolineNodes.get(iso[0]));
            nodes[iso[0]] = 0;

            for (int i = 1; i < nIsolineNodes; i++) {
                if (cmp.compare(iso[i], iso[iLast]) == 0)
                    nodes[iso[i]] = nodes[iso[iLast]];
                else {
                    cleanedNodes.add(isolineNodes.get(iso[i]));
                    nodes[iso[i]] = nNoDuplicates;
                    nNoDuplicates += 1;
                    iLast = i;
                }
            }
            nOutNodes += nNoDuplicates;
            globalNewNodes[iIsoline] = cleanedNodes;
            globalNodes[iIsoline] = nodes;
        }

        int[] nodes = new int[outSegmentsLength];
        NewNode[] newNodes = new NewNode[nOutNodes];
        int k = 0, l = 0;
        for (int iIsoline = 0; iIsoline < nIsolines; iIsoline++) {
            if (globalNodes[iIsoline] != null) {
                for (int i = 0; i < globalNodes[iIsoline].length; i++, k++)
                    nodes[k] = globalNodes[iIsoline][i] + l;
                for (int i = 0; i < globalNewNodes[iIsoline].size(); i++, l++)
                    newNodes[l] = globalNewNodes[iIsoline].get(i);
            }
        }

        if (nOutNodes == 0)
            return null;
        IrregularField outField = new IrregularField(nOutNodes);
        int nInterpolable = inField.getNComponents() + 1;
        if (inField.hasMask())
            nInterpolable += 1;
        ArrayList<TimeData> interpolableData = new ArrayList<>();
        ArrayList<DataArray> components = new ArrayList<>();
        int[] vLens = new int[nInterpolable];
        interpolableData.add(component.getTimeData());
        components.add(component);
        vLens[0] = component.getVectorLength();
        int m = 1;
        for (int i = 0; i < inField.getNComponents(); i++) {
            DataArray cmp = inField.getComponent(i);
            if (cmp != component) {
                interpolableData.add(cmp.getTimeData());
                components.add(cmp);
                vLens[m] = cmp.getVectorLength();
                m += 1;
            }
        }
        interpolableData.add(inField.getCoords());
        vLens[m] = 3;
        m += 1;
        if (inField.hasMask()) {
            interpolableData.add(inField.getMask());
            vLens[m] = 1;
        }

        ArrayList<TimeData> interpolatedData =
                LinearInterpolation.interpolateToNewNodesSet(nOutNodes, interpolableData, vLens,
                                                             newNodes, timeDependentIsoComponent);

        for (m = 0; m < inField.getNComponents(); m++) {
            DataArray cmp = components.get(m);
            outField.addComponent(
                    DataArray.create(interpolatedData.get(m), vLens[m],
                                     cmp.getName(), cmp.getUnit(), cmp.getUserData()).
                                           preferredRanges(cmp.getPreferredMinValue(),
                                                           cmp.getPreferredMaxValue(),
                                                           cmp.getPreferredPhysMinValue(),
                                                           cmp.getPreferredPhysMaxValue()));
        }
        outField.setCoords(interpolatedData.get(m));
        m += 1;
        if (inField.hasMask())
            outField.setMask(interpolatedData.get(m));
        CellArray segCellArray = new CellArray(CellType.SEGMENT, nodes, null, null);
        CellSet outLines = new CellSet();
        outLines.addCells(segCellArray);
        outField.addCellSet(outLines);
        return outField;
    }

}
