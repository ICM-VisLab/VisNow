/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.pointProbe;

import java.awt.Color;
import java.util.Vector;
import org.jogamp.java3d.J3DGraphics2D;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import static org.visnow.vn.lib.utils.graphing.GraphParams.DEFAULT_COLORS;
import static org.visnow.vn.lib.utils.interpolation.SubsetGeometryComponents.INDEX_COORDS;
import org.visnow.vn.lib.utils.probeInterfaces.NumericDisplay;
import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay;

/**
 *
 * @author know
 */


public class PointValuesDisplay extends ProbeDisplay
{
    private final Vector<String> names; 
    private final Vector<float[]> vals;
    private final Vector<Color> colors;
    private Color bgr = Color.BLACK;
    private float bgrOpacity = 0;

    public PointValuesDisplay(Vector<String> names, Vector<float[]> vals, Vector<Color> colors, float[] center)
    {
        this.names = names;
        this.vals = vals;
        this.colors = colors;
        this.center = center;
    }
    
    public PointValuesDisplay(IrregularField field, float[] center)
    {
        this.fld = field;
        names = new Vector<>();
        vals = new Vector<>();
        colors = new Vector<>();
        names.add("coords");
        vals.add(center);
        colors.add(Color.LIGHT_GRAY);
        DataArray indices = field.getComponent(INDEX_COORDS);
        if (indices != null) {
            names.add("indices");
            vals.add(indices.getRawFloatArray().getData());
            colors.add(Color.LIGHT_GRAY);
        }
        int k = 0;
        for (DataArray component : field.getComponents())
            if (!component.getName().equals(INDEX_COORDS)) {
                names.add(component.getName());
                vals.add(component.getFloatElement(0));
                colors.add(DEFAULT_COLORS[k % DEFAULT_COLORS.length]);
                k += 1;
                }
        this.center = center;
    }
    
    public void setBgr(Color bgr)
    {
        this.bgr = bgr;
    }

    public void setBgrOpacity(float bgrOpacity)
    {
        this.bgrOpacity = bgrOpacity;
    }
    
    
    @Override
    public void draw(J3DGraphics2D gr, LocalToWindow ltw, int width, int height, String title)
    {
        updateScreenCoords(ltw);
        NumericDisplay.displayPointValues(gr, screenHandle[0] + probeOffset[0], screenHandle[1] + probeOffset[1], 
                                          names, vals, width, height, (int)(.013f * width),
                                          bgr, bgrOpacity, colors);
        gr.setColor(selected ? Color.RED : NumericDisplay.isDark(bgr) ? Color.LIGHT_GRAY : Color.DARK_GRAY);
        gr.drawLine(screenHandle[0] + probeOffset[0], screenHandle[1] - probeOffset[1], 
                    screenHandle[0],                  screenHandle[1]);
    }

    @Override
    public void drawOnMargin(J3DGraphics2D gr, LocalToWindow ltw, int width, int height, float scale, int index, int ix, int iy, String title)
    {
    }

    @Override
    public void draw(J3DGraphics2D gr, LocalToWindow ltw, int x, int y, int width, int height, int fontSize, String title, int index)
    {
        updateScreenCoords(ltw);
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateScreenCoords(LocalToWindow ltw)
    {
        ltw.transformPt(center, screenHandle);
    }
    
    public boolean pointInside(int ix, int iy)
    {
        return false;
    }
    
}
