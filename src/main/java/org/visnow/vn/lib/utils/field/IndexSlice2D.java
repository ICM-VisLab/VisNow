/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.utils.SliceUtils;
import org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams;
import static org.visnow.vn.lib.utils.interpolation.SubsetGeometryComponents.*;

/**
 *
 * @author know
 */


public class IndexSlice2D extends IndexSlice
{
    
    private IndexSlice2D(RegularField inField, IndexSliceParams params) throws Exception
    {
        if (inField == null || inField.getDimNum() != 3)
            throw new IllegalArgumentException("in IndexSlice2D inField must be regular 3D");
        this.inField = inField;
        this.params = params;
        inDims = inField.getDims();
        lDims =  inField.getLDims();
        fSlice = new float[inDims.length - 2];
        low = new int[2];
        up = new int[2];
        outDims = new int[2];
        nOutNodes = 1;
        for (int i = 0, j = 0; i < 3; i++) 
            if (params.getFixed()[i]) {
                axis = i;
                fSlice[0] = params.getPosition()[i];
            }
            else {
                low[j] = params.getLow()[i];
                up[j] =  params.getUp()[i];
                outDims[j] = up[j] - low[j];
                nOutNodes *= outDims[j];
                j += 1;
            }
        collectData(params.isAdjusting());
    }
    
    
    void computeSlicedData()
    {
        int slice = (int)fSlice[0];
        long start = 0, n0 = outDims[0], step0 = 1, n1 = outDims[1], step1 = 0;
        int offset = 0;
        switch (axis) {
            case 2:
                start = slice * inDims[0] * inDims[1] + low[1] * inDims[0] + low[0];
                step0 = 1;
                step1 = inDims[0];
                offset = inDims[0] * inDims[1];
                break;
            case 1:
                start = slice * inDims[0] + low[0];
                step0 = 1; 
                step1 = inDims[1] * inDims[0];
                offset = inDims[0];
                break;
            case 0:
                start = low[1] * inDims[0] * inDims[1] + low[0] * inDims[0] + slice;
                step0 = inDims[0]; 
                step1 = inDims[1] * inDims[0];
                offset = 1;
                break;
        }
        for (int idata =  0; idata < inData.length; idata++) {
            long veclen = (long)vlens[idata];
            if (slice == fSlice[0]){
                if (inData[idata] instanceof FloatLargeArray)
                    outData[idata] = SliceUtils.get2DSlice(inData[idata], start, n0, step0, n1, step1, veclen);
                else {
                    LargeArray tmp = SliceUtils.get2DSlice(inData[idata], start, n0, step0, n1, step1, veclen);
                    outData[idata] = new FloatLargeArray(tmp.length());
                    for (long i = 0; i < tmp.length(); i++) 
                        outData[idata].setFloat(i, tmp.getFloat(i));
                }
            }
            else {
                float t = fSlice[0] - slice;
                LargeArray d0 = SliceUtils.get2DSlice(inData[idata], start, n0, step0, n1, step1, veclen);
                LargeArray d1 = SliceUtils.get2DSlice(inData[idata], start + offset, n0, step0, n1, step1, veclen);
                for (long i = 0; i < outData[idata].length(); i++)
                    outData[idata].setFloat(i, t * d1.getFloat(i) + (1 - t) * d0.getFloat(i));
            }
        }
    }
    
    void addIndexCoords()
    {
        float[] indexCoords= new float[3 * nOutNodes];
        for (int i = 0, l = 0; i < outDims[1]; i++) 
            for (int j = 0; j < outDims[0]; j++, l += 3) 
                switch (axis) {
                    case 0:
                        indexCoords[l] =     fSlice[0];
                        indexCoords[l + 1] = j + low[0];
                        indexCoords[l + 2] = i + low[1];
                        break;
                    case 1:
                        indexCoords[l] =     j + low[0];
                        indexCoords[l + 1] = fSlice[0];
                        indexCoords[l + 2] = i + low[1];
                        break;
                    case 2:
                        indexCoords[l] =     j + low[0];
                        indexCoords[l + 1] = i + low[1];
                        indexCoords[l + 2] = fSlice[0];
                        break;
                }
        if (regularSlice.getComponent(INDEX_COORDS) != null)
            regularSlice.removeComponent(INDEX_COORDS);
        regularSlice.addComponent(DataArray.create(indexCoords, 3, INDEX_COORDS));
    }

    
    public static RegularField slice(RegularField inField, IndexSliceParams params)
    {
        try {
            return (RegularField)(new IndexSlice2D(inField, params).slice(params.isAdjusting()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
}
