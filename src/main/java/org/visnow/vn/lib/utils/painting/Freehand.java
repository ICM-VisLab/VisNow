/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.painting;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import org.visnow.vn.lib.utils.ImageUtils;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Freehand extends PaintingDevice
{

    protected BufferedImage outImg;
    Path2D.Float p;

    public Freehand()
    {
        ui = new FreehandUI();
        //cursor = new Cursor(Cursor.CROSSHAIR_CURSOR);
        cursor = Toolkit.getDefaultToolkit().createCustomCursor(
            new ImageIcon(getClass().getResource("/org/visnow/vn/lib/utils/painting/crayon.gif")).getImage(),
            new Point(0, 0),
            "Crayon");
    }

    public Freehand(Color color, Color bgTranspColor, Color cursorColor)
    {
        ui = new FreehandUI();
        //cursor = new Cursor(Cursor.CROSSHAIR_CURSOR);
        cursor = Toolkit.getDefaultToolkit().createCustomCursor(
            new ImageIcon(getClass().getResource("/org/visnow/vn/lib/utils/painting/crayon.gif")).getImage(),
            new Point(0, 0),
            "Crayon");
        this.color = color;
        this.cursorColor = color;
        this.bgTransparencyColor = bgTranspColor;
    }

    @Override
    public BufferedImage onMouseClicked(int x, int y, int button, int nClicks, BufferedImage cursorLayer)
    {
        return null;
    }

    @Override
    public void onMousePressed(int x, int y, int button, BufferedImage cursorLayer)
    {
        outImg = new BufferedImage(cursorLayer.getWidth(), cursorLayer.getHeight(), BufferedImage.TYPE_INT_ARGB);
        p = new Path2D.Float();
        p.moveTo(x, y);
        paintBackground(outImg);
        if (button == MouseEvent.BUTTON1) {
            paintOn(x, y, outImg, false, false);
            paintOn(x, y, cursorLayer, false, false);
        } else if (button == MouseEvent.BUTTON3) {
            paintOn(x, y, outImg, true, false);
            paintOn(x, y, cursorLayer, true, false);
        }
    }

    @Override
    public BufferedImage onMouseReleased(int x, int y, int button, BufferedImage cursorLayer)
    {
        p.lineTo(x, y);
        p.closePath();
        if (button == MouseEvent.BUTTON1) {
            paintOn(x, y, outImg, false, true);
        } else if (button == MouseEvent.BUTTON3) {
            paintOn(x, y, outImg, true, true);
        }
        paintBackground(cursorLayer);
        return outImg;
    }

    @Override
    public void onMouseDragged(int x, int y, int button, BufferedImage cursorLayer)
    {
        p.lineTo(x, y);
        if (button == MouseEvent.BUTTON1) {
            paintOn(x, y, outImg, false, false);
            paintOn(x, y, cursorLayer, false, false);
        } else if (button == MouseEvent.BUTTON3) {
            paintOn(x, y, outImg, true, false);
            paintOn(x, y, cursorLayer, true, false);
        }
    }

    @Override
    public void onMouseMoved(int x, int y, BufferedImage cursorLayer)
    {
    }

    protected void paintOn(int x, int y, BufferedImage layer, boolean inverse, boolean fill)
    {
        Graphics2D g = (Graphics2D) layer.getGraphics();
        Color oldColor = g.getColor();

        if (inverse) {
            g.setColor(Color.BLACK);
        } else {
            g.setColor(color);
        }

        g.draw(p);
        if (fill)
            g.fill(p);

        g.setColor(oldColor);
        ImageUtils.makeTransparent(layer, getBgTransparencyColor());
    }

    @Override
    protected void paintCursorOn(int x, int y, BufferedImage layer)
    {
    }

    @Override
    protected void paintOn(int x, int y, BufferedImage layer, boolean inverse)
    {
    }

}
