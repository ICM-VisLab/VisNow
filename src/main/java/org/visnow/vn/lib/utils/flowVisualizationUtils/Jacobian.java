/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.flowVisualizationUtils;

import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.utils.MatrixMath;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class Jacobian 
{
    private Jacobian()
    {
    }
    
    public static float[][] jacobian(int[] dims, float[] coords, int i, int j, int k)
    {
        float[][] jacobi = new float[3][3];
        int len = dims[0];
        int slc = dims[0] * dims[1]; 
        int m = (i * dims[1] + j) * dims[0] + k;
        for (int l = 0; l < 3; l++) {
            if (k == 0)
                jacobi[l][0] = coords[3 * (m + 1) + l] - coords[3 * m + l];
            else if (k == dims[0] - 1)
                jacobi[l][0] = coords[3 * m + l] - coords[3 * (m - 1) + l];
            else
                jacobi[l][0] = .5f * (coords[3 * (m + 1) + l] - coords[3 * (m - 1) + l]);
            if (j == 0)
                jacobi[l][1] = coords[3 * (m + len) + l] - coords[3 * m + l];
            else if (j == dims[1] - 1)
                jacobi[l][1] = coords[3 * m + l] - coords[3 * (m - len) + l];
            else
                jacobi[l][1] = .5f * (coords[3 * (m + len) + l] - coords[3 * (m - len) + l]);
            if (i == 0)
                jacobi[l][2] = coords[3 * (m + slc) + l] - coords[3 * m + l];
            else if (i == dims[2] - 1)
                jacobi[l][2] = coords[3 * m + l] - coords[3 * (m - slc) + l];
            else
                jacobi[l][2] = .5f * (coords[3 * (m + slc) + l] - coords[3 * (m - slc) + l]);
        }
        return jacobi;
    }
    
    public static float[][] jacobian(int[] dims, float[] coords, int j, int k)
    {
        int len = dims[0];
        float[][] jacobi = new float[2][2];
        int m = j * dims[0] + k;
        for (int l = 0; l < 2; l++) {
            if (k == 0)
                jacobi[l][0] = coords[3 * (m + 1) + l] - coords[3 * m + l];
            else if (k == dims[0] - 1)
                jacobi[l][0] = coords[3 * m + l] - coords[3 * (m - 1) + l];
            else
                jacobi[l][0] = .5f * (coords[3 * (m + 1) + l] - coords[3 * (m - 1) + l]);
            if (j == 0)
                jacobi[l][1] = coords[3 * (m + len) + l] - coords[3 * m + l];
            else if (j == dims[1] - 1)
                jacobi[l][1] = coords[3 * m + l] - coords[3 * (m - len) + l];
            else
                jacobi[l][1] = .5f * (coords[3 * (m + len) + l] - coords[3 * (m - len) + l]);
        }
        return jacobi;
    }

    
    public static float[][] jacobian(int[] dims, FloatLargeArray coords, int i, int j, int k)
    {
        float[][] jacobi = new float[3][3];
        int len = dims[0];
        int slc = dims[0] * dims[1]; 
        int m = (i * dims[1] + j) * dims[0] + k;
        for (int l = 0; l < 3; l++) {
            if (k == 0)
                jacobi[l][0] = coords.getFloat(3 * (m + 1) + l)- coords.getFloat(3 * m + l);
            else if (k == dims[0] - 1)
                jacobi[l][0] = coords.getFloat(3 * m + l)- coords.getFloat(3 * (m - 1) + l);
            else
                jacobi[l][0] = .5f * (coords.getFloat(3 * (m + 1) + l)- coords.getFloat(3 * (m - 1) + l));
            if (j == 0)
                jacobi[l][1] = coords.getFloat(3 * (m + len) + l)- coords.getFloat(3 * m + l);
            else if (j == dims[1] - 1)
                jacobi[l][1] = coords.getFloat(3 * m + l)- coords.getFloat(3 * (m - len) + l);
            else
                jacobi[l][1] = .5f * (coords.getFloat(3 * (m + len) + l)- coords.getFloat(3 * (m - len) + l));
            if (i == 0)
                jacobi[l][2] = coords.getFloat(3 * (m + slc) + l)- coords.getFloat(3 * m + l);
            else if (i == dims[2] - 1)
                jacobi[l][2] = coords.getFloat(3 * m + l)- coords.getFloat(3 * (m - slc) + l);
            else
                jacobi[l][2] = .5f * (coords.getFloat(3 * (m + slc) + l)- coords.getFloat(3 * (m - slc) + l));
        }
        return jacobi;
    }
    
    public static float[][] jacobian(int[] dims, FloatLargeArray coords, int j, int k)
    {
        int len = dims[0];
        float[][] jacobi = new float[2][2];
        int m = j * dims[0] + k;
        for (int l = 0; l < 2; l++) {
            if (k == 0)
                jacobi[l][0] = coords.getFloat(3 * (m + 1) + l)- coords.getFloat(3 * m + l);
            else if (k == dims[0] - 1)
                jacobi[l][0] = coords.getFloat(3 * m + l)- coords.getFloat(3 * (m - 1) + l);
            else
                jacobi[l][0] = .5f * (coords.getFloat(3 * (m + 1) + l)- coords.getFloat(3 * (m - 1) + l));
            if (j == 0)
                jacobi[l][1] = coords.getFloat(3 * (m + len) + l)- coords.getFloat(3 * m + l);
            else if (j == dims[1] - 1)
                jacobi[l][1] = coords.getFloat(3 * m + l)- coords.getFloat(3 * (m - len) + l);
            else
                jacobi[l][1] = .5f * (coords.getFloat(3 * (m + len) + l)- coords.getFloat(3 * (m - len) + l));
        }
        return jacobi;
    }
    private static class ComputeJacobian implements Runnable {

        int iThread;
        int nThreads;
        int[] dims;
        float[] coords;
        float[] jacobian;

            public ComputeJacobian(int iThread, int nThreads, int[] dims, 
                                   float[] coords, 
                                   float[] jacobian)
            {
                this.iThread = iThread;
                this.nThreads = nThreads;
                this.dims = dims;
                this.coords = coords;
                this.jacobian = jacobian;
            }

        @Override
        public void run() {
            if (dims.length == 3) {
                int m = ((iThread * dims[2]) / nThreads) * dims[0] * dims[1];
                int p = 9 * m;
                for (int i = (iThread * dims[2]) / nThreads;
                        i < ((iThread + 1) * dims[2]) / nThreads;
                        i++) {
                    for (int j = 0; j < dims[1]; j++) {
                        for (int k = 0; k < dims[0]; k++, m++) {
                            float[][] jacobi = jacobian(dims, coords, i, j, k);
                            for (int l = 0; l < 3; l++, p += 3) 
                                System.arraycopy(jacobi[l], 0, jacobian, p, 3);
                        }
                    }
                }
            } else if (dims.length == 2) {
                int m = ((iThread * dims[1]) / nThreads) * dims[0];
                int p = 4 * m;
                for (int j = 0; j < dims[1]; j++) {
                    for (int k = 0; k < dims[0]; k++, m++) {
                        float[][] jacobi = jacobian(dims, coords, j, k);
                        for (int l = 0; l < 2; l++, p += 2) 
                                System.arraycopy(jacobi[l], 0, jacobian, p, 2);
                    }
                }
            }

        }
    }
    /**
     * Computes jacobian for a given coordinate function defined on a regular field of 
     * given dimensions and space dimension
     * @param dims   array of dimensions of data
     * @param coords array of coordinates - must be of size nSpace * dims[0] * ...
     * @return jacobian of coords - an array of the size 
     * dims.length * dims[0] * ...,
     * null if the coords.length() is wrong
     */
    public static final float[] computeJacobian(int[] dims, float[] coords) 
    {
        int nOut = dims.length * dims.length;
        for (int dim : dims) 
            nOut *= dim;
        float[] jacobian = new float[nOut];
        int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new ComputeJacobian(iThread, nThreads, dims, 
                                                                  coords, jacobian));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        return jacobian;
    }
    
    private static class ComputeLargeArrayJacobian implements Runnable {

        int iThread;
        int nThreads;
        int[] dims;
        FloatLargeArray coords;
        FloatLargeArray jacobian;

            public ComputeLargeArrayJacobian(int iThread, int nThreads, int[] dims, 
                                       FloatLargeArray coords, 
                                       FloatLargeArray jacobian)
            {
                this.iThread = iThread;
                this.nThreads = nThreads;
                this.dims = dims;
                this.coords = coords;
                this.jacobian = jacobian;
            }

        @Override
        public void run() {
            if (dims.length == 3) {
                long m = ((iThread * dims[2]) / nThreads) * dims[0] * dims[1];
                long p = 9 * m;
                for (int i = (iThread * dims[2]) / nThreads;
                        i < ((iThread + 1) * dims[2]) / nThreads;
                        i++) {
                    for (int j = 0; j < dims[1]; j++) {
                        for (int k = 0; k < dims[0]; k++, m++) {
                            float[][] jacobi = jacobian(dims, coords, i, j, k);
                            for (int l = 0; l < 3; l++) 
                                for (int n = 0; n < 3; n++, p++) 
                                    jacobian.set(p, jacobi[l][n]);
                        }
                    }
                }
            } else if (dims.length == 2) {
                long m = ((iThread * dims[1]) / nThreads) * dims[0];
                long p = 4 * m;
                for (int j = 0; j < dims[1]; j++) {
                    for (int k = 0; k < dims[0]; k++, m++) {
                        float[][] jacobi = jacobian(dims, coords, j, k);
                        for (int l = 0; l < 2; l++) 
                            for (int n = 0; n < 2; n++, p++) 
                                jacobian.set(p, jacobi[l][n]);
                    }
                }
            }

        }
    }
    /**
     * Computes jacobian for a given coordinate function defined on a regular field of 
     * given dimensions and space dimension
     * @param dims   array of dimensions of data
     * @param coords array of coordinates - must be of size nSpace * dims[0] * ...
     * @return jacobian of coords - a large array of the size 
     * dims.length * dims[0] * ...,
     * null if the coords.length() is wrong
     */
    public static final FloatLargeArray computeLargeArrayJacobian(int[] dims, FloatLargeArray coords) 
    {
        long nOut = dims.length * dims.length;
        for (int dim : dims) 
            nOut *= dim;
        FloatLargeArray jacobian = new FloatLargeArray(nOut);
        int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new ComputeLargeArrayJacobian(iThread, nThreads, dims, 
                                                                            coords, jacobian));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        return jacobian;
    }
    
    private static class InvertJacobians implements Runnable {

        int iThread;
        int nThreads;
        int nDims;
        float[] jacobians;
        float[] invertedJacobians;

        public InvertJacobians(int iThread, int nThreads, int nDims, float[] jacobians, float[] invertedJacobians)
        {
            this.iThread = iThread;
            this.nThreads = nThreads;
            this.nDims = nDims;
            this.jacobians = jacobians;
            this.invertedJacobians = invertedJacobians;
        }

        @Override
        public void run() {
            float[][] jac = new float[nDims][nDims];
            float[][] invJac = new float[nDims][nDims];
            for (int i = 0, k = 0; i < jacobians.length;) {
                for (int j = 0; j < nDims; j++, i += nDims) 
                    System.arraycopy(jacobians, i, jac[j], 0, nDims);
                if (MatrixMath.invert(jac, invJac))
                    for (int j = 0; j < nDims; j++, k += nDims) 
                        System.arraycopy(invJac[j], 0, invertedJacobians, k, nDims);
                else
                    for (int j = 0; j < nDims * nDims; j++, k++) 
                        invertedJacobians[k] = 0;
            }
        }
    }
    
    public static final float[] invertJacobians(int nDims, float[] jacobians) 
    {
        float[] invertedJacobians = new float[jacobians.length];
        int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new InvertJacobians(iThread, nThreads, nDims, 
                                                                  jacobians, invertedJacobians));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        return invertedJacobians;
    }
    
    private static class InvertLargeArrayJacobians implements Runnable {

        int iThread;
        int nThreads;
        int nDims;
        FloatLargeArray jacobians;
        FloatLargeArray invertedJacobians;

        public InvertLargeArrayJacobians(int iThread, int nThreads, int nDims, 
                                         FloatLargeArray jacobians, FloatLargeArray invertedJacobians)
        {
            this.iThread = iThread;
            this.nThreads = nThreads;
            this.nDims = nDims;
            this.jacobians = jacobians;
            this.invertedJacobians = invertedJacobians;
        }

        @Override
        public void run() {
            float[][] jac = new float[nDims][nDims];
            float[][] invJac = new float[nDims][nDims];
            for (long i = 0, k = 0; i < jacobians.length();) {
                for (int j = 0; j < nDims; j++) 
                    for (int l = 0; l < nDims; l++, i++) 
                        jac[j][l] = jacobians.get(i);
                if (MatrixMath.invert(jac, invJac))
                    for (int j = 0; j < nDims; j++) 
                        for (int l = 0; l < nDims; l++, k++) 
                            jacobians.set(k,jac[j][l]);
                else
                    for (int j = 0; j < nDims * nDims; j++, k++) 
                        jacobians.set(k,0);
                    
            }
        }
    }
    
    public static final FloatLargeArray invertLargeArrayJacobians(int nDims, FloatLargeArray jacobians) 
    {
        FloatLargeArray invertedJacobians = new FloatLargeArray(jacobians.length());
        int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new InvertLargeArrayJacobians(iThread, nThreads, nDims, 
                                                                  jacobians, invertedJacobians));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        return invertedJacobians;
    }
    
    public static final float[] invertInPlace(int nDims, float[] jacobians) 
    {
        int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new InvertJacobians(iThread, nThreads, nDims, 
                                                                  jacobians, jacobians));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        return jacobians;
    }
    
    public static final FloatLargeArray invertInPlaceLargeArray(int nDims, FloatLargeArray jacobians) 
    {
        int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new InvertLargeArrayJacobians(iThread, nThreads, nDims, 
                                                                  jacobians, jacobians));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        return jacobians;
    }
    
    public static final float[] computeJacobian(RegularField field)
    {
        if (!field.hasCoords() || field.getTrueNSpace() == -1 || field.getNNodes() > Integer.MAX_VALUE / 10)
            return null;
        return computeJacobian(field.getDims(), field.getCurrentCoords().getFloatData());
    }
    
    public static final FloatLargeArray computeLargeJacobian(RegularField field)
    {
        if (!field.hasCoords() || field.getTrueNSpace() == -1)
            return null;
        if (field.getNNodes() < Integer.MAX_VALUE / 10)
            return new FloatLargeArray(computeJacobian(field.getDims(), 
                                                       field.getCurrentCoords().getFloatData()));
        else
            return computeLargeArrayJacobian(field.getDims(), field.getCurrentCoords());
    }
    
    public static final FloatLargeArray computeLargeInvertedJacobian(RegularField field)
    {
        if (!field.hasCoords() || field.getTrueNSpace() == -1)
            return null;
        if (field.getNNodes() < Integer.MAX_VALUE / 10)
            return new FloatLargeArray(invertInPlace(field.getDimNum(),
                                                     computeJacobian(field.getDims(), 
                                                         field.getCurrentCoords().getFloatData())));
        else
            return invertInPlaceLargeArray(field.getDimNum(),computeLargeArrayJacobian(field.getDims(), 
                                                                  field.getCurrentCoords()));
    }
    
    public static final float[] computeInvertedJacobian(RegularField field)
    {
        if (!field.hasCoords() || field.getTrueNSpace() == -1 || field.getNNodes() > Integer.MAX_VALUE / 10)
            return null;
        return invertInPlace(field.getDimNum(), computeJacobian(field.getDims(), 
                                                field.getCurrentCoords().getFloatData()));
    }
}
