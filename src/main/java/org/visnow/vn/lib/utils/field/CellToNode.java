/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.utils.field;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 *
 * @author know
 */
public class CellToNode
{
    
    public static IrregularField convertCellDataToNodeData(boolean drop, IrregularField inField)
    {
        HashMap<String, ArrayList<CellSet>> datas = new HashMap<>();
        
        int[] cellDataOffsets = new int[inField.getNCellSets()];
        int off = 0;
        for (int i = 0; i < inField.getNCellSets(); i++) {
            cellDataOffsets[i] = off;
            CellSet cs = inField.getCellSet(i);
            if (cs.getComponent(0) != null) 
                off += cs.getComponent(0).getNElements();
        }
        
        for (CellSet cs : inField.getCellSets()) 
            dataArrayLoop:        
            for (DataArray inDA : cs.getComponents()) {
                 DataArraySchema daS = inDA.getSchema();
                 String cDaN = daS.getName();
                 while (datas.containsKey(cDaN)) {
                     DataArray xDA = datas.get(cDaN).get(0).getComponent(cDaN);
                     if (inDA.getType() == xDA.getType() && 
                         inDA.getVectorLength() == xDA.getVectorLength() &&
                         inDA.getUnit().equals(xDA.getUnit())) {
                         datas.get(cDaN).add(cs);
                         continue dataArrayLoop;         //compatible cell data array found in some cell set
                     }
                     cDaN = cDaN + "_";
                 } // no data compatible with inDA found
                 ArrayList<CellSet> newV = new ArrayList<>();
                 newV.add(cs);
                 datas.put(cDaN, newV);
            }
        IrregularField outField = inField.cloneShallow();
        int nNodes = (int) inField.getNNodes();
        int[] counts = new int[nNodes];
        for (String daName : datas.keySet()) {
            DataArray da = datas.get(daName).get(0).getComponent(daName);
            int vlen = da.getVectorLength();
            float[] outD = new float[nNodes * vlen];
            java.util.Arrays.fill(counts, 0);
            java.util.Arrays.fill(outD, 0.f);
            for (CellSet cs : datas.get(daName)) {
                
                int offset = 0;
                for (int i = 0; i < inField.getNCellSets(); i++) 
                    if (cs == inField.getCellSet(i))
                        offset = cellDataOffsets[i];
                
                int badInd = 0;
                float[] inD = cs.getComponent(daName).getRawFloatArray().getData();
                for (CellArray ca : cs.getCellArrays()) {
                    if (ca == null)
                        continue;
                    int nv = ca.getNCellNodes();
                    int[] nodes = ca.getNodes();
                    int[] indices = ca.getDataIndices();
                    for (int i = 0; i < ca.getNCells(); i++) {
                        int iin = vlen * (indices[i] - offset);                      // cell data for i-th cell start at iin
//                        int iin = vlen * indices[i];                      // cell data for i-th cell start at iin
                        for (int l = nv * i; l < nv * (i + 1); l++) {     // nodes of i-th cell
                            int iout = vlen * nodes[l];                   // node data for l-th node start at iout
                            counts[nodes[l]] += 1;
                            for (int k = 0; k < vlen; k++)
                                try {
                                    outD[iout + k] += inD[iin + k];
                                } catch (Exception e) {
                                    badInd += 1;
                                }
                        }
                    }
                }
            }
            float avg = 0;
            for (int i = 0; i < nNodes; i++) {
                if (counts[i] == 0)
                    continue;
                for (int j = i * vlen; j < (i + 1) * vlen; j++) {
                    outD[j] /= counts[i];
                    avg += outD[j];
                }
            }
            System.out.println("");
            outField.addComponent(DataArray.create(outD, vlen, daName).unit(da.getUnit()).userData(da.getUserData()));
        }
        if (drop) {
            outField.getCellSets().clear();
            for (CellSet cs : inField.getCellSets()) {
                CellSet outCS = new CellSet(cs.getName());
                outCS.setCellArrays(cs.getCellArrays());
                outCS.setBoundaryCellArrays(cs.getBoundaryCellArrays());
                outField.addCellSet(outCS);
            }
        }
        return outField;
    }

    public static void convertNodeDataToCellData(IrregularField inField, IrregularField outField)
    {
        int nComp = inField.getNComponents();
        DataArray[] interpolatedComponents = new DataArray[nComp];
        FloatLargeArray[] inData = new FloatLargeArray[nComp];
        int[] vLens = new int[nComp];
        int nInterpolated = 0;
        for (int i = 0; i < inField.getNComponents(); i++) {
            DataArray component = inField.getComponent(i);
            if (component.isNumeric()) {
                interpolatedComponents[nInterpolated] = component;
                vLens[nInterpolated] = component.getVectorLength();
                if(component.getType() == DataArrayType.FIELD_DATA_FLOAT) 
                    inData[nInterpolated] = (FloatLargeArray)component.getRawArray();
                else 
                    inData[nInterpolated] = component.getRawFloatArray();
                nInterpolated += 1;
            }
        }
        for (CellSet cs : inField.getCellSets()) {
            
            int totalCells = 0;
            for (CellArray ca : cs.getCellArrays())
                if (ca != null) 
                    totalCells += ca.getNCells();
            float[][] outData = new float[nInterpolated][];
            for (int i = 0; i < nInterpolated; i++) {
                outData[i] = new float[vLens[i] * totalCells];
                Arrays.fill(outData[i], 0);
            }
            int k = 0, tc = 0;
            for (CellArray ca : cs.getCellArrays()) {
                if (ca == null)
                    continue;
                int[] indices = new int[ca.getNCells()];
                for (int i = 0; i < indices.length; i++, tc++)
                    indices[i] = tc;
                int[] nodes = ca.getNodes();
                int nCells = ca.getNCells();
                int nCellNodes = ca.getNCellNodes();
                for (int i = 0; i < nCells; i++, k++) 
                    for (int j = 0; j < nInterpolated; j++) {
                        int vlen = vLens[j];
                        for (int l = 0; l < vlen; l++) {
                            for (int m = 0; m < nCellNodes; m++) 
                                try {
                                outData[j][vlen * k + l] += 
                                        inData[j].get(vlen * nodes[nCellNodes * i + m] + l);
                                    
                                } catch (Exception e) {
                                    System.out.println("");
                                }
                            outData[j][vlen * k + l] /= nCellNodes;
                        }
                    }
                ca.setDataIndices(indices);
            }
            for (int i = 0; i < nInterpolated; i++) {
                DataArray inDA = interpolatedComponents[i];
                cs.addComponent(DataArray.create(outData[i], vLens[i], inDA.getName()).
                                unit(inDA.getUnit()).
                                preferredRanges(inDA.getPreferredMinValue(),     inDA.getPreferredMaxValue(), 
                                                inDA.getPreferredPhysMinValue(), inDA.getPreferredPhysMaxValue()).
                                userData(inDA.getUserData()));
            }
        }
    }

    private CellToNode()
    {
    }

}
