/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.templates.visualization.guis;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import org.visnow.vn.gui.events.BooleanChangeListener;
import org.visnow.vn.gui.events.BooleanEvent;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class VisualizationModuleGUI extends javax.swing.JPanel
{

    /**
     * Creates new form FieldVisualizationGUI
     */
    public VisualizationModuleGUI()
    {
        initComponents();
    }



    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT
     * modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        computePanel = new javax.swing.JPanel();
        showButton = new javax.swing.JToggleButton();

        setLayout(new java.awt.BorderLayout());

        computePanel.setName("computePanel"); // NOI18N
        computePanel.setLayout(new java.awt.BorderLayout());

        showButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/visnow/vn/gui/icons/ball_gray_20.png"))); // NOI18N
        showButton.setSelected(true);
        showButton.setText("Presentation visible");
        showButton.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        showButton.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        showButton.setIconTextGap(10);
        showButton.setMargin(new java.awt.Insets(2, 18, 2, 14));
        showButton.setMaximumSize(new java.awt.Dimension(200, 36));
        showButton.setMinimumSize(new java.awt.Dimension(200, 26));
        showButton.setName("showButton"); // NOI18N
        showButton.setPreferredSize(new java.awt.Dimension(200, 32));
        showButton.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/org/visnow/vn/gui/icons/b4s.png"))); // NOI18N
        showButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showButtonActionPerformed(evt);
            }
        });
        computePanel.add(showButton, java.awt.BorderLayout.PAGE_START);

        add(computePanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void showButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_showButtonActionPerformed
    {//GEN-HEADEREND:event_showButtonActionPerformed

        if (visEventListener != null)
            visEventListener.booleanChanged(new BooleanEvent(this, showButton.isSelected()));
    }//GEN-LAST:event_showButtonActionPerformed

    public void addComputeGUI(JPanel gui, String title)
    {
        computePanel.removeAll();
        computePanel.add(gui, BorderLayout.CENTER);
    }

    public void addComputeGUI(JPanel gui)
    {
        addComputeGUI(gui, "Computation");
    }

    public void setPanel(JPanel panel)
    {
        computePanel.add(panel, java.awt.BorderLayout.CENTER);
    }

    private BooleanChangeListener visEventListener = null;


    public void setVisEventListener(BooleanChangeListener visEventListener)
    {
        this.visEventListener = visEventListener;
    }

    public void removeVisEventListener()
    {
        visEventListener = null;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JPanel computePanel;
    protected javax.swing.JToggleButton showButton;
    // End of variables declaration//GEN-END:variables

}
