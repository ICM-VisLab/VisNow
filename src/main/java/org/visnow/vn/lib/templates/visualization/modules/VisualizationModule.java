/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.templates.visualization.modules;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Vector;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import org.jogamp.java3d.PickInfo;
import org.visnow.vn.engine.core.Link;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.Output;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.main.ModuleSaturation;
import org.visnow.vn.geometries.events.ColorListener;
import org.visnow.vn.geometries.events.LightDirectionListener;
import org.visnow.vn.geometries.events.ProjectionListener;
import org.visnow.vn.geometries.events.ResizeListener;
import org.visnow.vn.geometries.objects.DataMappedGeometryObject;
import org.visnow.vn.geometries.objects.GeometryObject;
import org.visnow.vn.geometries.objects.GeometryParent;
import org.visnow.vn.geometries.parameters.PresentationParams;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.Pick3DListener;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.FrameRenderedListener;
import org.visnow.vn.gui.events.BooleanChangeListener;
import org.visnow.vn.gui.events.BooleanEvent;
import org.visnow.vn.lib.templates.visualization.guis.VisualizationModuleGUI;
import org.visnow.vn.lib.types.VNGeometryObject;
import org.visnow.vn.lib.utils.TimeStamper;
import org.visnow.vn.lib.utils.events.MouseRestingListener;
import org.visnow.vn.lib.utils.geometry2D.GeometryObject2DStruct;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public abstract class VisualizationModule extends ModuleCore implements RenderWindowListeningModule
{

    public static final int VERTICAL = 0;
    public static final int HORIZONTAL = 1;
    public static final int MICRO = 2;

    public static OutputEgg geometryOutput = new OutputEgg("outObj", VNGeometryObject.class, 1);

    protected PresentationParams presentationParams = new PresentationParams();
    protected DataMappingParams dataMappingParams = presentationParams.getDataMappingParams();
    protected RenderingParams renderingParams = presentationParams.getRenderingParams();
    protected GeometryObject2DStruct outObj2DStruct = new GeometryObject2DStruct();
    protected DataMappedGeometryObject outObj = new DataMappedGeometryObject(dataMappingParams);
    protected GeometryParent parent = null;
    protected BufferedImage textureImage = null;
    protected int timestamp = 0;
    protected Color bgrColor = Color.BLACK;
    protected boolean uiOnFrame = false;
    protected ProjectionListener projectionListener = null;
    protected LightDirectionListener lightDirectionListener = null;
    protected ColorListener backGroundColorListener = null;
    protected float[] lightDir = new float[3];
    protected float[] lastLightDir = new float[3];
    protected boolean graphOutputActive = true;

    protected VisualizationModuleGUI gui = new VisualizationModuleGUI();

    public class ModuleIdData
    {

        protected VisualizationModule module;

        public ModuleIdData(VisualizationModule module)
        {
            this.module = module;
        }

        public String getModuleId()
        {
            return this.module.getName();
        }

        public VisualizationModule getModule()
        {
            return module;
        }

    }

    /**
     * Creates a new instance of VisualizationModule
     */
    public VisualizationModule()
    {
        outObj.setCreator(this);
        timestamp = TimeStamper.getTimestamp();
        outObj.setName("object" + timestamp);
        outObj2DStruct.setName("object" + timestamp);
        gui.setVisEventListener(new BooleanChangeListener(){
            @Override
            public void booleanChanged(BooleanEvent e)
            {
                graphOutputActive = e.getState();
                updatePendingGeometry();
                Output output = getOutputs().getOutput("outObj");

                output.setActive(graphOutputActive);
                VisualizationModule.this.getApplication().getArea().getPanel().repaint();

                Object geometry = output.getValue();
                if (geometry != null && geometry instanceof VNGeometryObject)
                    ((VNGeometryObject)geometry).show(graphOutputActive);
            }

            @Override
            public void stateChanged(ChangeEvent e)
            {
            }
        });
        super.setPanel(gui);

    }

    public void setPanel(JPanel panel)
    {
        gui.setPanel(panel);
    }

    public void show(DataMappingParams params)
    {
    }

    private void visInitFinished()
    {
        outObj.getGeometryObj().setUserData(new ModuleIdData(this));
        outObj2DStruct.setParentModulePort(this.getName() + ".out.outObj");        
        setOutputValue("outObj", new VNGeometryObject(outObj, outObj2DStruct));
    }

    @Override
    public final void onInitFinished()
    {
        //UWAGA - nie zamieniać kolejności wywołania dwóch kolejnych linii.
        //Powoduje to zawieszanie VN przy uzyciu File -> Open data.
        visInitFinished();
        onInitFinishedLocal();
    }

    public void onInitFinishedLocal()
    {
    }

    @Override
    public void onDelete()
    {
        detach();
    }

    public void attach()
    {
        synchronized (parent) {
            outObj.attach();
        }
    }

    public boolean detach()
    {
        return outObj.detach();
    }

    public GeometryParent getParent()
    {
        return parent;
    }

    public void setParent(GeometryParent parent)
    {
        synchronized (parent) {
            this.parent = parent;
            outObj.setParentGeom(parent);
        }
    }

    public Color getBgrColor()
    {
        return bgrColor;
    }

    protected void updatePendingGeometry()
    {

    }

    @Override
    public ProjectionListener getProjectionListener()
    {
        return projectionListener;
    }

    @Override
    public LightDirectionListener getLightDirectionListener()
    {
        return lightDirectionListener;
    }

    @Override
    public ColorListener getBackgroundColorListener()
    {
        return backGroundColorListener;
    }

    @Override
    public FrameRenderedListener getFrameRenderedListener()
    {
        return null;
    }

    @Override
    public Pick3DListener getPick3DListener()
    {
        return parameters.getPick3DListener();
    }

    @Override
    public MouseRestingListener getMouseRestingListener()
    {
        return null;
    }

    @Override
    public ResizeListener getResizeListener()
    {
        return null;
    }

    public GeometryObject getOutObject()
    {
        return outObj;
    }

    public void showPickInfo(String pickedItemName, PickInfo info)
    {
        System.out.println(pickedItemName + " " + info);
    }

    public void processPickInfo(String pickedItemName, PickInfo info, int screenX, int screenY)
    {

    }

    /**
     * Serializes parameters into one string.
     * @return string containing values of presentation parameters
     */
    public String getPresentationParameterString()
    {
        return presentationParams.valuesToString();
    }


    /**
     * Presentation parameters setter.
     * Needed in user-defined (alternative to VisNow default) Presentation GUI.
     *
     * @param p PresentationParams object
     */
    public void setPresentationParams(PresentationParams p)
    {
        presentationParams = p;
    }

    /**
     * Presentation parameters getter.
     * Needed in user-defined (alternative to VisNow default) Presentation GUI.
     *
     * @return PresentationParams object
     */
    public PresentationParams getPresentationParams()
    {
        return presentationParams;
    }

    @Override
    public void onLocalSaturationChange(ModuleSaturation mSaturation)
    {
        if (mSaturation == ModuleSaturation.wrongData || mSaturation == ModuleSaturation.noData || mSaturation == ModuleSaturation.notLinked) {
            for (Output output : this.getOutputs()) {
                if (output.getType() == VNGeometryObject.class) {
                    continue;
                }
                output.setValue(null);
            }
            outObj.clearAllGeometry();
            //outObj2DStruct.setGeometryObject2D(null); ??
        }
    }


}
