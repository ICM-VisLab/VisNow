/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadVisNowField.utils;

import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;

/**
 *
 * @author Krzysztof S. Nowinski University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class CellSetIOSchema
{

    private CellSet cellSet;
    /**
     * Cell set name duplicated for faster access
     */
    protected String name;
    protected int[][] cellNodeArrays = new int[Cell.getNProperCellTypes()][];
    protected int[][] cellIndexArrays = new int[Cell.getNProperCellTypes()][];
    protected byte[][] cellOrientations = new byte[Cell.getNProperCellTypes()][];

    public CellSetIOSchema(CellSet cellSet)
    {
        this.cellSet = cellSet;
        name = cellSet.getName();
        for (int i = 0; i < Cell.getNProperCellTypes(); i++) {
            CellArray ca = cellSet.getCellArray(CellType.getType(i));
            if (ca == null) {
                cellNodeArrays[i] = null;
                cellIndexArrays[i] = null;
                cellOrientations[i] = null;
            } else {
                cellNodeArrays[i] = ca.getNodes();
                cellIndexArrays[i] = ca.getDataIndices();
                cellOrientations[i] = ca.getOrientations();
            }
        }
    }

    /**
     * Get the value of cellSet
     *
     * @return the value of cellSet
     */
    public CellSet getCellSet()
    {
        return cellSet;
    }

    /**
     * Set the value of cellSet
     *
     * @param cellSet new value of cellSet
     */
    public void setCellSet(CellSet cellSet)
    {
        this.cellSet = cellSet;
        name = cellSet.getName();
    }

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName()
    {
        return name;
    }
}
