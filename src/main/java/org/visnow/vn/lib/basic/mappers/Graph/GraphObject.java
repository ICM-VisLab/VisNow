/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Graph;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import javax.swing.event.ChangeEvent;
import org.visnow.jscic.Field;
import org.visnow.jscic.FieldType;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.geometries.events.ColorEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.PickEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.PickListener;
import org.visnow.vn.lib.templates.visualization.modules.VisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNGeometryObject;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.events.MouseRestingEvent;
import org.visnow.vn.lib.utils.events.MouseRestingListener;
import org.visnow.vn.lib.utils.field.ValueRanges;
import org.visnow.vn.lib.utils.graphing.*;
import static org.visnow.vn.lib.utils.graphing.GraphParams.*;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class GraphObject extends VisualizationModule

{
    protected GraphWorld graphWorld;
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI ui = null;
    protected GraphParams params;
    protected Field inField = null;
    protected boolean fromInput = true;
    protected boolean fromParams = false;
    protected float startTime = 0, endTime = 0;
    protected float[] minima, maxima;
    protected float[] mMinima, mMaxima;
    protected float[] pMinima, physMinima, coeffs;
    protected int[] exponents;

    protected PickListener pickListener = new PickListener(){
        @Override
        public void pickChanged(PickEvent e)
        {
            if (params.updateTop()) {
                params.setTopRightCorner(e.getScreenPickCoordinates()[0] / (float)outObj.getRenderingWindow().getWidth(),
                                         e.getScreenPickCoordinates()[1] / (float)outObj.getRenderingWindow().getHeight());
                outObj.getRenderingWindow().refresh();
                ui.updatePositionSliders(params.getXPosition(), params.getGraphWidth(), params.getYPosition(), params.getGraphHeight());
                params.setUpdateTop(false);
            }
            else if (params.updateOrigin()) {
                params.setBottomLeftCorner(e.getScreenPickCoordinates()[0] / (float)outObj.getRenderingWindow().getWidth(),
                                           e.getScreenPickCoordinates()[1] / (float)outObj.getRenderingWindow().getHeight());
                outObj.getRenderingWindow().refresh();
                ui.updatePositionSliders(params.getXPosition(), params.getGraphWidth(), params.getYPosition(), params.getGraphHeight());
                params.setUpdateOrigin(false);
            }
        }
    };

    protected MouseRestingListener toolTipListener = new MouseRestingListener(){
        @Override
        public void mouseResting(MouseRestingEvent e)
        {
            params.setTooltipPosition(new int[]{e.getX(), e.getY()});
        }

        @Override
        public void stateChanged(ChangeEvent e)
        {
        }
    };

    @Override
    public MouseRestingListener getMouseRestingListener()
    {
        return toolTipListener;
    }

    public GraphObject()
    {
        parameters = params = new GraphParams();
        params.setTitleInFrame(true);
        graphWorld = new GraphWorld(params);
        graphWorld.setDrawArgLegend(true);
        graphWorld.setDrawLegendParams(true, params.vertical());
        backGroundColorListener = (ColorEvent e) -> {
            params.setWindowBgrColor(e.getSelectedColor());
        };
        params.addParameterChangelistener((String name) -> {
            switch (name) {
                case EFFECTIVE_BGR_COLOR:
                    if (ui != null)
                        ui.updateEffectiveBgrColor();
                    if (inField == null)
                        return;
                    if (outObj.getRenderingWindow() != null)
                        outObj.getRenderingWindow().refresh();
                    break;
                case KEEP_PREFERRED_RANGES:
                    updateDataExtents();
                    update();
                    break;
            }
            if (outObj.getRenderingWindow() != null)
                outObj.getRenderingWindow().refresh();
        });
        SwingInstancer.swingRunAndWait(() -> {
            ui = new GUI();
        });
        ui.setParams(params);
        setPanel(ui);
    }

    private boolean isGraphCompatible(Field x, Field y)
    {
        if ((x == null) || (y == null) ||x.getNComponents() != y.getNComponents() ||
            (x.getType() == FieldType.FIELD_REGULAR && ((RegularField)x).getDimNum() == 1) !=
            (y.getType() == FieldType.FIELD_REGULAR && ((RegularField)y).getDimNum() == 1))
            return false;
        if (!x.hasCoords() && !y.hasCoords()) {
            float[][] xAf = ((RegularField)x).getAffine();
            float[][] yAf = ((RegularField)y).getAffine();
            int[] dims = ((RegularField)x).getDims();
            boolean[] xEssential = {false, false, false};
            boolean[] yEssential = {false, false, false};
            for (int i = 0; i < dims.length; i++)
                for (int j = 0; j < 3; j++) {
                    xEssential[j] |= (xAf[i][j] != 0);
                    yEssential[j] |= (yAf[i][j] != 0);
                }
            for (int i = 0; i < xEssential.length; i++)
                if (xEssential[i] != yEssential[i])
                    return false;
        }
        for (DataArray dX : x.getComponents()) {
            DataArray dY = y.getComponent(dX.getName());
            if (dY == null || (dY.isNumeric() != dX.isNumeric()))
                return false;
        }
        return true;
    }


    protected void initValueRanges()
    {
        int nCmp = inField.getNComponents();
        minima = new float[nCmp];
        maxima = new float[nCmp];
        Arrays.fill(minima, Float.MAX_VALUE);
        Arrays.fill(maxima, -Float.MAX_VALUE);
        mMinima = new float[nCmp];
        mMaxima = new float[nCmp];
        pMinima = new float[nCmp];
        physMinima = new float[nCmp];
        coeffs = new float[nCmp];
        exponents = new int[nCmp];
    }

    public void updateTimeRange()
    {
        if (params.getDisplayedData() == null || params.getDisplayedData().length < 1)
            return;
        startTime =  Float.MAX_VALUE;
        endTime   = -Float.MAX_VALUE;
        DisplayedData[] displayedData = params.getDisplayedData();
        try {
            for (int i = 0, j = 0; i < displayedData.length; i++)
            if (displayedData[i].isDisplayed()) {
                int iData = displayedData[i].getIndex();
                DataArray da = inField.getComponent(iData);
                if (da.getStartTime() < startTime)
                    startTime = da.getStartTime();
                if (da.getEndTime() > endTime)
                    endTime = da.getEndTime();
            }
        } catch (ConcurrentModificationException e) {
        }
        if (startTime > endTime)
            startTime = endTime = 0;
    }

    private void updateDataExtents()
    {
        initValueRanges();
        ValueRanges.updateValueRanges(inField,
                                      minima,  maxima,     mMinima, mMaxima,
                                      pMinima, physMinima, coeffs,  params.keepPreferredRanges());
        ValueRanges.updateExponentRanges(minima, maxima, mMinima, mMaxima,
                                         pMinima, physMinima, coeffs, exponents);
        graphWorld.setInData(minima,  maxima,     mMinima, mMaxima,
                             pMinima, physMinima, coeffs,  exponents);
    }

    protected void update()
    {
        outObj.clearGeometries2D();
        outObj.addGeometry2D(graphWorld);
        if (outObj.getRenderingWindow() != null)
            outObj.getRenderingWindow().refresh();
    }

    @Override
    public void onInitFinishedLocal()
    {
        outObj.addGeometry2D(graphWorld);
        outObj.addPickListener(pickListener);
        setOutputValue("outObj", new VNGeometryObject(outObj));
    }

    @Override
    public void onActive()
    {
        if (!fromParams) {
            fromInput = true;
            if (getInputFirstValue("inField") == null) {
                params.setDisplayedData(new DisplayedData[0]);
                return;
            }
            Field in = ((VNField) getInputFirstValue("inField")).getField();
            if (in == null || in.isEmpty())
                return;
            boolean updateUI = inField == null || !isGraphCompatible(in, inField);
            inField = in;
            if (inField instanceof RegularField && ((RegularField)in).getDims().length != 1)
                params.setDrawPointCloud(true);

            if (updateUI || params.getDisplayedData() == null ||
                            params.getDisplayedData().length == 0)
                ui.setInField(inField);
            graphWorld.setInField(inField);
            updateDataExtents();
            fromInput = false;
        }
        fromParams = false;
        update();
        if (params.getArgument().isEmpty())
            params.setArgument("index");
    }
}
