/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.SelectCellSets;



import org.apache.log4j.Logger;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.parameters.PresentationParams;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.utils.SwingInstancer;

import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import static org.visnow.vn.lib.basic.filters.SelectCellSets.SelectedCellSetsShared.*;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.field.ExtractCellSets;

/**
 * Spline interpolation.
 * 
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 *
 * Revisions above 25 modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl),
 * University of Warsaw, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class SelectedCellSets extends OutFieldVisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(SelectedCellSets.class);
    
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
   
    protected IrregularField inField = null;
   
    private int runQueue = 0;
    
    private GUI gui = new GUI();
   
    public SelectedCellSets()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener() {
            @Override
            public void parameterChanged(String name)
            {
                if (name != null && name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startAction();
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable() {
            @Override
            public void run() {
                gui = new GUI();
                gui.setParameters(parameters);
                ui.addComputeGUI(gui);
                setPanel(ui);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(META_CELL_SET_NAMES, new String[0]),
            new Parameter<>(SELECTED_CELL_SETS, new String[0]),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN)
        };
    }
    
    private void validateParametersAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        if (resetParameters) {
            String[] cellSetNames = new String[inField.getNCellSets()];
            for (int i = 0; i < cellSetNames.length; i++)
                cellSetNames[i] = inField.getCellSet(i).getName();
            parameters.set(META_CELL_SET_NAMES, cellSetNames);
        } else {
        }
    }
    
    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending) {
        gui.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }
    
    
    @Override
    public void onActive()
    {
        LOGGER.debug("FromVNA: " + isFromVNA());

        if (getInputFirstValue("inField") != null) {
            // 1. Get new field.
            IrregularField newInField = ((VNIrregularField) getInputFirstValue("inField")).getField();
            // 1a. Set "Different Field" flag.
            boolean isNewField = !isFromVNA() && newInField != inField;
            inField = newInField;

            // 2. Validate clonedParameters.             
            Parameters p;
            synchronized (parameters) {
                validateParametersAndSetSmart(isNewField);
                // 2b. Clone clonedParameters (local read-only copy).
                p = parameters.getReadOnlyClone();
            }
            // 3. Update GUI (GUI doesn't change clonedParameters! Assuming correct set of clonedParameters).
            notifyGUIs(p, isFromVNA() || isNewField, isFromVNA() || isNewField);

            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0); // Can run (-> decreased) in "run dynamically" mode on input attach or new inField data.

                // 4. Run computation and propagate.
                for (CellSet cellSet : inField.getCellSets()) 
                    cellSet.setSelected(false);
                for (String name : parameters.get(SELECTED_CELL_SETS)) 
                    for (CellSet cellSet : inField.getCellSets()) 
                        if (cellSet.getName().equals(name))
                            cellSet.setSelected(true);
                outIrregularField = ExtractCellSets.extractCellSets(inField);
                if (outIrregularField == null)
                    setOutputValue("outField", null);
                else
                    setOutputValue("outField", new VNIrregularField(outIrregularField));
                outField = outIrregularField;
                prepareOutputGeometry();
                renderingParams.setShadingMode(RenderingParams.FLAT_SHADED);
                for (PresentationParams csParams : presentationParams.getChildrenParams()) 
                    csParams.getRenderingParams().setShadingMode(RenderingParams.FLAT_SHADED);
                show();
            }
        }
    }
}
