/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.unused;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import org.jogamp.java3d.InputDevice;
import org.jogamp.java3d.Sensor;
import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.Vector3d;

/**
 *
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 */
public class MouseInputDevice implements InputDevice
{

    private Sensor sensor;
    private Transform3D transform = new Transform3D();
    private Vector3d transl = new Vector3d();
    private int[] buttons = new int[3];
    private Component parent;
    private MouseListener lm;
    private MouseMotionListener lmm;
    private MouseWheelListener lmw;

    public MouseInputDevice(Component c)
    {
        parent = c;
        c.addMouseListener(lm = new MouseAdapter()
        {
            @Override
            public void mousePressed(MouseEvent e)
            {
                setButtons(e);
            }

            @Override
            public void mouseReleased(MouseEvent e)
            {
                setButtons(e);
            }
        });

        c.addMouseMotionListener(lmm = new MouseMotionListener()
        {
            public void mouseDragged(MouseEvent e)
            {
                transl.x = e.getX();
                transl.y = e.getY();
                setButtons(e);
            }

            public void mouseMoved(MouseEvent e)
            {
                transl.x = e.getX();
                transl.y = e.getY();
                setButtons(e);
            }
        });

        c.addMouseWheelListener(lmw = new MouseWheelListener()
        {
            int wheel = 0;

            public void mouseWheelMoved(MouseWheelEvent e)
            {
                wheel += e.getWheelRotation();
                transl.z = wheel;
            }
        });
    }

    private void setButtons(MouseEvent e)
    {
        buttons[0] = ((MouseEvent.BUTTON1_DOWN_MASK & e.getModifiersEx()) != 0) ? 1 : 0;
        buttons[1] = ((MouseEvent.BUTTON2_DOWN_MASK & e.getModifiersEx()) != 0) ? 1 : 0;
        buttons[2] = ((MouseEvent.BUTTON3_DOWN_MASK & e.getModifiersEx()) != 0) ? 1 : 0;
    }

    public boolean initialize()
    {
        sensor = new Sensor(this, 3, 3);

        return true;
    }

    public void setNominalPositionAndOrientation()
    {
    }

    public void pollAndProcessInput()
    {
        transform.setTranslation(transl);
        sensor.setNextSensorRead(BLOCKING, transform, buttons);
    }

    public void processStreamInput()
    {
    }

    public void close()
    {
        parent.removeMouseListener(lm);
        parent.removeMouseMotionListener(lmm);
        parent.removeMouseWheelListener(lmw);
    }

    public int getProcessingMode()
    {
        return InputDevice.DEMAND_DRIVEN;
    }

    public void setProcessingMode(int mode)
    {
    }

    public int getSensorCount()
    {
        return 1;
    }

    public Sensor getSensor(int sensorIndex)
    {
        if (sensorIndex != 0) {
            throw new IndexOutOfBoundsException("Sensor index " + sensorIndex + " too large");
        }
        return sensor;
    }
}
