/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.ToolsActivityWizard;

import javax.swing.event.EventListenerList;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class RootTreeModel implements TreeModel
{

    protected EventListenerList listenerList = new EventListenerList();
    private RootEntry root;

    public RootTreeModel(RootEntry root)
    {
        this.root = root;
    }

    public Object getRoot()
    {
        return root;
    }

    public Object getChild(Object parent, int index)
    {
        if (!(parent instanceof Entry))
            return null;

        if (parent instanceof RootEntry) {
            return ((RootEntry) parent).getBranch(index);
        }

        if (parent instanceof BranchEntry) {
            return ((BranchEntry) parent).getDependancy(index);
        }

        if (parent instanceof CpointEntry) {
            return ((CpointEntry) parent).getDependancy(index);
        }

        return null;
    }

    public int getChildCount(Object parent)
    {
        if (!(parent instanceof Entry))
            return 0;

        if (parent instanceof RootEntry) {
            return ((RootEntry) parent).getBranchesSize();
        }

        if (parent instanceof BranchEntry) {
            return ((BranchEntry) parent).getDependanciesSize();
        }

        if (parent instanceof CpointEntry) {
            return ((CpointEntry) parent).getDependanciesSize();
        }

        return 0;
    }

    public boolean isLeaf(Object node)
    {
        if (node instanceof RootEntry) {
            return (((RootEntry) node).getBranchesSize() == 0);
        }

        if (node instanceof BranchEntry) {
            return (((BranchEntry) node).getDependanciesSize() == 0);
        }

        if (node instanceof CpointEntry) {
            return (((CpointEntry) node).getDependanciesSize() == 0);
        }

        if (node instanceof ToolEntry) {
            return true;
        }

        return true;
    }

    public void valueForPathChanged(TreePath path, Object newValue)
    {
    }

    public int getIndexOfChild(Object parent, Object child)
    {
        if (!(parent instanceof Entry) || !(child instanceof Entry))
            return -1;

        if ((parent instanceof RootEntry) && (child instanceof BranchEntry)) {
            return ((RootEntry) parent).getBranchIndex((BranchEntry) child);
        }

        if ((parent instanceof BranchEntry) && (child instanceof Entry)) {
            return ((BranchEntry) parent).getDependancyIndex((Entry) child);
        }

        if ((parent instanceof CpointEntry) && (child instanceof ToolEntry)) {
            return ((CpointEntry) parent).getDependancyIndex((ToolEntry) child);
        }

        return -1;
    }

    public void addTreeModelListener(TreeModelListener l)
    {
        listenerList.add(TreeModelListener.class, l);
    }

    public void removeTreeModelListener(TreeModelListener l)
    {
        listenerList.remove(TreeModelListener.class, l);
    }

}
