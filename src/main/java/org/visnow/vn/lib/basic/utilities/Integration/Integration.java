/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.utilities.Integration;

import org.visnow.jscic.IrregularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Integration extends ModuleCore
{

    public static InputEgg[] inputEggs = null;

    /**
     * Creates a new instance of CreateGrid
     */
    IntegrationPanel outPanel = null;
    IrregularField inField = null;

    public Integration()
    {
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                outPanel = new IntegrationPanel();
            }
        });
        setPanel(outPanel);
        //WTF-MUI:addModuleUI(outPanel);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null)
            return;
        inField = ((VNIrregularField) getInputFirstValue("inField")).getField();
        outPanel.setField(inField);

    }

}
