/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.Convolution;

import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.dataarrays.FloatDataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jvia.spatialops.Padding.PaddingType;
import org.visnow.vn.lib.utils.convolution.ConvolutionCore;

/**
 *
 * @author Piotr Wendykier(piotrw@icm.edu.pl)
 *
 */
public class Core
{
    
    private RegularField inFieldData = null;
    private RegularField inFieldKernel = null;
    private RegularField outField = null;
    private ConvolutionCore conv;


    public void update(int[] components, PaddingType padding, boolean normalizeKernel)
    {

        if (inFieldData == null || inFieldKernel == null) {
            outField = null;
            return;
        }

        if (components == null) {
            return;
        }

        outField = new RegularField(inFieldData.getDims());
        if (inFieldData.getCurrentCoords() == null) {
            outField.setAffine(inFieldData.getAffine());
        } else {
            outField.setCurrentCoords(inFieldData.getCurrentCoords());
        }
        
        conv = ConvolutionCore.loadConvolutionLibrary();
        
        DataArray kernel = inFieldKernel.getComponent(0);

        if (normalizeKernel) {
            FloatLargeArray kdata = kernel.getRawFloatArray();
            double sum = 0;
            for (long i = 0; i < kdata.length(); i++) {
                sum += kdata.getFloat(i);
            }
            for (long i = 0; i < kdata.length(); i++) {
                kdata.setFloat(i, (float)(kdata.getFloat(i) / sum));
            }
            kernel = new FloatDataArray(kdata, new DataArraySchema(kernel.getName(), DataArrayType.FIELD_DATA_FLOAT, kdata.length(), 1, false));
        }

        for (int n = 0; n < components.length; n++) {
            DataArray data = inFieldData.getComponent(components[n]);
            conv.setInput(data, inFieldData.getLDims(), kernel, inFieldKernel.getLDims(), padding);
            conv.calculateConvolution();
            outField.addComponent(conv.getOutput());
        }

    }

    public void setInFieldData(RegularField field)
    {
        this.inFieldData = field;
    }

    public void setInFieldKernel(RegularField field)
    {
        this.inFieldKernel = field;
    }

    /**
     * @return the outField
     */
    public RegularField getOutField()
    {
        return outField;
    }
}
