/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.gui.haptics;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListModel;
import org.apache.log4j.Logger;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.gui.JListClickHelper;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.ForceContext;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.IForce;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.IForceListModel;

/**
 * A component displaying a list of created forces.
 * <p/>
 * NOTE: As a model it uses
 * {@link ForceContext}!
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class ForcesList extends javax.swing.JList
{

    private static final Logger LOGGER = Logger.getLogger(new Throwable().getStackTrace()[0].getClassName());
    IForceListController forceListController;

    /**
     * Only for Swing GUI builder
     */
    public ForcesList()
    {
        forceListController = null;

        /* custom cell renderer - unchangeable forces (safety damping) will be displayed in gray italics */
        this.setCellRenderer(new DefaultListCellRenderer()
        {
            // based on: http://tutiez.com/how-to-display-custom-objects-in-jlist-using-listcellrenderer.html
            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index,
                                                          boolean isSelected, boolean cellHasFocus)
            {
                IForce force = (IForce) value; // Using value we are getting the object in JList
                setText(force.toString()); // text - as in default renderer
                boolean canBeChanged = force.canBeChanged();
                if (canBeChanged) {
                    if (force.isEnabled())
                        setForeground(list.getForeground());
                    else
                        setForeground(Color.GRAY);
                    setFont(list.getFont());
                } else {
                    setForeground(list.getForeground());
                    Font font = getFont().deriveFont(Font.ITALIC);
                    setFont(font);
                }

                if (isSelected) {
                    setBackground(list.getSelectionBackground());
                } else {
                    setBackground(list.getBackground());
                }
                return this;
            }
        });

    }

    public void initMouseHandling(IForceListController forceListController1)
    {
        this.forceListController = forceListController1;

        this.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mousePressed(MouseEvent evt)
            {

                ForcesList list = (ForcesList) evt.getSource();

                int index = JListClickHelper.getClickedLocation(list, evt.getPoint()); //could not be locationToIndex(evt.getPoint()), see comment in getClickedLocation

                if (index < 0) // no element is under the mouse
                    clearSelection();
            }

            @Override
            public void mouseClicked(MouseEvent evt)
            {
                if (!isEnabled())
                    return;

                ForcesList list = (ForcesList) evt.getSource();

                int index = JListClickHelper.getClickedLocation(list, evt.getPoint()); //could not be locationToIndex(evt.getPoint()), see comment in getClickedLocation
                IForce force = null;
                if (index >= 0)
                    force = getIForceListModel().get(index);

                if (force != null && force.canBeChanged())
                    setSelectedIndex(index);
                else
                    clearSelection();

                switch (evt.getButton()) {

                    case MouseEvent.BUTTON1: { // left button - enable/disable or show new force dialog
                        if (evt.getClickCount() == 2) {
                            if (force != null)
                                forceListController.showEditDialog(force);
                            else
                                forceListController.showNewForceDialog();
                        }
                        break;
                    }
                    //TODO: change (index) to (force) in argument
                    case MouseEvent.BUTTON2: { // wheel button pressed - delete force
                        if (index >= 0)
                            removeForce(index);
                        break;
                    }
                    case MouseEvent.BUTTON3: { // right button - show edit dialog
                        if (index >= 0)
                            swapEnableForce(index);
                    }
                }
            }
        });
    }

    /**
     * Removes a currently selected force.
     * If none selected, it does nothing.
     */
    public void removeSelectedForce()
    {
        int index = getSelectedIndex();
        if (index > -1) {
            removeForce(index);
            setSelectedProperIndex(index);
        }
    }

    /**
     * Removes force from a given index.
     * <p/>
     * NOTE: It DOES NOT check whether
     * <code>index</code> is valid.
     */
    public void removeForce(int index)
    {
        getIForceListModel().remove(index);
    }

    /**
     * Disables or enables a currently selected force.
     * If none selected, it does nothing.
     */
    public void swapEnableSelectedForce()
    {
        int index = getSelectedIndex();
        if (index > -1) {
            swapEnableForce(index);
        }
    }

    /**
     * Disables or enables force from a given index.
     * <p/>
     * NOTE: It DOES NOT check whether
     * <code>index</code> is valid.
     */
    public void swapEnableForce(int index)
    {
        getIForceListModel().swapEnabled(index);
    }

    /**
     * Duplicates a currently selected force.
     * If none selected, it does nothing.
     */
    void duplicateSelectedForce()
    {
        int index = getSelectedIndex();
        if (index > -1) {
            int newIndex = duplicateForce(index);
            if (newIndex != -1)
                setSelectedProperIndex(newIndex);
        }

    }

    /**
     * Duplicates a force from a given index (inserts it as the next index).
     * <p/>
     * NOTE: It DOES NOT check whether
     * <code>index</code> is valid.
     */
    private int duplicateForce(int index)
    {
        int newIndex = getIForceListModel().duplicateForce(index);
        return newIndex;
    }

    public void setForce(IForce oldForce, IForce newForce)
    {
        getIForceListModel().set(oldForce, newForce);
    }

    /**
     * Adds a force to the list.
     * <p/>
     * @param force
     */
    void addForce(IForce force)
    {
        getIForceListModel().addForce(force);
    }

    // ===== MODEL ========
    @Override
    public void setModel(ListModel model)
    {
        throw new UnsupportedOperationException("Use only ForceListModel!");
    }

    public void setModel(IForceListModel model)
    {
        super.setModel(model);
    }

    public IForceListModel getIForceListModel()
    {
        return (IForceListModel) super.getModel();
    }

    /**
     * Selects
     * <code>index</code> or the biggest/the smallest visible index if
     * <code>index</code> is out of bounds.
     */
    private void setSelectedProperIndex(int index)
    {
        int lastVisibleIndex = getLastVisibleIndex();
        if (index > lastVisibleIndex)
            index = lastVisibleIndex;

        int firstVisibleIndex = getFirstVisibleIndex();
        if (index < firstVisibleIndex)
            index = firstVisibleIndex;

        setSelectedIndex(index);
    }

    /**
     * Calls JList.setEnabled, but earlier if the list is being disabled, clears selection. This is
     * to fix a bug in Swing - a disable list still has an element selected. Disgusting.
     */
    @Override
    public void setEnabled(boolean enabled)
    {
        if (!enabled)
            clearSelection();
        super.setEnabled(enabled);
    }
}
//revised.
