/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.ConnectedComponents;

//import org.visnow.vn.lib.basic.filters.SurfaceComponents.*;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.gui.widgets.RunButton;

/**
 *
 * @author pregulski
 */


public class ConnectedComponentsShared
{
    //Minimal component sizes
    static final ParameterName<Integer> MIN_COMPONENT_SIZE = new ParameterName("minComponentSize");
    //Number of connected separate components
    static final ParameterName<Integer> SEPARATE_COMPONENTS = new ParameterName("separateComponents");
    //Index of currently component
    static final ParameterName<Integer> MODIFIED_COMPONENT = new ParameterName("modifiedComponent");
    //Names of cellsets
    static final ParameterName<String[]> COMPONENT_NAMES = new ParameterName("CellSetComponentNames");
    //Selection of the modifiedSelection
    static final ParameterName<boolean[]> MODIFIED_SELECTION = new ParameterName("modifiedSelected");

    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>("Running message");
    //Number of nodes in every CelllSet
    static final ParameterName<int[]> META_CELLSET_COMPONENT_ACTIVE_N_NODES = new ParameterName("CellSetActiveNNodes");
    //Name of the CellSet currently selected
    static final ParameterName<String> META_CELLSET_MODIFIED_NAME = new ParameterName("modifiedName");
    // decides if jTable should be repainted
    static final ParameterName<Boolean> META_DIFFERENT_PARAMS = new ParameterName("differentParams");
}
