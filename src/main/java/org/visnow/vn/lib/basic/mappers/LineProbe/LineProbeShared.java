/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.LineProbe;


import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay.Position;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class LineProbeShared
{
    public static enum ProbeType {GEOMETRIC, INDEX};
    public static final String SHOW_ACCUMULATED_STRING = "Output accumulated";
    public static final String ADD_SLICE_STRING = "Add slice";
    public static final String DEL_SLICE_STRING = "Remove slice";
    public static final String CLEAR_SLICES_STRING = "Clear slices";
    public static final String GRAPH_POSITION_STRING = "graphs position";
    public static final String INIT_MARGIN_STRING = "init margin";
    public static final String GAP_STRING = "gap";
    public static final String POINTER_LINE_STRING = "pointer line";
    public static final String PROBE_TYPE_STRING = "probe type";
    public static final String SELECTED_GRAPHS_STRING = "selected graphs";
    public static final String GRAPH_TITLE_STRING = "graph title";

    static final ParameterName<Boolean>    SHOW_ACCUMULATED = new ParameterName(SHOW_ACCUMULATED_STRING);
    static final ParameterName<Boolean>    ADD_SLICE = new ParameterName(ADD_SLICE_STRING);
    static final ParameterName<Boolean>    DEL_SLICE = new ParameterName(DEL_SLICE_STRING);
    static final ParameterName<Boolean>    CLEAR_SLICES = new ParameterName(CLEAR_SLICES_STRING);
    static final ParameterName<Position>   GRAPHS_POSITION = new ParameterName(GRAPH_POSITION_STRING);
    static final ParameterName<Boolean>    POINTER_LINE = new ParameterName(POINTER_LINE_STRING);
    static final ParameterName<ProbeType>  PROBE_TYPE = new ParameterName(PROBE_TYPE_STRING);
    static final ParameterName<Integer>    INIT_MARGIN = new ParameterName(INIT_MARGIN_STRING);
    static final ParameterName<Integer>    GAP = new ParameterName(GAP_STRING);
    static final ParameterName<boolean[]>  SELECTED_GRAPHS = new ParameterName(SELECTED_GRAPHS_STRING);
    static final ParameterName<String>     GRAPH_TITLE = new ParameterName(GRAPH_TITLE_STRING);
}
