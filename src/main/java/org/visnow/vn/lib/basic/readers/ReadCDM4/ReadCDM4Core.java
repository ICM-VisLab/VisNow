/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

 package org.visnow.vn.lib.basic.readers.ReadCDM4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.log4j.Logger;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayStatistics;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.utils.UnitUtils;
import org.visnow.vn.engine.core.ProgressAgent;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;
import ucar.ma2.Array;
import ucar.ma2.DataType;
import ucar.ma2.IndexIterator;
import ucar.ma2.StructureData;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.NetcdfFiles;
import ucar.nc2.Variable;
import ucar.units.Unit;

/**
 * Static methods for reading CDM4 files.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class ReadCDM4Core
{

    private static final Logger LOGGER = Logger.getLogger(ReadCDM4Core.class);

    /**
     * Returns dimensions of a given variable
     *
     * @param filename     Name of CDM4 file (with system path)
     * @param variableName CDM4 variable name
     *
     * @return CDM4 dimensions
     */
    public static int[] getVariableShape(String filename, String variableName)
    {
        if (filename == null) {
            return null;
        }

        try (NetcdfFile dataFile = NetcdfFiles.open(filename, null)) {

            Variable variable = dataFile.findVariable(variableName);
            if (variable == null) {
                String err = "Could not find the variable \"" + variableName + "\".";
                LOGGER.error(err);
                VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                return null;
            }
            return variable.getShape();
        } catch (Exception e) {
            String err = "File " + filename + " is not supported.";
            LOGGER.error(err);
            VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
            return null;
        }
    }

    /**
     * The method gives the names of variables stored in given file.
     * These names are escaped, ie. with escape character "\" before some of the
     * special characters, like "." (dot). Most methods assume variable names
     * given to them as arguments are escaped. Example element of the output
     * array may look like this: <tt>MyGroup/MyDatasetVersion1\.2</tt>.
     *
     * @param filename Name of CDM4 file (with system path).
     *
     * @return The output array of strings consist of escaped names, without
     *         any dimension information.
     *
     * @see getVariablesNames
     * @see getVariablesNamesUnescaped
     * @see getVariablesNamesAndDimensions
     * @see getVariablesNamesAndDimensionsUnescaped
     */
    public static String[] getVariablesNames(String filename)
    {
        String[] out = getVariablesNamesAndDimensions(filename);
        if (out == null) {
            return null;
        }

        for (int i = 0; i < out.length; ++i) {
            out[i] = stripDimension(out[i]);
        }

        return out;
    }

    /**
     * The method gives the names of variables of certain dimension (given shape),
     * stored in given file.
     * These names are escaped, ie. with escape character "\" before some of the
     * special characters, like "." (dot). Most methods assume variable names
     * given to them as arguments are escaped. Example element of the output
     * array may look like this: <tt>MyGroup/MyDatasetVersion1\.2</tt>.
     *
     * @param filename Name of CDM4 file (with system path).
     * @param shape    Required dimension (shape) of arrays stored in variables,
     *                 eg. <tt>new int[]{100, 100, 20}</tt>.
     *
     * @return The output array of strings consist of escaped names, without
     *         any dimension information.
     *
     * @see getVariablesNames
     * @see getVariablesNamesUnescaped
     * @see getVariablesNamesAndDimensions
     * @see getVariablesNamesAndDimensionsUnescaped
     */
    public static String[] getVariablesNames(String filename, int[] shape)
    {
        String[] out = getVariablesNamesAndDimensions(filename, shape);
        if (out == null) {
            return null;
        }

        for (int i = 0; out.length >= i; ++i) {
            out[i] = stripDimension(out[i]);
        }

        return out;
    }

    /**
     * The method gives unescaped names and dimensions of variables stored in given file.
     * Unescaped names are stripped from escape characters (which are usually
     * needed, when identifying variable by name - used by most methods), hence
     * more human-readable. Example element of the output array may look like this:
     * <tt>MyGroup/MyDatasetVersion1.2(Nx=100, Ny=100, Nz=20)</tt>.
     *
     * @param filename Name of CDM4 file (with system path).
     *
     * @return The output array of strings consist of unescaped names and dimensions
     *         of variables in file.
     *
     * @see getVariablesNames
     * @see getVariablesNamesUnescaped
     * @see getVariablesNamesAndDimensions
     * @see getVariablesNamesAndDimensionsUnescaped
     */
    public static String[] getVariablesNamesAndDimensionsUnescaped(String filename)
    {
        String[] out = getVariablesNamesAndDimensions(filename);
        if (out == null) {
            return null;
        }

        for (int i = 0; i < out.length; ++i) {
            out[i] = NetcdfFile.makeNameUnescaped(out[i]);
        }

        return out;
    }

    /**
     * The method gives unescaped names and dimensions of variables of certain
     * dimension (given shape), stored in given file.
     * Unescaped names are stripped from escape characters (which are usually
     * needed, when identifying variable by name - used by most methods), hence
     * more human-readable. Example element of the output array may look like this:
     * <tt>MyGroup/MyDatasetVersion1.2(Nx=100, Ny=100, Nz=20)</tt>.
     *
     * @param filename Name of CDM4 file (with system path).
     * @param shape    Required dimension (shape) of arrays stored in variables,
     *                 eg. <tt>new int[]{100, 100, 20}</tt>.
     *
     * @return The output array of strings consist of unescaped names and dimensions
     *         of variables in file.
     *
     * @see getVariablesNames
     * @see getVariablesNamesUnescaped
     * @see getVariablesNamesAndDimensions
     * @see getVariablesNamesAndDimensionsUnescaped
     */
    public static String[] getVariablesNamesAndDimensionsUnescaped(String filename, int[] shape)
    {
        String[] out = getVariablesNamesAndDimensions(filename, shape);
        if (out == null) {
            return null;
        }

        for (int i = 0; i < out.length; ++i) {
            out[i] = NetcdfFile.makeNameUnescaped(out[i]);
        }

        return out;
    }

    /**
     * The method gives the names and dimensions of variables stored in given file.
     * The names are escaped, ie. with escape character "\" before some of the
     * special characters, like "." (dot). Most methods assume variable names
     * given to them as arguments are escaped. Example element of the output array
     * may look like this: <tt>MyGroup/MyDatasetVersion1\.2(Nx=100, Ny=100, Nz=20)</tt>.
     *
     * @param filename Name of CDM4 file (with system path).
     *
     * @return The output array of strings consist of escaped names and dimensions
     *         of variables in file.
     *
     * @see getVariablesNames
     * @see getVariablesNamesUnescaped
     * @see getVariablesNamesAndDimensions
     * @see getVariablesNamesAndDimensionsUnescaped
     */
    private static String[] getVariablesNamesAndDimensions(String filename)
    {
        return getVariablesNamesAndDimensions(filename, null);
    }

    /**
     * The method gives the names and dimensions of variables of certain dimension
     * (given shape), stored in given file.
     * The names are escaped, ie. with escape character "\" before some of the
     * special characters, like "." (dot). Most methods assume variable names
     * given to them as arguments are escaped. Example element of the output array
     * may look like this: <tt>MyGroup/MyDatasetVersion1\.2(Nx=100, Ny=100, Nz=20)</tt>.
     *
     * @param filename Name of CDM4 file (with system path).
     * @param shape    Required dimension (shape) of arrays stored in variables,
     *                 eg. <tt>new int[]{100, 100, 20}</tt>.
     *
     * @return The output array of strings consist of escaped names and dimensions
     *         of variables in file.
     *
     * @see getVariablesNames
     * @see getVariablesNamesUnescaped
     * @see getVariablesNamesAndDimensions
     * @see getVariablesNamesAndDimensionsUnescaped
     */
    private static String[] getVariablesNamesAndDimensions(String filename, int[] shape)
    {
        if (filename == null) {
            return null;
        }
        ArrayList<String> variablesNamesAndDimensions;
        try (NetcdfFile dataFile = NetcdfFiles.open(filename)) {
            variablesNamesAndDimensions = new ArrayList<>();
            List<Variable> variablesList = dataFile.getVariables();
            if (shape != null) {
                for (Variable variable : variablesList) {
                    if (Arrays.equals(variable.getShape(), shape)) {
                        variablesNamesAndDimensions.add(variable.getNameAndDimensions());
                    }
                }
            } else { // FilterVariables
                for (Variable variable : variablesList) {
                    if (variable.getShape().length == 0) {
                        /* Meta-data, probably...? */
                        String warning = "Could not determine the shape of variable \"" + variable.getNameAndDimensions() + "\", seems an empty array.";
                        LOGGER.warn(warning);
                        VisNow.get().userMessageSend(null, "Empty array.", warning, Level.WARNING);
                        continue;
                    }
                    variablesNamesAndDimensions.add(variable.getNameAndDimensions());
                }
            }

            return variablesNamesAndDimensions.toArray(new String[0]);
        } catch (Exception e) {
            String err = "Could not read file " + filename;
            LOGGER.error(err);
            VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
            return null;
        }
    }

    /**
     * The method extracts the name of variable from the String containing the
     * variable name along with dimensions.
     * The principle of operation is based on the fact, that NetCDF variable names
     * can't consist of "(" character; see:
     * {@link http://www.unidata.ucar.edu/software/netcdf/docs/netcdf/CDL-Syntax.html}.
     * <p>
     * Citation (as of 20th May 2014), for the record:
     * <i>In CDL, just as for netCDF, the names of dimensions, variables and attributes
     * (and, in netCDF-4 files, groups, user-defined types, compound member names, and
     * enumeration symbols) consist of arbitrary sequences of alphanumeric characters,
     * underscore '_', period '.', plus '+', hyphen '-', or at sign '@', but beginning
     * with a letter or underscore.</i>
     *
     * @param variableName Name of variable (escaped or unescaped, doesn't matter),
     *                     with dimensions.
     *
     * @return The name of variable, without any dimension information.
     *
     * @see getVariablesNames
     * @see getVariablesNamesUnescaped
     * @see getVariablesNamesAndDimensions
     * @see getVariablesNamesAndDimensionsUnescaped
     */
    private static String stripDimension(String variableName)
    {
        return variableName.split("\\(")[0];
    }

    /**
     * Returns a pair [fieldDims, veclen] for the given CDM4 dimensions.
     *
     * @param cdm4Dims              CDM4 dimensions
     * @param firstDimensionIsTime  if true, then the first CDM4 dimension will be read as VisNow time dimension
     * @param lastDimensionIsVector if true, then the last CDM4 dimension will be read as VisNow vector length
     *
     * @return pair [fieldDims, veclen]
     */
    private static ImmutablePair<long[], Integer> extractDimsAndVeclen(List<Dimension> cdm4Dims, boolean firstDimensionIsTime, boolean lastDimensionIsVector)
    {
        int veclen = 1;
        List<Long> dims_list = new ArrayList<>();

        for (int i = firstDimensionIsTime ? 1 : 0; i < cdm4Dims.size(); i++) {
            Dimension dim = cdm4Dims.get(i);
            if (lastDimensionIsVector && i == cdm4Dims.size() - 1) {
                veclen = dim.getLength();
                continue;
            }
            long length = dim.getLength();
            if (length > 1) {
                dims_list.add(length);
            }
        }
        int len = dims_list.size();
        if (len < 1 || len > 3) {
            return null; //Only 1D, 2D and 3D regular fields are supported
        }

        long[] dims = new long[len];
        for (int i = 0; i < len; ++i) {
            dims[i] = dims_list.get(i);
        }

        /* Swap the dimensions to conform to VisNow way of storing data. */
        if (len > 1) {
            long tmp = dims[0];
            dims[0] = dims[len - 1];
            dims[len - 1] = tmp;
        }
        return new ImmutablePair<>(dims, veclen);
    }

    /**
     * Converts ucar.ma2.Array data type to LargeArray data type.
     *
     * @param in ucar.ma2.Array data type
     *
     * @return LargeArray data type
     */
    private static LargeArrayType ucarDataTypeToLargeArrayType(DataType in)
    {
        switch (in) {
            case BOOLEAN:
                return LargeArrayType.LOGIC;
            case BYTE:
                return LargeArrayType.BYTE;
            case SHORT:
            case OPAQUE:
            case ENUM2:
                return LargeArrayType.SHORT;
            case INT:
            case USHORT:
            case ENUM4:
                return LargeArrayType.INT;
            case LONG:
            case UINT:
                return LargeArrayType.LONG;
            case FLOAT:
                return LargeArrayType.FLOAT;
            case UBYTE:
            case CHAR:
            case ENUM1:
                return LargeArrayType.UNSIGNED_BYTE;
            case STRING:
                return LargeArrayType.STRING;
            case STRUCTURE:
                return LargeArrayType.COMPLEX_FLOAT;
            case OBJECT:
                return LargeArrayType.OBJECT;
            default:
                return LargeArrayType.DOUBLE;
        }
    }

    /**
     * Converts ucar.ma2.Array to the list of LargeArrays.
     *
     * @param ucarArray            input array
     * @param dims                 VisNow field dimensions
     * @param veclen               VisNow component vector length
     * @param firstDimensionIsTime if true, then the first dimension of ucarArray is treated as time dimension
     * @param progressAgent        progress monitor
     * @param currentProgress      current progress of the progress monitor
     * @param progressStep         progress step used in the progress monitor
     *
     * @return list of LargeArrays
     */
    private static ArrayList<LargeArray> ucarArrayToLargeArrays(Array ucarArray, long[] dims, int veclen, boolean firstDimensionIsTime, ProgressAgent progressAgent, float currentProgress, float progressStep)
    {
        LargeArrayType arrayType = ucarDataTypeToLargeArrayType(ucarArray.getDataType());
        long nelements = veclen;
        int ntimesteps = 1;
        for (int i = 0; i < dims.length; i++) {
            nelements *= (long) dims[i];
        }
        if (firstDimensionIsTime) {
            ntimesteps = (int) (ucarArray.getSize() / nelements);
        }
        ArrayList<LargeArray> list = new ArrayList<>(ntimesteps);
        IndexIterator iter = ucarArray.getIndexIterator();

        for (int t = 0; t < ntimesteps; t++) {
            LargeArray la = LargeArrayUtils.create(arrayType, nelements);
            switch (la.getType()) {
                case LOGIC:
                    for (long i = 0; i < nelements; i++) {
                        la.setBoolean(i, iter.getBooleanNext());
                    }
                    break;
                case BYTE:
                case UNSIGNED_BYTE:
                    for (long i = 0; i < nelements; i++) {
                        la.setByte(i, iter.getByteNext());
                    }
                    break;
                case SHORT:
                    for (long i = 0; i < nelements; i++) {
                        la.setShort(i, iter.getShortNext());
                    }
                    break;
                case INT:
                    for (long i = 0; i < nelements; i++) {
                        la.setInt(i, iter.getIntNext());
                    }
                    break;
                case LONG:
                    for (long i = 0; i < nelements; i++) {
                        la.setLong(i, iter.getLongNext());
                    }
                    break;
                case FLOAT:
                    for (long i = 0; i < nelements; i++) {
                        la.setFloat(i, iter.getFloatNext());
                    }
                    break;
                case COMPLEX_FLOAT:
                    for (long i = 0; i < nelements; i++) {
                        StructureData sd = (StructureData) iter.getObjectNext();
                        if (!sd.getMembers().toString().equals("[re, im]")) {
                            throw new IllegalArgumentException("Cannot convert structured CDM file to complex DataArray.");
                        }
                        la.set(i, new float[]{sd.getScalarFloat("re"), sd.getScalarFloat("im")});
                    }
                    break;
                case STRING:
                case OBJECT:
                    for (long i = 0; i < nelements; i++) {
                        la.set(i, iter.getObjectNext());
                    }
                    break;
                default:
                    for (long i = 0; i < nelements; i++) {
                        la.setDouble(i, iter.getDoubleNext());
                    }
            }
            if (progressAgent != null) {
                progressAgent.setProgress(currentProgress + ((t + 1) / (double) ntimesteps) * progressStep);
            }
            list.add(la);
        }

        if (arrayType == LargeArrayType.BYTE || arrayType == LargeArrayType.SHORT) { // Try to convert to LOGIC or UNSIGNED_BYTE
            short min = Short.MAX_VALUE;
            short max = Short.MIN_VALUE;
            LargeArrayType outType = arrayType;
            for (LargeArray largeArray : list) {
                short mins = (short) LargeArrayStatistics.min(largeArray);
                short maxs = (short) LargeArrayStatistics.max(largeArray);
                if (mins < min) {
                    min = mins;
                }
                if (maxs > max) {
                    max = maxs;
                }
            }
            if (min >= 0 && max <= 1) {
                outType = LargeArrayType.LOGIC;
            } else if (min >= 0 && max <= 255) {
                outType = LargeArrayType.UNSIGNED_BYTE;
            } else if (arrayType == LargeArrayType.BYTE) {
                outType = LargeArrayType.SHORT;
            }
            if (outType != arrayType) {
                ArrayList<LargeArray> newlist = new ArrayList<>(list.size());
                for (LargeArray largeArray : list) {
                    newlist.add(LargeArrayUtils.convert(largeArray, outType));
                }
                list = newlist;
            }
        }

        return list;
    }

    /**
     * Reads data from a file and stores them in a Field.
     * Only regular fields are supported at this time.
     *
     * @param filename              Name of CDM4 file (with system path)
     * @param variablesNames        list of variable names
     * @param firstDimensionIsTime  if true, then the first dimension of ucar.ma2.Array (specified by variablesNames) is treated as time dimension
     * @param lastDimensionIsVector if true, then the last dimension of ucar.ma2.Array (specified by variablesNames) is treated as vector length
     * @param progressAgent         progress monitor
     *
     * @return a new Field or null if an error occurred
     */
    public static Field createFieldFromVariables(String filename, String[] variablesNames, boolean firstDimensionIsTime, boolean lastDimensionIsVector, ProgressAgent progressAgent)
    {
        if (filename == null) {
            return null;
        }
        try (NetcdfFile dataFile = NetcdfFiles.open(filename)) {
            ImmutablePair<long[], Integer> pair;
            long[] dims = null;
            int veclen = 1;
            RegularField field = null;
            float progressStep = 0.9f / variablesNames.length;

            for (int i = 0; i < variablesNames.length; ++i) {

                Variable variable = dataFile.findVariable(variablesNames[i]);

                if (variable == null) {
                    String err = "Could not find variable \"" + variablesNames[i] + "\".";
                    LOGGER.error(err);
                    VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                    return null;
                }

                List<Dimension> dimensions = variable.getDimensions();
                if (dims == null) {
                    pair = extractDimsAndVeclen(dimensions, firstDimensionIsTime, lastDimensionIsVector);
                    if (pair == null) {
                        String err = "Only 1D, 2D and 3D variables of length greater than 1 are supported.";
                        LOGGER.error(err);
                        VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                        return null;
                    }
                    dims = pair.getKey();
                    veclen = pair.getValue();
                    field = new RegularField(dims);
                }

                Array ucarArray = variable.read();
                if (ucarArray == null) {
                    String err = "Could not read array \"" + variablesNames[i] + "\".";
                    LOGGER.error(err);
                    VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                    return null;
                }
                ArrayList<LargeArray> la_list = ucarArrayToLargeArrays(ucarArray, dims, veclen, firstDimensionIsTime, progressAgent, i * progressStep, progressStep);
                if (la_list == null || la_list.isEmpty()) {
                    String err = "Could not convert array \"" + variablesNames[i] + "\".";
                    LOGGER.error(err);
                    VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                    return null;
                }
                Unit u;
                String unit = variable.getUnitsString();
                try {
                    u = UnitUtils.getUnit(unit);
                    unit = u.toString();
                } catch (Exception e) {
                    unit = "1";
                }
                if (firstDimensionIsTime) {
                    ArrayList<Float> timeSeries = new ArrayList<>(la_list.size());
                    for (int j = 0; j < la_list.size(); j++) {
                        timeSeries.add((float) j);
                    }
                    TimeData td = new TimeData(timeSeries, la_list, 0);
                    field.addComponent(DataArray.create(td, veclen, NetcdfFile.makeNameUnescaped(variablesNames[i]), unit, null));
                } else {
                    field.addComponent(DataArray.create(la_list.get(0), veclen, NetcdfFile.makeNameUnescaped(variablesNames[i]), unit, null));
                }
            }

            return field;
        } catch (Exception e) {
            String err = "Could not read file " + filename;
            LOGGER.error(err);
            VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
            return null;
        }
    }
}
