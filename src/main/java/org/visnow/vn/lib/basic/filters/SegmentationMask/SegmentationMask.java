/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.SegmentationMask;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.LinkFace;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 * @author babor
 */
public class SegmentationMask extends ModuleCore
{

    private GUI ui = null;
    protected Params params;
    private Core core = new Core();
    private RegularField inField = null;
    private RegularField inSegmentationField = null;

    public SegmentationMask()
    {
        params = new Params();
        core.setParams(params);
        params.addChangeListener(new ChangeListener()
        {

            public void stateChanged(ChangeEvent evt)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {

            public void run()
            {
                ui = new GUI();
            }
        });
        ui.setParams(params);
        setPanel(ui);

        core.addFloatValueModificationListener(new FloatValueModificationListener()
        {

            public void floatValueChanged(FloatValueModificationEvent e)
            {
                setProgress(e.getVal());
            }
        });
    }

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            RegularField newInField = ((VNRegularField) getInputFirstValue("inField")).getField();
            if (newInField != null && inField != newInField) {
                inField = newInField;
                int[] inDims = inField.getDims();
                if (inDims.length != 3 ||
                    inDims[0] < 2 ||
                    inDims[1] < 2 ||
                    inDims[2] < 2 ||
                    inField.getNComponents() < 1) {
                    return;
                }
                ui.setInField(inField);
            }
        }

        if (getInputFirstValue("inSegmentationField") != null) {
            RegularField newInSegmentationField = ((VNRegularField) getInputFirstValue("inSegmentationField")).getField();
            if (newInSegmentationField != null && inSegmentationField != newInSegmentationField) {
                inSegmentationField = newInSegmentationField;
                int[] inDims = inSegmentationField.getDims();
                if (inDims.length != 3 ||
                    inDims[0] < 2 ||
                    inDims[1] < 2 ||
                    inDims[2] < 2 ||
                    inSegmentationField.getNComponents() < 1) {
                    return;
                }
                ui.setInSegmentationField(inSegmentationField);
            }
        }
        
        if(inField != null) {
            core.setInField(inField);
            core.setSegmentationField(inSegmentationField);
            core.update();
            setOutputValue("outField", new VNRegularField(core.getOutField()));
        } else {
            setOutputValue("outField", null);            
        }
    }

    @Override
    public void onInputDetach(LinkFace link)
    {
        if (link.getInput().getName().equals("inField")) {
            inField = null;
            ui.setInField(null);
        }
        if (link.getInput().getName().equals("inSegmentationField")) {
            inSegmentationField = null;
            ui.setInSegmentationField(null);
        }

    }
}
