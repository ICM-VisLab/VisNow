/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.FlowVisualization;

import org.visnow.vn.lib.utils.flowVisualizationUtils.GlyphFlowGUI;
import java.awt.CardLayout;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.parameters.DataMappingParams;
import org.visnow.vn.gui.widgets.SectionHeader;
import org.visnow.vn.lib.utils.flowVisualizationUtils.AnimatedStreamParams;
import static org.visnow.vn.lib.utils.flowVisualizationUtils.StreamlinesShared.*;
import org.visnow.vn.lib.utils.flowVisualizationUtils.GlyphFlowParams;
import org.visnow.vn.lib.utils.flowVisualizationUtils.SeedPointParams;
import org.visnow.vn.lib.utils.flowVisualizationUtils.SeedPointsGUI;
import org.visnow.vn.lib.utils.flowVisualizationUtils.StreamlinePresentationParams;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 * <p>
 * Revisions above 564 modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl),
 * University of Warsaw, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class GUI extends javax.swing.JPanel
{
    private Parameters parameters = null;
    private DataMappingParams dataMappingParams;
    private boolean streamlinesReady = false;

    public GUI()
    {
        initComponents();
        streamlinesGUI.addComponentNameListener(glyphFlowGUI.getVectorComponentChangeListener());
        presentationChoicePanel.setVisible(streamlinesReady);
        ((CardLayout)dynamicPresentationPanel.getLayout()).show(dynamicPresentationPanel, "streamlinePanel");
    }

    /**
     * Bind parameterproxy to this gui
     *
     * @param parameters proxy to target parameters
     */
    public void setParameterProxy(Parameters parameters)
    {
        streamlinesGUI.setParameterProxy(parameters);
        this.parameters = parameters;
    }

    void update(ParameterProxy p, boolean setRunButtonPending)
    {
        streamlinesGUI.update(p, setRunButtonPending);

        switch (p.get(ANIMATION_TYPE)) {
        case 0:
            streamlinesButton.setSelected(true);
            break;
        case 1:
            animatedStreamlinesButton.setSelected(true);
            break;
        case 2:
            glyphFlowButton.setSelected(true);
            break;
        default:
        }
    }

    public void streamlinesStatus(boolean streamlinesReady) {
        this.streamlinesReady = streamlinesReady;
        presentationChoicePanel.setVisible(streamlinesReady);
        streamlinesButton.setSelected(true);
        legendSectionHeader.setEnabled(streamlinesReady);
        colormapLegendGUI.setEnabled(streamlinesReady);
        panelsUpdate();
    }

    public void setPresentationParams(StreamlinePresentationParams streamlinePresentationParams,
                                      AnimatedStreamParams animatedStreamParams,
                                      GlyphFlowParams glyphFlowParams)
    {
        streamlinePresentationGUI.setParams(streamlinePresentationParams);
        animatedStreamGUI.setParams(animatedStreamParams);
        glyphFlowGUI.setParams(glyphFlowParams);
    }

    public void setDataMappingParams(DataMappingParams dataMappingParams)
    {
        this.dataMappingParams = dataMappingParams;
        colormapPanel.setMap(dataMappingParams.getColorMap0());
        colormapLegendGUI.setParams(dataMappingParams.getColormapLegendParameters());
        transparencyEditor.setParams(dataMappingParams.getTransparencyParams());
    }

    public void setSeedPointParams(SeedPointParams seedPointParams)
    {
        streamlinesGUI.setSeedPointParams(seedPointParams);
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        filler = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        controlsPane = new javax.swing.JTabbedPane();
        streamlineControlPanel = new javax.swing.JPanel();
        streamlinesGUI = new org.visnow.vn.lib.utils.flowVisualizationUtils.StreamlinesGUI();
        jPanel5 = new javax.swing.JPanel();
        presentationPanel = new javax.swing.JPanel();
        presentationChoicePanel = new javax.swing.JPanel();
        streamlinesButton = new javax.swing.JRadioButton();
        animatedStreamlinesButton = new javax.swing.JRadioButton();
        glyphFlowButton = new javax.swing.JRadioButton();
        dynamicPresentationPanel = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        animatedStreamGUI = new org.visnow.vn.lib.utils.flowVisualizationUtils.AnimatedStreamGUI();
        jPanel4 = new javax.swing.JPanel();
        glyphFlowGUI = new org.visnow.vn.lib.utils.flowVisualizationUtils.GlyphFlowGUI();
        jPanel2 = new javax.swing.JPanel();
        streamlinePresentationGUI = new org.visnow.vn.lib.utils.flowVisualizationUtils.StreamlinePresentationGUI();
        jPanel1 = new javax.swing.JPanel();
        dataMappingPanel = new javax.swing.JPanel();
        colormapPanel = new org.visnow.vn.geometries.gui.ComponentColormappingPanel();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        legendSectionHeader = new org.visnow.vn.gui.widgets.SectionHeader();
        colormapLegendGUI = new org.visnow.vn.geometries.gui.ColormapLegendGUI();
        transparencySectionHeader = new org.visnow.vn.gui.widgets.SectionHeader();
        transparencyEditor = new org.visnow.vn.geometries.gui.TransparencyEditor.TransparencyEditor();

        setMinimumSize(new java.awt.Dimension(900, 930));
        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(730, 950));
        setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weighty = 1.0;
        add(filler, gridBagConstraints);

        controlsPane.setMinimumSize(new java.awt.Dimension(200, 900));
        controlsPane.setPreferredSize(new java.awt.Dimension(230, 950));
        controlsPane.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                controlsPaneStateChanged(evt);
            }
        });

        streamlineControlPanel.setMinimumSize(new java.awt.Dimension(200, 550));
        streamlineControlPanel.setOpaque(false);
        streamlineControlPanel.setPreferredSize(new java.awt.Dimension(230, 595));
        streamlineControlPanel.setLayout(new java.awt.GridBagLayout());

        streamlinesGUI.setMinimumSize(new java.awt.Dimension(200, 950));
        streamlinesGUI.setOpaque(false);
        streamlinesGUI.setParameterProxy(null);
        streamlinesGUI.setPreferredSize(new java.awt.Dimension(230, 950));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        streamlineControlPanel.add(streamlinesGUI, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        streamlineControlPanel.add(jPanel5, gridBagConstraints);

        controlsPane.addTab("Computation", streamlineControlPanel);

        presentationPanel.setMinimumSize(new java.awt.Dimension(200, 570));
        presentationPanel.setLayout(new java.awt.GridBagLayout());

        presentationChoicePanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        presentationChoicePanel.setLayout(new java.awt.GridLayout(3, 0));

        buttonGroup1.add(streamlinesButton);
        streamlinesButton.setSelected(true);
        streamlinesButton.setText("static (streamlines)");
        streamlinesButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                streamlinesButtonActionPerformed(evt);
            }
        });
        presentationChoicePanel.add(streamlinesButton);

        buttonGroup1.add(animatedStreamlinesButton);
        animatedStreamlinesButton.setText("animated streamlines");
        animatedStreamlinesButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                animatedStreamlinesButtonActionPerformed(evt);
            }
        });
        presentationChoicePanel.add(animatedStreamlinesButton);

        buttonGroup1.add(glyphFlowButton);
        glyphFlowButton.setText("streamlines and glyph flow");
        glyphFlowButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                glyphFlowButtonActionPerformed(evt);
            }
        });
        presentationChoicePanel.add(glyphFlowButton);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        presentationPanel.add(presentationChoicePanel, gridBagConstraints);

        dynamicPresentationPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        dynamicPresentationPanel.setMinimumSize(new java.awt.Dimension(194, 500));
        dynamicPresentationPanel.setPreferredSize(new java.awt.Dimension(204, 530));
        dynamicPresentationPanel.setLayout(new java.awt.CardLayout());

        jPanel3.setLayout(new java.awt.BorderLayout());
        jPanel3.add(animatedStreamGUI, java.awt.BorderLayout.CENTER);

        dynamicPresentationPanel.add(jPanel3, "animatedStreamPanel");

        jPanel4.setMinimumSize(new java.awt.Dimension(190, 480));
        jPanel4.setName(""); // NOI18N
        jPanel4.setPreferredSize(new java.awt.Dimension(200, 500));
        jPanel4.setLayout(new java.awt.BorderLayout());

        glyphFlowGUI.setMinimumSize(new java.awt.Dimension(190, 480));
        glyphFlowGUI.setPreferredSize(new java.awt.Dimension(200, 500));
        jPanel4.add(glyphFlowGUI, java.awt.BorderLayout.CENTER);

        dynamicPresentationPanel.add(jPanel4, "glyphFlowPanel");

        jPanel2.setLayout(new java.awt.BorderLayout());
        jPanel2.add(streamlinePresentationGUI, java.awt.BorderLayout.CENTER);

        dynamicPresentationPanel.add(jPanel2, "streamlinePanel");
        jPanel2.getAccessibleContext().setAccessibleDescription("");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        presentationPanel.add(dynamicPresentationPanel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        presentationPanel.add(jPanel1, gridBagConstraints);

        controlsPane.addTab("Display", presentationPanel);

        dataMappingPanel.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        dataMappingPanel.add(colormapPanel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        dataMappingPanel.add(filler1, gridBagConstraints);

        legendSectionHeader.setExpanded(false);
        legendSectionHeader.setText("Legend");
        legendSectionHeader.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                legendSectionHeaderUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        dataMappingPanel.add(legendSectionHeader, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 4);
        dataMappingPanel.add(colormapLegendGUI, gridBagConstraints);

        transparencySectionHeader.setExpanded(false);
        transparencySectionHeader.setShowCheckBox(false);
        transparencySectionHeader.setText("Transparency");
        transparencySectionHeader.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                transparencySectionHeaderUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        dataMappingPanel.add(transparencySectionHeader, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(8, 4, 28, 4);
        dataMappingPanel.add(transparencyEditor, gridBagConstraints);

        controlsPane.addTab("Datamap", dataMappingPanel);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(controlsPane, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void streamlinesButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_streamlinesButtonActionPerformed
    {//GEN-HEADEREND:event_streamlinesButtonActionPerformed
        presentationSelected();
        parameters.set(ANIMATION_TYPE, NO_ANIMATION);
    }//GEN-LAST:event_streamlinesButtonActionPerformed

    private void animatedStreamlinesButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_animatedStreamlinesButtonActionPerformed
    {//GEN-HEADEREND:event_animatedStreamlinesButtonActionPerformed
        presentationSelected();
        parameters.set(ANIMATION_TYPE, STREAM_ANIMATION);
    }//GEN-LAST:event_animatedStreamlinesButtonActionPerformed

    private void glyphFlowButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_glyphFlowButtonActionPerformed
    {//GEN-HEADEREND:event_glyphFlowButtonActionPerformed
        presentationSelected();
        parameters.set(ANIMATION_TYPE, GLYPH_ANIMATION);
    }//GEN-LAST:event_glyphFlowButtonActionPerformed

    private void legendSectionHeaderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {//GEN-FIRST:event_legendSectionHeaderUserChangeAction
        panelsUpdate();
        if (evt.getEventType() == SectionHeader.EVENT_CHECKBOX_SWITCHED)
        colormapLegendGUI.processEnable(legendSectionHeader.isSelected());
    }//GEN-LAST:event_legendSectionHeaderUserChangeAction

    private void controlsPaneStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_controlsPaneStateChanged
        animatedStreamGUI.setStopAnimation();
        glyphFlowGUI.setStopAnimation();
        if (parameters != null)
            parameters.set(SETTING_SEED_POINTS, controlsPane.getSelectedIndex() == 0);
    }//GEN-LAST:event_controlsPaneStateChanged

    private void transparencySectionHeaderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_transparencySectionHeaderUserChangeAction
    {//GEN-HEADEREND:event_transparencySectionHeaderUserChangeAction
        if (evt.getEventType() == SectionHeader.EVENT_TOGGLE_EXPANDED)
        panelsUpdate();
    }//GEN-LAST:event_transparencySectionHeaderUserChangeAction

    private void panelsUpdate()
    {
        colormapLegendGUI.setVisible(streamlinesReady && legendSectionHeader.isExpanded());
        colormapLegendGUI.setEnabled(streamlinesReady && legendSectionHeader.isSelected());
        transparencyEditor.setVisible(transparencySectionHeader.isExpanded() && transparencySectionHeader.isVisible());
    }

    private void presentationSelected()
    {
        animatedStreamGUI.setStopAnimation();
        CardLayout cl = (CardLayout)(dynamicPresentationPanel.getLayout());
        String sel = "streamlinePanel";
        if (animatedStreamlinesButton.isSelected())
            sel = "animatedStreamPanel";
        else if (glyphFlowButton.isSelected())
            sel = "glyphFlowPanel";
        cl.show(dynamicPresentationPanel, sel);
    }

    public GlyphFlowGUI getGlyphFlowGUI() {
        return glyphFlowGUI;
    }

    public SeedPointsGUI getSeedPointsGUI()
    {
        return streamlinesGUI.getSeedPointsGUI();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.visnow.vn.lib.utils.flowVisualizationUtils.AnimatedStreamGUI animatedStreamGUI;
    private javax.swing.JRadioButton animatedStreamlinesButton;
    private javax.swing.ButtonGroup buttonGroup1;
    private org.visnow.vn.geometries.gui.ColormapLegendGUI colormapLegendGUI;
    private org.visnow.vn.geometries.gui.ComponentColormappingPanel colormapPanel;
    private javax.swing.JTabbedPane controlsPane;
    private javax.swing.JPanel dataMappingPanel;
    private javax.swing.JPanel dynamicPresentationPanel;
    private javax.swing.Box.Filler filler;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JRadioButton glyphFlowButton;
    private org.visnow.vn.lib.utils.flowVisualizationUtils.GlyphFlowGUI glyphFlowGUI;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private org.visnow.vn.gui.widgets.SectionHeader legendSectionHeader;
    private javax.swing.JPanel presentationChoicePanel;
    private javax.swing.JPanel presentationPanel;
    private javax.swing.JPanel streamlineControlPanel;
    private org.visnow.vn.lib.utils.flowVisualizationUtils.StreamlinePresentationGUI streamlinePresentationGUI;
    private javax.swing.JRadioButton streamlinesButton;
    private org.visnow.vn.lib.utils.flowVisualizationUtils.StreamlinesGUI streamlinesGUI;
    private org.visnow.vn.geometries.gui.TransparencyEditor.TransparencyEditor transparencyEditor;
    private org.visnow.vn.gui.widgets.SectionHeader transparencySectionHeader;
    // End of variables declaration//GEN-END:variables
}
