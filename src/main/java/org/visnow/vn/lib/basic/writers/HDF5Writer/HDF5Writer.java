/* VisNow
   Copyright (C) 2006-2019 University of Warsaw, ICM
   Copyright (C) 2020 onward visnow.org
   All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.writers.HDF5Writer;

import java.io.File;
import org.visnow.jscic.Field;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;
import org.visnow.vn.system.utils.usermessage.UserMessage;
import javax.swing.JOptionPane;
import org.visnow.jscic.RegularField;
import static org.visnow.vn.lib.basic.writers.HDF5Writer.HDF5WriterShared.*;

/**
 * Writer for HDF5 files.
 * Only VisNow RegularFields are supported.
 * ObjectDataArray components are not supported.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class HDF5Writer extends ModuleCore
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI ui = null;
    protected Field inField;
    private boolean write = false; 

    public HDF5Writer()
    {
        parameters.addParameterChangelistener((String name) -> {
             if (name.equals("Write")) {
                write = true;
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(() -> {
            ui = new GUI();
            setPanel(ui);
            ui.setParameters(parameters);
        });

    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FILENAME, ""),
            new Parameter<>(APPEND, false),
            new Parameter<>(WRITE, false)            
        };
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        ui.updateGUI(clonedParameterProxy);
    }

    private static boolean canWriteToOutputFile(String path, boolean append)
    {
        boolean result = true;
        File outFile = new File(path);
        if (!outFile.getParentFile().canWrite()) {
            JOptionPane.showMessageDialog(null, "Cannot write file (check write permissions)");
            return false;
        }
        if (outFile.exists() && append == false) {
            Object ans = JOptionPane.showConfirmDialog(null, "File " + outFile.getName() + " exists.\nOwerwrite?", "", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            result = ans instanceof Integer && (Integer) ans == JOptionPane.YES_OPTION;
        }
        return result;
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            Field newField = ((VNField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = !isFromVNA() && (inField == null || inField.getTimestamp() != newField.getTimestamp());
            inField = newField;

            Parameters p = parameters.getReadOnlyClone();

            notifyGUIs(p, isFromVNA() || isDifferentField, true);

            String outFileName = p.get(FILENAME);
            if (write && outFileName != null && !outFileName.isEmpty()) {
                if (canWriteToOutputFile(outFileName, p.get(APPEND))) {
                    if (HDF5WriterCore.writeField((RegularField) inField, p.get(FILENAME), p.get(APPEND)) == true) {
                        VisNow.get().userMessageSend(new UserMessage("VisNow", "", "HDF5 file written", "", Level.INFO));
                    } else {
                        VisNow.get().userMessageSend(new UserMessage("VisNow", "", "Error writing HDF5 file", "", Level.ERROR));
                    }
                }
                write = false;
            }
        }
    }
}
