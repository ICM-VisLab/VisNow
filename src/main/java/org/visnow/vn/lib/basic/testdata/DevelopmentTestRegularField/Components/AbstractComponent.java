/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.testdata.DevelopmentTestRegularField.Components;

/**
 * All components must extend this class. Obligatory user defined methods include
 * getName() method for human-readable name of the data component.
 * 
 *
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl), Warsaw University, ICM
 */
public abstract class AbstractComponent
{
    /**
     * An array actually containing the data.
     * Assumption: really an array of type...
     */
    protected Object data = null;

    /**
     * Number of data values per point in the grid.
     * Common values include 1 (scalar field), 3 (vector field).
     */
    protected int veclen;

    /**
     * Always keep no-argument constructor in subclasses; This will be the
     * default constructor invoked automatically by the main module object.
     */
    public AbstractComponent()
    {
    }

    /**
     * Method, that computes actual coordinates of every point in the grid.
     * 
     * @param dims An array containing the dimensions of the grid.
     * @see org.visnow.jscic.RegularField
     */
    public abstract void compute(int[] dims);

    public Object getData()
    {
        return this.data;
    }

    public void setVeclen(int veclen)
    {
        this.veclen = veclen;
    }

    public int getVeclen()
    {
        return this.veclen;
    }
}
