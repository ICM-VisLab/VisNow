/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.ViewPanels;

import java.awt.BorderLayout;
import org.jogamp.java3d.Canvas3D;
import javax.swing.JPopupMenu;
import org.visnow.vn.geometries.viewer3d.Display3DPanel;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Display3DViewPanel extends ViewPanel
{

    private Display3DPanel d3dPanel = new Display3DPanel();

    public Display3DViewPanel()
    {
        super("3D view", ViewPanel.VIEW_3D);
        JPopupMenu.setDefaultLightWeightPopupEnabled(false);
        d3dPanel.setMinimumSize(this.getMinimumSize());
        this.removeAll();
        this.add(d3dPanel, BorderLayout.CENTER);
    }

    public Display3DPanel getDisplay3DPanel()
    {
        return d3dPanel;
    }

    @Override
    public void preRemove()
    {
        d3dPanel.getCanvas().stopRenderer();
    }

    @Override
    public void postAdd()
    {
        d3dPanel.getCanvas().startRenderer();
    }

}
