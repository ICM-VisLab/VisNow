/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.gui;

import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.JList;

/**
 * Helper class for determining which index was clicked in JList.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class JListClickHelper
{

    /**
     * Method returns the actually clicked position. The difference between this and locationToIndex
     * is that the latter returns <b>the closest</b> index to the clicked point and not the actually
     * clicked position. So we must use getCellBounds to check that.
     *
     * Official documentation: "To determine if the cell actually contains the specified location,
     * compare the point against the cell's bounds, as provided by getCellBounds. This method
     * returns -1 if the model is empty".
     *
     * @param p clicked point
     * <p>
     * @return index of a clicked element or -1 if none clicked
     * <p>
     * @see <a
     * href="http://docs.oracle.com/javase/6/docs/api/javax/swing/JList.html#locationToIndex%28java.awt.Point%29">locationToIndex()
     * in Java documentation</a>
     */
    public static int getClickedLocation(JList list, Point p)
    {
        int index = list.locationToIndex(p);
        if (index < 0) { // model is empty
            return -1;
        }

        Rectangle r = list.getCellBounds(index, index);
        if (r.contains(p)) {
            return index;
        } else {
            return -1;
        }
    }
}
