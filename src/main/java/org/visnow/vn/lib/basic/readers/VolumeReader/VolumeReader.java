/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.VolumeReader;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import javax.swing.SwingUtilities;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.readers.VolumeReader.VolumeReaderShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University, Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 */
public class VolumeReader extends OutFieldVisualizationModule
{

    protected GUI computeUI = null;
    protected boolean fromGUI = false;
    protected boolean avsCompatible = true;

    public VolumeReader()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FILENAME, ""),
            new Parameter<>(SCALE, new float[]{1, 1, 1}),
            new Parameter<>(ORIG, new float[]{0, 0, 0}),
            new Parameter<>(MIN, new float[]{-1, -1, -1}),
            new Parameter<>(MAX, new float[]{1, 1, 1}),
            new Parameter<>(TYPE, 0)
        };
    }

    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    public static OutputEgg[] outputEggs = null;

    private RegularField readVolume(String fileName, int type, float[] origin, float[] scale)
    {
        DataInput in = null;
        try {
            if (fileName.endsWith("gz") || fileName.endsWith("GZ"))
                in = new DataInputStream(new GZIPInputStream(new FileInputStream(fileName)));
            else
                in = new DataInputStream(new FileInputStream(fileName));
        } catch (IOException e) {
            return null;
        }
        outRegularField = null;
        int n = 0;
        String[] udata = new String[1000];
        int[] dims = new int[3];
        float[][] pts;
        try {
            String componentName = (new File(fileName)).getName();
            if (componentName == null || componentName.length() < 1)
                componentName = "data";
            dims[0] = in.readUnsignedByte();

            if (dims[0] != 0) {
                avsCompatible = true;
                dims[1] = in.readUnsignedByte();
                dims[2] = in.readUnsignedByte();
            } else {
                avsCompatible = true;
                dims[0] = in.readInt();
                dims[1] = in.readInt();
                dims[2] = in.readInt();
            }

            computeUI.setAVSCompatible(avsCompatible);
            outRegularField = new RegularField(dims);

            pts = new float[][]{{0, 0, 0}, {dims[0] - 1, dims[1] - 1, dims[2] - 1}};
            float[][] affine = new float[][]{{0, 0, 0}, {1, 0, 0}, {0, 1, 0}, {0, 0, 1}};

            byte[] data0 = new byte[dims[0] * dims[1] * dims[2]];
            in.readFully(data0);
            outRegularField.addComponent(DataArray.create(data0, 1, componentName));
            try {
                for (int i = 0; i < affine.length; i++)
                    for (int j = 0; j < affine[i].length; j++)
                        affine[i][j] = in.readFloat();
                if (type == FROM_FILE)
                    outRegularField.setAffine(affine);
            } catch (EOFException e) {
                outRegularField.setPreferredExtents(pts);
            }
            try {
                String name = in.readUTF();
                outRegularField.getComponent(0).setName(name);
                for (n = 0; n < udata.length; n++)
                    udata[n] = in.readUTF();
            } catch (EOFException e) {
                if (n > 0) {
                    String[] uData = new String[n];
                    System.arraycopy(udata, 0, uData, 0, n);
                    outRegularField.getComponent(0).setUserData(uData);
                }
            }
            if (in instanceof InputStream)
                ((InputStream) in).close();
        } catch (IOException e) {
            return null;
        }
        float[][] affine = new float[4][3];
        if (type == FROM_INDICES) {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++)
                    affine[i][j] = 0;
                affine[i][i] = 1;
                affine[3][i] = 0;
            }
            outRegularField.setAffine(affine);
        }
        if (type == NORMALIZED) {
            int dmax = 0;
            for (int i = 0; i < 3; i++)
                if (dims[i] > dmax)
                    dmax = dims[i];
            for (int i = 0; i < 3; i++) {
                pts[0][i] = -(dims[i] - 1.f) / (dmax - 1.f);
                pts[1][i] = (dims[i] - 1.f) / (dmax - 1.f);
            }
            outRegularField.setPreferredExtents(pts);
        }

        if (type == USER_AFFINE) {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++)
                    affine[i][j] = 0;
                affine[i][i] = scale[i];
                affine[3][i] = origin[i];
            }
            outRegularField.setAffine(affine);
        }

        if (type == USER_EXTENTS) {

            outRegularField.setPreferredExtents(new float[][]{parameters.get(MIN), parameters.get(MAX)});
        }

        return outRegularField;
    }

    @Override
    public void onActive()
    {
        
        Parameters p;
        synchronized (parameters) {
            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, false, false);

        if (p.get(FILENAME).equals("")) {
            outField = null;
            show();
            setOutputValue("outRegularField", null);
        }
        else {
            File file = new File(p.get(FILENAME));
            if (!file.exists()) {
                VisNow.get().userMessageSend(this, "Error loading file", "File " + file.getAbsolutePath() + " does not exist.", Level.ERROR);
                outField = null;
                show();
                setOutputValue("outRegularField", null);
                return;
            }
            outRegularField = readVolume(p.get(FILENAME), p.get(TYPE), p.get(ORIG), p.get(SCALE));
            outField = outRegularField;
            if (outRegularField == null) {
                VisNow.get().userMessageSend(this, "Error loading file", "Cannot read file " + file.getAbsolutePath(), Level.ERROR);
                outField = null;
                show();
                setOutputValue("outRegularField", null);
                return;
            }
            VisNow.get().userMessageSend(this, "<html>File successfully loaded", outRegularField.toMultilineString(), Level.INFO);
            setOutputValue("volume", new VNRegularField(outRegularField));
            prepareOutputGeometry();
            show();
        }
    }
    
    
    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag())
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    computeUI.activateOpenDialog();
                }
            });
    }

}
