/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.testdata.DevelopmentTestRegularField.Components;

import org.visnow.jlargearrays.FloatLargeArray;

import org.apache.log4j.Logger;

import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.ComplexFloatLargeArray;

/**
 * Class creating data array of complex type containing some constant.
 * Class cannot be extended, as static methods (e.g. <tt>name</tt>) are not inherited.
 *
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl), Warsaw University, ICM
 * @see AbstractComponent
 */
public final class ConstantComplex extends AbstractComponent
{
    private static final Logger LOGGER = Logger.getLogger(ConstantComplex.class);

    /* Local variables, useful for computing the values of this component. */
    private final float constant_re = -(float) E;
    private final float constant_im = -(float) PI;

    private FloatLargeArray data_re;
    private FloatLargeArray data_im;

    private final int nThreads = Runtime.getRuntime().availableProcessors();

    public ConstantComplex()
    {
        veclen = 1;
        data = null;

        data_re = null;
        data_im = null;
    }

    /**
     * Method providing a human-readable name of component presented in
     * this class.
     * All methods, that inherit from <tt>AbstractComponent</tt> class should have
     * this method implemented. Lack of this method will result in appearance
     * of (not always informative) bare class names in user interfaces.
     *
     * @return The human-readable name of the data component.
     * @see AbstractComponent
     */
    public static final String getName()
    {
        return "Constant (complex)";
    }

    /**
     * Method, that computes actual values of component on every point
     * on the grid.
     * Core calculations should be carried out in this method. A constructor
     * should be kept as light as possible.
     *
     * @param dims An array containing the dimensions of the grid. It must have
     *             one to three elements - by design all of these values (i.e. 1, 2 and 3)
     *             are supported by every data component class.
     * @see AbstractComponent
     * @see org.visnow.jscic.RegularField
     */
    @Override
    public void compute(int[] dims)
    {
        long length = 1;

        for (int i = 0; i < dims.length; i++) {
            length *= dims[i];
        }

        data_re = new FloatLargeArray(length, false);
        data_im = new FloatLargeArray(length, false);

        Thread[] workThreads = new Thread[nThreads];
        for (int i = 0; i < workThreads.length; i++) {
            workThreads[i] = new Thread(new ComputeThreaded(nThreads, i, dims));
            workThreads[i].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        
        data = new ComplexFloatLargeArray(data_re, data_im);
    }

    class ComputeThreaded implements Runnable
    {
        int nThreads;
        int iThread;

        int[] dims;

        public ComputeThreaded(int nThreads, int iThread, int[] dims)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;

            this.dims = dims;
        }

        @Override
        public void run()
        {
            switch (dims.length) {
                case 3:
                    for (long k = iThread; k < dims[2]; k += nThreads) {
                        for (long j = 0; j < dims[1]; ++j) {
                            for (long i = 0; i < dims[0]; ++i) {
                                ((FloatLargeArray) data_re).setFloat(i + dims[0] * (j + k * dims[1]), constant_re);
                                ((FloatLargeArray) data_im).setFloat(i + dims[0] * (j + k * dims[1]), constant_im);
                            }
                        }
                    }
                    break;
                case 2:
                    for (long j = iThread; j < dims[1]; j += nThreads) {
                        for (long i = 0; i < dims[0]; ++i) {
                            ((FloatLargeArray) data_re).setFloat(i + dims[0] * j, constant_re);
                            ((FloatLargeArray) data_im).setFloat(i + dims[0] * j, constant_im);
                        }
                    }
                    break;
                case 1:
                    for (long i = iThread; i < dims[0]; i += nThreads) {
                        ((FloatLargeArray) data_re).setFloat(i, constant_re);
                        ((FloatLargeArray) data_im).setFloat(i, constant_im);
                    }
                    break;
                default:
                    LOGGER.fatal("Unrecognized structure of \"dims\" array... dims.length = " + dims.length);
                    throw new RuntimeException("Unrecognized structure of \"dims\" array... dims.length = " + dims.length);
            }
        }
    }
}