/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Skeletonizer;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.Pick3DEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.Pick3DListener;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.PickType;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Skeletonizer extends OutFieldVisualizationModule
{

    /**
     * Creates a new instance of CreateGrid
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected SkeletonCompute skeletonizer = null;
    protected GUI computeUI;
    protected RegularField lastInField = null;
    protected RegularField inField;
    protected RegularField mapField;
    protected RegularField segmentField;
    protected SkeletonizerParams params;
    protected float[] crds;
    protected short[] rData;
    protected int nPolys;
    protected int[] polylines;
    protected int pickedNode = -1;
    protected int[][] startPoints = null;
    protected FloatValueModificationListener progressListener
        = new FloatValueModificationListener()
        {
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                setProgress(e.getVal());
            }
        };

    protected Pick3DListener pick3DListener = new Pick3DListener(PickType.POINT)
    {
        @Override
        public void handlePick3D(Pick3DEvent e)
        {
            float[] x = e.getPoint();
            if (x == null || outIrregularField == null || polylines == null)
                return;
            int k = -1;
            float fmin = Float.MAX_VALUE;
            float[] coords = outIrregularField.getCurrentCoords() == null ? null : outIrregularField.getCurrentCoords().getData();
            for (int i = 0; i < coords.length; i += 3) {
                float f = (coords[i] - x[0]) * (coords[i] - x[0]) +
                    (coords[i + 1] - x[1]) * (coords[i + 1] - x[1]) +
                    (coords[i + 2] - x[2]) * (coords[i + 2] - x[2]);
                if (f < fmin) {
                    fmin = f;
                    k = i / 3;
                }
            }
            if (k >= 0) {
                pickedNode = k;
                params.setRecompute(SkeletonizerParams.SELECT);
                startAction();
            }
        }
    };

    public Skeletonizer()
    {
        parameters = params = new SkeletonizerParams();
        params.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                if (params.isRecompute() > 0)
                    startAction();
                else
                    show();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                computeUI = new GUI();
            }
        });
        computeUI.setParams(params);
        ui.addComputeGUI(computeUI);
        outObj.setName("skeletonizer");
        setPanel(ui);
    }

    @Override
    public Pick3DListener getPick3DListener()
    {
        return pick3DListener;
    }

    public void onActive()
    {
        if (getInputFirstValue("inField") == null)
            return;
        inField = ((VNRegularField) getInputFirstValue("inField")).getField();
        if (inField == null)
            return;
        if (getInputFirstValue("start points") != null && 
            ((VNField)getInputFirstValue("start points")).getField() != null) {
            Field startPointsField = ((VNField)getInputFirstValue("start points")).getField();
            startPoints = new int[(int)startPointsField.getNNodes()][3];
            float[] c = startPointsField.getCurrentCoords().getData();
            for (int i = 0, k = 0; i < startPointsField.getNNodes(); i++) 
                for (int j = 0; j < 3; j++, k++) 
                    startPoints[i][j] = (int)c[k];
        }
        if (inField != lastInField) {
            lastInField = inField;
            if (skeletonizer != null)
                skeletonizer.clearFloatValueModificationListener();
            if (inField.getDims().length == 2)
                skeletonizer = new Compute2D(inField, params);
            else
                skeletonizer = new Compute3D(inField, params);
            skeletonizer.addFloatValueModificationListener(progressListener);
            skeletonizer.addActivityListener(computeUI.getActivityListener());
            computeUI.setInfield(inField);
            params.setRecompute(SkeletonizerParams.SKELETONIZE);
            return;
        }
        if (inField == null)
            return;
        if (params.isRecompute() == SkeletonizerParams.SKELETONIZE) {
            skeletonizer.setInitDiameter(params.getMinCenterDepth());
            skeletonizer.updateSkeleton();
            mapField = inField.cloneShallow();
            mapField.removeComponents();
            mapField.addComponent(inField.getComponent(params.getComponent()).cloneShallow());
            mapField.addComponent(DataArray.create(skeletonizer.getOutd(), 1, "distance map"));
            setOutputValue("mapField", new VNRegularField(mapField));
        }
        if (params.isRecompute() >= SkeletonizerParams.MIN_LENGTH) {
            nPolys = 0;
            int[] activeSets = params.getSets();
            if (activeSets == null || activeSets.length < 1)
                activeSets = new int[]{2};
            int[][] nNodes = skeletonizer.getNNodes();
            if (nNodes == null)
                return;
            float[][][] coords = skeletonizer.getCoords();
            short[][][] radii = skeletonizer.getRadiiData();
            int[][] endSegments = skeletonizer.getEndSegments();
            int[][] endNodes    = skeletonizer.getEndNodes();
            int[][] startNodes  = new int[endSegments.length][];
            int[][] resEndNodes    = new int[endSegments.length][];
            for (int i = 0; i < resEndNodes.length; i++) {
                startNodes[i]     = new int[endNodes[i].length];
                resEndNodes[i]    = new int[endNodes[i].length];
                for (int j = 0; j < resEndNodes[i].length; j++) 
                    resEndNodes[i][j] = startNodes[i][j] = -1;
            }
            int nVerts = 0, nCells = 0, nMax = 0;
            for (int iSet = 0; iSet < activeSets.length; iSet++) {
                int nSet = activeSets[iSet] - 2;
                if (nSet > nMax)
                    nMax = nSet;
                if (nNodes[nSet] == null || nNodes[nSet].length < 1)
                    continue;
                int cNPolys = nNodes[nSet].length;
                for (int iPoly = 0; iPoly < cNPolys; iPoly++)
                    if (nNodes[nSet][iPoly] >= params.getMinSegLen()) {
                        startNodes[nSet][iPoly] = nVerts;
                        if (endSegments[nSet][iPoly] != -1 && startNodes[nSet][endSegments[nSet][iPoly]] != -1) {
                            resEndNodes[nSet][iPoly] = startNodes[nSet][endSegments[nSet][iPoly]] + endNodes[nSet][iPoly];
                            nCells += 1;
                        }
                        nVerts += nNodes[nSet][iPoly];
                        nCells += nNodes[nSet][iPoly] - 1;
//                        int iEndSeg = endSegments[nSet][iPoly];
//                        if (iEndSeg >= 0 && startNodes[nSet][iEndSeg] >= 0)
//                            nCells += 1;
                        nPolys += 1;
                    }
            }
            polylines = new int[nPolys + 1];
            polylines[0] = 0;
            if (nVerts == 0)
                return;
            crds = new float[3 * nVerts];
            params.setRecompute(0);
            int[] cells = new int[2 * nCells];
            rData = new short[nVerts];
            for (int iSet = 0, k = 0, l = 0, m = 0, p = 1; iSet < activeSets.length; iSet++) {
                int nSet = activeSets[iSet] - 2;
                if (nNodes[nSet] == null || nNodes[nSet].length < 1)
                    continue;
                int cNPolys = nNodes[nSet].length;
                for (int iPoly = 0; iPoly < cNPolys; iPoly++) {
                    if (nNodes[nSet][iPoly] < params.getMinSegLen())
                        continue;
                    System.arraycopy(radii[nSet][iPoly], 0, rData, m, nNodes[nSet][iPoly]);
                    System.arraycopy(coords[nSet][iPoly], 0, crds, k, coords[nSet][iPoly].length);
                    k += coords[nSet][iPoly].length;
                    polylines[p] = polylines[p - 1] + nNodes[nSet][iPoly];
                    for (int i = 0; i < nNodes[nSet][iPoly] - 1; i++, l += 2) {
                        cells[l] = m + i;
                        cells[l + 1] = m + i + 1;
                    }
                    if (resEndNodes[nSet][iPoly] != -1)
                    {
                        cells[l] = m + nNodes[nSet][iPoly] - 1;
                        cells[l + 1] = resEndNodes[nSet][iPoly];
                        l += 2;
                    }
                    m += nNodes[nSet][iPoly];
                }
            }
            
            
            byte[] edgeOrientations = new byte[nCells];
            for (int i = 0; i < edgeOrientations.length; i++)
                edgeOrientations[i] = 1;
            outIrregularField = new IrregularField(nVerts);
            outIrregularField.setCurrentCoords(new FloatLargeArray(crds));
            CellSet cellSet = new CellSet(inField.getName() + "skeleton");
            DataArray da = DataArray.create(rData, 1, "radii");
            outIrregularField.addComponent(da);
            CellArray skeletonSegments = new CellArray(CellType.SEGMENT, cells, edgeOrientations, null);
            cellSet.setBoundaryCellArray(skeletonSegments);
            cellSet.setCellArray(skeletonSegments);
            outIrregularField.addCellSet(cellSet);
            outIrregularField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
            setOutputValue("outField", new VNIrregularField(outIrregularField));
            outField = outIrregularField;
            prepareOutputGeometry();
            IrregularField tree = SkeletonTree.createTree(crds, cells, rData);
            setOutputValue("treeField", new VNIrregularField(tree));
        }
        if (params.isRecompute() == SkeletonizerParams.SELECT)
            for (int i = 1; i < polylines.length; i++)
                if (pickedNode < polylines[i]) {
                    int low = polylines[i - 1];
                    int up = polylines[i];
                    int n = up - low;
                    System.out.printf("%5d < %5d < %5d%n", low, pickedNode, up);
                    int[] segDims = new int[]{n};
                    segmentField = new RegularField(segDims);
                    float[] segCoords = new float[3 * n];
                    short[] segRData = new short[n];
                    System.arraycopy(crds, 3 * low, segCoords, 0, 3 * n);
                    System.arraycopy(rData, low, segRData, 0, n);
                    segmentField.setCurrentCoords(new FloatLargeArray(segCoords));
                    segmentField.addComponent(DataArray.create(segRData, 1, "radii"));
                    setOutputValue("segmentField", new VNRegularField(segmentField));
                    
                    break;
                }
        show();
        params.setRecompute(0);
    }
}
