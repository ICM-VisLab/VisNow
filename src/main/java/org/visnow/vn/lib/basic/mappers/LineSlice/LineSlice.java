/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.LineSlice;

import java.util.Arrays;
import org.apache.log4j.Logger;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.RegularField;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.TimeData;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.mappers.LineSlice.LineSliceShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class LineSlice extends OutFieldVisualizationModule
{

    private static final Logger LOGGER = Logger.getLogger(LineSlice.class);
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private LineSliceGUI computeUI = null;
    protected RegularField inField = null;
    protected int dim;
    protected int[] dims;
    protected int nComps;
    private boolean fromParams = false;

    public LineSlice()
    {
        parameters.addParameterChangelistener((String name) -> {
            fromParams = true;
            startAction();
        });
        SwingInstancer.swingRunAndWait(() -> {
            computeUI = new LineSliceGUI();
        });
        computeUI.setParameters(parameters);
        ui.addComputeGUI(computeUI);
        setPanel(ui);
    }

    private class SliceArray implements Runnable
    {

        private final int iThread;
        private final int nThreads;
        private final DataArray[] out;
        long n, start, step;

        public SliceArray(int iThread, int nThreads,
                          long n, long start, long step,
                          DataArray[] out)
        {
            this.iThread = iThread;
            this.nThreads = nThreads;

            this.n = n;
            this.start = start;
            this.step = step;

            this.out = out;
        }

        @Override
        public void run()
        {
            int nData = inField.getNComponents();
            for (int idata = (iThread * nData) / nThreads;
                idata < ((iThread + 1) * nData) / nThreads;
                idata++) {
                DataArray data = inField.getComponent(idata);
                if (data == null)
                    continue;
                TimeData timeData = data.getTimeData();
                int veclen = data.getVectorLength();
                DataArray outDa = DataArray.create(data.getType(), n, veclen,
                                                   data.getName(), data.getUnit(),
                                                   data.getUserData());
                for (int iStep = 0; iStep < timeData.getNSteps(); iStep++) {
                    LargeArray inData = timeData.getValue(timeData.getTime(iStep));
                    LargeArray outData = LargeArrayUtils.create(inData.getType(), n * veclen, false);
                    for (long i = 0, j = start; i < n; i++, j += step) {
                        for (int k = 0; k < veclen; k++) {
                            outData.set(i * veclen + k, inData.get(j * veclen + k));
                        }
                    }
                    outDa.addRawArray(outData, timeData.getTime(iStep), false);
                }

                if (outDa != null) {
                    outDa.recomputeStatistics();
                    outDa.setPreferredRanges(data.getPreferredMinValue(),     data.getPreferredMaxValue(),
                                             data.getPreferredPhysMinValue(), data.getPreferredPhysMaxValue());
                    out[idata] = outDa;
                }
            }
        }
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(I_AXIS_VALUE, -1),
            new Parameter<>(J_AXIS_VALUE, -1),
            new Parameter<>(K_AXIS_VALUE, -1),
            new Parameter<>(SELECTED_AXIS, Axis.I),
            new Parameter<>(RECALCULATE_MIN_MAX, false),
            new Parameter<>(ADJUSTING, false),
            new Parameter<>(META_FIELD_DIMENSION_LENGTHS, new int[]{2, 2, 2})
        };
    }

    public void update()
    {
        Axis axis = parameters.get(SELECTED_AXIS);
        int ax = axis == Axis.I ? 0 : axis == Axis.J ? 1 : 2;
        int[] crd = new int[] {parameters.get(I_AXIS_VALUE), parameters.get(J_AXIS_VALUE), parameters.get(K_AXIS_VALUE)};
        int dimNum = inField.getDimNum();


        //validate sliders
        for (int i = 0; i < dimNum; i++)
            if (i != ax && (crd[i] < 0 || crd[i] > dims[i])) {
                outField = null;
                outRegularField = null;
                setOutputValue("outField", null);
                return;
            }

        int[] outDims = new int[1];
        outDims[0] = dims[ax];
        outRegularField = new RegularField(outDims);
        float[][] affine = inField.getAffine();
        float[][] sliceAffine = new float[4][3];
        System.arraycopy(affine[ax], 0, sliceAffine[0], 0, 3);
        System.arraycopy(affine[3],  0, sliceAffine[3], 0, 3);
        for (int i = 1; i < 3; i++)
            Arrays.fill(sliceAffine[i], 0);
        for (int i = 0; i < dimNum; i++)
            if (i != ax)
                for (int j = 0; j < 3; j++)
                    sliceAffine[3][j] += crd[i] * affine[i][j];
        outRegularField.setAffine(sliceAffine);
        outRegularField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
        outRegularField.setAxesNames(inField.getAxesNames());
        outRegularField.setCoordsUnits(inField.getCoordsUnits());

        // now we are rewriting values from input data to output array
        // input data is 1D even when there are many dimensions, therefore
        // step variable must be used
        long n = dims[ax];
        long start = 0, step = 1;
        for (int i = 0; i < ax; i++)
            step *= dims[i];
        for (int i = 0, l = 1; i < dimNum; i++) {
            if (i != ax)
                start += crd[i] * l;
            l *= dims[i];
        }

        if (inField.getCurrentCoords() != null) {
            FloatLargeArray inCoords = inField.getCurrentCoords();
            FloatLargeArray outCoords = new FloatLargeArray(n * 3);
            for (long i = 0, j = start; i < n; i++, j += step) {
                for (int k = 0; k < 3; k++) {
                    outCoords.set(i * 3 + k, inCoords.get(j * 3 + k));
                }
            }
            outRegularField.setCoords(outCoords, 0);
        }

        DataArray[] sortedData = new DataArray[inField.getNComponents()];
        int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        if (nThreads > inField.getNComponents())
            nThreads = inField.getNComponents();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread]
                = new Thread(new SliceArray(iThread, nThreads,
                                            n, start, step, sortedData));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        for (int i = 0; i < sortedData.length; i++) {
            if (!parameters.get(RECALCULATE_MIN_MAX)) {
                DataArray inDA = inField.getComponent(i);
                sortedData[i].setPreferredRanges(
                        inDA.getPreferredMinValue(),     inDA.getPreferredMaxValue(),
                        inDA.getPreferredPhysMinValue(), inDA.getPreferredPhysMaxValue());
                sortedData[i].getSchema().setStatisticsComputed(true);
            }
            outRegularField.addComponent(sortedData[i]);
        }
        outField = outRegularField;
        
    }

    private void validateParamsAndSetSmart()
    {
        parameters.setParameterActive(false);
        parameters.set(META_FIELD_DIMENSION_LENGTHS, dims);

        if (parameters.get(SELECTED_AXIS) == Axis.K && dims.length < 3)
            parameters.set(SELECTED_AXIS, Axis.J);

        if (parameters.get(I_AXIS_VALUE) > dims[0] || parameters.get(I_AXIS_VALUE) < 0)
            parameters.set(I_AXIS_VALUE, dims[0] / 2);
        if (parameters.get(J_AXIS_VALUE) > dims[1] || parameters.get(J_AXIS_VALUE) < 0)
            parameters.set(J_AXIS_VALUE, dims[1] / 2);
        if (dims.length > 2 && (parameters.get(K_AXIS_VALUE) > dims[2] || parameters.get(K_AXIS_VALUE) < 0))
            parameters.set(K_AXIS_VALUE, dims[2] / 2);

        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully);
    }

    @Override
    public void onActive()
    {
        if (!fromParams) {
            if (getInputFirstValue("inField") == null)
                return;
            RegularField newField = ((VNRegularField) getInputFirstValue("inField")).getField();
            boolean dimChanged = !isFromVNA() && (inField == null || !Arrays.equals(inField.getDims(), newField.getDims()));
            inField = newField;
            dims = inField.getDims();
            Parameters p;
            synchronized (parameters) {
                validateParamsAndSetSmart();
                p = parameters.getReadOnlyClone();
                notifyGUIs(p, isFromVNA() || dimChanged, false);
            }
        }
        fromParams = false;
        update();
        setOutputValue("outField", new VNRegularField(outRegularField));
//        if (!parameters.get(ADJUSTING))
            prepareOutputGeometry();
        show();
    }

}
