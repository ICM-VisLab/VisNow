/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces;

import org.jogamp.vecmath.Point3f;
import org.jogamp.vecmath.Vector3f;
import org.apache.log4j.Logger;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ITrackerToVworldGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.CoordinateSystem;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.HapticLocationData;
import static org.apache.commons.math3.util.FastMath.*;

/**
 * Damping effect.
 * Tracker coordinate system is used to get velocity and compute force. It makes the damping
 * independent of size of haptic field.
 * <p/>
 * When spring constant is high, haptic device starts shaking.
 * <p/>
 * Damping constant must be carefully set. When damping constant is high, buzzing and shuddering or
 * even a kicks in opposite direction may occur.<br/>
 * As Krzysiek Madejski wrote, taking an average of current force and the previous one was
 * sufficient to get rid of those effects.
 * For other ways of coping with those effects, see comment in {@link DampingField}.
 * <p/>
 * From OpenHaptics Programmer's Guide about Damping:
 * "Setting damping too high can cause instability and oscillation. The purpose of damping is
 * to provide some retardation to the device's velocity; however, if the value is too high, then
 * the reaction force could instead send the device in the opposite direction of its current
 * motion. Then in the next iteration, the damping force would again send the device in the
 * opposite direction, etc. This oscillation will manifest as buzzing."
 * <p/>
 *
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of
 * Warsaw, 2013
 */
public class Damping extends AbstractForce
{

    /**
     * Uses tracker velocity to compute damping. The same as safety damping - it is independent of
     * size of haptic field.
     */
    private final static CoordinateSystem forceCoordinateSystem = CoordinateSystem.TRACKER;
    //
    public static final float MAX_DAMPING_CONSTANT = 1;
    public static final float MAX_VELOCITY_DERIVATE = 4;
    public static final float DEFAULT_DAMPING_CONSTANT = 20f;
    protected float dampingConstant;
    protected Point3f lastVelocity = new Point3f();
    protected Point3f lastForce = new Point3f();
    protected Algorithm algorithm = Algorithm.AVERAGE_FORCE_50_MEMORY;
    //
    private static final Logger LOGGER = Logger.getLogger(new Throwable().getStackTrace()[0].getClassName());

    public enum Algorithm
    {

        DIRECT,
        AVERAGE_FORCE_50,
        AVERAGE_FORCE_50_WFUSE,
        AVERAGE_FORCE_50_MEMORY,
    }

    /**
     * Copy constructor. To be used only by
     * <code>clone()</code> method.
     * <p/>
     * @param force Object to be copied from
     */
    protected Damping(Damping force)
    {
        super(force);
        dampingConstant = force.dampingConstant;
        algorithm = force.algorithm;
    }

    public Damping()
    {
        this(DEFAULT_DAMPING_CONSTANT);
    }

    public Damping(float dampingConstant)
    {
        super(forceCoordinateSystem);

        setDampingConstant(dampingConstant);
    }

    @Override
    public void getForce(HapticLocationData locationData, Vector3f out_force)
        throws ITrackerToVworldGetter.NoDataException
    {

        Vector3f velocity = locationData.getCurrentTrackerVelocity();

        switch (algorithm) {
            case AVERAGE_FORCE_50_MEMORY:
                //tlumienie z usrednianiem sił
                out_force.x = (dampingConstant * velocity.x + lastForce.x) / 2;
                out_force.y = (dampingConstant * velocity.y + lastForce.y) / 2;
                out_force.z = (dampingConstant * velocity.z + lastForce.z) / 2;

                //ograniczenie sil
                if (abs(velocity.x - lastVelocity.x) > MAX_VELOCITY_DERIVATE) {
                    out_force.x = (lastForce.x * 3 + out_force.x) * 0.25f;
                }
                if (abs(velocity.y - lastVelocity.y) > MAX_VELOCITY_DERIVATE) {
                    out_force.y = (lastForce.y * 3 + out_force.y) * 0.25f;
                }
                if (abs(velocity.z - lastVelocity.z) > MAX_VELOCITY_DERIVATE) {
                    out_force.z = (lastForce.z * 3 + out_force.z) * 0.25f;
                }

                lastVelocity.set(velocity);
                lastForce.set(out_force);
                break;

            case AVERAGE_FORCE_50:
                out_force.scale(dampingConstant, velocity);
                out_force.add(lastForce);
                out_force.scale(0.5f);

                lastForce.scale(dampingConstant, velocity);
                break;

            case AVERAGE_FORCE_50_WFUSE:
                out_force.scale(dampingConstant, velocity);
                out_force.add(lastForce);
                out_force.scale(0.5f);

                LOGGER.info("Force: " + abs(out_force.x) + ";" +
                    abs(out_force.y) + ";" +
                    abs(out_force.z) + ";");
                //TODO MEDIUM: probably there is a typo below: three times "velocity.z".
                // force limits (by Krzysztof Madejski)
                if (abs(out_force.x) > 8) {
                    out_force.x = dampingConstant * signum(velocity.z);
                }
                if (abs(out_force.y) > 8) {
                    out_force.y = dampingConstant * signum(velocity.z);
                }
                if (abs(out_force.z) > 8) {
                    out_force.z = dampingConstant * signum(velocity.z);
                }

                /*
                 * TODO: Jakoś działa, choć jak szybko machnę sam, to się włącza
                 * i leciutko samo leci
                 if (abs(velocity[0] - lastVelocity[0]) > MAX_VELOCITY_DERIVATE) {
                 outForce[0] = dampingConstant * signum(velocity[0]);
                 }
                 if (abs(velocity[1] - lastVelocity[1]) > MAX_VELOCITY_DERIVATE) {
                 outForce[1] = dampingConstant * signum(velocity[1]);
                 }
                 if (abs(velocity[2] - lastVelocity[2]) > MAX_VELOCITY_DERIVATE) {
                 outForce[2] = dampingConstant * signum(velocity[2]);
                 }
                 */
                lastVelocity.set(velocity);
                lastForce.scale(dampingConstant, velocity);
                break;

            case DIRECT:
                out_force.scale(dampingConstant, velocity);
                break;
        }

        out_force.set(out_force);
    }

    private void resetLastFields()
    {
        lastVelocity.scale(0);
        lastForce.scale(0);
    }

    public Algorithm getAlgorithm()
    {
        return algorithm;
    }

    public void setAlgorithm(Algorithm algorithm)
    {
        this.algorithm = algorithm;
        resetLastFields();
    }

    public double getDampingConstant()
    {
        return dampingConstant;
    }

    public final void setDampingConstant(float dampingConstant)
    {
        this.dampingConstant = -abs(dampingConstant);
    }

    @Override
    public IForce clone()
    {
        return new Damping(this);
    }

    @Override
    public String getName()
    {
        return "Damping [k=" + dampingConstant + "]";
    }
}
