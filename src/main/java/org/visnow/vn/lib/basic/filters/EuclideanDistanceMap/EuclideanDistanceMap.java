/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.EuclideanDistanceMap;

import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNRegularField;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class EuclideanDistanceMap extends OutFieldVisualizationModule
{
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    protected RegularField inRegularField = null;
    protected Field inMesh = null;
    protected int trueDim;
    protected int nThreads = 1;
    protected int nMeshNodes = 0;
    protected float[] meshCoords;

    public EuclideanDistanceMap()
    {
        setPanel(ui);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null || getInputFirstValue("inMesh") == null)
            return;
        RegularField field = ((VNRegularField) getInputFirstValue("inField")).getField();
        if (field == null)
            return;
        if (inRegularField != field) {
            inRegularField = field;
            if (inRegularField.getTrueNSpace() != inRegularField.getDimNum() || inRegularField.hasCoords())
                return;
            
        }
        Field mesh = ((VNField) getInputFirstValue("inMesh")).getField();
        if (mesh == null || trueDim < mesh.getTrueNSpace())
            return;
        if (inMesh != mesh)
            inMesh = mesh;
        nMeshNodes = (int)inMesh.getNNodes();
        outField = inMesh.cloneShallow();
        if (inMesh.hasCoords())
            meshCoords = inMesh.getCurrentCoords().getData();
        else if (inMesh instanceof RegularField)
            meshCoords = ((RegularField)inMesh).getCoordsFromAffine().getData();
        else
           return;
        int[] dims = inRegularField.getDims();
        float[] setCoords; 
        if (dims.length == 2) {
        
        }
//        float[] map = createMap2D(inRegularField.getDims(), meshCoords);

        if (outField instanceof RegularField)
            setOutputValue("outRegularField", new VNRegularField((RegularField) outField));

        if (outField != null){
            if (outField instanceof RegularField) {
            setOutputValue("outRegularField", new VNRegularField((RegularField) outField));
            setOutputValue("outIrregularField", null);
        } else {
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", new VNIrregularField((IrregularField) outField));
        }
        } else {
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", null);
        }
        prepareOutputGeometry();
        show();

    }
}
