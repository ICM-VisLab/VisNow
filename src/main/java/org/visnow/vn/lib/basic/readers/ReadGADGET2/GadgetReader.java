/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadGADGET2;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class GadgetReader extends OutFieldVisualizationModule
{

    protected GUI computeUI = null;
    protected Params params;
    protected boolean fromGUI = false;
    protected RegularField outMassDensityField = null;
    private ReadGadgetData core;

    /**
     * Creates a new instance of CreateGrid
     */
    public GadgetReader()
    {
        parameters = params = new Params();
        params.addChangeListener(new ChangeListener()
        {

            @Override
            public void stateChanged(ChangeEvent evt)
            {
                startAction();
            }
        });
        params.addParameterChangelistener(new ParameterChangeListener()
        {

            @Override
            public void parameterChanged(String name)
            {
                if ("show".equals(name)) {
                    fromGUI = true;
                    startAction();
                }
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {

            @Override
            public void run()
            {
                computeUI = new GUI();
            }
        });
        computeUI.setParams(params);
        ui.addComputeGUI(computeUI);
        setPanel(ui);

        core = new ReadGadgetData(params);
        core.addChangeListener(new ChangeListener()
        {

            @Override
            public void stateChanged(ChangeEvent e)
            {
                setProgress(core.getProgress());
            }
        });
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    public static OutputEgg[] outputEggs = null;

    @Override
    public void onActive()
    {
        if (!fromGUI) {
            if (params.getFilePaths() == null) {
                return;
            }
            outIrregularField = null;

            //long tic = System.currentTimeMillis();
            //outIrregularField = core.readGadgetFullDataTimeSequence(params.getFilePaths());
            outIrregularField = core.readGadgetFullDataTimeSequenceThr(params.getFilePaths());
            //long toc = System.currentTimeMillis();
            //System.out.println("time = "+(toc-tic));

            if (outIrregularField == null) {
                return;
            }

            computeUI.setFieldDescription(outIrregularField.description());
            setOutputValue("outField", new VNIrregularField(outIrregularField));
        }
        outField = outRegularField;
        if (params.isShow()) {
            prepareOutputGeometry();
            show();
        }
        fromGUI = false;
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag())
            computeUI.activateOpenDialog();
    }

}
