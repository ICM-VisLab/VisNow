/* VisNow
   Copyright (C) 2006-2019 University of Warsaw, ICM
   Copyright (C) 2020 onward visnow.org
   All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.writers.HDF5Writer;

import ch.systemsx.cisd.hdf5.CompoundElement;

/**
 * Class representing a complex number. It is needed for storing ComplexDataArray in HDF5 file.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class ComplexNumber
{

    @CompoundElement(memberName = "re")
    private float re;

    @CompoundElement(memberName = "im")
    private float im;

    ComplexNumber()
    {
    }

    public ComplexNumber(float re, float im)
    {
        this.re = re;
        this.im = im;
    }

    public float getRe()
    {
        return re;
    }

    public float getIm()
    {
        return im;
    }

    public float[] get()
    {
        return new float[]{re, im};
    }

    @Override
    public String toString()
    {
        return "ComplexNumber [re=" + re + ", im=" + im + "]";
    }

}
