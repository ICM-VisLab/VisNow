/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.MulticomponentHistogram;

import java.util.ArrayList;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.Field;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class OperationsTableModel extends DefaultTableModel
{

    private Field field = null;
    private int[] components = null;
    private int[] componentsVeclen = null;
    private int[][] map = null;

    public OperationsTableModel(Field field)
    {
        super();
        this.field = field;

        ArrayList<DataArraySchema> tmp = field.getSchema().getComponentSchemas();
        components = new int[tmp.size()];
        componentsVeclen = new int[tmp.size()];
        for (int i = 0; i < tmp.size(); i++) {
            components[i] = i;
            componentsVeclen[i] = field.getComponent(i).getVectorLength();
        }

        map = new int[6][components.length];
        for (int i = 0; i < components.length; i++) {
            for (int j = 0; j < 6; j++) {
                map[j][i] = 0;
            }
        }
    }

    public OperationsTableModel(Field field, int[][] map)
    {
        super();
        this.field = field;

        ArrayList<DataArraySchema> tmp = field.getSchema().getComponentSchemas();
        components = new int[tmp.size()];
        componentsVeclen = new int[tmp.size()];
        for (int i = 0; i < tmp.size(); i++) {
            components[i] = i;
            componentsVeclen[i] = field.getComponent(i).getVectorLength();
        }

        this.map = map;
    }

    public int[][] getMap()
    {
        return map;
    }

    @Override
    public int getRowCount()
    {
        if (field == null)
            return 0;

        return components.length;
    }

    @Override
    public int getColumnCount()
    {
        return 7;
    }

    @Override
    public String getColumnName(int columnIndex)
    {
        switch (columnIndex) {
            case 0:
                return "Component";
            case 1:
                return "sum";
            case 2:
                return "min";
            case 3:
                return "max";
            case 4:
                return "avg";
            case 5:
                return "std";
            case 6:
                return "vstd";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        switch (columnIndex) {
            case 0:
                return String.class;
            default:
                return Integer.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        return !(columnIndex == 0) || (columnIndex == 6 && componentsVeclen[rowIndex] == 1);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        if (field == null)
            return null;

        if (rowIndex < 0 || rowIndex >= components.length)
            return null;

        switch (columnIndex) {
            case 0:
                return field.getComponent(components[rowIndex]).getName();
            default:
                return map[columnIndex - 1][rowIndex];
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex)
    {
        if (field == null)
            return;

        switch (columnIndex) {
            case 0:
                return;
            default:
                map[columnIndex - 1][rowIndex] = (Integer) aValue;
                break;
        }
        fireTableCellUpdated(rowIndex, columnIndex);
        fireTableDataChanged();
    }

    /**
     * @return the componentsVeclen
     */
    public int[] getComponentsVeclen()
    {
        return componentsVeclen;
    }

}
