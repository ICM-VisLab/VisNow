/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadVisNowField.utils;

import java.io.LineNumberReader;
import java.util.Scanner;
import javax.imageio.stream.ImageInputStream;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Skip
{

    private Skip()
    {
    }

    public static void skip(SkipSchema schema, ImageInputStream in)
    {
        try {
            switch (schema.getType()) {
                case SkipSchema.COUNT:
                    in.skipBytes(schema.getCount());
                    return;
                default:
                    String line;
                    do {
                        line = in.readLine();
                        if (line.matches(schema.getPattern()))
                            return;
                    } while (line != null);
            }
        } catch (Exception e) {
        }
    }

    public static void skip(SkipSchema schema, LineNumberReader in)
    {
        String line;
        int lineNumber;
        try {
            switch (schema.getType()) {
                case SkipSchema.COUNT:
                    for (int i = 0; i < schema.getCount(); i++)
                        in.readLine();
                    return;
                case SkipSchema.PATTERN:
                    do {
                        lineNumber = in.getLineNumber();
                        line = in.readLine();
                        if (line.matches(schema.getPattern())) {
                            in.setLineNumber(lineNumber);
                            return;
                        }
                    } while (line != null);
                default:
                    do {
                        line = in.readLine();
                        if (line.matches(schema.getPattern()))
                            return;
                    } while (line != null);
            }
        } catch (Exception e) {
        }
    }

    public static void skip(SkipSchema schema, Scanner in)
    {
        String line;
        int lineNumber;
        try {
            switch (schema.getType()) {
                case SkipSchema.COUNT:
                    for (int i = 0; i < schema.getCount(); i++)
                        in.nextLine();
                    return;
                default:
                    in.findInLine(schema.getPattern());
                    in.nextLine();
            }
        } catch (Exception e) {
        }
    }

    public static void skip(SkipSchema schema, Object in)
    {
        if (in instanceof Scanner)
            skip(schema, (Scanner) in);
        else if (in instanceof LineNumberReader)
            skip(schema, (LineNumberReader) in);
        else if (in instanceof ImageInputStream)
            skip(schema, (ImageInputStream) in);
    }
}
