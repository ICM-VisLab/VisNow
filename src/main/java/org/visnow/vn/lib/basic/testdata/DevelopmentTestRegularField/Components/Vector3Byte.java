/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.testdata.DevelopmentTestRegularField.Components;

import org.visnow.jlargearrays.UnsignedByteLargeArray;

import org.apache.log4j.Logger;

import static org.apache.commons.math3.util.FastMath.*;

/**
 * Class creating data array consisting of 3-dimensional <tt>byte</tt> vectors.
 * Class cannot be extended, as static methods (e.g. <tt>name</tt>) are not inherited.
 *
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl), Warsaw University, ICM
 * @see AbstractComponent
 */
public final class Vector3Byte extends AbstractComponent
{
    private static final Logger LOGGER = Logger.getLogger(Vector3Byte.class);

    private final int nThreads = Runtime.getRuntime().availableProcessors();

    public Vector3Byte()
    {
        veclen = 3;
        data = null;
    }

    /**
     * Method providing a human-readable name of component presented in
     * this class.
     * All methods, that inherit from <tt>AbstractComponent</tt> class should have
     * this method implemented. Lack of this method will result in appearance
     * of (not always informative) bare class names in user interfaces.
     *
     * @return The human-readable name of the data component.
     * @see AbstractComponent
     */
    public static final String getName()
    {
        return "Three-dimensional vector (byte)";
    }

    /**
     * Method, that computes actual values of component on every point
     * on the grid.
     * Core calculations should be carried out in this method. A constructor
     * should be kept as light as possible.
     *
     * @param dims An array containing the dimensions of the grid. It must have
     *             one to three elements - by design all of these values (i.e. 1, 2 and 3)
     *             are supported by every data component class.
     * @see AbstractComponent
     * @see org.visnow.jscic.RegularField
     */
    @Override
    public void compute(int[] dims)
    {
        long length = veclen;

        for (int i = 0; i < dims.length; i++) {
            length *= dims[i];
        }

        data = new UnsignedByteLargeArray(length, false);

        Thread[] workThreads = new Thread[nThreads];
        for (int i = 0; i < workThreads.length; i++) {
            workThreads[i] = new Thread(new ComputeThreaded(nThreads, i, dims));
            workThreads[i].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
    }

    class ComputeThreaded implements Runnable
    {
        int nThreads;
        int iThread;

        int[] dims;

        public ComputeThreaded(int nThreads, int iThread, int[] dims)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;

            this.dims = dims;
        }

        @Override
        public void run()
        {
            double x, y, z;

            final double x0 = 6.0, y0 = 6.0, z0 = 6.0;
            double dist0;

            switch (dims.length) {
                case 3:
                    for (long k = iThread; k < dims[2]; k += nThreads) {
                        for (long j = 0; j < dims[1]; ++j) {
                            for (long i = 0; i < dims[0]; ++i) {
                                x = 5.0 / ((double) dims[0] - 1.0) * (double) i;
                                y = 5.0 / ((double) dims[1] - 1.0) * (double) j;
                                z = 5.0 / ((double) dims[2] - 1.0) * (double) k;
                                dist0 = sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0) + (z - z0) * (z - z0));

                                ((UnsignedByteLargeArray) data).setByte(veclen * (i + dims[0] * (j + k * dims[1])) + 0, (byte) ((byte) 0xFF & (byte) (-150.0 * (x - x0) / (dist0 * dist0 * dist0))));
                                ((UnsignedByteLargeArray) data).setByte(veclen * (i + dims[0] * (j + k * dims[1])) + 1, (byte) ((byte) 0xFF & (byte) (-150.0 * (y - y0) / (dist0 * dist0 * dist0))));
                                ((UnsignedByteLargeArray) data).setByte(veclen * (i + dims[0] * (j + k * dims[1])) + 2, (byte) ((byte) 0xFF & (byte) (-150.0 * (z - z0) / (dist0 * dist0 * dist0))));
                            }
                        }
                    }
                    break;
                case 2:
                    for (long j = iThread; j < dims[1]; j += nThreads) {
                        for (long i = 0; i < dims[0]; ++i) {
                            x = 5.0 / ((double) dims[0] - 1.0) * (double) i;
                            y = 5.0 / ((double) dims[1] - 1.0) * (double) j;
                            z = 2.5;
                            dist0 = sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0) + (z - z0) * (z - z0));

                            ((UnsignedByteLargeArray) data).setByte(veclen * (i + dims[0] * j) + 0, (byte) ((byte) 0xFF & (byte) (-150.0 * (x - x0) / (dist0 * dist0 * dist0))));
                            ((UnsignedByteLargeArray) data).setByte(veclen * (i + dims[0] * j) + 1, (byte) ((byte) 0xFF & (byte) (-150.0 * (y - y0) / (dist0 * dist0 * dist0))));
                            ((UnsignedByteLargeArray) data).setByte(veclen * (i + dims[0] * j) + 2, (byte) ((byte) 0xFF & (byte) (-150.0 * (z - z0) / (dist0 * dist0 * dist0))));
                        }
                    }
                    break;
                case 1:
                    for (long i = iThread; i < dims[0]; i += nThreads) {
                        x = 5.0 / ((double) dims[0] - 1.0) * (double) i;
                        y = 1.5;
                        z = 2.5;
                        dist0 = sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0) + (z - z0) * (z - z0));

                        ((UnsignedByteLargeArray) data).setByte(veclen * i + 0, (byte) ((byte) 0xFF & (byte) (-150.0 * (x - x0) / (dist0 * dist0 * dist0))));
                        ((UnsignedByteLargeArray) data).setByte(veclen * i + 1, (byte) ((byte) 0xFF & (byte) (-150.0 * (y - y0) / (dist0 * dist0 * dist0))));
                        ((UnsignedByteLargeArray) data).setByte(veclen * i + 2, (byte) ((byte) 0xFF & (byte) (-150.0 * (z - z0) / (dist0 * dist0 * dist0))));
                    }
                    break;
                default:
                    LOGGER.fatal("Unrecognized structure of \"dims\" array... dims.length = " + dims.length);
                    throw new RuntimeException("Unrecognized structure of \"dims\" array... dims.length = " + dims.length);
            }
        }
    }
}