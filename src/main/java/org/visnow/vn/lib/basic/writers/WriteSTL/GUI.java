/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.writers.WriteSTL;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.writers.WriteSTL.WriteSTLShared.*;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.swing.filechooser.VNFileChooser;

public class GUI extends javax.swing.JPanel
{

    private String path = null;
    private String lastPath = null;
    private Parameters parameters;

    /**
     * Creates new form EmptyVisnowModuleGUI
     */
    public GUI()
    {
        initComponents();
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        fileChooser = new javax.swing.JFileChooser();
        buttonGroup1 = new javax.swing.ButtonGroup();
        selectButton = new javax.swing.JButton();
        tfFileName = new javax.swing.JTextField();
        writeButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();

        setLayout(new java.awt.GridBagLayout());

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/visnow/vn/lib/basic/writers/WriteSTL/Bundle"); // NOI18N
        selectButton.setText(bundle.getString("GUI.selectButton.text")); // NOI18N
        selectButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                selectButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 1);
        add(selectButton, gridBagConstraints);

        tfFileName.setText(bundle.getString("GUI.tfFileName.text")); // NOI18N
        tfFileName.setMinimumSize(new java.awt.Dimension(4, 22));
        tfFileName.setPreferredSize(new java.awt.Dimension(78, 22));
        tfFileName.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                tfFileNameActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
        add(tfFileName, gridBagConstraints);

        writeButton.setText(bundle.getString("GUI.writeButton.text")); // NOI18N
        writeButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                writeButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 1);
        add(writeButton, gridBagConstraints);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 146, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 312, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        add(jPanel1, gridBagConstraints);

        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setSelected(true);
        jRadioButton1.setText(bundle.getString("GUI.jRadioButton1.text")); // NOI18N
        jRadioButton1.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jRadioButton1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.ABOVE_BASELINE;
        add(jRadioButton1, gridBagConstraints);

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setText(bundle.getString("GUI.jRadioButton2.text")); // NOI18N
        jRadioButton2.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jRadioButton2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.ABOVE_BASELINE;
        add(jRadioButton2, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void tfFileNameActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_tfFileNameActionPerformed
    {//GEN-HEADEREND:event_tfFileNameActionPerformed
        parameters.set(FILE_NAME, tfFileName.getText());
    }//GEN-LAST:event_tfFileNameActionPerformed

    private void writeButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_writeButtonActionPerformed
    {//GEN-HEADEREND:event_writeButtonActionPerformed
        if (parameters.get(FILE_NAME) != null && parameters.get(FILE_NAME).length() > 4 && VNFileChooser.checkForOverwrite(path)) {
            parameters.set(WRITE, true);
        }
    }//GEN-LAST:event_writeButtonActionPerformed

    private void selectButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_selectButtonActionPerformed
    {//GEN-HEADEREND:event_selectButtonActionPerformed
        if (lastPath == null)
            fileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getUsableDataPath(WriteSTL.class)));
        else
            fileChooser.setCurrentDirectory(new File(lastPath));
        FileNameExtensionFilter stlExtensionFilter = new FileNameExtensionFilter("STL", "stl");
        fileChooser.setFileFilter(stlExtensionFilter);
        int returnVal = fileChooser.showDialog(this, "Select");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            path = VNFileChooser.filenameWithExtenstionAddedIfNecessary(fileChooser.getSelectedFile(), stlExtensionFilter);
            tfFileName.setText(path);
            lastPath = path.substring(0, path.lastIndexOf(File.separator));
            VisNow.get().getMainConfig().setLastDataPath(lastPath, WriteSTL.class);
            parameters.set(FILE_NAME, path, WRITE, false);
        }
    }//GEN-LAST:event_selectButtonActionPerformed

    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
        if (this.jRadioButton1.isSelected()) {
            parameters.set(ASCII, true);
        }
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
        if (this.jRadioButton2.isSelected()) {
            parameters.set(ASCII, false);
//            this.jLabel1.setText("add JVM arg: -Dfile.encoding=ISO-8859-1");
        }
    }//GEN-LAST:event_jRadioButton2ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JButton selectButton;
    private javax.swing.JTextField tfFileName;
    private javax.swing.JButton writeButton;
    // End of variables declaration//GEN-END:variables

    void updateGUI(ParameterProxy p, boolean resetFully)
    {
        tfFileName.setText(p.get(FILE_NAME));
        path = p.get(FILE_NAME);
        if (resetFully) {
            jRadioButton1.setSelected(p.get(ASCII));
            jRadioButton2.setSelected(!p.get(ASCII));
        }
    }

    void setParameters(Parameters parameters)
    {
        this.parameters = parameters;
    }
}
