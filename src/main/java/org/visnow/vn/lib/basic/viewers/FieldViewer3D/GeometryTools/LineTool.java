/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.GeometryTools;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.CalculableParameter;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class LineTool extends GeometryTool
{

    private final int pointRadius = 2;

    private Point startPoint = null;
    private Point currentPoint = null;
    private Point endPoint = null;

    private Metadata startPointMetadata = null;
    private Metadata endPointMetadata = null;

    @Override
    public void paint(Graphics g)
    {
        if (holding && startPoint != null && currentPoint != null) {
            g.setColor(Color.RED);
            g.fillRect(startPoint.x - pointRadius, startPoint.y - pointRadius, 2 * pointRadius + 1, 2 * pointRadius + 1);
            g.drawLine(startPoint.x, startPoint.y, currentPoint.x, currentPoint.y);
        }
    }

    @Override
    public void mouseDragged(MouseEvent e)
    {
    }

    @Override
    public void mouseMoved(MouseEvent e)
    {
        if (holding) {
            currentPoint = e.getPoint();
            fireGeometryToolRepaintNeeded();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
        if (holding) {
            holding = false;
            this.endPoint = e.getPoint();
            if (metadata != null)
                this.endPointMetadata = metadata.clone();
            fireGeometryToolRepaintNeeded();
            metadata = null;
            fireGeometryToolStateChanged();
        } else {
            holding = true;
            this.startPoint = e.getPoint();
            if (metadata != null)
                this.startPointMetadata = metadata.clone();
            this.currentPoint = e.getPoint();
            fireGeometryToolRepaintNeeded();
        }
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
    }

    @Override
    public void mouseEntered(MouseEvent e)
    {

    }

    @Override
    public void mouseExited(MouseEvent e)
    {

    }

    @Override
    public Cursor getCursor()
    {
        return Cursor.getDefaultCursor();
    }

    @Override
    public boolean isMouseWheelBlocking()
    {
        return false;
    }

    @Override
    public int[][] getPoints()
    {
        if (startPoint == null || endPoint == null)
            return null;

        int[][] out = new int[2][2];
        out[0][0] = startPoint.x;
        out[0][1] = startPoint.y;
        out[1][0] = endPoint.x;
        out[1][1] = endPoint.y;
        return out;
    }

    @Override
    public Metadata[] getPointMetadata()
    {
        if (startPoint == null || endPoint == null)
            return null;

        Metadata[] out = new Metadata[2];
        out[0] = startPointMetadata;
        out[1] = endPointMetadata;
        return out;
    }

    @Override
    public int[][] getConnections()
    {
        if (startPoint == null || endPoint == null)
            return null;

        int[][] out = new int[1][2];
        out[0][0] = 0;
        out[0][1] = 1;
        return out;

    }

    @Override
    public CalculableParameter getCalculable()
    {
        return null;
    }

    @Override
    public Metadata getCalculableMetadata()
    {
        return null;
    }

    @Override
    public int getMinimumNPoints()
    {
        return 2;
    }

}
