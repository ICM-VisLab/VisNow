/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.AddMargins;

import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.gui.widgets.RunButton;
import org.visnow.jvia.spatialops.Padding.PaddingType;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class AddMarginsShared {
    //Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!

    //the amount of padding in each dimension
    //2-dim array of length 3 x 2
    static final ParameterName<long[][]> MARGINS = new ParameterName("Margins");

    static final ParameterName<PaddingType> PADDING_TYPE = new ParameterName("Padding type");

    static final ParameterName<Boolean> REFLECT_VECTORS = new ParameterName("Reflect vectors");

    //list of unique integers within range 0 .. <number_of_components>-1 
    static final ParameterName<int[]> SELECTED_COMPONENTS = new ParameterName("Selected components");

    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>("Running message");
    
    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic)    
    //not null array of Strings, may be empty
    static final ParameterName<String[]> META_COMPONENT_NAMES = new ParameterName("META component names");
    
}
