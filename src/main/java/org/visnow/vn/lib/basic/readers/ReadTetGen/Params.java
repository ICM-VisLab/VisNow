/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadTetGen;

import org.visnow.vn.lib.basic.readers.ReadUCD.*;
import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.lib.utils.io.InputSource;

/**
 *
 * @author @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class Params extends Parameters
{

    @SuppressWarnings("unchecked")
    public Params()
    {
        super(eggs);
    }

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<String>("fileName", ParameterType.filename, ""),
        new ParameterEgg<Boolean>("materials as sets", ParameterType.independent, true),
        new ParameterEgg<Boolean>("show", ParameterType.independent, true),
        new ParameterEgg<Integer>("input source", ParameterType.independent, InputSource.FILE)
    };

    public String getFileName()
    {
        return (String) getValue("fileName");
    }

    public void setFileName(String fileName)
    {
        setValue("fileName", fileName);
    }

    public boolean materialsAsSets()
    {
        return (Boolean) getValue("materials as sets");
    }

    public void setMaterialsAsSets(boolean materialsAsSets)
    {
        setValue("materials as sets", materialsAsSets);
    }

    public boolean isShow()
    {
        return (Boolean) getValue("show");
    }

    public void setShow(boolean show)
    {
        setValue("show", show);
    }

    public int getSource()
    {
        return (Integer) getValue("input source");
    }

    public void setSource(int source)
    {
        setValue("input source", source);
    }
}
