/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadFluent;

import java.util.ArrayList;
import java.util.HashMap;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw
 * Interdisciplinary Centre for Mathematical and Computational Modelling *
 */
public class FluentCell implements Cloneable
{

    //Fluent Element Types
    public static final int ELEMENT_TYPE_MIXED = 0; // nodes/cell   faces/cell   visnow id
    public static final int ELEMENT_TYPE_TRIANGULAR = 1; // 3            3            2
    public static final int ELEMENT_TYPE_TETRAHEDRAL = 2; // 4            4            4
    public static final int ELEMENT_TYPE_QUADRILATERAL = 3; // 4            4            3
    public static final int ELEMENT_TYPE_HEXAHEDRAL = 4; // 8            6            7
    public static final int ELEMENT_TYPE_PYRAMID = 5; // 5            5            5
    public static final int ELEMENT_TYPE_WEDGE = 6; // 6            5            6
    public static final int ELEMENT_TYPE_POLYHEDRAL = 7; // NN           NF          N/A
    
    public static final HashMap<Integer, CellType> mapTypeToVisNow = new HashMap<Integer, CellType>();

    static {
        mapTypeToVisNow.put(ELEMENT_TYPE_TRIANGULAR, CellType.TRIANGLE);
        mapTypeToVisNow.put(ELEMENT_TYPE_TETRAHEDRAL, CellType.TETRA);
        mapTypeToVisNow.put(ELEMENT_TYPE_QUADRILATERAL, CellType.QUAD);
        mapTypeToVisNow.put(ELEMENT_TYPE_HEXAHEDRAL, CellType.HEXAHEDRON);
        mapTypeToVisNow.put(ELEMENT_TYPE_PYRAMID, CellType.PYRAMID);
        mapTypeToVisNow.put(ELEMENT_TYPE_WEDGE, CellType.PRISM);
    }

    private int type = -1;
    private int zone = -1;
    private ArrayList<Integer> faces = new ArrayList<Integer>();
    private int parent;
    private int child;
    private int[] nodes;

    public FluentCell()
    {
        this.parent = 0;
        this.child = 0;
    }

    public FluentCell(int type, int zone, int parent, int child)
    {
        this.type = type;
        this.zone = zone;
        this.parent = parent;
        this.child = child;
    }

    /**
     * @return the type
     */
    public int getType()
    {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type)
    {
        this.type = type;
    }

    /**
     * @return the zone
     */
    public int getZone()
    {
        return zone;
    }

    /**
     * @param zone the zone to set
     */
    public void setZone(int zone)
    {
        this.zone = zone;
    }

    /**
     * @return the faces
     */
    public ArrayList<Integer> getFaces()
    {
        return faces;
    }

    public Integer getFace(int i)
    {
        return faces.get(i);
    }

    public void setFaces(ArrayList<Integer> faces)
    {
        this.faces = faces;
    }

    public void addFace(int f)
    {
        faces.add(f);
    }
    
    /**
     * @return the parent
     */
    public int getParent()
    {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(int parent)
    {
        this.parent = parent;
    }

    /**
     * @return the child
     */
    public int getChild()
    {
        return child;
    }

    /**
     * @param child the child to set
     */
    public void setChild(int child)
    {
        this.child = child;
    }

    /**
     * @return the nodes
     */
    public int[] getNodes()
    {
        return nodes;
    }

    public int getNode(int i)
    {
        return nodes[i];
    }

    public void setNodes(int[] nodes)
    {
        this.nodes = nodes;
    }

    @Override
    public Object clone() throws CloneNotSupportedException
    {
        FluentCell out = (FluentCell) super.clone();
        out.setType(type);
        out.setZone(zone);
        out.setFaces(faces);
        out.setParent(parent);
        out.setChild(child);
        out.setNodes(nodes);
        return out;
    }

}
