/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.InterpolateToMesh;

import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.field.FieldInterpolateToMesh;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class InterpolateToMesh extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    protected Field inField = null;
    protected RegularField inRegularField = null;
    protected Field inMesh = null;
    protected int trueDim;
    protected int nThreads = 1;
    protected int nMeshNodes = 0;
    protected float[] meshCoords;
    protected LogicLargeArray valid = null;
    protected int[] vLens;
    protected String[] names;
    protected DataArray[] inData;
    protected LargeArray[] outData;

    public InterpolateToMesh()
    {
        setPanel(ui);
    }
//
//    private class PartialInterpolation implements Runnable
//    {
//
//        private int iThread;
//        private TetrahedronPosition interp = new TetrahedronPosition();
//
//        public PartialInterpolation(int iThread)
//        {
//            this.iThread = iThread;
//        }
//
//        @Override
//        public void run()
//        {
//            float[] fs = new float[trueDim];
//            int dk = nMeshNodes / nThreads;
//            int kstart = iThread * dk + min(iThread, nMeshNodes % nThreads);
//            int kend = (iThread + 1) * dk + min(iThread + 1, nMeshNodes % nThreads);
//            for (int n = kstart; n < kend; n++) {
//                if (iThread == 0)
//                    setProgress((float) n / nMeshNodes);
//                try {
//                    System.arraycopy(meshCoords, 3 * n, fs, 0, fs.length);
//                    if (inField.getFieldCoords(fs, interp)) {
//                        float[] coeffs = interp.coords;
//                        int[] verts = interp.verts;
//                        for (int k = 0; k < inData.length; k++) {
//                            float[] out = new float[vLens[k]];
//                            for (int i = 0; i < out.length; i++)
//                                out[i] = 0;
//                            for (int i = 0; i < verts.length; i++) {
//                                int iv = verts[i];
//                                float c = coeffs[i];
//                                float[] in = inData[k].getFloatElement(iv);
//                                for (int j = 0; j < out.length; j++)
//                                    out[j] += c * in[j];
//                            }
//                            for (int i = 0; i < out.length; i++)
//                                outData[k].setFloat(out.length * n + i, out[i]);
//                        }
//                        valid.setValueAtIndex(n, true);
//                    } else {
//                        valid.setValueAtIndex(n, false);
//                    }
//
//                } catch (Exception e) {
//                    System.out.println("null at " + n + " from " + nMeshNodes);
//                }
//            }
//        }
//    }
//
//    private void interpolateIrregularFieldToMesh()
//    {
//        if (inField.getGeoTree() == null) {
//            System.out.println("creating cell tree");
//            long start = System.currentTimeMillis();
//            inField.createGeoTree();
//            System.out.println("cell tree created in " + ((float) (System.currentTimeMillis() - start)) / 1000 + "seconds");
//        }
//        int nNumericData = 0;
//        for (int i = 0; i < inField.getNComponents(); i++)
//            if (inField.getComponent(i).isNumeric())
//                nNumericData += 1;
//        inData = new DataArray[nNumericData];
//        outData = new LargeArray[nNumericData];
//        vLens = new int[nNumericData];
//        names = new String[nNumericData];
//        for (int i = 0, j = 0; i < inField.getNComponents(); i++)
//            if (inField.getComponent(i).isNumeric()) {
//                inData[j] = inField.getComponent(i);
//                vLens[j] = inField.getComponent(i).getVectorLength();
//                names[j] = inField.getComponent(i).getName();
//                outData[j] = new FloatLargeArray(vLens[j] * nMeshNodes);
//                j += 1;
//            }
//
//        nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
//        Thread[] workThreads = new Thread[nThreads];
//        for (int iThread = 0; iThread < nThreads; iThread++) {
//            workThreads[iThread] = new Thread(new PartialInterpolation(iThread));
//            workThreads[iThread].start();
//        }
//        for (Thread workThread : workThreads)
//            try {
//                workThread.join();
//            } catch (InterruptedException e) {
//            }
//        outField.setMask(valid);
//        for (int i = 0; i < outData.length; i++)
//            outField.addComponent(DataArray.create(outData[i], vLens[i], names[i]));
//    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null || getInputFirstValue("inMesh") == null)
            return;
        Field field = ((VNField) getInputFirstValue("inField")).getField();
        if (field == null)
            return;
        if (inField != field) {
            inField = field;
            trueDim = inField.getTrueNSpace();
            if (trueDim < 3)
                return;
        }
        Field mesh = ((VNField) getInputFirstValue("inMesh")).getField();
        if (mesh == null || trueDim < mesh.getTrueNSpace())
            return;
        if (inMesh != mesh)
            inMesh = mesh;
        nMeshNodes = (int) inMesh.getNNodes();

//        valid = new LogicLargeArray((int) outField.getNNodes());
        if (inField.hasCoords()) {
            outField = new FieldInterpolateToMesh(inField, inMesh).interpolate();
//            interpolateIrregularFieldToMesh();
        } else {
            outField = inMesh.cloneShallow();
            if (inMesh.hasCoords())
                meshCoords = inMesh.getCurrentCoords().getData();
            else if (inMesh instanceof RegularField)
                meshCoords = ((RegularField) inMesh).getCoordsFromAffine().getData();
            else
            return;
            inRegularField = (RegularField) inField;
            org.visnow.vn.lib.utils.InterpolateToMesh.updateOutField(inRegularField, outField);
        }

        if (outField instanceof RegularField)
            setOutputValue("outRegularField", new VNRegularField((RegularField) outField));

        if (outField != null) {
            if (outField instanceof RegularField) {
                setOutputValue("outRegularField", new VNRegularField((RegularField) outField));
                setOutputValue("outIrregularField", null);
            } else {
                setOutputValue("outRegularField", null);
                setOutputValue("outIrregularField", new VNIrregularField((IrregularField) outField));
            }
        } else {
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", null);
        }
        prepareOutputGeometry();
        show();
    }
}
