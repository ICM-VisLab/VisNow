/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

 package org.visnow.vn.lib.basic.readers.ReadCDM4;

import ch.systemsx.cisd.base.mdarray.MDAbstractArray;
import ch.systemsx.cisd.base.mdarray.MDArray;
import ch.systemsx.cisd.base.mdarray.MDByteArray;
import ch.systemsx.cisd.base.mdarray.MDDoubleArray;
import ch.systemsx.cisd.base.mdarray.MDFloatArray;
import ch.systemsx.cisd.base.mdarray.MDIntArray;
import ch.systemsx.cisd.base.mdarray.MDLongArray;
import ch.systemsx.cisd.base.mdarray.MDShortArray;
import ch.systemsx.cisd.hdf5.HDF5DataClass;
import ch.systemsx.cisd.hdf5.HDF5DataSetInformation;
import ch.systemsx.cisd.hdf5.HDF5DataTypeInformation;
import ch.systemsx.cisd.hdf5.HDF5Factory;
import ch.systemsx.cisd.hdf5.IHDF5ObjectReadOnlyInfoProviderHandler;
import ch.systemsx.cisd.hdf5.IHDF5Reader;
import ch.systemsx.cisd.hdf5.UnsignedIntUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.log4j.Logger;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.utils.UnitUtils;
import org.visnow.vn.engine.core.ProgressAgent;
import org.visnow.vn.lib.basic.writers.HDF5Writer.ComplexNumber;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;
import ucar.units.Unit;

/**
 * Static methods for reading HDF5 files.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class ReadHDF5Core
{

    private static final Logger LOGGER = Logger.getLogger(ReadHDF5Core.class);

    /**
     * Reverses the order of elements in an array.
     *
     * @param v       input / output array
     * @param inPlace if true, then the operation is performed in-place
     *
     * @return array with reversed elements
     */
    public static long[] vectorReverse(long[] v, boolean inPlace)
    {
        if (inPlace) {
            int end = v.length - 1;
            for (int i = 0; i < v.length / 2; i++) {
                long t = v[i];
                v[i] = v[end];
                v[end] = t;
                end--;
            }
            return v;
        } else {
            long[] reversed = new long[v.length];
            int pos = v.length - 1;
            for (int i = 0; i < v.length; i++)
                reversed[i] = v[pos--];
            return reversed;
        }
    }

    /**
     * Returns true if a given file is in HDF5 format and stores VisNow field, false otherwise
     *
     * @param filename file path
     *
     * @return true if a given file is in HDF5 format and stores VisNow field, false otherwise
     */
    public static boolean isVisNowField(String filename)
    {
        if (filename == null) {
            return false;
        }

        try (IHDF5Reader reader = HDF5Factory.openForReading(filename)) {
            if (reader.object().isGroup("visnow")) {
                return true;
            } else {
                return false;
            }
        }
        catch(Exception ex) {
            return false;
        }
    }

    /**
     * Returns the names of VisNow components for a given field.
     *
     * @param filename  file path to HDF5 file
     * @param fieldName name of VisNow field stored in HDF5 file
     * @param strict    if true, then strict names (full HDF5 hierarchy) are returned
     *
     * @return names of VisNow components
     */
    public static String[] getVisNowComponentNames(String filename, String fieldName, boolean strict)
    {
        String err = "Could not read file " + filename;
        if (filename == null || fieldName == null) {
            LOGGER.error(err);
            VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
            return null;
        }
        ArrayList<String> components = new ArrayList<>();
        try (IHDF5Reader reader = HDF5Factory.openForReading(filename)) {
            if (!reader.object().isGroup(fieldName)) {
                err = "Could not find group " + fieldName + " in file " + filename;
                LOGGER.error(err);
                VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                return null;
            }
            List<String> members = reader.object().getAllGroupMembers(fieldName);
            for (String member : members) {
                String strictName = fieldName + "/" + member;
                if (reader.object().isGroup(strictName)) {
                    if (!member.equals("vnfield_geometry")) {
                        if (strict) {
                            components.add(strictName);
                        } else {
                            components.add(member);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.error(err);
            VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
            return null;
        }
        return components.toArray(new String[0]);
    }

    /**
     * Returns the names of VisNow fields stored in a given HDF5 file.
     *
     * @param filename   file path to HDF5 file
     * @param strict     if true, then strict names (full HDF5 hierarchy) are returned
     * @param dimensions if true, then diemnsions string is added to the name of each field
     *
     * @return
     */
    public static String[] getVisNowFieldNamesAndDimensions(String filename, boolean strict, boolean dimensions)
    {
        String err = "Could not read file " + filename;
        if (filename == null) {
            LOGGER.error(err);
            VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
            return null;
        }
        ArrayList<String> fieldsAndDimensions = new ArrayList<>();
        try (IHDF5Reader reader = HDF5Factory.openForReading(filename)) {
            if (reader.object().isGroup("visnow")) {
                List<String> members = reader.object().getAllGroupMembers("visnow");
                for (String member : members) {
                    String strictName = "visnow/" + member;
                    if (reader.object().isGroup(strictName)) {
                        long[] dims = getHDF5Dims(reader, "visnow/" + member);
                        if (dims == null) {
                            LOGGER.error(err);
                            VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                            return null;
                        }
                        if (strict && dimensions) {
                            fieldsAndDimensions.add(strictName + " " + toDimensionsString(dims));
                        } else if (!strict && dimensions) {
                            fieldsAndDimensions.add(member + " " + toDimensionsString(dims));
                        } else if (strict && !dimensions) {
                            fieldsAndDimensions.add(strictName);
                        } else {
                            fieldsAndDimensions.add(member);
                        }
                    }
                }
            } else {
                err = "Could not find group \"visnow\" in file " + filename;
                LOGGER.error(err);
                VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                return null;
            }
        } catch (Exception ex) {
            LOGGER.error(err);
            VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
            return null;
        }
        return fieldsAndDimensions.toArray(new String[0]);
    }

    /**
     * Reads data from an HDF5 file and stores them in a Field.
     *
     * @param filename             file path to HDF5 file
     * @param visNowFieldName      VisNow field name
     * @param visNowComponentNames VisNow component names
     * @param progressAgent        progress monitor
     *
     * @return a new Field or null if an error occurred
     */
    public static Field readVisNowField(String filename, String visNowFieldName, String[] visNowComponentNames, ProgressAgent progressAgent)
    {
        if (filename == null || visNowFieldName == null || visNowFieldName.equals("") || visNowComponentNames == null || visNowComponentNames.length == 0) {
            return null;
        }
        try (IHDF5Reader reader = HDF5Factory.openForReading(filename)) {
            ImmutablePair<Boolean, Integer> pair;
            long[] hdf5Dims;
            long[] dims;
            long[] fieldDims;
            int veclen;
            RegularField field;
            ArrayList<LargeArray> dataseries;
            ArrayList<Float> timeseries = null;
            String unit = "1";
            String timeUnit = "1";
            String[] userData = null;
            double[] preferredRange = null;
            double[] preferredPhysicalRange = null;
            float progressStep = 0.8f / visNowComponentNames.length;
            int i;
            boolean firstDimensionIsTime;
            HDF5DataTypeInformation attrInfo;
            HDF5DataSetInformation datasetInfo;
            IHDF5ObjectReadOnlyInfoProviderHandler objectReader = reader.object();

            if (!objectReader.isGroup(visNowFieldName)) {
                String err = "Could not find group \"" + visNowFieldName + "\".";
                LOGGER.error(err);
                VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                return null;
            }
            dims = getHDF5Dims(reader, visNowFieldName);
            if (dims == null) {
                String err = "Could not fild attribute \"dimensions\" in \"" + visNowFieldName + "\" group or that attribute has invalid properties.";
                LOGGER.error(err);
                VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                return null;
            }
            fieldDims = vectorReverse(dims, false);

            if (objectReader.hasAttribute(visNowFieldName, "time_unit")) {
                attrInfo = objectReader.getAttributeInformation(visNowFieldName, "time_unit");
                if (attrInfo.getDataClass() == HDF5DataClass.STRING) {
                    timeUnit = reader.string().getAttr(visNowFieldName, "time_unit");
                }
            }

            field = new RegularField(fieldDims);

            // Read components
            for (i = 0; i < visNowComponentNames.length; ++i) {

                if (!objectReader.isGroup(visNowComponentNames[i])) {
                    String err = "Could not find group \"" + visNowComponentNames[i] + "\".";
                    LOGGER.error(err);
                    VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                    return null;
                }

                if (!objectReader.isDataSet(visNowComponentNames[i] + "/dataseries")) {
                    String err = "Could not find variable \"" + visNowComponentNames[i] + "/dataseries\".";
                    LOGGER.error(err);
                    VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                    return null;
                }
                datasetInfo = objectReader.getDataSetInformation(visNowComponentNames[i] + "/dataseries");

                hdf5Dims = datasetInfo.getDimensions();
                pair = extractTimeAndVeclen(hdf5Dims, dims);
                firstDimensionIsTime = pair.getKey();
                veclen = pair.getValue();

                if (firstDimensionIsTime) {
                    if (!objectReader.isDataSet(visNowComponentNames[i] + "/timeseries")) {
                        String err = "Could not find variable \"" + visNowComponentNames[i] + "/timeseries\".";
                        LOGGER.error(err);
                        VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                        return null;
                    }
                }

                dataseries = readDataset(reader, visNowComponentNames[i] + "/dataseries", datasetInfo, fieldDims, veclen, firstDimensionIsTime, progressAgent, i * progressStep, progressStep);
                if (dataseries == null || dataseries.isEmpty()) {
                    String err = "Could not read dataset \"" + visNowComponentNames[i] + "/dataseries\".";
                    LOGGER.error(err);
                    VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                    return null;
                }

                if (firstDimensionIsTime) {
                    timeseries = readTimeSeries(reader, visNowComponentNames[i] + "/timeseries");
                    if (timeseries == null || timeseries.isEmpty()) {
                        String err = "Could not read dataset \"" + visNowComponentNames[i] + "/timeseries\".";
                        LOGGER.error(err);
                        VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                        return null;
                    }
                }

                // Read attributes
                if (objectReader.hasAttribute(visNowComponentNames[i], "unit")) {
                    attrInfo = objectReader.getAttributeInformation(visNowComponentNames[i], "unit");
                    if (attrInfo.getDataClass() == HDF5DataClass.STRING) {
                        try {
                            Unit u = UnitUtils.getUnit(reader.string().getAttr(visNowComponentNames[i], "unit"));
                            unit = u.toString();
                        } catch (Exception e) {
                            unit = "1";
                        }
                    }
                } else {
                    unit = "1";
                }
                if (objectReader.hasAttribute(visNowComponentNames[i], "preferred_range")) {
                    attrInfo = objectReader.getAttributeInformation(visNowComponentNames[i], "preferred_range");
                    if (attrInfo.getRank() == 1 && attrInfo.getNumberOfElements() == 2 && attrInfo.getDataClass() == HDF5DataClass.FLOAT && attrInfo.getElementSize() == 8) {
                        preferredRange = reader.float64().getArrayAttr(visNowComponentNames[i], "preferred_range");
                    }
                } else {
                    preferredRange = null;
                }
                if (objectReader.hasAttribute(visNowComponentNames[i], "preferred_physical_range")) {
                    attrInfo = objectReader.getAttributeInformation(visNowComponentNames[i], "preferred_physical_range");
                    if (attrInfo.getRank() == 1 && attrInfo.getNumberOfElements() == 2 && attrInfo.getDataClass() == HDF5DataClass.FLOAT && attrInfo.getElementSize() == 8) {
                        preferredPhysicalRange = reader.float64().getArrayAttr(visNowComponentNames[i], "preferred_physical_range");
                    }
                } else {
                    preferredPhysicalRange = null;
                }
                if (objectReader.hasAttribute(visNowComponentNames[i], "user_data")) {
                    attrInfo = objectReader.getAttributeInformation(visNowComponentNames[i], "user_data");
                    if (attrInfo.getRank() == 1 && attrInfo.getDataClass() == HDF5DataClass.STRING) {
                        userData = reader.string().getArrayAttr(visNowComponentNames[i], "user_data");
                    }
                } else {
                    userData = null;
                }

                if (firstDimensionIsTime) {
                    if (!timeUnit.equals("1")) {
                        field.setTimeUnit(timeUnit);
                    }
                    TimeData td = new TimeData(timeseries, dataseries, timeseries.get(0));
                    DataArray da = DataArray.create(td, veclen, extractName(visNowComponentNames[i]), unit, userData);
                    setPreferedRanges(da, preferredRange, preferredPhysicalRange);
                    field.addComponent(da);
                } else {
                    DataArray da = DataArray.create(dataseries.get(0), veclen, extractName(visNowComponentNames[i]), unit, userData);
                    setPreferedRanges(da, preferredRange, preferredPhysicalRange);
                    field.addComponent(da);
                }
            }

            // Read geometry
            if (objectReader.isGroup(visNowFieldName + "/vnfield_geometry")) {
                if (!objectReader.isDataSet(visNowFieldName + "/vnfield_geometry/affine")) {
                    if (objectReader.isGroup(visNowFieldName + "/vnfield_geometry/coordinates")) {
                        //Read coordinates
                        if (!objectReader.isDataSet(visNowFieldName + "/vnfield_geometry/coordinates/dataseries")) {
                            String err = "Could not find dataset \"" + visNowFieldName + "/vnfield_geometry/coordinates/dataseries\".";
                            LOGGER.error(err);
                            VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                            return null;
                        }
                        datasetInfo = objectReader.getDataSetInformation(visNowFieldName + "/vnfield_geometry/coordinates/dataseries");
                        hdf5Dims = datasetInfo.getDimensions();
                        pair = extractTimeAndVeclen(hdf5Dims, dims);
                        firstDimensionIsTime = pair.getKey();
                        veclen = pair.getValue();
                        dataseries = readDataset(reader, visNowFieldName + "/vnfield_geometry/coordinates/dataseries", datasetInfo, fieldDims, veclen, firstDimensionIsTime, progressAgent, i * progressStep, progressStep);
                        if (dataseries == null || dataseries.isEmpty()) {
                            String err = "Could not read dataset \"" + visNowFieldName + "/vnfield_geometry/coordinates/dataseries\".";
                            LOGGER.error(err);
                            VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                            return null;
                        }
                        if (firstDimensionIsTime) {
                            if (!objectReader.isDataSet(visNowFieldName + "/vnfield_geometry/coordinates/timeseries")) {
                                String err = "Could not find dataset \"" + visNowFieldName + "/vnfield_geometry/coordinates/timeseries\".";
                                LOGGER.error(err);
                                VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                                return null;
                            }
                            timeseries = readTimeSeries(reader, visNowFieldName + "/vnfield_geometry/coordinates/timeseries");
                            if (timeseries == null || timeseries.isEmpty()) {
                                String err = "Could not read dataset \"" + visNowFieldName + "/vnfield_geometry/coordinates/timeseries\".";
                                LOGGER.error(err);
                                VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                                return null;
                            }
                            TimeData coords = new TimeData(timeseries, dataseries, timeseries.get(0));
                            field.setCoords(coords);

                        } else {
                            field.setCurrentCoords((FloatLargeArray) dataseries.get(0));
                        }
                    }
                } else {
                    //Read affine
                    float[][] affine = readFloatMatrix(reader, visNowFieldName + "/vnfield_geometry/affine");
                    if (affine != null && affine.length == 4 && affine[0].length == 3) {
                        field.setAffine(affine);
                    }
                }

                //Read extents
                if (objectReader.isDataSet(visNowFieldName + "/vnfield_geometry/extents")) {
                    float[][] extents = readFloatMatrix(reader, visNowFieldName + "/vnfield_geometry/extents");
                    if (extents != null && extents.length == 3 && extents[0].length == 2) {
                        field.getSchema().setExtents(extents);
                    }
                }

                //Read preferred_extents and preferred_physical_extents
                if (objectReader.isDataSet(visNowFieldName + "/vnfield_geometry/preferred_extents")) {
                    float[][] preferred_extents = readFloatMatrix(reader, visNowFieldName + "/vnfield_geometry/preferred_extents");
                    if (preferred_extents != null && preferred_extents.length == 3 && preferred_extents[0].length == 2) {
                        if (objectReader.isDataSet(visNowFieldName + "/vnfield_geometry/preferred_physical_extents")) {
                            float[][] preferred_physical_extents = readFloatMatrix(reader, visNowFieldName + "/vnfield_geometry/preferred_physical_extents");
                            if (preferred_physical_extents != null && preferred_physical_extents.length == 3 && preferred_physical_extents[0].length == 2) {
                                field.setPreferredExtents(preferred_extents, preferred_physical_extents);
                            }
                        } else {
                            field.setPreferredExtents(preferred_extents);
                        }

                    }
                }

                //Read coords_units
                if (objectReader.hasAttribute(visNowFieldName + "/vnfield_geometry", "coords_units")) {
                    attrInfo = objectReader.getAttributeInformation(visNowFieldName + "/vnfield_geometry", "coords_units");
                    if (attrInfo.getRank() == 1 && attrInfo.getDataClass() == HDF5DataClass.STRING) {
                        String[] coordsUnits = reader.string().getArrayAttr(visNowFieldName + "/vnfield_geometry", "coords_units");
                        if (coordsUnits != null && coordsUnits.length == 3) {
                            for (int j = 0; j < 3; j++) {
                                try {
                                    Unit u = UnitUtils.getUnit(coordsUnits[j]);
                                    coordsUnits[j] = u.toString();
                                } catch (Exception e) {
                                    coordsUnits[j] = "1";
                                }
                            }
                            field.setCoordsUnits(coordsUnits);
                        }
                    }
                }
            }

            // Read mask
            if (objectReader.isGroup(visNowFieldName + "/vnfield_mask")) {
                if (!objectReader.isDataSet(visNowFieldName + "/vnfield_mask/dataseries")) {
                    String err = "Could not find dataset \"" + visNowFieldName + "/vnfield_mask/dataseries\".";
                    LOGGER.error(err);
                    VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                    return null;
                }
                datasetInfo = objectReader.getDataSetInformation(visNowFieldName + "/vnfield_mask/dataseries");
                hdf5Dims = datasetInfo.getDimensions();
                pair = extractTimeAndVeclen(hdf5Dims, dims);
                firstDimensionIsTime = pair.getKey();
                veclen = pair.getValue();
                dataseries = readDataset(reader, visNowFieldName + "/vnfield_mask/dataseries", datasetInfo, fieldDims, veclen, firstDimensionIsTime, progressAgent, i * progressStep, progressStep);
                if (dataseries == null || dataseries.isEmpty()) {
                    String err = "Could not read dataset \"" + visNowFieldName + "/vnfield_mask/dataseries\".";
                    LOGGER.error(err);
                    VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                    return null;
                }
                if (firstDimensionIsTime) {
                    if (!objectReader.isDataSet(visNowFieldName + "/vnfield_mask/timeseries")) {
                        String err = "Could not find dataset \"" + visNowFieldName + "/vnfield_mask/timeseries\".";
                        LOGGER.error(err);
                        VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                        return null;
                    }
                    timeseries = readTimeSeries(reader, visNowFieldName + "/vnfield_mask/timeseries");
                    if (timeseries == null || timeseries.isEmpty()) {
                        String err = "Could not read dataset \"" + visNowFieldName + "/vnfield_mask/timeseries\".";
                        LOGGER.error(err);
                        VisNow.get().userMessageSend(null, "Error reading file", err, Level.ERROR);
                        return null;
                    }
                    TimeData mask = new TimeData(timeseries, dataseries, timeseries.get(0));
                    field.setMask(mask);
                } else {
                    field.setCurrentMask((LogicLargeArray) dataseries.get(0));
                }
            }
            return field;
        }
    }

    /**
     * Returns a pair [firstDimensionIsTime, veclen] for the given HDF5 dimensions and VisNow field dimensions
     *
     * @param hdf5Dims  HDF5 dimensions
     * @param fieldDims VisNow field dimensios
     *
     * @return pair [firstDimensionIsTime, veclen]
     */
    private static ImmutablePair<Boolean, Integer> extractTimeAndVeclen(long[] hdf5Dims, long[] fieldDims)
    {
        boolean firstDimensionIsTime = false;
        int veclen = 1;
        long firstDim = hdf5Dims[0];
        long lastDim = hdf5Dims[hdf5Dims.length - 1];

        if (hdf5Dims.length != fieldDims.length) {
            if (firstDim != fieldDims[0]) {
                firstDimensionIsTime = true;
            }
            if (lastDim != fieldDims[fieldDims.length - 1]) {
                veclen = (int) lastDim;
            }
        }
        return new ImmutablePair<>(firstDimensionIsTime, veclen);
    }

    /**
     * Extracts name from a strict name (full HDF5 hierarchy).
     *
     * @param strictName strict name (full HDF5 hierarchy)
     *
     * @return name
     */
    private static String extractName(String strictName)
    {
        String[] tmp = strictName.split("/");
        return tmp[tmp.length - 1];
    }

    /**
     * Converts given array to a string representation.
     *
     * @param dims array
     *
     * @return string representation of array
     */
    private static String toDimensionsString(long[] dims)
    {
        StringBuilder res = new StringBuilder("(");
        for (int i = 0; i < dims.length; i++) {
            res.append(Long.toString(dims[i]));
            if (i < dims.length - 1) {
                res.append(",");
            }
        }
        res.append(")");
        return res.toString();
    }

    /**
     * Converts HDF5 dataset type to LargeArrayType
     *
     * @param datasetType HDF5 dataset type
     *
     * @return LargeArray type
     */
    private static LargeArrayType datasetTypeToLargeArrayType(HDF5DataTypeInformation datasetType)
    {
        switch (datasetType.getDataClass()) {
            case INTEGER:
                switch (datasetType.getElementSize()) {
                    case 1:
                        if (datasetType.isSigned()) {
                            return LargeArrayType.LOGIC;
                        } else {
                            return LargeArrayType.UNSIGNED_BYTE;
                        }
                    case 2:
                        if (datasetType.isSigned()) {
                            return LargeArrayType.SHORT;
                        } else {
                            return LargeArrayType.INT;
                        }
                    case 4:
                        if (datasetType.isSigned()) {
                            return LargeArrayType.INT;
                        } else {
                            return LargeArrayType.LONG;
                        }
                    case 8:
                        if (datasetType.isSigned()) {
                            return LargeArrayType.LONG;
                        } else {
                            return null;
                        }
                    default:
                        return null;
                }
            case FLOAT:
                switch (datasetType.getElementSize()) {
                    case 4:
                        return LargeArrayType.FLOAT;
                    case 8:
                        return LargeArrayType.DOUBLE;
                    default:
                        return null;
                }
            case STRING:
                return LargeArrayType.STRING;
            case COMPOUND:
                return LargeArrayType.COMPLEX_FLOAT;
            default:
                return null;

        }
    }

    /**
     * Fills LogicLargeArray with data stored in HDF5 dataset.
     *
     * @param typeInfo HDF5 dataset type
     * @param iter     HDF5 data iterator
     * @param array    output array
     *
     * @return true if successful, false otherwise
     */
    private static boolean fillLogicLargeArray(HDF5DataTypeInformation typeInfo, Iterator<MDAbstractArray<Byte>.ArrayEntry> iter, LargeArray array)
    {
        if (array == null || array.getType() != LargeArrayType.LOGIC ||
            typeInfo == null || typeInfo.getDataClass() != HDF5DataClass.INTEGER || typeInfo.getElementSize() != 1 || !typeInfo.isSigned()) {
            return false;
        }
        long nelements = array.length();
        for (long i = 0; i < nelements; i++) {
            array.setBoolean(i, iter.next().getValue() != 0);
        }
        return true;
    }

    /**
     * Fills UnsignedByteLargeArray with data stored in HDF5 dataset.
     *
     * @param typeInfo HDF5 dataset type
     * @param iter     HDF5 data iterator
     * @param array    output array
     *
     * @return true if successful, false otherwise
     */
    private static boolean fillUnsignedByteLargeArray(HDF5DataTypeInformation typeInfo, Iterator<MDAbstractArray<Byte>.ArrayEntry> iter, LargeArray array)
    {
        if (array == null || array.getType() != LargeArrayType.UNSIGNED_BYTE ||
            typeInfo == null || typeInfo.getDataClass() != HDF5DataClass.INTEGER || typeInfo.getElementSize() != 1 || typeInfo.isSigned()) {
            return false;
        }
        long nelements = array.length();
        for (long i = 0; i < nelements; i++) {
            array.setUnsignedByte(i, UnsignedIntUtils.toUint8(iter.next().getValue()));
        }
        return true;
    }

    /**
     * Fills ShortLargeArray with data stored in HDF5 dataset.
     *
     * @param typeInfo HDF5 dataset type
     * @param iter     HDF5 data iterator
     * @param array    output array
     *
     * @return true if successful, false otherwise
     */
    private static boolean fillShortLargeArray(HDF5DataTypeInformation typeInfo, Iterator<MDAbstractArray<Short>.ArrayEntry> iter, LargeArray array)
    {
        if (array == null || array.getType() != LargeArrayType.SHORT ||
            typeInfo == null || typeInfo.getDataClass() != HDF5DataClass.INTEGER || typeInfo.getElementSize() != 2 || !typeInfo.isSigned()) {
            return false;
        }
        long nelements = array.length();
        for (long i = 0; i < nelements; i++) {
            array.setShort(i, iter.next().getValue());
        }
        return true;
    }

    /**
     * Fills IntLargeArray with data stored in HDF5 dataset of type uint16.
     *
     * @param typeInfo HDF5 dataset type
     * @param iter     HDF5 data iterator
     * @param array    output array
     *
     * @return true if successful, false otherwise
     */
    private static boolean fillIntLargeArrayUnit16(HDF5DataTypeInformation typeInfo, Iterator<MDAbstractArray<Short>.ArrayEntry> iter, LargeArray array)
    {
        if (array == null || array.getType() != LargeArrayType.INT ||
            typeInfo.getDataClass() != HDF5DataClass.INTEGER || typeInfo.getElementSize() != 2 || typeInfo.isSigned()) {
            return false;
        }
        long nelements = array.length();
        for (long i = 0; i < nelements; i++) {
            array.setInt(i, UnsignedIntUtils.toUint16(iter.next().getValue()));
        }
        return true;
    }

    /**
     * Fills IntLargeArray with data stored in HDF5 dataset of type int32.
     *
     * @param typeInfo HDF5 dataset type
     * @param iter     HDF5 data iterator
     * @param array    output array
     *
     * @return true if successful, false otherwise
     */
    private static boolean fillIntLargeArrayInt32(HDF5DataTypeInformation typeInfo, Iterator<MDAbstractArray<Integer>.ArrayEntry> iter, LargeArray array)
    {
        if (array == null || array.getType() != LargeArrayType.INT ||
            typeInfo.getDataClass() != HDF5DataClass.INTEGER || typeInfo.getElementSize() != 4 || !typeInfo.isSigned()) {
            return false;
        }
        long nelements = array.length();
        for (long i = 0; i < nelements; i++) {
            array.setInt(i, iter.next().getValue());
        }
        return true;
    }

    /**
     * Fills LongLargeArray with data stored in HDF5 dataset of type uint32.
     *
     * @param typeInfo HDF5 dataset type
     * @param iter     HDF5 data iterator
     * @param array    output array
     *
     * @return true if successful, false otherwise
     */
    private static boolean fillLongLargeArrayUnit32(HDF5DataTypeInformation typeInfo, Iterator<MDAbstractArray<Integer>.ArrayEntry> iter, LargeArray array)
    {
        if (array == null || array.getType() != LargeArrayType.LONG ||
            typeInfo.getDataClass() != HDF5DataClass.INTEGER || typeInfo.getElementSize() != 4 || typeInfo.isSigned()) {
            return false;
        }

        long nelements = array.length();
        for (long i = 0; i < nelements; i++) {
            array.setLong(i, UnsignedIntUtils.toUint32(iter.next().getValue()));
        }
        return true;
    }

    /**
     * Fills LongLargeArray with data stored in HDF5 dataset.
     *
     * @param typeInfo HDF5 dataset type
     * @param iter     HDF5 data iterator
     * @param array    output array
     *
     * @return true if successful, false otherwise
     */
    private static boolean fillLongLargeArray(HDF5DataTypeInformation typeInfo, Iterator<MDAbstractArray<Long>.ArrayEntry> iter, LargeArray array)
    {
        if (array == null || array.getType() != LargeArrayType.LONG ||
            typeInfo.getDataClass() != HDF5DataClass.INTEGER || typeInfo.getElementSize() != 8 || !typeInfo.isSigned()) {
            return false;
        }
        long nelements = array.length();
        for (long i = 0; i < nelements; i++) {
            array.setLong(i, iter.next().getValue());
        }
        return true;
    }

    /**
     * Fills FloatLargeArray with data stored in HDF5 dataset.
     *
     * @param typeInfo HDF5 dataset type
     * @param iter     HDF5 data iterator
     * @param array    output array
     *
     * @return true if successful, false otherwise
     */
    private static boolean fillFloatLargeArray(HDF5DataTypeInformation typeInfo, Iterator<MDAbstractArray<Float>.ArrayEntry> iter, LargeArray array)
    {
        if (array == null || array.getType() != LargeArrayType.FLOAT ||
            typeInfo == null || typeInfo.getDataClass() != HDF5DataClass.FLOAT || typeInfo.getElementSize() != 4) {
            return false;
        }
        long nelements = array.length();
        for (long i = 0; i < nelements; i++) {
            array.setFloat(i, iter.next().getValue());
        }
        return true;
    }

    /**
     * Fills FloatDoubleArray with data stored in HDF5 dataset.
     *
     * @param typeInfo HDF5 dataset type
     * @param iter     HDF5 data iterator
     * @param array    output array
     *
     * @return true if successful, false otherwise
     */
    private static boolean fillDoubleLargeArray(HDF5DataTypeInformation typeInfo, Iterator<MDAbstractArray<Double>.ArrayEntry> iter, LargeArray array)
    {
        if (array == null || array.getType() != LargeArrayType.DOUBLE ||
            typeInfo == null || typeInfo.getDataClass() != HDF5DataClass.FLOAT || typeInfo.getElementSize() != 8) {
            return false;
        }
        long nelements = array.length();
        for (long i = 0; i < nelements; i++) {
            array.setDouble(i, iter.next().getValue());
        }
        return true;
    }

    /**
     * Fills StringLargeArray with data stored in HDF5 dataset.
     *
     * @param typeInfo HDF5 dataset type
     * @param iter     HDF5 data iterator
     * @param array    output array
     *
     * @return true if successful, false otherwise
     */
    private static boolean fillStringLargeArray(HDF5DataTypeInformation typeInfo, Iterator<MDAbstractArray<String>.ArrayEntry> iter, LargeArray array)
    {
        if (array == null || array.getType() != LargeArrayType.STRING ||
            typeInfo == null || typeInfo.getDataClass() != HDF5DataClass.STRING) {
            return false;
        }
        long nelements = array.length();
        for (long i = 0; i < nelements; i++) {
            array.set(i, iter.next().getValue());
        }
        return true;
    }

    /**
     * Fills ComplexFloatLargeArray with data stored in HDF5 dataset.
     *
     * @param typeInfo HDF5 dataset type
     * @param iter     HDF5 data iterator
     * @param array    output array
     *
     * @return true if successful, false otherwise
     */
    private static boolean fillComplexLargeArray(HDF5DataTypeInformation typeInfo, Iterator<MDAbstractArray<ComplexNumber>.ArrayEntry> iter, LargeArray array)
    {
        if (array == null || array.getType() != LargeArrayType.COMPLEX_FLOAT ||
            typeInfo == null || typeInfo.getDataClass() != HDF5DataClass.COMPOUND) {
            return false;
        }
        long nelements = array.length();
        ComplexFloatLargeArray carray = (ComplexFloatLargeArray) array;
        for (long i = 0; i < nelements; i++) {
            carray.setComplexFloat(i, iter.next().getValue().get());
        }
        return true;
    }

    /**
     * Reads HDF5 dataset and stores the data in the list of LargeArrays.
     *
     * @param reader          HDF5 reader
     * @param datasetName     name of HDF5 dataset to read
     * @param datasetInfo     HDF5 dataset information
     * @param fieldDims       VisNow field dimensions
     * @param veclen          VisNow component vector length
     * @param isTimeDependent if true, then the first dimension of HDF5 dataset is treated as VisNow time dimension
     * @param progressAgent   progress monitor
     * @param currentProgress current progress of the progress monitor
     * @param progressStep    progress step used in the progress monitor
     *
     * @return list of LargeArrays
     */
    private static ArrayList<LargeArray> readDataset(IHDF5Reader reader, String datasetName, HDF5DataSetInformation datasetInfo, long[] fieldDims, int veclen, boolean isTimeDependent, ProgressAgent progressAgent, float currentProgress, float progressStep)
    {
        HDF5DataTypeInformation typeInfo = datasetInfo.getTypeInformation();
        LargeArrayType arrayType = datasetTypeToLargeArrayType(typeInfo);
        if (arrayType == null) {
            return null;
        }
        long nelements = veclen;
        int ntimesteps = 1;
        for (int i = 0; i < fieldDims.length; i++) {
            nelements *= (long) fieldDims[i];
        }
        if (isTimeDependent) {
            ntimesteps = (int) (datasetInfo.getNumberOfElements() / nelements);
        }
        ArrayList<LargeArray> list = new ArrayList<>(ntimesteps);
        Iterator<MDAbstractArray<Byte>.ArrayEntry> byteIter = null;
        Iterator<MDAbstractArray<Short>.ArrayEntry> shortIter = null;
        Iterator<MDAbstractArray<Integer>.ArrayEntry> intIter = null;
        Iterator<MDAbstractArray<Long>.ArrayEntry> longIter = null;
        Iterator<MDAbstractArray<Float>.ArrayEntry> floatIter = null;
        Iterator<MDAbstractArray<Double>.ArrayEntry> doubleIter = null;
        Iterator<MDAbstractArray<String>.ArrayEntry> stringIter = null;
        Iterator<MDAbstractArray<ComplexNumber>.ArrayEntry> complexIter = null;

        for (int t = 0; t < ntimesteps; t++) {
            LargeArray la = LargeArrayUtils.create(arrayType, nelements);
            switch (typeInfo.getDataClass()) {
                case INTEGER: {
                    switch (typeInfo.getElementSize()) {
                        case 1:
                            if (typeInfo.isSigned()) {
                                if (t == 0) {
                                    MDByteArray mdarray = reader.int8().readMDArray(datasetName);
                                    if (mdarray == null) {
                                        return null;
                                    }
                                    byteIter = mdarray.iterator();
                                    if (byteIter == null) {
                                        return null;
                                    }
                                }
                                if (!fillLogicLargeArray(typeInfo, byteIter, la)) {
                                    return null;
                                }
                            } else {
                                if (t == 0) {
                                    MDByteArray mdarray = reader.uint8().readMDArray(datasetName);
                                    if (mdarray == null) {
                                        return null;
                                    }
                                    byteIter = mdarray.iterator();
                                    if (byteIter == null) {
                                        return null;
                                    }
                                }
                                if (!fillUnsignedByteLargeArray(typeInfo, byteIter, la)) {
                                    return null;
                                }
                            }
                            break;
                        case 2:
                            if (typeInfo.isSigned()) {
                                if (t == 0) {
                                    MDShortArray mdarray = reader.int16().readMDArray(datasetName);
                                    if (mdarray == null) {
                                        return null;
                                    }
                                    shortIter = mdarray.iterator();
                                    if (shortIter == null) {
                                        return null;
                                    }
                                }
                                if (!fillShortLargeArray(typeInfo, shortIter, la)) {
                                    return null;
                                }
                            } else {
                                if (t == 0) {
                                    MDShortArray mdarray = reader.uint16().readMDArray(datasetName);
                                    if (mdarray == null) {
                                        return null;
                                    }
                                    shortIter = mdarray.iterator();
                                    if (shortIter == null) {
                                        return null;
                                    }
                                }
                                if (!fillIntLargeArrayUnit16(typeInfo, shortIter, la)) {
                                    return null;
                                }
                            }
                            break;
                        case 4:
                            if (typeInfo.isSigned()) {
                                if (t == 0) {
                                    MDIntArray mdarray = reader.int32().readMDArray(datasetName);
                                    if (mdarray == null) {
                                        return null;
                                    }
                                    intIter = mdarray.iterator();
                                    if (intIter == null) {
                                        return null;
                                    }
                                }
                                if (!fillIntLargeArrayInt32(typeInfo, intIter, la)) {
                                    return null;
                                }
                            } else {
                                if (t == 0) {
                                    MDIntArray mdarray = reader.uint32().readMDArray(datasetName);
                                    if (mdarray == null) {
                                        return null;
                                    }
                                    intIter = mdarray.iterator();
                                    if (intIter == null) {
                                        return null;
                                    }
                                }
                                if (!fillLongLargeArrayUnit32(typeInfo, intIter, la)) {
                                    return null;
                                }
                            }
                            break;
                        case 8:
                            if (typeInfo.isSigned()) {
                                if (t == 0) {
                                    MDLongArray mdarray = reader.int64().readMDArray(datasetName);
                                    if (mdarray == null) {
                                        return null;
                                    }
                                    longIter = mdarray.iterator();
                                    if (longIter == null) {
                                        return null;
                                    }
                                }
                                if (!fillLongLargeArray(typeInfo, longIter, la)) {
                                    return null;
                                }
                            } else {
                                return null;
                            }
                            break;
                        default:
                            return null;
                    }
                }
                break;
                case FLOAT: {
                    switch (typeInfo.getElementSize()) {
                        case 4: {
                            if (t == 0) {
                                MDFloatArray mdarray = reader.float32().readMDArray(datasetName);
                                if (mdarray == null) {
                                    return null;
                                }
                                floatIter = mdarray.iterator();
                                if (floatIter == null) {
                                    return null;
                                }
                            }
                            if (!fillFloatLargeArray(typeInfo, floatIter, la)) {
                                return null;
                            }
                        }
                        break;
                        case 8: {
                            if (t == 0) {
                                MDDoubleArray mdarray = reader.float64().readMDArray(datasetName);
                                if (mdarray == null) {
                                    return null;
                                }
                                doubleIter = mdarray.iterator();
                                if (doubleIter == null) {
                                    return null;
                                }
                            }
                            if (!fillDoubleLargeArray(typeInfo, doubleIter, la)) {
                                return null;
                            }
                        }
                        break;
                        default:
                            return null;
                    }
                }
                break;
                case STRING: {
                    if (t == 0) {
                        MDArray<String> mdarray = reader.string().readMDArray(datasetName);
                        if (mdarray == null) {
                            return null;
                        }
                        stringIter = mdarray.iterator();
                        if (stringIter == null) {
                            return null;
                        }
                    }
                    if (!fillStringLargeArray(typeInfo, stringIter, la)) {
                        return null;
                    }
                }
                break;

                case COMPOUND: {
                    if (t == 0) {
                        MDArray<ComplexNumber> mdarray = reader.compound().readMDArray(datasetName, ComplexNumber.class
                        );
                        if (mdarray == null) {
                            return null;
                        }
                        complexIter = mdarray.iterator();
                        if (complexIter == null) {
                            return null;
                        }
                    }
                    if (!fillComplexLargeArray(typeInfo, complexIter, la)) {
                        return null;
                    }
                }
                break;
                default:
                    return null;

            }

            if (progressAgent != null) {
                progressAgent.setProgress(currentProgress + ((t + 1) / (double) ntimesteps) * progressStep);
            }
            list.add(la);
        }

        return list;
    }

    /**
     * Reads HDF5 dataset and stores the data in the list of Floats.
     *
     * @param reader      HDF5 reader
     * @param datasetName name of HDF5 dataset
     *
     * @return list of Floats.
     */
    private static ArrayList<Float> readTimeSeries(IHDF5Reader reader, String datasetName)
    {
        HDF5DataSetInformation datasetInfo = reader.object().getDataSetInformation(datasetName);
        if (datasetInfo == null || datasetInfo.getRank() != 1 || datasetInfo.getTypeInformation().getDataClass() != HDF5DataClass.FLOAT || datasetInfo.getTypeInformation().getElementSize() != 4) {
            return null;
        }
        float[] timeseries = reader.readFloatArray(datasetName);
        if (timeseries == null) {
            return null;
        }
        ArrayList<Float> list = new ArrayList<>(timeseries.length);
        for (int i = 0; i < timeseries.length; i++) {
            list.add(timeseries[i]);
        }
        return list;
    }

    /**
     * Reads float matrix from HDF5 dataset.
     *
     * @param reader      HDF5 reader
     * @param datasetName name of HDF5 dataset
     *
     * @return float matrix
     */
    private static float[][] readFloatMatrix(IHDF5Reader reader, String datasetName)
    {
        HDF5DataSetInformation datasetInfo = reader.object().getDataSetInformation(datasetName);
        if (datasetInfo == null || datasetInfo.getRank() != 2 || datasetInfo.getTypeInformation().getDataClass() != HDF5DataClass.FLOAT || datasetInfo.getTypeInformation().getElementSize() != 4) {
            return null;
        }
        return reader.readFloatMatrix(datasetName);
    }

    /**
     * Sets preferred ranges in a given DataArray.
     *
     * @param da                     DataArray
     * @param preferredRange         preferred range
     * @param preferredPhysicalRange preferred physical range
     */
    private static void setPreferedRanges(DataArray da, double[] preferredRange, double[] preferredPhysicalRange)
    {
        if (da == null) {
            return;
        }
        if (preferredRange != null && preferredPhysicalRange == null) {
            da.setPreferredRange(preferredRange[0], preferredRange[1]);
        } else if (preferredRange != null && preferredPhysicalRange != null) {
            da.setPreferredRanges(preferredRange[0], preferredRange[1], preferredPhysicalRange[0], preferredPhysicalRange[1]);
        }
    }

    /**
     * Returns the value of "dimensions" attribute stored in a given HDF5 group.
     *
     * @param reader          HDF5 reader
     * @param visNowFieldName HDF5 group
     *
     * @return the value of "dimensions" attribute
     */
    private static long[] getHDF5Dims(IHDF5Reader reader, String visNowFieldName)
    {
        IHDF5ObjectReadOnlyInfoProviderHandler objectReader = reader.object();
        if (!objectReader.hasAttribute(visNowFieldName, "dimensions")) {
            return null;
        }
        HDF5DataTypeInformation attrInfo = objectReader.getAttributeInformation(visNowFieldName, "dimensions");
        if (!(attrInfo.getRank() == 1 && attrInfo.getNumberOfElements() >= 1 && attrInfo.getNumberOfElements() <= 3 && attrInfo.getDataClass() == HDF5DataClass.INTEGER && attrInfo.isSigned() && attrInfo.getElementSize() == 8)) {
            return null;
        }
        return reader.int64().getArrayAttr(visNowFieldName, "dimensions");
    }

}
