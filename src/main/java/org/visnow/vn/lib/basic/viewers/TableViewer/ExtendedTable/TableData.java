//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.TableViewer.ExtendedTable;

import java.util.LinkedHashMap;
import java.util.Map;
import org.visnow.vn.lib.basic.viewers.TableViewer.DataSet;

/**
 * Class holding all available data series (string representation of VisNow component) and selected (displayed) data series.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 * @author norkap
 */
public class TableData
{

    private Map<String, DataSet> series;
    private Map<String, DataSet> seriesToDisplay;

    public TableData()
    {
        this.series = new LinkedHashMap<>();
        this.seriesToDisplay = new LinkedHashMap<>();
    }

    public void updateDisplayedSeries()
    {
        if (series.size() > 0) {
            if (seriesToDisplay.size() > 0) {
                Map<String, DataSet> newSeriesToDisplay = new LinkedHashMap<>();
                seriesToDisplay.keySet().forEach(key -> {
                    DataSet ds = series.get(key);
                    if (ds != null) newSeriesToDisplay.put(key, ds);
                });
                if (newSeriesToDisplay.size() > 0) {
                    seriesToDisplay = newSeriesToDisplay;
                } else {
                    seriesToDisplay.clear();
                    DataSet first = getFirstEntry(series);
                    seriesToDisplay.put(first.getName(), first);
                }
            } else {
                DataSet first = getFirstEntry(series);
                seriesToDisplay.put(first.getName(), first);
            }
        } else {
            seriesToDisplay.clear();
        }
    }

    public Map<String, DataSet> getSeries()
    {
        return series;
    }

    public Map<String, DataSet> getDisplayedSeries()
    {
        return this.seriesToDisplay;
    }

    public int getSeriesCount()
    {
        return series.size();
    }

    public int getDisplayedSeriesCount()
    {
        return this.seriesToDisplay.size();
    }

    public String[] getSeriesNames()
    {
        return series.keySet().toArray(new String[0]);
    }

    public String[] getDisplayedSeriesNames()
    {
        return seriesToDisplay.keySet().toArray(new String[0]);
    }

    public String getDisplayedSeriesName(int idx)
    {
        return seriesToDisplay.keySet().toArray(new String[0])[idx];
    }

    public void setSeries(Map<String, DataSet> series)
    {
        this.series = series;
    }

    public boolean addDatasetToDisplayedSeries(String key)
    {
        if (!seriesToDisplay.containsKey(key)) {
            seriesToDisplay.put(key, series.get(key));
            return true;
        }
        return false;
    }

    public void removeDatasetsFromDisplayedSeries(String[] names)
    {
        for (String name : names) {
            seriesToDisplay.remove(name);
        }
    }

    private DataSet getFirstEntry(Map<String, DataSet> series)
    {
        return series.values().iterator().next();
    }

}
