/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices;

/**
 * Reference to a device storing name of a device and a friendly name without an accessible
 * reference to a device. Used for displaying the device name and basic
 * read-only parameters without having a reference to IPassiveDevice (so before acquiring the device
 * for use by Viewer3D).
 * <p/>
 * In fact it stores a IPassiveDevice pointer, but ONLY as a PRIVATE reference, which must not be
 * accesed from outside.
 *
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class DeviceName
{

    private final IPassiveDevice device;

    public DeviceName(IPassiveDevice device)
    {
        this.device = device;
    }

    public String getName()
    {
        return device.getDeviceName();
    }

    public String getFriendlyName()
    {
        return device.getDeviceFriendlyName();
    }

    public boolean isUsed()
    {
        return device.isUsed();
    }

    public boolean isOwnedByMe(Object owner)
    {
        return device.isOwnedByMe(owner);
    }

    public boolean isAttachable()
    {
        return device.isAttachable();
    }
}
