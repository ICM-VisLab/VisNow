/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.IntensityEqualizer;

import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.gui.widgets.RunButton;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;


public class IntensityEqualizerShared
{
    
    public static final String COMPONENT_STRING       = "Component";
    public static final String GAIN_STRING            = "Gain";
    public static final String WEIGHTS_STRING         = "Weights";
    public static final String RUNNING_MESSAGE_STRING = "Running message";
    
    static final ParameterName<ComponentFeature> COMPONENT = new ParameterName(COMPONENT_STRING);    
    static final ParameterName<Float> GAIN = new ParameterName(GAIN_STRING);    
    static final ParameterName<float[]> WEIGHTS = new ParameterName(WEIGHTS_STRING);    
    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>(RUNNING_MESSAGE_STRING);
}
