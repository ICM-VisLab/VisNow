/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadVisNowField.utils;

import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FilePartSchema;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class SkipSchema extends FilePartSchema
{

    public static final int COUNT = 0;
    public static final int PATTERN = 1;
    public static final int PATTERN_INCL = 2;

    private int type = COUNT;
    private int count = 0;
    private String pattern = "";

    public SkipSchema(int n)
    {
        type = COUNT;
        count = n;
    }

    public SkipSchema(String pattern)
    {
        type = PATTERN;
        this.pattern = pattern;
    }

    public SkipSchema(String pattern, boolean incl)
    {
        type = PATTERN_INCL;
        this.pattern = pattern;
    }

    public int getCount()
    {
        return count;
    }

    public String getPattern()
    {
        return pattern;
    }

    public int getType()
    {
        return type;
    }

    @Override
    public String toString()
    {
        switch (type) {
            case COUNT:
                return "skip " + count + " items";
            case PATTERN:
                return "skip to" + pattern;
            default:
                return "skip including " + pattern;
        }
    }

    @Override
    public String toString(boolean t)
    {
        return toString();
    }

}
