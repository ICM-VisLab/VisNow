/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.testdata.TestIrregularFieldFlow;

import static org.apache.commons.math3.util.FastMath.*;
import org.apache.log4j.Logger;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import static org.visnow.vn.lib.basic.testdata.TestIrregularFieldFlow.TestIrregularFieldFlowShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class TestIrregularFieldFlow extends OutFieldVisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(TestIrregularFieldFlow.class);

    /**
     * Creates a new instance of TestGeometryObject
     */
    protected GUI computeUI = null;
    public static OutputEgg[] outputEggs = null;
    private int runQueue = 0;

    public TestIrregularFieldFlow()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                    startAction();
                }

            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParameters(parameters);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(POINT_COUNT, 50),
            new Parameter<>(TIMESTEPS_NUMBER, 50),
            new Parameter<>(RUNNING_MESSAGE, RUN_DYNAMICALLY)};

    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    private void createTestField(int nPoints, int nTimeFrames)
    {
        int nnPoints = nPoints;

        outIrregularField = new IrregularField(nnPoints);

        TimeData timeCoords = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
        FloatLargeArray coords = new FloatLargeArray(3 * nnPoints);
        int[] data = new int[nnPoints];
        String[] texts = new String[nnPoints];
        int[] cells = new int[nnPoints];
        byte[] orient = new byte[nnPoints];
        for (int i = 0; i < data.length; i++) {
            data[i] = i + 1;
            texts[i] = "point " + i;
            cells[i] = i;
            orient[i] = 1;
        }

        //random inital positions
        for (long i = 0; i < nnPoints; i++) {
            if (i % 2 == 1) {
                for (long m = 0; m < 3; m++) {
                    coords.setFloat(3 * i + m, (float) random());
                }
            }
        }
        for (long i = 0; i < nnPoints; i++) {
            if (i % 2 == 0) {
                for (long m = 0; m < 3; m++) {
                    coords.setFloat(3 * i + m, 0);
                }
            }
        }

        int nSplit = nTimeFrames / 3;

        timeCoords.setValue(coords, 0);
        
        for (int n = 1; n < nTimeFrames; n++) {
            
            FloatLargeArray tmp = new FloatLargeArray(3 * nnPoints);
            for (long i = 0; i < nnPoints; i++) {
                if (i % 2 == 1) {
                    tmp.setFloat(i * 3, coords.getFloat(i * 3) + (float) sin(14.0f * i * n / (nTimeFrames * nnPoints)));
                    tmp.setFloat(i * 3 + 1, coords.getFloat(i * 3 + 1) + 1.0f / nnPoints);
                    tmp.setFloat(i * 3 + 2, coords.getFloat(i * 3 + 2) + (float) sin(7.0f * i * n / (nTimeFrames * nnPoints)));
                }
            }

            for (long i = 0; i < nnPoints; i++) {
                if (i % 2 == 0) {
                    if (n < nSplit) {
                        tmp.setFloat(i * 3, 0);
                        tmp.setFloat(i * 3 + 1, 0);
                        tmp.setFloat(i * 3 + 2, 0);
                 } else {
                        long l = (i + 1) * 3;
                        tmp.setFloat(i * 3, coords.getFloat(l) + (float) sin(14.0f * i * (n - nSplit) / (nTimeFrames * nnPoints)));
                        tmp.setFloat(i * 3 + 1, coords.getFloat(l + 1) + (n - nSplit) * 0.5f / nnPoints);
                        tmp.setFloat(i * 3 + 2, coords.getFloat(l + 2) + (float) sin(7.0f * i * (n - nSplit) / (nTimeFrames * nnPoints)));
                    }
                }
            }
            timeCoords.setValue(tmp, n);

        }
        CellArray ca = new CellArray(CellType.POINT, cells, orient, null);
        CellSet cs = new CellSet();
        cs.setCellArray(ca);
        outIrregularField.addCellSet(cs);
        outIrregularField.setCoords(timeCoords);
        outIrregularField.addComponent(DataArray.create(data, 1, "points"));
        outIrregularField.addComponent(DataArray.create(texts, 1, "texts"));

        TimeData timeMask = new TimeData(DataArrayType.FIELD_DATA_LOGIC); 
        for (int n = 0; n < nTimeFrames; n++) {
            LogicLargeArray valid = new LogicLargeArray(nnPoints);
            for (int i = 0; i < nnPoints; i++) {
                valid.setBoolean(i, (n >= nSplit || i % 2 == 1));
            }
            timeMask.setValue(valid, n);
        }
        outIrregularField.setMask(timeMask);

    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }
    
    private void validateParamsAndSetSmart()
    {
        parameters.setParameterActive(false);

        if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE) parameters.set(RUNNING_MESSAGE, NO_RUN);

        parameters.setParameterActive(true);
    }

    @Override
    public void onActive()
    {
        int i = 0;
        if (true) i = 1;
        Parameters p;
        synchronized (parameters) {
            validateParamsAndSetSmart();
            p = parameters.getReadOnlyClone();
        }
        
        notifyGUIs(p, isFromVNA(), false);

        if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
            runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data
            createTestField(p.get(POINT_COUNT), p.get(TIMESTEPS_NUMBER));
            setOutputValue("outField", new VNIrregularField(outIrregularField));
            outField = outIrregularField;
            prepareOutputGeometry();
            show();
        }
    }
}
