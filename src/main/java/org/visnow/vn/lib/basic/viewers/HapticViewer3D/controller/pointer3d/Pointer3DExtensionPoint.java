/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller.pointer3d;

import org.jogamp.java3d.utils.geometry.Sphere;
import org.jogamp.java3d.ColoringAttributes;
import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.Vector3f;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import org.visnow.vn.geometries.objects.generics.OpenTransformGroup;

/**
 * An extension to the arrow pointer meaning that a POINT pick 3D is active (a pick 3D that will
 * use only a picked point).
 * <p/>
 * It shows a small ball next to the arrow's head.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class Pointer3DExtensionPoint extends Pointer3DExtension
{

    public Pointer3DExtensionPoint(float radius, float xOffset, float yOffset, float zOffset)
    {
        initialize(radius, xOffset, yOffset, zOffset);
    }

    /**
     * Creates small ball to be displayed next to the arrow
     */
    private void initialize(float radius, float xOffset, float yOffset, float zOffset)
    {

        OpenTransformGroup tra = new OpenTransformGroup("pointer point transform group");
        Transform3D move = new Transform3D();
        move.setTranslation(new Vector3f(xOffset, yOffset, zOffset));
        tra.setTransform(move);
        Sphere sphere = new Sphere(radius);
        OpenAppearance appearance = createAppearance(false);
        appearance.setColoringAttributes(new ColoringAttributes(0.3f, 0.3f, 1.0f, ColoringAttributes.FASTEST));
        sphere.setAppearance(appearance);
        tra.addChild(sphere);
        this.addChild(tra);
    }
}
