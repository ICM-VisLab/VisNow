/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.DifferentialOperations;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.widgets.RunButton.RunState;
import static org.visnow.vn.lib.basic.filters.DifferentialOperations.DifferentialOperationsShared.*;
import static org.visnow.vn.lib.basic.filters.DifferentialOperations.DifferentialOperationsShared.ScalarOperation.*;
import static org.visnow.vn.lib.basic.filters.DifferentialOperations.DifferentialOperationsShared.VectorOperation.*;
import static org.visnow.vn.lib.basic.filters.DifferentialOperations.DifferentialOperationsShared.TimeOperation.*;
import org.visnow.vn.system.swing.FixedGridBagLayoutPanel;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public final class GUI extends FixedGridBagLayoutPanel
{
    protected Parameters parameters = null;
    protected boolean tableActive = false;  // this flag is necessary to distinguish user action on jTable from setter actions.


    protected final static ScalarOperation[] SCALAR_TABLE_COLUMN_ORDER =
         {null, GRADIENT, GRADIENT_COMPONENTS, GRADIENT_NORM, NORMALIZED_GRADIENT,
                LAPLACIAN, HESSIAN, HESSIAN_EIGEN};
    protected final static String[] SCALAR_TABLE_TOOLTIPS = {
        null, "gradient", "partial derivatives as scalars", "gradient norm", "normalized gradient",
        "Laplacian","Hessian matrix (upper triangle)","Hessian eigenvalues and eigenvectors"
    };
    protected final static int[] SCALAR_TABLE_COLUMN_WIDTHS = new int[]{60, 15, 20, 15, 25, 15, 15, 30};
    protected DefaultTableModel scalarTableModel =
            new DefaultTableModel(new Object[][]{{null, null, null, null, null, null, null, null}},
                                  new String[]{"", "<html>\u2207</html>", "<html>[\u2202/\u2202x<inf>i</inf>]</html>]", "<html>|\u2207|</html>",
                                               "<html>\u2207/|\u2207|</html>", "\u0394", "<html>D<sup>2</sup></html>",
                                               "<html>D<sup>2</sup>eig</html>"})
    {
        Class[] types = new Class[]{
            java.lang.String.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class,
            java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class
        };
        boolean[] canEdit = new boolean[] {false, true, true, true, true, true, true, true, true};
        @Override
        public Class getColumnClass(int columnIndex)
        {
            return types[columnIndex];
        }
        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex)
        {
            return canEdit[columnIndex];
        }
    };
    protected javax.swing.JTable scalarComponentTable;
    protected JTableHeader createScalarTableHeader() {
        return new JTableHeader(scalarComponentTable.getColumnModel()) {
            @Override
            public String getToolTipText(MouseEvent e) {
                java.awt.Point p = e.getPoint();
                int index = columnModel.getColumnIndexAtX(p.x);
                int realIndex = columnModel.getColumn(index).getModelIndex();
                return SCALAR_TABLE_TOOLTIPS[realIndex];
            }
        };
    };
    protected TableModelListener scalarTableListener = new TableModelListener()
    {
        @Override
        public void tableChanged(TableModelEvent e)
        {
            if (scalarComponentTable.getModel().getRowCount() == parameters.get(META_SCALAR_COMPONENT_NAMES).length) {
                ScalarOperation[][] scalarOperations = new ScalarOperation[parameters.get(META_SCALAR_COMPONENT_NAMES).length][];
                for (int row = 0; row < scalarOperations.length; row++) {
                    List<ScalarOperation> operations = new ArrayList<>();
                    for (int column = 1; column < SCALAR_TABLE_COLUMN_ORDER.length; column++)
                        if ((Boolean) scalarComponentTable.getModel().getValueAt(row, column))
                            operations.add(SCALAR_TABLE_COLUMN_ORDER[column]);
                    scalarOperations[row] = operations.toArray(new ScalarOperation[operations.size()]);
                }
                if (tableActive) runButton.setPendingIfNoAuto();
                parameters.set(SCALAR_OPERATIONS, scalarOperations);
            }
        }
    };

    private final static VectorOperation[] VECTOR_TABLE_COLUMN_ORDER = {null, DERIV, ROT, DIV};
    protected final static String[] VECTOR_TABLE_TOOLTIPS = {null, "partial derivatives", "rotation", "divergence"};
    protected final static int[] VECTOR_TABLE_COLUMN_WIDTHS = new int[]{100, 20, 20, 20};
    protected final DefaultTableModel vectorTableModel =
            new DefaultTableModel(new Object [][] {{null, null, null, null, null}},
                                  new String []    {"", "<html>[\u2202/\u2202x<inf>i</inf>]</html>]", "rot", "div"})
    {
        Class[] types = new Class [] {
            java.lang.String.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class
        };
        boolean[] canEdit = new boolean[]{false, true, true, true};
        @Override
        public Class getColumnClass(int columnIndex)
        {
            return types [columnIndex];
        }
        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex)
        {
            return canEdit[columnIndex];
        }
    };
    protected javax.swing.JTable vectorComponentTable;
    protected JTableHeader createVectorTableHeader() {
        return new JTableHeader(vectorComponentTable.getColumnModel()) {
            @Override
            public String getToolTipText(MouseEvent e) {
                java.awt.Point p = e.getPoint();
                int index = columnModel.getColumnIndexAtX(p.x);
                int realIndex = columnModel.getColumn(index).getModelIndex();
                return VECTOR_TABLE_TOOLTIPS[realIndex];
            }
        };
    };
    protected TableModelListener vectorTableListener = new TableModelListener()
    {
        @Override
        public void tableChanged(TableModelEvent e)
        {
            if (vectorComponentTable.getModel().getRowCount() == parameters.get(META_VECTOR_COMPONENT_NAMES).length) {
                VectorOperation[][] vectorOperations = new VectorOperation[parameters.get(META_VECTOR_COMPONENT_NAMES).length][];
                for (int row = 0; row < vectorOperations.length; row++) {
                    List<VectorOperation> operations = new ArrayList<>();
                    for (int column = 1; column < VECTOR_TABLE_COLUMN_ORDER.length; column++)
                        if ((Boolean) vectorComponentTable.getModel().getValueAt(row, column))
                            operations.add(VECTOR_TABLE_COLUMN_ORDER[column]);
                    vectorOperations[row] = operations.toArray(new VectorOperation[operations.size()]);
                }

                if (tableActive) runButton.setPendingIfNoAuto();
                parameters.set(VECTOR_OPERATIONS, vectorOperations);
            }
        }
    };

    private final static TimeOperation[] TIME_TABLE_COLUMN_ORDER = {null, D_DT, D2_DT2};
    protected final static String[] TIME_TABLE_TOOLTIPS = {null, "time derivative (change velocity)", "time change acceleration"};
    protected final static int[] TIME_TABLE_COLUMN_WIDTHS = new int[]{120, 30, 30};
    protected DefaultTableModel timeTableModel
            = new DefaultTableModel(  new Object[][] {{null, null, null}}, new String[]{"", "d/dt", "<html>d<sup>2</sup>/dt<sup>2</sup>"})
    {
        Class[] types = new Class[]{
            java.lang.String.class, java.lang.Boolean.class, java.lang.Boolean.class
        };
        boolean[] canEdit = new boolean[]{false, true, true};
        @Override
        public Class getColumnClass(int columnIndex)
        {
            return types[columnIndex];
        }
        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex)
        {
            return canEdit[columnIndex];
        }
    };
    private javax.swing.JTable timeComponentTable;

    protected JTableHeader createTimeTableHeader() {
        return new JTableHeader(timeComponentTable.getColumnModel()) {
            @Override
            public String getToolTipText(MouseEvent e) {
                java.awt.Point p = e.getPoint();
                int index = columnModel.getColumnIndexAtX(p.x);
                int realIndex = columnModel.getColumn(index).getModelIndex();
                return TIME_TABLE_TOOLTIPS[realIndex];
            }
        };
    };
     protected TableModelListener timeTableListener = new TableModelListener()
    {
        @Override
        public void tableChanged(TableModelEvent e)
        {
            if (timeComponentTable.getModel().getRowCount() == parameters.get(META_TIME_COMPONENT_NAMES).length) {
                TimeOperation[][] timeOperations = new TimeOperation[parameters.get(META_TIME_COMPONENT_NAMES).length][];
                for (int row = 0; row < timeOperations.length; row++) {
                    List<TimeOperation> operations = new ArrayList<>();
                    for (int column = 1; column < TIME_TABLE_COLUMN_ORDER.length; column++)
                        if ((Boolean) timeComponentTable.getModel().getValueAt(row, column))
                            operations.add(TIME_TABLE_COLUMN_ORDER[column]);
                    timeOperations[row] = operations.toArray(new TimeOperation[operations.size()]);
                }
                if (tableActive) runButton.setPendingIfNoAuto();
                parameters.set(TIME_OPERATIONS, timeOperations);
            }
        }
    };
    /**
     * Creates new form GUI
     */
    public GUI()
    {
        initComponents();
        scalarComponentTable = new javax.swing.JTable();
        scalarComponentTable.setModel(scalarTableModel);
        for (int i = 0; i < SCALAR_TABLE_COLUMN_WIDTHS.length; i++)
            scalarComponentTable.getColumnModel().getColumn(i).setPreferredWidth(SCALAR_TABLE_COLUMN_WIDTHS[i]);
        scalarComponentTable.getTableHeader().setReorderingAllowed(false);
        scalarComponentTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        ((DefaultTableModel)scalarComponentTable.getModel()).addTableModelListener(scalarTableListener);
        JPanel scalarComponentTablePanel = new JPanel();
        scalarComponentTablePanel.setLayout(new BorderLayout());
        scalarComponentTablePanel.add(scalarComponentTable, BorderLayout.CENTER);
        scalarComponentTablePanel.add(createScalarTableHeader() , BorderLayout.NORTH);
        jScrollPane1.getViewport().add(scalarComponentTablePanel);

        vectorComponentTable = new javax.swing.JTable();
        vectorComponentTable.setModel(vectorTableModel);
        for (int i = 0; i < VECTOR_TABLE_COLUMN_WIDTHS.length; i++)
            vectorComponentTable.getColumnModel().getColumn(i).setPreferredWidth(VECTOR_TABLE_COLUMN_WIDTHS[i]);
        vectorComponentTable.getTableHeader().setReorderingAllowed(false);
        vectorComponentTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        ((DefaultTableModel)vectorComponentTable.getModel()).addTableModelListener(vectorTableListener);
        JPanel vectorComponentTablePanel = new JPanel();
        vectorComponentTablePanel.setLayout(new BorderLayout());
        vectorComponentTablePanel.add(vectorComponentTable, BorderLayout.CENTER);
        vectorComponentTablePanel.add(createVectorTableHeader() , BorderLayout.NORTH);
        jScrollPane2.getViewport().add(vectorComponentTablePanel);

        timeComponentTable = new javax.swing.JTable();
        timeComponentTable.setModel(timeTableModel);
        for (int i = 0; i < TIME_TABLE_COLUMN_WIDTHS.length; i++)
            timeComponentTable.getColumnModel().getColumn(i).setPreferredWidth(TIME_TABLE_COLUMN_WIDTHS[i]);
        timeComponentTable.getTableHeader().setReorderingAllowed(false);
        timeComponentTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        ((DefaultTableModel)timeComponentTable.getModel()).addTableModelListener(timeTableListener);
        JPanel timeComponentTablePanel = new JPanel();
        timeComponentTablePanel.setLayout(new BorderLayout());
        timeComponentTablePanel.add(timeComponentTable, BorderLayout.CENTER);
        timeComponentTablePanel.add(createTimeTableHeader() , BorderLayout.NORTH);
        jScrollPane3.getViewport().add(timeComponentTablePanel);

        runButton.setPendingIfNoAuto();
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        scalarPanel = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        vectorPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        runButton = new org.visnow.vn.gui.widgets.RunButton();
        timePanel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        currentTimeOnlyButton = new javax.swing.JRadioButton();
        allTimeStepsButton = new javax.swing.JRadioButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        noOpLabel = new javax.swing.JLabel();
        noOpLabel1 = new javax.swing.JLabel();

        scalarPanel.setLayout(new java.awt.GridBagLayout());

        jLabel2.setText("Scalar components:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        scalarPanel.add(jLabel2, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        scalarPanel.add(jScrollPane1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(scalarPanel, gridBagConstraints);

        vectorPanel.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("Vector components:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        vectorPanel.add(jLabel1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        vectorPanel.add(jScrollPane2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(vectorPanel, gridBagConstraints);

        runButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener() {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
                runButtonUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt) {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(runButton, gridBagConstraints);

        timePanel.setLayout(new java.awt.GridBagLayout());

        jLabel3.setText("Time derivatives:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        timePanel.add(jLabel3, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        timePanel.add(jScrollPane3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(timePanel, gridBagConstraints);

        buttonGroup1.add(currentTimeOnlyButton);
        currentTimeOnlyButton.setSelected(true);
        currentTimeOnlyButton.setText("compute for current time only");
        currentTimeOnlyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                currentTimeOnlyButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 6, 0, 0);
        add(currentTimeOnlyButton, gridBagConstraints);

        buttonGroup1.add(allTimeStepsButton);
        allTimeStepsButton.setText("compute for all time steps");
        allTimeStepsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                allTimeStepsButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 7, 0, 0);
        add(allTimeStepsButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        add(filler1, gridBagConstraints);

        noOpLabel.setForeground(new java.awt.Color(153, 0, 51));
        noOpLabel.setText("<html>Spatial derivatives (gradient, <p>\nLaplacian etc.) are available<p> \nfor regular fields only</html>");
        noOpLabel.setInheritsPopupMenu(false);
        noOpLabel.setMaximumSize(new java.awt.Dimension(2147483647, 120));
        noOpLabel.setMinimumSize(new java.awt.Dimension(103, 60));
        noOpLabel.setName(""); // NOI18N
        noOpLabel.setPreferredSize(new java.awt.Dimension(707, 60));
        noOpLabel.setVerifyInputWhenFocusTarget(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(noOpLabel, gridBagConstraints);

        noOpLabel1.setForeground(new java.awt.Color(153, 0, 51));
        noOpLabel1.setText("<html>The input field has no time <p>\ndependent components, thus time<p>\ndifferentials are not available.</html>");
        noOpLabel1.setInheritsPopupMenu(false);
        noOpLabel1.setMaximumSize(new java.awt.Dimension(2147483647, 120));
        noOpLabel1.setMinimumSize(new java.awt.Dimension(103, 60));
        noOpLabel1.setName(""); // NOI18N
        noOpLabel1.setPreferredSize(new java.awt.Dimension(707, 60));
        noOpLabel1.setVerifyInputWhenFocusTarget(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(noOpLabel1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void runButtonUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_runButtonUserChangeAction
    {//GEN-HEADEREND:event_runButtonUserChangeAction
        parameters.set(RUNNING_MESSAGE, (RunState) evt.getEventData());
    }//GEN-LAST:event_runButtonUserChangeAction

    private void currentTimeOnlyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_currentTimeOnlyButtonActionPerformed
        parameters.set(COMPUTE_FULLY, allTimeStepsButton.isSelected());
    }//GEN-LAST:event_currentTimeOnlyButtonActionPerformed

    private void allTimeStepsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_allTimeStepsButtonActionPerformed
        parameters.set(COMPUTE_FULLY, allTimeStepsButton.isSelected());
    }//GEN-LAST:event_allTimeStepsButtonActionPerformed

    /**
     * @param p cloned parameters
     */
    void updateGUI(final ParameterProxy p, boolean resetFully, boolean setRunButtonPending)
    {

        tableActive = false;

        scalarPanel.setVisible(p.get(META_SCALAR_COMPONENT_NAMES).length > 0);
        DefaultTableModel scalarComponentTableModel =
                (DefaultTableModel) scalarComponentTable.getModel();
        scalarComponentTableModel.setRowCount(0);

        Object[] row = new Object[SCALAR_TABLE_COLUMN_ORDER.length];

        for (int i = 0; i < p.get(META_SCALAR_COMPONENT_NAMES).length; i++) {
            Arrays.fill(row, Boolean.FALSE);
            row[0] = p.get(META_SCALAR_COMPONENT_NAMES)[i];

            for (ScalarOperation scalarOperation : p.get(SCALAR_OPERATIONS)[i])
                row[Arrays.asList(SCALAR_TABLE_COLUMN_ORDER).indexOf(scalarOperation)] = true;
            scalarComponentTableModel.addRow(row);
        }

        vectorPanel.setVisible(p.get(META_VECTOR_COMPONENT_NAMES).length > 0);

        DefaultTableModel vectorComponentTableModel =
                (DefaultTableModel)vectorComponentTable.getModel();
        vectorComponentTableModel.setRowCount(0);

        row = new Object[VECTOR_TABLE_COLUMN_ORDER.length];

        for (int i = 0; i < p.get(META_VECTOR_COMPONENT_NAMES).length; i++) {
            Arrays.fill(row, Boolean.FALSE);
            row[0] = p.get(META_VECTOR_COMPONENT_NAMES)[i];

            for (VectorOperation vectorOperation : p.get(VECTOR_OPERATIONS)[i])
                row[Arrays.asList(VECTOR_TABLE_COLUMN_ORDER).indexOf(vectorOperation)] = true;
            vectorComponentTableModel.addRow(row);
        }

        timePanel.setVisible(p.get(META_TIME_COMPONENT_NAMES).length > 0);

        DefaultTableModel timeComponentTableModel =
                (DefaultTableModel)timeComponentTable.getModel();
        timeComponentTableModel.setRowCount(0);

        row = new Object[TIME_TABLE_COLUMN_ORDER.length];

        for (int i = 0; i < p.get(META_TIME_COMPONENT_NAMES).length; i++) {
            Arrays.fill(row, Boolean.FALSE);
            row[0] = p.get(META_TIME_COMPONENT_NAMES)[i];

            for (TimeOperation timeOperation : p.get(TIME_OPERATIONS)[i])
                row[Arrays.asList(TIME_TABLE_COLUMN_ORDER).indexOf(timeOperation)] = true;
            timeComponentTableModel.addRow(row);
        }
        noOpLabel.setVisible(p.get(META_SCALAR_COMPONENT_NAMES).length == 0 &&
                             p.get(META_VECTOR_COMPONENT_NAMES).length == 0);
        noOpLabel1.setVisible(p.get(META_TIME_COMPONENT_NAMES).length  == 0);
        runButton.setVisible(p.get(META_SCALAR_COMPONENT_NAMES).length != 0 ||
                             p.get(META_VECTOR_COMPONENT_NAMES).length != 0 ||
                             p.get(META_TIME_COMPONENT_NAMES).length   != 0);
        tableActive = true;

        runButton.updateAutoState(p.get(RUNNING_MESSAGE));
        runButton.updatePendingState(setRunButtonPending);
    }

    public void setParameters(Parameters parameters)
    {
        this.parameters = parameters;
    }

    public void fieldIsTimeDependent(boolean timeDependent)
    {
        allTimeStepsButton.setVisible(timeDependent);
        currentTimeOnlyButton.setVisible(timeDependent);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton allTimeStepsButton;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JRadioButton currentTimeOnlyButton;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel noOpLabel;
    private javax.swing.JLabel noOpLabel1;
    private org.visnow.vn.gui.widgets.RunButton runButton;
    private javax.swing.JPanel scalarPanel;
    private javax.swing.JPanel timePanel;
    private javax.swing.JPanel vectorPanel;
    // End of variables declaration//GEN-END:variables

    public static void main(String[] args)
    {
        JFrame f = new JFrame();
        f.add(new GUI());
        f.pack();
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }
}
