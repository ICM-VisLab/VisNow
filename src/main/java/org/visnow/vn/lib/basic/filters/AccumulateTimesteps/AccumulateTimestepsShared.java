//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.AccumulateTimesteps;

import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.lib.utils.field.MergeTimesteps.Action;

/**
 *
 * @author know
 */
public class AccumulateTimestepsShared
{

    public static final String RESET_STRING =          "reset";
    public static final String ACCUMULATE_STRING =     "accumulate";
    public static final String PAUSE_STRING =          "pause";
    public static final String TIME_INCREMENT_STRING = "time increment";
    public static final String ITEMS_STRING =          "items";
    public static final String ACTIONS_STRING =        "actions";
    public static final String TO_CLIPBOARD_STRING =   "copy to clipboard";

    public static final ParameterName<Boolean>  RESET =             new ParameterName<>(RESET_STRING);
    public static final ParameterName<Boolean>  ACCUMULATE_INPUTS = new ParameterName<>(ACCUMULATE_STRING);
    public static final ParameterName<Boolean>  PAUSE =             new ParameterName<>(PAUSE_STRING);
    public static final ParameterName<Float>    TIME_INCREMENT =    new ParameterName<>(TIME_INCREMENT_STRING);
    public static final ParameterName<String[]> ITEMS =             new ParameterName<>(ITEMS_STRING);
    public static final ParameterName<Action[]> ACTIONS =           new ParameterName<>(ACTIONS_STRING);
    public static final ParameterName<Boolean>  TO_CLIPBOARD =      new ParameterName<>(TO_CLIPBOARD_STRING);

    public static Parameter[] createDefaultParameters()
    {
        return new Parameter[]
        {
            new Parameter<>(RESET, false),
            new Parameter<>(ACCUMULATE_INPUTS, false),
            new Parameter<>(PAUSE, false),
            new Parameter<>(TIME_INCREMENT, 1f),
            new Parameter<>(ITEMS, new String[0]),
            new Parameter<>(ACTIONS, new Action[0]),
            new Parameter<>(TO_CLIPBOARD, false),
        };
    }

}
