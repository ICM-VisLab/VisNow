/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.RibbonPlot;

import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.jogamp.vecmath.Color3f;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.geometries.events.ColorEvent;
import static org.visnow.vn.lib.utils.graph3d.GenericGraph3DShared.*;
import static org.visnow.vn.lib.basic.mappers.RibbonPlot.RibbonPlotShared.*;
import org.visnow.vn.lib.utils.graph3d.AbstractGraph3D;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class RibbonPlot extends AbstractGraph3D
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    protected GUI computeUI = null;
    protected RegularField regularInField;
    protected int[] inDims = null;
    protected int axis = -1;
    protected float[] inNormalVector = new float[3];
    protected boolean ribbon = true;

    public RibbonPlot()
    {
        outObj.setName("ribbon plot");
        renderingParams.setDisplayMode(RenderingParams.SURFACE | RenderingParams.SEGMENT_CELLS);
        parameters.addParameterChangelistener((String name) -> {
            axis   = parameters.get(DIRECTION);
            ribbon = parameters.get(RIBBON);
            if (parameters.get(ADJUSTING) && irregularFieldGeometry != null) {
                newOutFieldStructure = false;
                updateCoords();
                irregularFieldGeometry.updateCoords();
            } else {
                newOutFieldStructure = true;
                startAction();
            }
        });
        backGroundColorListener = (ColorEvent e) -> {
            renderingParams.setColor(e.getSelectedColor());
            Color3f bgrC = e.getSelectedColor3f();
            renderingParams.setAmbientColor(bgrC);
            renderingParams.setDiffuseColor(bgrC);
        };

        SwingInstancer.swingRunAndWait(() -> {
            computeUI = new GUI();
            computeUI.setParameters(parameters);
            ui.addComputeGUI(computeUI);
            setPanel(ui);
            genericGUI = computeUI.getGenericGraph3DGUI();
        });
    }

    void updateCells()
    {
        int[] cells;
        byte[] orientations;
        int[] edges = new int[4 * inDims[1] * inDims[0]];
        byte[] edgeOrientations = new byte[2 * inDims[1] * inDims[0]];
        axis   = parameters.get(DIRECTION);
        ribbon = parameters.get(RIBBON);
        for (int i = 0; i < edgeOrientations.length; i++)
            edgeOrientations[i] = 1;
        if (axis == 0) {
            orientations = new byte[inDims[1] * (inDims[0] - 1)];
            for (int i = 0; i < orientations.length; i++)
                orientations[i] = 1;
            cells = new int[4 * inDims[1] * (inDims[0] - 1)];
            for (int i = 0, k = 0, l = 0; i < inDims[1]; i++, l += 2)
                for (int j = 0; j < inDims[0] - 1; j++, l += 2, k += 4) {
                    cells[k] = l;
                    cells[k + 1] = l + 1;
                    cells[k + 2] = l + 3;
                    cells[k + 3] = l + 2;
                }
            for (int i = 0, k = 0, l = 0; i < inDims[1]; i++, l += 2) {
                edges[k] = l;
                edges[k + 1] = l + 1;
                k += 2;
                for (int j = 0; j < inDims[0] - 1; j++, l += 2, k += 4) {
                    edges[k] = l;
                    edges[k + 1] = l + 2;
                    edges[k + 2] = l + 1;
                    edges[k + 3] = l + 3;
                }
                edges[k] = l;
                edges[k + 1] = l + 1;
                k += 2;
            }
        } else {
            orientations = new byte[inDims[0] * (inDims[1] - 1)];
            for (int i = 0; i < orientations.length; i++)
                orientations[i] = 1;
            cells = new int[4 * inDims[0] * (inDims[1] - 1)];
            for (int i = 0, k = 0; i < inDims[0]; i++)
                for (int j = 0, l = 2 * i; j < inDims[1] - 1; j++, l += 2 * inDims[0], k += 4) {
                    cells[k] = l;
                    cells[k + 1] = l + 1;
                    cells[k + 2] = l + 2 * inDims[0] + 1;
                    cells[k + 3] = l + 2 * inDims[0];
                }
            for (int i = 0, k = 0; i < inDims[0]; i++) {
                int l = 2 * i;
                edges[k] = l;
                edges[k + 1] = l + 1;
                k += 2;
                for (int j = 0; j < inDims[1] - 1; j++, l += 2 * inDims[0], k += 4) {
                    edges[k] = l;
                    edges[k + 1] = l + 2 * inDims[0];
                    edges[k + 2] = l + 1;
                    edges[k + 3] = l + 2 * inDims[0] + 1;
                }
                edges[k] = l;
                edges[k + 1] = l + 1;
                k += 2;
            }
        }
        CellArray ribbons = new CellArray(CellType.QUAD, cells, orientations, null);
        CellArray ribbonEdges = new CellArray(CellType.SEGMENT, edges, edgeOrientations, null);
        outIrregularField.getCellSets().clear();
        CellSet cs = new CellSet();
        cs.setCellArray(ribbons);
        cs.setCellArray(ribbonEdges);
        cs.generateDisplayData(outIrregularField.getCurrentCoords());
        outIrregularField.addCellSet(cs);
    }

    @Override
    protected void updateCoords()
    {
        axis   = parameters.get(DIRECTION);
        ribbon = parameters.get(RIBBON);
        if(inField == null)
            return;
        float[] tCoords = new float[inCoords.length];
        updateScale();
        if (inField.hasMask()) {
            LogicLargeArray mask = inField.getCurrentMask();
            for (int i = 0, k = 0, l = 0; i < inField.getNNodes(); i++, l++) {
                if (mask.getBoolean(i))
                    for (int j = 0; j < 3; j++, k++)
                        tCoords[k] = inCoords[k] + scale * normals[k] * (vals[l] - base);
                else
                    for (int j = 0; j < 3; j++, k++)
                        tCoords[k] = inCoords[k];
            }
        }
        else
            for (int i = 0, k = 0, l = 0; i < inField.getNNodes(); i++, l++)
                for (int j = 0; j < 3; j++, k++)
                    tCoords[k] = inCoords[k] + scale * normals[k] * (vals[l] - base);
        if (!ribbon)
            for (int i = 0, k = 0, l = 0; i < inField.getNNodes(); i++, k += 3, l += 6) {
                System.arraycopy(inCoords, k, outCoords, l,     3);
                System.arraycopy(tCoords,  k, outCoords, l + 3, 3);
            }
        else if (inField.getCurrentCoords() == null) {
                float[] transverseDir = (axis == 0) ? ((RegularField)inField).getAffine()[1] :
                                                      ((RegularField)inField).getAffine()[0];
                for (int i = 0, k = 0, l = 0; i < inField.getNNodes(); i++, l += 6)
                    for (int j = 0; j < 3; j++, k++) {
                        outCoords[l +     j] = tCoords[k] - .5f * transverseDir[j];
                        outCoords[l + 3 + j] = tCoords[k] + .5f * transverseDir[j];
                }
            }
            else {
                int plusOff = 0, minusOff = 0;
                float t = 0;
                for (int i = 0, l = 0, m = 0; i < inDims[1]; i++)
                    for (int j = 0; j < inDims[0]; j++, m += 6)  {
                        if (axis == 0) {
                            plusOff  =  3 * inDims[0];
                            minusOff = -3 * inDims[0];
                            t = .5f;
                            if (i == 0) {
                                minusOff = 0;
                                t = 1;
                            }
                            if (i == inDims[1] - 1) {
                                plusOff = 0;
                                t = 1;
                            }
                        }
                        else {
                            plusOff  =  3;
                            minusOff = -3;
                            t = .5f;
                            if (j == 0) {
                                minusOff = 0;
                                t = 1;
                            }
                            if (j == inDims[0] - 1) {
                                plusOff = 0;
                                t = 1;
                            }

                        }
                        for (int k = 0; k < 3; k++, l++) {
                            float u = .5f * t * (inCoords[l + plusOff] - inCoords[l + minusOff]);
                            outCoords[m +     k] = tCoords[l] - u;
                            outCoords[m + 3 + k] = tCoords[l] + u;
                        }
                    }
            }
        outIrregularField.setCurrentCoords(new FloatLargeArray(outCoords));
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        Parameter[] generalParams = super.createDefaultParameters();
        Parameter[] currentParams = new Parameter[]{
            new Parameter<>(DIRECTION,    0),
            new Parameter<>(RIBBON,       true),
        };
        Parameter[] params = new Parameter[generalParams.length + currentParams.length];
        System.arraycopy(generalParams, 0, params, 0, generalParams.length);
        System.arraycopy(currentParams, 0, params, generalParams.length, currentParams.length);
        return params;
    }

    @Override
    protected void updateOutputData()
    {
        outIrregularField.getComponents().clear();
        for (DataArray dta : inField.getComponents()) {
            int vl = dta.getVectorLength();
            LargeArray inDD = dta.getRawArray();
            LargeArray outDD = LargeArrayUtils.create(inDD.getType(), 2 * inDD.length(), false);
            for (long i = 0, j = 0; i < inField.getNNodes(); i++, j += 2) {
                LargeArrayUtils.arraycopy(inDD, i * vl, outDD, j * vl, vl);
                LargeArrayUtils.arraycopy(inDD, i * vl, outDD, (j + 1) * vl, vl);
            }
            outIrregularField.addComponent(DataArray.create(outDD, vl, dta.getName()).
                                                     preferredRanges(dta.getPreferredMinValue(),    dta.getPreferredMaxValue(),
                                                                     dta.getPreferredPhysMinValue(),dta.getPreferredPhysMaxValue()));
        }
    }

    @Override
    protected void updateRenderingParams()
    {
        renderingParams.setShadingMode(parameters.get(RIBBON) ?
                                       RenderingParams.GOURAUD_SHADED :
                                       RenderingParams.BACKGROUND);
        renderingParams.setLineThickness(parameters.get(RIBBON) ? 1 :2);
//        renderingParams.setTransparency(.999f);
    }

    @Override
    public void createOutField()
    {
        regularInField = (RegularField)inField;
        inDims = regularInField.getDims();
        axis = parameters.get(DIRECTION);
        ribbon = parameters.get(RIBBON);
        createGraph3DData();
        outIrregularField = new IrregularField(2 * (int) inField.getNNodes());
        if (inField.hasMask()) {
            LogicLargeArray outMask = new LogicLargeArray(2 * regularInField.getNNodes());
            for (long i = 0, l = 0; i < regularInField.getNNodes(); i++)
                for (int j = 0; j < 2; j++, l++)
                    outMask.setBoolean(l, inField.getCurrentMask().getBoolean(i));
            outIrregularField.setCurrentMask(outMask);
        }
        outCoords = new float[3 * (int) outIrregularField.getNNodes()];
        updateCoords();
        updateCells();
        outField = outIrregularField;
    }
}
