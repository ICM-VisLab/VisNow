/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.TextGlyphs;

import org.visnow.vn.lib.utils.text.GlyphsObject;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.VisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNGeometryObject;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class TextGlyphs extends VisualizationModule //implements RenderWindowListeningModule
{

    /**
     * Creates a new instance of CreateGrid
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI ui = null;
    private Params params;
    private Field inField = null;
    private boolean fromIn = false;
    private GlyphsObject glyphObj = new GlyphsObject();

    public TextGlyphs()
    {
        parameters = params = new Params();
        params.addChangeListener((ChangeEvent evt) -> {
            if (inField != null && !fromIn)
                glyphObj.update(inField, params);
        });
        SwingInstancer.swingRunAndWait(() -> {
            ui = new GUI();
            ui.setParams(params);
            setPanel(ui);
        });
    }

    @Override
    public void onInitFinishedLocal()
    {
        outObj = glyphObj;
        outObj.setCreator(this);
        setOutputValue("outObj", new VNGeometryObject(outObj));
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null &&
            ((VNField) getInputFirstValue("inField")).getField() != null) {
            params.setActive(false);
            fromIn = true;
            if (((VNField) getInputFirstValue("inField")).getField() != inField) {
                inField = ((VNField) getInputFirstValue("inField")).getField();
                if (inField instanceof RegularField)
                    params.getDownsizeParams().setFieldData(inField.getNNodes(), ((RegularField)inField).getDims());
                else
                    params.getDownsizeParams().setFieldData(inField.getNNodes(), null);
                params.getValidComponentRange().setContainer(inField);
                ui.setInData(inField, dataMappingParams);
                params.setChange(Params.COUNT_CHANGED);
            } else
                params.setChange(Params.GLYPHS_CHANGED);
            params.setActive(true);
            fromIn = false;
        }
        else
            inField = null;
        glyphObj.update(inField, params);
    }
}
