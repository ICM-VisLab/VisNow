/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadImage;

import org.visnow.vn.engine.core.ParameterName;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class ReadImageShared
{
    //not null array (may be empty)
    static final ParameterName<String[]> FILENAMES = new ParameterName("File names");

    static final ParameterName<Boolean> CONVERT_TO_GRAYSCALE = new ParameterName("Convert to grayscale");
       
    static final ParameterName<Boolean> RETAIN_ORIGINAL = new ParameterName("Retain original");
    
//TODO: solve problem with not normalized weights (not summing to 1) - this is important for custom weights. 
    //Probably it might be normalized in logic (be aware of 0,0,0 weights though)
    //length = 3
    static final ParameterName<float[]> RGB_WEIGHTS = new ParameterName("RGB weights");

    static final ParameterName<ReadImage.LayeringMode> LAYERING_MODE = new ParameterName("Layering mode");
    
    static final ParameterName<float[]> AFFINE_SCALING_FACTORS = new ParameterName("Scaling factors for affine");
    
    static final ParameterName<String[]> AFFINE_UNITS = new ParameterName("Units for affine");
    
    static final ParameterName<Integer> META_FIELD_RANK = new ParameterName("META rank");

    static final ParameterName<Boolean> META_FIELD_TIME_DEPENDENT = new ParameterName("META time dependent");
    
}
