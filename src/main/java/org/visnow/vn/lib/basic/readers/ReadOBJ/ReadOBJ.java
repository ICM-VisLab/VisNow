/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadOBJ;

import org.jogamp.java3d.loaders.IncorrectFormatException;
import org.jogamp.java3d.loaders.ParsingErrorException;
import org.jogamp.java3d.loaders.Scene;
import org.jogamp.java3d.loaders.objectfile.ObjectFile;
import org.jogamp.java3d.utils.geometry.GeometryInfo;
import java.io.FileNotFoundException;
import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedTriangleArray;
import org.jogamp.java3d.Shape3D;
import javax.swing.SwingUtilities;
import org.jogamp.java3d.BranchGroup;
import static org.jogamp.java3d.GeometryArray.TEXTURE_COORDINATE_2;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.objects.GeometryObject;
import org.visnow.vn.geometries.parameters.RenderingParams;
import static org.visnow.vn.lib.basic.readers.ReadOBJ.ReadOBJShared.READOBJ_PATH;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNGeometryObject;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.Pair;
import org.visnow.vn.system.main.VisNow;

/**
 * @author theki
 */
public class ReadOBJ extends OutFieldVisualizationModule
{

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ReadOBJ.class);
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI computeUI;
    private GeometryObject outScene;

    /**
     * Creates a new instance of Convolution
     */
    public ReadOBJ()
    {

        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startIfNotInQueue();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {

            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return ReadOBJShared.createDefaultParametersAsList().toArray(new Parameter[]{});
    }

    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {

        Parameters p = parameters.getReadOnlyClone();
        Pair geom = createOBJGeometry(p.get(READOBJ_PATH));
        if (geom != null) {
            outIrregularField = (IrregularField) geom.getE();
            outScene = (GeometryObject) geom.getF();
        } else {
            outIrregularField = null;
            outScene = null;
        }
        if (outIrregularField != null) {
            setOutputValue("outField", new VNIrregularField(outIrregularField));
        } else {
            setOutputValue("outField", null);
        }
        outField = outIrregularField;
        prepareOutputGeometry();
        irregularFieldGeometry.getFieldDisplayParams().getRenderingParams().setShadingMode(RenderingParams.FLAT_SHADED);

        if (outScene != null) {
            if (outField != null) {
                outScene.setExtents(outField.getPreferredExtents());
            }
            setOutputValue("outScene", new VNGeometryObject(outScene));
        } else {
            setOutputValue("outScene", null);
        }
        show();

    }
    
    private boolean arrayHasUV(IndexedTriangleArray ita) {
        return ita.getTexCoordSetCount() > 0 && ((ita.getVertexFormat() | TEXTURE_COORDINATE_2) != 0);
    }

    public Pair<IrregularField, GeometryObject> createOBJGeometry(String path)
    {
        if (path == null) {
            LOGGER.info("path == null");
            return null;
        }

        ObjectFile a = new ObjectFile();
        a.setFlags(ObjectFile.RESIZE | ObjectFile.TRIANGULATE | ObjectFile.STRIPIFY);

        Scene s = null;

        try {
            s = a.load(path);
        } catch (FileNotFoundException ex) {
            LOGGER.info("File " + path + " not found");
            VisNow.get().userMessageSend(this, "Error loading obj", "File " + path + " does not exist.", org.visnow.vn.system.utils.usermessage.Level.ERROR);
            return null;
        } catch (IncorrectFormatException ex) {
            LOGGER.info("Incorrect format of obj");
            VisNow.get().userMessageSend(this, "Incorrect format of obj", "Change format of: " + path + " ", org.visnow.vn.system.utils.usermessage.Level.ERROR);
            return null;
        } catch (ParsingErrorException ex) {
            LOGGER.info("Incorrect parsing format of obj");
            VisNow.get().userMessageSend(this, "Incorrect parsing format of obj", "Change format of: " + path + " ", org.visnow.vn.system.utils.usermessage.Level.ERROR);
            return null;
        }

        if (s == null) {
            VisNow.get().userMessageSend(this, "Incorrect format of obj", "Change format of: " + path + " ", org.visnow.vn.system.utils.usermessage.Level.ERROR);
            return null;
        }

        BranchGroup sceneGroup;
        try {
            sceneGroup = s.getSceneGroup();
        } catch (Exception ex) {
            LOGGER.info("Incorrect parsing format of obj");
            VisNow.get().userMessageSend(this, "Incorrect parsing format of obj", "Change format of: " + path + " ", org.visnow.vn.system.utils.usermessage.Level.ERROR);
            return null;
        }

        if (sceneGroup == null) {
            VisNow.get().userMessageSend(this, "Incorrect format of obj", "Change format of: " + path + " ", org.visnow.vn.system.utils.usermessage.Level.ERROR);
            return null;
        }

        int nNodes = 0;
        int nIndices = 0;
        boolean hasUV = false;
        for (int i = 0; i < sceneGroup.numChildren(); i++) {
            GeometryInfo geomInfo;
            try {
                geomInfo = new GeometryInfo((GeometryArray) ((Shape3D) sceneGroup.getChild(i)).getGeometry());
            } catch (Exception ex) {
                LOGGER.info("Incorrect parsing format of obj");
                VisNow.get().userMessageSend(this, "Incorrect parsing format of obj", "Change format of: " + path + " ", org.visnow.vn.system.utils.usermessage.Level.ERROR);
                return null;
            }
            geomInfo.convertToIndexedTriangles();
            IndexedTriangleArray ita = (IndexedTriangleArray) geomInfo.getIndexedGeometryArray();
            hasUV = hasUV || arrayHasUV(ita);
            nIndices += ita.getIndexCount();
            nNodes += ita.getVertexCount();
        }
        IrregularField outFld = new IrregularField(nNodes);
        FloatLargeArray coords = new FloatLargeArray(3 * nNodes, false);
        int[] indices = new int[nIndices];
        FloatLargeArray u = hasUV ? new FloatLargeArray(nNodes, true) : null;
        FloatLargeArray v = hasUV ? new FloatLargeArray(nNodes, true) : null;

        int offset = 0;
        int indOffset = 0;
        for (int i = 0; i < sceneGroup.numChildren(); i++) {
            GeometryInfo geomInfo = new GeometryInfo((GeometryArray) ((Shape3D) sceneGroup.getChild(i)).getGeometry());
            geomInfo.convertToIndexedTriangles();
            IndexedTriangleArray ita = (IndexedTriangleArray) geomInfo.getIndexedGeometryArray();
            boolean hasLocalUV = arrayHasUV(ita);
            int n = ita.getVertexCount();
            int count = ita.getIndexCount();
            int[] cells = new int[count];
            float[] coord = new float[n * 3];
            ita.getCoordinates(0, coord);
            ita.getCoordinateIndices(0, cells);
            LargeArrayUtils.arraycopy(coord, 0, coords, 3 * offset, coord.length);
            ita.getCoordinateIndices(0, cells);
            for (int j = 0; j < cells.length; j++) {
                indices[indOffset + j] += cells[j] + offset;
            }
            if (hasLocalUV) {
                float[] uvLocal = new float[2 * n];
                ita.getTextureCoordinates(0, 0, uvLocal);
                for (int j = 0; j < n; j++) {
                    u.setFloat(offset + j, uvLocal[2 * j]);
                    v.setFloat(offset + j, uvLocal[2 * j + 1]);
                }
            }
            offset += n;
            indOffset += count;
        }
        outFld.setCurrentCoords(coords);
        byte[] orientations = new byte[nIndices / 3];
        for (int j = 0; j < orientations.length; j++) {
            orientations[j] = 1;
        }
        CellArray ca = new CellArray(CellType.TRIANGLE, indices, orientations, null);
        CellSet cs = new CellSet("cells ");
        cs.setCellArray(ca);
        outFld.addCellSet(cs);
        FloatLargeArray temp1 = new FloatLargeArray(nNodes, false);
        for (long i = 0; i < nNodes; i++) {
            temp1.setFloat(i, i);
        }
        outFld.addComponent(DataArray.create(temp1, 1, "obj"));
        if (hasUV) {
            outFld.addComponent(DataArray.create(u, 1, "u"));
            outFld.addComponent(DataArray.create(v, 1, "v"));
        }

        GeometryObject scene = new GeometryObject("Scene");
        scene.addNode(sceneGroup);

        VisNow.get().userMessageSend(this, "File successfully loaded", "", org.visnow.vn.system.utils.usermessage.Level.INFO);
        return new Pair(outFld, scene);
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag())
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    computeUI.activateOpenDialog();
                }
            });
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

}
