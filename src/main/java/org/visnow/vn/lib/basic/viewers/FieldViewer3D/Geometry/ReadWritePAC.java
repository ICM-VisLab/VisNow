/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import org.visnow.jscic.IrregularField;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl), University of Warsaw, ICM
 *
 */
public class ReadWritePAC
{

    public static IrregularField readPAC(String filePath)
    {
        ArrayList<PointDescriptor> pts = new ArrayList<PointDescriptor>();
        ArrayList<ConnectionDescriptor> conn = new ArrayList<ConnectionDescriptor>();
        readPAC(filePath, pts, conn);
        return GeometryFieldConverter.pac2field(pts, conn, true, false, null);
    }

    public static void readPAC(String filePath, ArrayList<PointDescriptor> pds, ArrayList<ConnectionDescriptor> cds)
    {
        if (pds == null || cds == null || filePath == null)
            return;

        try {
            File f = new File(filePath);
            BufferedReader input = new BufferedReader(new FileReader(f));

            String line;

            line = input.readLine();
            if (line == null || (!line.equals("points and connections data file") && !line.equals("#PAC data file"))) {
                input.close();
                return;
            }

            line = input.readLine();
            if (line == null || !line.startsWith("points")) {
                input.close();
                return;
            }
            String[] tmp = line.split(" ");
            int nPoints = Integer.parseInt(tmp[1]);

            String[] names = new String[nPoints];
            int[][] pts = new int[nPoints][3];
            float[][] coords = new float[nPoints][3];
            int[] classes = new int[nPoints];

            for (int i = 0; i < nPoints; i++) {
                line = input.readLine();
                tmp = line.split("\t");
                names[i] = new String(tmp[0]);
                pts[i][0] = Integer.parseInt(tmp[1]);
                pts[i][1] = Integer.parseInt(tmp[2]);
                pts[i][2] = Integer.parseInt(tmp[3]);
                if (tmp.length >= 7) {
                    coords[i][0] = Float.parseFloat(tmp[4]);
                    coords[i][1] = Float.parseFloat(tmp[5]);
                    coords[i][2] = Float.parseFloat(tmp[6]);
                }
                if (tmp.length >= 8) {
                    classes[i] = Integer.parseInt(tmp[7]);
                } else {
                    classes[i] = -1;
                }
            }

            line = input.readLine();
            line = input.readLine();
            if (line == null || !line.startsWith("connections")) {
                input.close();
                return;
            }
            tmp = line.split(" ");
            int nConns = Integer.parseInt(tmp[1]);
            String[] cnames = new String[nConns];
            int[][] conns = new int[nConns][2];
            for (int i = 0; i < nConns; i++) {
                line = input.readLine();
                if (line == null) {
                    input.close();
                    return;
                }
                tmp = line.split("\t");
                cnames[i] = new String(tmp[0]);
                conns[i][0] = Integer.parseInt(tmp[1]);
                conns[i][1] = Integer.parseInt(tmp[2]);
            }
            input.close();

            for (int i = 0; i < nPoints; i++) {
                if (pts[i][0] == -1 || pts[i][1] == -1 || pts[i][2] == -1) {
                    pds.add(new PointDescriptor(names[i], null, coords[i], classes[i]));
                } else {
                    pds.add(new PointDescriptor(names[i], pts[i], coords[i], classes[i]));
                }
            }

            for (int i = 0; i < nConns; i++) {
                cds.add(new ConnectionDescriptor(cnames[i], pds.get(conns[i][0]), pds.get(conns[i][1])));
            }
        } catch (Exception ex) {
        }
    }

    public static void writePAC(String fileName, IrregularField inField, String infoString)
    {
        if (inField == null || fileName == null)
            return;

        ArrayList<PointDescriptor> pts = new ArrayList<PointDescriptor>();
        ArrayList<ConnectionDescriptor> conn = new ArrayList<ConnectionDescriptor>();
        GeometryFieldConverter.field2pac(inField, null, pts, conn);
        writePAC(fileName, pts, conn, infoString);
    }

    public static void writePAC(String fileName, ArrayList<PointDescriptor> pts, ArrayList<ConnectionDescriptor> conn, String infoString)
    {
        try {
            File f = new File(fileName);
            BufferedWriter output = new BufferedWriter(new FileWriter(f));

            //output.write("points and connections data file");
            output.write("#PAC data file");
            output.newLine();
            String tmp = "";
            int[] indices;
            float[] coords;
            output.append("points " + pts.size());
            output.newLine();
            for (int i = 0; i < pts.size(); i++) {
                indices = pts.get(i).getIndices();
                coords = pts.get(i).getWorldCoords();
                if (indices != null) {
                    tmp = "" + pts.get(i).getName() + "\t" +
                        indices[0] + "\t" + indices[1] + "\t" + indices[2] + "\t" +
                        String.format("%.6e\t%.6e\t%.6e\t", coords[0], coords[1], coords[2]) +
                        pts.get(i).getMembership() + "\t";
                } else {
                    tmp = "" + pts.get(i).getName() + "\t" +
                        "-1\t-1\t-1\t" +
                        String.format("%.6e\t%.6e\t%.6e\t", coords[0], coords[1], coords[2]) +
                        pts.get(i).getMembership() + "\t";
                }
                output.append(tmp);
                output.newLine();
            }
            output.newLine();

            output.append("connections " + conn.size());
            output.newLine();
            int i1, i2;
            for (int i = 0; i < conn.size(); i++) {
                i1 = pts.indexOf(conn.get(i).getP1());
                i2 = pts.indexOf(conn.get(i).getP2());
                //tmp = String.format("%s\t%d\t%d\t%.6e",conn.get(i).getName(),i1,i2,conn.get(i).getLength());
                tmp = "" + conn.get(i).getName() + "\t" + i1 + "\t" + i2 + "\t" + String.format("%.6e", conn.get(i).getLength());
                output.append(tmp);
                output.newLine();
            }
            output.newLine();

            if (infoString != null) {
                output.append(infoString);
                output.newLine();
            }

            output.close();
        } catch (Exception ex) {
        }
    }
    
}
