/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.PlanarSlice;

import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;
import org.visnow.vn.lib.utils.probeInterfaces.ProbeDisplay.Position;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class PlanarSliceShared
{
    public static enum ProbeType {GEOMETRIC, INDEX};
    public static final String TYPE_STRING = "Type"; 
    public static final String ADD_SLICE_AREA_STRING = "Add slice area";
    public static final String DEL_SLICE_STRING = "Remove slice";
    public static final String CLEAR_SLICES_STRING = "Clear slices";
    public static final String SLICE_RESOLUTION_STRING = "resolution";
    public static final String SLICE_BANNERS_SCALE_STRING = "slice banners scale";
    public static final String SLICE_POSITION_STRING = "slice position";
    public static final String POINTER_LINE_STRING = "pointer line";
    public static final String ORDER_SLICES_STRING = "order slices";
    public static final String REFRESH_SLICES_STRING = "refresh slices";
    public static final String SLICE_TITLE_STRING = "slice title";
    public static final String TITLE_SIZE_STRING = "font size";
    public static final String TIME_STRING = "time";
    public static final String SHOW_ISO_STRING = "show isolines";
    public static final String ISO_COMPONENT_STRING = "isolines component";
    public static final String ISO_THRESHOLDS_STRING = "isoline thresholds";
    
    static final ParameterName<ProbeType>         TYPE = new ParameterName(TYPE_STRING); 
    static final ParameterName<Boolean>           ADD_SLICE_AREA = new ParameterName(ADD_SLICE_AREA_STRING);
    static final ParameterName<Boolean>           DEL_SLICE = new ParameterName(DEL_SLICE_STRING);
    static final ParameterName<Boolean>           CLEAR_SLICES = new ParameterName(CLEAR_SLICES_STRING);
    static final ParameterName<Integer>           SLICE_RESOLUTION = new ParameterName(SLICE_RESOLUTION_STRING);
    static final ParameterName<Boolean>           ORDER_SLICES = new ParameterName(ORDER_SLICES_STRING);
    static final ParameterName<Float>             SLICE_BANNERS_SCALE = new ParameterName(SLICE_BANNERS_SCALE_STRING);
    static final ParameterName<Position>          SLICE_POSITION = new ParameterName(SLICE_POSITION_STRING);
    static final ParameterName<Boolean>           POINTER_LINE = new ParameterName(POINTER_LINE_STRING);
    static final ParameterName<Boolean>           REFRESH_SLICES = new ParameterName(REFRESH_SLICES_STRING);
    static final ParameterName<Float>             TIME = new ParameterName(TIME_STRING);
    static final ParameterName<Float>             TITLE_SIZE = new ParameterName(TITLE_SIZE_STRING);
    static final ParameterName<Boolean>           SHOW_ISOLINES = new ParameterName(SHOW_ISO_STRING);
    static final ParameterName<ComponentFeature>  ISO_COMPONENT = new ParameterName(ISO_COMPONENT_STRING);
    static final ParameterName<float[]>           ISO_THRESHOLDS = new ParameterName(ISO_THRESHOLDS_STRING);
    static final ParameterName<String>            SLICE_TITLE = new ParameterName(SLICE_TITLE_STRING);
}
