/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.testdata.VNLogo;

import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import static org.apache.commons.math3.util.FastMath.*;
import org.apache.log4j.Logger;
import org.visnow.jscic.cells.CellType;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.objects.IrregularFieldGeometry;
import static org.visnow.vn.lib.basic.testdata.VNLogo.VNLogoShared.*;

/**
 * @author Krzysztof Nowinski (know@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class VNLogo extends OutFieldVisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(VNLogo.class);

    protected GUI computeUI = null;
    protected boolean fromUI = false;
    public static OutputEgg[] outputEggs = null;
    protected int level = 10;
    protected int nPoints = 4 + 10 * level;
    protected FloatLargeArray coords = new FloatLargeArray(3 * (long)nPoints, false);
    protected float[] v = new float[3 * nPoints];
    protected float[] data = new float[nPoints];
    protected float[] data0 = new float[nPoints];

    /**
     * Creates a new instance of TestGeometryObject
     */
    public VNLogo()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name.equalsIgnoreCase("Number of points"))
                    startAction();
                else
                    updateCoords(parameters);
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParameters(parameters);
            }
        });
        ui.addComputeGUI(computeUI);
        setPanel(ui);
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(POINT_NUMBER, 50),
            new Parameter<>(ASPECT, 0.3f),
            new Parameter<>(MESH_WIDTH, 0.3f),
            new Parameter<>(DISTANCE, 0.3f)
        };
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    private void updateCoords(Parameters p)
    {
        float aspect = p.get(ASPECT);
        float width = p.get(MESH_WIDTH);
        float distance = p.get(DISTANCE);

        float d = (float) PI / level;
        int k = 0;
        for (int i = 0; i <= 2 * level; i++, k += 2) {
            float cosPsi = (float) cos(i * d);
            float sinPsi = (float) sin(i * d);
            coords.setFloat(3 * k, i * d * aspect);
            coords.setFloat(3 * k + 3, i * d * aspect + width);
            coords.setFloat(3 * k + 1, cosPsi);
            coords.setFloat(3 * k + 4, cosPsi);
            coords.setFloat(3 * k + 2, sinPsi);
            coords.setFloat(3 * k + 5, sinPsi);
        }
        for (int i = -level; i <= 2 * level; i++, k += 2) {
            float cosPsi = (float) cos(i * d);
            float sinPsi = (float) sin(i * d);
            coords.setFloat(3 * k, i * d * aspect + distance + width);
            coords.setFloat(3 * k + 3, i * d * aspect + distance + 2 * width);
            coords.setFloat(3 * k + 1, cosPsi);
            coords.setFloat(3 * k + 4, cosPsi);
            coords.setFloat(3 * k + 2, sinPsi);
            coords.setFloat(3 * k + 5, sinPsi);
        }
        if (outIrregularField != null && irregularFieldGeometry != null) {
            outIrregularField.setCurrentCoords(coords);
            irregularFieldGeometry.updateCoords();
        }
    }
    
    private void createMesh(Parameters p)
    {
        level = p.get(POINT_NUMBER) / 5;

        nPoints = 4 + 10 * level;
        outIrregularField = new IrregularField(nPoints);
        coords = new FloatLargeArray((long)nPoints * 3, false);
        v = new float[nPoints * 3];
        data = new float[nPoints];
        data0 = new float[nPoints];
        float d = (float) PI / level;
        int k = 0;
        for (int i = 0; i <= 2 * level; i++, k += 2) {
            float cosPsi = (float) cos(i * d);
            float sinPsi = (float) sin(i * d);
            v[3 * k] = v[3 * k + 3] = 0;
            v[3 * k + 1] = cosPsi;
            v[3 * k + 4] = -cosPsi;
            v[3 * k + 2] = sinPsi;
            v[3 * k + 5] = -sinPsi;
            data[k] = data[k + 1] = 0;
            data0[k] = data0[k + 1] = cosPsi;
        }
        for (int i = -level; i <= 2 * level; i++, k += 2) {
            float cosPsi = (float) cos(i * d);
            float sinPsi = (float) sin(i * d);
            v[3 * k] = v[3 * k + 3] = 0;
            v[3 * k + 1] = cosPsi;
            v[3 * k + 4] = -cosPsi;
            v[3 * k + 2] = sinPsi;
            v[3 * k + 5] = -sinPsi;
            data[k] = data[k + 1] = 1;
            data0[k] = data0[k + 1] = cosPsi;
        }
        updateCoords(p);
        outIrregularField.removeComponents();
        outIrregularField.addComponent(DataArray.create(data, 1, "z"));
        outIrregularField.addComponent(DataArray.create(data0, 1, "x"));
        outIrregularField.addComponent(DataArray.create(v, 3, "v"));
        int nQuads = 5 * level;
        float[] data1 = {0, 1};
        int[] cells = new int[4 * nQuads];
        int[] cellDataIndices = new int[nQuads];
        int l = 0;
        k = 0;
        for (int i = 0; i < 2 * level; i++, k++, l++) {
            cells[4 * k] = 2 * l;
            cells[4 * k + 1] = 2 * l + 1;
            cells[4 * k + 2] = 2 * l + 3;
            cells[4 * k + 3] = 2 * l + 2;
            cellDataIndices[k] = 0;
        }
        l += 1;
        for (int i = 0; i < 3 * level; i++, k++, l++) {
            cells[4 * k] = 2 * l;
            cells[4 * k + 1] = 2 * l + 1;
            cells[4 * k + 2] = 2 * l + 3;
            cells[4 * k + 3] = 2 * l + 2;
            cellDataIndices[k] = 1;
        }
        CellArray ca = new CellArray(CellType.QUAD, cells, null, cellDataIndices);
        CellSet cs = new CellSet();
        cs.setCellArray(ca);
        cs.addComponent(DataArray.create(data1, 1, "c"));
        cs.generateDisplayData(coords);
        outIrregularField.addCellSet(cs);
    }

    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    /**
     * Method called on module init and when output geometry object structure is changed
     */
    public void onActive()
    {
        Parameters p;
        synchronized (parameters) {
            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, false, false);
        createMesh(p);
        setOutputValue("outField", new VNIrregularField(outIrregularField));
        outField = outIrregularField;
        prepareOutputGeometry();
        show();
    }
}
