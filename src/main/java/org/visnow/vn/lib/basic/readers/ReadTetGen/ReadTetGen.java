/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadTetGen;

import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.gui.widgets.FileErrorFrame;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ReadTetGen extends OutFieldVisualizationModule
{

    protected GUI computeUI = null;
    protected Params params;
    protected String fileName = " ";
    protected Cell[] stdCells = new Cell[Cell.getNProperCellTypes()];
    protected boolean ignoreUI = false;
    private String line = "";
    private LineNumberReader r;

    /**
     * Creates a new instance of CreateGrid
     */
    public ReadTetGen()
    {
        for (int i = 0; i < stdCells.length; i++)
            stdCells[i] = Cell.createCell(CellType.getType(i), 3, new int[CellType.getType(i).getNVertices()], (byte)1);
        parameters = params = new Params();
        params.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
            }
        });
        computeUI.setParams(params);
        ui.addComputeGUI(computeUI);
        setPanel(ui);
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    private void nextLine(LineNumberReader r)
    {
        line = "";
        try {
            while (line != null && (line.isEmpty() || line.startsWith("#")))
                line = r.readLine();
        } catch (IOException ex) {
        }
    }

    public static OutputEgg[] outputEggs = null;

    @Override
    public void onActive()
    {
        String[] items;
        String fName = "";
        LineNumberReader r1 = null;
        if (params.getFileName() != null && params.getFileName().length() > 0) {
            String tmpName = params.getFileName();
            fileName = tmpName.substring(0, tmpName.lastIndexOf("."));
            try {
                fName = fileName + ".smesh";
                r = new LineNumberReader(new FileReader(fName));
                while (line.isEmpty()) {
                    nextLine(r);
                    line = line.trim().replaceFirst("#.*", "");
                }
                items = line.split("\\s+");
                int nNodes = Integer.parseInt(items[0]);
                int nSpace = Integer.parseInt(items[1]);
                int nData = Integer.parseInt(items[2]);
                System.out.println(nNodes + "  " + nData);
                if (nNodes == 0) {
                    r1  = new LineNumberReader(new FileReader(fileName + ".node"));
                    while (line.isEmpty()) {
                        nextLine(r1);
                        line = line.trim().replaceFirst("#.*", "");
                    }
                    items = line.split("\\s+");
                    nNodes = Integer.parseInt(items[0]);
                    nSpace = Integer.parseInt(items[1]);
                    nData = Integer.parseInt(items[2]);
                }
                FloatLargeArray coords = new FloatLargeArray(3 * (long)nNodes, false);
                float[][] data = null;
                if (nData > 0)
                    data = new float[nData][nNodes];
                for (int i = 0; i < nNodes; i++) {
                    if (r1 == null)
                        nextLine(r);
                    else
                        nextLine(r1);
                    items = line.split("\\s+");
                    for (int j = 0; j < nSpace; j++)
                        coords.setFloat(3 * i + j, Float.parseFloat(items[j + 1]));
                    if (nData > 0)
                        for (int j = 0; j < nData; j++)
                            data[j][i] = Float.parseFloat(items[j + nSpace + 1]);
                }
                if (r1 != null)
                    r1.close();
                outIrregularField = new IrregularField(nNodes);
                outIrregularField.setCurrentCoords(coords);
                if (nData > 0)
                    for (int i = 0; i < nData; i++)
                        outIrregularField.addComponent(DataArray.create(data[i], 1, "data" + i));
                r.close();

                fName = fileName + ".ele";
                line = "";
                r = new LineNumberReader(new FileReader(fName));
                while (line.isEmpty()) {
                    nextLine(r);
                    line = line.replaceFirst("#.*", "");
                }
                items = line.split("\\s+");
                int nCells = Integer.parseInt(items[0]);
                int nCellData = Integer.parseInt(items[2]);
                System.out.println(nCells + "  " + nCellData);
                int[] nodes = new int[4 * nCells];
                float[][] cellData = null;
                int[] indices = new int[nCells];
                byte[] orientations = new byte[nCells];
                if (nCellData > 0)
                    cellData = new float[nCellData][nCells];
                for (int i = 0; i < nCells; i++) {
                    nextLine(r);
                    items = line.split("\\s+");
                    for (int j = 0; j < 4; j++)
                        nodes[4 * i + j] = Integer.parseInt(items[j + 1]) - 1;
                    if (nCellData > 0)
                        for (int j = 0; j < nCellData; j++)
                            cellData[j][i] = Float.parseFloat(items[j + 5 + j]);
                    indices[i] = i;
                    orientations[i] = 1;
                }
                CellArray ca = new CellArray(CellType.TETRA, nodes, orientations, indices);
                CellSet cs = new CellSet("tetras");
                cs.addCells(ca);
                if (nCellData > 0)
                    for (int i = 0; i < nCellData; i++)
                        cs.addComponent(DataArray.create(cellData[i], 1, "cell data" + i));
                cs.generateDisplayData(coords);
                outIrregularField.addCellSet(cs);
                r.close();
            } catch (Exception e) {
                FileErrorFrame.display("invalid input line", fName, r.getLineNumber(), e);
                System.out.println(fName + ":" + r.getLineNumber());
                System.out.println(line);
                return;
            }

            if (outIrregularField == null)
                return;
            computeUI.setFieldDescription(outIrregularField.description());
            setOutputValue("outField", new VNIrregularField(outIrregularField));
            outField = outIrregularField;
            if (params.isShow()) {
                prepareOutputGeometry();
                show();
            }
        }
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag())
            computeUI.activateOpenDialog();
    }

}
