/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Isolines;

import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class IsolinesParams extends Parameters
{

    protected static final String COMPONENT = "component";
    protected static final String THRESHOLDS = "thresholds";
    protected static final String CELL_SETS = "activeCellSets";
    protected static final String BOUNDARY_ISOLINES = "boundaryIsolines";
    protected static final String QUALITY = "estimate quality";

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Integer>(COMPONENT, ParameterType.dependent, 0),
        new ParameterEgg<float[]>(THRESHOLDS, ParameterType.dependent, null),
        new ParameterEgg<boolean[]>(CELL_SETS, ParameterType.dependent, null),
        new ParameterEgg<Boolean>(BOUNDARY_ISOLINES, ParameterType.dependent, false),
        new ParameterEgg<Boolean>(QUALITY, ParameterType.dependent, false),};

    public IsolinesParams()
    {
        super(eggs);
        setValue(THRESHOLDS, new float[]{127});
        setValue(CELL_SETS, new boolean[]{true});
        setValue(QUALITY, false);
    }

    public int getComponent()
    {
        return (Integer) getValue(COMPONENT);
    }

    public void setComponent(int component)
    {
        setValue(COMPONENT, component);
        fireStateChanged();
    }

    public float[] getThresholds()
    {
        return (float[]) getValue(THRESHOLDS);
    }

    public void setThresholds(float[] thresholds)
    {
        setValue(THRESHOLDS, thresholds);
        fireStateChanged();
    }

    public boolean isActiveCellSet(int i)
    {
        boolean[] aCS = (boolean[]) getValue(CELL_SETS);
        if (i < 0 || aCS == null || i >= aCS.length)
            return false;
        return aCS[i];
    }

    public boolean[] getActiveCellSets()
    {
        return (boolean[]) getValue(CELL_SETS);
    }

    public void setActiveCellSets(boolean[] aCS)
    {
        setValue(CELL_SETS, aCS);
        fireStateChanged();
    }

    public boolean drawOnBoundary()
    {
        return (Boolean) getValue(BOUNDARY_ISOLINES);
    }
    
    public void setDrawOnBoundary(boolean bDraw)
    {
        setValue(BOUNDARY_ISOLINES, bDraw);
        fireStateChanged();
    }
    
    public boolean isEstimateQuality()
    {
        return ((Boolean) getValue(QUALITY));
    }
    
    public void setEstimateQuality(boolean quality)
    {
        setValue(QUALITY, quality);
        fireStateChanged();
    }
}
