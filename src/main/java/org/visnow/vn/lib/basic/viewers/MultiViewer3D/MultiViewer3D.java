/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.MultiViewer3D;

import java.awt.Frame;
import java.util.Vector;
import javax.swing.JFrame;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.LinkFace;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.geometries.objects.GeometryObject;
import org.visnow.vn.geometries.viewer3d.Display3DPanel;
import org.visnow.vn.geometries.viewer3d.controls.Display3DControlsPanel;
import org.visnow.vn.gui.widgets.VisNowFrame;
import org.visnow.vn.lib.types.VNGeometryObject;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class MultiViewer3D extends ModuleCore
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(MultiViewer3D.class);
    private VisNowFrame frame;
    private MultiviewInternalFrame internalFrame = null;
    private Display3DPanel displayPanel = null;
    private Display3DControlsPanel controlsPanel = null;
    private GUI ui = null;

    /**
     * Creates a new instance of Viewer3D
     */
    public MultiViewer3D()
    {
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                frame = new VisNowFrame();
                frame.setTitle("VisNow OrthoViewer3D");
                internalFrame = new MultiviewInternalFrame();
                frame.add(internalFrame);
 
                ui = new GUI();
                ui.setViewerModule(MultiViewer3D.this);
                //            displayPanel = internalFrame.getDisplayPanel();
                //            controlsPanel = new Display3DControlsPanel(displayPanel);
                //            ui.setControlsPanel(controlsPanel);
                //            displayPanel.setControlsFrame(null);
                //            displayPanel.setControlsPanel(controlsPanel);
                internalFrame.setVisible(true);
                
                frame.setBounds(100, 100, 1200, 800);
                frame.setVisible(true);
            }
        });
        setPanel(ui);
    }
    
    public MultiviewInternalFrame getInternalFrame()
    {
        return this.internalFrame;
    }

    public VisNowFrame getFrame()
    {
        return this.frame;
    }

    @Override
    public boolean isViewer()
    {
        return true;
    }

    void showWindow()
    {
        frame.setVisible(true);
        frame.setExtendedState(Frame.NORMAL);
    }

    void setTransientControlsFrame(JFrame transientControlsFrame)
    {
        displayPanel.setTransientControlsFrame(transientControlsFrame);
    }

    @Override
    public void onDelete()
    {
        frame.dispose();
    }

    @Override
    public void onInputDetach(LinkFace link)
    {
        onActive();
    }

    @Override
    public void onInputAttach(LinkFace link)
    {
        onActive();
    }

    @Override
    public void onActive()
    {

        //      internalFrame.getDisplayPanel().setPostRenderSilent(true);
        if (!frame.isVisible())
            frame.setVisible(true);
        internalFrame.getUniBuilder().getScene().clearAllGeometry();
        Vector ins = getInputValues("inObject");
        for (Object obj : ins)
            if ((VNGeometryObject) obj != null) {
                GeometryObject geomObj = ((VNGeometryObject) obj).getGeometryObject();
                if (geomObj != null) {
                    geomObj.detach();
                    internalFrame.getUniBuilder().getScene().addGeomObject(geomObj);
                }
            }
        //      internalFrame.getDisplayPanel().setPostRenderSilent(false);
        //      //if(internalFrame.getDisplayPanel().isStoringFrames())
        //      if(internalFrame.getDisplayPanel().isWaitingForExternalTrigger())
        //          internalFrame.getDisplayPanel().forceRender();

        //window.repaint();
    }

    @Override
    public void onInitFinished()
    {
        updateFrameName();
    }

    @Override
    public void onNameChanged()
    {
        updateFrameName();
    }

    private void updateFrameName()
    {
        if (frame != null)
            frame.setTitle("VisNow OrthoViewer3D - " + this.getApplication().getTitle() + " - " + this.getName());
    }

}
