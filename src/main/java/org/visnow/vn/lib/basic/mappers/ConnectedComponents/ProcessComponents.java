/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.ConnectedComponents;

import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import static org.visnow.vn.lib.basic.mappers.ConnectedComponents.ConnectedComponentsShared.*;
import org.visnow.vn.lib.utils.numeric.HeapSort;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class ProcessComponents
{

    private IrregularField inField, outField;
    private int[][] cellSetComponents = null;
    private int[][] cellSetComponentSizes = null;
    private int[] cellSetNComponents = null;

    private static Logger logger = Logger.getLogger(ProcessComponents.class);
    private ProgressAgent progressAgent;

    public ProcessComponents()
    {
    }

    public void setInField(IrregularField inField, ProgressAgent progressAgent)
    {
        this.inField = inField;
        this.progressAgent = progressAgent;
        cellSetComponents = new int[inField.getNCellSets()][(int) inField.getNNodes()]; //holder for component indices: cellSetComponents[i][j] = k iff node j belongs to kth component of cell set i
        cellSetComponentSizes = new int[inField.getNCellSets()][]; //holder for component sizes:   cellSetComponentSizes[i][2*j] is number of vertices in jth component of cell set i,
        //for sorting purposes:         cellSetComponentSizes[i][2*j+1] is component number
        cellSetNComponents = new int[inField.getNCellSets()]; //holder for component numbers: cellSetNComponents[i] is number of components of cell set i
        for (int i = 0; i < inField.getNCellSets(); i++)
            split(i);
    }

    private void addNeighbors(int[] pNeighb, int[] neighbInd, CellArray cellArray)
    {
        if (cellArray == null && cellArray.getType().getNVertices() <= 1) 
            return;
        int n = cellArray.getType().getNVertices();
        int[] nodes = cellArray.getNodes();
        for (int k = 0; k < nodes.length; k += n) 
            for (int i = k; i < k + n; i++) {
                int i0 = nodes[i];
                for (int j = k; j < k + n; j++) 
                    if (i != j) {
                        int i1 = nodes[j];
                        for (int l = neighbInd[i0]; l < neighbInd[i0 + 1]; l++) {
                            if (pNeighb[l] == i1)
                                break;
                            if (pNeighb[l] == -1) {
                                pNeighb[l] = i1;
                                break;
                            }
                        }
                    }
            }
    }

    private void split(int nSet)
    {
        int[] neighb;
        int[] neighbInd;
        int n = 0, k0;
        CellSet cs = inField.getCellSet(nSet);

        neighbInd = new int[(int) inField.getNNodes() + 1];
        for (int i = 0; i < neighbInd.length; i++)
            neighbInd[i] = 0;
        
        for (CellArray cellArray : cs.getCellArrays()) 
            if (cellArray != null && cellArray.getType().getNVertices() > 1) {
                int k = cellArray.getType().getNVertices() - 1;
                int[] nodes = cellArray.getNodes();
                for (int i = 0; i < nodes.length; i++)
                    neighbInd[nodes[i]] += k;
            }

        int k = 0;
        for (int i = 0; i < neighbInd.length; i++) {
            int j = k + neighbInd[i];
            neighbInd[i] = k;
            k = j;
        }

        int[] pNeighb = new int[k];

        for (int i = 0; i < pNeighb.length; i++)
            pNeighb[i] = -1;
        
        for (CellArray cellArray : cs.getCellArrays()) 
            if (cellArray != null && cellArray.getType().getNVertices() > 1) {
                addNeighbors(pNeighb, neighbInd, cellArray);
            }

        k = 0;
        for (int i = 0; i < pNeighb.length; i++)
            if (pNeighb[i] != -1)
                k += 1;
        neighb = new int[k];

        k = 0;
        for (int i = 0; i < neighbInd.length - 1; i++) {
            k0 = k;
            for (int j = neighbInd[i]; j < neighbInd[i + 1]; j++) {
                if (pNeighb[j] == -1)
                    break;
                neighb[k] = pNeighb[j];
                k += 1;
            }
            neighbInd[i] = k0;
        }

        neighbInd[neighbInd.length - 1] = k;
        pNeighb = null;
        int nNodes = (int) inField.getNNodes();
        int[] components = cellSetComponents[nSet];
        int[] stack = new int[nNodes];
        int stackSize = -1;
        for (int i = 0; i < nNodes; i++)
            components[i] = -1;
        int comp = 0;
        for (int seed = 0; seed < components.length; comp++) {
            components[seed] = comp;
            stackSize = 0;
            stack[stackSize] = seed;
            while (stackSize >= 0) {
                int current = stack[stackSize];
                stackSize -= 1;
                components[current] = comp;
                for (int j = neighbInd[current]; j < neighbInd[current + 1]; j++) {
                    k = neighb[j];
                    if (components[k] == -1) {
                        stackSize += 1;
                        stack[stackSize] = k;
                        components[k] = comp;
                    }
                }
            }
            while (seed < nNodes && components[seed] != -1)
                seed += 1; //looking for first node not yet assigned to any component
        }
        stack = null;
        cellSetNComponents[nSet] = comp;
        cellSetComponentSizes[nSet] = new int[2 * comp];
        for (int i = 0; i < comp; i++) {
            cellSetComponentSizes[nSet][2 * i] = 0;
            cellSetComponentSizes[nSet][2 * i + 1] = i;
        }
        for (int i = 0; i < nNodes; i++)
            cellSetComponentSizes[nSet][2 * components[i]] += 1;
        HeapSort.sort(cellSetComponentSizes[nSet], 2, false);
    }

    public IrregularField buildOutput(Parameters p)
    {
        int nSeparateComponents = p.get(SEPARATE_COMPONENTS);
        outField = inField.cloneShallow();
        outField.removeComponents();
        outField.getCellSets().clear();
        int[] maxComponentIndices = new int[cellSetComponentSizes.length]; //maxComponentIndices[i] is the index of largest component not yet assigned to output
        for (int i = 0; i < maxComponentIndices.length; i++)
            maxComponentIndices[i] = 0;
        orderingLoop:
        for (int nSeparate = 0; nSeparate < nSeparateComponents; nSeparate++) {
            progressAgent.increase();
            int maxComponentIndex = -1;
            int maxComponentSize = p.get(MIN_COMPONENT_SIZE);
            for (int nSet = 0; nSet < cellSetComponentSizes.length; nSet++)
                if (maxComponentIndices[nSet] < cellSetComponentSizes[nSet].length / 2 &&
                        cellSetComponentSizes[nSet][2 * maxComponentIndices[nSet]] > maxComponentSize) {
                    maxComponentIndex = nSet;
                    maxComponentSize = cellSetComponentSizes[nSet][maxComponentIndices[nSet]];
                }
            if (maxComponentIndex == -1)
                break orderingLoop;
            int maxComponentMark = cellSetComponentSizes[maxComponentIndex][2 * maxComponentIndices[maxComponentIndex] + 1];
            maxComponentIndices[maxComponentIndex] += 1;
            CellSet inCS = inField.getCellSet(maxComponentIndex);
            String prefix = inField.getNCellSets() > 1 ? inCS.getName() + "_" : "";
            CellSet outCS = new CellSet(prefix + "cmp_" + nSeparate);
            for (DataArray inComponent : inCS.getComponents())
                outCS.addComponent(inComponent.cloneShallow());
            for (CellArray inCellArray : inCS.getCellArrays()) {
                if (inCellArray == null || inCellArray.getNCells() < 1)
                    continue;
                int inNCells          = inCellArray.getNCells();
                int nCellNodes        = inCellArray.getNCellNodes();
                int[] inNodes         = inCellArray.getNodes();
                int[] inIndices       = inCellArray.getDataIndices();
                byte[] inOrientations = inCellArray.getOrientations();
                int nCells = 0;
                for (int i = 0; i < inNodes.length; i += nCellNodes)
                    if (cellSetComponents[maxComponentIndex][inNodes[i]] == maxComponentMark)
                        nCells += 1;
                if (nCells == 0)
                    continue;
                int[] outNodes         = new int[nCellNodes * nCells];
                int[] outIndices       = inIndices == null ? null : new int[nCells];
                byte[] outOrientations = new byte[nCells];
                for (int i = 0, k = 0; i < inNCells; i ++)
                    if (cellSetComponents[maxComponentIndex][inNodes[nCellNodes * i]] == maxComponentMark) {
                        System.arraycopy(inNodes, nCellNodes * i, outNodes, nCellNodes * k, nCellNodes);
                        outOrientations[k] = inOrientations[i];
                        if (inIndices != null)
                            outIndices[k] = inIndices[i];
                        k += 1;
                    }
                outCS.setCellArray(new CellArray(inCellArray.getType(), outNodes, outOrientations, outIndices));
            }
            outCS.generateDisplayData(outField.getCurrentCoords());
            outField.addCellSet(outCS);
        }

        int[] componentNumber = new int[(int) inField.getNNodes()];
        for (int i = 0; i < componentNumber.length; i++)
            componentNumber[i] = 0;
        for (int i = 0; i < outField.getNCellSets(); i++) {
            CellSet cs = outField.getCellSet(i);
            for (int j = 0; j < cs.getBoundaryCellArrays().length; j++) {
                CellArray cArr = cs.getBoundaryCellArray(CellType.getType(j));
                if (cArr != null && cArr.getNodes() != null) {
                    int[] cN = cArr.getNodes();
                    for (int k = 0; k < cN.length; k++)
                        componentNumber[cN[k]] = i;
                }
            }
            for (int j = 0; j < cs.getCellArrays().length; j++) {
                CellArray cArr = cs.getCellArray(CellType.getType(j));
                if (cArr != null && cArr.getNodes() != null) {
                    int[] cN = cArr.getNodes();
                    for (int k = 0; k < cN.length; k++)
                        componentNumber[cN[k]] = i;
                }
            }
        }
        outField.addComponent(DataArray.create(componentNumber, 1, "component index"));
        for (int i = 0; i < inField.getNComponents(); i++)
            outField.addComponent(inField.getComponent(i).cloneShallow());

        return outField;
    }

    public IrregularField getOutField()
    {
        return outField;
    }
}
