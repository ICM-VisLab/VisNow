/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.writers.WriteOBJ;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.geometries.objects.GeometryObject;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNGeometryObject;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class WriteOBJ extends OutFieldVisualizationModule/*ModuleCore*/

{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    private GUI ui = null;
    protected Params params;

    public WriteOBJ()
    {
        parameters = params = new Params();
        params.addParameterChangelistener(new ParameterChangeListener()
        {

            public void parameterChanged(String name)
            {
                if (name.equals("write")) {
                    if (outIrregularField != null && params.getFileName() != null && params.isWrite()) {
                        write();
                        params.setWrite(false);
                    }
                }
            }
        });
        params.addChangeListener(new ChangeListener()
        {

            public void stateChanged(ChangeEvent evt)
            {
                startAction();
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable()
        {

            public void run()
            {
                ui = new GUI();
                ui.setParams(params);
                setPanel(ui);
            }
        });

    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null) {
            return;
        }
        outIrregularField = ((VNIrregularField) getInputFirstValue("inField")).getField();
        //        GeometryObject inObject = ((VNGeometryObject) getInputFirstValue("inObject")).getGeometryObject();
    }

    protected void write()
    {
        try {
            File f = new File(params.getFileName());
            System.out.println("writing OBJ file: " + f.getAbsolutePath());
            OBJWriter writer = new OBJWriter(f);
            outField = outIrregularField;
            prepareOutputGeometry();
            show();
            writer.writeNode(outObj.getGeometryObj());
            writer.close();
            System.out.println("done");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
