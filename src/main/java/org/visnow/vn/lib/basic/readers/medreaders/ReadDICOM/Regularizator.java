/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.medreaders.ReadDICOM;

import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.RegularFieldInterpolator;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.ShortLargeArray;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Regularizator
{

    public Regularizator()
    {

    }

    public RegularField regularizeToMin(RegularField inField, int nThreads)
    {
        if (inField == null)
            return null;

        float[][] inAffine = inField.getAffine();
        float dx = 0, dy = 0, dz = 0;
        for (int i = 0; i < 3; i++) {
            dx += inAffine[0][i] * inAffine[0][i];
            dy += inAffine[1][i] * inAffine[1][i];
            dz += inAffine[2][i] * inAffine[2][i];
        }
        dx = (float) sqrt(dx);
        dy = (float) sqrt(dy);
        dz = (float) sqrt(dz);

        dx = min(dx, min(dy, dz));

        return regularize(inField, nThreads, dx);
    }

    public RegularField regularizeToX(RegularField inField, int nThreads)
    {
        if (inField == null)
            return null;

        float[][] inAffine = inField.getAffine();
        float dx = 0;
        for (int i = 0; i < 3; i++) {
            dx += inAffine[0][i] * inAffine[0][i];
        }
        dx = (float) sqrt(dx);

        return regularize(inField, nThreads, dx);
    }

    public RegularField regularize(RegularField inField, int nThreads, float voxelSize)
    {
        if (inField == null)
            return null;

        if (voxelSize <= 0)
            return null;

        float[][] inExtents = inField.getPreferredExtents();
        float dx = voxelSize;

        int[] outDims = new int[3];
        outDims[0] = max(2, (int) floor((inExtents[1][0] - inExtents[0][0]) / dx) + 1);
        outDims[1] = max(2, (int) floor((inExtents[1][1] - inExtents[0][1]) / dx) + 1);
        outDims[2] = max(2, (int) floor((inExtents[1][2] - inExtents[0][2]) / dx) + 1);

        float[][] outAffine = new float[4][3];
        outAffine[0][0] = dx;
        outAffine[0][1] = 0;
        outAffine[0][2] = 0;
        outAffine[1][0] = 0;
        outAffine[1][1] = dx;
        outAffine[1][2] = 0;
        outAffine[2][0] = 0;
        outAffine[2][1] = 0;
        outAffine[2][2] = dx;
        outAffine[3][0] = inExtents[0][0];
        outAffine[3][1] = inExtents[0][1];
        outAffine[3][2] = inExtents[0][2];

        RegularField outField = new RegularField(outDims);
        outField.setAffine(outAffine);
        outField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());

        WorkerThread[] workers = new WorkerThread[nThreads];

        for (int component = 0; component < inField.getNComponents(); component++) {
            if (!inField.getComponent(component).isNumeric() || inField.getComponent(component).getVectorLength() != 1)
                continue;
            double minv = inField.getComponent(component).getPreferredMinValue();
            DataArray outDataArray = null;
            switch (inField.getComponent(component).getType()) {
                case FIELD_DATA_BYTE:
                    byte[] outBData = new byte[outDims[0] * outDims[1] * outDims[2]];
                    for (int i = 0; i < workers.length; i++) {
                        workers[i] = new WorkerThread(i, nThreads, inField, outField,
                                                      (byte[]) inField.getComponent(component).getRawArray().getData(), outBData, minv);
                        workers[i].start();
                    }
                    for (WorkerThread worker : workers) {
                        try {
                            worker.join();
                        } catch (InterruptedException ex) {
                        }
                    }
                    outDataArray = DataArray.create(outBData, 1, inField.getComponent(component).getName());
                    break;
                case FIELD_DATA_SHORT:
                    short[] outSData = new short[outDims[0] * outDims[1] * outDims[2]];
                    for (int i = 0; i < workers.length; i++) {
                        workers[i] = new WorkerThread(i, nThreads, inField, outField,
                                                      (short[]) inField.getComponent(component).getRawArray().getData(), outSData, minv);
                        workers[i].start();
                    }
                    for (WorkerThread worker : workers) {
                        try {
                            worker.join();
                        } catch (InterruptedException ex) {
                        }
                    }
                    outField.addComponent(DataArray.create(outSData, 1, inField.getComponent(component).getName()));
                    break;
                case FIELD_DATA_INT:
                    int[] outIData = new int[outDims[0] * outDims[1] * outDims[2]];
                    for (int i = 0; i < workers.length; i++) {
                        workers[i] = new WorkerThread(i, nThreads, inField, outField,
                                                      (int[]) inField.getComponent(component).getRawArray().getData(), outIData, minv);
                        workers[i].start();
                    }
                    for (WorkerThread worker : workers) {
                        try {
                            worker.join();
                        } catch (InterruptedException ex) {
                        }
                    }
                    outField.addComponent(DataArray.create(outIData, 1, inField.getComponent(0).getName()));
                    break;
                case FIELD_DATA_FLOAT:
                    float[] outFData = new float[outDims[0] * outDims[1] * outDims[2]];
                    for (int i = 0; i < workers.length; i++) {
                        workers[i] = new WorkerThread(i, nThreads, inField, outField,
                                                      (float[]) inField.getComponent(component).getRawArray().getData(), outFData, minv);
                        workers[i].start();
                    }
                    for (WorkerThread worker : workers) {
                        try {
                            worker.join();
                        } catch (InterruptedException ex) {
                        }
                    }
                    outField.addComponent(DataArray.create(outFData, 1, inField.getComponent(component).getName()));
                    break;
                default:
                    float[] outData = new float[outDims[0] * outDims[1] * outDims[2]];
                    for (int i = 0; i < workers.length; i++) {
                        workers[i] = new WorkerThread(i, nThreads, inField, outField,
                                                      inField.getComponent(component).getRawFloatArray().getData(), outData, minv);
                        workers[i].start();
                    }
                    for (WorkerThread worker : workers) {
                        try {
                            worker.join();
                        } catch (InterruptedException ex) {
                        }
                    }
                    outField.addComponent(DataArray.create(outData, 1, inField.getComponent(component).getName()));
                    break;
            }
            if (outDataArray != null) {
                outDataArray.setPreferredRanges(inField.getComponent(component).getPreferredMinValue(), inField.getComponent(component).getPreferredMaxValue(), inField.getComponent(component).getPreferredPhysMinValue(), inField.getComponent(component).getPreferredPhysMaxValue());
                outField.addComponent(outDataArray);
            }

        }
        return outField;
    }

    private class WorkerThread extends Thread
    {

        private RegularField inField = null;
        private RegularField outField = null;
        private byte[] inBData = null;
        private byte[] outBData = null;
        private short[] inSData = null;
        private short[] outSData = null;
        private int[] inIData = null;
        private int[] outIData = null;
        private float[] inFData = null;
        private float[] outFData = null;
        private int nThreads = 0;
        private int iThread = 0;
        private double minv;

        public WorkerThread(int iThread, int nThreads, RegularField inField, RegularField outField,
                            byte[] inData, byte[] outData, double minv)
        {
            this.iThread = iThread;
            this.nThreads = nThreads;
            this.inField = inField;
            this.outField = outField;
            this.inBData = inData;
            this.outBData = outData;
            this.minv = minv;
        }

        public WorkerThread(int iThread, int nThreads, RegularField inField, RegularField outField,
                            short[] inData, short[] outData, double minv)
        {
            this.iThread = iThread;
            this.nThreads = nThreads;
            this.inField = inField;
            this.outField = outField;
            this.inSData = inData;
            this.outSData = outData;
            this.minv = minv;
        }

        public WorkerThread(int iThread, int nThreads, RegularField inField, RegularField outField,
                            int[] inData, int[] outData, double minv)
        {
            this.iThread = iThread;
            this.nThreads = nThreads;
            this.inField = inField;
            this.outField = outField;
            this.inIData = inData;
            this.outIData = outData;
            this.minv = minv;
        }

        public WorkerThread(int iThread, int nThreads, RegularField inField, RegularField outField,
                            float[] inData, float[] outData, double minv)
        {
            this.iThread = iThread;
            this.nThreads = nThreads;
            this.inField = inField;
            this.outField = outField;
            this.inFData = inData;
            this.outFData = outData;
            this.minv = minv;
        }

        @Override
        public void run()
        {
            if (inBData != null)
                runByte();
            else if (inSData != null)
                runShort();
            else if (inIData != null)
                runInt();
            else if (inFData != null)
                runFloat();
        }

        public void runByte()
        {
            int[] inDims = inField.getDims();
            int[] outDims = outField.getDims();
            float[] pIn = new float[3], pOut = new float[3];
            byte c;
            byte min = (byte) (0xff & (int) minv);
            for (int k = iThread; k < outDims[2]; k += nThreads) {
                for (int j = 0; j < outDims[1]; j++) {
                    for (int i = 0; i < outDims[0]; i++) {
                        try {
                            pOut = outField.getGridCoords(i, j, k);
                            pIn = inField.getFloatIndices(pOut[0], pOut[1], pOut[2]);
                            if (pIn[0] < 0 || pIn[0] >= inDims[0] || pIn[1] < 0 || pIn[1] >= inDims[1] || pIn[2] < 0 || pIn[2] >= inDims[2])
                                c = min;
                            else
                                c = RegularFieldInterpolator.getInterpolatedScalarData3D(new UnsignedByteLargeArray(inBData), inDims, pIn[0], pIn[1], pIn[2]);
                            outBData[k * outDims[0] * outDims[1] + j * outDims[0] + i] = c;

                        } catch (Exception e) {
//                            System.out.println("shit for i = "+i+", j = "+j+", k = "+k+
//                                    ", in[0] = "+pIn[0]+", in[1] = "+pIn[1] +", in[1] = "+pIn[2]+
//                                    ", out[0] = "+pOut[0]+", out[1] = "+pOut[1] +", out[1] = "+pOut[2]);
                        }
                    }
                }
                if (iThread == 0)
                    Regularizator.this.fireStatusChanged((float) (k + 1) / (float) (outDims[2]));
            }
        }

        public void runShort()
        {
            int[] inDims = inField.getDims();
            int[] outDims = outField.getDims();
            float[] pIn, pOut;
            short c;
            short min = (short) minv;
            for (int k = iThread; k < outDims[2]; k += nThreads) {
                for (int j = 0; j < outDims[1]; j++) {
                    for (int i = 0; i < outDims[0]; i++) {
                        pOut = outField.getGridCoords(i, j, k);
                        pIn = inField.getFloatIndices(pOut[0], pOut[1], pOut[2]);
                        if (pIn[0] < 0 || pIn[0] >= inDims[0] || pIn[1] < 0 || pIn[1] >= inDims[1] || pIn[2] < 0 || pIn[2] >= inDims[2])
                            c = min;
                        else
                            c = RegularFieldInterpolator.getInterpolatedScalarData3D(new ShortLargeArray(inSData), inDims, pIn[0], pIn[1], pIn[2]);
                        outSData[k * outDims[0] * outDims[1] + j * outDims[0] + i] = c;
                    }
                }
                if (iThread == 0)
                    Regularizator.this.fireStatusChanged((float) (k + 1) / (float) (outDims[2]));
            }
        }

        public void runInt()
        {
            int[] inDims = inField.getDims();
            int[] outDims = outField.getDims();
            float[] pIn, pOut;
            int c;
            int min = (int) minv;
            for (int k = iThread; k < outDims[2]; k += nThreads) {
                for (int j = 0; j < outDims[1]; j++) {
                    for (int i = 0; i < outDims[0]; i++) {
                        pOut = outField.getGridCoords(i, j, k);
                        pIn = inField.getFloatIndices(pOut[0], pOut[1], pOut[2]);
                        if (pIn[0] < 0 || pIn[0] >= inDims[0] || pIn[1] < 0 || pIn[1] >= inDims[1] || pIn[2] < 0 || pIn[2] >= inDims[2])
                            c = min;
                        else
                            c = RegularFieldInterpolator.getInterpolatedScalarData3D(new IntLargeArray(inIData), inDims, pIn[0], pIn[1], pIn[2]);
                        outIData[k * outDims[0] * outDims[1] + j * outDims[0] + i] = c;
                    }
                }
                if (iThread == 0)
                    Regularizator.this.fireStatusChanged((float) (k + 1) / (float) (outDims[2]));
            }
        }

        public void runFloat()
        {
            int[] inDims = inField.getDims();
            int[] outDims = outField.getDims();
            float[] pIn, pOut;
            float c;
            float min = (float) minv;
            for (int k = iThread; k < outDims[2]; k += nThreads) {
                for (int j = 0; j < outDims[1]; j++) {
                    for (int i = 0; i < outDims[0]; i++) {

                        pOut = outField.getGridCoords(i, j, k);
                        pIn = inField.getFloatIndices(pOut[0], pOut[1], pOut[2]);
                        if (pIn[0] < 0 || pIn[0] >= inDims[0] || pIn[1] < 0 || pIn[1] >= inDims[1] || pIn[2] < 0 || pIn[2] >= inDims[2])
                            c = min;
                        else
                            c = RegularFieldInterpolator.getInterpolatedScalarData3D(new FloatLargeArray(inFData), inDims, pIn[0], pIn[1], pIn[2]);
                        outFData[k * outDims[0] * outDims[1] + j * outDims[0] + i] = c;
                    }
                }
                if (iThread == 0)
                    Regularizator.this.fireStatusChanged((float) (k + 1) / (float) (outDims[2]));
            }
        }

    }

    private transient FloatValueModificationListener statusListener = null;

    public void addFloatValueModificationListener(FloatValueModificationListener listener)
    {
        if (statusListener == null) {
            this.statusListener = listener;
        } else {
            System.out.println("" + this + ": only one status listener can be added");
        }
    }

    private void fireStatusChanged(float status)
    {
        FloatValueModificationEvent e = new FloatValueModificationEvent(this, status, true);
        if (statusListener != null) {
            statusListener.floatValueChanged(e);
        }
    }

}
