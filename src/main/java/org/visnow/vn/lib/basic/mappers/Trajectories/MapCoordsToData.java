/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Trajectories;

import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.LogicLargeArray;

/**
 *
 * @author creed
 */
public class MapCoordsToData
{

    public static RegularField MapCoordsToData(Field field)
    {
        final int nNodes = (int) field.getNNodes();
        final int nFrames = field.getNFrames();
        int[] dims = new int[]{nFrames, nNodes};
        //		float[][] points = {{-.5f, -.5f, 0}, {.5f, .5f, 0}};
        //		RegularField mappedField = new RegularField(dims, points);
        RegularField mappedField = new RegularField(dims);

        float[][] mappedCoords = new float[3][nNodes * nFrames];
        LogicLargeArray mask = new LogicLargeArray(nNodes * nFrames);
        boolean maskNeeded = false;

        if (field.hasMask()) {
            for (int n = 0; n < nNodes; n++) {
                for (int t = 0; t < nFrames; t++) {
                    boolean b = field.getMask(t).getBoolean(n);
                    mask.setBoolean(n * nFrames + t, b);
                    if (b == false)
                        maskNeeded = true;
                }
            }
        }

        if (maskNeeded) {
            mappedField.setCurrentMask(mask);
        }

        for (int d = 0; d < 3; d++) {
            for (int n = 0; n < nNodes; n++) {
                for (int t = 0; t < nFrames; t++) {
                    float val = field.getCoords(t).getFloat(3 * n + d);
                    mappedCoords[d][n * nFrames + t] = val;
                    //                  mappedCoords[d][ t * nNodes + n] = val;
                }
            }
        }

        String[] coordsDesc = new String[]{"x", "y", "z"};
        assert (3 <= coordsDesc.length);
        for (int d = 0; d < 3; d++) {
            DataArray da = DataArray.create(mappedCoords[d], 1, coordsDesc[d]);
            mappedField.addComponent(da);
        }

        return mappedField;
    }
}
