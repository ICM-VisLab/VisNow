/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.Threshold;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Threshold extends ModuleCore
{

    private GUI ui = null;
    private Core core = new Core();
    protected Params params;
    Field inField = null;
    private boolean fromGUI = false;

    public Threshold()
    {
        parameters = params = new Params();
        core.setParams(params);
        params.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                fromGUI = true;
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                ui = new GUI();
            }
        });
        ui.setParams(params);
        setPanel(ui);
    }

    @Override
    public void onActive()
    {
        if(!fromGUI) {
            if (getInputFirstValue("inField") == null)
                return;
            Field inFld = ((VNField) getInputFirstValue("inField")).getField();
            if (inFld == null)
                return;
            if (inFld != inField) {
                inField = inFld;
                ui.setInField(inField);
                core.setInField(inField);
            }
        } else {
            core.update();
            Field outField = core.getOutField();
            if (outField != null && outField instanceof RegularField) {
                setOutputValue("outRegularField", new VNRegularField((RegularField) outField));
                setOutputValue("outIrregularField", null);
            } else if (outField != null && outField instanceof IrregularField) {
                setOutputValue("outRegularField", null);
                setOutputValue("outIrregularField", new VNIrregularField((IrregularField) outField));
            } else {
                setOutputValue("outRegularField", null);
                setOutputValue("outIrregularField", null);
            }
        }
        fromGUI = false;

    }

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

}
