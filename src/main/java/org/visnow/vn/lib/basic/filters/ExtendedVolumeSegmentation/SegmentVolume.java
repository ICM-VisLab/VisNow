/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.ExtendedVolumeSegmentation;

import org.visnow.vn.lib.basic.filters.VolumeSegmentation.*;
import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.RegularField;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
abstract public class SegmentVolume
{

    protected static final int SEGLEN = 1000;
    public static final int SIMILARITY_COMPUTED = 1;
    public static final int COMPUTING_OUT = 2;
    protected static final int UNKNOWN = 0;
    protected static final int BACKGROUND = 1;
    protected int status = 0;
    protected RegularField inField = null;
    protected ArrayList<int[]> selectedPoints = null;
    protected int[] dims = null;
    protected byte[] mask = null;
    protected boolean[] allowed = null;
    protected int[] off;
    protected int[] dist;
    protected int[] outd = null;
    protected int ndata;
    protected int low = -1;
    protected int up = 256;
    protected int lmax = 256;

    protected void extendMargins(short val)
    {
        if (outd == null)
            return;
        int i, j, k, l, m;
        k = dims[0] * dims[1] * (dims[2] - 1);
        l = dims[0] * dims[1];
        m = dims[0] * dims[1] * dims[2];
        for (i = 0; i < l; i++)
            outd[i] = outd[i + k] = val;
        k = dims[0] * (dims[1] - 1);
        for (i = 0; i < m; i += l)
            for (j = 0; j < dims[0]; j++)
                outd[i + j] = outd[i + j + k] = val;
        k = dims[0] - 1;
        for (i = 0; i < m; i += l)
            for (j = 0; j < l; j += dims[0])
                outd[i + j] = outd[i + j + k] = val;
    }

    abstract public void setIndices(ArrayList<Integer> indices);

    abstract public void setWeights(float[] weights);

    abstract public void setTollerance(short tollerance);

    public void setRange(int low, int up)
    {
        this.low = low;
        this.up = up;
    }

    abstract public void computeDistance(ArrayList<int[]> selectedPoints, boolean[] allowed);

    abstract public void setComponent(int component);

    public int getStatus()
    {
        return status;
    }

    /**
     * Utility field holding list of ChangeListeners.
     */
    private transient ArrayList<ChangeListener> changeListenerList
        = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     * <p>
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     */
    public void fireStateChanged()
    {
        ChangeEvent e = new ChangeEvent(this);
        for (ChangeListener listener : changeListenerList)
            listener.stateChanged(e);
    }

    public int getLmax()
    {
        return lmax;
    }

    public void setLmax(int lmax)
    {
        this.lmax = lmax;
    }
}
