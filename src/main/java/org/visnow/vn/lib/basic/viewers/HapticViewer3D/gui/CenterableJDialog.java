/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.gui;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Point;
import java.awt.Window;

/**
 * Centers itself over a component (see method {@link #centerOn(java.awt.Component) }).
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class CenterableJDialog extends javax.swing.JDialog
{

    /**
     * Set's the dialog's position to the center of the component given.
     *
     * @param component component to be centered over
     */
    public void centerOn(Component component)
    {
        if (component == null) {
            throw new NullPointerException("component must not be null");
        }

        Point p = component.getLocationOnScreen();
        p.x += component.getWidth() / 2;
        p.x -= this.getWidth() / 2;

        p.y += component.getHeight() / 2;
        p.y -= this.getHeight() / 2;

        setLocation(p);

    }

    // below all the constructors - only super(...) is called there
    public CenterableJDialog()
    {
        super();
    }

    public CenterableJDialog(Frame owner)
    {
        super(owner);
    }

    public CenterableJDialog(Frame owner, boolean modal)
    {
        super(owner, modal);
    }

    public CenterableJDialog(Frame owner, String title)
    {
        super(owner, title);
    }

    public CenterableJDialog(Frame owner, String title, boolean modal)
    {
        super(owner, title, modal);
    }

    public CenterableJDialog(Frame owner, String title, boolean modal, GraphicsConfiguration gc)
    {
        super(owner, title, modal, gc);
    }

    public CenterableJDialog(Dialog owner)
    {
        super(owner);
    }

    public CenterableJDialog(Dialog owner, boolean modal)
    {
        super(owner, modal);
    }

    public CenterableJDialog(Dialog owner, String title)
    {
        super(owner, title);
    }

    public CenterableJDialog(Dialog owner, String title, boolean modal)
    {
        super(owner, title, modal);
    }

    public CenterableJDialog(Dialog owner, String title, boolean modal, GraphicsConfiguration gc)
    {
        super(owner, title, modal, gc);
    }

    public CenterableJDialog(Window owner)
    {
        super(owner);
    }

    public CenterableJDialog(Window owner, ModalityType modalityType)
    {
        super(owner, modalityType);
    }

    public CenterableJDialog(Window owner, String title)
    {
        super(owner, title);
    }

    public CenterableJDialog(Window owner, String title, ModalityType modalityType)
    {
        super(owner, title, modalityType);
    }

    public CenterableJDialog(Window owner, String title, ModalityType modalityType, GraphicsConfiguration gc)
    {
        super(owner, title, modalityType, gc);
    }
}
//revised.
