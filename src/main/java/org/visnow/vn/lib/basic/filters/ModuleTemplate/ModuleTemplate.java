/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.ModuleTemplate;

import java.util.Arrays;
import org.apache.log4j.Logger;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.filters.ModuleTemplate.ModuleTemplateShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

public class ModuleTemplate extends OutFieldVisualizationModule
{
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    private ModuleTemplateGUI computeUI;
    private RegularField inField = null;

    public ModuleTemplate()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new ModuleTemplateGUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(PARAMETER_INTEGER, 1),
            new Parameter<>(META_RANGE, new int[]{0, 1})

        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);

        //1. validate parameters
        //....
        //2. reset parameters / set smart values        
        if (resetParameters) {
        
            int[] range = new int[]{inField.getDims()[0] / 2, inField.getDims()[0] - 1};
            parameters.set(META_RANGE, range);
            int intParam = parameters.get(PARAMETER_INTEGER);
            if (intParam < range[0]) intParam = range[0];
            if (intParam > range[1]) intParam = range[1];
            parameters.set(PARAMETER_INTEGER, intParam);
        }

        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            //1. get new field
            RegularField newInField = ((VNRegularField) getInputFirstValue("inField")).getField();
            //1a. set "different Field" flag
            boolean isDifferentField = !isFromVNA() && (inField == null || !Arrays.equals(newInField.getComponentNames(), inField.getComponentNames()));
            inField = newInField;

            Parameters p;
            synchronized (parameters) {
                //2. validate params             
                validateParamsAndSetSmart(isDifferentField);
                //2b. clone param (local read-only copy)
                p = parameters.getReadOnlyClone();
            }
            
            //3. update gui (GUI doesn't change parameters !!!!!!!!!!!!! - assuming correct set of parameters)
            notifyGUIs(p, isDifferentField || isFromVNA(), false);
            
            //4. run computation and propagate
            System.out.println("p.get(PARAMETER_INTEGER) = " + p.get(PARAMETER_INTEGER));
        }
    }
}
