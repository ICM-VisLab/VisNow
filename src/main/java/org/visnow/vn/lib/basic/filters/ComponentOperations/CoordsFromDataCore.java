/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.ComponentOperations;

import org.visnow.jscic.Field;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.RegularField;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LongLargeArray;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.filters.ComponentOperations.ComponentOperationsShared.*;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class CoordsFromDataCore
{

    private Parameters params = null;
    private Field inField = null;
    private Field outField = null;
    private int[] coordsComp = null;
    private float[] coordScale = null;
    private float[] coordShift = null;
    private float[] varShifts = null;
    private FloatLargeArray coords = null;
    private FloatLargeArray inCoords = null;
    private String[] axesNames = null;
    private String[] coordsUnits = null;
    private double[][] coordsPhysMappingCoeffs = null;

    public CoordsFromDataCore()
    {

    }

    public void setData(Field inField, Parameters p, Field outField)
    {
        this.inField = inField;
        this.outField = outField;
        this.params = p;
    }

    void update()
    {
        if (inField == null)
            return;
        int nFieldData = inField.getNComponents();
        inCoords = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords();
        if (inCoords == null && inField instanceof RegularField)
            inCoords = ((RegularField) inField).getCoordsFromAffine();
        coords = new FloatLargeArray(3 * inField.getNNodes());
        coordsComp = new int[]{params.get(XCOORD_COMPONENT),
                               params.get(YCOORD_COMPONENT),
                               params.get(ZCOORD_COMPONENT)};
        for (int i = 0; i < coordsComp.length; i++)
            if (coordsComp[i] >= nFieldData)
                coordsComp[i] = nFieldData - 1;

        varShifts = new float[]{params.get(XVAR_SHIFT),
                                params.get(YVAR_SHIFT),
                                params.get(ZVAR_SHIFT)};
        coordScale = new float[]{params.get(XCOORD_SCALE_VALUE),
                                 params.get(YCOORD_SCALE_VALUE),
                                 params.get(ZCOORD_SCALE_VALUE)};
        coordShift = new float[]{params.get(XCOORD_SHIFT),
                                 params.get(YCOORD_SHIFT),
                                 params.get(ZCOORD_SHIFT)};

        long nData = inField.getNNodes();

        if (params.get(ADD_INDEX_COMPONENT)) {
            LongLargeArray indexData = new LongLargeArray(nData);
            for (long i = 0; i < nData; i++)
                indexData.set(i,i);
            outField.addComponent(DataArray.create(indexData, 1, "index"));
        }

        axesNames = new String[]{"", "", ""};
        coordsUnits = new String[]{"1","1","1"};
        coordsPhysMappingCoeffs = new double[][]{{1,0},{1,0},{1,0}};
        for (int iCoord = 0; iCoord < 3; iCoord++) {
            int comp = coordsComp[iCoord];
            float scale = coordScale[iCoord];
            float varShift = varShifts[iCoord];
            float shift = coordShift[iCoord];
            FloatLargeArray c = null;
            if (comp >= 0) {
                c = inField.getComponent(comp).getRawFloatArray();
                axesNames[iCoord] = inField.getComponent(comp).getName();
                coordsUnits[iCoord] = inField.getComponent(comp).getUnit();
                coordsPhysMappingCoeffs[iCoord] = inField.getComponent(comp).getPhysicalMappingCoefficients(); //original phys mapping
                coordsPhysMappingCoeffs[iCoord][1] = coordsPhysMappingCoeffs[iCoord][1]+coordsPhysMappingCoeffs[iCoord][0]*(varShift-shift/scale); //compensation for variable scaling and shifting
                coordsPhysMappingCoeffs[iCoord][0] = coordsPhysMappingCoeffs[iCoord][0]/scale;
                for (long i = 0; i < nData; i++)
                    coords.set(3 * i + iCoord, scale * (c.get(i) - varShift) + shift);
            } else if (comp == -10) {
                axesNames[iCoord] = "x";
                coordsUnits[iCoord] = inField.getCoordsUnit(0);
                coordsPhysMappingCoeffs[iCoord] = inField.getPhysicalExtentsMappingCoefficients()[0];
                coordsPhysMappingCoeffs[iCoord][1] = coordsPhysMappingCoeffs[iCoord][1]+coordsPhysMappingCoeffs[iCoord][0]*(varShift-shift/scale);
                coordsPhysMappingCoeffs[iCoord][0] = coordsPhysMappingCoeffs[iCoord][0]/scale;
                for (long i = 0; i < nData; i++)
                    coords.set(3 * i + iCoord, scale * (inCoords.get(3 * i) - varShift) + shift);
            } else if (comp == -11) {
                axesNames[iCoord] = "y";
                coordsUnits[iCoord] = inField.getCoordsUnit(1);
                coordsPhysMappingCoeffs[iCoord] = inField.getPhysicalExtentsMappingCoefficients()[1];
                coordsPhysMappingCoeffs[iCoord][1] = coordsPhysMappingCoeffs[iCoord][1]+coordsPhysMappingCoeffs[iCoord][0]*(varShift-shift/scale);
                coordsPhysMappingCoeffs[iCoord][0] = coordsPhysMappingCoeffs[iCoord][0]/scale;
                for (long i = 0; i < nData; i++)
                    coords.set(3 * i + iCoord, scale * (inCoords.get(3 * i + 1) - varShift) + shift);
            } else if (comp == -12) {
                axesNames[iCoord] = "z";
                coordsUnits[iCoord] = inField.getCoordsUnit(2);
                coordsPhysMappingCoeffs[iCoord] = inField.getPhysicalExtentsMappingCoefficients()[2];
                coordsPhysMappingCoeffs[iCoord][1] = coordsPhysMappingCoeffs[iCoord][1]+coordsPhysMappingCoeffs[iCoord][0]*(varShift-shift/scale);
                coordsPhysMappingCoeffs[iCoord][0] = coordsPhysMappingCoeffs[iCoord][0]/scale;
                for (long i = 0; i < nData; i++)
                    coords.set(3 * i + iCoord, scale * (inCoords.get(3 * i + 2) - varShift) + shift);
            } else if (comp == -100) {
                for (long i = 0; i < nData; i++)
                    coords.set(3 * i + iCoord, shift);
            }
        }
        if (inField instanceof RegularField) {
            int[] dims = ((RegularField)inField).getDims();

            for (int iCoord = 0; iCoord < 3; iCoord++) {
                int comp = coordsComp[iCoord];
                float scale = coordScale[iCoord];
                float shift = coordShift[iCoord];
                float varShift = varShifts[iCoord];
                long n;
                switch (dims.length) {
                    case 1:
                        if (comp == -1) {
                            axesNames[iCoord] = "i";
                            coordsPhysMappingCoeffs[iCoord][1] = coordsPhysMappingCoeffs[iCoord][1]+coordsPhysMappingCoeffs[iCoord][0]*(varShift-shift/scale);
                            coordsPhysMappingCoeffs[iCoord][0] = coordsPhysMappingCoeffs[iCoord][0]/scale;
                            for (long i = 0; i < nData; i++)
                                coords.set(3 * i + iCoord, scale * (i - varShift) + shift);
                        }
                        break;
                    case 2:
                        switch (comp) {
                            case -1:
                                axesNames[iCoord] = "i";
                                coordsPhysMappingCoeffs[iCoord][1] = coordsPhysMappingCoeffs[iCoord][1]+coordsPhysMappingCoeffs[iCoord][0]*(varShift-shift/scale);
                                coordsPhysMappingCoeffs[iCoord][0] = coordsPhysMappingCoeffs[iCoord][0]/scale;
                                n = 0;
                                for (int j = 0; j < dims[1]; j++)
                                    for (int i = 0; i < dims[0]; i++, n++)
                                        coords.set(3 * n + iCoord, scale * (i - varShift) + shift);
                                break;
                            case -2:
                                axesNames[iCoord] = "j";
                                coordsPhysMappingCoeffs[iCoord][1] = coordsPhysMappingCoeffs[iCoord][1]+coordsPhysMappingCoeffs[iCoord][0]*(varShift-shift/scale);
                                coordsPhysMappingCoeffs[iCoord][0] = coordsPhysMappingCoeffs[iCoord][0]/scale;
                                n = 0;
                                for (int j = 0; j < dims[1]; j++)
                                    for (int i = 0; i < dims[0]; i++, n++)
                                        coords.set(3 * n + iCoord, scale * (j - varShift) + shift);
                                break;
                        }
                        break;
                    case 3:
                        switch (comp) {
                            case -1:
                                axesNames[iCoord] = "i";
                                coordsPhysMappingCoeffs[iCoord][1] = coordsPhysMappingCoeffs[iCoord][1]+coordsPhysMappingCoeffs[iCoord][0]*(varShift-shift/scale);
                                coordsPhysMappingCoeffs[iCoord][0] = coordsPhysMappingCoeffs[iCoord][0]/scale;
                                n = 0;
                                for (int k = 0; k < dims[2]; k++)
                                    for (int j = 0; j < dims[1]; j++)
                                        for (int i = 0; i < dims[0]; i++, n++)
                                            coords.set(3 * n + iCoord, scale * (i - varShift) + shift);
                                break;
                            case -2:
                                axesNames[iCoord] = "j";
                                coordsPhysMappingCoeffs[iCoord][1] = coordsPhysMappingCoeffs[iCoord][1]+coordsPhysMappingCoeffs[iCoord][0]*(varShift-shift/scale);
                                coordsPhysMappingCoeffs[iCoord][0] = coordsPhysMappingCoeffs[iCoord][0]/scale;
                                n = 0; 
                                for (int k = 0; k < dims[2]; k++)
                                    for (int j = 0; j < dims[1]; j++)
                                        for (int i = 0; i < dims[0]; i++, n++)
                                            coords.set(3 * n + iCoord, scale * (j - varShift) + shift);
                                break;
                            case -3:
                                axesNames[iCoord] = "k";
                                coordsPhysMappingCoeffs[iCoord][1] = coordsPhysMappingCoeffs[iCoord][1]+coordsPhysMappingCoeffs[iCoord][0]*(varShift-shift/scale);
                                coordsPhysMappingCoeffs[iCoord][0] = coordsPhysMappingCoeffs[iCoord][0]/scale;
                                n = 0;
                                for (int k = 0; k < dims[2]; k++)
                                    for (int j = 0; j < dims[1]; j++)
                                        for (int i = 0; i < dims[0]; i++, n++)
                                            coords.set(3 * n + iCoord, scale * (k - varShift) + shift);
                                break;
                        }
                        break;
                }                
            }      
        }
            
        outField.setCurrentCoords(coords);
        //TODO time - what if setting coords from time dependent component, what if setting from several components with different timesteps, what if setting from x,y,z that also can be time-dependent in inField
        
        float[][] extents = outField.getExtents();
        float[][] physicalExtents = new float[2][3];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 3; j++) {
                physicalExtents[i][j] = (float) (coordsPhysMappingCoeffs[j][0] * (extents[i][j]) + coordsPhysMappingCoeffs[j][1]);  
            } 
        } 
        outField.setPreferredExtents(extents, physicalExtents);   
        
        outField.setAxesNames(axesNames);
        outField.setCoordsUnits(coordsUnits);
    }

}
