/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.unused;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import org.jogamp.java3d.TransformGroup;
import org.jogamp.java3d.WakeupCriterion;
import org.jogamp.vecmath.Vector3d;

/**
 * Modifed version of org.jogamp.java3d.utils.behaviors.mouse.MouseRotate
 *
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 */
public class VMouseRotate extends VMouseBehavior
{

    double x_angle, y_angle;
    double x_factor = .03;
    double y_factor = .03;

    /**
     * Creates a rotate behavior given the transform group.
     * <p/>
     * @param transformGroup The transformGroup to operate on.
     */
    public VMouseRotate(ViewLock vLock, TransformGroup transformGroup)
    {
        super(vLock, transformGroup);
    }

    /**
     * Creates a default mouse rotate behavior.
     *
     */
    public VMouseRotate(ViewLock vLock)
    {
        super(vLock);
    }

    public VMouseRotate(ViewLock vLock, Component c)
    {
        super(vLock, c);
    }

    @Override
    public void initialize()
    {
        super.initialize();
        x_angle = 0;
        y_angle = 0;
        if ((flags & INVERT_INPUT) == INVERT_INPUT) {
            invert = true;
            x_factor *= -1;
            y_factor *= -1;
        }
    }

    /**
     * Return the x-axis movement multipler.
     *
     */
    public double getXFactor()
    {
        return x_factor;
    }

    /**
     * Return the y-axis movement multipler.
     *
     */
    public double getYFactor()
    {
        return y_factor;
    }

    /**
     * Set the x-axis amd y-axis movement multipler with factor.
     *
     */
    public void setFactor(double factor)
    {
        x_factor = y_factor = factor;
    }

    /**
     * Set the x-axis amd y-axis movement multipler with xFactor and yFactor
     * respectively.
     *
     */
    public void setFactor(double xFactor, double yFactor)
    {
        x_factor = xFactor;
        y_factor = yFactor;
    }

    protected void doProcess(MouseEvent evt)
    {
        int id;
        int dx, dy;

        processMouseEvent(evt);

        id = evt.getID();
        if (!evt.isMetaDown() && !evt.isAltDown()) {
            if (id == MouseEvent.MOUSE_DRAGGED && vLock.ownLock(this)) {
                x = evt.getX();
                y = evt.getY();

                dx = x - x_last;
                dy = y - y_last;

                if (!reset) {
                    x_angle = dy * y_factor;
                    y_angle = dx * x_factor;

                    transformX.rotX(x_angle);
                    transformY.rotY(y_angle);

                    transformGroup.getTransform(currXform);

                    // Remember old translation
                    Vector3d translation = new Vector3d();
                    currXform.get(translation);

                    // Translate to origin
                    currXform.setTranslation(new Vector3d(0.0, 0.0, 0.0));
                    if (invert) {
                        currXform.mul(currXform, transformX);
                        currXform.mul(currXform, transformY);
                    } else {
                        currXform.mul(transformX, currXform);
                        currXform.mul(transformY, currXform);
                    }

                    // Set old translation back
                    currXform.setTranslation(translation);

                    // Update xform
                    transformGroup.setTransform(currXform);
                } else {
                    reset = false;
                }

                x_last = x;
                y_last = y;
            } else if (id == MouseEvent.MOUSE_PRESSED) {
                initAction(evt);
            } else if (id == MouseEvent.MOUSE_RELEASED) {
                endAction(evt);
            }
        }
    }

    @Override
    public void processStimulus(Iterator<WakeupCriterion> itrtr) {
    }
}
