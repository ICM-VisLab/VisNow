/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.ExtendedVolumeSegmentation;

import org.visnow.vn.lib.basic.filters.VolumeSegmentation.*;
import java.util.ArrayList;
import java.util.Stack;
import javax.swing.ProgressMonitor;
import org.visnow.jscic.RegularField;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class SimilaritySegmentVolume extends SegmentVolume
{

    protected ArrayList<Integer> indices = null;
    protected short[][] data = null;
    protected float[] weights = null;
    /*
     * mask[i] = 1 for background,
     * mask[i] = 0 for voxels with unknown status
     * mask[i] = k>1 for k-throws segmented set
     */
    protected short tollerance = 300;
    protected Stack<int[]>[] queue = null;
    protected Stack<int[]> free = new Stack<int[]>();
    protected boolean inside;
    protected int[][] framePoints = null;
    protected int segIndex = 2;

    /**
     * Holds value of property backgroundThreshold.
     */
    /**
     * Creates a new instance of SegmentVolume
     * <p>
     * @param inField
     */
    public SimilaritySegmentVolume(RegularField inField, RegularField distField, RegularField outField)
    {
        dims = inField.getDims();
        off = inField.getPartNeighbOffsets();
        ndata = (int) inField.getNNodes();
        this.inField = inField;
        outd = (int[])distField.getComponent(0).getRawArray().getData();
        mask = (byte[])outField.getComponent(0).getRawArray().getData();
    }

    @Override
    public void computeDistance(ArrayList<int[]> selectedPoints, boolean[] allowed)
    {
        if (tollerance < 0 || allowed == null || selectedPoints.isEmpty())
            return;
        boolean isSomethingAllowed = false;
        for (int i = 0; i < allowed.length; i++)
            if (allowed[i]) {
                isSomethingAllowed = true;
                break;
            }
        if (!isSomethingAllowed)
            return;
        this.selectedPoints = selectedPoints;
        this.allowed = allowed;

        data = new short[indices.size()][];
        for (int i = 0; i < indices.size(); i++)
            data[i] = inField.getComponent(indices.get(i)).getRawShortArray().getData();

        status = 0;
        new Thread(new ComputeSimilarity()).start();
    }

    private class ComputeSimilarity implements Runnable
    {

        @SuppressWarnings("unchecked")
        @Override
        public synchronized void run()
        {
            int i, j, k, n, cd, d;
            int[] p;
            int[] qSegment;
            int[] qSeg = null;
            //fast implementation of a priority queue as table of stacks        
            ProgressMonitor monitor = new ProgressMonitor(null, "Computing similarity field...", " ", 0, 100);
            monitor.setMillisToDecideToPopup(100);
            queue = new Stack[tollerance];
            monitor.setMillisToPopup(100);
            for (i = 0; i < tollerance; i++)
                queue[i] = new Stack<int[]>();
            for (i = 0; i < ndata; i++)
                outd[i] = tollerance;
            //set outd to 0 on boundary 
            extendMargins((short) 0);
            qSegment = new int[SEGLEN];
            //initializing queue to selected points and setting seed values at selected points        
            for (i = 0; i < selectedPoints.size(); i++) {
                p = selectedPoints.get(i);
                k = (dims[1] * p[2] + p[1]) * dims[0] + p[0];
                outd[k] = 0;
                if (i % SEGLEN == 0) {
                    qSegment = new int[SEGLEN];
                    queue[0].push(qSegment);
                    for (j = 0; j < SEGLEN; j++)
                        qSegment[j] = -1;
                }
                qSegment[i % SEGLEN] = k;
            }
            for (i = 0; i < tollerance; i++) {
                monitor.setProgress((i * 100) / (tollerance - 1));
                while (!queue[i].empty()) {
                    qSegment = queue[i].pop();
                    for (k = 0; k < qSegment.length && qSegment[k] != -1; k++) {
                        n = qSegment[k];
                        if (!allowed[mask[n]])
                            continue;
                        cd = outd[n];
                        for (j = 0; j < off.length; j++) {
                            int of = off[j];
                            if (!allowed[mask[n + of]] || data[0][n + of] < low || data[0][n + of] > up)
                                continue;
                            d = outd[n + of];
                            if (d < cd)
                                continue;
                            float dd = 0;
                            for (int id = 0; id < data.length; id++) {
                                if (weights[id] == 0)
                                    continue;
                                int ll = data[id][n + of] - data[id][n];
                                dd += weights[id] * ll * ll;
                            }
                            int l = cd + (int) dd;
                            if (l < d) {
                                outd[n + of] = l;
                                if (l == i && qSegment[SEGLEN - 1] == -1)
                                    qSeg = qSegment;
                                else {
                                    qSeg = null;
                                    if (!queue[l].empty())
                                        qSeg = queue[l].peek();
                                    if (queue[l].empty() || qSeg[SEGLEN - 1] != -1) {
                                        if (free.empty()) {
                                            qSeg = new int[SEGLEN];
                                        } else
                                            qSeg = free.pop();
                                        for (int ll = 0; ll < SEGLEN; ll++)
                                            qSeg[ll] = -1;
                                        queue[l].push(qSeg);
                                    }
                                }
                                for (int ll = 0; ll < SEGLEN; ll++)
                                    if (qSeg[ll] == -1) {
                                        qSeg[ll] = n + of;
                                        break;
                                    }
                            }
                        }
                    }
                    free.push(qSegment);
                }
            }
            free.clear();
            status = SIMILARITY_COMPUTED;
            fireStateChanged();
        }
    }

    @Override
    public void setIndices(ArrayList<Integer> indices)
    {
        this.indices = indices;
    }

    @Override
    public void setWeights(float[] weights)
    {
        this.weights = weights;
    }

    @Override
    public void setTollerance(short tollerance)
    {
        this.tollerance = tollerance;
    }

    public void setComponent(int component)
    {

    }

}
