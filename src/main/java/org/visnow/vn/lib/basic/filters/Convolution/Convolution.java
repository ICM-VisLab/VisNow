/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.Convolution;

import java.util.Arrays;
import org.apache.log4j.Logger;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.filters.Convolution.ConvolutionShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.jvia.spatialops.Padding.PaddingType;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;

/**
 *
 * @author Piotr Wendykier (piotrw@icm.edu.pl)
 */
public class Convolution extends OutFieldVisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(Convolution.class);

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI computeUI;
    protected RegularField inFieldData;
    protected RegularField inFieldKernel;
    private Core core = new Core();

    public Convolution()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                LOGGER.debug("name = " + name);
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(PADDING_TYPE, PaddingType.FIXED),
            new Parameter<>(NORMALIZE_KERNEL, true),
            new Parameter<>(SELECTED_COMPONENTS, new int[]{}),
            new Parameter<>(META_COMPONENT_NAMES, new String[]{})
        };
    }

    public void update()
    {
        core.setInFieldData(inFieldData);
        core.setInFieldKernel(inFieldKernel);
        if (parameters.get(SELECTED_COMPONENTS).length > 0) {
            core.update(parameters.get(SELECTED_COMPONENTS), parameters.get(PADDING_TYPE), parameters.get(NORMALIZE_KERNEL));
            outRegularField = core.getOutField();
            outField = outRegularField;
            setOutputValue("outField", new VNRegularField(outRegularField));
        } else {
            outRegularField = null;
            outField = null;
            setOutputValue("outField", null);
        }
        prepareOutputGeometry();
        show();
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        LOGGER.debug("isFromVNA = " + isFromVNA());

        VNField inFldData = (VNField) getInputFirstValue("inFieldData");
        VNField inFldKernel = (VNField) getInputFirstValue("inFieldKernel");
        if (inFldData != null && inFldKernel != null) {
            Field newInFieldData = inFldData.getField();
            boolean isDifferentField = !isFromVNA() && (inFieldData == null || !Arrays.equals(newInFieldData.getComponentNames(), inFieldData.getComponentNames()));

//            if (newInFieldData != null && newInFieldData instanceof RegularField && inFieldData != newInFieldData) {
            inFieldData = (RegularField) newInFieldData;
            VisNow.get().userMessageSend(this, "<html>Data field updated successfully.", inFieldData != null ? inFieldData.toMultilineString() : "", Level.INFO);
//            }

            Parameters p;
            synchronized (parameters) {
                parameters.setParameterActive(false);
                parameters.set(META_COMPONENT_NAMES, inFieldData.getComponentNames());
                if (isDifferentField && inFieldData.getComponentNames().length > 0)
                    parameters.set(SELECTED_COMPONENTS, new int[]{0});
                parameters.setParameterActive(true);
                p = parameters.getReadOnlyClone();
            }
            notifyGUIs(p, false, false);

            //XXX: cloned parameters should be used from here onwards
//            if (inFieldData != null) {
            int[] dimsData = inFieldData.getDims();
            RegularField newInFieldKernel = (RegularField) inFldKernel.getField();
//                if (newInFieldKernel != null && newInFieldKernel instanceof RegularField) {
            if (newInFieldKernel.getNComponents() > 1) {
                VisNow.get().userMessageSend(this, "Kernel field cannot contain multiple components.", "", Level.ERROR);
                setOutputValue("outField", null);
                return;
            }

            if (newInFieldKernel.getComponent(0).getVectorLength() > 1) {
                VisNow.get().userMessageSend(this, "Kernel field cannot contain vector components.", "", Level.ERROR);
                setOutputValue("outField", null);
                return;
            }

            int[] dimsKernel = newInFieldKernel.getDims();
            if (dimsKernel.length != dimsData.length) {
                VisNow.get().userMessageSend(this, "The rank of data field (" + inFieldData.getDimNum() + ") is different than the rank of kernel field (" + newInFieldKernel.getDimNum() + ").", "", Level.ERROR);
                return;
            }
            for (int i = 0; i < dimsKernel.length; i++) {
                if (dimsKernel[i] > dimsData[i]) {
                    VisNow.get().userMessageSend(this, "The dimensions of kernel field cannot be larger that the dimensions of data field.", "", Level.ERROR);
                    setOutputValue("outField", null);
                    return;
                }
            }

            inFieldKernel = newInFieldKernel;
            VisNow.get().userMessageSend(this, "<html>Kernel field updated successfully.", inFieldKernel != null ? inFieldKernel.toMultilineString() : "", Level.INFO);
//                }
//            }
            update();
        } else {
            setOutputValue("outField", null);
        }
    }
}
