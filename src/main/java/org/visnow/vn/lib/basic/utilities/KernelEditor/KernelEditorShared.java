/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.utilities.KernelEditor;

import org.visnow.vn.engine.core.ParameterName;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class KernelEditorShared
{
    public enum KernelType
    {
        CONSTANT("Constant"),
        GAUSSIAN("Gaussian"),
        LINEAR("Linear"),
        CONICAL("Conical"),
        CUSTOM("Custom");

        private String name;

        private KernelType(String name)
        {
            this.name = name;
        }

        @Override
        public String toString()
        {
            return name;
        }
    }

//Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    static final ParameterName<KernelType> KERNEL_TYPE = new ParameterName("Kernel type");

    //1, 2 or 3
    static final ParameterName<Integer> RANK = new ParameterName("Rank");

    // >= 1
    static final ParameterName<Integer> RADIUS = new ParameterName("Radius");

    static final ParameterName<Float> GAUSSIAN_SIGMA = new ParameterName("Gaussian sigma");

    static final ParameterName<Boolean> NORMALIZE_KERNEL = new ParameterName("Normalize kernel");

    //non-empty array of length: (2 * RADIUS + 1)^RANK
    static final ParameterName<float[]> KERNEL = new ParameterName("Kernel");
}
