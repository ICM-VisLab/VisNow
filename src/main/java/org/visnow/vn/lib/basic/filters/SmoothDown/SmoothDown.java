/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.SmoothDown;

import java.util.Arrays;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import static org.visnow.vn.lib.basic.filters.SmoothDown.SmoothDownShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.field.FieldSmoothDown;
import org.visnow.vn.system.main.VisNow;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class SmoothDown extends OutFieldVisualizationModule
{

    private SmoothDownGUI computeUI = new SmoothDownGUI();
    protected RegularField inField = null;
    protected boolean fromGUI = false;
    private int runQueue = 0;
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private final int nThreads = VisNow.availableProcessors();

    public SmoothDown()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name.equals(SMOOTHDOWN_RUNNING_MESSAGE.getName()) && parameters.get(SMOOTHDOWN_RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(SMOOTHDOWN_RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startIfNotInQueue();

            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new SmoothDownGUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(SMOOTHDOWN_SIGMA, 1.f),
            new Parameter<>(SMOOTHDOWN_DOWNSIZE, new int[]{3, 3, 3}),
            new Parameter<>(SMOOTHDOWN_RUNNING_MESSAGE, NO_RUN)
        };
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(parameters, resetFully, setRunButtonPending);
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        int[] dims = inField.getDims();

        int[] downsizes = parameters.get(SMOOTHDOWN_DOWNSIZE);

        if (!resetParameters) { //validate
            for (int i = 0; i < inField.getDimNum(); i++) {
                int maxDownsize = dims[i] / 2;
                if (downsizes[i] > maxDownsize)
                    if (downsizes[i] < maxDownsize + 2) //"smart" :(
                        downsizes[i] = maxDownsize;
                    else
                        downsizes[i] = Math.min(3, maxDownsize); //default (3) :(
            }
        } else { //set smart
            int scale = dims.length == 3 ? 100 : dims.length == 2 ? 1000 : 1000_000;
            for (int i = 0; i < dims.length; i++) downsizes[i] = (dims[i] + scale - 1) / scale;
        }
        parameters.set(SMOOTHDOWN_DOWNSIZE, downsizes);
        parameters.setParameterActive(true);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            RegularField newField = ((VNRegularField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = !isFromVNA() && (inField == null || !Arrays.equals(inField.getDims(), newField.getDims()));
            boolean isNewField = !isFromVNA() && newField != inField;
            inField = newField;

            Parameters p;
            synchronized (parameters) {
                validateParamsAndSetSmart(isDifferentField);
                p = parameters.getReadOnlyClone();
            }

            notifyGUIs(p, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

            if (runQueue > 0 || p.get(SMOOTHDOWN_RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0);
                outRegularField = FieldSmoothDown.smoothDown(inField, p.get(SMOOTHDOWN_DOWNSIZE), p.get(SMOOTHDOWN_SIGMA), nThreads);
                if (outRegularField == null) {
                    setOutputValue("outField", null);
                } else {
                    setOutputValue("outField", new VNRegularField(outRegularField));
                }
                outField = outRegularField;
                prepareOutputGeometry();
                show();
            }
        }
    }
}
