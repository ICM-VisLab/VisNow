/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class PointDescriptorTableModel extends AbstractTableModel
{

    private ArrayList<PointDescriptor> pts = new ArrayList<PointDescriptor>();
    private GeometryParams params = null;

    public PointDescriptorTableModel(GeometryParams params)
    {
        this.params = params;
        this.params.addGeometryParamsListener(new GeometryParamsListener()
        {

            @Override
            public void onGeometryParamsChanged(GeometryParamsEvent e)
            {
                if (e.getType() == GeometryParamsEvent.TYPE_POINT_MODIFIED || e.getType() == GeometryParamsEvent.TYPE_POINT_ADDED || e.getType() == GeometryParamsEvent.TYPE_POINT_REMOVED || e.getType() == GeometryParamsEvent.TYPE_ALL) {
                    fireTableDataChanged();
                }
                if (e.getType() == GeometryParamsEvent.TYPE_POINT_CLASS) {
                    fireTableStructureChanged();
                }
            }
        });
        this.pts = params.getPointsDescriptors();
    }

    public void refresh()
    {
        fireTableStructureChanged();
        fireTableDataChanged();
    }

    @Override
    public int getRowCount()
    {
        if (pts == null)
            return 0;

        return pts.size();
    }

    @Override
    public int getColumnCount()
    {
        if (params.getCurrentClassId() == -1)
            return 4;
        else
            return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        if (pts == null)
            return null;

        if (rowIndex >= pts.size())
            return null;

        switch (columnIndex) {
            case 0:
                return pts.get(rowIndex).getName();
            case 1:
                return (Float) pts.get(rowIndex).getWorldCoords()[0];
            case 2:
                return (Float) pts.get(rowIndex).getWorldCoords()[1];
            case 3:
                return (Float) pts.get(rowIndex).getWorldCoords()[2];
            case 4:
                return (Integer) pts.get(rowIndex).getMembership();
        }
        return null;
    }

    @Override
    public String getColumnName(int columnIndex)
    {
        switch (columnIndex) {
            case 0:
                return "Point label";
            case 1:
                return "x";
            case 2:
                return "y";
            case 3:
                return "z";
            case 4:
                return "class";
            default:
                return "";
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        if (columnIndex == 0)
            return String.class;
        else if (columnIndex == 4)
            return Integer.class;
        else
            return Float.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        return (columnIndex == 4 || !pts.get(rowIndex).isDependant());
    }

    @Override
    public void setValueAt(Object aValue, int row, int column)
    {
        if (params.getPointsDescriptor(row) instanceof DependantPointDescriptor)
            return;

        if (column == 0) {
            if (!(aValue instanceof String))
                return;
            String tmp = (String) aValue;
            pts.get(row).setName(tmp);
        } else if (column == 4) {
            if (!(aValue instanceof Integer))
                return;
            Integer tmp = (Integer) aValue;
            pts.get(row).setMembership(tmp);
        } else {
            if (!(aValue instanceof Float))
                return;
            Float v = (Float) aValue;

            float[] coords = pts.get(row).getWorldCoords();
            int[] indices = pts.get(row).getIndices();
            coords[column - 1] = v;
            int[] newIndices = params.getInField().getIndices(coords[0], coords[1], coords[2]);
            //indices[column-1] = newIndices[column-1];
            //pts.get(row).setIndices(indices);
            //pts.get(row).setWorldCoords(coords);
            params.modifyPoint(row, newIndices);
        }
        //params.fireGeometryParamsChanged(GeometryParamsEvent.TYPE_POINT_MODIFIED);
        fireTableCellUpdated(row, column);
    }

}
