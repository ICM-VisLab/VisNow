/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces;

import org.jogamp.vecmath.Point3f;
import org.jogamp.vecmath.Tuple3f;
import org.jogamp.vecmath.Vector3f;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ILocalToVworldGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.ITrackerToVworldGetter;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.CoordinateSystem;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces.HapticLocationData;

/**
 * Spring or any other force directly proportional to the distance. Although its center is described
 * in local coordinates, the force is generated also
 * when cursor is outside of the haptic outline.
 * <p/>
 * Its value is dependent on size of haptic field - the same spring constant might be good for small
 * haptic field and very bad for large one. Default value is good for test regular field 3D.
 * <p/>
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of
 * Warsaw, 2013
 */
public class Spring extends AbstractForce
{

    private final static CoordinateSystem forceCoordinateSystem = CoordinateSystem.LOCAL;
    //
    /**
     * The proper value is dependent on size of haptic field - the same spring constant might be
     * good for small haptic field and very bad for large one. Default value is good for test
     * regular field 3D.
     */
    public final static float DEFAULT_SPRING_CONSTANT = 50.0f;
    private float springConstant;
    private Tuple3f center;

    public Spring()
    {
        this(DEFAULT_SPRING_CONSTANT, new Point3f());
    }

    public Spring(float springConstant, Tuple3f center)
    {
        super(forceCoordinateSystem);

        if (center == null) {
            throw new NullPointerException("Argument 'center' cannot be null");
        }
        this.springConstant = springConstant;
        this.center = center;
    }

    /**
     * Copy constructor. To be used only by
     * <code>clone()</code> method.
     * <p/>
     * @param force Object to be copied from
     */
    protected Spring(Spring force)
    {
        super(force);
        center = new Point3f(force.center);
        springConstant = force.springConstant;
    }

    @Override
    public void getForce(HapticLocationData locationData, Vector3f out_force)
        throws ITrackerToVworldGetter.NoDataException,
        ILocalToVworldGetter.NoDataException
    {

        Point3f position = locationData.getCurrentLocalPosition();
        out_force.sub(center, position);
        out_force.scale(springConstant);
    }

    public Tuple3f getCenter()
    {
        return center;
    }

    public void setCenter(Tuple3f center)
    {
        this.center.set(center);
    }

    public float getConstant()
    {
        return springConstant;
    }

    public void setConstant(float constant)
    {
        this.springConstant = constant;
    }

    @Override
    public IForce clone()
    {
        return new Spring(this);
    }

    @Override
    public String getName()
    {
        return getClassSimpleName() + " [k=" + springConstant + ", c=" + center + "]";
    }
}
// revised.
