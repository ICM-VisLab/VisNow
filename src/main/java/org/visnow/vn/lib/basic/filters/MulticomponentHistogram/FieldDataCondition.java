/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.MulticomponentHistogram;

import org.visnow.jscic.Field;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class FieldDataCondition extends Condition
{

    private Field field;

    protected Integer component1 = null;
    protected Integer component2 = null;
    protected Float componentValue = 0.0f;

    public FieldDataCondition(Field field, Integer component1, Operator componentOperator, Integer component2)
    {
        this.field = field;
        this.component1 = component1;
        this.operator = componentOperator;
        this.component2 = component2;
        this.componentValue = null;
    }

    public FieldDataCondition(Field field, Integer component1, Operator componentOperator, Float componentValue)
    {
        this.field = field;
        this.component1 = component1;
        this.operator = componentOperator;
        this.component2 = null;
        this.componentValue = componentValue;
    }

    public boolean check(int n)
    {
        if (component1 == null || operator == null || (component2 == null && componentValue == null))
            return true;

        if (component2 != null)
            return chackDataByComponent(n);

        if (componentValue != null)
            return checkDataByValue(n);

        return false;
    }

    private boolean chackDataByComponent(int n)
    {
        return decide(field.getComponent(component1).getFloatElement(n)[0], field.getComponent(component2).getFloatElement(n)[0]);
    }

    private boolean checkDataByValue(int n)
    {
        return decide(field.getComponent(component1).getFloatElement(n)[0], componentValue);
    }

    public Integer getComponent1()
    {
        return component1;
    }

    public void setComponent1(Integer component)
    {
        this.component1 = component;
    }

    public Integer getComponent2()
    {
        return component2;
    }

    public void setComponent2(Integer component)
    {
        this.component2 = component;
    }

    public Float getComponentValue()
    {
        return componentValue;
    }

    public void setComponentValue(Float value)
    {
        this.componentValue = value;
    }

    public Field getField()
    {
        return field;
    }

}
