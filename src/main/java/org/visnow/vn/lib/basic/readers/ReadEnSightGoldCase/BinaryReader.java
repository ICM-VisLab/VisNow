/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadEnSightGoldCase;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.Cell;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.widgets.FileErrorFrame;
import static org.visnow.vn.lib.basic.readers.ReadEnSightGoldCase.ReadEnSightGoldCaseShared.MATERIALS_AS_SETS;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class BinaryReader extends Reader
{

    private DataArray[] readDataArrays(ImageInputStream inStream, int nData)
    {
        try {
            byte[] dataLabels = new byte[1024];
            inStream.readFully(dataLabels);
            String[] labels = new String(dataLabels).split("\\.");
            byte[] dataUnits = new byte[1024];
            inStream.readFully(dataUnits);
            String[] units = new String(dataUnits).split("\\.");
            int nDataArrays = inStream.readInt();
            DataArray[] dataArrays = new DataArray[nDataArrays];
            int[] vlens = new int[nDataArrays];
            inStream.readFully(vlens, 0, vlens.length);
            for (int i = 0; i < nDataArrays; i++)
                System.out.printf("%20s %20s %3d%n", labels[i], units[i], vlens[i]);
            float[][] arrays = new float[nDataArrays][];
            for (int i = 0; i < nDataArrays; i++) {
                arrays[i] = new float[vlens[i] * nData];
                inStream.readFully(arrays[i], 0, arrays[i].length);
            }

            for (int i = 0; i < nDataArrays; i++)
                dataArrays[i] = DataArray.create(arrays[i], vlens[i], labels[i], units[i], null);
            return dataArrays;
        } catch (IOException iOException) {
            iOException.printStackTrace();
        }
        return null;
    }

    public IrregularField readEnSightGoldGeometry(Parameters params, String filename)
    {
        IrregularField outField = null;
        int currentLine = 0;
        String fn = "";
        try {
            fn = new File(filename).getName();
            ImageInputStream inStream = new FileImageInputStream(new File(filename));
            inStream.skipBytes(1);
            int nnodes = inStream.readInt();
            int ncells = inStream.readInt();
            int nnodedata = inStream.readInt();
            int ncelldata = inStream.readInt();
            int nListNodes = inStream.readInt();
            outField = new IrregularField(nnodes);

            int[] cellids = new int[ncells];
            int[] cellmats = new int[ncells];
            int[] celltypes = new int[ncells];
            int[] ncellsoftype = new int[Cell.getNProperCellTypes()];
            int maxMat = 0;
            for (int i = 0; i < ncells; i++) {
                cellids[i] = inStream.readInt();
                cellmats[i] = inStream.readInt();
                if (cellmats[i] > maxMat)
                    maxMat = cellmats[i];
                inStream.skipBytes(4);
                celltypes[i] = inStream.readInt();
            }
            maxMat += 1;
            int[][] cellTypeNumbers = new int[maxMat][Cell.getNProperCellTypes()];
            for (int i = 0; i < cellTypeNumbers.length; i++)
                for (int j = 0; j < cellTypeNumbers[i].length; j++)
                    cellTypeNumbers[i][j] = 0;
            int[][] cellArrays = new int[Cell.getNProperCellTypes()][];
            int[][] cellIndices = new int[Cell.getNProperCellTypes()][];
            int[] cellArrayIndices = new int[Cell.getNProperCellTypes()];
            for (int i = 0; i < ncells; i++)
                cellTypeNumbers[cellmats[i]][celltypes[i]] += 1;
            int[] listNodes = new int[nListNodes];
            inStream.readFully(listNodes, 0, listNodes.length);
            FloatLargeArray coords = new FloatLargeArray(3 * (long)nnodes, false);
            float[] c = new float[nnodes];
            inStream.readFully(c, 0, c.length);
            for (int i = 0; i < c.length; i++)
                coords.setFloat(3 * i, c[i]);
            inStream.readFully(c, 0, c.length);
            for (int i = 0; i < c.length; i++)
                coords.setFloat(3 * i + 1, c[i]);
            inStream.readFully(c, 0, c.length);
            for (int i = 0; i < c.length; i++)
                coords.setFloat(3 * i + 2, c[i]);
            outField.setCurrentCoords(coords);
            DataArray[] dataArrays = null;
            if (nnodedata > 0) {
                dataArrays = readDataArrays(inStream, nnodes);
                for (int i = 0; i < dataArrays.length; i++)
                    outField.addComponent(dataArrays[i]);
            }
            int[] tNodes = new int[8];
            if (params.get(MATERIALS_AS_SETS)) {

            } else {
                for (int i = 0; i < Cell.getNProperCellTypes(); i++) {
                    int n = 0;
                    for (int j = 0; j < cellTypeNumbers.length; j++)
                        n += cellTypeNumbers[j][i];
                    cellArrayIndices[i] = 0;
                    cellArrays[i] = null;
                    if (n == 0)
                        continue;
                    cellArrays[i] = new int[n * CellType.getType(i).getNVertices()];
                    cellIndices[i] = new int[n];
                }
                for (int i = 0, j = 0; i < ncells; i++) {
                    int type = celltypes[i];
                    int nv = CellType.getType(type).getNVertices();
                    int m = cellArrayIndices[type];
                    for (int k = 0; k < tNodes.length; k++)
                        tNodes[k] = -1;
                    for (int l = 0; l < nv; l++, j++)
                        tNodes[UCDnodeOrders[type][l]] = listNodes[j] - 1;
                    System.arraycopy(tNodes, 0, cellArrays[type], m * nv, nv);
                    cellIndices[type][m] = i;
                    cellArrayIndices[type] = m + 1;
                }
                CellSet cs = new CellSet();
                for (int i = 0; i < Cell.getNProperCellTypes(); i++)
                    if (cellArrays[i] != null) {
                        byte[] orientations = new byte[cellIndices[i].length];
                        for (int j = 0; j < orientations.length; j++)
                            orientations[j] = 1;
                        cs.setCellArray(new CellArray(CellType.getType(i), cellArrays[i], orientations, cellIndices[i]));
                    }
                if (ncelldata > 0) {
                    dataArrays = readDataArrays(inStream, ncells);
                    for (int i = 0; i < dataArrays.length; i++)
                        cs.addComponent(dataArrays[i]);
                }
                cs.generateDisplayData(coords);
                outField.addCellSet(cs);
            }
            outField.setName(fn);
        } catch (FileNotFoundException e) {
            System.out.println("could not open file " + filename);
        } catch (Exception e) {
            FileErrorFrame.display("Error parsing UCD file ", filename, currentLine, e);
            return null;
        }
        return outField;
    }

    @Override
    public DataArray readEnSightGoldVariable(Parameters params, int nData, int veclen, String name, String filename)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
