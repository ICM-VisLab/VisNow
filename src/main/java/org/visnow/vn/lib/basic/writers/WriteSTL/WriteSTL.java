/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.writers.WriteSTL;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import org.jogamp.vecmath.Vector3f;
import org.apache.log4j.Logger;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.writers.WriteSTL.WriteSTLShared.*;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 * @author pregulski
 */
public class WriteSTL extends ModuleCore
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    protected GUI ui = null;
    protected IrregularField inField;
    private float[] normals, coords;
    private int[] indices;
    int nFacets = 0;

    private static final Logger LOGGER = Logger.getLogger(WriteSTL.class);

    public WriteSTL()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {

            @Override
            public void run()
            {
                ui = new GUI();
                setPanel(ui);
                ui.setParameters(parameters);
            }
        });

    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FILE_NAME, ""),
            new Parameter<>(ASCII, true),
            new Parameter<>(WRITE, false)
        };
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        ui.updateGUI(clonedParameterProxy, resetFully);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null) {
            return;
        }

        IrregularField newField = ((VNIrregularField) getInputFirstValue("inField")).getField();
        boolean isDifferentField = !isFromVNA() && (inField == null || inField.getTimestamp() != newField.getTimestamp());
        inField = newField;

        Parameters p = parameters.getReadOnlyClone();

        notifyGUIs(p, isFromVNA() || isDifferentField, true);

        if (isDifferentField || isFromVNA()) {
            createIndicesNormals();
        } else if (p.get(ASCII) && p.get(WRITE)) {
            createOutputASCII(p.get(FILE_NAME));
        } else if (p.get(WRITE)) {
            createOutputBinary(p.get(FILE_NAME));
        }
    }

    protected void createIndicesNormals()
    {
        normals = inField.getNormals() == null ? null : inField.getNormals().getData();
        coords = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords().getData();
        ArrayList<CellSet> cellSets = inField.getCellSets();
        indices = new int[0];
        for (CellSet cs : cellSets) {
            if (cs.getBoundaryCellArray(CellType.TRIANGLE) != null) {
                CellArray cellArray = cs.getBoundaryCellArray(CellType.TRIANGLE);
                int[] nodes = cellArray.getNodes();
                int[] tmp = new int[indices.length + nodes.length];
                System.arraycopy(indices, 0, tmp, 0, indices.length);
                System.arraycopy(nodes, 0, tmp, indices.length, nodes.length);
                indices = tmp;
            }
        }

        nFacets = indices.length / 3;
        if (normals == null || normals.length < (nFacets * 3)) {
            normals = new float[3 * nFacets];
            Vector3f v1, v2;
            for (int i = 0; i < nFacets; i++) {
                v1 = new Vector3f(coords[3 * indices[3 * i + 1] + 0] - coords[3 * indices[3 * i + 0] + 0],
                                  coords[3 * indices[3 * i + 1] + 1] - coords[3 * indices[3 * i + 0] + 1],
                                  coords[3 * indices[3 * i + 1] + 2] - coords[3 * indices[3 * i + 0] + 2]);
                v2 = new Vector3f(coords[3 * indices[3 * i + 2] + 0] - coords[3 * indices[3 * i + 0] + 0],
                                  coords[3 * indices[3 * i + 2] + 1] - coords[3 * indices[3 * i + 0] + 1],
                                  coords[3 * indices[3 * i + 2] + 2] - coords[3 * indices[3 * i + 0] + 2]);
                v1.cross(v1, v2);
                normals[3 * i] = v1.x;
                normals[3 * i] = v1.y;
                normals[3 * i] = v1.z;
            }
        }
    }

    protected void createOutputASCII(String path)
    {
        FileWriter outFile;
        try {
            outFile = new FileWriter(path, false);
            LOGGER.info("writing STL file: " + outFile.toString());
            String name = (new File(path)).getName();
            outFile.write("solid " + name + System.getProperty("line.separator")); //header

            for (int f = 0; f < nFacets; f++) {
                outFile.write("facet normal " + normals[3 * f] + " " + normals[3 * f + 1] + " " + normals[3 * f + 2] + System.getProperty("line.separator"));
                outFile.write("outer loop" + System.getProperty("line.separator"));
                outFile.write("vertex " + coords[3 * indices[3 * f + 0] + 0] + " " + coords[3 * indices[3 * f + 0] + 1] + " " + coords[3 * indices[3 * f + 0] + 2] + System.getProperty("line.separator"));
                outFile.write("vertex " + coords[3 * indices[3 * f + 1] + 0] + " " + coords[3 * indices[3 * f + 1] + 1] + " " + coords[3 * indices[3 * f + 1] + 2] + System.getProperty("line.separator"));
                outFile.write("vertex " + coords[3 * indices[3 * f + 2] + 0] + " " + coords[3 * indices[3 * f + 2] + 1] + " " + coords[3 * indices[3 * f + 2] + 2] + System.getProperty("line.separator"));
                outFile.write("endloop" + System.getProperty("line.separator"));
                outFile.write("endfacet" + System.getProperty("line.separator"));
            }

            outFile.write("endsolid " + name); //endFile
            outFile.close();
            LOGGER.info("written");
        } catch (IOException ex) {
            LOGGER.error("cannot write file", ex);
        }
    }

    protected void createOutputBinary(String path)
    {
        FileWriter outFile;
        try {
            LOGGER.info("writing STL file in binary mode ");

            String property = System.getProperty("file.encoding");
            System.setProperty("file.encoding", "ISO-8859-1");
            Field charset = Charset.class.getDeclaredField("defaultCharset");
            charset.setAccessible(true);
            charset.set(null, null);

            int capacity = 84 + nFacets * 50;

            ByteBuffer bb = ByteBuffer.allocate(capacity);

            bb.order(ByteOrder.LITTLE_ENDIAN);
            bb.putChar('V');

            for (int i = 1; i < 40; i++) {
                bb.putChar(' ');
            }

            byte[] bytes = ByteBuffer.allocate(4).putInt(nFacets).array();
            bb.put(bytes[3]);
            bb.put(bytes[2]);
            bb.put(bytes[1]);
            bb.put(bytes[0]);

            short anUnsignedShort = 0;
            for (int f = 0; f < nFacets; f++) {
                bb.putFloat(normals[3 * f]);
                bb.putFloat(normals[3 * f + 1]);
                bb.putFloat(normals[3 * f + 2]);
                bb.putFloat(coords[3 * indices[3 * f + 0] + 0]);
                bb.putFloat(coords[3 * indices[3 * f + 0] + 1]);
                bb.putFloat(coords[3 * indices[3 * f + 0] + 2]);
                bb.putFloat(coords[3 * indices[3 * f + 1] + 0]);
                bb.putFloat(coords[3 * indices[3 * f + 1] + 1]);
                bb.putFloat(coords[3 * indices[3 * f + 1] + 2]);
                bb.putFloat(coords[3 * indices[3 * f + 2] + 0]);
                bb.putFloat(coords[3 * indices[3 * f + 2] + 1]);
                bb.putFloat(coords[3 * indices[3 * f + 2] + 2]);
                bb.put((byte) ((anUnsignedShort & 0xFF00) >> 8));
                bb.put((byte) ((anUnsignedShort & 0x00FF)));
            }

            byte[] b = bb.array();
            outFile = new FileWriter(path, false);
            outFile.write(new String(b));
            outFile.close();

            System.setProperty("file.encoding", property);
            Field charset2 = Charset.class.getDeclaredField("defaultCharset");
            charset2.setAccessible(true);
            charset2.set(null, null);
            LOGGER.info("STL written");

        } catch (IOException ex) {
            LOGGER.error("cannot write file", ex);
        } catch (NoSuchFieldException ex) {
            LOGGER.error(ex);
        } catch (SecurityException ex) {
            LOGGER.error(ex);
        } catch (IllegalArgumentException ex) {
            LOGGER.error(ex);
        } catch (IllegalAccessException ex) {
            LOGGER.error(ex);
        }
    }
}
