//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.TableViewer.ExtendedTable;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Map;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import org.visnow.vn.gui.utils.DataSeriesChangedListener;
import org.visnow.vn.lib.basic.viewers.TableViewer.DataSet;
import org.visnow.vn.gui.utils.KeyListenerAdapter;

/**
 * Extended table.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 * @author norkap
 */
public class ExtendedTable implements DataSeriesChangedListener
{

    private final TableData data;
    private final ExtendedTablePanel tableGUI;
    private boolean oneDTab;

    public ExtendedTable(TableData data, ExtendedTablePanel tableGUI, boolean oneDTab)
    {
        this.data = data;
        this.tableGUI = tableGUI;
        this.oneDTab = oneDTab;
        
        tableGUI.addSeriesListener(this);
        tableGUI.addRemoveSeriesListener(new RemoveSeriesListener());
        tableGUI.addSaveAsCSVSeriesListener(new SaveAsCSVSeriesListener());
        tableGUI.addSaveAsHDF5SeriesListener(new SaveAsHDF5SeriesListener());
        tableGUI.addSeriesFormatChangedListener(new SeriesFormatChangedListener());
        tableGUI.addKeyPressedOnSeriesTableListener(new KeyListenerAdapter()
        {

            @Override
            public void keyPressed(KeyEvent e)
            {
                if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                    removeSeries();
                }
            }
        });
    }

    public ExtendedTablePanel getTablePanel()
    {
        return tableGUI;
    }
    
    public void setOneDTab(boolean oneDTab) {
        if(this.oneDTab != oneDTab) {
            this.oneDTab = oneDTab;
            update();
        }
    }

    public void setSeries(Map<String, DataSet> series)
    {
        data.setSeries(series);
    }

    public void update()
    {
        data.updateDisplayedSeries();
        tableGUI.initGUIComponents(data.getSeriesNames());
        tableGUI.update(data.getDisplayedSeries(), oneDTab);
        tableGUI.showTable();
    }

    private void addSeries(String[] names)
    {
        boolean res = true;
        boolean seriesAdded = false;
        if (data.getSeriesCount() > 0) {
            for (String name : names) {
                if (data.addDatasetToDisplayedSeries(name)) {
                    seriesAdded = true;
                }
            }
            if (seriesAdded) {
                tableGUI.update(data.getDisplayedSeries(), oneDTab);
            }
        }
    }

    private void addSeries()
    {
        if (data.getSeriesCount() > 0 && data.addDatasetToDisplayedSeries(tableGUI.getSelectedComponentName())) {
            tableGUI.update(data.getDisplayedSeries(), oneDTab);
        }
    }

    private void removeSeries()
    {
        String[] names = tableGUI.getRemovedSeriesNames();
        if (names != null && names.length > 0) {
            data.removeDatasetsFromDisplayedSeries(names);
            tableGUI.update(data.getDisplayedSeries(), oneDTab);
        }
    }
    
    private void saveSeries(String format)
    {
        String[] names = tableGUI.getRemovedSeriesNames();
        if (names != null && names.length > 0) {
            tableGUI.saveTables(data.getDisplayedSeries(), names, format);
        }
    }
    
    private void updateSeriesFormat(FormatEditor fEditor)
    {
        String f = tableGUI.getSeriesFormat(fEditor);
        data.getDisplayedSeries().get(tableGUI.getSelectedSeriesName()).setFormat(f);
        tableGUI.updateTable(data.getDisplayedSeries().get(tableGUI.getSelectedSeriesName()), oneDTab);
    }
    
    @Override
    public void addSeriesToDisplay(String[] names)
    {
        addSeries(names);
    }

    class AddSeriesActionListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            addSeries();
        }

    }

    class RemoveSeriesListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            removeSeries();
        }

    }
    
    class SaveAsCSVSeriesListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            saveSeries("csv");
        }

    }

    class SaveAsHDF5SeriesListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            saveSeries("hdf5");
        }

    }

    
    class SeriesFormatChangedListener implements CellEditorListener
    {

        @Override
        public void editingStopped(ChangeEvent e)
        {
            if (e.getSource() instanceof FormatEditor) {
                updateSeriesFormat((FormatEditor) e.getSource());
            }
        }

        @Override
        public void editingCanceled(ChangeEvent e)
        {

        }

    }

}
