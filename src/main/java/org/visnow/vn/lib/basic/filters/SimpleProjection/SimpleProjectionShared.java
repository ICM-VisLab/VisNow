/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.SimpleProjection;

import org.visnow.vn.engine.core.ParameterName;

/**
 * 
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */

public class SimpleProjectionShared
{ 
    static final String MAX  = "Maximum";
    static final String MIN  = "Minimum";
    static final String MEAN = "Average";
    static final String NORMALIZED_MEAN = "Normalized average";
    
    static final String[] METHODS_NAMES = {
        MAX, MIN, MEAN, NORMALIZED_MEAN 
    };
    
    static enum Axis {
        I, J, K
    };
    
    //Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    //
    //Specification:
    
    // String from METHOD_NAMES
    static final ParameterName<String> METHOD = new ParameterName("Projection method");
    
    static final ParameterName<Axis> AXIS_L0 = new ParameterName("Axis (first level of projection)");
    
    static final ParameterName<Axis> AXIS_L1 = new ParameterName("Axis (second level of projection)");
    // An Integer between 1 and 2.
    static final ParameterName<Integer> LEVELS = new ParameterName("Number of levels of projection");
    
    // Meta-parameters:
    static final ParameterName<Integer> META_NUMBER_OF_DIMENSIONS = new ParameterName("Number of dimensions of the current field");
}