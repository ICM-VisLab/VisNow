/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadVisNowField.utils;

/**
 *
 * @author know
 */


public enum FieldDescriptionKeywords
{
    FIELD_NAME       (new String[] {"field", "name"}),
    DIMENSIONS       (new String[] {"dim"}),
    EXTENTS          (new String[] {"ext", "xt"}),
    PHYSICAL_EXTENTS (new String[] {"phys"}),
    TIME_UNIT        (new String[] {"time"}),
    MASK             (new String[] {"mask", "valid"}),
    COORDS           (new String[] {"coord", "crd"}),
    COORD_UNIT       (new String[] {"unit", "coord unit", "crd unit"}),
    COMPONENT        (new String[] {"comp", "cmp"}),
    USER_DATA        (new String[] {"us"}),
    ORIGIN           (new String[] {"o"}),
    V0               (new String[] {"v0", "vi", "vx", "vu"}),
    V1               (new String[] {"v1", "vj", "vy", "vv"}),
    V2               (new String[] {"v2", "vk", "vz"}),
    R0               (new String[] {"r0", "rx", "ru"}),
    R1               (new String[] {"r1", "ry", "rv"}),
    R2               (new String[] {"r2", "rz"}),
    FILE             (new String[] {"file"}),
    CELL_SET         (new String[] {"cell"}),
    UNKNOWN          (new String[] {});

    private final String[] keys;

    private FieldDescriptionKeywords(String[] keys)
    {
        this.keys = keys;
    }
    public static FieldDescriptionKeywords getKeyword(String key)
    {
        for (FieldDescriptionKeywords k : FieldDescriptionKeywords.values())
            for (String s : k.keys)
               if (key.toLowerCase().startsWith(s))
                   return k;
        return UNKNOWN;
    }

}
