/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.AddMargins;

import java.util.Arrays;
import org.apache.log4j.Logger;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import static org.visnow.vn.lib.basic.filters.AddMargins.AddMarginsShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.jvia.spatialops.Padding;
import org.visnow.jvia.spatialops.Padding.PaddingType;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class AddMargins extends OutFieldVisualizationModule
{

    private static final Logger LOGGER = Logger.getLogger(AddMargins.class);

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI computeUI = null;
    protected RegularField inField = null;
    private int runQueue = 0;

    public AddMargins()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(MARGINS, new long[][]{{10, 10}, {0, 0}, {0, 0}}),
            new Parameter<>(PADDING_TYPE, PaddingType.ZERO),
            new Parameter<>(REFLECT_VECTORS, false),
            new Parameter<>(SELECTED_COMPONENTS, new int[]{}),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN),
            new Parameter<>(META_COMPONENT_NAMES, new String[]{})
        };
    }

    protected void extendedField(Parameters p)
    {
        try {
            outRegularField = Padding.padField(inField, p.get(SELECTED_COMPONENTS), p.get(MARGINS), parameters.get(PADDING_TYPE), parameters.get(REFLECT_VECTORS));
        } catch (Exception ex) {
            VisNow.get().userMessageSend(null, "Cannot create output field", ex.getMessage(), Level.ERROR);
        }
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        parameters.set(META_COMPONENT_NAMES, inField.getComponentNames());
        if (resetParameters && inField.getNComponents() > 0)
            parameters.set(SELECTED_COMPONENTS, new int[]{0});
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    @Override
    public void onActive()
    {
        LOGGER.debug("isFromVNA = " + isFromVNA());

        if (getInputFirstValue("inField") != null) {
            RegularField newField = ((VNRegularField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = !isFromVNA() && (inField == null || !Arrays.equals(inField.getComponentNames(), newField.getComponentNames()));
            boolean isNewField = !isFromVNA() && newField != inField;
            inField = newField;

            Parameters p;

            synchronized (parameters) {
                validateParamsAndSetSmart(isDifferentField);
                p = parameters.getReadOnlyClone();
            }

            notifyGUIs(p, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data
                extendedField(p);
                if (outRegularField == null) {
                    setOutputValue("outField", null);
                } else {
                    setOutputValue("outField", new VNRegularField(outRegularField));
                }
                outField = outRegularField;
                prepareOutputGeometry();
                show();
            }
        }
    }
}
