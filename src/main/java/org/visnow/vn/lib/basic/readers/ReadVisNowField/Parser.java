/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadVisNowField;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import org.visnow.vn.gui.widgets.FileErrorFrame;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.FieldIOSchema;
import org.visnow.vn.lib.utils.io.VNIOException;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Parser
{

    private LineNumberReader r;
    private String fileName;
    private File headerFile;
    private String line;
    private boolean rangesSet = false;
    private HeaderParser parser = null;

    public Parser(String fileName, boolean isURL, FileErrorFrame errorFrame) throws IOException
    {
        this.fileName = fileName;
        if (isURL) {
            URL url = new URL(fileName);
            URLConnection urlConnection = url.openConnection();
            r = new LineNumberReader(new InputStreamReader(urlConnection.getInputStream()));
        } else {
            headerFile = new File(fileName);
            r = new LineNumberReader(new FileReader(headerFile));
        }
        line = "";
    }

    public FieldIOSchema parseFieldHeader()
            throws VNIOException
    {
        FieldIOSchema out = null;
        try {
            line = r.readLine().trim();
            if (line.startsWith("#VisNow regular field")) {
                parser = new RegularFieldHeaderParser(r, headerFile, fileName);
                out = ((RegularFieldHeaderParser)parser).parseHeader();
            } else if (line.startsWith("#VisNow irregular field")) {
                parser = new IrregularFieldHeaderParser(r, headerFile, fileName);
                out = ((IrregularFieldHeaderParser)parser).parseHeader();
            } else if (line.startsWith("#VisNow point field")) {
                parser = new PointFieldHeaderParser(r, headerFile, fileName);
                out = ((PointFieldHeaderParser)parser).parseHeader();
            } else {
                throw new VNIOException("field description file should start with \"#VisNow (ir)regular field\" or \"#VisNow point field\"", fileName, 0);
            }
        } catch (IOException e) {
            throw new VNIOException("could not read", fileName, -1);
        }
        rangesSet = false;
        if(parser != null)
           rangesSet = parser.isRangesSet();
        return out;
    }

    /**
     * @return the rangesSet
     */
    public boolean isRangesSet() {
        return rangesSet;
    }

    public float[][] getPhysicalExtents()
    {
        return parser.getPhysicalExtents();
    }

    public boolean isUpdatePhysicalExtents()
    {
        return parser.isUpdatePhysicalExtents();
    }

}
