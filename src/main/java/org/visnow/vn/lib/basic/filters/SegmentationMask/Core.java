/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.SegmentationMask;

import org.visnow.jscic.dataarrays.ComplexDataArray;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.RegularField;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Core
{

    private Params params = new Params();
    private RegularField inField = null;
    private RegularField segmentationField = null;
    private RegularField outField = null;

    private float progress = 0.0f;

    public Core()
    {

    }

    public void update()
    {
        progress = 0.0f;
        if (inField == null || segmentationField == null) {
            outField = null;
            return;
        }

        int[] fieldDims = inField.getDims();
        int[] segmentationFieldDims = segmentationField.getDims();
        if (fieldDims.length != 3 || segmentationFieldDims.length != 3 || fieldDims[0] != segmentationFieldDims[0] || fieldDims[1] != segmentationFieldDims[1] || fieldDims[2] != segmentationFieldDims[2]) {
            outField = null;
            return;
        }

        String[] map = segmentationField.getComponent(0).getUserData();
        if (map == null || map.length < 2 || !map[0].equals("MAP")) {
            outField = null;
            return;
        }

        int fieldComponent = params.getSelectedComponent();
        int selectedSegmentation = params.getSelectedSegmentation();
        if (selectedSegmentation + 1 >= map.length) {
            outField = null;
            return;
        }
        String[] tmp = map[selectedSegmentation + 1].split(":");
        String segName = tmp[1];
        int segValue = 0;
        try {
            segValue = Integer.parseInt(tmp[0]);
        } catch (NumberFormatException ex) {
            outField = null;
            return;
        }

        outField = new RegularField(fieldDims);
        if (inField.getCurrentCoords() != null) {
            outField.setCurrentCoords(inField.getCurrentCoords());
        } else {
            outField.setAffine(inField.getAffine());
        }

        byte[] inSegmentationData = segmentationField.getComponent(0).getRawByteArray().getData();
        int progressStep = 1;

        switch (inField.getComponent(fieldComponent).getType()) {
            case FIELD_DATA_BYTE:
                byte[] inBData = (byte[])inField.getComponent(fieldComponent).getRawArray().getData();
                byte[] outBData = new byte[inBData.length];
                progressStep = (int) ceil((float) outBData.length / 50.0f);
                int bMinv = Integer.MAX_VALUE;
                for (int i = 0; i < outBData.length; i++) {
                    if (inSegmentationData[i] == (byte) segValue) {
                        outBData[i] = inBData[i];
                        if ((int) (inBData[i] & 0xFF) < bMinv)
                            bMinv = (int) (inBData[i] & 0xFF);
                    }
                    if (i % progressStep == 0) {
                        progress = (i + 1) * 0.5f / (float) outBData.length;
                        fireStatusChanged(progress);
                    }
                }

                for (int i = 0; i < outBData.length; i++) {
                    if (inSegmentationData[i] != (byte) segValue) {
                        outBData[i] = (byte) bMinv;
                    }
                    if (i % progressStep == 0) {
                        progress = 0.5f + (i + 1) * 0.5f / (float) outBData.length;
                        fireStatusChanged(progress);
                    }
                }
                outField.addComponent(DataArray.create(outBData, inField.getComponent(fieldComponent).getVectorLength(), inField.getComponent(fieldComponent).getName() + "_" + segName));
                break;
            case FIELD_DATA_SHORT:
            case FIELD_DATA_INT:
            case FIELD_DATA_FLOAT:
            case FIELD_DATA_DOUBLE:
                LargeArray inDData = inField.getComponent(fieldComponent).getRawArray();
                LargeArray outDData = LargeArrayUtils.create(inDData.getType(), inDData.length(), false);
                progressStep = (int) ceil((float) outDData.length() / 50.0f);
                double dMinv = Double.POSITIVE_INFINITY;
                for (int i = 0; i < outDData.length(); i++) {
                    if (inSegmentationData[i] == (byte) segValue) {
                        outDData.set(i, inDData.get(i));
                        if (inDData.getDouble(i) < dMinv)
                            dMinv = inDData.getDouble(i);
                    }
                    if (i % progressStep == 0) {
                        progress = (i + 1) * 0.5f / (float) outDData.length();
                        fireStatusChanged(progress);
                    }
                }
                for (int i = 0; i < outDData.length(); i++) {
                    if (inSegmentationData[i] != (byte) segValue) {
                        outDData.setDouble(i, dMinv);
                    }
                    if (i % progressStep == 0) {
                        progress = 0.5f + (i + 1) * 0.5f / (float) outDData.length();
                        fireStatusChanged(progress);
                    }
                }
                outField.addComponent(DataArray.create(outDData, inField.getComponent(fieldComponent).getVectorLength(), inField.getComponent(fieldComponent).getName() + "_" + segName));
                break;
            case FIELD_DATA_COMPLEX:
                float[] inFRealData = ((ComplexDataArray) inField.getComponent(fieldComponent)).getFloatRealArray().getFloatData();
                float[] inFImagData = ((ComplexDataArray) inField.getComponent(fieldComponent)).getFloatImaginaryArray().getFloatData();
                float[] outFRealData = new float[inFRealData.length];
                float[] outFImagData = new float[inFImagData.length];
                progressStep = (int) ceil((float) outFRealData.length / 50.0f);
                for (int i = 0; i < outFRealData.length; i++) {
                    if (inSegmentationData[i] == (byte) segValue) {
                        outFRealData[i] = inFRealData[i];
                        outFImagData[i] = inFImagData[i];
                    } else {
                        outFRealData[i] = 0;
                        outFImagData[i] = 0;
                    }
                    if (i % progressStep == 0) {
                        progress = (float) (i + 1) / (float) outFRealData.length;
                        fireStatusChanged(progress);
                    }
                }
                outField.addComponent(DataArray.create(new ComplexFloatLargeArray(new FloatLargeArray(outFRealData), new FloatLargeArray(outFImagData)), inField.getComponent(fieldComponent).getVectorLength(), inField.getComponent(fieldComponent).getName() + "_" + segName));
                break;
        }
        progress = 1.0f;
        fireStatusChanged(progress);
    }

    /**
     * @param inField the inField to set
     */
    public void setInField(RegularField inField)
    {
        this.inField = inField;
    }

    /**
     * @param segmentationField the segmentationField to set
     */
    public void setSegmentationField(RegularField segmentationField)
    {
        this.segmentationField = segmentationField;
    }

    /**
     * @return the outField
     */
    public RegularField getOutField()
    {
        return outField;
    }

    /**
     * @param params the params to set
     */
    public void setParams(Params params)
    {
        this.params = params;
    }

    private transient FloatValueModificationListener statusListener = null;

    public void addFloatValueModificationListener(FloatValueModificationListener listener)
    {
        if (statusListener == null) {
            this.statusListener = listener;
        } else {
            System.out.println("" + this + ": only one status listener can be added");
        }
    }

    private void fireStatusChanged(float status)
    {
        FloatValueModificationEvent e = new FloatValueModificationEvent(this, status, true);
        if (statusListener != null) {
            statusListener.floatValueChanged(e);
        }
    }

}
