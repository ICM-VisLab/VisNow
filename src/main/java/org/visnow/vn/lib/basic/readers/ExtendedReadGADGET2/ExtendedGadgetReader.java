/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ExtendedReadGADGET2;

import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.utils.SwingInstancer;
import static org.visnow.vn.lib.basic.readers.ExtendedReadGADGET2.ExtendedGadgetReaderShared.*;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNRegularField;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ExtendedGadgetReader extends OutFieldVisualizationModule
{

    protected GUI computeUI = null;
    protected RegularField outDensityField = null;
    public static OutputEgg[] outputEggs = null;
    public static InputEgg[] inputEggs = null;

    private ExtendedGadgetReaderCore core;

    public ExtendedGadgetReader()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        
        core = new ExtendedGadgetReaderCore(parameters);
        
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FILE_PATHS, new String[]{}),
            new Parameter<>(READ_VELOCITY, true),
            new Parameter<>(READ_ID, true),
            new Parameter<>(READ_TYPE, false),
            new Parameter<>(READ_MASS, false),
            new Parameter<>(READ_ENERGY, false),
            new Parameter<>(READ_DENSITY, false),
            new Parameter<>(READ_TEMPERATURE, false),
            new Parameter<>(DOWNSIZE, 1),
            new Parameter<>(SHOW, false),
            new Parameter<>(DENSITY_FIELD_DIMS, new int[]{64, 64, 64}),
            new Parameter<>(DENSITY_FIELD_LOG, true)
        };
    }

    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        Parameters p;
        synchronized (parameters) {
            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, false, false);
        
        if (p.get(FILE_PATHS) != null && p.get(FILE_PATHS).length > 0) {
            core.recalculate();

            //FIXME: this should be fixed - now field names may be mistaken (here and in setOutputValue)
            outIrregularField = (IrregularField) core.getOutField(ExtendedGadgetReaderCore.OUT_IFIELD_MAIN);
            outDensityField = (RegularField) core.getOutField(ExtendedGadgetReaderCore.OUT_RFIELD_DENSITY);

            computeUI.setFieldDescription(outIrregularField.description());
            setOutputValue(ExtendedGadgetReaderCore.OUT_IFIELD_MAIN, new VNIrregularField(outIrregularField));
            setOutputValue(ExtendedGadgetReaderCore.OUT_RFIELD_DENSITY, new VNRegularField(outDensityField));

            if (p.get(SHOW)) {
                outField = outIrregularField;
                prepareOutputGeometry();
                show();
            } else {
                outField = null;
                prepareOutputGeometry();
            }
        }
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag())
            computeUI.activateOpenDialog();
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

}
