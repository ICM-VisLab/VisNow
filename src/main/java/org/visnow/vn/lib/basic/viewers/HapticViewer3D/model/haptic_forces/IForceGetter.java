/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.haptic_forces;

import org.jogamp.vecmath.Vector3f;

/**
 * Returns read-only force parameters: force vector, general force clamp and flags: force enabled,
 * force blocked on drag, force blocked because of lack of data.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public interface IForceGetter
{

    public float getForceClamp();

    public float getForceScale();

    /**
     * Returns a recently computed force.
     * Even if force output is disabled (not to be mixed with disabling each of forces!), it will
     * return non-zero value.
     *
     * @param out_force (output parameter) resultant force - computed using forces there were
     *                  enabled.
     */
    public void getLastComputedForce(Vector3f out_force);

    /**
     * Return true if the value of a force was too big and needed to be clamped.
     */
    public boolean wasLastForceClamped();

    /**
     * Returns whether force output of the ForceContext, connected to the device context in
     * IForceGetter, is enabled.
     *
     * @return whether forces will be generated
     */
    public boolean isForceOutputEnabled();

    /**
     * Returns whether the force context connected to the device context in IForceGetter has blocked
     * force output because of inability to compute it (probably there is no link to haptic data in
     * the net of VisNow modules).
     *
     * @return true if blocked because of lack of data, false otherwise
     */
    public boolean isForceOutputDataBlocked();

    /**
     * Returns whether the force context connected to the device context in IForceGetter has blocked
     * force output because of scene being dragged by a haptic device.
     *
     * @return true if blocked because of dragging, false otherwise
     */
    public boolean isForceOutputDragBlocked();
}
