/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.testdata.TestCells;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class TestCells extends OutFieldVisualizationModule
{

    protected boolean fromUI = false;
    public static OutputEgg[] outputEggs = null;
    protected GUI computeUI = null;
    protected Params params;

    /**
     * Creates a new instance of TestGeometryObject
     */
    public TestCells()
    {
        parameters = params = new Params();
        params.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                    startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParams(params);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{};
    }
    
    @Override
    public boolean isGenerator()
    {
        return true;
    }

    public IrregularField createTestField()
    {
        int nPoints = 15;
        outIrregularField = new IrregularField(nPoints);
        float[] coords = new float[]{
            -1, -1, -1,
            1, -1, -1,
            1, 1, -1,
            -1, 1, -1,
            -1, -1, 1,
            1, -1, 1,
            1, 1, 1,
            -1, 1, 1,
            0, 2, -1,
            0, 2, 1,
            0, 1, 3,
            -1, -1, 3,
            -1, 1, 3,
            1, -1, 3,
            1, 1, 3
        };
        outIrregularField.setCurrentCoords(new FloatLargeArray(coords));
        
        if (params.isSingleSet()) {
            
            CellSet cs = new CellSet("3d cells");
            cs.addCells(new CellArray(CellType.HEXAHEDRON, new int[]{0, 1, 2, 3, 4, 5, 6, 7}, null, new int[]{0}));
            cs.addCells(new CellArray(CellType.PRISM, new int[]{2, 8, 3, 6, 9, 7}, null, new int[]{1}));
            cs.addCells(new CellArray(CellType.PYRAMID, new int[]{4, 5, 6, 7, 10}, null, new int[]{2}));
            cs.addCells(new CellArray(CellType.TETRA, new int[]{6, 7, 10, 9}, null, new int[]{3}));
            cs.addCells(new CellArray(CellType.QUAD, new int[]{4, 11, 12, 7}, null, new int[]{4}));
            cs.addCells(new CellArray(CellType.TRIANGLE, new int[]{10, 12, 11}, null, new int[]{5}));
            cs.addCells(new CellArray(CellType.SEGMENT, new int[]{11, 13}, null, new int[]{6}));
            cs.addCells(new CellArray(CellType.POINT, new int[]{14}, null, new int[]{7}));
            cs.addComponent(DataArray.create(new float[]{CellType.HEXAHEDRON.getValue(), 
                                                         CellType.PRISM.getValue(), 
                                                         CellType.PYRAMID.getValue(), 
                                                         CellType.TETRA.getValue(),
                                                         CellType.QUAD.getValue(), 
                                                         CellType.TRIANGLE.getValue(),
                                                         CellType.SEGMENT.getValue(), 
                                                         CellType.POINT.getValue()}, 1, "c"));
            for (DataArray da: cs.getComponents()) 
                da.recomputeStatistics();
            cs.generateDisplayData(new FloatLargeArray(coords));
            outIrregularField.addCellSet(cs);
        } else {
            CellSet cs = new CellSet("3d cells");
            cs.addCells(new CellArray(CellType.HEXAHEDRON, new int[]{0, 1, 2, 3, 4, 5, 6, 7}, null, new int[]{0}));
            cs.addCells(new CellArray(CellType.PRISM, new int[]{2, 8, 3, 6, 9, 7}, null, new int[]{1}));
            cs.addCells(new CellArray(CellType.PYRAMID, new int[]{4, 5, 6, 7, 10}, null, new int[]{2}));
            cs.addCells(new CellArray(CellType.TETRA, new int[]{6, 7, 10, 9}, null, new int[]{3}));
            cs.addComponent(DataArray.create(new float[]{CellType.HEXAHEDRON.getValue(), 
                                                         CellType.PRISM.getValue(), 
                                                         CellType.PYRAMID.getValue(), 
                                                         CellType.TETRA.getValue()}, 1, "c"));
            cs.addComponent(DataArray.create(new float[]{4, 0, 1, 2}, 1, "a"));
            for (DataArray da: cs.getComponents()) 
                da.recomputeStatistics();
            for (DataArray da: cs.getComponents()) 
                da.recomputeStatistics();
            cs.generateDisplayData(new FloatLargeArray(coords));
            outIrregularField.addCellSet(cs);
            CellSet cs1 = new CellSet("0, 1, 2d cells");
            cs1.addCells(new CellArray(CellType.QUAD, new int[]{4, 11, 12, 7}, null, new int[]{0}));
            cs1.addCells(new CellArray(CellType.TRIANGLE, new int[]{10, 12, 11}, null, new int[]{1}));
            cs1.addCells(new CellArray(CellType.SEGMENT, new int[]{11, 13}, null, new int[]{2}));
            cs1.addCells(new CellArray(CellType.POINT, new int[]{14}, null, new int[]{3}));
            cs1.addComponent(DataArray.create(new float[]{CellType.QUAD.getValue(), 
                                                          CellType.TRIANGLE.getValue(),
                                                          CellType.SEGMENT.getValue(), 
                                                          CellType.POINT.getValue()}, 1, "c"));
            cs.addComponent(DataArray.create(new float[]{4, 0, 1, 2}, 1, "b"));
            for (DataArray da: cs1.getComponents()) 
                da.recomputeStatistics();
            cs1.generateDisplayData(new FloatLargeArray(coords));
            outIrregularField.addCellSet(cs1);
        }

            outIrregularField.addComponent(DataArray.create(
                new float[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14}, 1, "nv"));
        outIrregularField.addComponent(DataArray.create(
                new float[]{0, 0, 0, 0, 4, 5, 6, 7, 15, 14, 13, 12, 11, 10, 9}, 1, "d0"));
        outIrregularField.setName("all_cells_field");
        return outIrregularField;
    }

    @Override
    public void onActive()
    {
        outField = outIrregularField = createTestField();
        setOutputValue("outField", new VNIrregularField(outIrregularField));
        prepareOutputGeometry();
        renderingParams.setShadingMode(RenderingParams.FLAT_SHADED);
        show();
    }
}
