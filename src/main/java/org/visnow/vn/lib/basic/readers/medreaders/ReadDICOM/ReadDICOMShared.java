/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.medreaders.ReadDICOM;

import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class ReadDICOMShared
{

    public static final int READ_AS_AUTO = 1;
    public static final int READ_AS_BYTES = 0;
    public static final int READ_AS_HISTOGRAM = 2;
    public static final int READ_AS_WINDOW = 3;

    public static final int VOXELSIZE_FROM_PIXELSIZE = 0;
    public static final int VOXELSIZE_FROM_SLICESDISTANCE = 1;
    public static final int VOXELSIZE_FROM_MANUALVALUE = 2;

    public static final int POSITION_ORIGINAL = 0;
    public static final int POSITION_CENTER = 1;
    public static final int POSITION_ZERO = 2;

    public static final int UNIT_MM = 0;
    public static final int UNIT_CM = 1;
    public static final int UNIT_M = 2;

    public static final ParameterName<String[]> FILE_LIST = new ParameterName("File list");
    public static final ParameterName<String> DIR_PATH = new ParameterName("Dir path");
    public static final ParameterName<String> PATIENT_NAME = new ParameterName("Patient name");
    public static final ParameterName<Integer> READ_AS = new ParameterName("Read as");
    public static final ParameterName<Integer> LOW = new ParameterName("Low");
    public static final ParameterName<Integer> HIGH = new ParameterName("High");
    public static final ParameterName<int[]> DOWNSIZE = new ParameterName("Downsize");
    public static final ParameterName<Boolean> INPAINT_MISSING_SLICES = new ParameterName("Inpaint missing slices");
    public static final ParameterName<Boolean> READ_AS_VOLUME = new ParameterName("Read as volume");
    public static final ParameterName<String> INFO_STRING = new ParameterName("Info string");
    public static final ParameterName<Boolean> INTERPOLATE_DATA = new ParameterName("Interpolate data");
    public static final ParameterName<Integer> INTERPOLATE_DATA_VOXEL_SIZE_FROM = new ParameterName("Interpolate data voxel size from");
    public static final ParameterName<Float> INTERPOLATE_DATA_VOXEL_SIZE_MANUAL_VALUE = new ParameterName("Interpolate data voxel size manual value");
    public static final ParameterName<Integer> LENGTH_UNIT = new ParameterName("Length unit");
    public static final ParameterName<Integer> POSITION = new ParameterName("Position");
    public static final ParameterName<Boolean> FRAMES_AS_TIME = new ParameterName("Frames as time");
    public static final ParameterName<int[]> FRAMES_RANGE = new ParameterName("Frames range");
    public static final ParameterName<Boolean> IGNORE_ORIENTATION = new ParameterName("Ingnore orietnation");
    public static final ParameterName<Integer> SLICE_DENOISING_LEVEL = new ParameterName("Slice denoising level");
    public static final ParameterName<Boolean> READ_SUBDIRECTORY = new ParameterName("Read subdirectory");

    public static Parameter[] getDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FILE_LIST, new String[0]),
            new Parameter<>(DIR_PATH, ""),
            new Parameter<>(PATIENT_NAME, ""),
            new Parameter<>(READ_AS, ReadDICOMShared.READ_AS_AUTO),
            new Parameter<>(LOW, -1024),
            new Parameter<>(HIGH, 1024),
            new Parameter<>(DOWNSIZE, new int[]{1, 1, 1}),
            new Parameter<>(INPAINT_MISSING_SLICES, false),
            new Parameter<>(READ_AS_VOLUME, true),
            new Parameter<>(INFO_STRING, ""),
            new Parameter<>(INTERPOLATE_DATA, false),
            new Parameter<>(INTERPOLATE_DATA_VOXEL_SIZE_FROM, VOXELSIZE_FROM_PIXELSIZE),
            new Parameter<>(INTERPOLATE_DATA_VOXEL_SIZE_MANUAL_VALUE, 1.0f),
            new Parameter<>(LENGTH_UNIT, VisNow.get()!=null ? ("meter".equalsIgnoreCase(VisNow.get().getMainConfig().getDefaultLengthUnit()) ? UNIT_M : "centimeter".equalsIgnoreCase(VisNow.get().getMainConfig().getDefaultLengthUnit()) ? UNIT_CM : UNIT_MM) : UNIT_MM),
            new Parameter<>(POSITION, POSITION_ORIGINAL),
            new Parameter<>(FRAMES_AS_TIME, false),
            new Parameter<>(FRAMES_RANGE, null),
            new Parameter<>(IGNORE_ORIENTATION, false),
            new Parameter<>(READ_SUBDIRECTORY, false),
            new Parameter<>(SLICE_DENOISING_LEVEL, 0)};
    }

}
