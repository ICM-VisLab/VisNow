/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.GaussianCurvature;

import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class GaussianCurvature extends OutFieldVisualizationModule
{

    /**
     * Creates a new instance of SurfaceSmoother
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected int nNodes;
    protected double[] angles;
    protected float[] areas;

    public GaussianCurvature()
    {
        setPanel(ui);
    }
    
    private void compute()
    {
        angles = new double[nNodes];
        areas  = new float[nNodes];
        
        for (int i = 0; i < nNodes; i++) {
            angles[i] = 2 * Math.PI;
            areas[i] = 0;
        }
        float[] coords = outIrregularField.getCurrentCoords().getData();
        int[] triangleNodes = outIrregularField.getCellSet(0).getBoundaryCellArray(CellType.TRIANGLE).getNodes();
        int[] nodes = new int[3];
        for (int triangle = 0; triangle < triangleNodes.length; triangle += 3) {
            System.arraycopy(triangleNodes, triangle, nodes, 0, 3);
            double[][] edges = new double[3][3];
            double[] lengths   = new double[3];
            double[] trAngles = new double[3];
            double area;
            for (int i = 0; i < 3; i++) {
                int k = nodes[i];
                int l = nodes[(i + 1) % 3];
                for (int j = 0; j < 3; j++) 
                {
                    double u = edges[i][j] = coords[3 * l + j] - coords[3 * k + j];
                    lengths[i] += u * u;
                }
                lengths[i] = Math.sqrt(lengths[i]);
            }
            double s = 0;
            double as = -Math.PI;
            for (int i = 0; i < 3; i++) {
                double u = 0;
                int k = i - 1; 
                if (k < 0)
                    k = 2;
                int l = (i + 1) % 3;
                for (int j = 0; j < 3; j++) 
                    u -= edges[k][j] * edges[l][j];
                trAngles[i] = u = Math.acos(u / (lengths[k] * lengths[l]));
                as += u;
                if (i == 0)
                    s = .5 * lengths[k] * lengths[l] * Math.sin(u);
                angles[nodes[i]] -= u;
                areas[nodes[i]] += s;
            }
//            System.out.println(""+as);
        }
        for (int i = 0; i < nNodes; i++)
            if (Math.abs(angles[i]) > 1)
                angles[i] = 0;
            else
                angles[i] /= areas[i];
        outIrregularField.addComponent(DataArray.create(angles, 1, "gaussian curvature"));
        outIrregularField.addComponent(DataArray.create(areas, 1, "star area"));
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null || ((VNField) getInputFirstValue("inField")).getField() == null)
            return;
        outIrregularField = ((VNField) getInputFirstValue("inField")).getField().getTriangulated();
        nNodes = (int)outIrregularField.getNNodes();
        compute();
        setOutputValue("outField", new VNIrregularField(outIrregularField));
        outField = outIrregularField;
        prepareOutputGeometry();
        show();
    }
}
