/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.ComponentOperations;

import java.util.Vector;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.gui.widgets.RunButton;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class ComponentOperationsShared
{

    public static final int NOOP = 0;
    public static final int BYTE = DataArrayType.FIELD_DATA_BYTE.getValue();
    public static final int BYTE_NORMALIZED = BYTE + DataArrayType.getMaxValue();
    public static final int SHORT = DataArrayType.FIELD_DATA_SHORT.getValue();
    public static final int SHORT_NORMALIZED = SHORT + DataArrayType.getMaxValue();
    public static final int INT = DataArrayType.FIELD_DATA_INT.getValue();
    public static final int FLOAT = DataArrayType.FIELD_DATA_FLOAT.getValue();
    public static final int DOUBLE = DataArrayType.FIELD_DATA_DOUBLE.getValue();
    public static final int LOG = 2 * DataArrayType.getMaxValue() + 1;
    public static final int ATAN = 2 * DataArrayType.getMaxValue() + 2;
    public static final int[] actionCodes = new int[]{NOOP, BYTE, BYTE_NORMALIZED, SHORT, SHORT_NORMALIZED, INT, FLOAT, DOUBLE, LOG, ATAN};
    public static final String[] actionNames = new String[]{"", "to byte", "byte normalize", "to short", "short normalize", "to int", "to float", "to double", "log", "atan"};

//Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    

    static final ParameterName<int[]> ACTIONS = new ParameterName("Actions");
    static final ParameterName<boolean[]> RETAIN = new ParameterName("Retain");
    static final ParameterName<Boolean> USE_COORDS = new ParameterName("UseCoords");
    static final ParameterName<Integer> NDIMS = new ParameterName("NDims");
    static final ParameterName<Integer> XCOORD_COMPONENT = new ParameterName("XCoordComponent");
    static final ParameterName<Integer> YCOORD_COMPONENT = new ParameterName("YCoordComponent");
    static final ParameterName<Integer> ZCOORD_COMPONENT = new ParameterName("ZCoordComponent");
    static final ParameterName<Boolean> ADD_INDEX_COMPONENT = new ParameterName("AddIndexComponent");
    static final ParameterName<Float> XCOORD_SCALE_VALUE = new ParameterName("XCoordScaleValue");
    static final ParameterName<Float> YCOORD_SCALE_VALUE = new ParameterName("YCoordScaleValue");
    static final ParameterName<Float> ZCOORD_SCALE_VALUE = new ParameterName("ZCoordScaleValue");
    static final ParameterName<Float> XVAR_SHIFT = new ParameterName("XVarShift");
    static final ParameterName<Float> YVAR_SHIFT = new ParameterName("YVarShift");
    static final ParameterName<Float> ZVAR_SHIFT = new ParameterName("ZVarShift");
    static final ParameterName<Float> XCOORD_SHIFT = new ParameterName("XCoordShift");
    static final ParameterName<Float> YCOORD_SHIFT = new ParameterName("YCoordShift");
    static final ParameterName<Float> ZCOORD_SHIFT = new ParameterName("zCoordShift");
    static final ParameterName<double[]> MIN = new ParameterName("Min");
    static final ParameterName<double[]> MAX = new ParameterName("Max");
    static final ParameterName<Vector<VectorComponent>> VECTOR_COMPONENTS = new ParameterName("VectorComponents");
    static final ParameterName<boolean[]> VCNORMS = new ParameterName("VCNorms");
    static final ParameterName<boolean[]> VCNORMALIZE = new ParameterName("VCNormalize");
    static final ParameterName<boolean[]> VCSPLIT = new ParameterName("VCSplit");
    static final ParameterName<Boolean> FIX3D = new ParameterName("Fix3D");
    static final ParameterName<Integer> MASK_COMPONENT = new ParameterName("MaskComponent");
    static final ParameterName<Boolean> RECOMPUTE_MIN_MAX = new ParameterName("RecomputeMinMax");
    static final ParameterName<float[]> MASK_MIN_MAX = new ParameterName("MaskMinMax");
    static final ParameterName<float[]> MASK_LOW_UP = new ParameterName("MaskLowUp");
    static final ParameterName<Boolean> ADD_TO_MASK = new ParameterName("AddToMask");
    static final ParameterName<Vector<ComplexComponent>> COMPLEX_COMBINE_COMPONENTS = new ParameterName("ComplexCombineComponents");
    static final ParameterName<boolean[]> COMPLEX_SPLIT_RE = new ParameterName("ComplexSplitRe");
    static final ParameterName<boolean[]> COMPLEX_SPLIT_IM = new ParameterName("ComplexSplitIm");
    static final ParameterName<boolean[]> COMPLEX_SPLIT_ABS = new ParameterName("ComplexSplitAbs");
    static final ParameterName<boolean[]> COMPLEX_SPLIT_ARG = new ParameterName("ComplexSplitArg");
    static final ParameterName<RunButton.RunState> RUNNING_MESSAGE = new ParameterName<>("Running message");

    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic)    
    //not null array of Strings, may be empty
    static final ParameterName<String[]> META_COMPONENT_NAMES = new ParameterName("META component names");
    static final ParameterName<int[]> META_COMPONENT_VECLEN = new ParameterName("META component veclen");
    static final ParameterName<DataArrayType[]> META_COMPONENT_TYPES = new ParameterName("META component types");
    static final ParameterName<DataContainerSchema> META_SCHEMA = new ParameterName("META field schema");
    static final ParameterName<float[][]> META_EXTENDS = new ParameterName("META field extends");
    static final ParameterName<Boolean> META_IS_REGULAR_FIELD = new ParameterName("META is regular field");

}
