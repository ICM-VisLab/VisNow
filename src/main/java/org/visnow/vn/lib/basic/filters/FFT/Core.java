/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.FFT;

import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.lib.utils.fft.FftCore;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Core
{

    private RegularField inField = null;
    private RegularField outField = null;
    private FftCore fourier;

    public void update(boolean forward, boolean center)
    {
       if (inField == null) {
            outField = null;
            return;
        }

        outField = new RegularField(inField.getDims());
        if (inField.getCurrentCoords() == null) {
            outField.setAffine(inField.getAffine());
        } else {
            outField.setCurrentCoords(inField.getCurrentCoords());
        }

        fourier = FftCore.loadFftLibrary();

        for (int i = 0; i < inField.getNComponents(); i++) {
            if (inField.getComponent(i).getVectorLength() > 1) {
                continue;
            }

            fourier.setInput(inField.getComponent(i), inField.getLDims());

            if (forward) {
                fourier.calculateFFT(center);
                DataArray fft = fourier.getOutput();
                fft.setName("FT_" + inField.getComponent(i).getName());
                outField.addComponent(fft);
            } else {
                fourier.calculateIFFT(center);
                DataArray ifft = fourier.getOutput();
                ifft.setName("IFT_" + inField.getComponent(i).getName());
                outField.addComponent(ifft);
            }
        }
    }

    public void setInField(RegularField field)
    {
        this.inField = field;
    }

    /**
     * @return the outField
     */
    public RegularField getOutField()
    {
        return outField;
    }
}
