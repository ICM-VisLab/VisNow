/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.IntensityEqualizer;

import static org.apache.commons.math3.util.FastMath.min;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.ProgressAgent;
import org.visnow.jscic.utils.ScalarMath;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.lib.utils.field.subset.FieldSample;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class IntensityEqualizerCore
{
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(IntensityEqualizerCore.class);

    private float progress = 0.0f;
    private RegularField inProcessedField = null;
    private RegularField outField = null;
    private int component = -1;

    private float[][] levels = null;
    private float mean = 0;
    private int[] outDims = null;
    private int[] cropIndicesLow = null;
    private int[] cropIndicesUp = null;
    

    private final GaussianPyramid gp = new GaussianPyramid();
    private ProgressAgent progressAgent = null;

    /**
     * Creates the instance of IntensityEqualizerCore.
     * IntensityEqualizerCore uses Gauss/Laplace pyramid to reconstruct the data with different 
     * weights of pyramid componnets. 
     */
    public IntensityEqualizerCore()
    {

    }

    /**
     * Sets input field and pareameters.
     * @param inField - Input field.
     * @param component - Index oof field component to process. It has to be a numeric non-complex scalar (veclen=1) component.  
     */
    public void setInField(RegularField inField, int component)
    {
        setInField(inField, component, null);
    }
    
    /**
     * Sets input field and pareameters.
     * @param inField - Input field.
     * @param component - Index oof field component to process. It has to be a numeric non-complex scalar (veclen=1) component.  
     * @param progressAgent - Reference to ProgressAgent object for computation progress monitoring.
     */
    public void setInField(RegularField inField, int component, ProgressAgent progressAgent)
    {
        this.component = component;
        this.inProcessedField = preprocess(inField);
        this.progressAgent = progressAgent;
        updatePyramid();
    }

    /**
     * Returns field with equalized components.
     * @return - resulting field.
     */
    public RegularField getOutField()
    {
        return outField;
    }

    private void updatePyramid()
    {
        if (inProcessedField == null) {
            outField = null;
            return;
        }
    
        progress = 0.0f;
        if(progressAgent != null) progressAgent.setProgress(progress);

        int nLevels = estimateNLevels(inProcessedField.getDims());
        log.debug("calculating " + nLevels + " levels");

        levels = new float[nLevels][];

        DataStruct G0 = new DataStruct(inProcessedField.getComponent(0).getRawFloatArray().getData(), inProcessedField.getDims());
        if (G0 == null)
            return;

        outDims = G0.getDims();

        DataStruct G = G0;
        DataStruct G1 = gp.reduce(G0.getData(), G0.getDims());

        progress = 1.0f / (float) nLevels;
        if(progressAgent != null) progressAgent.setProgress(progress);

        for (int i = 0; i < nLevels - 1; i++) {
            DataStruct tmpG = substract(G, gp.expand(G1.getData(), G1.getDims()));
            for (int j = 0; j < i; j++) {
                tmpG = gp.expand(tmpG.getData(), tmpG.getDims());
            }
            levels[i] = tmpG.getData();

            G = G1;

            if (i == nLevels - 2)
                break;
            G1 = gp.reduce(G1.getData(), G1.getDims());
            tmpG = null;
            progress = (float) (i + 1) / (float) nLevels;
            if(progressAgent != null) progressAgent.setProgress(progress);
        }

        DataStruct tmpG = subtractMean(G);
        //DataStruct tmpG = G;
        for (int j = 0; j < nLevels - 1; j++) {
            tmpG = gp.expand(tmpG.getData(), tmpG.getDims());
        }
        levels[nLevels - 1] = tmpG.getData();
        progress = 1.0f;
        if(progressAgent != null) progressAgent.setProgress(progress);
    }

    /**
     * Reconstructs the output field from a pre-build pyramid using the provided parameteers.
     * @param weights - array of reconstruction weights for each pyramid level.
     * @param gain - global multiplier.
     * @param crop - flag if the output field should be cropped to match input field size.
     * @param keepLevels - flag if pyramid levels should be present in the output field.
     */
    public void updateOutput(float[] weights, float gain, boolean crop, boolean keepLevels)
    {  
        if (inProcessedField == null || levels == null || weights == null || weights.length > estimateNLevels(inProcessedField.getDims())) {
            return;
        }

        int N = outDims[0] * outDims[1];
        if (outDims.length == 3) {
            N = N * outDims[2];
        }
        float[] out = new float[N];

        for (int i = 0; i < N; i++) {
            out[i] = mean;
        }

        for (int j = 0; j < weights.length; j++) {
            for (int i = 0; i < N; i++) {
                out[i] += gain * weights[j] * levels[j][i];
            }
            progress = 0.5f * (float) (j + 1) / (float) (weights.length);
            if(progressAgent != null) progressAgent.setProgress(progress);
        }

        progress = 0.5f;
        if(progressAgent != null) progressAgent.setProgress(progress);

        outField = new RegularField(outDims);
        outField.setAffine(inProcessedField.getAffine());


        float inMax = (float) inProcessedField.getComponent(0).getPreferredMaxValue();
        float inMin = (float) inProcessedField.getComponent(0).getPreferredMinValue();
        for (int i = 0; i < N; i++) {
            if (out[i] < inMin)
                out[i] = inMin;
            else if (out[i] > inMax)
                out[i] = inMax;
        }
        
        LargeArray outLA = LargeArrayUtils.create(inProcessedField.getComponent(0).getRawArray().getType(), N);
        for (int i = 0; i < out.length; i++) {            
            outLA.setFloat(i, out[i]);        
            if (i % 1000 == 0) {
                   progress = 0.5f + 0.5f * (float) (i + 1) / (float) (N);
                   if(progressAgent != null) progressAgent.setProgress(progress);
               }            
        }
        outField.addComponent(DataArray.create(outLA, 1, 
                        inProcessedField.getComponent(0).getName() + "_eq", 
                        inProcessedField.getComponent(0).getUnit(),
                        inProcessedField.getComponent(0).getUserData())
        );

        if(keepLevels) {
            for (int i = 0; i < weights.length; i++) {
                outField.addComponent(DataArray.create(levels[i], 1, "level" + i));
            }
        }
        
        if(crop) {
            outField = FieldSample.regularSample(outField, new int[]{1,1,1}, cropIndicesLow, cropIndicesUp, true);            
        }
        
        progress = 1.0f;
        if(progressAgent != null) progressAgent.setProgress(progress);
    }

    private DataStruct substract(DataStruct G, DataStruct G1)
    {
        if (G == null || G1 == null)
            return null;

        int[] dims = G.getDims();
        int[] dims1 = G1.getDims();
        if (dims.length != dims1.length)
            return null;

        for (int i = 0; i < dims1.length; i++) {
            if (dims[i] != dims1[i])
                return null;
        }

        float[] in = G.getData();
        float[] in1 = G1.getData();
        float[] out = null;
        if (dims.length == 2) {
            out = new float[dims[0] * dims[1]];
        } else if (dims.length == 3) {
            out = new float[dims[0] * dims[1] * dims[2]];
        }

        int nThreads = org.visnow.vn.system.main.VisNow.availableProcessors();
        SubstractWorker[] workers = new SubstractWorker[nThreads];
        for (int i = 0; i < workers.length; i++) {
            workers[i] = new SubstractWorker(i, nThreads, in, in1, out);
            workers[i].start();
        }

        for (int i = 0; i < workers.length; i++) {
            try {
                workers[i].join();
                workers[i] = null;
            } catch (InterruptedException ex) {
                return null;
            }
        }

        return new DataStruct(out, dims);
    }

    private class SubstractWorker extends Thread
    {

        private final int iThread;
        private final int nThreads;
        private final float[] data1;
        private final float[] data2;
        private final float[] out;

        public SubstractWorker(int iThread, int nThreads, float[] data1, float[] data2, float[] out)
        {
            this.iThread = iThread;
            this.nThreads = nThreads;
            this.data1 = data1;
            this.data2 = data2;
            this.out = out;
        }

        @Override
        public void run()
        {
            log.debug("started substract worker " + (iThread + 1) + " of " + nThreads);

            if (data1 == null || data2 == null || out == null)
                return;

            for (int i = iThread; i < out.length; i += nThreads) {
                out[i] = data1[i] - data2[i];
            }

            log.debug("finished substract worker " + (iThread + 1) + " of " + nThreads);
        }
    }

    private DataStruct subtractMean(DataStruct G)
    {
        if (G == null)
            return null;

        int[] dims = G.getDims();
        float[] in = G.getData();
        float[] out = null;
        if (dims.length == 2) {
            out = new float[dims[0] * dims[1]];
        } else {
            out = new float[dims[0] * dims[1] * dims[2]];
        }

        mean = 0;
        for (int i = 0; i < out.length; i++) {
            mean += in[i];
        }
        mean = mean / (float) out.length;
        for (int i = 0; i < out.length; i++) {
            out[i] = in[i] - mean;
        }
        return new DataStruct(out, dims);
    }

    private RegularField preprocess(RegularField inField)
    {
        if (inField == null) {
            return null;
        }

        if (inField.getDims().length < 2 || inField.getDims().length > 3) {
            return null;
        }
        
        if(inField.getComponent(component) == null || 
                inField.getComponent(component).getVectorLength() != 1 || 
                !inField.getComponent(component).isNumeric() || 
                inField.getComponent(component).getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            return null;
        }  
        
        int[] inDims = inField.getDims();
        int[] outDimsL = new int[inDims.length];
        int[] offsets = {0, 0, 0};
        cropIndicesLow = new int[] {0,0,0};
        cropIndicesUp = new int[] {0,0,0};
        int N = 1;

        int max = 0;
        for (int i = 0; i < outDimsL.length; i++) {
            outDimsL[i] = ScalarMath.nextPower2(inDims[i]);
            if (outDimsL[i] > max) max = outDimsL[i];
        }
        for (int i = 0; i < outDimsL.length; i++) {
            outDimsL[i] = max;
        }

        for (int i = 0; i < outDimsL.length; i++) {
            offsets[i] = (outDimsL[i] - inDims[i]) / 2;
            cropIndicesLow[i] = offsets[i];
            cropIndicesUp[i] = offsets[i]+inDims[i];
            log.debug("old dims[" + i + "]=" + inDims[i]);
            log.debug("new dims[" + i + "]=" + outDimsL[i]);
            log.debug("offsets[" + i + "]=" + offsets[i]);
            N = N * outDimsL[i];
        }

        RegularField out = new RegularField(outDimsL);

        int ii, jj, kk;
        LargeArray bData = inField.getComponent(component).getRawArray();
        LargeArray bOut = LargeArrayUtils.create(bData.getType(), N, false);
        if (inDims.length == 2) {
            for (int j = 0; j < outDimsL[1]; j++) {
                jj = j - offsets[1];
                for (int i = 0; i < outDimsL[0]; i++) {
                    ii = i - offsets[0];
                    if (ii < 0 || jj < 0 || ii >= inDims[0] || jj >= inDims[1])
                        bOut.set(j * outDimsL[0] + i, 0);
                    else
                        bOut.set(j * outDimsL[0] + i, bData.get(jj * inDims[0] + ii));
                }
            }
        } else if (inDims.length == 3) {
            for (int k = 0; k < outDimsL[2]; k++) {
                kk = k - offsets[2];
                for (int j = 0; j < outDimsL[1]; j++) {
                    jj = j - offsets[1];
                    for (int i = 0; i < outDimsL[0]; i++) {
                        ii = i - offsets[0];
                        if (ii < 0 || jj < 0 || kk < 0 || ii >= inDims[0] || jj >= inDims[1] || kk >= inDims[2])
                            bOut.set(k * outDimsL[0] * outDimsL[1] + j * outDimsL[0] + i, 0);
                        else
                            bOut.set(k * outDimsL[0] * outDimsL[1] + j * outDimsL[0] + i, bData.get(kk * inDims[0] * inDims[1] + jj * inDims[0] + ii));
                    }
                }
            }
        }
        out.addComponent(DataArray.create(bOut, 1, inField.getComponent(component).getName(), inField.getComponent(component).getUnit(), inField.getComponent(component).getUserData()));
        float[][] inAffine = inField.getAffine();
        float[][] outAffine = new float[4][];
        outAffine[0] = inAffine[0];
        outAffine[1] = inAffine[1];
        outAffine[2] = inAffine[2];
        outAffine[3] = new float[3];
        if (inDims.length == 2)
            for (int i = 0; i < 3; i++) {
                outAffine[3][i] = inAffine[3][i] - offsets[0] * inAffine[0][i] - offsets[1] * inAffine[1][i];
            }
        else if (inDims.length == 3)
            for (int i = 0; i < 3; i++) {
                outAffine[3][i] = inAffine[3][i] - offsets[0] * inAffine[0][i] - offsets[1] * inAffine[1][i] - offsets[2] * inAffine[2][i];
            }
        out.setAffine(outAffine);
        return out;
    }
    
    /**
     * Estimates the number of pyramid levels for a given field dimensions.
     * @param dims - field dimensions.
     * @return number of possible pyramid levels
     */
    public static int estimateNLevels(int[] dims) {
        int[] outDimsL = new int[dims.length];
        int max = 0;
        for (int i = 0; i < outDimsL.length; i++) {
            outDimsL[i] = ScalarMath.nextPower2(dims[i]);
            if(outDimsL[i] > max) max = outDimsL[i];
        }
        int n = (31 - Integer.numberOfLeadingZeros(max));
        if (n < 1)
            n = 1;        
        return n;
    }
}
