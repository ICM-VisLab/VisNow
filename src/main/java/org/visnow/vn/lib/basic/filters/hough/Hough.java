/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.hough;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.types.VNRegularField;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Hough extends ModuleCore
{

    private GUI ui = new GUI();
    private Core core = new Core();
    protected Params params;
    RegularField inField = null;

    public Hough()
    {
        parameters = params = new Params();
        ui.setParams(params);
        core.setParams(params);
        params.addChangeListener(new ChangeListener()
        {

            public void stateChanged(ChangeEvent evt)
            {
                startAction();
            }
        });
        core.addChangeListener(new ChangeListener()
        {

            public void stateChanged(ChangeEvent evt)
            {
                Hough.this.setProgress(core.getProgress());
            }
        });
        setPanel(ui);
        //WTF-MUI:addModuleUI(ui);
    }

    private static InputEgg[] inputEggs = null;
    private static OutputEgg[] outputEggs = null;

    public static InputEgg[] getInputEggs()
    {
        if (inputEggs == null) {
            inputEggs = new InputEgg[]{
                new InputEgg("inField", VNRegularField.class, InputEgg.NECESSARY | InputEgg.TRIGGERING | InputEgg.NORMAL),};
        }
        return inputEggs;
    }

    public static OutputEgg[] getOutputEggs()
    {
        if (outputEggs == null) {
            outputEggs = new OutputEgg[]{
                new OutputEgg("outField", VNRegularField.class),};
        }
        return outputEggs;
    }

    @Override
    public void onActive()
    {

        if (getInputFirstValue("inField") == null) {
            return;
        }
        RegularField inFld = ((VNRegularField) getInputFirstValue("inField")).getField();
        if (inFld == null) {
            return;
        }
        if (inFld != inField) {
            inField = inFld;
            ui.setInField(inField);
            core.setInField(inField);
        }
        core.update();

        setOutputValue("outField", new VNRegularField(core.getOutField()));
    }
}
