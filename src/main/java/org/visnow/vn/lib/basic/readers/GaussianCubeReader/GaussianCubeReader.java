/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.GaussianCubeReader;


import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.readers.GaussianCubeReader.GaussianCubeShared.*;
import org.visnow.vn.lib.basic.readers.ReaderParams;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.io.VolumeReader;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 * 
 * new flow added by Norbert Kapinski (norkap@icm.edu.pl)
 */
public class GaussianCubeReader extends OutFieldVisualizationModule
{

    protected GUI computeUI = null;
    protected boolean fromGUI = false;
    public static OutputEgg[] outputEggs = null;

    /**
     * Creates a new instance of CreateGrid
     */
    public GaussianCubeReader()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI("Gaussian cube file", "Gaussian cube", "cub", "cube");
                computeUI.setParameters(parameters);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });

    }
    
    
     @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FILENAME, ""),
        };
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }
    
    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        Parameters p = parameters.getReadOnlyClone();
        notifyGUIs(p, isFromVNA(), false);
        
         if (p.get(FILENAME).isEmpty()) outRegularField = null;
         else{
            outRegularField = VolumeReader.readGaussianCube(p.get(FILENAME));
         }
         if (outRegularField == null)
            return;

        setOutputValue("outField", new VNRegularField(outRegularField));
        outField = outRegularField;
        prepareOutputGeometry();
        show();
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag())
            computeUI.activateOpenDialog();
    }

}
