/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadVisNowField.utils;

import org.visnow.jscic.CellSet;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ComponentIOSchema extends DataElementIOSchema
{

    protected int component = 0;
    protected String cmpName = null;

    public ComponentIOSchema(DataContainer dataset, int component, int coord, DataArrayType type, int veclen, int nData, int offsetFrom, int offsetTo)
    {
        super(dataset, component, coord, type, veclen, nData, offsetFrom, offsetTo);
        this.component = component;
    }

    @Override
    public String toString()
    {
        StringBuilder s = new StringBuilder("[");
        if (cmpName == null)
            s.append("cmp ");
        if (dataset instanceof CellSet)
            s.append("").append(((CellSet) dataset).getName()).append(".");
        if (cmpName == null)
            s.append("").append(component);
        else
            s.append(cmpName);
        if (coord != -1)
            s.append(".").append(coord).append(" ");
        s.append(" ").append(type.toString());
        s.append(" off ").append(offsetFrom);
        if (offsetTo != -1)
            s.append("-" + offsetTo);
        return s.toString() + "]";
    }

    /**
     * Get the value of component
     *
     * @return the value of component
     */
    public int getComponent()
    {
        return component;
    }

    public String getCmpName()
    {
        return cmpName;
    }

    public void setComponent(int component)
    {
        this.component = component;
    }

    public void setCmpName(String cmpName)
    {
        this.cmpName = cmpName;
    }
}
