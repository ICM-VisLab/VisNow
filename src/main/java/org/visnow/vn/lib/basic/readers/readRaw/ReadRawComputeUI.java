/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.readRaw;

import java.io.File;
import java.util.Arrays;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import org.apache.log4j.Logger;
import org.visnow.vn.engine.core.ParameterProxy;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.gui.swingwrappers.TextField;
import org.visnow.vn.gui.widgets.RunButton;
import static org.visnow.vn.lib.basic.readers.readRaw.ReadRawShared.*;
import org.visnow.vn.lib.utils.StringUtils;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.swing.UIStyle;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class ReadRawComputeUI extends javax.swing.JPanel
{
    private static final Logger LOGGER = Logger.getLogger(ReadRawComputeUI.class);

    private Parameters parameters;
    private JFileChooser fileChooser;

    /**
     * Creates new form ModuleTemplateGUI
     */
    public ReadRawComputeUI()
    {
        initComponents();
    }

    void setParameters(Parameters parameters)
    {
        this.parameters = parameters;
    }

    void updateGUI(ParameterProxy p, boolean resetFully, boolean setRunButtonPending)
    {
        fileSizeValueLabel.setText(p.get(META_FILE_SIZE_INFO));

        String fileNames = StringUtils.join(p.get(FILE_NAMES), File.pathSeparator);
        if (!fileNameTF.getText().equals(fileNames))
            fileNameTF.setText(fileNames);
        typeCB.setSelectedItem(p.get(NUMBER_TYPE));
        endianCB.setSelectedItem(p.get(ENDIANNESS));
        alignmentTF.setValue(p.get(IN_FILE_ALIGNMENT));
        dim1DRB.setSelected(p.get(NUMBER_OF_DIMENSIONS) == 1);
        dim2DRB.setSelected(p.get(NUMBER_OF_DIMENSIONS) == 2);
        dim3DRB.setSelected(p.get(NUMBER_OF_DIMENSIONS) == 3);
        inputDimSlider1.setValue(p.get(DATA_DIMS)[0]);
        inputDimSlider2.setValue(p.get(DATA_DIMS)[1]);
        inputDimSlider3.setValue(p.get(DATA_DIMS)[2]);

        updateSliderVisibility();
        runButton.updateAutoState(p.get(RUNNING_MESSAGE));
        runButton.updatePendingState(setRunButtonPending);
    }

    private void updateSliderVisibility()
    {
        zPanel.setVisible(dim3DRB.isSelected());
        yPanel.setVisible(dim2DRB.isSelected() || dim3DRB.isSelected());
    }

    
    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        dimensionsButtonGroup = new javax.swing.ButtonGroup();
        multipleFilesButtonGroup = new javax.swing.ButtonGroup();
        uIStyleFakeInitializer1 = new org.visnow.vn.system.swing.UIStyleFakeInitializer();
        sectionHeader1 = new org.visnow.vn.gui.widgets.SectionHeader();
        inputPanel = new javax.swing.JPanel();
        filePanel = new javax.swing.JPanel();
        fileNameTF = new org.visnow.vn.gui.swingwrappers.TextField();
        browseButton = new javax.swing.JButton();
        typePanel = new javax.swing.JPanel();
        typeLabel = new javax.swing.JLabel();
        typeCB = new org.visnow.vn.gui.swingwrappers.ComboBox();
        endianPanel = new javax.swing.JPanel();
        endianLabel = new javax.swing.JLabel();
        endianCB = new org.visnow.vn.gui.swingwrappers.ComboBox();
        fileSizePanel = new javax.swing.JPanel();
        fileSizeLabel = new javax.swing.JLabel();
        fileSizeValueLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        skipBytesPanel = new javax.swing.JPanel();
        alignmentLabel = new javax.swing.JLabel();
        alignmentTF = new org.visnow.vn.gui.components.NumericTextField();
        jLabel1 = new javax.swing.JLabel();
        sectionHeader2 = new org.visnow.vn.gui.widgets.SectionHeader();
        jPanel1 = new javax.swing.JPanel();
        dimensionsPanel = new javax.swing.JPanel();
        dim1DRB = new org.visnow.vn.gui.swingwrappers.RadioButton();
        dim2DRB = new org.visnow.vn.gui.swingwrappers.RadioButton();
        dim3DRB = new org.visnow.vn.gui.swingwrappers.RadioButton();
        xPanel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        inputDimSlider1 = new org.visnow.vn.gui.widgets.ExtendedSlider();
        yPanel = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        inputDimSlider2 = new org.visnow.vn.gui.widgets.ExtendedSlider();
        zPanel = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        inputDimSlider3 = new org.visnow.vn.gui.widgets.ExtendedSlider();
        runButton = new org.visnow.vn.gui.widgets.RunButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));

        setBorder(javax.swing.BorderFactory.createEmptyBorder(4, 4, 4, 4));
        setLayout(new java.awt.GridBagLayout());
        add(uIStyleFakeInitializer1, new java.awt.GridBagConstraints());

        sectionHeader1.setOpaque(false);
        sectionHeader1.setShowCheckBox(false);
        sectionHeader1.setShowIcon(false);
        sectionHeader1.setText("Input");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(sectionHeader1, gridBagConstraints);

        inputPanel.setLayout(new java.awt.GridBagLayout());

        filePanel.setLayout(new java.awt.GridBagLayout());

        fileNameTF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                fileNameTFUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        fileNameTF.addFocusListener(new java.awt.event.FocusAdapter()
        {
            public void focusGained(java.awt.event.FocusEvent evt)
            {
                fileNameTFFocusGained(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 4);
        filePanel.add(fileNameTF, gridBagConstraints);

        browseButton.setText("Browse...");
        browseButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                browseButtonActionPerformed(evt);
            }
        });
        filePanel.add(browseButton, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 4, 0);
        inputPanel.add(filePanel, gridBagConstraints);

        typePanel.setLayout(new java.awt.GridBagLayout());

        typeLabel.setText("Type:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 7);
        typePanel.add(typeLabel, gridBagConstraints);

        typeCB.setListData(NumberType.values());
        typeCB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                typeCBUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        typePanel.add(typeCB, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 4, 0);
        inputPanel.add(typePanel, gridBagConstraints);

        endianPanel.setLayout(new java.awt.GridBagLayout());

        endianLabel.setText("Endian:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 7);
        endianPanel.add(endianLabel, gridBagConstraints);

        endianCB.setListData(Endianness.values());
        endianCB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                endianCBUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        endianPanel.add(endianCB, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 4, 0);
        inputPanel.add(endianPanel, gridBagConstraints);

        fileSizePanel.setLayout(new java.awt.GridBagLayout());

        fileSizeLabel.setText("File size:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 4);
        fileSizePanel.add(fileSizeLabel, gridBagConstraints);
        fileSizePanel.add(fileSizeValueLabel, new java.awt.GridBagConstraints());

        jLabel2.setText("bytes");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 0);
        fileSizePanel.add(jLabel2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 4, 0);
        inputPanel.add(fileSizePanel, gridBagConstraints);

        skipBytesPanel.setLayout(new java.awt.GridBagLayout());

        alignmentLabel.setText("In file alignment:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 4);
        skipBytesPanel.add(alignmentLabel, gridBagConstraints);

        alignmentTF.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        alignmentTF.setToolTipText("<html>\nAlignment within single file:<br/>\nn &ge; 0: skip n bytes head and read data aligned to start of file<br/>\nn &lt; 0: skip -n-1 bytes tail and read data aligned to end of file");
        alignmentTF.setAutoToolTip(false);
        alignmentTF.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.LONG);
        alignmentTF.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                alignmentTFUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        skipBytesPanel.add(alignmentTF, gridBagConstraints);

        jLabel1.setText("bytes");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 0);
        skipBytesPanel.add(jLabel1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 4, 0);
        inputPanel.add(skipBytesPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 4, 0);
        add(inputPanel, gridBagConstraints);

        sectionHeader2.setOpaque(false);
        sectionHeader2.setShowCheckBox(false);
        sectionHeader2.setShowIcon(false);
        sectionHeader2.setText("Dimensions");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(sectionHeader2, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        dimensionsPanel.setLayout(new java.awt.GridBagLayout());

        dimensionsButtonGroup.add(dim1DRB);
        dim1DRB.setText("1D");
        dim1DRB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                dimRBUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        dimensionsPanel.add(dim1DRB, new java.awt.GridBagConstraints());

        dimensionsButtonGroup.add(dim2DRB);
        dim2DRB.setSelected(true);
        dim2DRB.setText("2D");
        dim2DRB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                dimRBUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        dimensionsPanel.add(dim2DRB, new java.awt.GridBagConstraints());

        dimensionsButtonGroup.add(dim3DRB);
        dim3DRB.setText("3D");
        dim3DRB.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                dimRBUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        dimensionsPanel.add(dim3DRB, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 4, 0);
        jPanel1.add(dimensionsPanel, gridBagConstraints);

        xPanel.setLayout(new java.awt.GridBagLayout());

        jLabel3.setText("X:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        xPanel.add(jLabel3, gridBagConstraints);

        inputDimSlider1.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        inputDimSlider1.setGlobalMin(2);
        inputDimSlider1.setMax(768);
        inputDimSlider1.setSubmitOnAdjusting(false);
        inputDimSlider1.setValue(100);
        inputDimSlider1.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                inputDimSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 4, 0);
        xPanel.add(inputDimSlider1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(xPanel, gridBagConstraints);

        yPanel.setLayout(new java.awt.GridBagLayout());

        jLabel4.setText("Y:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        yPanel.add(jLabel4, gridBagConstraints);

        inputDimSlider2.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        inputDimSlider2.setGlobalMin(2);
        inputDimSlider2.setMax(768);
        inputDimSlider2.setSubmitOnAdjusting(false);
        inputDimSlider2.setValue(100);
        inputDimSlider2.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                inputDimSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 4, 0);
        yPanel.add(inputDimSlider2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(yPanel, gridBagConstraints);

        zPanel.setLayout(new java.awt.GridBagLayout());

        jLabel5.setText("Z:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        zPanel.add(jLabel5, gridBagConstraints);

        inputDimSlider3.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.INT);
        inputDimSlider3.setGlobalMin(2);
        inputDimSlider3.setMax(512);
        inputDimSlider3.setSubmitOnAdjusting(false);
        inputDimSlider3.setValue(100);
        inputDimSlider3.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                inputDimSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 4, 0);
        zPanel.add(inputDimSlider3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanel1.add(zPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 4, 0);
        add(jPanel1, gridBagConstraints);

        runButton.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                runButtonUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(12, 0, 4, 0);
        add(runButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.weighty = 1.0;
        add(filler1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void browseButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_browseButtonActionPerformed
    {//GEN-HEADEREND:event_browseButtonActionPerformed
        if (fileChooser == null) {
            fileChooser = new JFileChooser();
            fileChooser.setMultiSelectionEnabled(true);
        }
        fileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getUsableDataPath(ReadRaw.class)));

        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            File[] files = fileChooser.getSelectedFiles();
            String[] fileStrings = new String[files.length];
            for (int i = 0; i < files.length; i++) fileStrings[i] = files[i].getAbsolutePath();
            VisNow.get().getMainConfig().setLastDataPath(fileStrings[0].substring(0, fileStrings[0].lastIndexOf(File.separator)), ReadRaw.class);

            StringUtils.sortFilenameList(fileStrings);

            LOGGER.debug(Arrays.toString(fileStrings));

            fileNameTF.setText(StringUtils.join(fileStrings, File.pathSeparator));

            parameters.set(FILE_NAMES, fileStrings);
        }
    }//GEN-LAST:event_browseButtonActionPerformed

    private void fileNameTFFocusGained(java.awt.event.FocusEvent evt)//GEN-FIRST:event_fileNameTFFocusGained
    {//GEN-HEADEREND:event_fileNameTFFocusGained
        ((TextField) evt.getSource()).selectAll();
    }//GEN-LAST:event_fileNameTFFocusGained

    private void fileNameTFUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_fileNameTFUserChangeAction
    {//GEN-HEADEREND:event_fileNameTFUserChangeAction
        String filenamesJoined = fileNameTF.getText();
        String[] filenames = filenamesJoined.split("\\" + File.pathSeparatorChar);
        if (filenames.length == 1 && filenames[0].equals("")) {
            parameters.set(FILE_NAMES, new String[0]);
        } else {
            parameters.set(FILE_NAMES, filenames);
        }
    }//GEN-LAST:event_fileNameTFUserChangeAction

    private void typeCBUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_typeCBUserChangeAction
    {//GEN-HEADEREND:event_typeCBUserChangeAction
        parameters.set(NUMBER_TYPE, (NumberType) typeCB.getSelectedItem());
    }//GEN-LAST:event_typeCBUserChangeAction

    private void endianCBUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_endianCBUserChangeAction
    {//GEN-HEADEREND:event_endianCBUserChangeAction
        parameters.set(ENDIANNESS, (Endianness) endianCB.getSelectedItem());
    }//GEN-LAST:event_endianCBUserChangeAction

    private void alignmentTFUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_alignmentTFUserChangeAction
    {//GEN-HEADEREND:event_alignmentTFUserChangeAction
        parameters.set(IN_FILE_ALIGNMENT, (long) alignmentTF.getValue());
    }//GEN-LAST:event_alignmentTFUserChangeAction

    private void dimRBUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_dimRBUserChangeAction
    {//GEN-HEADEREND:event_dimRBUserChangeAction
        parameters.set(NUMBER_OF_DIMENSIONS, dim1DRB.isSelected() ? 1 : dim2DRB.isSelected() ? 2 : 3);
        updateSliderVisibility();
    }//GEN-LAST:event_dimRBUserChangeAction

    private void runButtonUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_runButtonUserChangeAction
    {//GEN-HEADEREND:event_runButtonUserChangeAction
        parameters.set(RUNNING_MESSAGE, (RunButton.RunState) evt.getEventData());
    }//GEN-LAST:event_runButtonUserChangeAction

    private void inputDimSliderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_inputDimSliderUserChangeAction
    {//GEN-HEADEREND:event_inputDimSliderUserChangeAction
        parameters.set(DATA_DIMS, new long[]{(int) inputDimSlider1.getValue(), (int) inputDimSlider2.getValue(), (int) inputDimSlider3.getValue()});
    }//GEN-LAST:event_inputDimSliderUserChangeAction


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel alignmentLabel;
    private org.visnow.vn.gui.components.NumericTextField alignmentTF;
    private javax.swing.JButton browseButton;
    private org.visnow.vn.gui.swingwrappers.RadioButton dim1DRB;
    private org.visnow.vn.gui.swingwrappers.RadioButton dim2DRB;
    private org.visnow.vn.gui.swingwrappers.RadioButton dim3DRB;
    private javax.swing.ButtonGroup dimensionsButtonGroup;
    private javax.swing.JPanel dimensionsPanel;
    private org.visnow.vn.gui.swingwrappers.ComboBox endianCB;
    private javax.swing.JLabel endianLabel;
    private javax.swing.JPanel endianPanel;
    private org.visnow.vn.gui.swingwrappers.TextField fileNameTF;
    private javax.swing.JPanel filePanel;
    private javax.swing.JLabel fileSizeLabel;
    private javax.swing.JPanel fileSizePanel;
    private javax.swing.JLabel fileSizeValueLabel;
    private javax.swing.Box.Filler filler1;
    private org.visnow.vn.gui.widgets.ExtendedSlider inputDimSlider1;
    private org.visnow.vn.gui.widgets.ExtendedSlider inputDimSlider2;
    private org.visnow.vn.gui.widgets.ExtendedSlider inputDimSlider3;
    private javax.swing.JPanel inputPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.ButtonGroup multipleFilesButtonGroup;
    private org.visnow.vn.gui.widgets.RunButton runButton;
    private org.visnow.vn.gui.widgets.SectionHeader sectionHeader1;
    private org.visnow.vn.gui.widgets.SectionHeader sectionHeader2;
    private javax.swing.JPanel skipBytesPanel;
    private org.visnow.vn.gui.swingwrappers.ComboBox typeCB;
    private javax.swing.JLabel typeLabel;
    private javax.swing.JPanel typePanel;
    private org.visnow.vn.system.swing.UIStyleFakeInitializer uIStyleFakeInitializer1;
    private javax.swing.JPanel xPanel;
    private javax.swing.JPanel yPanel;
    private javax.swing.JPanel zPanel;
    // End of variables declaration//GEN-END:variables

    void activateOpenDialog()
    {
        browseButtonActionPerformed(null);
    }
    
    public static void main(String[] args)
    {
        VisNow.initLogging(true);
        UIStyle.initStyle();

        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                JFrame f = new JFrame();
                f.add(new ReadRawComputeUI());
                f.pack();
                f.setVisible(true);
                f.setLocationRelativeTo(null);
                f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            }
        });

    }
}
