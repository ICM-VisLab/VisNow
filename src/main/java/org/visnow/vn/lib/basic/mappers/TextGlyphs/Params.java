/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.TextGlyphs;

import javax.swing.event.ChangeEvent;
import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.parameters.FontParams;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrange;
import org.visnow.vn.lib.gui.FieldBasedUI.DownsizeUI.DownsizeParams;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class Params extends Parameters
{

    public static final int THRESHOLD_CHANGED = 3;
    public static final int COUNT_CHANGED = 3;
    public static final int GLYPHS_CHANGED = 2;
    public static final int COORDS_CHANGED = 1;
    protected int change = 0;
    protected ComponentSubrange validComponentRange = new ComponentSubrange(true, false, false, true, true, true);
    protected FontParams fontParams = new FontParams();
    protected DownsizeParams downsizeParams = new DownsizeParams(1, 2000, 50, false);
    protected static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<String>("component", ParameterType.dependent, ""),
        new ParameterEgg<Float>("scale", ParameterType.dependent, .02f),
        new ParameterEgg<Boolean>("insideRange", ParameterType.dependent, true),
        new ParameterEgg<String>("format", ParameterType.dependent, "%4.1f"),
        new ParameterEgg<int[]>("lowCrop", ParameterType.dependent, null),
        new ParameterEgg<int[]>("upCrop", ParameterType.dependent, null),};

    public Params()
    {
        super(eggs);
        setValue("lowCrop", new int[]{0, 0, 0});
        setValue("upCrop",  new int[]{1, 1, 1});
        validComponentRange.addChangeListener((ChangeEvent e) -> {
            if (!validComponentRange.isAdjusting()) {
                change = THRESHOLD_CHANGED;
                fireStateChanged();
            }
        });
        fontParams.addChangeListener((ChangeEvent e) -> {
            change = GLYPHS_CHANGED;
            fireStateChanged();
        });
        downsizeParams.addChangeListener((ChangeEvent e) -> {
            change = COUNT_CHANGED;
            fireStateChanged();
        });
    }

    public String getComponent()
    {
        return (String) getValue("component");
    }

    public void setComponent(String component)
    {
        setValue("component", component);
        change = max(change, GLYPHS_CHANGED);
        fireStateChanged();
    }

    public ComponentSubrange getValidComponentRange()
    {
        return validComponentRange;
    }

    public String getFormat()
    {
        return (String) getValue("format");
    }

    public void setFormat(String format)
    {
        setValue("format", format);
        change = max(change, GLYPHS_CHANGED);
        fireStateChanged();
    }

    public int getChange()
    {
        return change;
    }

    public void setChange(int change)
    {
        this.change = change;
    }

    public boolean isInsideRange()
    {
        return (Boolean)getValue("insideRange");
    }

    public void setInsideRange(boolean inside)
    {
        setValue("insideRange", inside);
        change = max(change, COUNT_CHANGED);
        fireStateChanged();
    }

    @Override
    public void fireStateChanged()
    {
        if (!active)
            return;
        ChangeEvent e = new ChangeEvent(this);
        for (int i = 0; i < changeListenerList.size(); i++) {
            changeListenerList.get(i).stateChanged(e);
        }
    }

    public DownsizeParams getDownsizeParams()
    {
        return downsizeParams;
    }

    public FontParams getFontParams()
    {
        return fontParams;
    }

    public long getPreferredSize()
    {
        return downsizeParams.getPreferredSize();
    }

    public int[] getLowCrop()
    {
        return (int[]) getValue("lowCrop");
    }

    public int[] getUpCrop()
    {
        return (int[]) getValue("upCrop");
    }

    public int[] getDown()
    {
        return downsizeParams.getDown();
    }

    public int getDownsize()
    {
        return downsizeParams.getDownFactor();
    }

    public void setCrop(int[] lowCrop, int[] upCrop)
    {
        setValue("lowCrop", lowCrop);
        setValue("upCrop", upCrop);
        change = COUNT_CHANGED;
        fireStateChanged();
    }

}
