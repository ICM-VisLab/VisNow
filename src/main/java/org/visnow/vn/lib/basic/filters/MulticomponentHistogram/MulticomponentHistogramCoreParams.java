/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.MulticomponentHistogram;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class MulticomponentHistogramCoreParams
{
    static final int BINNING_BY_COMPONENTS = 0;
    static final int BINNING_BY_COORDINATES = 1;

    private final int nDims;
    private final int binning;
    private final int[] dims;
    private final int[] selectedComponents;
    private final int[] selectedCoords;
    private final boolean countLogScale;
    private final boolean countDropBackground;
    private final float logConstant;
    private final boolean outGeometryToData;
    private final boolean roundByteDimsTo32;
    private final HistogramOperation[] histogramOperations;

    public MulticomponentHistogramCoreParams(int nDims, int binning, int[] dims, int[] selectedComponents, int[] selectedCoords, boolean countLogScale, boolean countDropBackground, float logConstant, boolean outGeometryToData, boolean roundByteDimsTo32, HistogramOperation[] histogramOperations)
    {
        this.nDims = nDims;
        this.binning = binning;
        this.dims = dims;
        this.selectedComponents = selectedComponents;
        this.selectedCoords = selectedCoords;
        this.countLogScale = countLogScale;
        this.countDropBackground = countDropBackground;
        this.logConstant = logConstant;
        this.outGeometryToData = outGeometryToData;
        this.roundByteDimsTo32 = roundByteDimsTo32;
        this.histogramOperations = histogramOperations;
    }

    public int getNDims()
    {
        return nDims;
    }

    public int getBinning()
    {
        return binning;
    }

    public int[] getDims()
    {
        return dims;
    }

    public int[] getSelectedComponents()
    {
        return selectedComponents;
    }

    public int[] getSelectedCoords()
    {
        return selectedCoords;
    }

    public boolean isCountLogScale()
    {
        return countLogScale;
    }

    public boolean isCountDropBackground()
    {
        return countDropBackground;
    }

    public float getLogConstant()
    {
        return logConstant;
    }

    public boolean isOutGeometryToData()
    {
        return outGeometryToData;
    }

    public boolean isRoundByteDimsTo32()
    {
        return roundByteDimsTo32;
    }
    
    public Condition[] getFilterConditions()
    {
        return null;
    }

    public Condition.Logic[] getFilterConditionsLogic()
    {
        return null;
    }

    public HistogramOperation[] getHistogramOperations()
    {
        return histogramOperations;
    }
}
