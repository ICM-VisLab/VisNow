/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadVisNowField;

import org.apache.log4j.Logger;
import java.io.File;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Vector;
import org.visnow.jscic.PointField;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.*;
import org.visnow.vn.lib.utils.io.VNIOException;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class PointFieldHeaderParser extends HeaderParser
{
    private static final Logger LOGGER = Logger.getLogger(PointFieldHeaderParser.class);

    //   private String line = "";
    protected PointField pointField;

    public PointFieldHeaderParser(LineNumberReader r, File headerFile, String fileName)
    {
        super(r, headerFile, fileName);
    }

    public PointFieldIOSchema parseHeader()
            throws VNIOException
    {
        PointFieldIOSchema schema;
        Vector<String[]> res = new Vector<>();
        String name = "";
        long nnodes = 0;
        String[] userData = null;
        boolean hasMask = false;
        try {
            line = nextLine();
            ParseResult result = processLine(line, new String[]{"field", "name"},
                                     new String[]{"c", "file"}, res);
            switch (result) {
                case ACCEPTED:
                    for (int i = 0; i < res.size(); i++) {
                        String[] strings = res.get(i);
                        if ((strings[0].startsWith("name") || strings[0].startsWith("field")) && strings.length > 1)
                            name = strings[1];
                        else if (strings[0].startsWith("nnodes") || strings[0].startsWith("nodes") || strings[0].startsWith("el")) {
                            if (strings.length < 2) {
                                throw new VNIOException("no nodes count specified", fileName, r.getLineNumber());
                            }
                            try {
                                nnodes = Long.parseLong(strings[1]);
                            } catch (NumberFormatException e) {
                                throw new VNIOException("node count " + strings[1] + " is not integer ", fileName, r.getLineNumber());
                            }
                        } else if (strings[0].startsWith("valid") || strings[0].startsWith("mask"))
                            hasMask = true;
                        else  if (strings[0].startsWith("user:")) {
                            userData = strings[0].substring(5).split(";");
                            for (int j = 0; j < userData.length; j++) {
                                if (userData[j].startsWith("__"))
                                    try {
                                        int k = Integer.parseInt(userData[j].substring(2));
                                        userData[j] = stringsInLine[k];
                                    } catch (NumberFormatException e) {
                                    }
                            }
                        }
                    }
                    break;
                case EOF:
                    throw new VNIOException("no field description line ", fileName, r.getLineNumber());
                case ERROR:
                    throw new VNIOException("bad field file ", fileName, r.getLineNumber());
                case BREAK:
                    throw new VNIOException("no field description line ", fileName, r.getLineNumber());
                default:
                    break;
            }

            if (nnodes < 1)
                throw new VNIOException("no nodes count specified ", fileName, r.getLineNumber());
            pointField = new PointField(nnodes);
            pointField.setName(name);
            if (hasMask)
                pointField.setCurrentMask(new LogicLargeArray(pointField.getNNodes(), true));
            pointField.setUserData(userData);
            schema = new PointFieldIOSchema(pointField, headerFile, fileName);
            line = nextLine();
            parseAxesDescription(pointField);
            parseExtentEntry(pointField);
            parseDataItems(pointField);
            field = pointField;
            DataFileSchema dataFileSchema;
            schema.generateDataInputStatus();
            parsedSchema = schema;

            file_loop:
            while ((dataFileSchema = parseFileEntry()) != null)
                schema.addFileSchema(dataFileSchema);
            r.close();

        } catch (IOException e) {
            throw new VNIOException("bad header file ", fileName, r.getLineNumber());
        }
        return schema;
    }

}
