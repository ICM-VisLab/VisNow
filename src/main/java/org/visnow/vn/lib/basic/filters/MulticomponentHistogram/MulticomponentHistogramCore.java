/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.MulticomponentHistogram;

import java.util.ArrayList;
import java.util.HashMap;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.utils.MatrixMath;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class MulticomponentHistogramCore
{
    private Field inField;
    private RegularField outField;

    //FIXME: no field schema testing ... modules may be incorrectly connected now (in Unicore wizard).
    public final static String IN_FIELD_MAIN = "inField";
    public final static String OUT_RFIELD_MAIN = "outField";

    public MulticomponentHistogramCore()
    {
    }

    public void update(MulticomponentHistogramCoreParams params)
    {
        if (inField == null) {
            return;
        }

        outField = null;

        HistogramOperation[] tmpOps = params.getHistogramOperations();
        HistogramOperation[] ops = null;
        int nOps = 0;
        if (tmpOps != null)
            nOps = tmpOps.length;

        nOps++;
        ops = new HistogramOperation[nOps];
        ops[0] = new HistogramOperation(params.isCountLogScale(), params.getLogConstant(), params.isCountDropBackground());
        for (int i = 0; i < nOps - 1; i++) {
            ops[i + 1] = tmpOps[i];
        }

        int[] tmp = params.getDims();
        int nDims = params.getNDims();
        int[] histDims = new int[nDims];
        for (int i = 0; i < nDims; i++) {
            histDims[i] = tmp[i];
        }

        float[][] affine = new float[4][3];
        float[][] ext = new float[2][3];
        float[][] physExt = new float[2][3];
        ArrayList<DataArray> outData = new ArrayList<DataArray>();
        boolean roundByteDimsTo32 = params.isRoundByteDimsTo32();

        switch (params.getBinning()) {
            case MulticomponentHistogramCoreParams.BINNING_BY_COMPONENTS:
                DataArray[] data = new DataArray[nDims];
                int[] sel = params.getSelectedComponents();
                for (int i = 0; i < nDims; i++) {
                    data[i] = inField.getComponent(sel[i]);
                }

                for (int i = 0; i < ops.length; i++) {
                    DataArray hist = null;
                    if (ops[i].getComponent() == null || ops[i].getComponent().getVectorLength() == 1) {
                        hist = HistogramBuilder.buildDataHistogram(histDims, roundByteDimsTo32, data, ops[i], params.getFilterConditions(), params.getFilterConditionsLogic());
                    } else {
                        hist = HistogramBuilder.buildVectorDataHistogram(histDims, roundByteDimsTo32, data, ops[i], params.getFilterConditions(), params.getFilterConditionsLogic());
                    }
                    if (hist != null) {
                        outData.add(hist);
                    }
                    fireStatusChanged((float) (i + 1) / (float) ops.length);
                }

                outField = new RegularField(histDims);
                for (int i = 0; i < outData.size(); i++) {
                    outField.addComponent(outData.get(i));
                }
                
                if (!params.isOutGeometryToData()) {
                    for (int i = 0; i < histDims.length; i++) {
                        affine[i][i] = 1.0f;
                    }
                    outField.setAffine(affine);
                    ext = outField.getExtents();
                    for (int i = 0; i < histDims.length; i++) {
                        if (roundByteDimsTo32 && data[i].getType() == DataArrayType.FIELD_DATA_BYTE) {
                            physExt[0][i] = 0.0f;
                            physExt[1][i] = 255.0f;
                        } else {
                            physExt[0][i] = (float) data[i].getPreferredPhysMinValue();
                            physExt[1][i] = (float) data[i].getPreferredPhysMaxValue();
                        }
                    }
                    outField.setPreferredExtents(ext, physExt);
                } else {                        
                    for (int i = 0; i < histDims.length; i++) {
                        ext[0][i] = (float)data[i].getPreferredMinValue();
                        ext[1][i] = (float)data[i].getPreferredMaxValue();
                        physExt[0][i] = (float) data[i].getPreferredPhysMinValue();
                        physExt[1][i] = (float) data[i].getPreferredPhysMaxValue();
                    }
                    affine = MatrixMath.computeAffineFromExtents(histDims, ext); 
                    outField.setAffine(affine);
                    outField.setPreferredExtents(ext, physExt);       
                }
                
                String[] axesNames = new String[]{"","",""};
                String[] coordsUnits = new String[]{"1","1","1"};
                for (int i = 0; i < nDims; i++) {
                    axesNames[i] = new String(data[i].getName());
                    String u = data[i].getUnit();
                    if(u != null && !u.isEmpty())
                        coordsUnits[i] = new String(u);
                }
                outField.setAxesNames(axesNames);
                outField.setCoordsUnits(coordsUnits);

                break;
            case MulticomponentHistogramCoreParams.BINNING_BY_COORDINATES:

                int[] histCoords = params.getSelectedCoords();

                for (int i = 0; i < ops.length; i++) {
                    float[] hist = null;

                    if (ops[i].getComponent() == null || ops[i].getComponent().getVectorLength() == 1) {
                        hist = HistogramBuilder.buildCoordsHistogram(histDims, histCoords, inField, ops[i], params.getFilterConditions(), params.getFilterConditionsLogic());
                        if (hist != null) {
                            outData.add(DataArray.create(hist, 1, ops[i].toString()));
                        }
                    } else {
                        hist = HistogramBuilder.buildVectorCoordsHistogram(histDims, histCoords, inField, ops[i], params.getFilterConditions(), params.getFilterConditionsLogic());
                        if (hist != null) {
                            if (ops[i].getOperation() == HistogramOperation.Operation.VSTD) {
                                int veclen = ops[i].getComponent().getVectorLength();
                                int nData = hist.length / (veclen * veclen);
                                for (int v = 0; v < veclen; v++) {
                                    float[] tmpHist = new float[nData * veclen];
                                    for (int m = 0; m < nData; m++) {
                                        for (int l = 0; l < veclen; l++) {
                                            //tmpHist[m*veclen + l] = hist[m*veclen*veclen + v*veclen + l];
                                            tmpHist[m * veclen + l] = hist[m * veclen * veclen + l * veclen + v];
                                        }
                                    }
                                    outData.add(DataArray.create(tmpHist, veclen, ops[i].toString() + "_" + v));
                                }
                            } else {
                                outData.add(DataArray.create(hist, ops[i].getComponent().getVectorLength(), ops[i].toString()));
                            }
                        }
                    }

                    fireStatusChanged((float) (i + 1) / (float) ops.length);
                }

                outField = new RegularField(histDims);
                for (int i = 0; i < outData.size(); i++) {
                    outField.addComponent(outData.get(i));
                }
           
                if (!params.isOutGeometryToData()) {
                    float[][] inPhysExt = inField.getPreferredPhysicalExtents();
                    for (int i = 0; i < histDims.length; i++) {
                        affine[i][i] = 1.0f;
                    }
                    outField.setAffine(affine);
                    ext = outField.getExtents();
                    for (int i = 0; i < histDims.length; i++) {
                        physExt[0][i] = inPhysExt[0][histCoords[i]];
                        physExt[1][i] = inPhysExt[1][histCoords[i]];
                    }
                    outField.setPreferredExtents(ext, physExt);
                } else {  
                    float[][] inExt = inField.getPreferredExtents();
                    float[][] inPhysExt = inField.getPreferredPhysicalExtents();
                    for (int i = 0; i < histDims.length; i++) {
                        ext[0][i] = inExt[0][histCoords[i]];
                        ext[1][i] = inExt[1][histCoords[i]];
                        physExt[0][i] = inPhysExt[0][histCoords[i]];
                        physExt[1][i] = inPhysExt[1][histCoords[i]];
                    }
                    affine = MatrixMath.computeAffineFromExtents(histDims, ext); 
                    outField.setAffine(affine);
                    outField.setPreferredExtents(ext, physExt);
                }
                
                break;
        }

        if (outField.getNComponents() == 0)
            outField = null;

        fireStatusChanged(1.0f);
    }

    public Field getOutField(String name)
    {
        if (name.equals(OUT_RFIELD_MAIN))
            return outField;
        else
            throw new IllegalArgumentException("Incorrect field name: " + name);
    }

    public void setInField(String name, Field inField)
    {
        if (name.equals(IN_FIELD_MAIN))
            this.inField = inField;
        else
            throw new IllegalArgumentException("Incorrect field name: " + name);
    }

    //    private abstract class HistogramOperationThread extends Thread{
    //        protected float[] hist = null;
    //        
    //        public float[] getHistogram() {
    //            return hist;
    //        }
    //    }
    //    
    //    private class DataScalarHistogramOperationThread extends HistogramOperationThread {
    //        private int[] histDims;
    //        private DataArray[] data; 
    //        private HistogramOperation op;
    //        private Condition[] filterConditions; 
    //        private Condition.Logic[] filterConditionsLogic;       
    //        
    //        public DataScalarHistogramOperationThread(int[] histDims, DataArray[] data, HistogramOperation op, Condition[] filterConditions, Condition.Logic[] filterConditionsLogic) {
    //            this.histDims = histDims;
    //            this.data = data;
    //            this.op = op;    
    //            this.filterConditions = filterConditions;
    //            this.filterConditionsLogic = filterConditionsLogic;
    //        }
    //        
    //        @Override
    //        public void run() {
    //            hist = HistogramBuilder.buildDataHistogram(histDims, data, op, filterConditions, filterConditionsLogic);                                
    //        }
    //    }
    //
    //    private class DataVectorHistogramOperationThread extends HistogramOperationThread {
    //        private int[] histDims;
    //        private DataArray[] data; 
    //        private HistogramOperation op;
    //        private Condition[] filterConditions; 
    //        private Condition.Logic[] filterConditionsLogic;       
    //        
    //        public DataVectorHistogramOperationThread(int[] histDims, DataArray[] data, HistogramOperation op, Condition[] filterConditions, Condition.Logic[] filterConditionsLogic) {
    //            this.histDims = histDims;
    //            this.data = data;
    //            this.op = op;    
    //            this.filterConditions = filterConditions;
    //            this.filterConditionsLogic = filterConditionsLogic;
    //        }
    //        
    //        @Override
    //        public void run() {
    //            hist = HistogramBuilder.buildVectorDataHistogram(histDims, data, op, filterConditions, filterConditionsLogic);                        
    //        }
    //    }
    //
    //    private class CoordsScalarHistogramOperationThread extends HistogramOperationThread {
    //        private int[] histDims;        
    //        private int[] histCoords;        
    //        private Field field;
    //        private HistogramOperation op;
    //        private Condition[] filterConditions; 
    //        private Condition.Logic[] filterConditionsLogic;       
    //        
    //        public CoordsScalarHistogramOperationThread(int[] histDims, int[] histCoords, Field field, HistogramOperation op, Condition[] filterConditions, Condition.Logic[] filterConditionsLogic) {
    //            this.histDims = histDims;
    //            this.histCoords = histCoords;
    //            this.field = field;
    //            this.op = op;    
    //            this.filterConditions = filterConditions;
    //            this.filterConditionsLogic = filterConditionsLogic;
    //        }
    //        
    //        @Override
    //        public void run() {
    //            hist = HistogramBuilder.buildCoordsHistogram(histDims, histCoords, field, op, filterConditions, filterConditionsLogic);                    
    //        }
    //    }
    //    
    //    private class CoordsVectorHistogramOperationThread extends HistogramOperationThread {
    //        private int[] histDims;        
    //        private int[] histCoords;        
    //        private Field field;
    //        private HistogramOperation op;
    //        private Condition[] filterConditions; 
    //        private Condition.Logic[] filterConditionsLogic;       
    //        
    //        public CoordsVectorHistogramOperationThread(int[] histDims, int[] histCoords, Field field, HistogramOperation op, Condition[] filterConditions, Condition.Logic[] filterConditionsLogic) {
    //            this.histDims = histDims;
    //            this.histCoords = histCoords;
    //            this.field = field;
    //            this.op = op;    
    //            this.filterConditions = filterConditions;
    //            this.filterConditionsLogic = filterConditionsLogic;
    //        }
    //        
    //        @Override
    //        public void run() {
    //            hist = HistogramBuilder.buildVectorCoordsHistogram(histDims, histCoords, field, op, filterConditions, filterConditionsLogic);                    
    //        }
    //    }
    //    
    private transient FloatValueModificationListener statusListener = null;

    public void addFloatValueModificationListener(FloatValueModificationListener listener)
    {
        if (statusListener == null)
            this.statusListener = listener;
        else
            System.out.println("" + this + ": only one status listener can be added");
    }

    private void fireStatusChanged(float status)
    {
        FloatValueModificationEvent e = new FloatValueModificationEvent(this, status, true);
        if (statusListener != null)
            statusListener.floatValueChanged(e);
    }

}
