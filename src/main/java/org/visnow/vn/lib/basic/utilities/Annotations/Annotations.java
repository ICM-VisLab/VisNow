/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.utilities.Annotations;

import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.PointField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.geometries.parameters.FontParams;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.Pick3DEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.Pick3DListener;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.PickType;
import org.visnow.vn.lib.templates.visualization.modules.VisualizationModule;
import org.visnow.vn.lib.types.VNGeometryObject;
import org.visnow.vn.lib.types.VNPointField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Annotations extends VisualizationModule
{
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private Params params = new Params();
    private FontParams fontParams = new FontParams();
    private GUI ui = null;
    private Pick3DEvent lastEvt = null;
    private final ArrayList<float[]> coords = new ArrayList<>();
    private final ArrayList<String[]> texts = new ArrayList<>();
    private AnnotationsObject glyphObj =
            new AnnotationsObject(fontParams, coords, texts);

    public Annotations()
    {
        params.addParameterChangelistener((String name) -> {
            glyphObj.update();
            if (params.isOutput()) {
                startAction();
                params.clearOutput();
            }
        });
        fontParams.setDecoration(FontParams.Decoration.NONE);
        fontParams.setPosition(FontParams.Position.N);
        fontParams.addChangeListener((ChangeEvent e) -> {
            glyphObj.update();
        });
        SwingInstancer.swingRunAndWait(() -> {
            ui = new GUI();
            setPanel(ui);
            ui.setParams(params);
            ui.setData(coords, texts);
            ui.setFontParams(fontParams);
        });
    }

    @Override
    public void onInitFinishedLocal()
    {
        outObj = glyphObj;
        outObj.setCreator(this);
        outObj.getGeometryObj().setUserData(new ModuleIdData(this));
        outObj2DStruct.setParentModulePort(this.getName() + ".out.outObj");
        setOutputValue("outObj", new VNGeometryObject(outObj, outObj2DStruct));
    }

    @Override
    public void onActive()
    {
        glyphObj.update();
        if (coords == null || coords.isEmpty())
            return;
        float[] coordArray = new float[3 * coords.size()];
        for (int i = 0; i <coords.size() ; i++)
            System.arraycopy(coords.get(i), 0, coordArray, 3 * i, 3);
        String[][] textArray = texts.toArray(new String[0][]);
        PointField outField = new PointField(coords.size());
        outField.setName("annotations");
        outField.setCurrentCoords(new FloatLargeArray(coordArray));
        String[] outTexts = new String[coords.size()];
        for (int i = 0; i < texts.size(); i++) {
            StringBuilder str = new StringBuilder();
            for (int j = 0; j < textArray[i].length; j++) {
                if (j > 0)
                    str.append(";");
                str.append(textArray[i][j].replaceAll("\n", "_;_"));
            }
            outTexts[i] = str.toString();
        }
        outField.addComponent(DataArray.create(outTexts, 1, "texts"));
        setOutputValue("annotationsField", new VNPointField(outField));
    }

    @Override
    public Pick3DListener getPick3DListener() {
        return new Pick3DListener(PickType.POINT)
        {
            @Override
            public void handlePick3D(Pick3DEvent e)
            {
                if (params.isActivated() && e.getEvt().isShiftDown() && e != lastEvt) {
                    lastEvt = e;
                    float[] x = e.getPoint(); // x will always be not-null
                    coords.add(x);
                    texts.add(new String[] {String.format("[%.3f%n %.3f%n %.3f]", x[0], x[1], x[2])});
                    ui.updateGUI();
                    glyphObj.update();
                }
            }
        };
    }

}
