/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Axes3D;

import javax.swing.event.ChangeEvent;

import org.visnow.vn.lib.templates.visualization.modules.VisualizationModule;
import org.visnow.jscic.Field;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.geometries.events.ColorListener;
import org.visnow.vn.geometries.events.ResizeListener;
import org.visnow.vn.geometries.parameters.FontParams;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNGeometryObject;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Axes3D extends VisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private Axes3DGUI ui = null;
    private Field inField = null;
    private Axes3DParams params;
    private Axes3DObject axes3DObject = new Axes3DObject();
    private boolean fromInput = false;

    /**
     * Creates a new instance of CreateGrid
     */
    public Axes3D()
    {
        params = new Axes3DParams();
        parameters = params;
        params.getFontParams().setPosition(FontParams.Position.N);
        params.getFontParams().setShift(0);
        params.addChangeListener((ChangeEvent evt) -> {
            if (!fromInput && inField != null) {
                params.setActiveValue(false);
                axes3DObject.update(inField, params);
                params.setActiveValue(true);
            }
        });
        SwingInstancer.swingRunAndWait(() -> {
            ui = new Axes3DGUI();
            ui.setParams(params);
            setPanel(ui);
        });
    }
    
    @Override
    public ColorListener getBackgroundColorListener() 
    {
        return e -> params.getFontParams().setBgColor(e.getSelectedColor());
    }
    
    @Override
    public void onInitFinishedLocal()
    {
        outObj = axes3DObject;
        outObj.setCreator(this);
        setOutputValue("outObj", new VNGeometryObject(outObj));
    }

    public static OutputEgg[] getOutputEggs()
    {
        if (outputEggs == null) {
            outputEggs = new OutputEgg[]{
                geometryOutput
            };
        }
        return outputEggs;
    }

    @Override
    public void onActive()
    {
        fromInput = true;
        if (getInputFirstValue("inField") == null) {
            axes3DObject.update(null, params);
            return;
        }
        inField = ((VNField) getInputFirstValue("inField")).getField();
        if (inField == null) {
            axes3DObject.update(null, params);
            return;
        }
        ui.setInfield(inField);
        fromInput = false;
        params.setActiveValue(false);
        axes3DObject.update(inField, params);
        params.setActiveValue(true);
    }
}
