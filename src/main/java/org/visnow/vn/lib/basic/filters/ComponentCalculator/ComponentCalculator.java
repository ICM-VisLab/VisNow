/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.ComponentCalculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.LongLargeArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.PointField;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.gui.widgets.RunButton.RunState.*;
import static org.visnow.vn.lib.basic.filters.ComponentCalculator.ComponentCalculatorShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNPointField;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.lib.utils.expressions.ArrayExpressionParser;
import org.visnow.vn.lib.utils.expressions.Operator;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;

/**
 *
 ** @author Bartosz Borucki (babor@icm.edu.pl), University of Warsaw, ICM
 * modified by:
 * @author Krzysztof Nowinski (know@icm.edu.pl, knowpl@gmail.com), University of Warsaw, ICM
 *
 */
public class ComponentCalculator extends OutFieldVisualizationModule
{

    private static final Logger LOGGER = Logger.getLogger(ComponentCalculator.class);
    private static final String[] COORD_SYMBOLS = {"_x", "_y", "_z"};
    private static final String[] INDEX_SYMBOLS = {"_i", "_j", "_k"};
    private GUI computeUI = null;
    private Field inField = null;

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    private int runQueue = 0;
    private int[][] linesRemap;
    private String preamble = "";

    public ComponentCalculator()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name != null && name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startIfNotInQueue();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParameterProxy(parameters);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(EXPRESSION_LINES, new String[]{}),
            new Parameter<>(PRECISION, Precision.SINGLE),
            new Parameter<>(META_IS_GENERATOR, true),
            //
            new Parameter<>(INPUT_ALIASES, new String[]{}),
            new Parameter<>(IGNORE_UNITS, true),
            new Parameter<>(RETAIN, false),
            new Parameter<>(META_FIELD_DIMENSION, 3),
            new Parameter<>(META_COMPONENT_NAMES, new String[]{}),
            new Parameter<>(META_COMPONENT_VECLEN, new int[]{}),
            //
            new Parameter<>(GENERATOR_DIMENSION_LENGTHS, new int[]{50, 50, 50}),
            new Parameter<>(GENERATOR_EXTENTS, new float[][]{{-0.5f, -0.5f, -0.5f}, {0.5f, 0.5f, 0.5f}}),
            //
            new Parameter<>(RUNNING_MESSAGE, NO_RUN)
        };
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending, preamble);
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);

        if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE) parameters.set(RUNNING_MESSAGE, NO_RUN);

        parameters.set(META_IS_GENERATOR, inField == null);

        if (parameters.get(META_IS_GENERATOR)) {
            //validate extents
            float[][] extents = parameters.get(GENERATOR_EXTENTS);
            for (int i = 0; i < extents[0].length; i++)
                if (extents[0][i] + 10000 * Math.ulp(extents[0][i]) > extents[1][i]) extents[1][i] = extents[0][i] + 10000 * Math.ulp(extents[0][i]);
            parameters.set(GENERATOR_EXTENTS, extents);
        } else {
            parameters.set(META_FIELD_DIMENSION, inField instanceof RegularField ? ((RegularField)inField).getDimNum() : -1);
            parameters.set(META_COMPONENT_NAMES, inField.getComponentNames());
            parameters.set(META_COMPONENT_VECLEN, inField.getComponentVectorLengths());
        }

        if (resetParameters) {
            String[] aliasesToValidate;

            if (resetParameters) //create new aliases
                aliasesToValidate = inField.getComponentNames();
            else //validate aliases
                aliasesToValidate = parameters.get(INPUT_ALIASES);

            Set<String> reservedAliases = new HashSet<>();
            for (Operator op : Operator.values())
                reservedAliases.add(op.toString());

            for (int i = 0; i < aliasesToValidate.length; i++) {
                if (resetParameters || !isAliasValid(aliasesToValidate[i]) || reservedAliases.contains(aliasesToValidate[i])) {
                    String alias = findSmartAlias(reservedAliases, aliasesToValidate[i]);
                    aliasesToValidate[i] = alias;
                }
                reservedAliases.add(aliasesToValidate[i]);
            }

            parameters.set(INPUT_ALIASES, aliasesToValidate);
                int maxAliasLen = 0;
                for (int i = 0; i < inField.getNComponents(); i++)
                    if (aliasesToValidate[i].length() > maxAliasLen)
                        maxAliasLen = aliasesToValidate[i].length();
                int[] dims = parameters.get(GENERATOR_DIMENSION_LENGTHS);
        }

        parameters.setParameterActive(true);
    }

    private boolean isAliasValid(String name)
    {
        return name.matches("[a-zA-Z][a-zA-Z0-9]*");
    }

    private static String findSmartAlias(Set<String> reservedAliases, String name)
    {
        if (name.startsWith("image_data_"))                //this is a convebtion used by image reader causing spurious aliases
            name = name.substring("image_data_".length()); // stripping leading repeated part
        String[] twords = name.split("[ _]+");
        int n = 0;
        for (int i = 0; i < twords.length; i++)
            if (twords[i] != null && !twords[i].isEmpty()) {
                twords[n] = twords[i].replaceAll("_","").replaceAll("^(([0-9]))", "data$1");
                n+= 1;
            }
        String[] words = new String[n];
        System.arraycopy(twords, 0, words, 0, n);
        String vowelsPattern = "[AEIOUYaeiouy]";
        StringBuilder aliasBuilder = new StringBuilder();
        for (String word : words) {
            if (word == null || word.isEmpty())
                continue;
            aliasBuilder.append(word.charAt(0));
            String alias = aliasBuilder.toString().toLowerCase();
            if (!reservedAliases.contains(alias) && !(alias.length() == 1 && "ijknxyzt".contains(alias)))
                return alias;
        }
        aliasBuilder = new StringBuilder();
        for (String word : words) {
            String consonantsInWord = word.replaceAll(vowelsPattern, "");
            for (int j = 0; j < consonantsInWord.length(); j++) {
                aliasBuilder.append(consonantsInWord.charAt(j));
                String alias = aliasBuilder.toString().toLowerCase();
                if (!reservedAliases.contains(alias))
                    return alias;
            }
        }
        aliasBuilder = new StringBuilder();
        for (String word : words) {
            for (int j = 0; j < word.length(); j++) {
                aliasBuilder.append(word.charAt(j));
                String alias = aliasBuilder.toString().toLowerCase();
                if (!reservedAliases.contains(alias))
                    return alias;
            }
        }
        aliasBuilder = new StringBuilder();
        for (String word : words)
            aliasBuilder.append(word.charAt(0));
        for (int i = 0; i < 1000; i++) {
            String alias = String.format(aliasBuilder.toString().toLowerCase() + "%d", i);
            if (!reservedAliases.contains(alias))
                return alias;
        }
        return name;
    }

    @Override
    public void onDelete()
    {
        computeUI.closeFrame();
    }

    @Override
    public void onActive()
    {
        //FIXME: there is a problem because here we don't know if input port is empty or incorrect (empty) field is connected - which makes difference for
        //ComponentCalculator as it works in two modes as a calculator and as a generator

        LOGGER.debug(isFromVNA() + " inField: " + getInputFirstValue("inField"));

        boolean isDifferentField = false;
        boolean isNewField = false;
        if (getInputFirstValue("inField") != null) {
            Field newInField = ((VNField) getInputFirstValue("inField")).getField();
            isDifferentField = !isFromVNA() && (inField == null || !Arrays.equals(newInField.getComponentNames(), inField.getComponentNames()));
            isNewField = !isFromVNA() && (inField == null || newInField != inField);
            inField = newInField;
        } else
            inField = null;

        Parameters p;
        synchronized (parameters) {
            validateParamsAndSetSmart(isDifferentField);
            p = parameters.getReadOnlyClone();
        }

        notifyGUIs(p, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

        if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
            runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data
            try {
                outField = null;

                ArrayList<String> expressions = splitInput(p.get(EXPRESSION_LINES));
                outField = (inField == null) ? generateOutField(expressions, p) :
                                               calculateOutField(expressions, p);
                if (outField == null) {
                    VisNow.get().userMessageSend(this, "No data calculated", "", Level.INFO);
                    outRegularField = null;
                    outIrregularField = null;
                }
                else if (outField instanceof RegularField)
                    outRegularField = (RegularField) outField;
                else if (outField instanceof IrregularField)
                    outIrregularField = (IrregularField) outField;
                else if (outField instanceof PointField)
                    outPointField = (PointField) outField;

                setOutputValue("outRegularField",   outRegularField   == null ? null : new VNRegularField(outRegularField));
                setOutputValue("outIrregularField", outIrregularField == null ? null : new VNIrregularField(outIrregularField));
                setOutputValue("outPointField",     outPointField     == null ? null : new VNPointField(outPointField));

            } catch (Exception ex) {
                computeUI.setErrorText(ex.getMessage());
                setOutputValue("outRegularField", null);
                setOutputValue("outIrregularField", null);
                setOutputValue("outPointField", null);
            }
            prepareOutputGeometry();
            show();
        }
    }

    ArrayList<String> splitInput(String[] tmpexpr)
    {
        linesRemap = new int[tmpexpr.length][2];
        ArrayList<String> expressions = new ArrayList<>();
        for (int i = 0, l = 0; i < tmpexpr.length; i++) {
            linesRemap[l][0] = i + 1;
            String tmp = tmpexpr[i].replaceAll("//.*$", "").replaceAll(" +","");  //skipping comment
            while (tmp.contains("\\") && i < tmpexpr.length - 1) {
                tmp = tmp.substring(0, tmp.indexOf("\\")) +
                      tmpexpr[i + 1].replaceAll("//.*$", "").replaceAll(" +",""); // joining next line on \ as continuation
                i += 1;
            }
            if (!tmp.isEmpty() && tmp.contains("=")) {
                expressions.add(tmp);
                linesRemap[l][1] = i + 1;
                l += 1;
            }
        }
        return expressions;
    }

    private String[] splitExpression(String expression) throws Exception
    {
        String[] exprParts = new String[3];
        String[] tmp = expression.split("=", 2);
        if (tmp.length != 2)
        if (tmp[0].startsWith("_") ||
            tmp[0].equals("E") ||
            tmp[0].equals("PI") ||
            tmp[0].equals("I"))
            throw new Exception("predefined variable " + tmp[0] + "<p>can not be modified");
        exprParts[0] = tmp[0];
        String[] expr = tmp[1].split("[\\[\\]]");
        exprParts[1] = expr[0];
        exprParts[2] = expr.length > 1 ? expr[1] : "1";
        return exprParts;
    }

    private String[]  getRightSides(ArrayList<String> expressions) throws Exception
    {
        String[] exprRightSides = new String[expressions.size()];
        for (int i = 0; i < expressions.size(); i++) {
            String[] tmp = expressions.get(i).split("=", 2);
            if (tmp.length < 2)
                continue;
            String[] expr = tmp[1].split("[\\[\\]]");
            exprRightSides[i]  = expr[0];
        }
        return exprRightSides;
    }

    private void setResult(String name, Field out, DataArray result,
                           Parameters p, String unit, Map<String, DataArray> vars) throws Exception
    {
        if (name.equals("coordinates") || name.equals("coords")) {
            if (result.getVectorLength() != 3)
                throw new Exception("coords must be a length 3 vector");
            out.removeCoords();
            if (p.get(PRECISION) == Precision.SINGLE)
                out.setCoords(result.getTimeData());
            else {
                ArrayList<Float> timeList = result.getTimeSeries();
                for (int j = 0; j < timeList.size(); j++)
                    out.setCoords((FloatLargeArray)LargeArrayUtils.convert(result.getRawArray(timeList.get(j)), LargeArrayType.FLOAT), timeList.get(j));
            }
            if (!unit.equals("1")) {
                String[] coordUnits = unit.split(",");
                if (coordUnits.length != 3)
                    out.setCoordsUnit(coordUnits[0]);
                else
                    out.setCoordsUnits(coordUnits);
            }
        }
        else if (name.equals("mask") && result.getType() == DataArrayType.FIELD_DATA_LOGIC)
            out.setMask(result.getTimeData());
        else {
            DataArray res = result.cloneShallow();
            if (!name.endsWith("@")) {
                if (out.getComponent(name) != null)
                    out.removeComponent(name);
                res.setName(name);
                if (!unit.equals("1"))
                    res.setUnit(unit);
                out.addComponent(res);
            }
            else
                name = name.substring(0, name.length() - 1);
            if (vars.containsKey(name))
                vars.remove(name);
            for (int j = 0; j < 100; j++)
                if (vars.containsKey(name + "." + j))
                    vars.remove(name + "." + j);
            vars.put(name, res);
            if (res.getVectorLength() > 1)
                for (int j = 0; j < result.getVectorLength(); j++)
                    vars.put(name + "." + j, res);
        }
    }

    private void displayError(ArrayList<String> expressions, int i, Exception e)
    {
        String line = "line " + linesRemap[i][0];
        if (linesRemap[i][0] != linesRemap[i][1])
            line = line + "-" + linesRemap[i][1];
        String message = e.getMessage();
        if (message.equals("The component already exists"))
            try {
                message = splitExpression(expressions.get(i))[1] + " is already added to the oufield<p>and can not be added twice ";
            } catch (Exception ex) {
            }
        if (expressions.get(i).length() > 30)
            computeUI.setErrorText(line + ": <p>" + expressions.get(i).substring(0, 30) + "...<p>" + message);
        else
            computeUI.setErrorText(line + ": <p>" + expressions.get(i) + "<p>" + message);

    }

    private Field generateOutField(ArrayList<String> expressions, Parameters p) throws Exception
    {
        if (expressions.isEmpty()) {
            computeUI.setErrorText("<html>No valid instruction <p> (no line with '=')");
            return null;
        }

        int[] dims = p.get(GENERATOR_DIMENSION_LENGTHS);
        if (dims == null)
            return null;
        int dim = dims.length;
        float[][] extents = p.get(GENERATOR_EXTENTS);
        if (extents == null)
            return null;
        RegularField out = new RegularField(dims);
        out.setName("Generated field");

        float[][] affine = new float[4][3];
        for (int i = 0; i < dim; i++) {
            affine[3][i] = extents[0][i];
            affine[i][i] = (extents[1][i] - extents[0][i]) / (dims[i] - 1);
        }
        out.setAffine(affine);
        long nNodes = out.getNNodes();

        String[] fieldVariables = listAllFieldVariables(out, null, p.get(IGNORE_UNITS));
        String[] variablesInUse = ArrayExpressionParser.listVariablesInUse(fieldVariables, getRightSides(expressions));
        Map<String, DataArray> vars = prepareVariables(out, variablesInUse, p);

        for (int i = 0; i < expressions.size(); i++) {
            try {
                String[] exprParts = splitExpression(expressions.get(i));
                if (exprParts[0] == null || exprParts[0].isEmpty() ||
                    exprParts[1] == null || exprParts[1].isEmpty())
                    continue;
                String name = exprParts[0];
                String expr = exprParts[1];
                String unit = exprParts[2];
                ArrayExpressionParser parser = new ArrayExpressionParser(nNodes, p.get(PRECISION) == Precision.DOUBLE, p.get(IGNORE_UNITS), vars);
                DataArray result = parser.evaluateExpr(expr);
                if (result != null)
                    setResult(name, out, result, p, unit, vars);
            } catch (Exception e) {
                displayError(expressions, i, e);
                return null;
            }
        }
        return out;
    }

    private Field calculateOutField(ArrayList<String> expressions, Parameters p) throws Exception
    {
        if (inField == null || expressions.isEmpty() && (inField.getNComponents() == 0 || !p.get(RETAIN)))
            return null;
        Field out = inField.cloneShallow();
        if (!p.get(RETAIN)) {
            out.removeComponents();
            if (out instanceof IrregularField)
                for (CellSet cellSet : ((IrregularField)out).getCellSets())
                    cellSet.removeComponents();
        } else if (p.get(IGNORE_UNITS)) {
            for (int i = 0; i < out.getNComponents(); i++) {
                out.getComponent(i).setUnit("1");
            }
            out.setCoordsUnit("1");
            out.setTimeUnit("1");
        }

        long nNodes = inField.getNNodes();
        String[] fieldVariables = listAllFieldVariables(inField, p.get(INPUT_ALIASES), p.get(IGNORE_UNITS));
        String[] variablesInUse = ArrayExpressionParser.listVariablesInUse(fieldVariables, getRightSides(expressions));
        Map<String, DataArray> vars = prepareVariables(inField, variablesInUse, p);

        for (int i = 0; i < expressions.size(); i++) {
            try {
                String[] exprParts = splitExpression(expressions.get(i));
                if (exprParts[0] == null || exprParts[0].isEmpty() ||
                    exprParts[1] == null || exprParts[1].isEmpty())
                    continue;
                String name = exprParts[0];
                String expr = exprParts[1];
                String unit = exprParts[2];
                if (inField.getComponent(name) != null)
                    throw new Exception("wrong result variable name!");

                ArrayExpressionParser parser = new ArrayExpressionParser(nNodes, p.get(PRECISION) == Precision.DOUBLE, p.get(IGNORE_UNITS), vars);
                DataArray result = parser.evaluateExpr(expr);
                if (result != null) {
                    if (result.hasParent())
                        result = result.cloneShallow();
                    setResult(name, out, result, p, unit, vars);
                }
            } catch (Exception e) {
                displayError(expressions, i, e);
                return null;
            }
        }
        return out;
    }

    private boolean isVariableInUse(String var, String[] variablesInUse)
    {
        for (String varInUse : variablesInUse)
            if (varInUse.equals(var) || varInUse.startsWith(var + "."))
                return true;
        return false;
    }

    private DataArray extractCoordinateData(FloatLargeArray coords, String unit, int i)
    {
        long n = coords.length() / 3;
        FloatLargeArray crd = new FloatLargeArray(n);
        for (long j = 0; j < n; j++)
            crd.setFloat(j, coords.getFloat(3 * j + i));
        return DataArray.create(crd, 1, "coords." + i, unit, null);
    }

    private DataArray createCoordinateData(RegularField field, int i)
    {
        long[] dims       = field.getLDims();
        float[][] affine = field.getAffine();
        FloatLargeArray coord = new FloatLargeArray(field.getNNodes());
        switch (dims.length) {
            case 1:
                for (long x = 0; x < dims[0]; x++) {
                    coord.setFloat(x, affine[3][0] + x * affine[0][i]);
                }
                break;
            case 2:
                for (long y = 0, l = 0; y < dims[1]; y++) {
                    for (long x = 0; x < dims[0]; x++, l++) {
                        coord.setFloat(l, affine[3][i] + x * affine[0][i] + y * affine[1][i]);
                    }
                }
                break;
            case 3:
                for (long z = 0, l = 0; z < dims[2]; z++) {
                    for (long y = 0; y < dims[1]; y++) {
                        for (long x = 0; x < dims[0]; x++, l++) {
                            coord.setFloat(l, affine[3][i] + x * affine[0][i] + y * affine[1][i] + z * affine[2][i]);
                        }
                    }
                }
                break;
        }
        return DataArray.create(coord, 1, "coords." + i, field.getCoordsUnits()[i], null);
    }

    private Map<String, DataArray> prepareVariables(Field field, String[] variablesInUse, Parameters p)
    {
        Map<String, DataArray> vars = new HashMap<>();
        String alias;
        int veclen;
        for (int i = 0; i < field.getNComponents(); i++) {
            alias = p.get(INPUT_ALIASES)[i];
            veclen = field.getComponent(i).getVectorLength();
            if (isVariableInUse(alias, variablesInUse) && !vars.containsKey(alias)) {
                vars.put(alias, field.getComponent(i).cloneShallow());
                if (veclen > 1) {
                    for (int j = 0; j < veclen; j++) {
                        vars.put(alias + "." + j, vars.get(alias));
                    }
                }
            }
        }

        if (field.hasMask()) {
            if (isVariableInUse("mask", variablesInUse) && !vars.containsKey("mask")) {
                DataArray mask = DataArray.create(field.getCurrentMask(), 1, "mask");
                vars.put("mask", mask);
            }
        }

        if (field.hasCoords()) {
            if (isVariableInUse("coords", variablesInUse) && !vars.containsKey("coords"))
                vars.put("coords", DataArray.create(field.getCoords(), 3, "coords", field.getCoordsUnit(0), null));
            for (int i = 0; i < 3; i++) {
                if (isVariableInUse("coords." + i, variablesInUse) && !vars.containsKey("coords." + i)) {
                    if (vars.containsKey(COORD_SYMBOLS[i]))
                        vars.put("coords." + i, vars.get(COORD_SYMBOLS[i]));
                    else
                        vars.put("coords." + i, extractCoordinateData(field.getCurrentCoords(), field.getCoordsUnits()[i], i));
                }
                if (isVariableInUse(COORD_SYMBOLS[i], variablesInUse) && !vars.containsKey(COORD_SYMBOLS[i])) {
                    if (vars.containsKey("coords." + i))
                        vars.put(COORD_SYMBOLS[i], vars.get("coords." + i));
                    else
                        vars.put(COORD_SYMBOLS[i], extractCoordinateData(field.getCurrentCoords(), field.getCoordsUnits()[i], i));
                }
            }
        }

        if (field instanceof RegularField) {
            //indices
            if (isVariableInUse("index", variablesInUse) && !vars.containsKey("index")) {
                int[] dims = ((RegularField) field).getDims();
                LongLargeArray indices = new LongLargeArray(field.getNNodes() * dims.length);
                for (int i = 0; i < dims.length; i++) {
                    LongLargeArray ind = ((RegularField) field).getIndices(i);
                    for (long j = 0; j < field.getNNodes(); j++) {
                        indices.setLong(dims.length * j + i, ind.getLong(j));
                    }
                }
                vars.put("index", DataArray.create(indices, dims.length, "index"));

            }

            for (int i = 0; i < 3; i++) {
                if (isVariableInUse("index." + i, variablesInUse) && !vars.containsKey("index." + i)) {
                    if (vars.containsKey(INDEX_SYMBOLS[i]))
                        vars.put("index." + i, vars.get(INDEX_SYMBOLS[i]));
                    else
                        vars.put("index." + i, DataArray.create(field.getIndices(i), 1, "index." + i));
                }
                if (isVariableInUse(INDEX_SYMBOLS[i], variablesInUse) && !vars.containsKey(INDEX_SYMBOLS[i])) {
                    if (vars.containsKey("index." + i))
                        vars.put(INDEX_SYMBOLS[i], vars.get("index." + i));
                    else
                        vars.put(INDEX_SYMBOLS[i], DataArray.create(field.getIndices(i), 1, "index." + i));
                }
            }
            if (field.getCoords() == null) {

                if (isVariableInUse("coords", variablesInUse) && !vars.containsKey("coords")) {
                    vars.put("coords", DataArray.create(((RegularField) field).getCoordsFromAffine(), 3, "coords", field.getCoordsUnit(0), null));
                }
                for (int i = 0; i < 3; i++) {
                    if (isVariableInUse("coords." + i, variablesInUse) && !vars.containsKey("coords." + i)) {
                        if (vars.containsKey(COORD_SYMBOLS[i]))
                            vars.put("coords." + i, vars.get(COORD_SYMBOLS[i]));
                        else
                            vars.put("coords." + i, createCoordinateData((RegularField) field, i));
                    }
                    if (isVariableInUse(COORD_SYMBOLS[i], variablesInUse) && !vars.containsKey(COORD_SYMBOLS[i])) {
                        if (vars.containsKey("coords." + i))
                            vars.put(COORD_SYMBOLS[i], vars.get("coords." + i));
                        else
                            vars.put(COORD_SYMBOLS[i], createCoordinateData((RegularField) field, i));
                    }
                }
            }
        } //end regular field

        if (field instanceof IrregularField) {
            if (isVariableInUse("index", variablesInUse) && !vars.containsKey("index")) {
                if (vars.containsKey("_i"))
                    vars.put("index", vars.get("_i"));
                else
                    vars.put("index", DataArray.create(field.getIndices(0), 1, "index"));
            }
             
            if (isVariableInUse("_i", variablesInUse) && !vars.containsKey("_i")) {
                if (vars.containsKey("index"))
                    vars.put("_i", vars.get("index"));
                else
                    vars.put("_i", DataArray.create(field.getIndices(0), 1, "_i"));
            }

        } //end irregular field

        if (field instanceof PointField) {
            if (isVariableInUse("index", variablesInUse) && !vars.containsKey("index")) {
                if (vars.containsKey("_i"))
                    vars.put("index", vars.get("_i"));
                else
                    vars.put("index", DataArray.create(field.getIndices(0), 1, "index"));
            }
            
            if (isVariableInUse("_i", variablesInUse) && !vars.containsKey("_i")) {
                if (vars.containsKey("index"))
                    vars.put("_i", vars.get("index"));
                else
                    vars.put("_i", DataArray.create(field.getIndices(0), 1, "_i"));
            }

        } //end point field

        return vars;
    }

    private static String[] listAllFieldVariables(Field field, String[] aliases, boolean ignoreUnits)
    {
        if (field == null)
            return null;

        ArrayList<String> list = new ArrayList<>();
        list.add("index");
        if (field instanceof RegularField) {
            for (int i = 0; i < ((RegularField) field).getDims().length; i++) {
                list.add(INDEX_SYMBOLS[i]);
            }
            if(((RegularField) field).getDims().length > 1)
                for (int i = 0; i < ((RegularField) field).getDims().length; i++) {
                    list.add("index." + i);
                }
        } else if (field instanceof IrregularField) {
            list.add(INDEX_SYMBOLS[0]);
        } else if (field instanceof PointField) {
            list.add(INDEX_SYMBOLS[0]);
        }
  
        if (ignoreUnits || field.hasCoordsUnitsEqual())
            list.add("coords");
        for (int i = 0; i < 3;  i++) {
            list.add(COORD_SYMBOLS[i]);
            list.add("coords." + i);
        }            

        if (field.hasMask()) {
            list.add("mask");
        }

        for (int i = 0; i < field.getNComponents(); i++) {
            int veclen = field.getComponent(i).getVectorLength();
            String alias = aliases[i];
            list.add(alias);
            for (int j = 0; j < veclen; j++)
                list.add(alias + "." + j);
        }
        String[] out = new String[list.size()];
        list.toArray(out);
        return out;
    }

}
