//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.TableViewer;

import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayType;

/**
 * Class holding data series (string representation of VisNow component).
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class DataSet
{

    private final String name;
    private final LargeArray data;
    private final double[] stats;
    private final int[] dims;
    private final int veclen;
    private String format;
    
    public DataSet(String name, LargeArray data, int[] dims, int veclen, double[] stats)
    {
        this.name = name;
        this.data = data;
        this.dims = dims;
        this.veclen = veclen;
        this.stats = stats;
        this.format = "%s";
        LargeArrayType type = data.getType();
        if (type.isIntegerNumericType()) {
            this.format = "%d";
        } else if (type.isRealNumericType() || type.isComplexNumericType()) {
            this.format = "%.2f";
        }
    }

    public LargeArray getData()
    {
        return data;
    }

    public String getName()
    {
        return name;
    }

    public double[] getStats()
    {
        return stats;
    }

    public int[] getDimentions()
    {
        return dims;
    }

    public int getVectorLength()
    {
        return veclen;
    }

    public String getFormat()
    {
        return format;
    }
    
    public void setFormat(String format)
    {
        this.format = format;
    }
}
