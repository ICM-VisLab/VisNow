/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Glyphs;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrange;
import org.visnow.vn.lib.gui.FieldBasedUI.DownsizeUI.DownsizeParams;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class Params extends Parameters
{
    public static final int GEOMETRY_CHANGED = 3;
    public static final int GLYPHS_CHANGED = 2;
    public static final int COORDS_CHANGED = 1;
    protected ComponentSubrange validComponentRange = new ComponentSubrange(true, false, false, true, true, true);
    protected int change = 0;
    protected DownsizeParams downsizeParams = new DownsizeParams(10, 100000, false);
    protected static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Integer>("component", ParameterType.dependent, 0),
        new ParameterEgg<int[]>("lowCrop", ParameterType.dependent, null),
        new ParameterEgg<int[]>("upCrop", ParameterType.dependent, null),
        new ParameterEgg<Integer>("type", ParameterType.independent, 0),
        new ParameterEgg<Boolean>("constant diam", ParameterType.dependent, false),
        new ParameterEgg<Boolean>("constant thickness", ParameterType.dependent, false),
        new ParameterEgg<Float>("scale", ParameterType.dependent, .1f),
        new ParameterEgg<Float>("thickness", ParameterType.dependent, .1f),
        new ParameterEgg<Float>("line thickness", ParameterType.dependent, .1f),
        new ParameterEgg<Float>("transparency", ParameterType.dependent, 0f),
        new ParameterEgg<Float>("smax", ParameterType.dependent, .1f),
        new ParameterEgg<Integer>("lod", ParameterType.independent, 1),
        new ParameterEgg<Boolean>("useAbs", ParameterType.independent, true),
        new ParameterEgg<Boolean>("useSqrt", ParameterType.independent, false),
        new ParameterEgg<Boolean>("insideRange", ParameterType.dependent, true)};
    
    protected ChangeListener listener = new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent e) {
            change = GEOMETRY_CHANGED;
            fireStateChanged();
        }
    };

    public Params()
    {
        super(eggs);
        setValue("lowCrop", new int[]{0, 0, 0});
        setValue("upCrop",  new int[]{1, 1, 1});
        validComponentRange.addChangeListener(listener);
        downsizeParams.addChangeListener(listener);
    }

    public int getComponent()
    {
        return (Integer) getValue("component");
    }

    public void setComponent(int component)
    {
        setValue("component", component);
        change = GEOMETRY_CHANGED;
    }

    public ComponentSubrange getValidComponentRange()
    {
        return validComponentRange;
    }

    
    public boolean isInsideRange()
    {
        return (Boolean)getValue("insideRange");
    }

    public void setInsideRange(boolean inside)
    {
        setValue("insideRange", inside);
        change = GEOMETRY_CHANGED;
        fireStateChanged();
    }
    
    public long getPreferredSize()
    {
        return downsizeParams.getPreferredSize();
    }

    public int getDownsize()
    {
        return downsizeParams.getDownFactor();
    }

    public int[] getDown()
    {
        return downsizeParams.getDown();
    }

    public DownsizeParams getDownsizeParams()
    {
        return downsizeParams;
    }

    public int getLod()
    {
        return (Integer) getValue("lod");
    }

    public void setLod(int lod)
    {
        setValue("lod", lod);
        change = max(change, GLYPHS_CHANGED);
        fireStateChanged();
    }

    public int[] getLowCrop()
    {
        return (int[]) getValue("lowCrop");
    }

    public boolean isConstantDiam()
    {
        return (Boolean) getValue("constant diam");
    }

    public void setConstantDiam(boolean cDiam)
    {
        setValue("constant diam", cDiam);
        change = max(change, COORDS_CHANGED);
        fireStateChanged();
    }

    public boolean isConstantThickness()
    {
        return (Boolean) getValue("constant thickness");
    }

    public void setConstantThickness(boolean constantThickness)
    {
        setValue("constant thickness", constantThickness);
        change = max(change, COORDS_CHANGED);
        fireStateChanged();
    }

    public float getScale()
    {
        return (Float) getValue("scale");
    }

    public void setScale(float scale)
    {
        setValue("scale", scale);
        change = max(change, COORDS_CHANGED);
        fireStateChanged();
    }

    public float getThickness()
    {
        return (Float) getValue("thickness");
    }

    public void setThickness(float thickness)
    {
        setValue("thickness", thickness);
        change = max(change, COORDS_CHANGED);
        fireStateChanged();
    }

    public float getLineThickness()
    {
        return (Float) getValue("line thickness");
    }

    public void setLineThickness(float thickness)
    {
        setValue("line thickness", thickness);
        change = max(change, COORDS_CHANGED);
        fireStateChanged();
    }

    public float getTransparency()
    {
        return (Float) getValue("transparency");
    }

    public void setTransparency(float transparency)
    {
        setValue("transparency", transparency);
        change = max(change, COORDS_CHANGED);
        fireStateChanged();
    }
    public int getType()
    {
        return (Integer) getValue("type");
    }

    public void setType(int type)
    {
        setValue("type", type);
        change = max(change, GLYPHS_CHANGED);
        fireStateChanged();
    }

    public int[] getUpCrop()
    {
        return (int[]) getValue("upCrop");
    }

    public void setCrop(int[] lowCrop, int[] upCrop)
    {
        setValue("lowCrop", lowCrop);
        setValue("upCrop", upCrop);
        change = GEOMETRY_CHANGED;
        fireStateChanged();
    }

    public boolean isUseAbs()
    {
        return (Boolean) getValue("useAbs");
    }

    public void setUseAbs(boolean useAbs)
    {
        setValue("useAbs", useAbs);
        change = max(change, COORDS_CHANGED);
        fireStateChanged();
    }

    public boolean isUseSqrt()
    {
        return (Boolean) getValue("useSqrt");
    }

    public void setUseSqrt(boolean useSqrt)
    {
        setValue("useSqrt", useSqrt);
        change = max(change, COORDS_CHANGED);
        fireStateChanged();
    }

    public int getChange()
    {
        return change;
    }

    public void setChange(int change)
    {
        this.change = change;
    }

    @Override
    public void fireStateChanged()
    {
        if (!active)
            return;
        ChangeEvent e = new ChangeEvent(this);
        for (int i = 0; i < changeListenerList.size(); i++) {
            changeListenerList.get(i).stateChanged(e);
        }
    }

}
