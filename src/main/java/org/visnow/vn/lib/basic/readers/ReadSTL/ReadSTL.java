/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadSTL;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.j3d.loaders.stl.STLFileReader;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.utils.VectorMath;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import static org.visnow.vn.lib.basic.readers.ReadSTL.ReadSTLShared.PATH;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;

/**
 * @author theki
 */
public class ReadSTL extends OutFieldVisualizationModule {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ReadSTL.class);

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI computeUI;

    /**
     * Creates a new instance of STLReader
     */
    public ReadSTL() {
        parameters.addParameterChangelistener(new ParameterChangeListener() {
            @Override
            public void parameterChanged(String name) {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable() {
            @Override
            public void run() {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    public void onActive() {
        Parameters parametersClone = parameters.getReadOnlyClone();
        notifyGUIs(parametersClone, false, false);

        String path = parametersClone.get(PATH);
        ProgressAgent progressAgent = getProgressAgent(120); //100 for read, 20 for geometry
        if (path.isEmpty()) {
            outIrregularField = null;
        } else {
            try {
                
                File f = new File(path);
                outIrregularField = readSTL(f, progressAgent);
                if (outIrregularField != null)
                    VisNow.get().userMessageSend(this, "Successfully read: " + path, "", Level.INFO);
            } catch (IOException ex) {
                outIrregularField = null;
                LOGGER.error("", ex);
                VisNow.get().userMessageSend(this, "Error reading file: " + path, "See log for details", Level.ERROR);
            }
        }
        LOGGER.debug("Preparing output geometry");
        outField = outIrregularField;
        if (outIrregularField != null) {
            setOutputValue("outField", new VNIrregularField(outIrregularField));
        } else {
            setOutputValue("outField", null);
        }
        progressAgent.setProgressStep(100);
        prepareOutputGeometry();
        show();
    }

    @Override
    protected Parameter[] createDefaultParameters() {
        return new Parameter[]{
            new Parameter<>(PATH, new String())};
    }

    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending) {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onInitFinishedLocal() {
        if (isForceFlag()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    computeUI.activateOpenDialog();
                }
            });
        }
    }

    @Override
    public boolean isGenerator() {
        return true;
    }

    public static IrregularField readSTL(File f, ProgressAgent progressAgent) throws IOException {
        IrregularField out = null;
        STLFileReader reader;
        try {
            reader = new STLFileReader(f);
        } catch (Exception ex) {
            LOGGER.error(ex);
            return null;

        }
        double[] normal = new double[3];
        double[][] vertices = new double[3][3];

        int nObjects = reader.getNumOfObjects();
        String[] objectNames = reader.getObjectNames();
        int[] nFacets = reader.getNumOfFacets();
        int[][] cellsIndices = new int[nObjects][];
        HashMap<Node, Integer> vertexMap = new HashMap<Node, Integer>();

        int progressTotalSteps = VectorMath.vectorElementsSum(nFacets);
        int progressStep = 0;

        int progressMod = progressTotalSteps / 100;
        if (progressMod == 0) {
            progressMod = 1;
        }

        int nodeCounter = 0;
        for (int n = 0; n < nObjects; n++) {
            cellsIndices[n] = new int[3 * nFacets[n]];
            for (int cellIndex = 0; cellIndex < nFacets[n]; cellIndex++) {
                if (!reader.getNextFacet(normal, vertices)) {
                    throw new IllegalStateException("ERROR: not enough facets in STL file.");
                }
                for (int i = 0; i < 3; i++) {
                    float[] p = new float[3];
                    for (int j = 0; j < p.length; j++) {
                        p[j] = (float) vertices[i][j];
                    }
                    Node node = new Node(p);
                    if (!vertexMap.containsKey(node)) {
                        vertexMap.put(node, nodeCounter);
                        cellsIndices[n][3 * cellIndex + i] = nodeCounter;
                        nodeCounter++;
                    } else {
                        cellsIndices[n][3 * cellIndex + i] = vertexMap.get(node);
                    }
                }
                if (progressStep++ % progressMod == 0) {
                    progressAgent.setProgressStep(20 + 100 * progressStep / progressTotalSteps);
                }
            }
        }
        reader.close();

        LOGGER.debug("File read finished");

        Set<Node> nodeSet = vertexMap.keySet();
        int nNodes = vertexMap.size();
        FloatLargeArray coords = new FloatLargeArray(3 * (long) nNodes, false);
        Iterator<Node> nodeIterator = nodeSet.iterator();
        int i;
        while (nodeIterator.hasNext()) {
            Node n = nodeIterator.next();
            i = vertexMap.get(n);
            LargeArrayUtils.arraycopy(n.getCoords(), 0, coords, 3 * i, 3);
        }
        vertexMap.clear();

        vertexMap = null;
        Runtime.getRuntime().gc();

        out = new IrregularField(nNodes);
        out.setCurrentCoords(coords);
        byte[] dummy = new byte[nNodes];
        out.addComponent(DataArray.create(dummy, 1, "dummy"));
        for (int n = 0; n < nObjects; n++) {
            CellArray ca = new CellArray(CellType.TRIANGLE, cellsIndices[n], null, null);
            CellSet cs = new CellSet(objectNames[n] != null ? objectNames[n] : "NoName");
            cs.setCellArray(ca);
            out.addCellSet(cs);
            cs.generateDisplayData(coords);
        }
        return out;
    }

    private static class Node {

        float[] p;

        public Node(float[] p) {
            this.p = p;
        }

        public float[] getCoords() {
            return p;
        }

        @Override
        public int hashCode() {
            return java.util.Arrays.hashCode(p);
        }

        @Override
        public boolean equals(Object o) {
            if (o == null || p == null) {
                return false;
            }

            if (!(o instanceof Node)) {
                return false;
            }

            Node on = (Node) o;
            float[] op = on.getCoords();
            if (p.length != op.length) {
                return false;
            }

            for (int i = 0; i < p.length; i++) {
                if (p[i] != op[i]) {
                    return false;
                }
            }

            return true;
        }

    }
}
