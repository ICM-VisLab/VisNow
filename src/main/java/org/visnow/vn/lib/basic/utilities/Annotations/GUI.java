/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.utilities.Annotations;

import java.awt.Component;
import java.util.ArrayList;
import javax.swing.AbstractCellEditor;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import org.visnow.vn.geometries.parameters.FontParams;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class GUI extends javax.swing.JPanel
{

    public static class TextAreaRenderer extends JTextArea implements TableCellRenderer
    {

        public TextAreaRenderer()
        {
            setLineWrap(true);
            setWrapStyleWord(true);
        }

        @Override
        public Component getTableCellRendererComponent(JTable jTable,
                                                       Object obj, boolean isSelected, boolean hasFocus,
                                                       int row, int column)
        {
            setBackground(isSelected ? jTable.getSelectionBackground() : jTable.getBackground());
            setForeground(isSelected ? jTable.getSelectionForeground() : jTable.getForeground());
            setText((String)obj);
            return this;
        }
    }

    public static class TextAreaEditor extends AbstractCellEditor implements TableCellEditor
    {

        JComponent component = new JTextArea();

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected,
                                                     int rowIndex, int vColIndex)
        {
            ((JTextArea)component).setText((String) value);
            return component;
        }

        @Override
        public Object getCellEditorValue()
        {
            return ((JTextArea) component).getText();
        }
    }

    public class MyTableModelListener implements TableModelListener
    {

        private final JTable table;

        // It is necessary to keep the table since it is not possible
        // to determine the table from the event's source
        MyTableModelListener(JTable table)
        {
            this.table = table;
        }

        @Override
        public void tableChanged(TableModelEvent e)
        {
            updateData();
        }
    }

    private Params params = null;
    private ArrayList<float[]> coords = null;
    private ArrayList<String[]> texts = null;
    private DefaultTableModel annoTableModel;
    private final String path = null;
    private final String lastPath = null;
    private final JFileChooser fileChooser = new JFileChooser();
    private String[] columnIds;
    private TableColumnModel colModel;
    /**
     * Creates new form GUI
     */
    public GUI()
    {
        initComponents();
        columnIds = new String[annoTable.getColumnCount()];
        annoTableModel = (DefaultTableModel) annoTable.getModel();
        for (int i = 0; i < annoTable.getColumnCount(); i++)
             columnIds[i] = annoTableModel.getColumnName(i);
        colModel = annoTable.getColumnModel();
        colModel.getColumn(0).setPreferredWidth(160);
        colModel.getColumn(1).setPreferredWidth(60);
        colModel.getColumn(0).setCellRenderer(new TextAreaRenderer());
        colModel.getColumn(1).setCellRenderer(new TextAreaRenderer());
        colModel.getColumn(0).setCellEditor(new TextAreaEditor());
        annoTable.setRowHeight(60);
        annoTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        annoTableModel.addTableModelListener(new MyTableModelListener(annoTable));
        annoTable.getTableHeader().setReorderingAllowed(false);
    }

    public void setData(ArrayList<float[]> coords, ArrayList<String[]> texts)
    {
        this.coords = coords;
        this.texts  = texts;
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT
     * modify this code. The content of this method is always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        activateBox = new javax.swing.JCheckBox();
        infoLabel = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        annoTable = new javax.swing.JTable();
        delButton = new javax.swing.JButton();
        clearButton = new javax.swing.JButton();
        fontGUI = new org.visnow.vn.geometries.gui.FontGUI();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        outButton = new javax.swing.JButton();

        setOpaque(false);
        setLayout(new java.awt.GridBagLayout());

        activateBox.setSelected(true);
        activateBox.setText("active");
        activateBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                activateBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(activateBox, gridBagConstraints);

        infoLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        infoLabel.setForeground(new java.awt.Color(0, 0, 0));
        infoLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        infoLabel.setText("<html> Select point to annotate by ctrl-shift-click on any visible obiect<p>or use click-drag-shift-click point positioning</html> ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(8, 0, 8, 0);
        add(infoLabel, gridBagConstraints);

        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jScrollPane1.setToolTipText("Add points by clicking in the 3D viewer window");
        jScrollPane1.setMinimumSize(new java.awt.Dimension(150, 200));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(191, 250));

        annoTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "text", "coords"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        annoTable.setToolTipText("<html>The enter key inserts newline to the edited text.<p>\nTo finish the modifications, click any other cell cell</html>");
        annoTable.setOpaque(false);
        annoTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(annoTable);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel3.add(jScrollPane1, gridBagConstraints);

        delButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        delButton.setText("delete selected annotations");
        delButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        jPanel3.add(delButton, gridBagConstraints);

        clearButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        clearButton.setText("delete all annotations");
        clearButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel3.add(clearButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 4, 0);
        add(jPanel3, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(fontGUI, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.weighty = 1.0;
        add(filler1, gridBagConstraints);

        outButton.setText("output annotations");
        outButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        add(outButton, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void clearButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_clearButtonActionPerformed
    {//GEN-HEADEREND:event_clearButtonActionPerformed
        clear();
        updateGUI();
        infoLabel.setVisible(true);
    }//GEN-LAST:event_clearButtonActionPerformed

    private void delButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delButtonActionPerformed
        int selectedRow = annoTable.getSelectedRow();
        remove(selectedRow);
        updateGUI();
    }//GEN-LAST:event_delButtonActionPerformed

    private void activateBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_activateBoxActionPerformed
       if (params != null)
           params.setActivated(activateBox.isSelected());
    }//GEN-LAST:event_activateBoxActionPerformed

    private void outButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_outButtonActionPerformed
        params.output();
    }//GEN-LAST:event_outButtonActionPerformed

    public void setParams(Params p)
    {
        this.params = p;
    }


    public void add(float[] crds, String[] txts)
    {
        coords.add(crds);
        texts.add(txts);
        params.fireStateChanged();
    }

    public void remove(int i)
    {
        if (i >= 0 && i < coords.size()) {
            coords.remove(i);
            texts.remove(i);
            params.fireStateChanged();
        }
    }

    public void clear()
    {
        coords.clear();
        texts.clear();
        params.fireStateChanged();
    }

    void updateGUI()
    {
        params.setActive(false);
        activateBox.setSelected(params.isActivated());
        String[][] dataVals = new String[texts.size()][2];
        for (int i = 0; i < texts.size(); i++) {
            dataVals[i][0] = texts.get(i)[0];
            dataVals[i][1] = String.format("%6.3f\n %6.3f\n %6.3f",
                  coords.get(i)[0], coords.get(i)[1], coords.get(i)[2]);
        }
        annoTableModel.setDataVector(dataVals, columnIds);
        colModel.getColumn(0).setCellRenderer(new TextAreaRenderer());
        colModel.getColumn(1).setCellRenderer(new TextAreaRenderer());
        colModel.getColumn(0).setCellEditor(new TextAreaEditor());
        params.setActive(true);
    }

    public void setFontParams(FontParams fontParams)
    {
        fontGUI.setParams(fontParams);
    }

    private void updateData()
    {
        int nNodes = annoTable.getRowCount();
        texts.clear();
        for (int i = 0; i < nNodes; i++) {
            texts.add(new String[]{(String)annoTable.getValueAt(i, 0)});
        }
        params.fireParameterChanged("");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox activateBox;
    private javax.swing.JTable annoTable;
    private javax.swing.JButton clearButton;
    private javax.swing.JButton delButton;
    private javax.swing.Box.Filler filler1;
    private org.visnow.vn.geometries.gui.FontGUI fontGUI;
    private javax.swing.JLabel infoLabel;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton outButton;
    // End of variables declaration//GEN-END:variables
}
