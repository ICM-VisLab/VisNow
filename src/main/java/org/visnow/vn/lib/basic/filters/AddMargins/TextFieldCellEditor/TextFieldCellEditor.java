/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.AddMargins.TextFieldCellEditor;

import java.awt.Component;
import javax.swing.DefaultCellEditor;
import javax.swing.JTable;

/**
 *
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl), Warsaw University, ICM
 */

/* The solution described in post http://stackoverflow.com/questions/1652942/can-a-jtable-save-data-whenever-a-cell-loses-focus */
public class TextFieldCellEditor extends DefaultCellEditor
{

    TextFieldCell textField; // an instance of edit field
    Class<?> columnClass; // specifies cell type class
    Object valueObject; // for storing correct value before editing

    public TextFieldCellEditor(TextFieldCell tf, Class<?> cc)
    {
        super(tf);
        textField = tf;
        columnClass = cc;
        valueObject = null;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
        TextFieldCell tf = (TextFieldCell) super.getTableCellEditorComponent(table, value, isSelected, row, column);
        if (value != null) {
            tf.setText(value.toString());
        }
        // we have to save current value to restore it on another cell selection
        // if edited value couldn't be parsed to this cell's type
        valueObject = value;
        return tf;
    }

    @Override
    public Object getCellEditorValue()
    {
        try {
            // converting edited value to specified cell's type
            if (columnClass.equals(Double.class))
                return Double.parseDouble(textField.getText());
            else if (columnClass.equals(Float.class))
                return Float.parseFloat(textField.getText());
            else if (columnClass.equals(Integer.class))
                return Integer.parseInt(textField.getText());
            else if (columnClass.equals(Byte.class))
                return Byte.parseByte(textField.getText());
            else if (columnClass.equals(String.class))
                return textField.getText();
        } catch (NumberFormatException ex) {

        }

        // this handles restoring cell's value on jumping to another cell
        if (valueObject != null) {
            if (valueObject instanceof Double)
                return ((Double) valueObject).doubleValue();
            else if (valueObject instanceof Float)
                return ((Float) valueObject).floatValue();
            else if (valueObject instanceof Integer)
                return ((Integer) valueObject).intValue();
            else if (valueObject instanceof Byte)
                return ((Byte) valueObject).byteValue();
            else if (valueObject instanceof String)
                return (String) valueObject;
        }

        return null;
    }
}
