/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller.pointer3d;

import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.QuadArray;
import org.jogamp.java3d.Shape3D;
import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.Color4f;
import org.jogamp.vecmath.Point3f;
import org.jogamp.vecmath.Vector3f;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import org.visnow.vn.geometries.objects.generics.OpenTransformGroup;
import static org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller.pointer3d.Pointer3DExtensionSlice.createPlain;

/**
 * An extension to the arrow pointer meaning that a PLAIN pick 3D is active (a pick 3D that will
 * use a picked point and orientation of a cursor to determine a plain in 3D).
 * <p/>
 * It shows a square depicting a plain which will be picked. By default the square is parallel to
 * the arrow's shaft.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class Pointer3DExtensionSlice extends Pointer3DExtension
{

    /**
     * The simpliest constructor. Sample plain located in the arrow's head will be parallel to the
     * arrow's head.
     * <p/>
     * @param baseWidth Base width of the arrow
     */
    public Pointer3DExtensionSlice(float baseWidth)
    {
        this(baseWidth, false, null);
    }

    /**
     * The simpliest constructor. Sample plain located in the arrow's head will be parallel to the
     * arrow's head.
     * <p/>
     * @param baseWidth Base width of the arrow
     */
    public Pointer3DExtensionSlice(float baseWidth, boolean transparent)
    {
        this(baseWidth, transparent, null);
    }

    /**
     *
     * @param baseWidth      Width of the arrow's head (it's an equilateral triangle)
     * @param plainTransform Transform to be applied to the sample plain located in the arrow's
     *                       head.<br/>
     * If set to null, no transform will be applied - sample plain will
     * remain parallel to the arrow's head.
     */
    public Pointer3DExtensionSlice(float baseWidth, boolean transparent, Transform3D plainTransform)
    {
        super("Pointer3DExtensionSlice");

        OpenTransformGroup tra = new OpenTransformGroup("pointer slice transform group");
        Shape3D plain = createPlain(2 * baseWidth, 2 * baseWidth, vertexFormat);

        OpenAppearance appearance = createAppearance(transparent);
        plain.setAppearance(appearance);

        tra.addChild(plain);

        if (plainTransform != null)
            tra.setTransform(plainTransform);

        this.addChild(tra);
    }

    protected static Shape3D createPlain(float plainWidth,
                                         float plainHeight,
                                         int vertexFormat)
    {

        //<editor-fold defaultstate="collapsed" desc=" create plane (two rectangles) ">
        final int SIZE = 8;
        Point3f[] vertices = new Point3f[SIZE];
        float offsetH = plainHeight / 2;
        float offsetW = plainWidth / 2;

        vertices[0] = new Point3f(-offsetH, 0, offsetW);
        vertices[1] = new Point3f(-offsetH, 0, -offsetW);
        vertices[2] = new Point3f(offsetH, 0, -offsetW);
        vertices[3] = new Point3f(offsetH, 0, offsetW);

        for (int i = 4; i < 8; ++i) {
            vertices[i] = vertices[7 - i];
        }
        QuadArray qa = new QuadArray(SIZE, vertexFormat | GeometryArray.COLOR_4);
        qa.setCoordinates(0, vertices);
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc=" normals for light ">
        {
            Vector3f v1 = new Vector3f();
            Vector3f v2 = new Vector3f();
            Vector3f[] normals = new Vector3f[2 * 4];

            for (int i = 0; i <= 1; ++i) {
                v1.sub(vertices[4 * i], vertices[4 * i + 1]);
                v2.sub(vertices[4 * i + 1], vertices[4 * i + 2]);

                Vector3f n0 = new Vector3f();
                n0.cross(v1, v2);
                n0.normalize();

                for (int j = 4 * i; j < 4 * i + 4; ++j) {
                    normals[j] = n0;
                }
            }
            qa.setNormals(0, normals);

        }

        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc=" color of the plain ">

        /* Alpha works only when transparency was enabled in appearance (PointerBasicArrow3D)! */
        Color4f[] colors = new Color4f[8];
        colors[0] = new Color4f(1.0f, 0.0f, 0.0f, 0.3f);
        colors[1] = new Color4f(0.0f, 1.0f, 0.0f, 0.3f);
        colors[2] = new Color4f(1.0f, 0.0f, 0.0f, 0.3f);
        colors[3] = new Color4f(1.0f, 1.0f, 0.0f, 0.3f);

        for (int i = 4; i < 8; ++i) {
            colors[i] = colors[7 - i];
        }

        qa.setColors(0, colors);
        //</editor-fold>

        Shape3D plain = new Shape3D(qa);
        return plain;
    }
}
