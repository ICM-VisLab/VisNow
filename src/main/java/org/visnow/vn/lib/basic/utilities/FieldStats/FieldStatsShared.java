/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.utilities.FieldStats;

import org.visnow.jscic.DataContainerSchema;
import org.visnow.vn.engine.core.ParameterName;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class FieldStatsShared
{


    public static final ParameterName<Boolean> LOG_SCALE = new ParameterName("Log scale");
    public static final ParameterName<Integer> SELECTED_COMPONENT = new ParameterName("Selected component");
    public static final ParameterName<Integer> BIN_COUNT = new ParameterName("Number of bins");
    

    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic)    
    public static final ParameterName<String> META_NAME = new ParameterName("META field name");
    public static final ParameterName<String[]> META_COMPONENT_NAMES = new ParameterName("META component names");
    public static final ParameterName<DataContainerSchema> META_SCHEMA = new ParameterName("META field schema");
    public static final ParameterName<Long> META_NNODES = new ParameterName("META field nnodes");
    public static final ParameterName<int[]> META_DIMS = new ParameterName("META field dimensions");
    public static final ParameterName<float[][]> META_EXTENDS = new ParameterName("META field extends");
    public static final ParameterName<float[][]> META_PHYS_EXTENDS = new ParameterName("META field physical extends");
    public static final ParameterName<double[]> META_AVG_GRAD = new ParameterName("META field avgGrad");
    public static final ParameterName<double[]> META_STDDEV_GRAD = new ParameterName("META field stdDevGrad");
    public static final ParameterName<long[][]> META_HISTOGRAM = new ParameterName("META histogram");
    public static final ParameterName<long[][]> META_DERIV_HISTOGRAM = new ParameterName("META derivHistogram");
    public static final ParameterName<double[][]> META_THR_HISTOGRAM = new ParameterName("META thrHistogram");
    public static final ParameterName<Boolean> META_IS_REGULAR_FIELD = new ParameterName("META is regular field");
    public static final ParameterName<Boolean> META_IS_HAS_MASK_OR_IS_TIME_DEPENDENT = new ParameterName("META field has a mask or is time dependent");
}
