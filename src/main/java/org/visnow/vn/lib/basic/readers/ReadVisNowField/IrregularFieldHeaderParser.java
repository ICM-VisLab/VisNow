/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadVisNowField;

import org.apache.log4j.Logger;
import java.io.File;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Vector;
import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jscic.DataContainer;
import org.visnow.vn.lib.basic.readers.ReadVisNowField.utils.*;
import org.visnow.vn.lib.utils.io.VNIOException;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class IrregularFieldHeaderParser extends HeaderParser
{
    private static final Logger LOGGER = Logger.getLogger(IrregularFieldHeaderParser.class);

    public static final String[][] PREFIXES        = {{"point", "pt"}, {"line", "seg"}, {"tri"}, {"quad"}, {"tet"}, {"pyr"}, {"prism"}, {"hex"}};
    public static final String[] FLAT_CELL_PREFIXES = {"point", "pt",   "line", "seg",   "tri",   "quad",   "tet",   "pyr",   "prism",   "hex"};

    protected IrregularField irregularField;

    public IrregularFieldHeaderParser(LineNumberReader r, File headerFile, String fileName)
    {
        super(r, headerFile, fileName);
    }

    protected CellSet parseCellSetEntry()
            throws VNIOException
    {
        Vector<String[]> res = new Vector<>();
        ParseResult result = processLine(line, new String[]{"cell"}, new String[]{"file"}, res);
        if (result != ParseResult.ACCEPTED)
            return null;
        String name = res.get(0)[1];
        CellSet cellSet = new CellSet(name);
        if (res.size() > 1)
            try {
                String[] item = res.get(1);
                if (item[0].startsWith("n"))
                    cellSet.setNDataValues(Integer.parseInt(item[1]));
            } catch (Exception e) {
                throw new VNIOException("cell set ndata field has invalid or missing value ", fileName, r.getLineNumber());
            }
        else
            cellSet.setNDataValues(-1);
        cell_array_loop:
        while (true) {
            line = nextLine();
            result = processLine(line, FLAT_CELL_PREFIXES,
                                 new String[]{"comp", "cmp", "file"}, res);
            switch (result) {
                case ACCEPTED:
                    String[] strings = res.get(0);
                    int type = -1;
                    int n = 0;
                    cell_type_loop:
                    for (int i = 0; i < PREFIXES.length; i++)
                        for (String item : PREFIXES[i]) {
                            if (strings[0].startsWith(item)) {
                                type = i;
                                try {
                                    n = Integer.parseInt(strings[1]);
                                } catch (NumberFormatException e) {
                                }
                                break cell_type_loop;
                            }
                            if (strings[1].startsWith(item)) {
                                type = i;
                                try {
                                    n = Integer.parseInt(strings[0]);
                                } catch (NumberFormatException e) {

                                }
                                break cell_type_loop;
                            }
                        }
                    if (type == -1)
                        break;
                    int[] nodes = new int[n * CellType.getType(type).getNVertices()];
                    int[] dataIndices = new int[n];
                    byte[] orientations = new byte[n];
                    CellArray ca = new CellArray(CellType.getType(type), nodes, orientations, dataIndices);
                    cellSet.addCells(ca);
                    break;
                case EOF:
                    return cellSet;
                case ERROR:
                    throw new VNIOException("invalid cell array entry ", fileName, r.getLineNumber());
                case BREAK:
                    break cell_array_loop;
                default:
                    break cell_array_loop;
            }
        }
        Vector<String> cellSetComponentNames = new Vector<>();
        DataArray currentComponent;
        component_loop:
        while ((currentComponent = parseComponentEntry(line, cellSet.getNDataValues(),
                                                       fileName, r.getLineNumber())) != null) {
            for (int i = 0; i < cellSetComponentNames.size(); i++)
                if (currentComponent.getName().equalsIgnoreCase(cellSetComponentNames.get(i))) {
                    throw new VNIOException("duplicate component name " + currentComponent.getName() +
                        ",<p>only first one is valid", fileName, r.getLineNumber());
                }
            cellSetComponentNames.add(currentComponent.getName());
            cellSet.addComponent(currentComponent);
            line = nextLine();
        }
        return cellSet;
    }

    public IrregularFieldIOSchema parseHeader()
            throws VNIOException
    {
        IrregularFieldIOSchema schema = null;
        Vector<String[]> res = new Vector<>();
        String name = "";
        long nnodes = 0;
        String[] userData = null;
        boolean hasMask = false;
        try {
            line = nextLine();
            ParseResult result = processLine(line, new String[]{"field", "name"},
                                     new String[]{"c", "file"}, res);
            switch (result) {
                case ACCEPTED:
                    for (int i = 0; i < res.size(); i++) {
                        String[] strings = res.get(i);
                        if ((strings[0].startsWith("name") || strings[0].startsWith("field")) && strings.length > 1)
                            name = strings[1];
                        else if (strings[0].startsWith("nnodes") || strings[0].startsWith("nodes") || strings[0].startsWith("el")) {
                            if (strings.length < 2) {
                                throw new VNIOException("no nodes count specified", fileName, r.getLineNumber());
                            }
                            try {
                                nnodes = Long.parseLong(strings[1]);
                            } catch (NumberFormatException e) {
                                throw new VNIOException("node count " + strings[1] + " is not integer ", fileName, r.getLineNumber());
                            }
                        }
                        else if (strings[0].startsWith("valid") || strings[0].startsWith("mask"))
                            hasMask = true;
                        else  if (strings[0].startsWith("user:")) {
                            userData = strings[0].substring(5).split(";");
                            for (int j = 0; j < userData.length; j++) {
                                if (userData[j].startsWith("__"))
                                    try {
                                        int k = Integer.parseInt(userData[j].substring(2));
                                        userData[j] = stringsInLine[k];
                                    } catch (NumberFormatException e) {
                                    }
                            }
                        }
                    }
                    break;
                case EOF:
                    throw new VNIOException("no field description line ", fileName, r.getLineNumber());
                case ERROR:
                    throw new VNIOException("bad field file ", fileName, r.getLineNumber());
                case BREAK:
                    throw new VNIOException("no field description line ", fileName, r.getLineNumber());
                default:
                    break;
            }
            if (nnodes < 1)
                throw new VNIOException("no nodes count specified ", fileName, r.getLineNumber());
            irregularField = new IrregularField(nnodes);
            irregularField.setName(name);
            if (hasMask)
                irregularField.setCurrentMask(new LogicLargeArray(irregularField.getNNodes(), true));
            irregularField.setUserData(userData);
            schema = new IrregularFieldIOSchema(irregularField, headerFile, fileName);
            line = nextLine();
            parseAxesDescription(irregularField);
            parseExtentEntry(irregularField);
            parseDataItems(irregularField);
            CellSet newCellSet = null;
            while ((newCellSet = parseCellSetEntry()) != null)
                irregularField.addCellSet(newCellSet);
            field = irregularField;

            for (CellSet cellSet : irregularField.getCellSets())
                tNames.add(cellSet.getName());
            createReservedNames();

            DataFileSchema dataFileSchema;
            schema.generateDataInputStatus();
            parsedSchema = schema;
            file_loop:
            while ((dataFileSchema = parseFileEntry()) != null)
                schema.addFileSchema(dataFileSchema);
            r.close();

        } catch (IOException e) {
            throw new VNIOException("bad header file ", fileName, r.getLineNumber());
        }
        return schema;
    }

    @Override
    protected int parseComponentSchema(DataContainer field, String[] strings, FileType fileType, Vector<DataElementIOSchema> compSchemas, int cOffset)
            throws VNIOException
    {
        int crd = 0, cmp = -1;
        int[] offsets = {-1, -1};
        cOffset = parseOffset(strings, fileType, cOffset, offsets);
        String[] keyData = strings[0].split("\\.");
        if (keyData.length == 1)
            crd = -1;
        else {
            try {
                crd = Integer.parseInt(keyData[keyData.length - 1]);
            } catch (NumberFormatException e) {
                crd = -1;
            }
        }
        String elementName = keyData[0];
        cmp = getComponentIndex(irregularField, elementName);
        if (cmp >= 0) {
            DataArray da = irregularField.getComponent(elementName);
            ComponentIOSchema cmpSchema = new ComponentIOSchema(irregularField, cmp, crd, da.getType(),
                                                                da.getVectorLength(), (int) irregularField.getNNodes(), offsets[0], offsets[1]);
            cmpSchema.setCmpName(da.getName());
            parsedSchema.dataElementFound(da.getName());
            compSchemas.add(cmpSchema);
            int offUnit = 1;
            if (fileType.isBinary())
                offUnit = types[cmp].getSize();
            if (crd == -1)
                return cOffset + offUnit * vlens[cmp];
            else
                return cOffset + offUnit;
        }
        if (elementName.startsWith("coord")) {
            cmp = irregularField.getNComponents();
            ComponentIOSchema cmpSchema = new ComponentIOSchema(irregularField, cmp, crd, DataArrayType.FIELD_DATA_FLOAT,
                                                                3, (int) irregularField.getNNodes(), offsets[0], offsets[1]);
            cmpSchema.setCmpName("coords");
            parsedSchema.dataElementFound("coords");
            compSchemas.add(cmpSchema);
            int offUnit = 1;
            if (fileType.isBinary())
                offUnit = 4;
            if (crd == -1)
                return cOffset + 3 * offUnit;
            else
                return cOffset + offUnit;
        }
        if (irregularField.hasMask() && elementName.startsWith("mask")) {
            cmp = irregularField.getNComponents() + 1;
            ComponentIOSchema cmpSchema = new ComponentIOSchema(irregularField, cmp, crd, DataArrayType.FIELD_DATA_LOGIC,
                                                                1, (int) irregularField.getNNodes(), offsets[0], offsets[1]);
            cmpSchema.setCmpName("mask");
            parsedSchema.dataElementFound("mask");
            compSchemas.add(cmpSchema);
            return cOffset + 1;
        }
        String[] dataID = elementName.split(":");
        for (int k = 0; k < irregularField.getNCellSets(); k++) {
            CellSet cs = irregularField.getCellSet(k);
            if (cs.getName().equalsIgnoreCase(dataID[0])) {
                if (dataID.length == 2) {
                    cmp = getComponentIndex(cs, dataID[1]);
                    if (cmp >= 0) {
                        DataArray da = cs.getComponent(cmp);
                        ComponentIOSchema cmpSchema = new ComponentIOSchema(cs, cmp, crd, da.getType(),
                                                                            da.getVectorLength(), (int) cs.getNElements(), offsets[0], offsets[1]);
                        cmpSchema.setCmpName(da.getName());
                        parsedSchema.dataElementFound(cs.getName() + ":" + da.getName());
                        compSchemas.add(cmpSchema);
                        int offUnit = 1;
                        if (fileType.isBinary())
                            offUnit = da.getType().getSize();
                        if (crd == -1)
                            return cOffset + offUnit * da.getVectorLength();
                        else
                            return cOffset + offUnit;
                    }
                } else {
                    for (int i = 0; i < PREFIXES.length; i++)
                        for (String item : PREFIXES[i])
                            if (dataID[1].startsWith(item)) {
                                if (dataID[2].startsWith("ind")) {
                                    IntArrayIOSchema aSch = new IntArrayIOSchema(cs, new IntLargeArray(cs.getCellArray(CellType.getType(i)).getDataIndices()), 1,
                                                                                 cs.getCellArray(CellType.getType(i)).getNCells(), -1, offsets[0], offsets[1]);
                                    parsedSchema.dataElementFound(cs.getName() + ":" + CellType.getType(i).getPluralName() + ":indices");
                                    compSchemas.add(aSch);

                                    if (fileType.isBinary())
                                        return cOffset + 4;
                                    else
                                        return cOffset + 1;
                                } else if (dataID[2].startsWith("ori")) {
                                    BooleanArrayIOSchema aSch = new BooleanArrayIOSchema(cs, new LogicLargeArray(cs.getCellArray(CellType.getType(i)).getOrientations()), 1,
                                                                                         cs.getCellArray(CellType.getType(i)).getNCells(), 0, offsets[0], offsets[1]);
                                    compSchemas.add(aSch);
                                    return cOffset + 1;
                                } else {
                                    int vlen = CellType.getType(i).getNVertices();
                                    IntArrayIOSchema aSch = new IntArrayIOSchema(cs, new IntLargeArray(cs.getCellArray(CellType.getType(i)).getNodes()), vlen,
                                                                                 cs.getCellArray(CellType.getType(i)).getNCells(), -1, offsets[0], offsets[1]);
                                    parsedSchema.dataElementFound(cs.getName() + ":" + CellType.getType(i).getPluralName() + ":nodes");
                                    compSchemas.add(aSch);
                                    if (fileType.isBinary())
                                        return cOffset + 4 * vlen;
                                    else
                                        return cOffset + vlen;
                                }
                            }
                }
            }
        }
        throw new VNIOException("can not find data item (component, cell array data etc): <p>" + elementName, fileName, r.getLineNumber());
    }

    public static void main(String[] args)
    {
        try {
            Parser  headerParser = new Parser("/home/know/sample_data/VisNowFields/flow.vnf", false, null);
            FieldIOSchema schema = headerParser.parseFieldHeader();
            for (String string : schema.getDescription()) {
                System.out.println(string);
            }
        } catch (Exception e) {
        }
    }
}
