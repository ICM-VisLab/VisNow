/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.RegularFieldSlice;

import org.visnow.vn.engine.core.ParameterName;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class RegularFieldSliceShared
{
    //Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    //
    //Specification:
    //>=0 <META_FIELD_DIMENSION_LENGTHS.length
    public static final ParameterName<Integer> SELECTED_AXIS = new ParameterName<>("Selected axis");
    public static final ParameterName<Boolean> RECALCULATE_MIN_MAX = new ParameterName<>("Recalculate min max");
    //>=0 <META_FIELD_DIMENSION_LENGTHS[0, 1 or 2 depends on SELECTED_AXIS]
    public static final ParameterName<Integer> SLICE_INDEX = new ParameterName<>("Slice index");

    public static final ParameterName<Boolean> ADJUSTING = new ParameterName<>("Adjusting");

    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic
    //not null array of integers, length = 3, values >=2
    public static final ParameterName<int[]> META_FIELD_DIMENSION_LENGTHS = new ParameterName("META dimension lengths");

    public static int getSmartSlice(int dimLength) {
        return dimLength / 2;
    }
}
