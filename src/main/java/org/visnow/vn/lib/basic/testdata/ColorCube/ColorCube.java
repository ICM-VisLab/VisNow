/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.testdata.ColorCube;

import org.visnow.jscic.CellArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.cells.CellType;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ColorCube extends OutFieldVisualizationModule
{

    protected boolean fromUI = false;
    public static OutputEgg[] outputEggs = null;

    /**
     * Creates a new instance of TestGeometryObject
     */
    public ColorCube()
    {
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                setPanel(ui);
            }
        });
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    public void createTestField()
    {
        int nPoints = 8;
        outIrregularField = new IrregularField(nPoints);
        float[] coords = new float[]{
            -1, -1, -1,
            1, -1, -1,
            1, 1, -1,
            -1, 1, -1,
            -1, -1, 1,
            1, -1, 1,
            1, 1, 1,
            -1, 1, 1
        };
        int[] colors = new int[]{
            0, 0, 0,
            255, 0, 0,
            255, 255, 0,
            0, 255, 0,
            0, 0, 255,
            255, 0, 255,
            255, 255, 255,
            0, 255, 255
        };
        outIrregularField.setCurrentCoords(new FloatLargeArray(coords));
        byte[] bColors = new byte[24];
        for (int i = 0; i < bColors.length; i++)
            bColors[i] = (byte) (0xff & colors[i]);

        CellSet cs = new CellSet();
        cs.addCells(new CellArray(CellType.HEXAHEDRON, new int[]{0, 1, 2, 3, 4, 5, 6, 7}, null, new int[]{0}));
        cs.generateDisplayData(new FloatLargeArray(coords));
        outIrregularField.addCellSet(cs);
        DataArray cls = DataArray.create(bColors, 3, "colors");
        cls.setUserData(new String[]{"colors"});
        outIrregularField.addComponent(cls);

    }

    @Override
    public void onActive()
    {
        createTestField();
        setOutputValue("outField", new VNIrregularField(outIrregularField));
        outField = outIrregularField;
        prepareOutputGeometry();
        show();
    }
}
