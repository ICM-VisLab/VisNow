/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.FieldCombiner;

import java.util.Vector;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.PointField;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.types.VNIrregularField;
import org.visnow.vn.lib.types.VNPointField;
import org.visnow.vn.lib.types.VNRegularField;

/**
 *
 * @author Krzysztof S. Nowinski
 * Warsaw University, ICM
 */
public class FieldCombiner extends OutFieldVisualizationModule
{

    public static InputEgg[]  inputEggs  = null;
    public static OutputEgg[] outputEggs = null;

    public FieldCombiner()
    {
        setPanel(ui);
    }

    @Override
    public void onActive()
    {
        Vector fields = getInputValues("inFields");
        if (fields == null || fields.isEmpty())
            return;
        boolean firstField = true;
        for (int i = 0; i < fields.size(); i++) {
            if (fields.get(i) == null || ((VNField) (fields.get(i))).getField() == null)
                continue;
            if (firstField) {
                outField = ((VNField) (fields.get(i))).getField().cloneShallow();
                firstField = false;
            } else {
                Field f = ((VNField) (fields.get(i))).getField();
                if (f.getNNodes() == outField.getNNodes())
                    for (int j = 0; j < f.getNComponents(); j++) 
                        outField.addComponent(f.getComponent(j).cloneShallow().
                                              name(f.getComponent(j).getName() + i + "." + j));
            }
        }
        if (outField == null)
            return;

        if (outField != null && outField instanceof RegularField) {
            setOutputValue("outRegularField", new VNRegularField((RegularField) outField));
            setOutputValue("outIrregularField", null);
            setOutputValue("outPointField", null);
        } else if (outField != null && outField instanceof IrregularField) {
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", new VNIrregularField((IrregularField) outField));
            setOutputValue("outPointField", null);
        } else if (outField != null && outField instanceof PointField) {
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", null);
            setOutputValue("outPointField", new VNPointField((PointField) outField));
        } else {
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", null);
            setOutputValue("outPointField", null);
        }
        prepareOutputGeometry();
        show();
    }
}
