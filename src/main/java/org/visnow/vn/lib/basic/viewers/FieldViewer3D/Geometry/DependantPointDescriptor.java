/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry;

import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.visnow.jscic.RegularField;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class DependantPointDescriptor extends PointDescriptor
{

    private CalculablePoint cp = null;
    private RegularField field = null;
    private static final Logger LOGGER = Logger.getLogger(DependantPointDescriptor.class);

    public DependantPointDescriptor(String s, CalculablePoint cp, RegularField field)
    {
        super(s, null, null);
        this.cp = cp;
        this.field = field;
    }

    public DependantPointDescriptor(int n, CalculablePoint cp, RegularField field)
    {
        super(n, null, null);
        this.cp = cp;
        this.field = field;
    }

    @Override
    public int[] getIndices()
    {
        if (cp == null){
            LOGGER.debug(getName() + " index cp is null");
            return null;
        }

        float[] tmp = cp.getValue();
        if (tmp != null)
            return field.getIndices(tmp[0], tmp[1], tmp[2]);
        else{
            LOGGER.debug(getName() + " index cp.value is null");
            float[] value = cp.getValue();
        }

        return null;
    }

    @Override
    public void setIndices(int[] indices)
    {
        LOGGER.debug("setting indices here is not recommended");
    }

    @Override
    public float[] getWorldCoords()
    {
        if (cp == null){
            System.out.println(getName() + " coords cp is null");
            return null;
        }
        if(cp.getValue() == null)
        System.out.println(getName() + " coords cp.value is null");

        return cp.getValue();
    }

    @Override
    public void setWorldCoords(float[] coords)
    {
        LOGGER.debug("setting coords here is not recommended");
    }

    @Override
    public float[] getPhysicalCoords()
    {
        return null;
    }

    @Override
    public void setPhysicalCoords(float[] physicalCoords)
    {
    }

    @Override
    public boolean isDependant()
    {
        return true;
    }

    public boolean dependsOn(PointDescriptor pd)
    {
        if (cp == null)
            return false;

        ArrayList<PointDescriptor> pds = cp.getDependantPointDescriptors();
        for (int i = 0; i < pds.size(); i++) {
            if (pds.get(i) == pd)
                return true;
        }

        return false;
    }
}
