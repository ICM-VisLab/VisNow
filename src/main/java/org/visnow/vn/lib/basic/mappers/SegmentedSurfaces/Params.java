/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.SegmentedSurfaces;

import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.Parameter;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class Params extends Parameters
{

    /**
     * subsets - selected subsets to be displayed
     * low - input field will be cropped from below according to these indices before isosurfacing
     * up - input field will be cropped from above according to these indices before isosurfacing
     * downsize - input field will be downsized according to these indices before isosurfacing
     */
    protected Parameter<int[]> downsize;
    protected Parameter<int[]> low;
    protected Parameter<int[]> up;
    protected Parameter<Boolean> dimensionsChanged;
    protected Parameter<Integer> nThreads;
    protected Parameter<Boolean> smoothing;
    protected Parameter<Integer> smoothSteps;

    @SuppressWarnings("unchecked")
    private void initParameters()
    {
        downsize = getParameter("downsize");
        low = getParameter("low");
        up = getParameter("up");
        dimensionsChanged = getParameter("dimensionsChanged");
        nThreads = getParameter("nThreads");
        smoothing = getParameter("smoothing");
        smoothSteps = getParameter("smoothSteps");

        dimensionsChanged.setValue(true);
        downsize.setValue(new int[]{2, 2, 2});
        low.setValue(new int[]{0, 0, 0});
        up.setValue(new int[]{0, 0, 0});
        nThreads.setValue(1);
        smoothing.setValue(false);
        smoothSteps.setValue(0);
    }

    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<int[]>("downsize", ParameterType.dependent),
        new ParameterEgg<int[]>("low", ParameterType.dependent),
        new ParameterEgg<int[]>("up", ParameterType.dependent),
        new ParameterEgg<Boolean>("dimensionsChanged", ParameterType.independent),
        new ParameterEgg<Integer>("nThreads", ParameterType.independent),
        new ParameterEgg<Boolean>("smoothing", ParameterType.independent),
        new ParameterEgg<Integer>("smoothSteps", ParameterType.independent)
    };

    public Params()
    {
        super(eggs);
        initParameters();
    }

    /**
     * Get the value of nThreads
     *
     * @return the value of nThreads
     */
    public int getNThreads()
    {
        return nThreads.getValue();
    }

    /**
     * Set the value of nThreads
     *
     * @param nThreads new value of nThreads
     */
    public void setNThreads(int nThreads)
    {
        this.nThreads.setValue(nThreads);
    }

    /**
     * Set the value of subsets
     *
     * @param subsets new value of subsets
     */
    public int[] getDownsize()
    {
        return downsize.getValue();
    }

    public void setDownsize(int[] downsize)
    {
        dimensionsChanged.setValue(false);
        int[] oldDown = this.downsize.getValue();
        if (oldDown == null)
            dimensionsChanged.setValue(true);
        else
            for (int i = 0; i < oldDown.length; i++)
                if (oldDown[i] != downsize[i])
                    dimensionsChanged.setValue(true);
        this.downsize.setValue(downsize);
    }

    public int[] getLow()
    {
        return low.getValue();
    }

    public void setLowUp(int[] low, int[] up)
    {
        dimensionsChanged.setValue(this.up.getValue() != up || this.low.getValue() != low);
        if (dimensionsChanged.getValue())
            smoothing.setValue(smoothSteps.getValue() != 0);
        this.low.setValue(low);
        this.up.setValue(up);
    }

    public int[] getUp()
    {
        return up.getValue();
    }

    /**
     * Get the value of dimensionsChanged
     *
     * @return the value of dimensionsChanged
     */
    public boolean isDimensionsChanged()
    {
        return dimensionsChanged.getValue();
    }

    /**
     * Set the value of dimensionsChanged
     *
     * @param dimensionsChanged new value of dimensionsChanged
     */
    public void setDimensionsChanged(boolean dimensionsChanged)
    {
        this.dimensionsChanged.setValue(dimensionsChanged);
    }

    /**
     * Utility field holding list of ChangeListeners.
     */
    private transient ArrayList<ChangeListener> changeListenerList
        = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     * <p>
     * @param listener The listener to register.
     */
    @Override
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */
    @Override
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param object Parameter #1 of the <CODE>ChangeEvent<CODE> constructor.
     */
    @Override
    public void fireStateChanged()
    {
        ChangeEvent e = new ChangeEvent(this);
        for (ChangeListener listener : changeListenerList)
            listener.stateChanged(e);
    }

    public int getSmoothSteps()
    {
        return smoothSteps.getValue();
    }

    public void setSmoothSteps(int smoothSteps)
    {
        this.smoothSteps.setValue(smoothSteps);
        smoothing.setValue(smoothSteps != 0);
    }

    public boolean isSmoothing()
    {
        return smoothing.getValue();
    }

    public void setSmoothing(boolean smoothing)
    {
        this.smoothing.setValue(smoothing && smoothSteps.getValue() != 0);
    }
}
