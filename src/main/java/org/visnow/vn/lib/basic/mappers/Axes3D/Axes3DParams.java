/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.Axes3D;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.parameters.FontParams;

/**
 *
 * @author Krzysztof S. Nowinski
 * Warsaw University, ICM
 */
public class Axes3DParams extends Parameters
{

    public static final int MIN = 0;
    public static final int ZERO = 1;
    public static final int CENTER = 2;
    public static final int MAX = 3;
    protected static final String DESCS = "axDescs";
    protected static final String SHOW_UNITS = "showUnits";
    protected static final String FORMATS = "axFormats";
    protected static final String GRID = "gridLines";
    protected static final String POSITION = "axPos";
    protected static final String BOX = "box";
    protected static final String AXES = "axes";
    protected static final String LINE_WIDTH = "lineWidth";
    protected static final String X_DENSITY = "xLabelDensity";
    protected static final String Y_DENSITY = "yLabelDensity";
    protected static final String Z_DENSITY = "zLabelDensity";
    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg(DESCS, ParameterType.independent, null),
        new ParameterEgg(SHOW_UNITS, ParameterType.independent, true),
        new ParameterEgg(FORMATS, ParameterType.independent, null),
        new ParameterEgg(GRID, ParameterType.independent, null),
        new ParameterEgg(POSITION, ParameterType.independent, null),
        new ParameterEgg(BOX, ParameterType.independent, true),
        new ParameterEgg(AXES, ParameterType.independent, true),
        new ParameterEgg(LINE_WIDTH, ParameterType.independent, 2),
        new ParameterEgg(X_DENSITY, ParameterType.independent, 6),
        new ParameterEgg(Y_DENSITY, ParameterType.independent, 6),
        new ParameterEgg(Z_DENSITY, ParameterType.independent, 6)
    };
    protected FontParams fontParams = new FontParams();

    public Axes3DParams()
    {
        super(eggs);
        setValue(DESCS, new String[]{"X", "Y", "Z"});
        setValue(FORMATS, new String[]{"%4.1f", "%4.1f", "%4.1f"});
        setValue(GRID, new boolean[]{true, true, true});
        setValue(POSITION, new int[][]{{MIN, MIN}, {MIN, MIN}, {MIN, MIN}});
        fontParams.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                fireStateChanged();
            }
        });
    }

    public String[] getAxDescs()
    {
        return (String[]) getValue(DESCS);
    }

    public String[] getAxFormats()
    {
        return (String[]) getValue(FORMATS);
    }

    public boolean[] getGridLines()
    {
        return (boolean[]) getValue(GRID);
    }

    public boolean isShowUnits()
    {
        return (boolean) getValue(SHOW_UNITS);
    }

    public void setXLabel(String s)
    {
        ((String[]) getValue(DESCS))[0] = s;
        fireStateChanged();
    }

    public void setYLabel(String s)
    {
        ((String[]) getValue(DESCS))[1] = s;
        fireStateChanged();
    }

    public void setZLabel(String s)
    {
        ((String[]) getValue(DESCS))[2] = s;
        fireStateChanged();
    }

    public void setXFormat(String s)
    {
        ((String[]) getValue(FORMATS))[0] = s;
        fireStateChanged();
    }

    public void setYFormat(String s)
    {
        ((String[]) getValue(FORMATS))[1] = s;
        fireStateChanged();
    }

    public void setZFormat(String s)
    {
        ((String[]) getValue(FORMATS))[2] = s;
        fireStateChanged();
    }

    public void setXGridLines(boolean show)
    {
        ((boolean[]) getValue(GRID))[0] = show;
        fireStateChanged();
    }

    public void setYGridLines(boolean show)
    {
        ((boolean[]) getValue(GRID))[1] = show;
        fireStateChanged();
    }

    public void setZGridLines(boolean show)
    {
        ((boolean[]) getValue(GRID))[2] = show;
        fireStateChanged();
    }

    public void setShowUnits(boolean show)
    {
        setValue(SHOW_UNITS, show);
        fireStateChanged();
    }

    public FontParams getFontParams()
    {
        return fontParams;
    }

    public int[][] getAxPos()
    {
        return (int[][]) getValue(POSITION);
    }

    public boolean isBox()
    {
        return (Boolean) getValue(BOX);
    }

    public void setBox(boolean box)
    {
        setValue(BOX, box);
        fireStateChanged();
    }

    public boolean isAxes()
    {
        return (Boolean) getValue(AXES);
    }

    public void setAxes(boolean axes)
    {
        setValue(AXES, axes);
        fireStateChanged();
    }

    public int getLineWidth()
    {
        return (Integer) getValue(LINE_WIDTH);
    }

    public void setLineWidth(int lineWidth)
    {
        setValue(LINE_WIDTH, lineWidth);
        fireStateChanged();
    }
    public int getXLabelDensity()
    {
        return (Integer) getValue(X_DENSITY);
    }

    public void setXLabelDensity(int labelDensity)
    {
        setValue(X_DENSITY, labelDensity);
        fireStateChanged();
    }

    public int getYLabelDensity()
    {
        return (Integer) getValue(Y_DENSITY);
    }

    public void setYLabelDensity(int labelDensity)
    {
        setValue(Y_DENSITY, labelDensity);
        fireStateChanged();
    }

    public int getZLabelDensity()
    {
        return (Integer) getValue(Z_DENSITY);
    }

    public void setZLabelDensity(int labelDensity)
    {
        setValue(Z_DENSITY, labelDensity);
        fireStateChanged();
    }
}
