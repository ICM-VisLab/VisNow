/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.utilities.Annotations;

import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.parameters.FontParams;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class Params extends Parameters
{
    protected FontParams fontParams = new FontParams();
    protected static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Boolean>("activated", ParameterType.dependent, true),
        new ParameterEgg<Boolean>("output", ParameterType.dependent, false)
    };

    public Params()
    {
        super(eggs);
        fontParams.addChangeListener(new ChangeListener()
        {

            @Override
            public void stateChanged(ChangeEvent e)
            {
                fireStateChanged();
            }
        });
    }

    public boolean isActivated()
    {
        return (Boolean)getValue("activated");
    }

    public void setActivated(boolean activated)
    {
        setValue("activated", activated);
    }


    public boolean isOutput()
    {
        return (Boolean)getValue("output");
    }

    public void output()
    {
        setValue("output", true);
        fireStateChanged();
    }


    public void clearOutput()
    {
        setValue("output", false);
    }

    @Override
    public void fireStateChanged()
    {
        if (!active)
            return;
        ChangeEvent e = new ChangeEvent(this);
        for (int i = 0; i < changeListenerList.size(); i++) {
            changeListenerList.get(i).stateChanged(e);
        }
    }

}
