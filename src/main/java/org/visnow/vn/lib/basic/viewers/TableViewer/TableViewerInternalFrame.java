//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.TableViewer;

import java.awt.Component;
import org.visnow.vn.lib.basic.viewers.TableViewer.ExtendedTable.ExtendedTablePanel;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.plaf.IconUIResource;
import org.visnow.vn.gui.utils.NodeIcon;

/**
 * The main frame of TableViewer.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 * @author norkap
 */
public class TableViewerInternalFrame extends JInternalFrame
{

    private final JMenuBar menuBar;
    private final JMenuItem addTableMenuItem;
    private final JCheckBoxMenuItem oneDTabMenuItem;
    private final JButton addTableButton;
    private final JScrollPane scrollPane;
    private final JPanel mainPanel;
    private final boolean oneDTab;

    public TableViewerInternalFrame(JFrame parent, boolean oneDTab)
    {
        setLayout(new GridBagLayout());
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setMinimumSize(new Dimension(810, 200));
        setPreferredSize(new Dimension(810, 400));
        ((javax.swing.plaf.basic.BasicInternalFrameUI) getUI()).setNorthPane(null); // remove titlebar
        setBorder(null);
        
        this.oneDTab = oneDTab;
        menuBar = new JMenuBar();
        JMenu menu = new JMenu("Options");

        addTableMenuItem = new JMenuItem("Add Table");
        
        oneDTabMenuItem = new JCheckBoxMenuItem("1D Tab", oneDTab);

        JMenuItem menuItem3 = new JMenuItem("Exit");
        menuItem3.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (parent != null) {
                    parent.dispose();
                } else {
                    dispose();
                }
            }
        });

        menu.add(addTableMenuItem);
        menu.add(oneDTabMenuItem);
        menu.addSeparator();
        menu.add(menuItem3);
        menuBar.add(menu);
        setJMenuBar(menuBar);

        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        scrollPane = new JScrollPane(mainPanel);
        scrollPane.setPreferredSize(new Dimension(1000, 400));

        GridBagConstraints gridBC = new GridBagConstraints();
        gridBC.weightx = 1;
        gridBC.weighty = 1;
        gridBC.fill = GridBagConstraints.BOTH;
        add(scrollPane, gridBC);

        addTableButton = new JButton("Add Table");
        Icon icon = new IconUIResource(new NodeIcon('+'));
        addTableButton.setIcon(icon);

        gridBC = new GridBagConstraints();
        gridBC.gridy = 2;
        gridBC.weightx = 1;
        gridBC.weighty = 0;
        gridBC.fill = GridBagConstraints.HORIZONTAL;
        add(addTableButton, gridBC);
    }

    public boolean getOneDTab()
    {
        return oneDTab;
    }
    
    public int getComponentIndex(Component comp)
    {
        for (int i = 0; i < mainPanel.getComponentCount(); i++) {
            if (comp == mainPanel.getComponent(i))
                return i;
        }
        return -1;
    }

    public Component getComponent(String name)
    {
        for (int i = 0; i < getContentPane().getComponentCount(); i++) {
            if (name.equals(getContentPane().getComponent(i).getName()))
                return getContentPane().getComponent(i);
        }
        return null;
    }

    public void addTableToDisplay(ExtendedTablePanel table)
    {
        GridBagConstraints gridBC = new GridBagConstraints();
        gridBC.weightx = 1;
        gridBC.weighty = 1;
        gridBC.fill = GridBagConstraints.BOTH;
        gridBC.gridx = 0;
        mainPanel.add(table, gridBC);
        Dimension dims = getMinimumSize();
        mainPanel.setMinimumSize(new Dimension(800, dims.height + 200));

        validate();
    }

    public void removeTableFromDisplay(ExtendedTablePanel table)
    {
        mainPanel.remove(table);
        Dimension minDims = getMinimumSize();
        mainPanel.setMinimumSize(new Dimension(800, minDims.height - 200));
        validate();
        repaint();
    }

    public void addTableActionListener(ActionListener addTableAL)
    {
        addTableMenuItem.addActionListener(addTableAL);
        addTableButton.addActionListener(addTableAL);
    }
    
    public void addOneDTabActionListener(ActionListener oneDTabAL)
    {
        oneDTabMenuItem.addActionListener(oneDTabAL);
    }

}
