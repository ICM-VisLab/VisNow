/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices;

/**
 * A device class that is encouraged to be used as a base class for all concrete devices (to be more
 * precise: use {@link BasicRegisteredDevice} as a base class).
 * <p/>
 * It handles storing internal device name (used by an underlying device library) and device
 * friendly name which will be shown to the user.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public abstract class BasicDevice implements IPassiveDevice
{

    /**
     * Internal device name to be used by underlying device libraries for identifying the device.
     */
    protected String deviceName;
    /**
     * A friendly name to be displayed to the user.
     */
    protected String deviceFriendlyName;

    BasicDevice(String deviceName_, String deviceFriendlyName_)
    {
        this.deviceName = deviceName_;
        this.deviceFriendlyName = deviceFriendlyName_;
    }

    @Override
    public String getDeviceName()
    {
        return deviceName;
    }

    @Override
    public String getDeviceFriendlyName()
    {
        return deviceFriendlyName;
    }

}
