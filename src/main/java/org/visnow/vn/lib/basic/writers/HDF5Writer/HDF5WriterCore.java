/* VisNow
   Copyright (C) 2006-2019 University of Warsaw, ICM
   Copyright (C) 2020 onward visnow.org
   All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.writers.HDF5Writer;

import ch.systemsx.cisd.base.mdarray.MDArray;
import ch.systemsx.cisd.base.mdarray.MDByteArray;
import ch.systemsx.cisd.base.mdarray.MDDoubleArray;
import ch.systemsx.cisd.base.mdarray.MDFloatArray;
import ch.systemsx.cisd.base.mdarray.MDIntArray;
import ch.systemsx.cisd.base.mdarray.MDLongArray;
import ch.systemsx.cisd.base.mdarray.MDShortArray;
import static ch.systemsx.cisd.hdf5.HDF5ArrayBlockParamsBuilder.blockOffset;
import ch.systemsx.cisd.hdf5.HDF5CompoundType;
import ch.systemsx.cisd.hdf5.HDF5DataSet;
import ch.systemsx.cisd.hdf5.HDF5Factory;
import ch.systemsx.cisd.hdf5.IHDF5ByteWriter;
import ch.systemsx.cisd.hdf5.IHDF5CompoundWriter;
import ch.systemsx.cisd.hdf5.IHDF5DoubleWriter;
import ch.systemsx.cisd.hdf5.IHDF5FloatWriter;
import ch.systemsx.cisd.hdf5.IHDF5IntWriter;
import ch.systemsx.cisd.hdf5.IHDF5LongWriter;
import ch.systemsx.cisd.hdf5.IHDF5ShortWriter;
import ch.systemsx.cisd.hdf5.IHDF5StringWriter;
import ch.systemsx.cisd.hdf5.IHDF5Writer;
import ch.systemsx.cisd.hdf5.IHDF5WriterConfigurator;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.vn.lib.basic.readers.ReadCDM4.ReadHDF5Core;
import static ch.systemsx.cisd.hdf5.MatrixUtils.dims;
import javax.swing.JOptionPane;

/**
 * Static methods for writing HDF5 files.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class HDF5WriterCore
{

    public static final int MINIMAL_CHUNK_SIZE = 1000;

    /**
     * Writes VisNow RegularField field to an HDF5 file.
     *
     * @param field  VisNow field
     * @param path   output file path
     * @param append if true, then the new data will be added to existing file
     *
     * @return true, if the operation was successful, false otherwise
     *
     */
    public static boolean writeField(RegularField field, String path, boolean append)
    {
        File file = new File(path);
        String fieldName = field.getName();
        ArrayList<DataArray> components = field.getComponents();
        long[] dims = ReadHDF5Core.vectorReverse(field.getLDims(), false);

        try {
            IHDF5WriterConfigurator conf = HDF5Factory.configure(file);
            if (!append) {
                conf.overwrite();
            }
            conf.fileFormat(IHDF5WriterConfigurator.FileFormatVersionBounds.V1_8_LATEST);
            try (IHDF5Writer writer = conf.writer()) {
                if (append && writer.object().isGroup("visnow/" + fieldName)) {
                    while (writer.object().isGroup("visnow/" + fieldName)) {
                        fieldName = (String) JOptionPane.showInputDialog(null, "Field " + fieldName + " already exists. Enter new name.", "Field exists", JOptionPane.INFORMATION_MESSAGE, null, null, fieldName);
                    }
                }
                writer.object().createGroup("visnow/" + fieldName + "/vnfield_geometry");
                for (DataArray component : components) {
                    if (component.getType() != DataArrayType.FIELD_DATA_OBJECT && component.getType() != DataArrayType.FIELD_DATA_UNKNOWN) {
                        if (!writeDataArray(writer, fieldName, component, dims)) {
                            writer.close();
                            return false;
                        }
                    }
                }
                writer.int64().setArrayAttr("visnow/" + fieldName, "dimensions", dims);

                if (!field.isTimeUnitless()) {
                    writer.string().setAttr("visnow/" + fieldName, "time_unit", field.getTimeUnit());
                }

                if (field.getCoordsUnits() != null && !Arrays.deepEquals(field.getCoordsUnits(), new String[]{"1", "1", "1"})) {
                    writer.string().setArrayAttr("visnow/" + fieldName + "/vnfield_geometry", "coords_units", field.getCoordsUnits());
                }

                if (field.hasCoords()) {
                    if (!writeCoords(writer, fieldName, field.getCoords(), dims)) {
                        writer.close();
                        return false;
                    }
                } else {
                    if (!writeFloatMatrix(writer, "visnow/" + fieldName + "/vnfield_geometry/affine", field.getAffine())) {
                        writer.close();
                        return false;
                    }
                }
                if (!writeFloatMatrix(writer, "visnow/" + fieldName + "/vnfield_geometry/extents", field.getExtents())) {
                    writer.close();
                    return false;
                }
                if (!Arrays.deepEquals(field.getExtents(), field.getPreferredExtents())) {
                    if (!writeFloatMatrix(writer, "visnow/" + fieldName + "/vnfield_geometry/preferred_extents", field.getPreferredExtents())) {
                        writer.close();
                        return false;
                    }
                }
                if (!Arrays.deepEquals(field.getPreferredExtents(), field.getPreferredPhysicalExtents())) {
                    if (!writeFloatMatrix(writer, "visnow/" + fieldName + "/vnfield_geometry/preferred_physical_extents", field.getPreferredPhysicalExtents())) {
                        writer.close();
                        return false;
                    }
                }

                if (field.hasMask()) {
                    if (!writeMask(writer, fieldName, field.getMask(), dims)) {
                        writer.close();
                        return false;
                    }
                }

                writer.close();
            }
        } catch (Exception ex) {
            return false;
        }

        return true;
    }

    /**
     * Returns list of chunk sizes for given dimensions.
     *
     * @param dims dimensions
     *
     * @return list of chunk sizes
     */
    private static int[][] calculateChunkSize(long[] dims)
    {
        int[][] chunkSize = new int[dims.length][];
        for (int d = 0; d < dims.length; d++) {
            if (dims[d] > MINIMAL_CHUNK_SIZE) {
                int blocks = (int) Math.ceil((double) dims[d] / (double) MINIMAL_CHUNK_SIZE);
                chunkSize[d] = new int[blocks];
                for (int i = 0; i < blocks; i++) {
                    if (i < blocks - 1) {
                        chunkSize[d][i] = MINIMAL_CHUNK_SIZE;
                    } else {
                        chunkSize[d][i] = (int) (dims[d] - i * MINIMAL_CHUNK_SIZE);
                    }
                }
            } else {
                chunkSize[d] = new int[1];
                chunkSize[d][0] = (int) dims[d];
            }
        }
        return chunkSize;
    }

    /**
     * Fills chunkData argument with data from LogicLargeArray or UnsignedByteLargeArray.
     *
     * @param data      input data
     * @param dims      dimensions
     * @param offset    offset
     * @param chunkData output data
     */
    private static void fillLogicOrByteChunk(final ArrayList<LargeArray> data, final long[] dims, final long[] offset, final MDByteArray chunkData)
    {
        int[] mddims = chunkData.dimensions();
        LargeArray la;
        switch (chunkData.rank()) {
            case 1:
                la = data.get(0);
                for (int i = 0; i < mddims[0]; i++) {
                    chunkData.set(la.getByte(offset[0] + i), i);
                }
                break;
            case 2:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            chunkData.set(la.getByte(offset[1] + j), i, j);
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            chunkData.set(la.getByte((offset[0] + i) * dims[1] + offset[1] + j), i, j);
                        }
                    }
                }
                break;
            case 3:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                chunkData.set(la.getByte((offset[1] + j) * dims[2] + offset[2] + k), i, j, k);
                            }
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                chunkData.set(la.getByte((offset[0] + i) * dims[1] * dims[2] + (offset[1] + j) * dims[2] + offset[2] + k), i, j, k);
                            }
                        }
                    }
                }
                break;
            case 4:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    chunkData.set(la.getByte((offset[1] + j) * dims[2] * dims[3] + (offset[2] + k) * dims[3] + offset[3] + l), i, j, k, l);
                                }
                            }
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    chunkData.set(la.getByte((offset[0] + i) * dims[1] * dims[2] * dims[3] + (offset[1] + j) * dims[2] * dims[3] +
                                        (offset[2] + k) * dims[3] + offset[3] + l), i, j, k, l);
                                }
                            }
                        }
                    }
                }
                break;
            case 5:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    for (int m = 0; m < mddims[4]; m++) {
                                        chunkData.set(la.getByte((offset[1] + j) * dims[2] * dims[3] * dims[4] + (offset[2] + k) * dims[3] * dims[4] +
                                            (offset[3] + l) * dims[4] + offset[4] + m), i, j, k, l, m);
                                    }
                                }
                            }
                        }
                    }

                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    for (int m = 0; m < mddims[4]; m++) {
                                        chunkData.set(la.getByte((offset[0] + i) * dims[1] * dims[2] * dims[3] * dims[4] + (offset[1] + j) * dims[2] * dims[3] * dims[4] +
                                            (offset[2] + k) * dims[3] * dims[4] + (offset[3] + l) * dims[4] + offset[4] + m), i, j, k, l, m);
                                    }
                                }
                            }
                        }
                    }
                }
                break;

            default:
                throw new IllegalArgumentException("Unsupported rank.");
        }
    }

    /**
     * Fills chunkData argument with data from ShortLargeArray.
     *
     * @param data      input data
     * @param dims      dimensions
     * @param offset    offset
     * @param chunkData output data
     */
    private static void fillShortChunk(final ArrayList<LargeArray> data, final long[] dims, final long[] offset, final MDShortArray chunkData)
    {
        int[] mddims = chunkData.dimensions();
        LargeArray la;
        switch (chunkData.rank()) {
            case 1:
                la = data.get(0);
                for (int i = 0; i < mddims[0]; i++) {
                    chunkData.set(la.getShort(offset[0] + i), i);
                }
                break;
            case 2:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            chunkData.set(la.getShort(offset[1] + j), i, j);
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            chunkData.set(la.getShort((offset[0] + i) * dims[1] + offset[1] + j), i, j);
                        }
                    }
                }
                break;
            case 3:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                chunkData.set(la.getShort((offset[1] + j) * dims[2] + offset[2] + k), i, j, k);
                            }
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                chunkData.set(la.getShort((offset[0] + i) * dims[1] * dims[2] + (offset[1] + j) * dims[2] + offset[2] + k), i, j, k);
                            }
                        }
                    }
                }
                break;
            case 4:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    chunkData.set(la.getShort((offset[1] + j) * dims[2] * dims[3] + (offset[2] + k) * dims[3] + offset[3] + l), i, j, k, l);
                                }
                            }
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    chunkData.set(la.getShort((offset[0] + i) * dims[1] * dims[2] * dims[3] + (offset[1] + j) * dims[2] * dims[3] +
                                        (offset[2] + k) * dims[3] + offset[3] + l), i, j, k, l);
                                }
                            }
                        }
                    }
                }
                break;
            case 5:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    for (int m = 0; m < mddims[4]; m++) {
                                        chunkData.set(la.getShort((offset[1] + j) * dims[2] * dims[3] * dims[4] + (offset[2] + k) * dims[3] * dims[4] +
                                            (offset[3] + l) * dims[4] + offset[4] + m), i, j, k, l, m);
                                    }
                                }
                            }
                        }
                    }

                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    for (int m = 0; m < mddims[4]; m++) {
                                        chunkData.set(la.getShort((offset[0] + i) * dims[1] * dims[2] * dims[3] * dims[4] + (offset[1] + j) * dims[2] * dims[3] * dims[4] +
                                            (offset[2] + k) * dims[3] * dims[4] + (offset[3] + l) * dims[4] + offset[4] + m), i, j, k, l, m);
                                    }
                                }
                            }
                        }
                    }
                }
                break;

            default:
                throw new IllegalArgumentException("Unsupported rank.");
        }
    }

    /**
     * Fills chunkData argument with data from IntLargeArray.
     *
     * @param data      input data
     * @param dims      dimensions
     * @param offset    offset
     * @param chunkData output data
     */
    private static void fillIntChunk(final ArrayList<LargeArray> data, final long[] dims, final long[] offset, final MDIntArray chunkData)
    {
        int[] mddims = chunkData.dimensions();
        LargeArray la;
        switch (chunkData.rank()) {
            case 1:
                la = data.get(0);
                for (int i = 0; i < mddims[0]; i++) {
                    chunkData.set(la.getInt(offset[0] + i), i);
                }
                break;
            case 2:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            chunkData.set(la.getInt(offset[1] + j), i, j);
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            chunkData.set(la.getInt((offset[0] + i) * dims[1] + offset[1] + j), i, j);
                        }
                    }
                }
                break;
            case 3:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                chunkData.set(la.getInt((offset[1] + j) * dims[2] + offset[2] + k), i, j, k);
                            }
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                chunkData.set(la.getInt((offset[0] + i) * dims[1] * dims[2] + (offset[1] + j) * dims[2] + offset[2] + k), i, j, k);
                            }
                        }
                    }
                }
                break;
            case 4:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    chunkData.set(la.getInt((offset[1] + j) * dims[2] * dims[3] + (offset[2] + k) * dims[3] + offset[3] + l), i, j, k, l);
                                }
                            }
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    chunkData.set(la.getInt((offset[0] + i) * dims[1] * dims[2] * dims[3] + (offset[1] + j) * dims[2] * dims[3] +
                                        (offset[2] + k) * dims[3] + offset[3] + l), i, j, k, l);
                                }
                            }
                        }
                    }
                }
                break;
            case 5:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    for (int m = 0; m < mddims[4]; m++) {
                                        chunkData.set(la.getInt((offset[1] + j) * dims[2] * dims[3] * dims[4] + (offset[2] + k) * dims[3] * dims[4] +
                                            (offset[3] + l) * dims[4] + offset[4] + m), i, j, k, l, m);
                                    }
                                }
                            }
                        }
                    }

                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    for (int m = 0; m < mddims[4]; m++) {
                                        chunkData.set(la.getInt((offset[0] + i) * dims[1] * dims[2] * dims[3] * dims[4] + (offset[1] + j) * dims[2] * dims[3] * dims[4] +
                                            (offset[2] + k) * dims[3] * dims[4] + (offset[3] + l) * dims[4] + offset[4] + m), i, j, k, l, m);
                                    }
                                }
                            }
                        }
                    }
                }
                break;

            default:
                throw new IllegalArgumentException("Unsupported rank.");
        }
    }

    /**
     * Fills chunkData argument with data from LongLargeArray.
     *
     * @param data      input data
     * @param dims      dimensions
     * @param offset    offset
     * @param chunkData output data
     */
    private static void fillLongChunk(final ArrayList<LargeArray> data, final long[] dims, final long[] offset, final MDLongArray chunkData)
    {
        int[] mddims = chunkData.dimensions();
        LargeArray la;
        switch (chunkData.rank()) {
            case 1:
                la = data.get(0);
                for (int i = 0; i < mddims[0]; i++) {
                    chunkData.set(la.getLong(offset[0] + i), i);
                }
                break;
            case 2:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            chunkData.set(la.getLong(offset[1] + j), i, j);
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            chunkData.set(la.getLong((offset[0] + i) * dims[1] + offset[1] + j), i, j);
                        }
                    }
                }
                break;
            case 3:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                chunkData.set(la.getLong((offset[1] + j) * dims[2] + offset[2] + k), i, j, k);
                            }
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                chunkData.set(la.getLong((offset[0] + i) * dims[1] * dims[2] + (offset[1] + j) * dims[2] + offset[2] + k), i, j, k);
                            }
                        }
                    }
                }
                break;
            case 4:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    chunkData.set(la.getLong((offset[1] + j) * dims[2] * dims[3] + (offset[2] + k) * dims[3] + offset[3] + l), i, j, k, l);
                                }
                            }
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    chunkData.set(la.getLong((offset[0] + i) * dims[1] * dims[2] * dims[3] + (offset[1] + j) * dims[2] * dims[3] +
                                        (offset[2] + k) * dims[3] + offset[3] + l), i, j, k, l);
                                }
                            }
                        }
                    }
                }
                break;
            case 5:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    for (int m = 0; m < mddims[4]; m++) {
                                        chunkData.set(la.getLong((offset[1] + j) * dims[2] * dims[3] * dims[4] + (offset[2] + k) * dims[3] * dims[4] +
                                            (offset[3] + l) * dims[4] + offset[4] + m), i, j, k, l, m);
                                    }
                                }
                            }
                        }
                    }

                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    for (int m = 0; m < mddims[4]; m++) {
                                        chunkData.set(la.getLong((offset[0] + i) * dims[1] * dims[2] * dims[3] * dims[4] + (offset[1] + j) * dims[2] * dims[3] * dims[4] +
                                            (offset[2] + k) * dims[3] * dims[4] + (offset[3] + l) * dims[4] + offset[4] + m), i, j, k, l, m);
                                    }
                                }
                            }
                        }
                    }
                }
                break;

            default:
                throw new IllegalArgumentException("Unsupported rank.");
        }
    }

    /**
     * Fills chunkData argument with data from FloatLargeArray.
     *
     * @param data      input data
     * @param dims      dimensions
     * @param offset    offset
     * @param chunkData output data
     */
    private static void fillFloatChunk(final ArrayList<LargeArray> data, final long[] dims, final long[] offset, final MDFloatArray chunkData)
    {
        int[] mddims = chunkData.dimensions();
        LargeArray la;
        switch (chunkData.rank()) {
            case 1:
                la = data.get(0);
                for (int i = 0; i < mddims[0]; i++) {
                    chunkData.set(la.getFloat(offset[0] + i), i);
                }
                break;
            case 2:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            chunkData.set(la.getFloat(offset[1] + j), i, j);
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            chunkData.set(la.getFloat((offset[0] + i) * dims[1] + offset[1] + j), i, j);
                        }
                    }
                }
                break;
            case 3:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                chunkData.set(la.getFloat((offset[1] + j) * dims[2] + offset[2] + k), i, j, k);
                            }
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                chunkData.set(la.getFloat((offset[0] + i) * dims[1] * dims[2] + (offset[1] + j) * dims[2] + offset[2] + k), i, j, k);
                            }
                        }
                    }
                }
                break;
            case 4:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    chunkData.set(la.getFloat((offset[1] + j) * dims[2] * dims[3] + (offset[2] + k) * dims[3] + offset[3] + l), i, j, k, l);
                                }
                            }
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    chunkData.set(la.getFloat((offset[0] + i) * dims[1] * dims[2] * dims[3] + (offset[1] + j) * dims[2] * dims[3] +
                                        (offset[2] + k) * dims[3] + offset[3] + l), i, j, k, l);
                                }
                            }
                        }
                    }
                }
                break;
            case 5:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    for (int m = 0; m < mddims[4]; m++) {
                                        chunkData.set(la.getFloat((offset[1] + j) * dims[2] * dims[3] * dims[4] + (offset[2] + k) * dims[3] * dims[4] +
                                            (offset[3] + l) * dims[4] + offset[4] + m), i, j, k, l, m);
                                    }
                                }
                            }
                        }
                    }

                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    for (int m = 0; m < mddims[4]; m++) {
                                        chunkData.set(la.getFloat((offset[0] + i) * dims[1] * dims[2] * dims[3] * dims[4] + (offset[1] + j) * dims[2] * dims[3] * dims[4] +
                                            (offset[2] + k) * dims[3] * dims[4] + (offset[3] + l) * dims[4] + offset[4] + m), i, j, k, l, m);
                                    }
                                }
                            }
                        }
                    }
                }
                break;

            default:
                throw new IllegalArgumentException("Unsupported rank.");
        }
    }

    /**
     * Fills chunkData argument with data from DoubleLargeArray.
     *
     * @param data      input data
     * @param dims      dimensions
     * @param offset    offset
     * @param chunkData output data
     */
    private static void fillDoubleChunk(final ArrayList<LargeArray> data, final long[] dims, final long[] offset, final MDDoubleArray chunkData)
    {
        int[] mddims = chunkData.dimensions();
        LargeArray la;
        switch (chunkData.rank()) {
            case 1:
                la = data.get(0);
                for (int i = 0; i < mddims[0]; i++) {
                    chunkData.set(la.getDouble(offset[0] + i), i);
                }
                break;
            case 2:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            chunkData.set(la.getDouble(offset[1] + j), i, j);
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            chunkData.set(la.getDouble((offset[0] + i) * dims[1] + offset[1] + j), i, j);
                        }
                    }
                }
                break;
            case 3:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                chunkData.set(la.getDouble((offset[1] + j) * dims[2] + offset[2] + k), i, j, k);
                            }
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                chunkData.set(la.getDouble((offset[0] + i) * dims[1] * dims[2] + (offset[1] + j) * dims[2] + offset[2] + k), i, j, k);
                            }
                        }
                    }
                }
                break;
            case 4:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    chunkData.set(la.getDouble((offset[1] + j) * dims[2] * dims[3] + (offset[2] + k) * dims[3] + offset[3] + l), i, j, k, l);
                                }
                            }
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    chunkData.set(la.getDouble((offset[0] + i) * dims[1] * dims[2] * dims[3] + (offset[1] + j) * dims[2] * dims[3] +
                                        (offset[2] + k) * dims[3] + offset[3] + l), i, j, k, l);
                                }
                            }
                        }
                    }
                }
                break;
            case 5:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    for (int m = 0; m < mddims[4]; m++) {
                                        chunkData.set(la.getDouble((offset[1] + j) * dims[2] * dims[3] * dims[4] + (offset[2] + k) * dims[3] * dims[4] +
                                            (offset[3] + l) * dims[4] + offset[4] + m), i, j, k, l, m);
                                    }
                                }
                            }
                        }
                    }

                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    for (int m = 0; m < mddims[4]; m++) {
                                        chunkData.set(la.getDouble((offset[0] + i) * dims[1] * dims[2] * dims[3] * dims[4] + (offset[1] + j) * dims[2] * dims[3] * dims[4] +
                                            (offset[2] + k) * dims[3] * dims[4] + (offset[3] + l) * dims[4] + offset[4] + m), i, j, k, l, m);
                                    }
                                }
                            }
                        }
                    }
                }
                break;

            default:
                throw new IllegalArgumentException("Unsupported rank.");
        }
    }

    /**
     * Fills chunkData argument with data from ComplexFloatLargeArray.
     *
     * @param data      input data
     * @param dims      dimensions
     * @param offset    offset
     * @param chunkData output data
     */
    private static void fillComplexChunk(final ArrayList<LargeArray> data, final long[] dims, final long[] offset, final MDArray<ComplexNumber> chunkData)
    {
        int[] mddims = chunkData.dimensions();
        ComplexFloatLargeArray la;
        FloatLargeArray re;
        FloatLargeArray im;
        switch (chunkData.rank()) {
            case 1:
                la = (ComplexFloatLargeArray) data.get(0);
                re = la.getRealArray();
                im = la.getImaginaryArray();
                for (int i = 0; i < mddims[0]; i++) {
                    chunkData.set(new ComplexNumber(re.get(offset[0] + i), im.get(offset[0] + i)), i);
                }
                break;
            case 2:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = (ComplexFloatLargeArray) data.get((int) offset[0] + i);
                        re = la.getRealArray();
                        im = la.getImaginaryArray();
                        for (int j = 0; j < mddims[1]; j++) {
                            chunkData.set(new ComplexNumber(re.getFloat(offset[1] + j), im.getFloat(offset[1] + j)), i, j);
                        }
                    }
                } else {
                    la = (ComplexFloatLargeArray) data.get(0);
                    re = la.getRealArray();
                    im = la.getImaginaryArray();
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            chunkData.set(new ComplexNumber(re.getFloat((offset[0] + i) * dims[1] + offset[1] + j),
                                                            im.getFloat((offset[0] + i) * dims[1] + offset[1] + j)), i, j);
                        }
                    }
                }
                break;
            case 3:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = (ComplexFloatLargeArray) data.get((int) offset[0] + i);
                        re = la.getRealArray();
                        im = la.getImaginaryArray();
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                chunkData.set(new ComplexNumber(re.getFloat((offset[1] + j) * dims[2] + offset[2] + k),
                                                                im.getFloat((offset[1] + j) * dims[2] + offset[2] + k)), i, j, k);
                            }
                        }
                    }
                } else {
                    la = (ComplexFloatLargeArray) data.get(0);
                    re = la.getRealArray();
                    im = la.getImaginaryArray();
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                chunkData.set(new ComplexNumber(re.getFloat((offset[0] + i) * dims[1] * dims[2] + (offset[1] + j) * dims[2] + offset[2] + k),
                                                                im.getFloat((offset[0] + i) * dims[1] * dims[2] + (offset[1] + j) * dims[2] + offset[2] + k)), i, j, k);
                            }
                        }
                    }
                }
                break;
            case 4:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = (ComplexFloatLargeArray) data.get((int) offset[0] + i);
                        re = la.getRealArray();
                        im = la.getImaginaryArray();
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    chunkData.set(new ComplexNumber(re.getFloat((offset[1] + j) * dims[2] * dims[3] + (offset[2] + k) * dims[3] + offset[3] + l),
                                                                    im.getFloat((offset[1] + j) * dims[2] * dims[3] + (offset[2] + k) * dims[3] + offset[3] + l)), i, j, k, l);
                                }
                            }
                        }
                    }
                } else {
                    la = (ComplexFloatLargeArray) data.get(0);
                    re = la.getRealArray();
                    im = la.getImaginaryArray();
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    chunkData.set(new ComplexNumber(re.getFloat((offset[0] + i) * dims[1] * dims[2] * dims[3] + (offset[1] + j) * dims[2] * dims[3] +
                                        (offset[2] + k) * dims[3] + offset[3] + l), im.getFloat((offset[0] + i) * dims[1] * dims[2] * dims[3] +
                                                                        (offset[1] + j) * dims[2] * dims[3] + (offset[2] + k) * dims[3] + offset[3] + l)), i, j, k, l);
                                }
                            }
                        }
                    }
                }
                break;
            case 5:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = (ComplexFloatLargeArray) data.get((int) offset[0] + i);
                        re = la.getRealArray();
                        im = la.getImaginaryArray();
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    for (int m = 0; m < mddims[4]; m++) {
                                        chunkData.set(new ComplexNumber(re.getFloat((offset[1] + j) * dims[2] * dims[3] * dims[4] + (offset[2] + k) * dims[3] * dims[4] +
                                            (offset[3] + l) * dims[4] + offset[4] + m), im.getFloat((offset[1] + j) * dims[2] * dims[3] * dims[4] +
                                                                            (offset[2] + k) * dims[3] * dims[4] + (offset[3] + l) * dims[4] + offset[4] + m)), i, j, k, l, m);
                                    }
                                }
                            }
                        }
                    }

                } else {
                    la = (ComplexFloatLargeArray) data.get(0);
                    re = la.getRealArray();
                    im = la.getImaginaryArray();
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    for (int m = 0; m < mddims[4]; m++) {
                                        chunkData.set(new ComplexNumber(re.getFloat((offset[0] + i) * dims[1] * dims[2] * dims[3] * dims[4] + (offset[1] + j) * dims[2] * dims[3] * dims[4] +
                                            (offset[2] + k) * dims[3] * dims[4] + (offset[3] + l) * dims[4] + offset[4] + m), im.getFloat((offset[0] + i) * dims[1] * dims[2] * dims[3] * dims[4] +
                                                                            (offset[1] + j) * dims[2] * dims[3] * dims[4] + (offset[2] + k) * dims[3] * dims[4] + (offset[3] + l) * dims[4] + offset[4] + m)), i, j, k, l, m);
                                    }
                                }
                            }
                        }
                    }
                }
                break;

            default:
                throw new IllegalArgumentException("Unsupported rank.");
        }
    }

    /**
     * Fills chunkData argument with data from StringLargeArray.
     *
     * @param data      input data
     * @param dims      dimensions
     * @param offset    offset
     * @param chunkData output data
     */
    private static void fillStringChunk(final ArrayList<LargeArray> data, final long[] dims, final long[] offset, final MDArray<String> chunkData)
    {
        int[] mddims = chunkData.dimensions();
        LargeArray la;
        switch (chunkData.rank()) {
            case 1:
                la = data.get(0);
                for (int i = 0; i < mddims[0]; i++) {
                    chunkData.set((String) la.get(offset[0] + i), i);
                }
                break;
            case 2:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            chunkData.set((String) la.get(offset[1] + j), i, j);
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            chunkData.set((String) la.get((offset[0] + i) * dims[1] + offset[1] + j), i, j);
                        }
                    }
                }
                break;
            case 3:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                chunkData.set((String) la.get((offset[1] + j) * dims[2] + offset[2] + k), i, j, k);
                            }
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                chunkData.set((String) la.get((offset[0] + i) * dims[1] * dims[2] + (offset[1] + j) * dims[2] + offset[2] + k), i, j, k);
                            }
                        }
                    }
                }
                break;
            case 4:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    chunkData.set((String) la.get((offset[1] + j) * dims[2] * dims[3] + (offset[2] + k) * dims[3] + offset[3] + l), i, j, k, l);
                                }
                            }
                        }
                    }
                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    chunkData.set((String) la.get((offset[0] + i) * dims[1] * dims[2] * dims[3] + (offset[1] + j) * dims[2] * dims[3] +
                                        (offset[2] + k) * dims[3] + offset[3] + l), i, j, k, l);
                                }
                            }
                        }
                    }
                }
                break;
            case 5:
                if (data.size() > 1) {
                    for (int i = 0; i < mddims[0]; i++) {
                        la = data.get((int) offset[0] + i);
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    for (int m = 0; m < mddims[4]; m++) {
                                        chunkData.set((String) la.get((offset[1] + j) * dims[2] * dims[3] * dims[4] + (offset[2] + k) * dims[3] * dims[4] +
                                            (offset[3] + l) * dims[4] + offset[4] + m), i, j, k, l, m);
                                    }
                                }
                            }
                        }
                    }

                } else {
                    la = data.get(0);
                    for (int i = 0; i < mddims[0]; i++) {
                        for (int j = 0; j < mddims[1]; j++) {
                            for (int k = 0; k < mddims[2]; k++) {
                                for (int l = 0; l < mddims[3]; l++) {
                                    for (int m = 0; m < mddims[4]; m++) {
                                        chunkData.set((String) la.get((offset[0] + i) * dims[1] * dims[2] * dims[3] * dims[4] + (offset[1] + j) * dims[2] * dims[3] * dims[4] +
                                            (offset[2] + k) * dims[3] * dims[4] + (offset[3] + l) * dims[4] + offset[4] + m), i, j, k, l, m);
                                    }
                                }
                            }
                        }
                    }
                }
                break;

            default:
                throw new IllegalArgumentException("Unsupported rank.");
        }
    }

    /**
     * Writes a DataArray of type logic or byte to HDF5 file.
     *
     * @param writer    HDF5 writer
     * @param fieldName VisNow field name
     * @param da        DataArray
     * @param fieldDims dimensions of VisNow field
     *
     * @return true if the operation was successful, false otherwise
     */
    private static boolean writeLogicOrByteDataArray(IHDF5Writer writer, String fieldName, DataArray da, long[] fieldDims)
    {
        if (writer == null || da == null || fieldDims == null) {
            return false;
        }
        if (!(da.getType() == DataArrayType.FIELD_DATA_LOGIC || da.getType() == DataArrayType.FIELD_DATA_BYTE)) {
            return false;
        }
        TimeData td = da.getTimeData();
        ArrayList<LargeArray> data = td.getValues();
        int veclen = da.getVectorLength();
        long[] offset = new long[(data.size() > 1 ? 1 : 0) + fieldDims.length + (veclen > 1 ? 1 : 0)];
        long[] dims;
        if (offset.length == fieldDims.length) {
            dims = fieldDims;

        } else {
            int off = 0;
            dims = new long[offset.length];
            if (data.size() > 1) {
                dims[0] = data.size();
                off = 1;
            }
            System.arraycopy(fieldDims, 0, dims, off, fieldDims.length);
            if (veclen > 1) {
                dims[dims.length - 1] = veclen;
            }
        }

        IHDF5ByteWriter byteWriter = da.getType() == DataArrayType.FIELD_DATA_BYTE ? writer.uint8() : writer.int8();

        switch (offset.length) {
            case 1: {
                int[][] chunkSize1D = calculateChunkSize(dims);
                int[] lengths = chunkSize1D[0];
                HDF5DataSet dataSet1D = byteWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(lengths[0]));
                for (int bx = 0; bx < chunkSize1D.length; bx++) {
                    MDByteArray chunkData = new MDByteArray(dims(lengths[bx]));
                    fillLogicOrByteChunk(data, dims, offset, chunkData);
                    offset[0] += lengths[bx];
                    byteWriter.writeMDArray(dataSet1D, chunkData, blockOffset(bx * lengths[0]));
                }
                break;
            }
            case 2: {
                int[][] chunkSize2D = calculateChunkSize(dims);
                int[] rows = chunkSize2D[0];
                int[] columns = chunkSize2D[1];
                HDF5DataSet dataSet2D = byteWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(rows[0], columns[0]));
                for (int r = 0; r < rows.length; r++) {
                    offset[1] = 0;
                    for (int c = 0; c < columns.length; c++) {
                        MDByteArray chunkData = new MDByteArray(dims(rows[r], columns[c]));
                        fillLogicOrByteChunk(data, dims, offset, chunkData);
                        offset[1] += columns[c];
                        byteWriter.writeMDArray(dataSet2D, chunkData, blockOffset(r * rows[0], c * columns[0]));
                    }
                    offset[0] += rows[r];
                }
                break;
            }
            case 3: {
                int[][] chunkSize3D = calculateChunkSize(dims);
                int[] slices = chunkSize3D[0];
                int[] rows = chunkSize3D[1];
                int[] columns = chunkSize3D[2];
                HDF5DataSet dataSet3D = byteWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(slices[0], rows[0], columns[0]));
                for (int s = 0; s < slices.length; s++) {
                    offset[1] = 0;
                    for (int r = 0; r < rows.length; r++) {
                        offset[2] = 0;
                        for (int c = 0; c < columns.length; c++) {
                            MDByteArray chunkData = new MDByteArray(dims(slices[s], rows[r], columns[c]));
                            fillLogicOrByteChunk(data, dims, offset, chunkData);
                            offset[2] += columns[c];
                            byteWriter.writeMDArray(dataSet3D, chunkData, blockOffset(s * slices[0], r * rows[0], c * columns[0]));
                        }
                        offset[1] += rows[r];
                    }
                    offset[0] += slices[s];
                }
                break;
            }
            case 4: {
                int[][] chunkSize4D = calculateChunkSize(dims);
                int[] dim0 = chunkSize4D[0];
                int[] dim1 = chunkSize4D[1];
                int[] dim2 = chunkSize4D[2];
                int[] dim3 = chunkSize4D[3];
                HDF5DataSet dataSet4D = byteWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                MDByteArray chunkData = new MDByteArray(dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3]));
                                fillLogicOrByteChunk(data, dims, offset, chunkData);
                                offset[3] += dim3[d3];
                                byteWriter.writeMDArray(dataSet4D, chunkData, blockOffset(d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0]));
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            case 5: {
                int[][] chunkSize5D = calculateChunkSize(dims);
                int[] dim0 = chunkSize5D[0];
                int[] dim1 = chunkSize5D[1];
                int[] dim2 = chunkSize5D[2];
                int[] dim3 = chunkSize5D[3];
                int[] dim4 = chunkSize5D[4];
                HDF5DataSet dataSet5D = byteWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0], dim4[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                offset[4] = 0;
                                for (int d4 = 0; d4 < dim4.length; d4++) {
                                    MDByteArray chunkData = new MDByteArray(dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3], dim4[d4]));
                                    fillLogicOrByteChunk(data, dims, offset, chunkData);
                                    offset[4] += dim4[d4];
                                    byteWriter.writeMDArray(dataSet5D, chunkData, blockOffset(d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0], d4 * dim4[0]));
                                }
                                offset[3] += dim3[d3];
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            default:
                return false;
        }
        return true;
    }

    /**
     * Writes a DataArray of type short to HDF5 file.
     *
     * @param writer    HDF5 writer
     * @param fieldName VisNow field name
     * @param da        DataArray
     * @param fieldDims dimensions of VisNow field
     *
     * @return true if the operation was successful, false otherwise
     */
    private static boolean writeShortDataArray(IHDF5Writer writer, String fieldName, DataArray da, long[] fieldDims)
    {
        if (writer == null || da == null || fieldDims == null) {
            return false;
        }
        if (da.getType() != DataArrayType.FIELD_DATA_SHORT) {
            return false;
        }
        TimeData td = da.getTimeData();
        ArrayList<LargeArray> data = td.getValues();
        int veclen = da.getVectorLength();
        long[] offset = new long[(data.size() > 1 ? 1 : 0) + fieldDims.length + (veclen > 1 ? 1 : 0)];
        long[] dims;
        if (offset.length == fieldDims.length) {
            dims = fieldDims;

        } else {
            int off = 0;
            dims = new long[offset.length];
            if (data.size() > 1) {
                dims[0] = data.size();
                off = 1;
            }
            System.arraycopy(fieldDims, 0, dims, off, fieldDims.length);
            if (veclen > 1) {
                dims[dims.length - 1] = veclen;
            }
        }

        IHDF5ShortWriter shortWriter = writer.int16();

        switch (offset.length) {
            case 1: {
                int[][] chunkSize1D = calculateChunkSize(dims);
                int[] lengths = chunkSize1D[0];
                HDF5DataSet dataSet1D = shortWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(lengths[0]));
                for (int bx = 0; bx < chunkSize1D.length; bx++) {
                    MDShortArray chunkData = new MDShortArray(dims(lengths[bx]));
                    fillShortChunk(data, dims, offset, chunkData);
                    offset[0] += lengths[bx];
                    shortWriter.writeMDArray(dataSet1D, chunkData, blockOffset(bx * lengths[0]));
                }
                break;
            }
            case 2: {
                int[][] chunkSize2D = calculateChunkSize(dims);
                int[] rows = chunkSize2D[0];
                int[] columns = chunkSize2D[1];
                HDF5DataSet dataSet2D = shortWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(rows[0], columns[0]));
                for (int r = 0; r < rows.length; r++) {
                    offset[1] = 0;
                    for (int c = 0; c < columns.length; c++) {
                        MDShortArray chunkData = new MDShortArray(dims(rows[r], columns[c]));
                        fillShortChunk(data, dims, offset, chunkData);
                        offset[1] += columns[c];
                        shortWriter.writeMDArray(dataSet2D, chunkData, blockOffset(r * rows[0], c * columns[0]));
                    }
                    offset[0] += rows[r];
                }
                break;
            }
            case 3: {
                int[][] chunkSize3D = calculateChunkSize(dims);
                int[] slices = chunkSize3D[0];
                int[] rows = chunkSize3D[1];
                int[] columns = chunkSize3D[2];
                HDF5DataSet dataSet3D = shortWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(slices[0], rows[0], columns[0]));
                for (int s = 0; s < slices.length; s++) {
                    offset[1] = 0;
                    for (int r = 0; r < rows.length; r++) {
                        offset[2] = 0;
                        for (int c = 0; c < columns.length; c++) {
                            MDShortArray chunkData = new MDShortArray(dims(slices[s], rows[r], columns[c]));
                            fillShortChunk(data, dims, offset, chunkData);
                            offset[2] += columns[c];
                            shortWriter.writeMDArray(dataSet3D, chunkData, blockOffset(s * slices[0], r * rows[0], c * columns[0]));
                        }
                        offset[1] += rows[r];
                    }
                    offset[0] += slices[s];
                }
                break;
            }
            case 4: {
                int[][] chunkSize4D = calculateChunkSize(dims);
                int[] dim0 = chunkSize4D[0];
                int[] dim1 = chunkSize4D[1];
                int[] dim2 = chunkSize4D[2];
                int[] dim3 = chunkSize4D[3];
                HDF5DataSet dataSet4D = shortWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                MDShortArray chunkData = new MDShortArray(dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3]));
                                fillShortChunk(data, dims, offset, chunkData);
                                offset[3] += dim3[d3];
                                shortWriter.writeMDArray(dataSet4D, chunkData, blockOffset(d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0]));
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            case 5: {
                int[][] chunkSize5D = calculateChunkSize(dims);
                int[] dim0 = chunkSize5D[0];
                int[] dim1 = chunkSize5D[1];
                int[] dim2 = chunkSize5D[2];
                int[] dim3 = chunkSize5D[3];
                int[] dim4 = chunkSize5D[4];
                HDF5DataSet dataSet5D = shortWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0], dim4[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                offset[4] = 0;
                                for (int d4 = 0; d4 < dim4.length; d4++) {
                                    MDShortArray chunkData = new MDShortArray(dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3], dim4[d4]));
                                    fillShortChunk(data, dims, offset, chunkData);
                                    offset[4] += dim4[d4];
                                    shortWriter.writeMDArray(dataSet5D, chunkData, blockOffset(d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0], d4 * dim4[0]));
                                }
                                offset[3] += dim3[d3];
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            default:
                return false;
        }
        return true;
    }

    /**
     * Writes a DataArray of type int to HDF5 file.
     *
     * @param writer    HDF5 writer
     * @param fieldName VisNow field name
     * @param da        DataArray
     * @param fieldDims dimensions of VisNow field
     *
     * @return true if the operation was successful, false otherwise
     */
    private static boolean writeIntDataArray(IHDF5Writer writer, String fieldName, DataArray da, long[] fieldDims)
    {
        if (writer == null || da == null || fieldDims == null) {
            return false;
        }
        if (da.getType() != DataArrayType.FIELD_DATA_INT) {
            return false;
        }
        TimeData td = da.getTimeData();
        ArrayList<LargeArray> data = td.getValues();
        int veclen = da.getVectorLength();
        long[] offset = new long[(data.size() > 1 ? 1 : 0) + fieldDims.length + (veclen > 1 ? 1 : 0)];
        long[] dims;
        if (offset.length == fieldDims.length) {
            dims = fieldDims;

        } else {
            int off = 0;
            dims = new long[offset.length];
            if (data.size() > 1) {
                dims[0] = data.size();
                off = 1;
            }
            System.arraycopy(fieldDims, 0, dims, off, fieldDims.length);
            if (veclen > 1) {
                dims[dims.length - 1] = veclen;
            }
        }

        IHDF5IntWriter intWriter = writer.int32();

        switch (offset.length) {
            case 1: {
                int[][] chunkSize1D = calculateChunkSize(dims);
                int[] lengths = chunkSize1D[0];
                HDF5DataSet dataSet1D = intWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(lengths[0]));
                for (int bx = 0; bx < chunkSize1D.length; bx++) {
                    MDIntArray chunkData = new MDIntArray(dims(lengths[bx]));
                    fillIntChunk(data, dims, offset, chunkData);
                    offset[0] += lengths[bx];
                    intWriter.writeMDArray(dataSet1D, chunkData, blockOffset(bx * lengths[0]));
                }
                break;
            }
            case 2: {
                int[][] chunkSize2D = calculateChunkSize(dims);
                int[] rows = chunkSize2D[0];
                int[] columns = chunkSize2D[1];
                HDF5DataSet dataSet2D = intWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(rows[0], columns[0]));
                for (int r = 0; r < rows.length; r++) {
                    offset[1] = 0;
                    for (int c = 0; c < columns.length; c++) {
                        MDIntArray chunkData = new MDIntArray(dims(rows[r], columns[c]));
                        fillIntChunk(data, dims, offset, chunkData);
                        offset[1] += columns[c];
                        intWriter.writeMDArray(dataSet2D, chunkData, blockOffset(r * rows[0], c * columns[0]));
                    }
                    offset[0] += rows[r];
                }
                break;
            }
            case 3: {
                int[][] chunkSize3D = calculateChunkSize(dims);
                int[] slices = chunkSize3D[0];
                int[] rows = chunkSize3D[1];
                int[] columns = chunkSize3D[2];
                HDF5DataSet dataSet3D = intWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(slices[0], rows[0], columns[0]));
                for (int s = 0; s < slices.length; s++) {
                    offset[1] = 0;
                    for (int r = 0; r < rows.length; r++) {
                        offset[2] = 0;
                        for (int c = 0; c < columns.length; c++) {
                            MDIntArray chunkData = new MDIntArray(dims(slices[s], rows[r], columns[c]));
                            fillIntChunk(data, dims, offset, chunkData);
                            offset[2] += columns[c];
                            intWriter.writeMDArray(dataSet3D, chunkData, blockOffset(s * slices[0], r * rows[0], c * columns[0]));
                        }
                        offset[1] += rows[r];
                    }
                    offset[0] += slices[s];
                }
                break;
            }
            case 4: {
                int[][] chunkSize4D = calculateChunkSize(dims);
                int[] dim0 = chunkSize4D[0];
                int[] dim1 = chunkSize4D[1];
                int[] dim2 = chunkSize4D[2];
                int[] dim3 = chunkSize4D[3];
                HDF5DataSet dataSet4D = intWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                MDIntArray chunkData = new MDIntArray(dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3]));
                                fillIntChunk(data, dims, offset, chunkData);
                                offset[3] += dim3[d3];
                                intWriter.writeMDArray(dataSet4D, chunkData, blockOffset(d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0]));
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            case 5: {
                int[][] chunkSize5D = calculateChunkSize(dims);
                int[] dim0 = chunkSize5D[0];
                int[] dim1 = chunkSize5D[1];
                int[] dim2 = chunkSize5D[2];
                int[] dim3 = chunkSize5D[3];
                int[] dim4 = chunkSize5D[4];
                HDF5DataSet dataSet5D = intWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0], dim4[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                offset[4] = 0;
                                for (int d4 = 0; d4 < dim4.length; d4++) {
                                    MDIntArray chunkData = new MDIntArray(dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3], dim4[d4]));
                                    fillIntChunk(data, dims, offset, chunkData);
                                    offset[4] += dim4[d4];
                                    intWriter.writeMDArray(dataSet5D, chunkData, blockOffset(d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0], d4 * dim4[0]));
                                }
                                offset[3] += dim3[d3];
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            default:
                return false;
        }
        return true;
    }

    /**
     * Writes a DataArray of type long to HDF5 file.
     *
     * @param writer    HDF5 writer
     * @param fieldName VisNow field name
     * @param da        DataArray
     * @param fieldDims dimensions of VisNow field
     *
     * @return true if the operation was successful, false otherwise
     */
    private static boolean writeLongDataArray(IHDF5Writer writer, String fieldName, DataArray da, long[] fieldDims)
    {
        if (writer == null || da == null || fieldDims == null) {
            return false;
        }
        if (da.getType() != DataArrayType.FIELD_DATA_LONG) {
            return false;
        }
        TimeData td = da.getTimeData();
        ArrayList<LargeArray> data = td.getValues();
        int veclen = da.getVectorLength();
        long[] offset = new long[(data.size() > 1 ? 1 : 0) + fieldDims.length + (veclen > 1 ? 1 : 0)];
        long[] dims;
        if (offset.length == fieldDims.length) {
            dims = fieldDims;

        } else {
            int off = 0;
            dims = new long[offset.length];
            if (data.size() > 1) {
                dims[0] = data.size();
                off = 1;
            }
            System.arraycopy(fieldDims, 0, dims, off, fieldDims.length);
            if (veclen > 1) {
                dims[dims.length - 1] = veclen;
            }
        }

        IHDF5LongWriter longWriter = writer.int64();

        switch (offset.length) {
            case 1: {
                int[][] chunkSize1D = calculateChunkSize(dims);
                int[] lengths = chunkSize1D[0];
                HDF5DataSet dataSet1D = longWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(lengths[0]));
                for (int bx = 0; bx < chunkSize1D.length; bx++) {
                    MDLongArray chunkData = new MDLongArray(dims(lengths[bx]));
                    fillLongChunk(data, dims, offset, chunkData);
                    offset[0] += lengths[bx];
                    longWriter.writeMDArray(dataSet1D, chunkData, blockOffset(bx * lengths[0]));
                }
                break;
            }
            case 2: {
                int[][] chunkSize2D = calculateChunkSize(dims);
                int[] rows = chunkSize2D[0];
                int[] columns = chunkSize2D[1];
                HDF5DataSet dataSet2D = longWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(rows[0], columns[0]));
                for (int r = 0; r < rows.length; r++) {
                    offset[1] = 0;
                    for (int c = 0; c < columns.length; c++) {
                        MDLongArray chunkData = new MDLongArray(dims(rows[r], columns[c]));
                        fillLongChunk(data, dims, offset, chunkData);
                        offset[1] += columns[c];
                        longWriter.writeMDArray(dataSet2D, chunkData, blockOffset(r * rows[0], c * columns[0]));
                    }
                    offset[0] += rows[r];
                }
                break;
            }
            case 3: {
                int[][] chunkSize3D = calculateChunkSize(dims);
                int[] slices = chunkSize3D[0];
                int[] rows = chunkSize3D[1];
                int[] columns = chunkSize3D[2];
                HDF5DataSet dataSet3D = longWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(slices[0], rows[0], columns[0]));
                for (int s = 0; s < slices.length; s++) {
                    offset[1] = 0;
                    for (int r = 0; r < rows.length; r++) {
                        offset[2] = 0;
                        for (int c = 0; c < columns.length; c++) {
                            MDLongArray chunkData = new MDLongArray(dims(slices[s], rows[r], columns[c]));
                            fillLongChunk(data, dims, offset, chunkData);
                            offset[2] += columns[c];
                            longWriter.writeMDArray(dataSet3D, chunkData, blockOffset(s * slices[0], r * rows[0], c * columns[0]));
                        }
                        offset[1] += rows[r];
                    }
                    offset[0] += slices[s];
                }
                break;
            }
            case 4: {
                int[][] chunkSize4D = calculateChunkSize(dims);
                int[] dim0 = chunkSize4D[0];
                int[] dim1 = chunkSize4D[1];
                int[] dim2 = chunkSize4D[2];
                int[] dim3 = chunkSize4D[3];
                HDF5DataSet dataSet4D = longWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                MDLongArray chunkData = new MDLongArray(dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3]));
                                fillLongChunk(data, dims, offset, chunkData);
                                offset[3] += dim3[d3];
                                longWriter.writeMDArray(dataSet4D, chunkData, blockOffset(d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0]));
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            case 5: {
                int[][] chunkSize5D = calculateChunkSize(dims);
                int[] dim0 = chunkSize5D[0];
                int[] dim1 = chunkSize5D[1];
                int[] dim2 = chunkSize5D[2];
                int[] dim3 = chunkSize5D[3];
                int[] dim4 = chunkSize5D[4];
                HDF5DataSet dataSet5D = longWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0], dim4[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                offset[4] = 0;
                                for (int d4 = 0; d4 < dim4.length; d4++) {
                                    MDLongArray chunkData = new MDLongArray(dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3], dim4[d4]));
                                    fillLongChunk(data, dims, offset, chunkData);
                                    offset[4] += dim4[d4];
                                    longWriter.writeMDArray(dataSet5D, chunkData, blockOffset(d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0], d4 * dim4[0]));
                                }
                                offset[3] += dim3[d3];
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            default:
                return false;
        }
        return true;
    }

    /**
     * Writes a DataArray of type float to HDF5 file.
     *
     * @param writer    HDF5 writer
     * @param fieldName VisNow field name
     * @param da        DataArray
     * @param fieldDims dimensions of VisNow field
     *
     * @return true if the operation was successful, false otherwise
     */
    private static boolean writeFloatDataArray(IHDF5Writer writer, String fieldName, DataArray da, long[] fieldDims)
    {
        if (writer == null || da == null || fieldDims == null) {
            return false;
        }
        TimeData td = da.getTimeData();
        ArrayList<LargeArray> data = td.getValues();
        int veclen = da.getVectorLength();
        long[] offset = new long[(data.size() > 1 ? 1 : 0) + fieldDims.length + (veclen > 1 ? 1 : 0)];
        long[] dims;
        if (offset.length == fieldDims.length) {
            dims = fieldDims;

        } else {
            int off = 0;
            dims = new long[offset.length];
            if (data.size() > 1) {
                dims[0] = data.size();
                off = 1;
            }
            System.arraycopy(fieldDims, 0, dims, off, fieldDims.length);
            if (veclen > 1) {
                dims[dims.length - 1] = veclen;
            }
        }

        IHDF5FloatWriter floatWriter = writer.float32();

        switch (offset.length) {
            case 1: {
                int[][] chunkSize1D = calculateChunkSize(dims);
                int[] lengths = chunkSize1D[0];
                HDF5DataSet dataSet1D = floatWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(lengths[0]));
                for (int bx = 0; bx < chunkSize1D.length; bx++) {
                    MDFloatArray chunkData = new MDFloatArray(dims(lengths[bx]));
                    fillFloatChunk(data, dims, offset, chunkData);
                    offset[0] += lengths[bx];
                    floatWriter.writeMDArray(dataSet1D, chunkData, blockOffset(bx * lengths[0]));
                }
                break;
            }
            case 2: {
                int[][] chunkSize2D = calculateChunkSize(dims);
                int[] rows = chunkSize2D[0];
                int[] columns = chunkSize2D[1];
                HDF5DataSet dataSet2D = floatWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(rows[0], columns[0]));
                for (int r = 0; r < rows.length; r++) {
                    offset[1] = 0;
                    for (int c = 0; c < columns.length; c++) {
                        MDFloatArray chunkData = new MDFloatArray(dims(rows[r], columns[c]));
                        fillFloatChunk(data, dims, offset, chunkData);
                        offset[1] += columns[c];
                        floatWriter.writeMDArray(dataSet2D, chunkData, blockOffset(r * rows[0], c * columns[0]));
                    }
                    offset[0] += rows[r];
                }
                break;
            }
            case 3: {
                int[][] chunkSize3D = calculateChunkSize(dims);
                int[] slices = chunkSize3D[0];
                int[] rows = chunkSize3D[1];
                int[] columns = chunkSize3D[2];
                HDF5DataSet dataSet3D = floatWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(slices[0], rows[0], columns[0]));
                for (int s = 0; s < slices.length; s++) {
                    offset[1] = 0;
                    for (int r = 0; r < rows.length; r++) {
                        offset[2] = 0;
                        for (int c = 0; c < columns.length; c++) {
                            MDFloatArray chunkData = new MDFloatArray(dims(slices[s], rows[r], columns[c]));
                            fillFloatChunk(data, dims, offset, chunkData);
                            offset[2] += columns[c];
                            floatWriter.writeMDArray(dataSet3D, chunkData, blockOffset(s * slices[0], r * rows[0], c * columns[0]));
                        }
                        offset[1] += rows[r];
                    }
                    offset[0] += slices[s];
                }
                break;
            }
            case 4: {
                int[][] chunkSize4D = calculateChunkSize(dims);
                int[] dim0 = chunkSize4D[0];
                int[] dim1 = chunkSize4D[1];
                int[] dim2 = chunkSize4D[2];
                int[] dim3 = chunkSize4D[3];
                HDF5DataSet dataSet4D = floatWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                MDFloatArray chunkData = new MDFloatArray(dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3]));
                                fillFloatChunk(data, dims, offset, chunkData);
                                offset[3] += dim3[d3];
                                floatWriter.writeMDArray(dataSet4D, chunkData, blockOffset(d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0]));
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            case 5: {
                int[][] chunkSize5D = calculateChunkSize(dims);
                int[] dim0 = chunkSize5D[0];
                int[] dim1 = chunkSize5D[1];
                int[] dim2 = chunkSize5D[2];
                int[] dim3 = chunkSize5D[3];
                int[] dim4 = chunkSize5D[4];
                HDF5DataSet dataSet5D = floatWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0], dim4[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                offset[4] = 0;
                                for (int d4 = 0; d4 < dim4.length; d4++) {
                                    MDFloatArray chunkData = new MDFloatArray(dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3], dim4[d4]));
                                    fillFloatChunk(data, dims, offset, chunkData);
                                    offset[4] += dim4[d4];
                                    floatWriter.writeMDArray(dataSet5D, chunkData, blockOffset(d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0], d4 * dim4[0]));
                                }
                                offset[3] += dim3[d3];
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            default:
                return false;
        }
        return true;
    }

    /**
     * Writes a DataArray of type double to HDF5 file.
     *
     * @param writer    HDF5 writer
     * @param fieldName VisNow field name
     * @param da        DataArray
     * @param fieldDims dimensions of VisNow field
     *
     * @return true if the operation was successful, false otherwise
     */
    private static boolean writeDoubleDataArray(IHDF5Writer writer, String fieldName, DataArray da, long[] fieldDims)
    {
        if (writer == null || da == null || fieldDims == null) {
            return false;
        }
        TimeData td = da.getTimeData();
        ArrayList<LargeArray> data = td.getValues();
        int veclen = da.getVectorLength();
        long[] offset = new long[(data.size() > 1 ? 1 : 0) + fieldDims.length + (veclen > 1 ? 1 : 0)];
        long[] dims;
        if (offset.length == fieldDims.length) {
            dims = fieldDims;

        } else {
            int off = 0;
            dims = new long[offset.length];
            if (data.size() > 1) {
                dims[0] = data.size();
                off = 1;
            }
            System.arraycopy(fieldDims, 0, dims, off, fieldDims.length);
            if (veclen > 1) {
                dims[dims.length - 1] = veclen;
            }
        }

        IHDF5DoubleWriter doubleWriter = writer.float64();

        switch (offset.length) {
            case 1: {
                int[][] chunkSize1D = calculateChunkSize(dims);
                int[] lengths = chunkSize1D[0];
                HDF5DataSet dataSet1D = doubleWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(lengths[0]));
                for (int bx = 0; bx < chunkSize1D.length; bx++) {
                    MDDoubleArray chunkData = new MDDoubleArray(dims(lengths[bx]));
                    fillDoubleChunk(data, dims, offset, chunkData);
                    offset[0] += lengths[bx];
                    doubleWriter.writeMDArray(dataSet1D, chunkData, blockOffset(bx * lengths[0]));
                }
                break;
            }
            case 2: {
                int[][] chunkSize2D = calculateChunkSize(dims);
                int[] rows = chunkSize2D[0];
                int[] columns = chunkSize2D[1];
                HDF5DataSet dataSet2D = doubleWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(rows[0], columns[0]));
                for (int r = 0; r < rows.length; r++) {
                    offset[1] = 0;
                    for (int c = 0; c < columns.length; c++) {
                        MDDoubleArray chunkData = new MDDoubleArray(dims(rows[r], columns[c]));
                        fillDoubleChunk(data, dims, offset, chunkData);
                        offset[1] += columns[c];
                        doubleWriter.writeMDArray(dataSet2D, chunkData, blockOffset(r * rows[0], c * columns[0]));
                    }
                    offset[0] += rows[r];
                }
                break;
            }
            case 3: {
                int[][] chunkSize3D = calculateChunkSize(dims);
                int[] slices = chunkSize3D[0];
                int[] rows = chunkSize3D[1];
                int[] columns = chunkSize3D[2];
                HDF5DataSet dataSet3D = doubleWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(slices[0], rows[0], columns[0]));
                for (int s = 0; s < slices.length; s++) {
                    offset[1] = 0;
                    for (int r = 0; r < rows.length; r++) {
                        offset[2] = 0;
                        for (int c = 0; c < columns.length; c++) {
                            MDDoubleArray chunkData = new MDDoubleArray(dims(slices[s], rows[r], columns[c]));
                            fillDoubleChunk(data, dims, offset, chunkData);
                            offset[2] += columns[c];
                            doubleWriter.writeMDArray(dataSet3D, chunkData, blockOffset(s * slices[0], r * rows[0], c * columns[0]));
                        }
                        offset[1] += rows[r];
                    }
                    offset[0] += slices[s];
                }
                break;
            }
            case 4: {
                int[][] chunkSize4D = calculateChunkSize(dims);
                int[] dim0 = chunkSize4D[0];
                int[] dim1 = chunkSize4D[1];
                int[] dim2 = chunkSize4D[2];
                int[] dim3 = chunkSize4D[3];
                HDF5DataSet dataSet4D = doubleWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                MDDoubleArray chunkData = new MDDoubleArray(dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3]));
                                fillDoubleChunk(data, dims, offset, chunkData);
                                offset[3] += dim3[d3];
                                doubleWriter.writeMDArray(dataSet4D, chunkData, blockOffset(d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0]));
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            case 5: {
                int[][] chunkSize5D = calculateChunkSize(dims);
                int[] dim0 = chunkSize5D[0];
                int[] dim1 = chunkSize5D[1];
                int[] dim2 = chunkSize5D[2];
                int[] dim3 = chunkSize5D[3];
                int[] dim4 = chunkSize5D[4];
                HDF5DataSet dataSet5D = doubleWriter.createMDArrayAndOpen("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0], dim4[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                offset[4] = 0;
                                for (int d4 = 0; d4 < dim4.length; d4++) {
                                    MDDoubleArray chunkData = new MDDoubleArray(dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3], dim4[d4]));
                                    fillDoubleChunk(data, dims, offset, chunkData);
                                    offset[4] += dim4[d4];
                                    doubleWriter.writeMDArray(dataSet5D, chunkData, blockOffset(d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0], d4 * dim4[0]));
                                }
                                offset[3] += dim3[d3];
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            default:
                return false;
        }
        return true;
    }

    /**
     * Writes a DataArray of type complex to HDF5 file.
     *
     * @param writer    HDF5 writer
     * @param fieldName VisNow field name
     * @param da        DataArray
     * @param fieldDims dimensions of VisNow field
     *
     * @return true if the operation was successful, false otherwise
     */
    private static boolean writeComplexDataArray(IHDF5Writer writer, String fieldName, DataArray da, long[] fieldDims)
    {
        if (writer == null || da == null || fieldDims == null) {
            return false;
        }
        if (da.getType() != DataArrayType.FIELD_DATA_COMPLEX) {
            return false;
        }
        TimeData td = da.getTimeData();
        ArrayList<LargeArray> data = td.getValues();
        int veclen = da.getVectorLength();
        long[] offset = new long[(data.size() > 1 ? 1 : 0) + fieldDims.length + (veclen > 1 ? 1 : 0)];
        long[] dims;
        if (offset.length == fieldDims.length) {
            dims = fieldDims;

        } else {
            int off = 0;
            dims = new long[offset.length];
            if (data.size() > 1) {
                dims[0] = data.size();
                off = 1;
            }
            System.arraycopy(fieldDims, 0, dims, off, fieldDims.length);
            if (veclen > 1) {
                dims[dims.length - 1] = veclen;
            }
        }

        IHDF5CompoundWriter compundWriter = writer.compound();
        HDF5CompoundType<ComplexNumber> type = compundWriter.getInferredType("ComplexNumber", ComplexNumber.class);

        switch (offset.length) {
            case 1: {
                int[][] chunkSize1D = calculateChunkSize(dims);
                int[] lengths = chunkSize1D[0];
                compundWriter.createMDArray("visnow/" + fieldName + "/" + da.getName() + "/dataseries", type, dims, dims(lengths[0]));
                for (int bx = 0; bx < chunkSize1D.length; bx++) {
                    MDArray<ComplexNumber> chunkData = new MDArray<>(ComplexNumber.class, dims(lengths[bx]));
                    fillComplexChunk(data, dims, offset, chunkData);
                    offset[0] += lengths[bx];
                    compundWriter.writeMDArrayBlockWithOffset("visnow/" + fieldName + "/" + da.getName() + "/dataseries", type, chunkData, new long[]{bx * lengths[0]});
                }
                break;
            }
            case 2: {
                int[][] chunkSize2D = calculateChunkSize(dims);
                int[] rows = chunkSize2D[0];
                int[] columns = chunkSize2D[1];
                compundWriter.createMDArray("visnow/" + fieldName + "/" + da.getName() + "/dataseries", type, dims, dims(rows[0], columns[0]));
                for (int r = 0; r < rows.length; r++) {
                    offset[1] = 0;
                    for (int c = 0; c < columns.length; c++) {
                        MDArray<ComplexNumber> chunkData = new MDArray<>(ComplexNumber.class, dims(rows[r], columns[c]));
                        fillComplexChunk(data, dims, offset, chunkData);
                        offset[1] += columns[c];
                        compundWriter.writeMDArrayBlockWithOffset("visnow/" + fieldName + "/" + da.getName() + "/dataseries", type, chunkData, new long[]{r * rows[0], c * columns[0]});
                    }
                    offset[0] += rows[r];
                }
                break;
            }
            case 3: {
                int[][] chunkSize3D = calculateChunkSize(dims);
                int[] slices = chunkSize3D[0];
                int[] rows = chunkSize3D[1];
                int[] columns = chunkSize3D[2];
                compundWriter.createMDArray("visnow/" + fieldName + "/" + da.getName() + "/dataseries", type, dims, dims(slices[0], rows[0], columns[0]));
                for (int s = 0; s < slices.length; s++) {
                    offset[1] = 0;
                    for (int r = 0; r < rows.length; r++) {
                        offset[2] = 0;
                        for (int c = 0; c < columns.length; c++) {
                            MDArray<ComplexNumber> chunkData = new MDArray<>(ComplexNumber.class, dims(slices[s], rows[r], columns[c]));
                            fillComplexChunk(data, dims, offset, chunkData);
                            offset[2] += columns[c];
                            compundWriter.writeMDArrayBlockWithOffset("visnow/" + fieldName + "/" + da.getName() + "/dataseries", type, chunkData, new long[]{s * slices[0], r * rows[0], c * columns[0]});
                        }
                        offset[1] += rows[r];
                    }
                    offset[0] += slices[s];
                }
                break;
            }
            case 4: {
                int[][] chunkSize4D = calculateChunkSize(dims);
                int[] dim0 = chunkSize4D[0];
                int[] dim1 = chunkSize4D[1];
                int[] dim2 = chunkSize4D[2];
                int[] dim3 = chunkSize4D[3];
                compundWriter.createMDArray("visnow/" + fieldName + "/" + da.getName() + "/dataseries", type, dims, dims(dim0[0], dim1[0], dim2[0], dim3[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                MDArray<ComplexNumber> chunkData = new MDArray<>(ComplexNumber.class, dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3]));
                                fillComplexChunk(data, dims, offset, chunkData);
                                offset[3] += dim3[d3];
                                compundWriter.writeMDArrayBlockWithOffset("visnow/" + fieldName + "/" + da.getName() + "/dataseries", type, chunkData, new long[]{d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0]});
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            case 5: {
                int[][] chunkSize5D = calculateChunkSize(dims);
                int[] dim0 = chunkSize5D[0];
                int[] dim1 = chunkSize5D[1];
                int[] dim2 = chunkSize5D[2];
                int[] dim3 = chunkSize5D[3];
                int[] dim4 = chunkSize5D[4];
                compundWriter.createMDArray("visnow/" + fieldName + "/" + da.getName() + "/dataseries", type, dims, dims(dim0[0], dim1[0], dim2[0], dim3[0], dim4[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                offset[4] = 0;
                                for (int d4 = 0; d4 < dim4.length; d4++) {
                                    MDArray<ComplexNumber> chunkData = new MDArray<>(ComplexNumber.class, dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3], dim4[d4]));
                                    fillComplexChunk(data, dims, offset, chunkData);
                                    offset[4] += dim4[d4];
                                    compundWriter.writeMDArrayBlockWithOffset("visnow/" + fieldName + "/" + da.getName() + "/dataseries", type, chunkData, new long[]{d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0], d4 * dim4[0]});
                                }
                                offset[3] += dim3[d3];
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            default:
                return false;
        }
        return true;
    }

    /**
     * Writes a DataArray of type string to HDF5 file.
     *
     * @param writer    HDF5 writer
     * @param fieldName VisNow field name
     * @param da        DataArray
     * @param fieldDims dimensions of VisNow field
     *
     * @return true if the operation was successful, false otherwise
     */
    private static boolean writeStringDataArray(IHDF5Writer writer, String fieldName, DataArray da, long[] fieldDims)
    {
        if (writer == null || da == null || fieldDims == null) {
            return false;
        }
        if (da.getType() != DataArrayType.FIELD_DATA_STRING) {
            return false;
        }
        TimeData td = da.getTimeData();
        ArrayList<LargeArray> data = td.getValues();
        int veclen = da.getVectorLength();
        long[] offset = new long[(data.size() > 1 ? 1 : 0) + fieldDims.length + (veclen > 1 ? 1 : 0)];
        long[] dims;
        if (offset.length == fieldDims.length) {
            dims = fieldDims;

        } else {
            int off = 0;
            dims = new long[offset.length];
            if (data.size() > 1) {
                dims[0] = data.size();
                off = 1;
            }
            System.arraycopy(fieldDims, 0, dims, off, fieldDims.length);
            if (veclen > 1) {
                dims[dims.length - 1] = veclen;
            }
        }

        IHDF5StringWriter stringWriter = writer.string();

        switch (offset.length) {
            case 1: {
                int[][] chunkSize1D = calculateChunkSize(dims);
                int[] lengths = chunkSize1D[0];
                stringWriter.createMDArrayVL("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(lengths[0]));
                for (int bx = 0; bx < chunkSize1D.length; bx++) {
                    MDArray<String> chunkData = new MDArray<>(String.class, dims(lengths[bx]));
                    fillStringChunk(data, dims, offset, chunkData);
                    offset[0] += lengths[bx];
                    stringWriter.writeMDArrayBlockWithOffset("visnow/" + fieldName + "/" + da.getName() + "/dataseries", chunkData, new long[]{bx * lengths[0]});
                }
                break;
            }
            case 2: {
                int[][] chunkSize2D = calculateChunkSize(dims);
                int[] rows = chunkSize2D[0];
                int[] columns = chunkSize2D[1];
                stringWriter.createMDArrayVL("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(rows[0], columns[0]));
                for (int r = 0; r < rows.length; r++) {
                    offset[1] = 0;
                    for (int c = 0; c < columns.length; c++) {
                        MDArray<String> chunkData = new MDArray<>(String.class, dims(rows[r], columns[c]));
                        fillStringChunk(data, dims, offset, chunkData);
                        offset[1] += columns[c];
                        stringWriter.writeMDArrayBlockWithOffset("visnow/" + fieldName + "/" + da.getName() + "/dataseries", chunkData, new long[]{r * rows[0], c * columns[0]});
                    }
                    offset[0] += rows[r];
                }
                break;
            }
            case 3: {
                int[][] chunkSize3D = calculateChunkSize(dims);
                int[] slices = chunkSize3D[0];
                int[] rows = chunkSize3D[1];
                int[] columns = chunkSize3D[2];
                stringWriter.createMDArrayVL("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(slices[0], rows[0], columns[0]));
                for (int s = 0; s < slices.length; s++) {
                    offset[1] = 0;
                    for (int r = 0; r < rows.length; r++) {
                        offset[2] = 0;
                        for (int c = 0; c < columns.length; c++) {
                            MDArray<String> chunkData = new MDArray<>(String.class, dims(slices[s], rows[r], columns[c]));
                            fillStringChunk(data, dims, offset, chunkData);
                            offset[2] += columns[c];
                            stringWriter.writeMDArrayBlockWithOffset("visnow/" + fieldName + "/" + da.getName() + "/dataseries", chunkData, new long[]{s * slices[0], r * rows[0], c * columns[0]});
                        }
                        offset[1] += rows[r];
                    }
                    offset[0] += slices[s];
                }
                break;
            }
            case 4: {
                int[][] chunkSize4D = calculateChunkSize(dims);
                int[] dim0 = chunkSize4D[0];
                int[] dim1 = chunkSize4D[1];
                int[] dim2 = chunkSize4D[2];
                int[] dim3 = chunkSize4D[3];
                stringWriter.createMDArrayVL("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                MDArray<String> chunkData = new MDArray<>(String.class, dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3]));
                                fillStringChunk(data, dims, offset, chunkData);
                                offset[3] += dim3[d3];
                                stringWriter.writeMDArrayBlockWithOffset("visnow/" + fieldName + "/" + da.getName() + "/dataseries", chunkData, new long[]{d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0]});
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            case 5: {
                int[][] chunkSize5D = calculateChunkSize(dims);
                int[] dim0 = chunkSize5D[0];
                int[] dim1 = chunkSize5D[1];
                int[] dim2 = chunkSize5D[2];
                int[] dim3 = chunkSize5D[3];
                int[] dim4 = chunkSize5D[4];
                stringWriter.createMDArrayVL("visnow/" + fieldName + "/" + da.getName() + "/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0], dim4[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                offset[4] = 0;
                                for (int d4 = 0; d4 < dim4.length; d4++) {
                                    MDArray<String> chunkData = new MDArray<>(String.class, dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3], dim4[d4]));
                                    fillStringChunk(data, dims, offset, chunkData);
                                    offset[4] += dim4[d4];
                                    stringWriter.writeMDArrayBlockWithOffset("visnow/" + fieldName + "/" + da.getName() + "/dataseries", chunkData, new long[]{d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0], d4 * dim4[0]});
                                }
                                offset[3] += dim3[d3];
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            default:
                return false;
        }
        return true;
    }

    /**
     * Writes a DataArray to HDF5 file. ObjectDataArray is not supported.
     *
     * @param writer    HDF5 writer
     * @param fieldName VisNow field name
     * @param da        DataArray
     * @param fieldDims dimensions of VisNow field
     *
     * @return true if the operation was successful, false otherwise
     */
    public static boolean writeDataArray(IHDF5Writer writer, String fieldName, DataArray da, long[] fieldDims)
    {
        boolean result = true;
        writer.object().createGroup("visnow/" + fieldName + "/" + da.getName());
        switch (da.getType()) {
            case FIELD_DATA_LOGIC:
            case FIELD_DATA_BYTE:
                result = writeLogicOrByteDataArray(writer, fieldName, da, fieldDims);
                break;
            case FIELD_DATA_SHORT:
                result = writeShortDataArray(writer, fieldName, da, fieldDims);
                break;
            case FIELD_DATA_INT:
                result = writeIntDataArray(writer, fieldName, da, fieldDims);
                break;
            case FIELD_DATA_LONG:
                result = writeLongDataArray(writer, fieldName, da, fieldDims);
                break;
            case FIELD_DATA_FLOAT:
                result = writeFloatDataArray(writer, fieldName, da, fieldDims);
                break;
            case FIELD_DATA_DOUBLE:
                result = writeDoubleDataArray(writer, fieldName, da, fieldDims);
                break;
            case FIELD_DATA_COMPLEX:
                result = writeComplexDataArray(writer, fieldName, da, fieldDims);
                break;
            case FIELD_DATA_STRING:
                result = writeStringDataArray(writer, fieldName, da, fieldDims);
                break;
            default:
                throw new IllegalArgumentException("Unsupported type of DataArray.");
        }
        if (result == false) {
            return false;
        }

        if (da.isTimeDependant()) {
            if (!writeFloatVector(writer, "visnow/" + fieldName + "/" + da.getName() + "/timeseries", da.getTimeData().getTimesAsArray())) {
                writer.close();
                return false;
            }
        }

        if (da.getMinValue() != da.getPreferredMinValue() || da.getMaxValue() != da.getPreferredMaxValue()) {
            double[] preferredRange = new double[]{da.getPreferredMinValue(), da.getPreferredMaxValue()};
            writer.float64().setArrayAttr("visnow/" + fieldName + "/" + da.getName(), "preferred_range", preferredRange);
        }

        if (da.getPreferredMinValue() != da.getPreferredPhysMinValue() || da.getPreferredMaxValue() != da.getPreferredPhysMaxValue()) {
            double[] preferredPhysRange = new double[]{da.getPreferredPhysMinValue(), da.getPreferredPhysMaxValue()};
            writer.float64().setArrayAttr("visnow/" + fieldName + "/" + da.getName(), "preferred_physical_range", preferredPhysRange);
        }

        if (da.getUserData() != null) {
            writer.string().setArrayAttr("visnow/" + fieldName + "/" + da.getName(), "user_data", da.getUserData());
        }

        if (!da.getUnit().equals("1")) {
            writer.string().setAttr("visnow/" + fieldName + "/" + da.getName(), "unit", da.getUnit());
        }

        return result;
    }

    /**
     * Writes coordinates to HDF5 file.
     *
     * @param writer    HDF5 writer
     * @param fieldName VisNow field name
     * @param coords    coordinates
     * @param fieldDims dimensions of VisNow field
     *
     * @return true if the operation was successful, false otherwise
     */
    private static boolean writeCoords(IHDF5Writer writer, String fieldName, TimeData coords, long[] fieldDims)
    {
        if (writer == null || coords == null || fieldDims == null) {
            return false;
        }
        if (coords.getType() != DataArrayType.FIELD_DATA_FLOAT) {
            return false;
        }
        float[] timeSeries = coords.getTimesAsArray();
        ArrayList<LargeArray> data = coords.getValues();
        long[] offset = new long[(coords.getNSteps() > 1 ? 1 : 0) + fieldDims.length + 1];
        int off = 0;
        long[] dims = new long[offset.length];
        if (coords.getNSteps() > 1) {
            dims[0] = coords.getNSteps();
            off = 1;
        }
        System.arraycopy(fieldDims, 0, dims, off, fieldDims.length);
        dims[dims.length - 1] = 3;

        IHDF5FloatWriter floatWriter = writer.float32();

        writer.object().createGroup("visnow/" + fieldName + "/vnfield_geometry/coordinates");

        writeFloatVector(writer, "visnow/" + fieldName + "/vnfield_geometry/coordinates/timeseries", timeSeries);

        switch (offset.length) {
            case 2: {
                int[][] chunkSize2D = calculateChunkSize(dims);
                int[] rows = chunkSize2D[0];
                int[] columns = chunkSize2D[1];
                HDF5DataSet dataSet2D = floatWriter.createMDArrayAndOpen("visnow/" + fieldName + "/vnfield_geometry/coordinates/dataseries", dims, dims(rows[0], columns[0]));
                for (int r = 0; r < rows.length; r++) {
                    offset[1] = 0;
                    for (int c = 0; c < columns.length; c++) {
                        MDFloatArray chunkData = new MDFloatArray(dims(rows[r], columns[c]));
                        fillFloatChunk(data, dims, offset, chunkData);
                        offset[1] += columns[c];
                        floatWriter.writeMDArray(dataSet2D, chunkData, blockOffset(r * rows[0], c * columns[0]));
                    }
                    offset[0] += rows[r];
                }
                break;
            }
            case 3: {
                int[][] chunkSize3D = calculateChunkSize(dims);
                int[] slices = chunkSize3D[0];
                int[] rows = chunkSize3D[1];
                int[] columns = chunkSize3D[2];
                HDF5DataSet dataSet3D = floatWriter.createMDArrayAndOpen("visnow/" + fieldName + "/vnfield_geometry/coordinates/dataseries", dims, dims(slices[0], rows[0], columns[0]));
                for (int s = 0; s < slices.length; s++) {
                    offset[1] = 0;
                    for (int r = 0; r < rows.length; r++) {
                        offset[2] = 0;
                        for (int c = 0; c < columns.length; c++) {
                            MDFloatArray chunkData = new MDFloatArray(dims(slices[s], rows[r], columns[c]));
                            fillFloatChunk(data, dims, offset, chunkData);
                            offset[2] += columns[c];
                            floatWriter.writeMDArray(dataSet3D, chunkData, blockOffset(s * slices[0], r * rows[0], c * columns[0]));
                        }
                        offset[1] += rows[r];
                    }
                    offset[0] += slices[s];
                }
                break;
            }
            case 4: {
                int[][] chunkSize4D = calculateChunkSize(dims);
                int[] dim0 = chunkSize4D[0];
                int[] dim1 = chunkSize4D[1];
                int[] dim2 = chunkSize4D[2];
                int[] dim3 = chunkSize4D[3];
                HDF5DataSet dataSet4D = floatWriter.createMDArrayAndOpen("visnow/" + fieldName + "/vnfield_geometry/coordinates/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                MDFloatArray chunkData = new MDFloatArray(dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3]));
                                fillFloatChunk(data, dims, offset, chunkData);
                                offset[3] += dim3[d3];
                                floatWriter.writeMDArray(dataSet4D, chunkData, blockOffset(d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0]));
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            case 5: {
                int[][] chunkSize5D = calculateChunkSize(dims);
                int[] dim0 = chunkSize5D[0];
                int[] dim1 = chunkSize5D[1];
                int[] dim2 = chunkSize5D[2];
                int[] dim3 = chunkSize5D[3];
                int[] dim4 = chunkSize5D[4];
                HDF5DataSet dataSet5D = floatWriter.createMDArrayAndOpen("visnow/" + fieldName + "/vnfield_geometry/coordinates/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0], dim4[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                offset[4] = 0;
                                for (int d4 = 0; d4 < dim4.length; d4++) {
                                    MDFloatArray chunkData = new MDFloatArray(dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3], dim4[d4]));
                                    fillFloatChunk(data, dims, offset, chunkData);
                                    offset[4] += dim4[d4];
                                    floatWriter.writeMDArray(dataSet5D, chunkData, blockOffset(d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0], d4 * dim4[0]));
                                }
                                offset[3] += dim3[d3];
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            default:
                return false;
        }
        return true;
    }

    /**
     * Writes mask to HDF5 file.
     *
     * @param writer    HDF5 writer
     * @param fieldName VisNow field name
     * @param mask      mask
     * @param fieldDims dimensions of VisNow field
     *
     * @return true if the operation was successful, false otherwise
     */
    private static boolean writeMask(IHDF5Writer writer, String fieldName, TimeData mask, long[] fieldDims)
    {
        if (writer == null || mask == null || fieldDims == null) {
            return false;
        }
        if (mask.getType() != DataArrayType.FIELD_DATA_LOGIC) {
            return false;
        }
        float[] timeSeries = mask.getTimesAsArray();
        ArrayList<LargeArray> data = mask.getValues();
        long[] offset = new long[(mask.getNSteps() > 1 ? 1 : 0) + fieldDims.length];
        int off = 0;
        long[] dims = new long[offset.length];
        if (mask.getNSteps() > 1) {
            dims[0] = mask.getNSteps();
            off = 1;
        }
        System.arraycopy(fieldDims, 0, dims, off, fieldDims.length);

        IHDF5ByteWriter byteWriter = writer.int8();

        writer.object().createGroup("visnow/" + fieldName + "/vnfield_mask");

        writeFloatVector(writer, "visnow/" + fieldName + "/vnfield_mask/timeseries", timeSeries);

        switch (offset.length) {
            case 2: {
                int[][] chunkSize2D = calculateChunkSize(dims);
                int[] rows = chunkSize2D[0];
                int[] columns = chunkSize2D[1];
                HDF5DataSet dataSet2D = byteWriter.createMDArrayAndOpen("visnow/" + fieldName + "/vnfield_mask/dataseries", dims, dims(rows[0], columns[0]));
                for (int r = 0; r < rows.length; r++) {
                    offset[1] = 0;
                    for (int c = 0; c < columns.length; c++) {
                        MDByteArray chunkData = new MDByteArray(dims(rows[r], columns[c]));
                        fillLogicOrByteChunk(data, dims, offset, chunkData);
                        offset[1] += columns[c];
                        byteWriter.writeMDArray(dataSet2D, chunkData, blockOffset(r * rows[0], c * columns[0]));
                    }
                    offset[0] += rows[r];
                }
                break;
            }
            case 3: {
                int[][] chunkSize3D = calculateChunkSize(dims);
                int[] slices = chunkSize3D[0];
                int[] rows = chunkSize3D[1];
                int[] columns = chunkSize3D[2];
                HDF5DataSet dataSet3D = byteWriter.createMDArrayAndOpen("visnow/" + fieldName + "/vnfield_mask/dataseries", dims, dims(slices[0], rows[0], columns[0]));
                for (int s = 0; s < slices.length; s++) {
                    offset[1] = 0;
                    for (int r = 0; r < rows.length; r++) {
                        offset[2] = 0;
                        for (int c = 0; c < columns.length; c++) {
                            MDByteArray chunkData = new MDByteArray(dims(slices[s], rows[r], columns[c]));
                            fillLogicOrByteChunk(data, dims, offset, chunkData);
                            offset[2] += columns[c];
                            byteWriter.writeMDArray(dataSet3D, chunkData, blockOffset(s * slices[0], r * rows[0], c * columns[0]));
                        }
                        offset[1] += rows[r];
                    }
                    offset[0] += slices[s];
                }
                break;
            }
            case 4: {
                int[][] chunkSize4D = calculateChunkSize(dims);
                int[] dim0 = chunkSize4D[0];
                int[] dim1 = chunkSize4D[1];
                int[] dim2 = chunkSize4D[2];
                int[] dim3 = chunkSize4D[3];
                HDF5DataSet dataSet4D = byteWriter.createMDArrayAndOpen("visnow/" + fieldName + "/vnfield_mask/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                MDByteArray chunkData = new MDByteArray(dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3]));
                                fillLogicOrByteChunk(data, dims, offset, chunkData);
                                offset[3] += dim3[d3];
                                byteWriter.writeMDArray(dataSet4D, chunkData, blockOffset(d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0]));
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            case 5: {
                int[][] chunkSize5D = calculateChunkSize(dims);
                int[] dim0 = chunkSize5D[0];
                int[] dim1 = chunkSize5D[1];
                int[] dim2 = chunkSize5D[2];
                int[] dim3 = chunkSize5D[3];
                int[] dim4 = chunkSize5D[4];
                HDF5DataSet dataSet5D = byteWriter.createMDArrayAndOpen("visnow/" + fieldName + "/vnfield_mask/dataseries", dims, dims(dim0[0], dim1[0], dim2[0], dim3[0], dim4[0]));
                for (int d0 = 0; d0 < dim0.length; d0++) {
                    offset[1] = 0;
                    for (int d1 = 0; d1 < dim1.length; d1++) {
                        offset[2] = 0;
                        for (int d2 = 0; d2 < dim2.length; d2++) {
                            offset[3] = 0;
                            for (int d3 = 0; d3 < dim3.length; d3++) {
                                offset[4] = 0;
                                for (int d4 = 0; d4 < dim4.length; d4++) {
                                    MDByteArray chunkData = new MDByteArray(dims(dim0[d0], dim1[d1], dim2[d2], dim3[d3], dim4[d4]));
                                    fillLogicOrByteChunk(data, dims, offset, chunkData);
                                    offset[4] += dim4[d4];
                                    byteWriter.writeMDArray(dataSet5D, chunkData, blockOffset(d0 * dim0[0], d1 * dim1[0], d2 * dim2[0], d3 * dim3[0], d4 * dim4[0]));
                                }
                                offset[3] += dim3[d3];
                            }
                            offset[2] += dim2[d2];
                        }
                        offset[1] += dim1[d1];
                    }
                    offset[0] += dim0[d0];
                }
                break;
            }
            default:
                return false;
        }
        return true;
    }

    /**
     * Writes float vector to HDF5 file.
     *
     * @param writer HDF5 writer
     * @param name   dataset name
     * @param vector input data
     *
     * @return true if the operation was successful, false otherwise
     */
    private static boolean writeFloatVector(IHDF5Writer writer, String name, float[] vector)
    {
        if (writer == null || name == null || vector == null) {
            return false;
        }
        writer.float32().writeArray(name, vector);
        return true;
    }

    /**
     * Writes float matrix to HDF5 file.
     *
     * @param writer HDF5 writer
     * @param name   dataset name
     * @param vector input data
     *
     * @return true if the operation was successful, false otherwise
     */
    private static boolean writeFloatMatrix(IHDF5Writer writer, String name, float[][] matrix)
    {
        if (writer == null || name == null || matrix == null) {
            return false;
        }
        writer.float32().writeMatrix(name, matrix);
        return true;
    }

}
