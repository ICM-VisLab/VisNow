/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.PointsWizard;

import javax.swing.event.ChangeEvent;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.CustomSlicesDescriptor;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.PointDescriptor;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class PointEntry extends Entry
{

    private PointDescriptor pointDescriptor = null;
    private CustomSlicesDescriptor slicesDescriptor = null;

    public PointEntry(String id, String name, String description)
    {
        super(Entry.ENTRY_TYPE_POINT, id, name, description);
    }

    @Override
    public String toString()
    {
        return getName();
        //return "point "+id;
    }

    @Override
    public boolean isReady()
    {
        return (pointDescriptor != null);
    }

    /**
     * @return the pointDescriptor
     */
    public PointDescriptor getPointDescriptor()
    {
        return pointDescriptor;
    }

    /**
     * @param pointDescriptor the pointDescriptor to set
     */
    public void setPointDescriptor(PointDescriptor pointDescriptor)
    {
        //pointDescriptor.setName(this.id);
        this.pointDescriptor = pointDescriptor;
        fireStateChanged();
    }

    /**
     * @return the slicesDescriptor
     */
    public CustomSlicesDescriptor getSlicesDescriptor()
    {
        return slicesDescriptor;
    }

    /**
     * @param slicesDescriptor the slicesDescriptor to set
     */
    public void setSlicesDescriptor(CustomSlicesDescriptor slicesDescriptor)
    {
        //pointDescriptor.setName(this.id);
        this.slicesDescriptor = slicesDescriptor;
        fireStateChanged();
    }

    public void stateChanged(ChangeEvent e)
    {
    }

}
