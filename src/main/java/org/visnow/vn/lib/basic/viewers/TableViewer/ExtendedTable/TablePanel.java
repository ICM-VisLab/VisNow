//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.TableViewer.ExtendedTable;

import org.visnow.vn.gui.utils.TableColumnAdjuster;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import javax.swing.AbstractListModel;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import org.apache.commons.math3.util.FastMath;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.vn.lib.basic.viewers.TableViewer.DataSet;
import org.visnow.vn.lib.utils.StringUtils;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.commons.io.FilenameUtils;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.lib.basic.writers.CSVWriter.CSVWriterCore;
import org.visnow.vn.lib.basic.writers.HDF5Writer.HDF5WriterCore;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.swing.filechooser.VNFileChooser;
import org.visnow.vn.system.utils.usermessage.Level;
import org.visnow.vn.system.utils.usermessage.UserMessage;

/**
 * Tabbed pane storing visual representation (as JTable) of selected components.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class TablePanel extends JTabbedPane
{

    private String lastPath;

    private String[][] oneDTabData;
    private String[] oneDTabColumnHeaders;
    private int oneDTabHeight = 0;
    private JPanel oneDPanel;
    private static final String ONE_D_COMPONENTS_TAB_NAME = "1D components";

    public void addTable(DataSet dataSet, boolean oneDTab)
    {

        final int MIN_COLUMN_WIDTH = 30;
        String[] rowHeaders;
        String[] columnHeaders;
        String[][] data = datasetToStringData(dataSet, dataSet.getFormat(), oneDTab);
        int width = data[0].length;
        int height = data.length;
        boolean oneDCase = oneDTab && dataSet.getDimentions().length == 1 && (oneDTabHeight == 0 || oneDTabHeight == height);
        if (oneDCase) {
            if (oneDTabData == null) {
                oneDTabHeight = height;
                oneDTabData = data;
                oneDTabColumnHeaders = new String[]{dataSet.getName()};
            } else {
                width = oneDTabData[0].length + 1;
                String[][] tempData = new String[oneDTabHeight][width];
                for (int i = 0; i < oneDTabHeight; i++) {
                    for (int j = 0; j < width - 1; j++) {
                        tempData[i][j] = oneDTabData[i][j];
                    }
                    tempData[i][width - 1] = data[i][0];
                }
                oneDTabData = tempData;
                data = oneDTabData;

                String[] tempColumnHeaders = new String[oneDTabColumnHeaders.length + 1];
                System.arraycopy(oneDTabColumnHeaders, 0, tempColumnHeaders, 0, oneDTabColumnHeaders.length);
                tempColumnHeaders[oneDTabColumnHeaders.length] = dataSet.getName();
                oneDTabColumnHeaders = tempColumnHeaders;
            }
            columnHeaders = oneDTabColumnHeaders;
            rowHeaders = new String[oneDTabHeight];
            for (int i = 0; i < oneDTabHeight; i++) {
                rowHeaders[i] = Integer.toString(i);
            }
        } else {
            columnHeaders = StringUtils.generateExcelColumnNames(width, true, 0);
            rowHeaders = new String[height];
            for (int i = 0; i < height; i++) {
                rowHeaders[i] = Integer.toString(i);
            }
        }
        ListModel<String> rowHeadersListModel = new RowHeaderListModel(rowHeaders);
        DefaultTableModel tableModel = new DefaultTableModel(height, width);
        tableModel.setDataVector(data, columnHeaders);

        JTable table = new JTable(tableModel)
        {
            DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

            {
                renderRight.setHorizontalAlignment(SwingConstants.RIGHT);
            }

            DefaultTableCellRenderer headerRenderer = new DefaultTableCellRenderer();

            @Override
            public TableCellRenderer getCellRenderer(int arg0, int arg1)
            {
                return renderRight;
            }

            @Override
            public boolean editCellAt(int row, int column, java.util.EventObject e)
            {
                return false;
            }

        };
        table.setFont(new Font(Font.DIALOG_INPUT, Font.BOLD, 12));
        table.setPreferredScrollableViewportSize(new Dimension(500, 100));
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setCellSelectionEnabled(true);
        table.changeSelection(0, 1, false, false);

        TableColumnAdjuster tca = new TableColumnAdjuster(table);
        tca.adjustColumns();

        JList<String> rowHeaderList = new JList<>(rowHeadersListModel);
        rowHeaderList.setFixedCellWidth(FastMath.max(MIN_COLUMN_WIDTH, String.valueOf(height).length() * 15));
        rowHeaderList.setFixedCellHeight(table.getRowHeight());
        rowHeaderList.setCellRenderer(new RowHeaderRenderer(table));

        JScrollPane scrollPane = new JScrollPane(table, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setPreferredSize(new Dimension(100, 100));
        scrollPane.setRowHeaderView(rowHeaderList);

        if (oneDCase == true) {
            oneDPanel = new JPanel();
            oneDPanel.setLayout(new BoxLayout(oneDPanel, BoxLayout.PAGE_AXIS));
            oneDPanel.add(Box.createRigidArea(new Dimension(0, 5)));
            oneDPanel.add(scrollPane);
            int idx = findTabByName(ONE_D_COMPONENTS_TAB_NAME);
            if (idx >= 0) {
                removeTabAt(idx);
            }
            addTab(ONE_D_COMPONENTS_TAB_NAME, null, oneDPanel);
            setSelectedIndex(findTabByName(ONE_D_COMPONENTS_TAB_NAME));
        } else {
            JPanel panel = new JPanel();
            panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
            panel.add(Box.createRigidArea(new Dimension(0, 5)));
            panel.add(scrollPane);
            addTab(dataSet.getName(), null, panel);
            setSelectedIndex(findTabByName(dataSet.getName()));
        }

    }

    public void updateTable(DataSet dataSet, boolean oneDTab)
    {

        String[][] data = datasetToStringData(dataSet, dataSet.getFormat(), oneDTab);
        int width = data[0].length;
        int height = data.length;
        boolean oneDCase = oneDTab && dataSet.getDimentions().length == 1 && oneDPanel != null && oneDTabData != null && oneDTabHeight == height;
        if (oneDCase == true) {
            width = oneDTabData[0].length + 1;
            String[][] tempData = new String[oneDTabHeight][width];
            for (int i = 0; i < oneDTabHeight; i++) {
                for (int j = 0; j < width - 1; j++) {
                    tempData[i][j] = oneDTabData[i][j];
                }
                tempData[i][width - 1] = data[i][0];
            }
            oneDTabData = tempData;

            String[] tempColumnHeaders = new String[oneDTabColumnHeaders.length + 1];
            System.arraycopy(oneDTabColumnHeaders, 0, tempColumnHeaders, 0, oneDTabColumnHeaders.length);
            tempColumnHeaders[oneDTabColumnHeaders.length] = dataSet.getName();
            oneDTabColumnHeaders = tempColumnHeaders;
            JScrollPane scrollPane = (JScrollPane) oneDPanel.getComponent(1);
            JTable table = (JTable) scrollPane.getViewport().getView();
            DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
            tableModel.setDataVector(oneDTabData, oneDTabColumnHeaders);
            table.setModel(tableModel);
            TableColumnAdjuster tca = new TableColumnAdjuster(table);
            tca.adjustColumns();
        } else {
            int tabIdx;
            if ((tabIdx = findTabByName(dataSet.getName())) >= 0) {
                JPanel panel = (JPanel) getComponentAt(tabIdx);
                JScrollPane scrollPane = (JScrollPane) panel.getComponent(1);
                JTable table = (JTable) scrollPane.getViewport().getView();
                DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
                String[] columnHeaders = StringUtils.generateExcelColumnNames(data[0].length, true, 0);
                tableModel.setDataVector(data, columnHeaders);
                table.setModel(tableModel);
                TableColumnAdjuster tca = new TableColumnAdjuster(table);
                tca.adjustColumns();
            }
        }
    }

    public void saveTables(Map<String, DataSet> series, String[] names, String format)
    {
        FileNameExtensionFilter filter = null;
        if (format.equalsIgnoreCase("csv")) {
            filter = new FileNameExtensionFilter("CSV files (*.csv)", "csv", "CSV");
        } else if (format.equalsIgnoreCase("hdf5")) {
            filter = new FileNameExtensionFilter("HDF5 files", "h5", "H5");
        } else {
            return;
        }
        JFileChooser fileChooser = new JFileChooser();
        if (lastPath == null) {
            fileChooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getUsableDataPath(TablePanel.class
            )));
        } else {
            fileChooser.setCurrentDirectory(new File(lastPath));
        }
        fileChooser.addChoosableFileFilter(filter);
        fileChooser.setLocation(0, 0);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setFileFilter(filter);
        fileChooser.setMultiSelectionEnabled(false);
        int returnVal = fileChooser.showSaveDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            String path = VNFileChooser.filenameWithExtenstionAddedIfNecessary(fileChooser.getSelectedFile(), filter);
            lastPath = path.substring(0, path.lastIndexOf(File.separator));
            VisNow.get().getMainConfig().setLastDataPath(lastPath, TablePanel.class
            );
            List<String> namesList = Arrays.asList(names);
            Map<String, DataSet> outSeries = series.entrySet().stream().
                filter(x -> namesList.contains(x.getValue().getName())).
                collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue()));
            if (format.equalsIgnoreCase("csv")) {
                saveTablesAsCVS(outSeries, path);
            } else if (format.equalsIgnoreCase("hdf5")) {
                saveTablesAsHDF5(outSeries, path);
            }
        }
    }

    private static String addSuffixToFileName(String path, String suffix)
    {
        String newPath = suffix.isEmpty() ? path : FilenameUtils.removeExtension(path) + "_" + suffix + "." + FilenameUtils.getExtension(path);
        File f = new File(newPath);
        int count = 1;
        while (f.exists()) {
            newPath = FilenameUtils.removeExtension(path) + (!suffix.isEmpty() ? "_" + suffix : "") + "(" + count + ")." + FilenameUtils.getExtension(path);
            f = new File(newPath);
            count++;
        }
        return newPath;
    }

    private static void saveTablesAsCVS(Map<String, DataSet> series, String path)
    {
        for (DataSet dataSet : series.values()) {
            String outPath = addSuffixToFileName(path, series.size() > 1 ? dataSet.getName() : "");
            DataArray da = DataArray.create(dataSet.getData(), dataSet.getVectorLength(), dataSet.getName());
            int[] dimsi = dataSet.getDimentions();
            long[] dims = new long[dimsi.length];
            for (int i = 0; i < dims.length; i++) {
                dims[i] = dimsi[i];
            }
            if (CSVWriterCore.writeCsvDataArray(da, dims, null, null, outPath, ",") == false) {
                VisNow.get().userMessageSend(new UserMessage("VisNow", "", "Error writing CSV file " + outPath, "Only 1D and 2D regular fields with scalar components are supported.", Level.ERROR));
            } else {
                VisNow.get().userMessageSend(new UserMessage("VisNow", "", "CSV file " + outPath + " written sucessfully.", "", Level.INFO));
            }
        }
    }

    private static void saveTablesAsHDF5(Map<String, DataSet> series, String path)
    {
        int[] dims = null;
        boolean differentDims = false;
        for (DataSet dataSet : series.values()) {
            if (dims == null) {
                dims = dataSet.getDimentions();
            } else {
                if (!Arrays.equals(dims, dataSet.getDimentions())) {
                    differentDims = true;
                    break;
                }
            }
        }
        if (differentDims) {
            for (DataSet dataSet : series.values()) {
                String outPath = addSuffixToFileName(path, series.size() > 1 ? dataSet.getName() : "");
                DataArray da = DataArray.create(dataSet.getData(), dataSet.getVectorLength(), dataSet.getName());
                RegularField field = new RegularField(dataSet.getDimentions());
                field.addComponent(da);
                if (HDF5WriterCore.writeField(field, outPath, false) == false) {
                    VisNow.get().userMessageSend(new UserMessage("VisNow", "", "Error writing HDF5 file " + outPath, "", Level.ERROR));
                } else {
                    VisNow.get().userMessageSend(new UserMessage("VisNow", "", "HDF5 file " + outPath + " written sucessfully.", "", Level.INFO));
                }
            }
        } else {
            ArrayList<DataArray> components = new ArrayList<>(series.size());
            for (DataSet dataSet : series.values()) {
                components.add(DataArray.create(dataSet.getData(), dataSet.getVectorLength(), dataSet.getName()));
            }
            RegularField field = new RegularField(dims);
            field.setComponents(components);
            if (HDF5WriterCore.writeField(field, path, false) == false) {
                VisNow.get().userMessageSend(new UserMessage("VisNow", "", "Error writing HDF5 file " + path, "", Level.ERROR));
            } else {
                VisNow.get().userMessageSend(new UserMessage("VisNow", "", "HDF5 file " + path + " written sucessfully.", "", Level.INFO));
            }
        }
    }

    public void removeAllTables()
    {
        removeAll();
        oneDTabData = null;
    }

    public String getSelectedTabName()
    {
        int idx = getSelectedIndex();
        if (idx >= 0) {
            return getTitleAt(idx);
        } else {
            return "";
        }
    }

    public void setSelectedTabByName(String name)
    {
        int idx = findTabByName(name);
        if (idx >= 0) {
            setSelectedIndex(idx);
        }
        else {
            setSelectedIndex(0);
        }
    }

    private int findTabByName(String title)
    {
        int tabCount = getTabCount();
        for (int i = 0; i < tabCount; i++) {
            String tabTitle = getTitleAt(i);
            if (tabTitle.equals(title)) return i;
        }
        return -1;

    }

    private static class RowHeaderListModel extends AbstractListModel<String>
    {

        private final String[] rowHeaders;

        public RowHeaderListModel(String[] rowHeaders)
        {
            this.rowHeaders = rowHeaders;
        }

        @Override
        public int getSize()
        {
            return rowHeaders.length;
        }

        @Override
        public String getElementAt(int index)
        {
            return rowHeaders[index];
        }
    }

    private static class RowHeaderRenderer extends JLabel implements ListCellRenderer<String>
    {

        RowHeaderRenderer(JTable table)
        {
            JTableHeader header = table.getTableHeader();
            setOpaque(true);
            setBorder(UIManager.getBorder("TableHeader.cellBorder"));
            setHorizontalAlignment(CENTER);
            setForeground(header.getForeground());
            setBackground(header.getBackground());
            setFont(header.getFont());
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends String> jlist, String e, int i, boolean bln, boolean bln1)
        {
            setText((e == null) ? "" : e);
            return this;
        }
    }

    private static String[][] datasetToStringData(DataSet dataset, String format, boolean oneDTab)
    {
        LargeArray data = dataset.getData();
        LargeArrayType type = data.getType();
        int vlen = dataset.getVectorLength();
        int[] dims = dataset.getDimentions();
        final String[][] stringData;
        if (dims.length == 2) {
            stringData = new String[dims[1]][dims[0]];
            int nthreads = (int) FastMath.min(dims[1], ConcurrencyUtils.getNumberOfThreads());
            Future[] futures = new Future[nthreads];
            int k = dims[1] / nthreads;
            for (int t = 0; t < nthreads; t++) {
                final int firstIdx = t * k;
                final int lastIdx = (t == nthreads - 1) ? dims[1] : firstIdx + k;
                if (!type.isComplexNumericType()) {
                    futures[t] = ConcurrencyUtils.submit(() -> {
                        String[] elem = new String[vlen];
                        for (int i = firstIdx; i < lastIdx; i++) {
                            for (int j = 0; j < dims[0]; j++) {
                                for (int v = 0; v < vlen; v++) {
                                    elem[v] = String.format(format, data.get(i * dims[0] * vlen + j * vlen + v));
                                }
                                if (vlen > 1) {
                                    stringData[i][j] = Arrays.toString(elem);
                                } else {
                                    stringData[i][j] = elem[0];
                                }
                            }
                        }
                    });
                } else {
                    futures[t] = ConcurrencyUtils.submit(() -> {
                        String[] elem = new String[vlen];
                        for (int i = firstIdx; i < lastIdx; i++) {
                            for (int j = 0; j < dims[0]; j++) {
                                for (int v = 0; v < vlen; v++) {
                                    elem[v] = StringUtils.complexToString((float[]) data.get(i * dims[0] * vlen + j * vlen + v), format);
                                }
                                if (vlen > 1) {
                                    stringData[i][j] = Arrays.toString(elem);
                                } else {
                                    stringData[i][j] = elem[0];
                                }
                            }
                        }
                    });

                }
            }
            try {
                ConcurrencyUtils.waitForCompletion(futures);
            } catch (InterruptedException | ExecutionException ex) {
                // don't do anything - return incomplete stringData
            }

        } else if (oneDTab == false) {
            stringData = new String[1][dims[0]];
            int nthreads = (int) FastMath.min(dims[0], ConcurrencyUtils.getNumberOfThreads());
            Future[] futures = new Future[nthreads];
            int k = dims[0] / nthreads;
            for (int t = 0; t < nthreads; t++) {
                final int firstIdx = t * k;
                final int lastIdx = (t == nthreads - 1) ? dims[0] : firstIdx + k;
                if (!type.isComplexNumericType()) {
                    futures[t] = ConcurrencyUtils.submit(() -> {
                        String[] elem = new String[vlen];
                        for (int i = firstIdx; i < lastIdx; i++) {
                            for (int v = 0; v < vlen; v++) {
                                elem[v] = String.format(format, data.get(i * vlen + v));
                            }
                            if (vlen > 1) {
                                stringData[0][i] = Arrays.toString(elem);
                            } else {
                                stringData[0][i] = elem[0];
                            }
                        }
                    });
                } else {
                    futures[t] = ConcurrencyUtils.submit(() -> {
                        String[] elem = new String[vlen];
                        for (int i = firstIdx; i < lastIdx; i++) {
                            for (int v = 0; v < vlen; v++) {
                                elem[v] = StringUtils.complexToString((float[]) data.get(i * vlen + v), format);
                            }
                            if (vlen > 1) {
                                stringData[0][i] = Arrays.toString(elem);
                            } else {
                                stringData[0][i] = elem[0];
                            }
                        }
                    });
                }
            }
            try {
                ConcurrencyUtils.waitForCompletion(futures);
            } catch (InterruptedException | ExecutionException ex) {
                // don't do anything - return incomplete stringData
            }
        } else {
            stringData = new String[dims[0]][1];
            int nthreads = (int) FastMath.min(dims[0], ConcurrencyUtils.getNumberOfThreads());
            Future[] futures = new Future[nthreads];
            int k = dims[0] / nthreads;
            for (int t = 0; t < nthreads; t++) {
                final int firstIdx = t * k;
                final int lastIdx = (t == nthreads - 1) ? dims[0] : firstIdx + k;
                if (!type.isComplexNumericType()) {
                    futures[t] = ConcurrencyUtils.submit(() -> {
                        String[] elem = new String[vlen];
                        for (int i = firstIdx; i < lastIdx; i++) {
                            for (int v = 0; v < vlen; v++) {
                                elem[v] = String.format(format, data.get(i * vlen + v));
                            }
                            if (vlen > 1) {
                                stringData[i][0] = Arrays.toString(elem);
                            } else {
                                stringData[i][0] = elem[0];
                            }
                        }
                    });
                } else {
                    futures[t] = ConcurrencyUtils.submit(() -> {
                        String[] elem = new String[vlen];
                        for (int i = firstIdx; i < lastIdx; i++) {
                            for (int v = 0; v < vlen; v++) {
                                elem[v] = StringUtils.complexToString((float[]) data.get(i * vlen + v), format);
                            }
                            if (vlen > 1) {
                                stringData[i][0] = Arrays.toString(elem);
                            } else {
                                stringData[i][0] = elem[0];
                            }
                        }
                    });
                }
            }
            try {
                ConcurrencyUtils.waitForCompletion(futures);
            } catch (InterruptedException | ExecutionException ex) {
                // don't do anything - return incomplete stringData
            }
        }
        return stringData;
    }
}
