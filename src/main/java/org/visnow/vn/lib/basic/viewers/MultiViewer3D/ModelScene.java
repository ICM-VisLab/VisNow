/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.MultiViewer3D;

import org.jogamp.java3d.utils.behaviors.mouse.MouseRotate;
import java.awt.Color;
import java.util.ArrayList;
import java.util.SortedSet;
import org.jogamp.java3d.Background;
import org.jogamp.java3d.BoundingSphere;
import org.jogamp.java3d.BranchGroup;
import org.jogamp.java3d.LinearFog;
import org.jogamp.java3d.Transform3D;
import org.jogamp.java3d.TransformGroup;
import org.jogamp.vecmath.Color3f;
import org.jogamp.vecmath.Matrix3d;
import org.jogamp.vecmath.Matrix3f;
import org.jogamp.vecmath.Point3d;
import org.jogamp.vecmath.Vector3d;
import org.jogamp.vecmath.Vector3f;
import org.visnow.vn.geometries.objects.GeometryObject;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.viewer3d.lights.DirectionalLightObject;
import org.visnow.vn.geometries.viewer3d.lights.EditableAmbientLight;
import org.visnow.vn.geometries.viewer3d.lights.LightsInitializers;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw, Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 */
class ModelScene
{

    protected GeometryObject rootObject = new GeometryObject("root_object");
    protected TransformGroup objScale = new TransformGroup();
    protected TransformGroup objRotate = new TransformGroup();
    protected TransformGroup objProjectionSwitch = new TransformGroup();
    protected TransformGroup objTranslate = new TransformGroup();
    protected BoundingSphere bounds = new BoundingSphere(new Point3d(0., 0., 0.), 1000.0);
    protected OpenBranchGroup objRoot = new OpenBranchGroup();
    protected OpenBranchGroup objMain = new OpenBranchGroup();
    protected EditableAmbientLight ambientLight = null;
    protected ArrayList<DirectionalLightObject> directionalLights = new ArrayList<>();
    protected float[][] rootExtents = new float[][]{{-1, -1, -1}, {1, 1, 1}};
    protected double externScale = 1;
    protected double mouseScale = 1;
    protected double scale = 1;
    protected double mouseWheelSensitivity = 1.02;
    protected Vector3d sceneCenter = new Vector3d(0., 0., 0.);
    protected Transform3D sceneNorm = new Transform3D();
    protected Background bg = new Background();
    protected LinearFog myFog = new LinearFog();
    protected OpenBranchGroup myFogGroup = new OpenBranchGroup();
    protected Color3f bgColor = new Color3f(0.f, 0.f, 0.f);
    protected Matrix3f axesSwitchingMatrix = new Matrix3f();

    public ModelScene()
    {
        objTranslate.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        objTranslate.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        objProjectionSwitch.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        objProjectionSwitch.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        objRotate.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        objScale.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        objScale.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        objMain.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
        objMain.setCapability(BranchGroup.ALLOW_CHILDREN_READ);
        objMain.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
        objTranslate.addChild(objMain);
        objScale.addChild(objTranslate);
        bg.setCapability(Background.ALLOW_COLOR_WRITE);
        bg.setApplicationBounds(bounds);
        myFog.setCapability(LinearFog.ALLOW_DISTANCE_WRITE);
        myFog.setCapability(LinearFog.ALLOW_COLOR_WRITE);
        myFog.setInfluencingBounds(bounds);
        myFog.setFrontDistance(1.);
        myFog.setBackDistance(4.);
        myFogGroup.addChild(myFog);
        objScale.addChild(bg);
        objProjectionSwitch.addChild(objScale);
        objRotate.addChild(objProjectionSwitch);
        objRoot.addChild(objRotate);

        ambientLight = new EditableAmbientLight(new Color3f(.25f, .25f, .25f), bounds, "ambient light", true);
        objRoot.addChild(ambientLight.getLight());
        LightsInitializers.createDirectionalLights(bounds, objMain, directionalLights);
        MouseRotate mouseRotate = new MouseRotate(objRotate);
        mouseRotate.setFactor(.01, .01);
        mouseRotate.setSchedulingBounds(bounds);
        objRoot.addChild(mouseRotate);
        //      MouseTranslate mouseTranslate = new MouseTranslate(objTranslate);
        //      mouseTranslate.setSchedulingBounds(bounds);
        //      objRoot.addChild(mouseTranslate);
        objMain.addChild(rootObject.getGeometryObj());
    }

    public void clearAllGeometry()
    {
        SortedSet<GeometryObject> children = rootObject.getChildren();
        for (GeometryObject child : children) {
            child.setCurrentViewer(null);
            child.setParentGeom(null);
        }
        rootObject.clearAllGeometry();
    }

    public void multSwitchTransform(Transform3D sForm)
    {
        Transform3D switchTrans = new Transform3D();
        Transform3D currentTrans = new Transform3D();
        objProjectionSwitch.getTransform(currentTrans);
        switchTrans.mul(sForm, currentTrans);
        objProjectionSwitch.setTransform(switchTrans);
        currentTrans.invert(switchTrans);
        currentTrans.get(axesSwitchingMatrix);
    }

    public void setSwitchTransform(Transform3D sForm)
    {
        objProjectionSwitch.setTransform(sForm);
    }

    public void updateExtents()
    {
        float dim = 0;
        rootExtents = rootObject.getExtents();
        for (int i = 0; i < 3; i++)
            if (dim < rootExtents[1][i] - rootExtents[0][i])
                dim = rootExtents[1][i] - rootExtents[0][i];
        if (dim == 0)
            dim = 1;
        sceneCenter = new Vector3d(-.6 * (rootExtents[1][0] + rootExtents[0][0]) / dim,
                                   -.6 * (rootExtents[1][1] + rootExtents[0][1]) / dim,
                                   -.6 * (rootExtents[1][2] + rootExtents[0][2]) / dim);
        setScale(1.2 / dim);
    }

    public void addGeomObject(GeometryObject obj)
    {
        rootObject.addChild(obj);
        updateExtents();
    }

    protected void updateScale()
    {
        sceneNorm = new Transform3D(new Matrix3d(1., 0., 0.,
                                                 0., 1., 0.,
                                                 0., 0., 1.),
                                    sceneCenter, externScale * mouseScale);
        objScale.setTransform(sceneNorm);
    }

    public void setScale(double scale)
    {
        externScale = scale;
        updateScale();
    }

    public void rescaleFromMouseWheel(java.awt.event.MouseWheelEvent evt)
    {
        int notches = evt.getWheelRotation();
        if (notches < 0)
            mouseScale /= mouseWheelSensitivity;
        else
            mouseScale *= mouseWheelSensitivity;
        updateScale();
    }

    public void setTranslate(float[] d)
    {
        Transform3D tr = new Transform3D();
        tr.setTranslation(new Vector3f(d));
        objTranslate.setTransform(tr);
    }

    public void reset()
    {
        objRotate.setTransform(new Transform3D());
        objTranslate.setTransform(new Transform3D());
        mouseScale = 1.;
        updateScale();
    }

    public void setBackgroundColor(Color c)
    {
        bgColor = new Color3f(c.getColorComponents(null));
        bg.setColor(bgColor);
        myFog.setColor(bgColor);
    }

    public GeometryObject getRootObject()
    {
        return rootObject;
    }

    public BranchGroup getSceneGraph()
    {
        return objRoot;
    }

    public BranchGroup getObjMain()
    {
        return objMain;
    }

    public TransformGroup getSceneRotateGroup()
    {
        return objRotate;
    }

    public Matrix3f getAxesSwitchingTransform()
    {
        return axesSwitchingMatrix;
    }
}
