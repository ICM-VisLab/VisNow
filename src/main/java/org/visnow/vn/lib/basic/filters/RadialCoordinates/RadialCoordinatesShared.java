/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.RadialCoordinates;

import org.visnow.vn.engine.core.ParameterName;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class RadialCoordinatesShared
{
    static final int CONSTANT_VALUE = -1;

    static final String MAP_SPHERICAL = "Spherical";
    static final String MAP_CYLINDRICAL = "Cylindrical";

    static final String[] MAP_TYPES = new String[]{
        MAP_SPHERICAL,
        MAP_CYLINDRICAL
    };

    //Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    //
    //Specification:
    
    //string from MAP_TYPES
    static final ParameterName<String> MAP_TYPE = new ParameterName("Map type");

    //CONSTANT_VALUE or [0 ... META_NUMBER_OF_DIMENSIONS-1] 
    static final ParameterName<Integer> RADIUS_AXIS = new ParameterName("Radius axis");
    static final ParameterName<Float> RADIUS_MIN = new ParameterName("Radius minimum");
    // >= RADIUS_MIN
    static final ParameterName<Float> RADIUS_MAX = new ParameterName("Radius maximum");
    //CONSTANT_VALUE or [0 ... META_NUMBER_OF_DIMENSIONS-1] 
    static final ParameterName<Integer> PHI_AXIS = new ParameterName("Phi axis");
    static final ParameterName<Float> PHI_MIN = new ParameterName("Phi minimum");
    // >= PHI_MIN
    static final ParameterName<Float> PHI_MAX = new ParameterName("Phi maximum");
    //CONSTANT_VALUE or [0 ... META_NUMBER_OF_DIMENSIONS-1] 
    static final ParameterName<Integer> PSI_AXIS = new ParameterName("Psi axis");
    static final ParameterName<Float> PSI_MIN = new ParameterName("Psi minimum");
    // >= PSI_MIN
    static final ParameterName<Float> PSI_MAX = new ParameterName("Psi maximum");
    //CONSTANT_VALUE or [0 ... META_NUMBER_OF_DIMENSIONS-1] 
    static final ParameterName<Integer> HEIGHT_AXIS = new ParameterName("Height axis");
    static final ParameterName<Float> HEIGHT_MIN = new ParameterName("Height minimum");
    // >= HEIGHT_MIN
    static final ParameterName<Float> HEIGHT_MAX = new ParameterName("Height maximum");
    static final ParameterName<Boolean> ADJUSTING = new ParameterName("Adjusting");
    static final ParameterName<Boolean> TRANSFORM_VECTORS = new ParameterName("Transform vectors");

    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic)    
    //integer: 1,2 or 3
    static final ParameterName<Integer> META_NUMBER_OF_DIMENSIONS = new ParameterName("META Number of dimensions");
}
