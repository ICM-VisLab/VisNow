//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.TableViewer;

import java.awt.Component;
import java.awt.Frame;
import org.visnow.vn.lib.basic.viewers.TableViewer.ExtendedTable.ExtendedTablePanel;
import org.visnow.vn.lib.basic.viewers.TableViewer.ExtendedTable.TableData;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.engine.core.Input;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.Inputs;
import org.visnow.vn.engine.core.Link;
import org.visnow.vn.engine.core.LinkFace;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.lib.basic.viewers.TableViewer.ExtendedTable.ExtendedTable;
import static org.visnow.vn.lib.basic.viewers.TableViewer.TableViewerShared.*;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.gui.utils.ExtendedMenuItem;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;
import org.visnow.vn.gui.utils.RemoveComponentActionListener;
import org.visnow.vn.gui.widgets.VisNowFrame;
import org.visnow.vn.lib.gui.SimpleOneButtonGUI;

/**
 * Table viewer
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 * @author norkap
 */
public class TableViewer extends ModuleCore implements RemoveComponentActionListener
{

    protected ArrayList<ExtendedTable> tables;
    private VisNowFrame frame;
    private TableViewerInternalFrame internalFrame;
    private SimpleOneButtonGUI ui = null;
    private boolean seriesChanged = false;
    private Map<String, String> currentConnectedComponents;
    private Map<String, String> previousConnectedComponents;
    private final boolean ONE_D_TAB_DEFAULT = true;

    public TableViewer()
    {
        this.tables = new ArrayList<>();
        this.previousConnectedComponents = new HashMap<>();
        this.currentConnectedComponents = new HashMap<>();

        parameters.addParameterChangelistener((String name) -> {
            if (name.equals(SERIES.getName())) {
                seriesChanged = true;
                startAction();
            } else {
                seriesChanged = false;
            }
        });

        SwingInstancer.swingRunAndWait(() -> {

            frame = new VisNowFrame();
            internalFrame = new TableViewerInternalFrame(frame, ONE_D_TAB_DEFAULT);
            frame.add(internalFrame);
            frame.setTitle("VisNow Table Viewer");

            ExtendedTablePanel tablePanel = new ExtendedTablePanel();
            tablePanel.addRemoveTableListener(getInstance());
            TableData data = new TableData();

            ExtendedTable table = new ExtendedTable(data, tablePanel, internalFrame.getOneDTab());
            tables.add(table);
            internalFrame.addTableActionListener(new AddTableListener());
            internalFrame.addOneDTabActionListener(new OneDTabListener());
            internalFrame.addTableToDisplay(tablePanel);
            internalFrame.pack();
            internalFrame.setVisible(true);

            ui = new SimpleOneButtonGUI();
            ui.addChangeListener(new ChangeListener()
            {
                @Override
                public void stateChanged(ChangeEvent evt)
                {
                    frame.setVisible(true);
                    frame.setExtendedState(Frame.NORMAL);
                }
            });
            setPanel(ui);

            frame.pack();
            frame.setVisible(true);

        });

    }

    private TableViewer getInstance()
    {
        return this;
    }

    public TableViewerInternalFrame getInternalFrame()
    {
        return this.internalFrame;
    }

    public VisNowFrame getFrame()
    {
        return this.frame;
    }

    public static InputEgg[] inputEggs = null;

    @Override
    public void onActive()
    {
        Parameters p = parameters.getReadOnlyClone();

        if (seriesChanged) {

            Map<String, DataSet> series = p.get(SERIES);

            for (ExtendedTable table : tables) {
                table.setSeries(series);
                table.update();
            }
            seriesChanged = false;
        } else {
            Inputs moduleInputs = getInputs();
            Iterator it = moduleInputs.iterator();

            if (it.hasNext()) {

                previousConnectedComponents = new HashMap<>(currentConnectedComponents.size());
                this.currentConnectedComponents.entrySet().forEach(entry -> {
                    String key = entry.getKey();
                    String val = entry.getValue();
                    previousConnectedComponents.put(key, val);
                });
                currentConnectedComponents = new HashMap<>();
                Map<String, DataSet> series = new LinkedHashMap<>();
                Vector<Link> links = ((Input) it.next()).getLinks();
                for (Link link : links) {
                    String moduleName = link.getName().getOutputModule();
                    VNRegularField field = (VNRegularField) link.getOutput().getData().getValue();

                    if (field != null) {
                        RegularField inFld = ((VNRegularField) field).getField();
                        if (inFld.getDimNum() == 3) {
                            VisNow.get().userMessageSend(this, "3D fields are not supported", "", Level.ERROR);
                        } else {
                            updateSeries(series, inFld, moduleName);
                        }
                    }
                }
                if (!currentConnectedComponents.equals(previousConnectedComponents)) {
                    parameters.set(SERIES, series);
                }
            }
        }
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(SERIES, new LinkedHashMap<>())
        };
    }

    private void setSeries(Map<String, DataSet> series)
    {
        parameters.set(SERIES, series);
    }

    @Override
    public void onInputAttach(LinkFace link)
    {

        VNRegularField field = (VNRegularField) link.getOutput().getData().getValue();
        String moduleName = link.getOutput().getModuleBox().getName();

        if (field == null)
            return;

        Map<String, DataSet> series = parameters.get(SERIES);
        RegularField inFld = ((VNRegularField) field).getField();
        if (inFld.getDimNum() == 3) {
            VisNow.get().userMessageSend(this, "3D fields are not supported", "", Level.ERROR);
            return;
        }
        boolean daAdded = updateSeries(series, inFld, moduleName);
        if (daAdded) {
            setSeries(series);
        }
    }

    @Override
    public void onInputDetach(LinkFace link)
    {

        String moduleName = link.getOutput().getModuleBox().getName();
        HashMap<String, String> newCurrentConnectedComponents = new HashMap<>();

        currentConnectedComponents.entrySet().forEach(entry -> {
            if ((entry.getKey()).split("&")[1].equals(moduleName)) {
                parameters.get(SERIES).remove(entry.getValue());
            } else {
                newCurrentConnectedComponents.put(entry.getKey(), entry.getValue());
            }
        });
        if (newCurrentConnectedComponents.size() != currentConnectedComponents.size()) {
            currentConnectedComponents = newCurrentConnectedComponents;
            parameters.fireParameterChanged(SERIES.getName());
        }
    }

    @Override
    public void removeComponent(Component comp)
    {
        int idx = internalFrame.getComponentIndex(comp);
        tables.remove(idx);
        internalFrame.removeTableFromDisplay((ExtendedTablePanel) comp);
        showLastComponentManagerContainer();
    }

    private void addTable()
    {
        ExtendedTablePanel tablePanel = new ExtendedTablePanel();
        tablePanel.addRemoveTableListener(this);
        TableData data = new TableData();
        ExtendedTable table = new ExtendedTable(data, tablePanel, internalFrame.getOneDTab());

        Parameters p = parameters.getReadOnlyClone();
        table.setSeries(p.get(SERIES));
        table.update();

        tables.add(table);
        internalFrame.addTableToDisplay(tablePanel);
        showLastComponentManagerContainer();
    }

    private void showLastComponentManagerContainer()
    {
        boolean show;
        for (int i = 0; i < tables.size(); i++) {
            show = (i == (tables.size() - 1));
            tables.get(i).getTablePanel().showComponentsList(show);
        }
    }

    class RemoveTableListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (e.getSource() instanceof ExtendedMenuItem) {
                ExtendedMenuItem mItem = (ExtendedMenuItem) e.getSource();
                removeComponent(SwingUtilities.getAncestorOfClass(ExtendedTablePanel.class, mItem.getAncestor()));
            }
        }

    }

    class AddTableListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            addTable();
        }

    }

    class OneDTabListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            AbstractButton aButton = (AbstractButton) e.getSource();
            boolean oneDTab = aButton.getModel().isSelected();
            for (ExtendedTable table : tables) {
                table.setOneDTab(oneDTab);
            }
        }

    }

    @Override
    public void onInitFinished()
    {
        updateFrameName();
    }

    @Override
    public void onNameChanged()
    {
        updateFrameName();
    }

    private void updateFrameName()
    {
        if (frame != null)
            frame.setTitle("VisNow Table Viewer - " + this.getApplication().getTitle() + " - " + this.getName());
    }

    @Override
    public void onDelete()
    {
        frame.dispose();
    }

    private boolean updateSeries(Map<String, DataSet> series, RegularField inFld, String moduleName)
    {
        boolean daAdded = false;
        for (int i = 0; i < inFld.getNComponents(); i++) {
            DataArray da = inFld.getComponent(i);
            String name = da.getName();
            int j = 2;
            String temp_name = name;
            while (series.containsKey(name)) {
                name = temp_name + j;
                j++;
            }
            series.put(name, TableViewerCore.createSeries(da, inFld, name));
            currentConnectedComponents.put(da.getName() + "&" + moduleName, name);
            daAdded = true;
        }
        return daAdded;
    }

}
