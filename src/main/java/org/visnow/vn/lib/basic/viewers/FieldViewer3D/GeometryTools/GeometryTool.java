/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.GeometryTools;

import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry.CalculableParameter;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public abstract class GeometryTool implements MouseMotionListener, MouseListener
{

    protected Metadata metadata = null;

    public abstract void paint(Graphics g);

    public abstract Cursor getCursor();

    public abstract boolean isMouseWheelBlocking();

    protected boolean holding = false;

    public boolean isHolding()
    {
        return holding;
    }

    public abstract int[][] getPoints();

    public abstract Metadata[] getPointMetadata();

    public abstract int[][] getConnections();

    public abstract CalculableParameter getCalculable();

    public abstract Metadata getCalculableMetadata();

    public abstract int getMinimumNPoints();

    public void setMetadata(Metadata metadata)
    {
        this.metadata = metadata;
    }

    public Metadata getMetadata()
    {
        return metadata;
    }

    private transient ArrayList<GeometryToolListener> changeListenerList = new ArrayList<GeometryToolListener>();

    /**
     * Registers GeometryToolListener to receive events.
     * <p>
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(GeometryToolListener listener)
    {
        if (!changeListenerList.contains(listener))
            changeListenerList.add(listener);
    }

    /**
     * Removes GeometryToolListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(GeometryToolListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param object Parameter #1 of the <CODE>ChangeEvent<CODE> constructor.
     */
    protected void fireGeometryToolStateChanged()
    {
        ChangeEvent e = new ChangeEvent(this);
        //      for (GeometryToolListener listener : changeListenerList) {
        //         listener.onGeometryToolStateChanged(e);
        //      }
        for (int i = 0; i < changeListenerList.size(); i++) {
            changeListenerList.get(i).onGeometryToolStateChanged(e);
        }
    }

    protected void fireGeometryToolRepaintNeeded()
    {
        for (GeometryToolListener listener : changeListenerList) {
            listener.onGeometryToolRepaintNeeded();
        }
    }

}
