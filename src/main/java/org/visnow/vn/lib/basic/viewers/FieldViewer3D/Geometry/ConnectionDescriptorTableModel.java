/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.FieldViewer3D.Geometry;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ConnectionDescriptorTableModel extends AbstractTableModel
{

    private ArrayList<ConnectionDescriptor> cons = new ArrayList<ConnectionDescriptor>();
    private GeometryParams params = null;

    public ConnectionDescriptorTableModel(GeometryParams params)
    {
        this.params = params;
        this.params.addGeometryParamsListener(new GeometryParamsListener()
        {
            @Override
            public void onGeometryParamsChanged(GeometryParamsEvent e)
            {
                if (e.getType() == GeometryParamsEvent.TYPE_CONNECTION || e.getType() == GeometryParamsEvent.TYPE_POINT_MODIFIED || e.getType() == GeometryParamsEvent.TYPE_ALL) {
                    fireTableDataChanged();
                }
            }
        });

        this.cons = params.getConnectionDescriptors();
    }

    @Override
    public int getRowCount()
    {
        if (cons == null)
            return 0;

        return cons.size();
    }

    @Override
    public int getColumnCount()
    {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        if (cons == null)
            return null;

        switch (columnIndex) {
            case 0:
                return cons.get(rowIndex).getName();
            case 1:
                return cons.get(rowIndex).getP1().getName();
            case 2:
                return cons.get(rowIndex).getP2().getName();
            case 3:
                return (Float) cons.get(rowIndex).getLength();
        }
        return null;
    }

    @Override
    public String getColumnName(int columnIndex)
    {
        switch (columnIndex) {
            case 0:
                return "name";
            case 1:
                return "p1";
            case 2:
                return "p2";
            case 3:
                return "dist.";
            default:
                return "";
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        if (columnIndex < 3)
            return String.class;
        else
            return Float.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        return (columnIndex == 0);
    }

    @Override
    public void setValueAt(Object aValue, int row, int column)
    {
        if (column == 0) {
            if (!(aValue instanceof String))
                return;
            String tmp = (String) aValue;
            cons.get(row).setName(tmp);
        }
        fireTableCellUpdated(row, column);
    }

}
