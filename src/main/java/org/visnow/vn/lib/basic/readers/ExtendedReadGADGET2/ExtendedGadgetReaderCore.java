/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ExtendedReadGADGET2;

import java.util.HashMap;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.RegularField;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.system.main.VisNow;
import static org.visnow.vn.lib.basic.readers.ExtendedReadGADGET2.ExtendedGadgetReaderShared.*;


//TODO: proper java doc 
//TODO: common "Core" interface
//TODO: Core classes should have also full knowledge about input/output port names/types - if they're going to be used as "batch" version of module.
/**
 * Core that provides main computational functionality: processing input
 * parameters and creating output fields.
 *
 * @author szpak
 */
public class ExtendedGadgetReaderCore
{

    final private ExtendedReadGadgetData extendedReadGadgetData;

    final private Parameters params;

    private RegularField outDensityField;
    private IrregularField outField;

    //FIXME: no field schema testing ... modules may be incorrectly connected now (in Unicore wizard).
    public final static String OUT_IFIELD_MAIN = "outField";
    public final static String OUT_RFIELD_DENSITY = "outDensityField";

    public ExtendedGadgetReaderCore(Parameters params)
    {
        this.params = params;
        extendedReadGadgetData = new ExtendedReadGadgetData();
    }

    public static boolean[] getReadMask(Parameters params)
    {
        boolean[] out = new boolean[7];
        out[0] = params.get(READ_VELOCITY);
        out[1] = params.get(READ_ID);
        out[2] = params.get(READ_TYPE);
        out[3] = params.get(READ_MASS);
        out[4] = params.get(READ_ENERGY);
        //out[5] = isReadDensity();
        out[5] = false;
        out[6] = params.get(READ_TEMPERATURE);
        return out;
    }


    //TODO: boolean flag can be used to mark failures
    /**
     * Processes params to create outField and outDensityField. On failure, null
     * fields are set.
     */
    public void recalculate()
    {
        //reset out fields
        outDensityField = null;
        outField = null;

        //if file exists
        if (params.get(FILE_PATHS).length > 0) {
            extendedReadGadgetData.read(
                params.get(FILE_PATHS),
                getReadMask(params),
                params.get(DOWNSIZE),
                params.get(DENSITY_FIELD_DIMS),
                params.get(DENSITY_FIELD_LOG),
                null,
                VisNow.getMemoryFree() / 3
            );

            outField = extendedReadGadgetData.getField();
            if (outField != null) {
                outDensityField = extendedReadGadgetData.getDensityField();
            }
        }
    }

    public static String[] getInFieldNames()
    {
        return new String[]{};
    }

    public static String[] getOutFieldNames()
    {
        return new String[]{OUT_IFIELD_MAIN, OUT_RFIELD_DENSITY};
    }

    public Field getOutField(String name)
    {
        if (name.equals(OUT_IFIELD_MAIN))
            return outField;
        else if (name.equals(OUT_RFIELD_DENSITY))
            return outDensityField;
        else
            throw new IllegalArgumentException("Incorrect field name: " + name);
    }
}
