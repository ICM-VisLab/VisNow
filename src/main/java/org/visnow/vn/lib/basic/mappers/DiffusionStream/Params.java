/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.mappers.DiffusionStream;

import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class Params extends Parameters
{

    private static final String COMP = "vectorComponent";
    private static final String DOWN = "down";
    private static final String DOWNSIZE = "downsize";
    private static final String STEP = "step";
    private static final String FWD = "forwardSteps";
    private static final String THR = "nThreads";
    private static final String MULTI = "multiplicity";
    private static final String DIFF = "diffusion";
    private static final String FULL_COMPUTE = "compute diffusion";

    private boolean dowsizeChanged = true;
    private static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Integer>(COMP, ParameterType.dependent, -1),
        new ParameterEgg<int[]>(DOWN, ParameterType.dependent, null),
        new ParameterEgg<Integer>(MULTI, ParameterType.dependent, 100),
        new ParameterEgg<Integer>(DOWNSIZE, ParameterType.dependent, 100),
        new ParameterEgg<Float>(STEP, ParameterType.dependent, .002f),
        new ParameterEgg<Float>(DIFF, ParameterType.dependent, .002f),
        new ParameterEgg<Integer>(FWD, ParameterType.dependent, 500),
        new ParameterEgg<Integer>(THR, ParameterType.dependent,
                                  org.visnow.vn.system.main.VisNow.availableProcessors()),
        new ParameterEgg<Boolean>(FULL_COMPUTE, ParameterType.dependent, false)
    };

    public Params()
    {
        super(eggs);
        setValue(DOWN, new int[]{5, 5, 5});
    }

    public int getDownsize()
    {
        return (Integer) getValue(DOWNSIZE);
    }

    public void setDownsize(int downsize)
    {
        if (downsize != getDownsize())
            dowsizeChanged = true;
        setValue(DOWNSIZE, downsize);
    }

    public int[] getDown()
    {
        return (int[]) getValue(DOWN);
    }

    public void setDown(int[] down)
    {
        int[] dwn = (int[]) getValue(DOWN);
        for (int i = 0; i < dwn.length; i++)
            if (dwn[i] != down[i])
                dowsizeChanged = true;
        setValue(DOWN, down);
    }

    /**
     * Get the value of nThreads
     *
     * @return the value of nThreads
     */
    public int getNThreads()
    {
        return (Integer) getValue(THR);
    }

    /**
     * Set the value of nThreads
     *
     * @param nThreads new value of nThreads
     */
    public void setNThreads(int nThreads)
    {
        setValue(THR, nThreads);
    }

    /**
     * Get the value of nForwardSteps
     *
     * @return the value of nForwardSteps
     */
    public int getNForwardSteps()
    {
        return (Integer) getValue(FWD);
    }

    /**
     * Set the value of nForwardSteps
     *
     * @param nForwardSteps new value of nForwardSteps
     */
    public void setNForwardSteps(int nForwardSteps)
    {
        setValue(FWD, nForwardSteps);
    }

    /**
     * Get the value of multiplicity
     *
     * @return the value of nForwardSteps
     */
    public int getMultiplicity()
    {
        return (Integer) getValue(MULTI);
    }

    /**
     * Set the value of multiplicity
     *
     * @param multiplicity new value of multiplicity
     */
    public void setMultiplicity(int multiplicity)
    {
        setValue(MULTI, multiplicity);
    }

    /**
     * Get the value of step
     *
     * @return the value of step
     */
    public float getStep()
    {
        return (Float) getValue(STEP);
    }

    /**
     * Set the value of step
     *
     * @param step new value of step
     */
    public void setStep(float step)
    {
        setValue(STEP, step);
    }

    /**
     * Get the value of diffusion coefficient
     *
     * @return the value of diffusion coefficient
     */
    public float getDiffCoeff()
    {
        return (Float) getValue(DIFF);
    }

    /**
     * Set the value of diffusion coefficient
     *
     * @param diffCoeff new value of diffusion coefficient
     */
    public void setDiffCoeff(float diffCoeff)
    {
        setValue(DIFF, diffCoeff);
    }

    /**
     * Get the value of vectorComponent
     *
     * @return the value of vectorComponent
     */
    public int getVectorComponent()
    {
        return (Integer) getValue(COMP);
    }

    /**
     * Set the value of vectorComponent
     *
     * @param vectorComponent new value of vectorComponent
     */
    public void setVectorComponent(int vectorComponent)
    {
        setValue(COMP, vectorComponent);
    }

    public boolean isDownsizeChanged()
    {
        return dowsizeChanged;
    }

    public void setDownsizeChanged(boolean dowsizeChanged)
    {
        this.dowsizeChanged = dowsizeChanged;
    }

    public boolean computeDiffusion()
    {
        return (Boolean) getValue(FULL_COMPUTE);
    }

    public void setComputeDiffusion(boolean computeDiffusion)
    {
        setValue(FULL_COMPUTE, computeDiffusion);
    }

}
