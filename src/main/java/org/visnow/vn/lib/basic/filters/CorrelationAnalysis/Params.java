/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.CorrelationAnalysis;

import org.visnow.vn.engine.core.ParameterEgg;
import org.visnow.vn.engine.core.ParameterType;
import org.visnow.vn.engine.core.Parameters;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class Params extends Parameters
{

    public static final int COMPONENTS_AS_VARIABLES = 3;
    public static final int ROWS_AS_VARIABLES = 2;
    public static final int COLUMNS_AS_VARIABLES = 1;
    protected int change = 0;
    protected static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Integer>("component", ParameterType.dependent, 0),
        new ParameterEgg<Integer>("variables", ParameterType.dependent, COMPONENTS_AS_VARIABLES),
        new ParameterEgg<Boolean>("correlations", ParameterType.dependent, true),};

    public Params()
    {
        super(eggs);
    }

    public int getComponent()
    {
        return (Integer) getValue("component");
    }

    public void setComponent(int component)
    {
        setValue("component", component);
        fireStateChanged();
    }

    public int getVariableType()
    {
        return (Integer) getValue("variables");
    }

    public void setVariableType(int type)
    {
        setValue("variables", type);
        fireStateChanged();
    }

    public boolean isCorrelation()
    {
        return (Boolean) getValue("correlations");
    }

    public void setCorrelations(boolean type)
    {
        setValue("correlations", type);
        fireStateChanged();
    }

}
