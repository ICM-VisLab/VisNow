/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.DropCoords;

import org.apache.log4j.Logger;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import static org.visnow.vn.lib.basic.filters.DropCoords.DropCoordsShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class DropCoords extends OutFieldVisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(DropCoords.class);
    
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    private GUI computeUI = null;
    protected RegularField inField = null;

    public DropCoords()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(ORIGIN, new float[]{0.f, 0.f, 0.f}),
            new Parameter<>(CELL_SIZES, new float[]{1.f, 1.f, 1.f}),
            new Parameter<>(META_FIELD_RANK, 1)
        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        parameters.set(META_FIELD_RANK, inField.getDimNum());
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        LOGGER.debug("fromVNA: " + isFromVNA());
        
        if (getInputFirstValue("inField") != null) {
            inField = ((VNRegularField) getInputFirstValue("inField")).getField();
            Parameters p;
            synchronized (parameters) {
                validateParamsAndSetSmart(false);
                p = parameters.getReadOnlyClone();
            }
            notifyGUIs(p, false, false);

            outRegularField = inField.cloneShallow();
            outRegularField.removeCoords();
            outField = outRegularField;

            float[][] affine = new float[4][3];
            for (int i = 0; i < affine.length; i++)
                for (int j = 0; j < affine[0].length; j++)
                    affine[i][j] = 0;
            float[] cell_extends = parameters.get(CELL_SIZES);
            float[] origin = parameters.get(ORIGIN);
            for (int i = 0; i < cell_extends.length; i++) {
                affine[i][i] = cell_extends[i];
                affine[3][i] = origin[i];
            }
            outRegularField.setAffine(affine);
            setOutputValue("outField", new VNRegularField(outRegularField));
            outField = outRegularField;
            prepareOutputGeometry();
            show();
        }
    }
}
