/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.Canny;

import org.visnow.jscic.dataarrays.DataArray;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 *
 * @author Andrzej Rutkowski (rudy@mat.uni.torun.pl)
 *
 */
public class CoreD extends CoreBase
{

    private double[] data;

    public CoreD(DataArray datain, int[] dims)
    {
        super(datain, dims);
    }

    @Override
    protected void initData()
    {
        if (dataArray.getType() == DataArrayType.FIELD_DATA_DOUBLE) {
            data = (double[]) dataArray.getRawArray().getData();
        } else {
            data = (double[]) dataArray.getRawDoubleArray().getData();
        }
    }

    @Override
    protected float calc3MagSqr(int x, int y, int z)
    {
        double[] in = data;
        int za = z * dims[0] * dims[1];
        int z1 = z == 0 ? 0 : za - dims[0] * dims[1];
        int z2 = z == dims[2] - 1 ? za : za + dims[0] * dims[1];
        int w = dims[0];
        int ya = y * w;
        int y1 = y == 0 ? 0 : ya - w;
        int y2 = y == dims[1] - 1 ? ya : ya + w;
        int x1 = x == 0 ? 0 : x - 1;
        int x2 = x == w - 1 ? w - 1 : x + 1;
        float dx, dy, dz;

        dx = (float) (in[z1 + y1 + x1] - in[z1 + y1 + x2] +
            2. * (in[z1 + ya + x1] - in[z1 + ya + x2]) +
            in[z1 + y2 + x1] - in[z1 + y2 + x2] +
            2. * (in[za + y1 + x1] - in[za + y1 + x2] +
            2. * (in[za + ya + x1] - in[za + ya + x2]) +
            in[za + y2 + x1] - in[za + y2 + x2]) +
            in[z2 + y1 + x1] - in[z2 + y1 + x2] +
            2. * (in[z2 + ya + x1] - in[z2 + ya + x2]) +
            in[z2 + y2 + x1] - in[z2 + y2 + x2]);
        dy = (float) (in[z1 + y1 + x1] + 2. * in[z1 + y1 + x] + in[z1 + y1 + x2] -
            in[z1 + y2 + x1] - 2. * in[z1 + y2 + x] - in[z1 + y2 + x2] +
            2. * (in[za + y1 + x1] + 2. * in[za + y1 + x] + in[za + y1 + x2] -
            in[za + y2 + x1] - 2. * in[za + y2 + x] - in[za + y2 + x2]) +
            in[z2 + y1 + x1] + 2. * in[z2 + y1 + x] + in[z2 + y1 + x2] -
            in[z2 + y2 + x1] - 2. * in[z2 + y2 + x] - in[z2 + y2 + x2]);
        dz = (float) (in[z1 + y1 + x1] + 2. * in[z1 + y1 + x] + in[z1 + y1 + x2] +
            2. * (in[z1 + ya + x1] + 2. * in[z1 + ya + x] + in[z1 + ya + x2]) +
            in[z1 + y2 + x1] + 2. * in[z1 + y2 + x] + in[z1 + y2 + x2] -
            in[z2 + y1 + x1] - 2. * in[z2 + y1 + x] - in[z2 + y1 + x2] -
            2. * (in[z2 + ya + x1] + 2. * in[z2 + ya + x] + in[z2 + ya + x2]) -
            in[z2 + y2 + x1] - 2. * in[z2 + y2 + x] - in[z2 + y2 + x2]);

        return dx * dx + dy * dy + dz * dz;
    }

    @Override
    protected float[] calc3Grad(int x, int y, int z)
    {
        double[] in = data;
        int za = z * dims[0] * dims[1];
        int z1 = z == 0 ? 0 : za - dims[0] * dims[1];
        int z2 = z == dims[2] - 1 ? za : za + dims[0] * dims[1];
        int w = dims[0];
        int ya = y * w;
        int y1 = y == 0 ? 0 : ya - w;
        int y2 = y == dims[1] - 1 ? ya : ya + w;
        int x1 = x == 0 ? 0 : x - 1;
        int x2 = x == w - 1 ? w - 1 : x + 1;
        float[] d = new float[3];
        d[0] = (float) (in[z1 + y1 + x1] - in[z1 + y1 + x2] +
            2. * (in[z1 + ya + x1] - in[z1 + ya + x2]) +
            in[z1 + y2 + x1] - in[z1 + y2 + x2] +
            2. * (in[za + y1 + x1] - in[za + y1 + x2] +
            2. * (in[za + ya + x1] - in[za + ya + x2]) +
            in[za + y2 + x1] - in[za + y2 + x2]) +
            in[z2 + y1 + x1] - in[z2 + y1 + x2] +
            2. * (in[z2 + ya + x1] - in[z2 + ya + x2]) +
            in[z2 + y2 + x1] - in[z2 + y2 + x2]);
        d[1] = (float) (in[z1 + y1 + x1] + 2. * in[z1 + y1 + x] + in[z1 + y1 + x2] -
            in[z1 + y2 + x1] - 2. * in[z1 + y2 + x] - in[z1 + y2 + x2] +
            2. * (in[za + y1 + x1] + 2. * in[za + y1 + x] + in[za + y1 + x2] -
            in[za + y2 + x1] - 2. * in[za + y2 + x] - in[za + y2 + x2]) +
            in[z2 + y1 + x1] + 2. * in[z2 + y1 + x] + in[z2 + y1 + x2] -
            in[z2 + y2 + x1] - 2. * in[z2 + y2 + x] - in[z2 + y2 + x2]);
        d[2] = (float) (in[z1 + y1 + x1] + 2. * in[z1 + y1 + x] + in[z1 + y1 + x2] +
            2. * (in[z1 + ya + x1] + 2. * in[z1 + ya + x] + in[z1 + ya + x2]) +
            in[z1 + y2 + x1] + 2. * in[z1 + y2 + x] + in[z1 + y2 + x2] -
            in[z2 + y1 + x1] - 2. * in[z2 + y1 + x] - in[z2 + y1 + x2] -
            2. * (in[z2 + ya + x1] + 2. * in[z2 + ya + x] + in[z2 + ya + x2]) -
            in[z2 + y2 + x1] - 2. * in[z2 + y2 + x] - in[z2 + y2 + x2]);
        return d;
    }

    @Override
    protected void calcDirs3MagsLayer(int z, float[][] dout, float[] mout)
    {
        double[] in = data;
        int za = z * dims[0] * dims[1];
        int z1 = z == 0 ? 0 : za - dims[0] * dims[1];
        int z2 = z == dims[2] - 1 ? za : za + dims[0] * dims[1];
        int w = dims[0];
        for (int y = 0; y < dims[1]; ++y) {
            int ya = y * w;
            int y1 = y == 0 ? 0 : ya - w;
            int y2 = y == dims[1] - 1 ? ya : ya + w;
            for (int x = 0; x < w; ++x) {
                int x1 = x == 0 ? 0 : x - 1;
                int x2 = x == w - 1 ? w - 1 : x + 1;
                dout[0][/*za + */ya + x] = (float) (in[z1 + y1 + x1] - in[z1 + y1 + x2] +
                    2. * (in[z1 + ya + x1] - in[z1 + ya + x2]) +
                    in[z1 + y2 + x1] - in[z1 + y2 + x2] +
                    2. * (in[za + y1 + x1] - in[za + y1 + x2] +
                    2. * (in[za + ya + x1] - in[za + ya + x2]) +
                    in[za + y2 + x1] - in[za + y2 + x2]) +
                    in[z2 + y1 + x1] - in[z2 + y1 + x2] +
                    2. * (in[z2 + ya + x1] - in[z2 + ya + x2]) +
                    in[z2 + y2 + x1] - in[z2 + y2 + x2]);
                dout[1][/*za + */ya + x] = (float) (in[z1 + y1 + x1] + 2. * in[z1 + y1 + x] + in[z1 + y1 + x2] -
                    in[z1 + y2 + x1] - 2. * in[z1 + y2 + x] - in[z1 + y2 + x2] +
                    2. * (in[za + y1 + x1] + 2. * in[za + y1 + x] + in[za + y1 + x2] -
                    in[za + y2 + x1] - 2. * in[za + y2 + x] - in[za + y2 + x2]) +
                    in[z2 + y1 + x1] + 2. * in[z2 + y1 + x] + in[z2 + y1 + x2] -
                    in[z2 + y2 + x1] - 2. * in[z2 + y2 + x] - in[z2 + y2 + x2]);
                dout[2][/*za + */ya + x] = (float) (in[z1 + y1 + x1] + 2. * in[z1 + y1 + x] + in[z1 + y1 + x2] +
                    2. * (in[z1 + ya + x1] + 2. * in[z1 + ya + x] + in[z1 + ya + x2]) +
                    in[z1 + y2 + x1] + 2. * in[z1 + y2 + x] + in[z1 + y2 + x2] -
                    in[z2 + y1 + x1] - 2. * in[z2 + y1 + x] - in[z2 + y1 + x2] -
                    2. * (in[z2 + ya + x1] + 2. * in[z2 + ya + x] + in[z2 + ya + x2]) -
                    in[z2 + y2 + x1] - 2. * in[z2 + y2 + x] - in[z2 + y2 + x2]);

                mout[ya + x] = (float) sqrt(
                    dout[0][ya + x] * dout[0][ya + x] +
                    dout[1][ya + x] * dout[1][ya + x] +
                    dout[2][ya + x] * dout[2][ya + x]);

            }
        }
    }

    @Override
    protected float[] calc2Grad(int x, int y)
    {
        double[] in = data;
        float[] dout = new float[2];
        int w = dims[0];
        int ya = y * w;
        int y1 = y == 0 ? 0 : ya - w;
        int y2 = y == dims[1] - 1 ? ya : ya + w;
        int x1 = x == 0 ? 0 : x - 1;
        int x2 = x == w - 1 ? w - 1 : x + 1;
        dout[0] = (float)(in[y1 + x1] - in[y1 + x2] + 2 * (in[ya + x1] - in[ya + x2]) + in[y2 + x1] - in[y2 + x2]);
        dout[1] = (float)(in[y1 + x1] + 2 * in[y1 + x] + in[y1 + x2] - in[y2 + x1] - 2 * in[y2 + x] - in[y2 + x2]);
        return dout;
    }

    @Override
    protected void calcDirs2(float[][] dout, float[] mout)
    {
        double[] in = data;
        int w = dims[0];
        for (int y = 0; y < dims[1]; ++y) {
            int ya = y * w;
            int y1 = y == 0 ? 0 : ya - w;
            int y2 = y == dims[1] - 1 ? ya : ya + w;
            for (int x = 0; x < w; ++x) {
                int x1 = x == 0 ? 0 : x - 1;
                int x2 = x == w - 1 ? w - 1 : x + 1;
                dout[0][ya + x] = (float) (in[y1 + x1] - in[y1 + x2] +
                    2. * (in[ya + x1] - in[ya + x2]) +
                    in[y2 + x1] - in[y2 + x2]);
                dout[1][ya + x] = (float) (in[y1 + x1] + 2. * in[y1 + x] + in[y1 + x2] -
                    in[y2 + x1] - 2. * in[y2 + x] - in[y2 + x2]);
                mout[ya + x] = (float) sqrt(
                    dout[0][ya + x] * dout[0][ya + x] +
                    dout[1][ya + x] * dout[1][ya + x]);
            }
        }
    }
}
