/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.Canny;

/**
 *
 * @author Andrzej Rutkowski (rudy@mat.uni.torun.pl)
 * @author modified by pegulski, ICM, UW
 */
public class CannyMaskedGradParams
{

    private int outputType = 1;
    private boolean dirOnly = true;
    private double dirX = 0;
    private double dirY = 0;
    private boolean appendMask = true;

    public boolean isAppendMask()
    {
        return appendMask;
    }

    public void setAppendMask(boolean appendMask)
    {
        this.appendMask = appendMask;
    }

    public boolean isDirOnly()
    {
        return dirOnly;
    }

    public void setDirOnly(boolean dirOnly)
    {
        this.dirOnly = dirOnly;
    }

    public double getDirX()
    {
        return dirX;
    }

    public void setDirX(double dirX)
    {
        this.dirX = dirX;
    }

    public double getDirY()
    {
        return dirY;
    }

    public void setDirY(double dirY)
    {
        this.dirY = dirY;
    }

    public int getOutputType()
    {
        return outputType;
    }

    public void setOutputType(int outputType)
    {
        this.outputType = outputType;
    }
}
