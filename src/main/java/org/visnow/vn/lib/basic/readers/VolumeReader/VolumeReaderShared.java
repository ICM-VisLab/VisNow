/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.VolumeReader;

import org.visnow.vn.engine.core.ParameterName;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class VolumeReaderShared
{

    public static final int FROM_FILE = 0;
    public static final int FROM_INDICES = 1;
    public static final int NORMALIZED = 2;
    public static final int USER_EXTENTS = 3;
    public static final int USER_AFFINE = 4;

    static final ParameterName<String> FILENAME = new ParameterName("File name");
    static final ParameterName<float[]> SCALE = new ParameterName("Scale");
    static final ParameterName<float[]> ORIG = new ParameterName("Orig");
    static final ParameterName<float[]> MIN = new ParameterName("Min");
    static final ParameterName<float[]> MAX = new ParameterName("max");
    static final ParameterName<Integer> TYPE = new ParameterName("Type");

}
