/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.filters.InterpolationToRegularField;

import java.awt.Color;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.Field;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.events.ColorEvent;
import org.visnow.vn.geometries.events.ColorListener;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.*;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphGUI;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams;
import org.visnow.vn.geometries.viewer3d.eventslisteners.pick.Pick3DListener;
import static org.visnow.vn.gui.widgets.RunButton.RunState.NO_RUN;
import static org.visnow.vn.gui.widgets.RunButton.RunState.RUN_DYNAMICALLY;
import static org.visnow.vn.gui.widgets.RunButton.RunState.RUN_ONCE;
import org.visnow.vn.lib.basic.filters.ConvexHull.ConvexHullCore;
import org.visnow.vn.lib.types.VNRegularField;
import org.visnow.vn.lib.utils.interpolation.RegularInterpolation;
import static org.visnow.vn.lib.basic.filters.InterpolationToRegularField.InterpolationToRegularFieldShared.*;
import org.visnow.vn.lib.utils.interpolation.OptimizedBox;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class InterpolationToRegularField extends OutFieldVisualizationModule
{

    private int runQueue = 0;

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    protected GUI computeUI = null;
    protected boolean ignoreUI = false;

    protected Field inField = null;
    protected int trueDim;

    protected FloatLargeArray inCoords = null;
    
    protected InteractiveGlyph glyph = new InteractiveGlyph(BOX);
    protected InteractiveGlyphParams glyphParams = glyph.getParams();
    protected InteractiveGlyphGUI glyphGUI = glyph.getComputeUI();
    
    protected float currentVolume = 0;
    protected boolean autoubdateBox = true;
    
    
    private float[][] outAffine;
    
    public InterpolationToRegularField()
    {
        backGroundColorListener = new ColorListener()
        {
            @Override
            public void colorChoosen(ColorEvent e) {
                Color bgr = e.getSelectedColor();
                float[] bgrF = new float[4];
                bgr.getColorComponents(bgrF);
                if (glyph != null)
                    glyph.setColors(bgrF[0] + bgrF[1] + bgrF[2] > 1.5);
            }
        };
        glyphParams.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (glyphParams.getAdjusting() < 0) 
                    currentVolume = glyphParams.getVolume();
                computeUI.armRunButton();
            }
        });
        
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name == null || name.isEmpty())
                    return;
                switch (name){
                case RUNNING_MESSAGE_STRING:
                    if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                        runQueue++;
                        startAction();
                    } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                        startAction();
                    break;
                case AUTO_STRING:
                    if (parameters.get(AUTO)) {
                        outAffine = OptimizedBox.optimizeInitBox(inCoords, trueDim, glyphParams);
                        glyph.getGlyph().setShowReper(false);
                        autoubdateBox = glyphParams.isAutoUpdateBox();
                        glyphParams.setAutoUpdateBox(true);
                    }
                    else {
                        glyph.getGlyph().setShowReper(true);
                        glyphParams.setAutoUpdateBox(autoubdateBox);
                    }
                    computeUI.armRunButton();
                    break;
                default:
                    computeUI.armRunButton();
                    break;
                }
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                computeUI.addGlyphUI(glyphGUI);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });

        outObj.addNode(glyph);
    }

    @Override
    public Pick3DListener getPick3DListener() {
        return glyph.getPick3DListener();
    }
    
    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(GLOBAL_RESOLUTION, true),
            new Parameter<>(RESOLUTION, 256),
            new Parameter<>(RESOLUTION0, 100),
            new Parameter<>(RESOLUTION1, 100),
            new Parameter<>(RESOLUTION2, 100),
            new Parameter<>(RESET_BOX, false),
            new Parameter<>(IS_OUTPUT, false),
            new Parameter<>(AUTO, true),
            new Parameter<>(META_TRUE_NSPACE, 3),
            new Parameter<>(META_DIAMETER, 1f),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN)
        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        parameters.set(META_TRUE_NSPACE, inField.getTrueNSpace());
        parameters.set(META_DIAMETER, inField.getDiameter());
        if (resetParameters) {
            parameters.set(AUTO, true);
            parameters.set(GLOBAL_RESOLUTION, true);
            parameters.set(RESOLUTION, 256);
            parameters.set(RESOLUTION0, 100);
            parameters.set(RESOLUTION1, 100);
            parameters.set(RESOLUTION2, 100);
        }
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }


    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null) {
            outField = null;
            prepareOutputGeometry();
            show();
            setOutputValue("outField", null);
            return;
        }
        Parameters p = parameters.getReadOnlyClone();
        Field newField = ((VNField) getInputFirstValue("inField")).getField();
        boolean isNewField = !isFromVNA() && newField != inField;
        if (isNewField) {
            inField = newField;
            switch (inField.getTrueNSpace()) {
            case 2:
                trueDim = 2;
                glyph.setType(RECTANGLE);
                break;
            case 3:
                trueDim = 3;
                glyph.setType(BOX);
                break;
            default:
                trueDim = 0;
                return;
            }
            inCoords = new FloatLargeArray(ConvexHullCore.convexHullCoords(inField));
            synchronized (parameters) {
                validateParamsAndSetSmart(isNewField);
                p = parameters.getReadOnlyClone();
            }
            glyph.setField(inField);
            notifyGUIs(p, isFromVNA() || isNewField, isFromVNA() || isNewField);
            computeUI.resetAuto();
            outAffine = OptimizedBox.optimizeInitBox(inCoords, trueDim, glyphParams);
            glyphParams.setAutoUpdateBox(true);
            glyph.getGlyph().setShowReper(false);
            computeUI.armRunButton();
            outObj.clearAllGeometry();
            outObj.addNode(glyph);
            setOutputValue("outField", null);
        }
        if (p != null && (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)) {
            runQueue = Math.max(runQueue - 1, 0);
            if (trueDim < 2)
                return;
            if (outAffine == null || !parameters.get(AUTO)) 
                outAffine = RegularInterpolation.outAffineFromboxVerts(glyph.getBoxVerts(trueDim), trueDim);
            
            if (parameters.get(GLOBAL_RESOLUTION))
                outField = outRegularField = 
                    RegularInterpolation.interpolate(inField, 
                                                     outAffine, 
                                                     parameters.get(RESOLUTION));
            else
                outField = outRegularField = 
                    RegularInterpolation.interpolate(inField, 
                                                     outAffine, 
                                                     new int[] {parameters.get(RESOLUTION0), 
                                                                parameters.get(RESOLUTION1), 
                                                                parameters.get(RESOLUTION2)});
            outAffine = null;
            setOutputValue("outField", new VNRegularField(outRegularField));
            prepareOutputGeometry();
            show();
            outObj.addNode(glyph);
            computeUI.disarmRunButton();
        }
    }
}
