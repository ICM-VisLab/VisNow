/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadLAS;

import com.github.mreutegg.laszip4j.LASPoint;
import com.github.mreutegg.laszip4j.LASReader;
import java.io.File;
import javax.swing.SwingUtilities;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.ShortLargeArray;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jscic.PointField;
import org.visnow.vn.engine.core.InputEgg;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.core.ProgressAgent;
import static org.visnow.vn.lib.basic.readers.ReadLAS.ReadLASShared.*;
import org.visnow.vn.lib.templates.visualization.modules.OutFieldVisualizationModule;
import org.visnow.vn.lib.types.VNPointField;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;

/**
 * Reader for LAS/LAZ files from LiDAR.
 *
 * {@link https://en.wikipedia.org/wiki/Lidar}.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class ReadLAS extends OutFieldVisualizationModule
{

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ReadLAS.class);
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI computeUI;
    private PointField outPointField;

    public ReadLAS()
    {

        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startIfNotInQueue();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {

            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return ReadLASShared.getDefaultParameters();
    }

    @Override
    protected void notifySwingGUIs(final org.visnow.vn.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {

        ProgressAgent progressAgent = getProgressAgent(100);
        Parameters p;
        synchronized (parameters) {
            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, false, false);
        progressAgent.setProgress(0.0);
        if (p.get(FILENAME).isEmpty()) {
            outField = null;
            progressAgent.setProgress(1.0);
            show();
            setOutputValue("outField", null);
        } else {
            outPointField = createLASGeometry(p.get(FILENAME), progressAgent);
            if (outPointField != null)
                setOutputValue("outField", new VNPointField(outPointField));
            else
                setOutputValue("outField", null);
            outField = outPointField;
            progressAgent.setProgress(0.9);
            prepareOutputGeometry();
            progressAgent.setProgress(1.0);
            show();
        }
    }

    public PointField createLASGeometry(String path, ProgressAgent progressAgent)
    {
        if (path == null) {
            LOGGER.info("path == null");
            return null;
        }

        LASReader reader = new LASReader(new File(path));

        long nNodes = reader.getHeader().getLegacyNumberOfPointRecords();
        PointField outFld = new PointField(nNodes);
        FloatLargeArray coord = new FloatLargeArray(nNodes * 3, false);
        int[] cells = new int[(int) nNodes];
        UnsignedByteLargeArray classifications = new UnsignedByteLargeArray(nNodes, false);
        IntLargeArray intensity = new IntLargeArray(nNodes, false);
        IntLargeArray red = null;
        IntLargeArray green = null;
        IntLargeArray blue = null;
        IntLargeArray nir = null;
        long idx = 0;
        boolean hasRGB = false;
        boolean hasNIR = false;
        int maxRGB = Integer.MIN_VALUE, maxInt = Integer.MIN_VALUE, maxNIR = Integer.MIN_VALUE;
        int minRGB = Integer.MAX_VALUE, minInt = Integer.MAX_VALUE, minNIR = Integer.MAX_VALUE;
        int maxClass = 0;
        for (LASPoint p : reader.getPoints()) {
            if(idx == 0) {
                if(p.hasRGB()) {
                    red = new IntLargeArray(nNodes, false);
                    green = new IntLargeArray(nNodes, false);
                    blue = new IntLargeArray(nNodes, false);
                    hasRGB = true;
                }
                if(p.hasNIR()) {
                    nir = new IntLargeArray(nNodes, false);
                    hasNIR = true;
                }
            }
            coord.setFloat(3 * idx, p.getX());
            coord.setFloat(3 * idx + 1, p.getY());
            coord.setFloat(3 * idx + 2, p.getZ());
            cells[(int) idx] = (int) idx;
            if(hasRGB) {
                red.setInt(idx,   p.getRed());
                green.setInt(idx, p.getGreen());
                blue.setInt(idx,  p.getBlue());
                if (p.getRed()   > maxRGB) maxRGB = p.getRed();
                if (p.getGreen() > maxRGB) maxRGB = p.getGreen();
                if (p.getBlue()  > maxRGB) maxRGB = p.getBlue();
                if (p.getRed()   < minRGB) minRGB = p.getRed();
                if (p.getGreen() < minRGB) minRGB = p.getGreen();
                if (p.getBlue()  < minRGB) minRGB = p.getBlue();
            }
            if(hasNIR) {
                nir.setInt(idx, p.getNIR());
                if (p.getNIR() > maxNIR) maxNIR = p.getNIR();
                if (p.getNIR() < minNIR) minNIR = p.getNIR();
            }
            intensity.setInt(idx, p.getIntensity());
            if (p.getIntensity() > maxInt) maxInt = p.getIntensity();
            if (p.getIntensity() < minInt) minInt = p.getIntensity();

            classifications.setUnsignedByte(idx++, p.getClassification());
            if (p.getClassification() > maxClass)
                maxClass = p.getClassification();
            progressAgent.setProgress((idx / (double) nNodes) * 0.9);
        }
        outFld.setCurrentCoords(coord);
        outFld.addComponent(DataArray.create(classifications, 1, "classifications").preferredRange(0, Math.max(1, maxClass)));
        if  (maxInt < 256 && minInt >= 0)
            outFld.addComponent(DataArray.create(
                        (UnsignedByteLargeArray)LargeArrayUtils.convert(intensity, LargeArrayType.UNSIGNED_BYTE), 1, "intensity").
                        preferredRange(0, Math.max(1, maxInt)));
        else if (minInt >= Short.MIN_VALUE && maxInt < Short.MAX_VALUE)
            outFld.addComponent(DataArray.create(
                        (ShortLargeArray)LargeArrayUtils.convert(intensity, LargeArrayType.SHORT), 1, "intensity").
                        preferredRange(minInt, maxInt));
        else
            outFld.addComponent(DataArray.create(intensity, 1, "intensity").
                        preferredRange(minInt, maxInt));
        if (hasRGB) {
            if (maxRGB < 256 && minRGB >= 0) {
                outFld.addComponent(DataArray.create(
                        (UnsignedByteLargeArray)LargeArrayUtils.convert(red,   LargeArrayType.UNSIGNED_BYTE), 1, "red").
                        preferredRange(0, maxRGB));
                outFld.addComponent(DataArray.create(
                        (UnsignedByteLargeArray)LargeArrayUtils.convert(green, LargeArrayType.UNSIGNED_BYTE), 1, "green").
                        preferredRange(0, maxRGB));
                outFld.addComponent(DataArray.create(
                        (UnsignedByteLargeArray)LargeArrayUtils.convert(blue,  LargeArrayType.UNSIGNED_BYTE), 1, "blue").
                        preferredRange(0, maxRGB));
            }
            else {
                outFld.addComponent(DataArray.create(red,   1, "red"));
                outFld.addComponent(DataArray.create(green, 1, "green"));
                outFld.addComponent(DataArray.create(blue,  1, "blue"));
            }
        }
        if (hasNIR) {
            if (maxNIR < 256 && minNIR >= 0)
                outFld.addComponent(DataArray.create(
                        (UnsignedByteLargeArray)LargeArrayUtils.convert(nir,   LargeArrayType.UNSIGNED_BYTE), 1, "nir").
                        preferredRange(0, maxNIR));
            else
                outFld.addComponent(DataArray.create(nir, 1, "nir"));
        }
        VisNow.get().userMessageSend(this, "File successfully loaded", "", org.visnow.vn.system.utils.usermessage.Level.INFO);
        return outFld;
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag())
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    computeUI.activateOpenDialog();
                }
            });
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

}
