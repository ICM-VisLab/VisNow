/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices;

import java.util.ArrayList;
import java.util.List;
import static org.visnow.vn.lib.basic.viewers.HapticViewer3D.model.devices.DeviceManager.getDevicesNamesList;

/**
 * Helper object that manages listeners that are notified about changes on list of devices. It's
 * used internally by {@link DeviceManager}.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class DeviceManagerListenerSupport
{
    //

    /**
     * List of listeners waiting for device changes.
     */
    private static ArrayList<DeviceManager.IDeviceManagerListener> registeredDevicesListeners
        = new ArrayList<DeviceManager.IDeviceManagerListener>();

    /**
     * Adds
     * <code>listener</code> to the list of objects that are notified of changes in devices (new
     * device, removing device(?), changes in using or not using a device).
     * This call immediately results in notifying the
     * <code>listener</code> of the list of already added devices.
     *
     * @param listener listener to be added
     */
    public synchronized void addRegisteredDevicesListener(DeviceManager.IDeviceManagerListener listener)
    {
        registeredDevicesListeners.add(listener);

        // sent to the newly added listener the list of already registered devices
        List<DeviceName> devicesNames = getDevicesNamesList();
        listener.devicesInitialList(devicesNames);
    }

    /**
     * Removes
     * <code>listener</code> from the list of objects that are notified of changes in devices (new
     * device, removing device(?), changes in using or not using a device).
     *
     * @param listener listener to be removed
     */
    public synchronized void removeRegisteredDevicesListener(DeviceManager.IDeviceManagerListener listener)
    {
        registeredDevicesListeners.remove(listener);
    }

    /**
     * Sends a list of registered devices to all listeners.
     */
    public synchronized void fireInitDevicesList()
    {
        List<DeviceName> devicesNames = getDevicesNamesList();
        for (DeviceManager.IDeviceManagerListener listener : registeredDevicesListeners) {
            listener.devicesInitialList(devicesNames);
        }
    }

    /**
     * Sends a list of registered devices to all listeners.
     * NOTE: there can be nulls here (for devices that are standard ones). They should be ignored.
     */
    public synchronized void fireDeviceAdded(IPassiveDevice device)
    {

        DeviceName deviceName = new DeviceName(device);

        for (DeviceManager.IDeviceManagerListener listener : registeredDevicesListeners) {
            listener.deviceAdded(deviceName);
        }
    }

    /**
     * Sends a list of registered devices to all listeners.
     * NOTE: there can be nulls here (for devices that are standard ones). They should be ignored.
     */
    public synchronized void fireDeviceRemoved(IPassiveDevice device)
    {

        DeviceName deviceName = new DeviceName(device);

        for (DeviceManager.IDeviceManagerListener listener : registeredDevicesListeners) {
            listener.deviceRemoved(deviceName);
        }
    }

    /**
     * Sends a list of registered devices to all listeners.
     * NOTE: there can be nulls here (for devices that are standard ones). They should be ignored.
     */
    public synchronized void fireDeviceChangedState(IPassiveDevice device)
    {

        DeviceName deviceName = new DeviceName(device);

        for (DeviceManager.IDeviceManagerListener listener : registeredDevicesListeners) {
            listener.deviceChangedState(deviceName);
        }
    }
}
