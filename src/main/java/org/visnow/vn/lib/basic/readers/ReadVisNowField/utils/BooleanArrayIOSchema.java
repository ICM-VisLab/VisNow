/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.basic.readers.ReadVisNowField.utils;

import org.visnow.jscic.CellSet;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jlargearrays.LogicLargeArray;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class BooleanArrayIOSchema extends DataElementIOSchema
{

    private final LogicLargeArray boolArray;

    public BooleanArrayIOSchema(CellSet dataset, LogicLargeArray boolArray, int veclen, long nData, int coord, int offsetFrom, int offsetTo)
    {
        super(dataset.getName(), dataset, coord, DataArrayType.FIELD_DATA_LOGIC, veclen, nData, offsetFrom, offsetTo);
        this.boolArray = boolArray;
    }

    public LogicLargeArray getBoolArray()
    {
        return boolArray;
    }

    @Override
    public String toString()
    {
        StringBuilder s = new StringBuilder("[");
        if (coord != -1)
            s.append("." + coord + " ");
        s.append(" boolean");
        s.append(" off " + offsetFrom);
        if (offsetTo != -1)
            s.append("-" + offsetTo);
        return s.toString() + "]";
    }

}
