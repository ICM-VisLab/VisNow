/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI;

import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.jscic.RegularFieldSchema;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class IndexSliceParams
{
    public static enum Type {UNDEFINED, POINT, LINE, PLANE, WHOLE};

    private final int[] dims =       {10, 10, 10};
    private final float[] position = {0,   0, 0};
    private final int[] low =        {0,   0, 0};
    private final int[] up =         {10, 10, 10};
    private final boolean[] fixed = {false, false, false};
    private int nDims = 3;
    private Type type = Type.WHOLE;
    private boolean active = false;
    private boolean adjusting = false;
    private boolean glyphVisible = true;
    private IndexSliceGUI gui = null;
    private boolean essentialChange = false;

    public void setFieldSchema(RegularFieldSchema schema)
    {
        long[] lDims = schema.getDims();
        nDims = lDims.length;
        for (int i = 0; i < lDims.length; i++) {
            low[i] = 0;
            up[i]  = dims[i] = (int)lDims[i];
            position[i] = .5f * dims[i];
        }
        if (type == Type.PLANE && nDims <= 2)
            setType(Type.WHOLE);
        if (gui != null)
            gui.updateFromParams();
        essentialChange = true;
    }

    public boolean isEssentialChange() {
        return essentialChange;
    }

    public void setEssentialChange(boolean essentialChange) {
        this.essentialChange = essentialChange;
    }

    public int getnDims()
    {
        return nDims;
    }

    public int[] getDims()
    {
        return dims;
    }

    public Type getType()
    {
        return type;
    }

    public void setType(Type type, boolean silent)
    {
        if (type != this.type) {
            switch (type) {
            case POINT:
                Arrays.fill(fixed, true);
                break;
            case WHOLE:
                Arrays.fill(fixed, false);
                break;
            case LINE:
                Arrays.fill(fixed, true);
                fixed[nDims - 1] = false;
                break;
            case PLANE:
                Arrays.fill(fixed, false);
                fixed[nDims - 1] = true;
                break;
            }
        }
        this.type = type;
        active = false;
        if (gui != null)
            gui.updateFromParams();
        active = true;
        essentialChange = true;
        if (!silent)
            fireStateChanged();
    }

    public void setType(Type type)
    {
        setType(type, true);
        fireStateChanged();
    }

    public boolean[] getFixed()
    {
        return fixed;
    }

    /**
     * Get the value of position
     *
     * @return the value of position
     */
    public float[] getPosition()
    {
        return position;
    }

    public int getAxis()
    {
        switch (type) {
        case LINE:
            for (int i = 0; i < dims.length; i++)
                if (!fixed[i])
                    return i;
            break;
        case PLANE:
            for (int i = 0; i < dims.length; i++)
                if (fixed[i])
                    return i;
            break;
        default:
        }
        return -1;
    }

    /**
     * Set the value of axis
     *
     * @param axis new value of axis
     */
    public void setAxis(int axis)
    {
        if (axis < 0 || axis >= nDims)
            return;
        switch (type) {
            case LINE:
                Arrays.fill(fixed, true);
                fixed[axis] = false;
                break;
            case PLANE:
                Arrays.fill(fixed, false);
                fixed[axis] = true;
                break;
            default:
        }
        essentialChange = true;
        glyphVisible = true;
    }

    /**
     * Get the value of extents
     *
     * @return the value of extents
     */
    public int[] getLow()
    {
        return low;
    }

    public int[] getUp()
    {
        return up;
    }

    /**
     * Get the value of adjusting
     *
     * @return the value of adjusting
     */
    public boolean isAdjusting()
    {
        return adjusting;
    }

    /**
     * Set the value of adjusting
     *
     * @param adjusting new value of adjusting
     */
    public void setAdjusting(boolean adjusting)
    {
        this.adjusting = adjusting;
    }

    public boolean isGlyphVisible()
    {
        return glyphVisible;
    }

    public void setGlyphVisible(boolean glyphVisible)
    {
        if (this.glyphVisible != glyphVisible)  {
            this.glyphVisible = glyphVisible;
            fireStateChanged();
        }
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setGUI(IndexSliceGUI gui)
    {
        this.gui = gui;
        gui.updateFromParams();
    }
    /**
     * Utility field holding list of global
        active = true;ChangeListeners.
     */
    protected transient ArrayList<ChangeListener> changeListenerList = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Clears ChangeListenerList.
     */
    public synchronized void clearChangeListeners()
    {
        changeListenerList.clear();
    }

    /**
     * Notifies all registered listeners about the event (calls
     * <code>stateChanged()</code> on each listener in
     * <code>changeListenerList</code>).
     */
    public void fireStateChanged()
    {
        if (active) {
            ChangeEvent e = new ChangeEvent(this);
            for (int i = 0; i < changeListenerList.size(); i++)
                changeListenerList.get(i).stateChanged(e);
        }
        essentialChange = false;
    }

}
