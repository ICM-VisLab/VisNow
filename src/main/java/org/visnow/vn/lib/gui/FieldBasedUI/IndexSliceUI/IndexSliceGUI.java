/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.gui.swingwrappers.RadioButton;
import org.visnow.vn.gui.widgets.FloatSlider;
import org.visnow.vn.gui.widgets.SubRangeSlider.ExtendedSubRangeSlider;
import org.visnow.vn.gui.widgets.SubRangeSlider.SubRangeModel;
import static org.visnow.vn.gui.widgets.SubRangeSlider.SubRangeModel.*;
import static org.visnow.vn.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams.Type.*;
import org.visnow.vn.lib.utils.flowVisualizationUtils.SeedPointParams.SeedPointsSelection;

/**
 *
 * @author know
 */
public class IndexSliceGUI extends javax.swing.JPanel
{
    private IndexSliceParams params;
    private int axis = 0;
    private IndexSliceParams.Type type = UNDEFINED;
    /**
     * Creates new form IndexSliceGUI
     */
    public IndexSliceGUI()
    {
        initComponents();
    }

    /**
     * Creates new form IndexSliceGUI
     */
    public IndexSliceGUI(IndexSliceParams.Type type)
    {
        initComponents();
        this.type = type;
    }

    /**
     * Creates new form IndexSliceGUI
     */
    public IndexSliceGUI(IndexSliceParams.Type type, boolean showHideButton)
    {
        initComponents();
        this.type = type;
        hideGlyphButton.setVisible(showHideButton);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        GridBagConstraints gridBagConstraints;

        buttonGroup1 = new ButtonGroup();
        buttonGroup2 = new ButtonGroup();
        hideGlyphButton = new JButton();
        iPanel = new JPanel();
        iSlider = new FloatSlider();
        iRangeSlider = new ExtendedSubRangeSlider();
        iRadioButton = new RadioButton();
        jPanel = new JPanel();
        jSlider = new FloatSlider();
        jRangeSlider = new ExtendedSubRangeSlider();
        jRadioButton = new RadioButton();
        kPanel = new JPanel();
        kSlider = new FloatSlider();
        kRangeSlider = new ExtendedSubRangeSlider();
        kRadioButton = new RadioButton();
        jLabel2 = new JLabel();
        jPanel1 = new JPanel();

        setLayout(new GridBagLayout());

        hideGlyphButton.setText("hide glyph");
        hideGlyphButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                hideGlyphButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(0, 6, 0, 6);
        add(hideGlyphButton, gridBagConstraints);

        iPanel.setMinimumSize(new Dimension(172, 60));
        iPanel.setPreferredSize(new Dimension(250, 65));
        iPanel.setLayout(new GridBagLayout());

        iSlider.setShowingFields(false);
        iSlider.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                iSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.1;
        iPanel.add(iSlider, gridBagConstraints);

        iRangeSlider.setExtent(2);
        iRangeSlider.setPolicy(STOP);
        iRangeSlider.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                iRangeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        iPanel.add(iRangeSlider, gridBagConstraints);

        buttonGroup1.add(iRadioButton);
        iRadioButton.setSelected(true);
        iRadioButton.setText("i"); // NOI18N
        iRadioButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                iRadioButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(0, 6, 0, 6);
        iPanel.add(iRadioButton, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 6, 0, 7);
        add(iPanel, gridBagConstraints);

        jPanel.setMinimumSize(new Dimension(172, 60));
        jPanel.setPreferredSize(new Dimension(254, 65));
        jPanel.setLayout(new GridBagLayout());

        jSlider.setShowingFields(false);
        jSlider.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                jSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 51;
        gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanel.add(jSlider, gridBagConstraints);

        jRangeSlider.setExtent(2);
        jRangeSlider.setPolicy(STOP);
        jRangeSlider.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                jRangeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel.add(jRangeSlider, gridBagConstraints);

        buttonGroup1.add(jRadioButton);
        jRadioButton.setText("j");
        jRadioButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                jRadioButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(0, 6, 0, 6);
        jPanel.add(jRadioButton, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(8, 6, 8, 7);
        add(jPanel, gridBagConstraints);

        kPanel.setMinimumSize(new Dimension(172, 60));
        kPanel.setPreferredSize(new Dimension(254, 65));
        kPanel.setLayout(new GridBagLayout());

        kSlider.setShowingFields(false);
        kSlider.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                kSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 59;
        gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 0, 1);
        kPanel.add(kSlider, gridBagConstraints);

        kRangeSlider.setExtent(2);
        kRangeSlider.setPolicy(STOP);
        kRangeSlider.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                kRangeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        kPanel.add(kRangeSlider, gridBagConstraints);

        buttonGroup1.add(kRadioButton);
        kRadioButton.setText("k"); // NOI18N
        kRadioButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                kRadioButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(0, 6, 0, 6);
        kPanel.add(kRadioButton, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 6, 0, 6);
        add(kPanel, gridBagConstraints);

        jLabel2.setText("Axis"); // NOI18N
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new Insets(4, 18, 4, 0);
        add(jLabel2, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        add(jPanel1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void updateAxis()
    {
        axis = iRadioButton.isSelected() ? 0 : jRadioButton.isSelected() ? 1 : 2;
        updateWidgets();
        if (params != null) {
            params.setGlyphVisible(true);
            params.setAxis(axis);
            updateWidgets();
            params.fireStateChanged();
        }
    }

    private void iSliderStateChanged(ChangeEvent evt)//GEN-FIRST:event_iSliderStateChanged
    {//GEN-HEADEREND:event_iSliderStateChanged
        if (params != null) {
            params.setGlyphVisible(true);
            params.getPosition()[0] = iSlider.getVal();
            params.setAdjusting(iSlider.isAdjusting());
            params.setEssentialChange(true);
            params.fireStateChanged();
        }
    }//GEN-LAST:event_iSliderStateChanged

    private void jSliderStateChanged(ChangeEvent evt)//GEN-FIRST:event_jSliderStateChanged
    {//GEN-HEADEREND:event_jSliderStateChanged
        if (params != null) {
            params.setGlyphVisible(true);
            params.getPosition()[1] = jSlider.getVal();
            params.setAdjusting(jSlider.isAdjusting());
            params.setEssentialChange(true);
            params.fireStateChanged();
        }
    }//GEN-LAST:event_jSliderStateChanged

    private void kSliderStateChanged(ChangeEvent evt)//GEN-FIRST:event_kSliderStateChanged
    {//GEN-HEADEREND:event_kSliderStateChanged
        if (params != null) {
            params.setGlyphVisible(true);
            params.getPosition()[2] = kSlider.getVal();
            params.setEssentialChange(true);
            params.setAdjusting(kSlider.isAdjusting());
            params.fireStateChanged();
        }
    }//GEN-LAST:event_kSliderStateChanged

    private void iRadioButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_iRadioButtonActionPerformed
    {//GEN-HEADEREND:event_iRadioButtonActionPerformed
        updateAxis();
    }//GEN-LAST:event_iRadioButtonActionPerformed

    private void jRadioButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_jRadioButtonActionPerformed
    {//GEN-HEADEREND:event_jRadioButtonActionPerformed
        updateAxis();
    }//GEN-LAST:event_jRadioButtonActionPerformed

    private void kRadioButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_kRadioButtonActionPerformed
    {//GEN-HEADEREND:event_kRadioButtonActionPerformed
        updateAxis();
    }//GEN-LAST:event_kRadioButtonActionPerformed

    private void hideGlyphButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_hideGlyphButtonActionPerformed
    {//GEN-HEADEREND:event_hideGlyphButtonActionPerformed
        params.setGlyphVisible(false);
        params.fireStateChanged();
    }//GEN-LAST:event_hideGlyphButtonActionPerformed

    private void iRangeSliderStateChanged(ChangeEvent evt)//GEN-FIRST:event_iRangeSliderStateChanged
    {//GEN-HEADEREND:event_iRangeSliderStateChanged
         if (params != null) {
            params.setGlyphVisible(true);
            params.getLow()[0] = iRangeSlider.getLow();
            params.getUp()[0] =  iRangeSlider.getUp();
            params.setAdjusting(iRangeSlider.isAdjusting());
            params.setEssentialChange(true);
            params.fireStateChanged();
        }
    }//GEN-LAST:event_iRangeSliderStateChanged

    private void jRangeSliderStateChanged(ChangeEvent evt)//GEN-FIRST:event_jRangeSliderStateChanged
    {//GEN-HEADEREND:event_jRangeSliderStateChanged
        if (params != null) {
            params.setGlyphVisible(true);
            params.getLow()[1] = jRangeSlider.getLow();
            params.getUp()[1] =  jRangeSlider.getUp();
            params.setAdjusting(jRangeSlider.isAdjusting());
            params.setEssentialChange(true);
            params.fireStateChanged();
        }
    }//GEN-LAST:event_jRangeSliderStateChanged

    private void kRangeSliderStateChanged(ChangeEvent evt)//GEN-FIRST:event_kRangeSliderStateChanged
    {//GEN-HEADEREND:event_kRangeSliderStateChanged
        if (params != null) {
            params.setGlyphVisible(true);
            params.getLow()[2] = kRangeSlider.getLow();
            params.getUp()[2] =  kRangeSlider.getUp();
            params.setAdjusting(kRangeSlider.isAdjusting());
            params.setEssentialChange(true);
            params.fireStateChanged();
        }
    }//GEN-LAST:event_kRangeSliderStateChanged

    public void setType(IndexSliceParams.Type type)
    {
        this.type = type;
        params.setActive(false);
        params.setType(type);
        updateWidgets();
        params.setActive(true);
        params.fireStateChanged();
    }

    public void updateAxesButtons(SeedPointsSelection type)
    {
        RadioButton[] axisButtons = new RadioButton[]
                                    {iRadioButton, jRadioButton, kRadioButton};
        for (int i = 0; i < 3; i++)
            axisButtons[i].setEnabled(type == SeedPointsSelection.LINE_SLICE ||
                                      type == SeedPointsSelection.PLANE_SLICE);
    }

    public void updateFromParams()
    {
        if (params == null)
            return;
        int[] dims = params.getDims();
        int[] low = params.getLow();
        int[] up =  params.getUp();
        params.setActive(false);
        if (type != UNDEFINED) {
            updateWidgets();
        }
        iSlider.setMax(dims[0] - 1);
        iRangeSlider.setParams(0, low[0], up[0], dims[0]);
        iSlider.setVal(.5f * (dims[0] - 1));
        if (dims.length > 1) {
            jSlider.setMax(dims[1] - 1);
            jRangeSlider.setParams(0, low[1], up[1],  dims[1]);
            jSlider.setVal(.5f * (dims[1] - 1));
            jPanel.setVisible(true);
        }
        else {
            jPanel.setVisible(false);
        }
        if (dims.length > 2) {
            kSlider.setMax(dims[2] - 1);
            kRangeSlider.setParams(0, low[2], up[2],  dims[2]);
            kSlider.setVal(.5f * (dims[2] - 1));
            kPanel.setVisible(true);
        }
        else {
            kPanel.setVisible(false);
        }
        params.setGlyphVisible(true);
        updateAxis();
        updateWidgets();
        params.setActive(true);
    }

    public void setParams(IndexSliceParams params)
    {
        this.params = params;
        params.setGUI(this);
        if (type != UNDEFINED)
            setType(type);
    }

    private void updateWidgets()
    {
        FloatSlider[] sliders = new FloatSlider[]
                                    {iSlider,      jSlider,      kSlider};
        ExtendedSubRangeSlider[] rangeSliders = new ExtendedSubRangeSlider[]
                                    {iRangeSlider, jRangeSlider, kRangeSlider};
        RadioButton[] axisButtons = new RadioButton[]
                                    {iRadioButton, jRadioButton, kRadioButton};
        JPanel[] axisPanels = new JPanel[]
                                    {iPanel,       jPanel,       kPanel};
        for (int i = 0; i < 3; i++)
            axisPanels[i].setVisible(i < params.getnDims());
        for (int i = 0; i < params.getnDims(); i++) {
            sliders[i].setVisible(params.getFixed()[i]);
            rangeSliders[i].setVisible(!params.getFixed()[i]);
            axisButtons[i].setEnabled(params.getType() == LINE || params.getType() == PLANE);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected ButtonGroup buttonGroup1;
    protected ButtonGroup buttonGroup2;
    protected JButton hideGlyphButton;
    protected JPanel iPanel;
    protected RadioButton iRadioButton;
    protected ExtendedSubRangeSlider iRangeSlider;
    protected FloatSlider iSlider;
    protected JLabel jLabel2;
    protected JPanel jPanel;
    protected JPanel jPanel1;
    protected RadioButton jRadioButton;
    protected ExtendedSubRangeSlider jRangeSlider;
    protected FloatSlider jSlider;
    protected JPanel kPanel;
    protected RadioButton kRadioButton;
    protected ExtendedSubRangeSlider kRangeSlider;
    protected FloatSlider kSlider;
    // End of variables declaration//GEN-END:variables
}
