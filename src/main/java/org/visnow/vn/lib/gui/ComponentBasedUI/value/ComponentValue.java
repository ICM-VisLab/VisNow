/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.gui.ComponentBasedUI.value;

import java.util.function.Predicate;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;
import org.visnow.vn.lib.gui.ComponentBasedUI.PreferredExtentChangeBehavior;
import static org.visnow.vn.lib.gui.ComponentBasedUI.PreferredExtentChangeBehavior.ALWAYS_RESET;
import static org.visnow.vn.lib.gui.ComponentBasedUI.PreferredExtentChangeBehavior.KEEP_IF_INSIDE;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ComponentValue extends ComponentFeature
{
    protected float physicalValue = 0;
    protected ComponentValueUI valUI = null;
    protected FloatValueModificationListener uiValueChangedListener =
           new FloatValueModificationListener()
           {
              @Override
              public void floatValueChanged(FloatValueModificationEvent e)
              {
                 if (componentSchema == null || e.isAdjusting() && !continuousUpdate)
                    return;
                 physicalValue = e.getVal();
                 fireStateChanged();
              }
           };

    public ComponentValue()
    {
       super();
       preferredExtentChangeBehavior = KEEP_IF_INSIDE;
    }

    public ComponentValue(boolean pseudoComponentsAllowed, String[] preferedItemNames,
                          boolean scalarsOnly, boolean vectorsOnly,
                          boolean addNull, boolean prefereNull,
                          boolean continuousUpdate,
                          PreferredExtentChangeBehavior preferredExtentChangeBehavior)
    {
        super(pseudoComponentsAllowed, preferedItemNames, true,
              scalarsOnly, vectorsOnly, addNull, prefereNull,
              continuousUpdate, preferredExtentChangeBehavior);
    }

    public ComponentValue(boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                          boolean addNull, boolean prefereNull, boolean continuousUpdate)
    {
        this(false, new String[]{}, scalarsOnly, vectorsOnly,
             addNull, prefereNull, continuousUpdate, KEEP_IF_INSIDE);
    }

    public ComponentValue(boolean pseudoComponentsAllowed, String[] preferedItemNames,
                          boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                          boolean addNull, boolean prefereNull, boolean continuousUpdate,
                          PreferredExtentChangeBehavior preferredExtentChangeBehavior)
    {
        super(pseudoComponentsAllowed, preferedItemNames,
              numericsOnly, scalarsOnly, vectorsOnly, addNull, prefereNull,
              continuousUpdate, preferredExtentChangeBehavior);
    }

    public ComponentValue(boolean pseudoComponentsAllowed,
                            boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate,
                            PreferredExtentChangeBehavior preferredExtentChangeBehavior)
    {
        this(pseudoComponentsAllowed, new String[]{}, numericsOnly, scalarsOnly, vectorsOnly,
             addNull, prefereNull, continuousUpdate, preferredExtentChangeBehavior);
    }

    public ComponentValue(boolean pseudoComponentsAllowed,
                            boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate)
    {
        this(pseudoComponentsAllowed, new String[]{}, numericsOnly, scalarsOnly, vectorsOnly,
             addNull, prefereNull, continuousUpdate, ALWAYS_RESET);
    }

    public ComponentValue(boolean pseudoComponentsAllowed, String[] preferedItemNames,
                            boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate)
    {
        this(pseudoComponentsAllowed, preferedItemNames, numericsOnly, scalarsOnly, vectorsOnly,
             addNull, prefereNull, continuousUpdate, ALWAYS_RESET);
    }

    /**
     * Standard constructor with all behavior definable
     * @param schemaAcceptor           lambda schema -> schema acceptable containing all component selection details
     * @param pseudoComponentsAllowed  true if indices and coordinates are allowed (see e.g. texture coordinates
     * @param preferedItemNames        some general component names can be preferred as initial selection
     * @param addNull                  true if null component is admissible (e.g. null as data mapping component)
     * @param prefereNull              true if initial selection is null component
     * @param continuousUpdate         if true, sliders in subclasses will notify listeners dynamically 
     * @param preferredExtentChangeBehavior subclass action taken if data daynamically change
     */
    public ComponentValue(Predicate<DataArraySchema> schemaAcceptor,
                            boolean pseudoComponentsAllowed, String[] preferedItemNames,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate,
                            PreferredExtentChangeBehavior preferredExtentChangeBehavior)
    {
        super(schemaAcceptor, pseudoComponentsAllowed, preferedItemNames, addNull, prefereNull, continuousUpdate, preferredExtentChangeBehavior);
    }

    /**
     * Simplified constructor without preferred component names selection and without pseudocomponents
     * @param schemaAcceptor           lambda schema -> schema acceptable containing all component selection details
     * @param addNull                  true if null component is admissible (e.g. null as data mapping component)
     * @param prefereNull              true if initial selection is null component
     * @param continuousUpdate         if true, sliders in subclasses will notify listeners dynamically 
     * @param preferredExtentChangeBehavior subclass action taken if data daynamically change
     */
    public ComponentValue(Predicate<DataArraySchema> schemaAcceptor,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate,
                            PreferredExtentChangeBehavior preferredExtentChangeBehavior)
    {
        this(schemaAcceptor, false, null, addNull, prefereNull, continuousUpdate, preferredExtentChangeBehavior);
    }
    
     /**
     * Simplified constructor with proper components, no preferred names and default range reset behavior in subclasses
     * @param schemaAcceptor           lambda schema -> schema acceptable containing all component selection details
     * @param addNull                  true if null component is admissible (e.g. null as data mapping component)
     * @param prefereNull              true if initial selection is null component
     * @param continuousUpdate         if true, sliders in subclasses will notify listeners dynamically 
     */
    public ComponentValue(Predicate<DataArraySchema> schemaAcceptor,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate)
    {
        this(schemaAcceptor, addNull, prefereNull, continuousUpdate, ALWAYS_RESET);
    }
    
     /**
     * Simplified constructor with proper components, no null component allowed, no preferred names and default range reset behavior in subclasses
     * @param schemaAcceptor           lambda schema -> schema acceptable containing all component selection details
     * @param continuousUpdate         if true, sliders in subclasses will notify listeners dynamically 
     */
    public ComponentValue(Predicate<DataArraySchema> schemaAcceptor, 
                            boolean continuousUpdate)
    {
        this(schemaAcceptor, false, false, false);
    }

      /**
     * Simplest constructor setting lambda only; sliders in subclasses will remain silent when dragged
     * @param schemaAcceptor  lambda schema -> schema acceptable containing all component selection details
     */
   public ComponentValue(Predicate<DataArraySchema> schemaAcceptor)
    {
        this(schemaAcceptor, false);
    }
    

    public void setValue(float value)
    {
        if (componentSchema == null)
            return;
        physicalValue = (float)(componentSchema.getPreferredPhysMinValue() +
            (componentSchema.getPreferredPhysMaxValue() - componentSchema.getPreferredPhysMinValue()) *
            (value - componentSchema.getPreferredMinValue()) / (componentSchema.getPreferredMaxValue() - componentSchema.getPreferredMinValue()));
        if (valUI != null)
            valUI.updateValue();
        fireStateChanged();
    }

    public void setPhysicalValue(float physicalValue)
    {
        if (componentSchema == null)
            return;
        this.physicalValue = physicalValue;
        if (valUI != null)
            valUI.updateValue();
        fireStateChanged();
    }

    void setPhysicalValueFromUI(float physicalValue)
    {
        if (componentSchema == null)
            return;
        this.physicalValue = physicalValue;
        if (continuousUpdate || valUI == null || !valUI.isAdjusting())
            fireStateChanged();
    }

    public float getValue()
    {
        return physToVal(physicalValue);
    }

    public float getPhysicalValue()
    {
        return physicalValue;
    }

    public void updateComponentValue()
    {
        if (componentSchema == null)
            return;
        if (oldComponentNameInvalid)
            physicalValue = (float)(componentSchema.getPreferredPhysMaxValue() + componentSchema.getPreferredPhysMinValue()) / 2;
        switch (preferredExtentChangeBehavior)
        {
        case ALWAYS_RESET:
            physicalValue = (float)(componentSchema.getPreferredPhysMaxValue() + componentSchema.getPreferredPhysMinValue()) / 2;
            break;
        case KEEP_IF_INSIDE:
            if (physicalValue < componentSchema.getPreferredPhysMinValue() ||
                physicalValue > componentSchema.getPreferredPhysMaxValue())
                physicalValue = (float)(componentSchema.getPreferredPhysMaxValue() + componentSchema.getPreferredPhysMinValue()) / 2;
            break;
        case ALWAYS_KEEP:
            break;
        }
        fireStateChanged();
    }

    @Override
    public void setComponentSchema(String componentName)
    {
        if (fireOnUpdate) {
            super.setComponentSchema(componentName);
            updateComponentValue();
        }
    }

    @Override
    public void setContainer(DataContainer container)
    {
        setContainerSchema(container.getSchema());
    }

    @Override
    protected void localUpdateUI()
    {
       if (ui != null && ui instanceof ComponentValueUI)
          valUI = (ComponentValueUI)ui;
       valUI.setListener(uiValueChangedListener);
    }

    @Override
    public void reset()
    {
        if (componentSchema != null)
        {
            setPhysicalValue((componentPhysMin + componentPhysMax) / 2);
            if (valUI != null)
            {
                valUI.updateRange();
                valUI.updateValue();
            }
        }
    }

    @Override
    protected void localUpdateComponent()
    {
        if (componentSchema == null)
            return;
        if (oldComponentNameInvalid ||
            physicalValue < componentPhysMin ||
            physicalValue > componentPhysMax)
            setPhysicalValue((componentPhysMin + componentPhysMax) / 2);
        if (valUI == null)
           return;
        SwingInstancer.swingRunLater(() -> {
            valUI.updateRange();
            valUI.updateValue();
        });

    }

    @Override
    public String toString()
    {
        if (componentSchema == null)
            return "[componentValue][/componentValue]";
        return "[componentValue]"+componentSchema.getName()+
                String.format(": %7.3f", physicalValue) +
               "[/componentValue]";
    }

    @Override
    public void updateFromString(String s)
    {
        s = s.trim();
        if (s == null || s.isEmpty() ||
           !s.startsWith("[componentValue]") || !s.endsWith("[/componentValue]"))
            return;
        String[] c = s.substring("[componentValue]".length(), s.length() - "[/componentValue]".length()).trim().split(" *:* +");
        setComponentSchema(c[0]);
        setPhysicalValue(Float.parseFloat(c[1]));
    }
}
