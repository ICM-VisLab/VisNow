/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.gui.ComponentBasedUI.array;


import java.awt.BorderLayout;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeatureUI;
import org.visnow.vn.lib.gui.FloatArrayEditor;

/**
 *
 * @author know
 */
public class ComponentValuesArrayUI extends ComponentFeatureUI
{

    protected FloatArrayEditor valuesArrayEditor = new FloatArrayEditor();
    protected ComponentValuesArray param;

    public ComponentValuesArrayUI()
    {
        super();
        add(valuesArrayEditor, BorderLayout.CENTER);
        valuesArrayEditor.addChangeListener((ChangeEvent e) -> {
            valChanged();
        });
    }

    public void setComponentValuesArray(ComponentValuesArray param)
    {
        userUpdate = false;
        this.param = param;
        super.setComponentFeature(param);
        param.setUI(this);
        valuesArrayEditor.setMinMax(param.getComponentPhysMin(),
                                    param.getComponentPhysMax());
        userUpdate = true;
    }

    public boolean isAdjusting()
    {
        return valuesArrayEditor.isAdjusting();
    }

    public float[] getValues()
    {
        return valuesArrayEditor.getThresholds();
    }

    void updateValue()
    {
        userUpdate = false;
        if (param != null)
            valuesArrayEditor.setThresholds(param.getPhysicalValues());
        userUpdate = true;
    }

    public void setMinMax(float min, float max)
    {
        valuesArrayEditor.setMinMax(min, max);
    }

    public void setStartSingle(boolean single)
    {
        valuesArrayEditor.setStartSingle(single);
    }

    public void setPreferredCount(int preferredCount)
    {
        valuesArrayEditor.setMaxRangeCount(preferredCount);
    }

    ChangeListener listener = null;

    public void setListener(ChangeListener listener)
    {
        this.listener = listener;
    }

    public void clearListener()
    {
        listener = null;
    }

    private void valChanged()
    {
        if (listener != null && userUpdate)
            listener.stateChanged(new ChangeEvent(this));
    }


    @Override
    protected void updateUIToNewComponent(boolean isNull)
    {
        valuesArrayEditor.setEnabled(!isNull);
    }


}
