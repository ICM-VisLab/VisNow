/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.gui.cropUI;

import java.util.ArrayList;
import java.util.List;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterName;
import org.visnow.vn.engine.core.ParameterProxy;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class CropUIShared
{
    private static final String NAMESPACE = "CropUI.";
    //Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    //
    //Specification:
    //low.length == high.length == 3,
    //0 <= LOW[i] < HIGH[i] < META_FIELD_DIMENSION_LENGTHS[i]
    //(when i < META_FIELD_DIMENSION_LENGTHS.length)
    public static final ParameterName<int[]> LOW = new ParameterName(NAMESPACE + "Low values");
    public static final ParameterName<int[]> HIGH = new ParameterName(NAMESPACE + "High values");

    //META PARAMETERS: (data that should be not set in GUI nor passed from GUI to logic
    //not null array of integers, length = 1..3, values >=2
    public static final ParameterName<int[]> META_FIELD_DIMENSION_LENGTHS = new ParameterName(NAMESPACE + "META dimension lengths");

    public static List<Parameter> createDefaultParametersAsList()
    {
        return new ArrayList<Parameter>()
        {
            {
                add(new Parameter<>(META_FIELD_DIMENSION_LENGTHS, new int[]{100, 100, 100}));
                add(new Parameter<>(LOW, new int[]{0, 0, 0}));
                add(new Parameter<>(HIGH, new int[]{99, 99, 99}));
            }
        };
    }

    private static void validateLowHigh(int[] low, int[] high, int[] dimensionLengths)
    {
        for (int i = 0; i < dimensionLengths.length; i++) {
            low[i] = Math.min(Math.max(low[i], 0), dimensionLengths[i] - 2);
            high[i] = Math.min(Math.max(high[i], low[i] + 1), dimensionLengths[i] - 1);
        }
    }

    /**
     * Validates low and high ranges to meet:
     * 0 &le; CROP_UI_LOW[i] &le; CROP_UI_HIGH[i] &lt; META_CROP_UI_FIELD_DIMENSION_LENGTHS[i]
     */
    public static void validateLowHighRange(ParameterProxy p)
    {
        int[] dimensionLengths = p.get(META_FIELD_DIMENSION_LENGTHS);
        int[] low = p.get(LOW);
        int[] high = p.get(HIGH);
        validateLowHigh(low, high, dimensionLengths);
        p.set(LOW, low,
              HIGH, high);
    }

    /**
     * Resets low and high ranges to meet:
     * 0 == CROP_UI_LOW[i], CROP_UI_HIGH[i] = META_CROP_UI_FIELD_DIMENSION_LENGTHS[i]
     */
    public static void resetLowHighRange(ParameterProxy p)
    {
        int[] dimensionLengths = p.get(META_FIELD_DIMENSION_LENGTHS);

        int[] high = new int[3];
        for (int i = 0; i < dimensionLengths.length; i++)
            high[i] = dimensionLengths[i] - 1;

        p.set(LOW, new int[]{0, 0, 0},
              HIGH, high);
    }

    /**
     * Rescales low, high and dimension_lengths, by factor dimensionLengths/oldDimensionLengths
     */
    public static void rescaleAll(ParameterProxy p, int[] oldDimensionLengths)
    {
        int[] dimensionLengths = p.get(META_FIELD_DIMENSION_LENGTHS);
        int[] low = p.get(LOW);
        int[] high = p.get(HIGH);

        for (int i = 0; i < dimensionLengths.length; i++) {
            float scale = (float) dimensionLengths[i] / oldDimensionLengths[i];
            low[i] *= scale;
            high[i] *= scale;
        }

        validateLowHigh(low, high, dimensionLengths);

        p.set(LOW, low,
              HIGH, high);
    }
}
