/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.gui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterName;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class DownsizeUIShared
{
    private static final String NAMESPACE = "DownsizeUI.";

    //Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    //
    //Specification:
    //length == 1, 2 or 3
    //each >=1
    public static final ParameterName<int[]> DOWNSIZE = new ParameterName(NAMESPACE + "Downsize");

    //length == DOWNSIZE.length
    //each >=1
    public static final ParameterName<int[]> META_DEFAULT_DOWNSIZE = new ParameterName(NAMESPACE + "META Default downsize");

    public static List<Parameter> createDefaultParametersAsList()
    {
        return new ArrayList<Parameter>()
        {
            {
                add(new Parameter<>(DOWNSIZE, new int[]{3, 3, 3}));
                add(new Parameter<>(META_DEFAULT_DOWNSIZE, new int[]{3, 3, 3}));
            }
        };
    }

    private static final int PREFERRED_NODE_NUMER = 1000_000;
    
    public static int[] getSmartDefault(int[] fieldDimensions)
    {
        return getSmartDefault(fieldDimensions, PREFERRED_NODE_NUMER);
    }
    
    public static int[] getSmartDefault(int[] fieldDimensions, int preferredNodeNumber)
    {
        long nodeNumber = 1;
        for (int i = 0; i < fieldDimensions.length; i++)
            nodeNumber *= (long)fieldDimensions[i];

        int singleDownsize = (int) Math.ceil(Math.pow((double) nodeNumber / preferredNodeNumber, 1.0 / fieldDimensions.length));

        int[] downsize = new int[fieldDimensions.length];
        Arrays.fill(downsize, singleDownsize);

        return downsize;
    }
}
