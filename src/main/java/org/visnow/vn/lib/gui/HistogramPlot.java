/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;
import org.visnow.jscic.utils.EngineeringFormattingUtils;

/**
 * Plots histogram and shows tool tips for bin under the mouse pointer.
 * <p>
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class HistogramPlot extends JPanel
{

    private long[] histogram = null;
    private Double minValue;
    private Double maxValue;
    private boolean logScale = false;
    private int margin = 1;

    public HistogramPlot()
    {
        //mouse listener for tooltip
        addMouseMotionListener(new MouseAdapter()
        {
            @Override
            public void mouseMoved(MouseEvent e)
            {
                if (histogram != null) {
                    int plotWidth = getWidth() - 2 * margin;
                    double xInPlot = 0.5 + Math.min(plotWidth - 1, Math.max(0, e.getX() - margin));
                    double posInPlot = (double) xInPlot / plotWidth; //[0 .. 1] in fact (0 .. 1)

                    int binCount = histogram.length;
                    int selectedBin = (int) Math.floor(posInPlot * binCount);

                    long histogramValue = histogram[selectedBin];
                    if (maxValue == null)
                        setToolTipText("count: " + histogramValue);
                    else {
                        double binStart = minValue;
                        double binEnd = minValue;

                        if (maxValue != minValue) {
                            binStart = minValue + selectedBin * (maxValue - minValue) / binCount;
                            binEnd = minValue + (selectedBin + 1) * (maxValue - minValue) / binCount;
                        }
                        String[] labels = EngineeringFormattingUtils.formatInContext(new double[]{binStart, binEnd});
                        setToolTipText("<html>count: " + histogramValue + "<br/>at: [" + labels[0] + ", " + labels[1] + "]");
                    }
                }
            }
        });
    }

    /**
     * Assuming that:
     * <li> one pixel "has width" - this is important for proper calculations of position and width of bins
     * <li> input data don't "have width" (!)
     * (so byte values with min = 0, max = 255 when split into 256 bins are split into [0,1*255/256], [1*255/256, 2*255/256], etc).
     */
    public void paint(Graphics g)
    {
        if (histogram != null) {
            int windowHeight = getHeight();
            int windowWidth = getWidth();
            int plotWidth = getWidth() - 2 * margin;
            int plotHeight = getHeight() - margin;

            g.setColor(Color.WHITE);
            g.fillRect(0, 0, windowWidth, windowHeight);
            long max = 0;
            for (int i = 0; i < histogram.length; i++) max = Math.max(histogram[i], max);

            g.setColor(Color.BLUE);
            for (int i = 0; i < histogram.length; i++) {
                int xPosPx = (int) Math.round(plotWidth * (double) i / histogram.length - 0.5); //-0.5 for proper rounding to pixels
                int xPosNextPx = i == histogram.length - 1
                        ? plotWidth - 1
                        : (int) Math.round(plotWidth * (double) (i + 1) / histogram.length - 0.5);
                xPosPx = Math.max(0, Math.min(plotWidth - 1, xPosPx));
                xPosNextPx = Math.max(0, Math.min(plotWidth - 1, xPosNextPx));

                int heightPx = 1 + (int) Math.round((plotHeight - 1) * (logScale ? Math.log1p(histogram[i]) / Math.log1p(max) : (double) histogram[i] / max));
                g.drawRect(margin + xPosPx, windowHeight - heightPx, xPosNextPx - xPosPx, heightPx - 1);
            }
        }
    }

    /**
     * Sets histogram data.
     * Use this setter to show only bin value in tool tip.
     */
    public void setData(long[] data, boolean logScale)
    {
        setData(data, logScale, null, null);
    }

    /**
     * Sets histogram data.
     * Use this setter to show bin value and bin range in tool tip.
     */
    public void setData(long[] data, boolean logScale, Double minDataValue, Double maxDataValue)
    {
        this.histogram = data;
        this.minValue = minDataValue;
        this.maxValue = maxDataValue;
        this.logScale = logScale;
        repaint();
    }
}
