/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.gui.ComponentBasedUI.range;

import java.util.function.Predicate;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.vn.gui.events.FloatPairModificationEvent;
import org.visnow.vn.gui.events.FloatPairModificationListener;
import org.visnow.vn.lib.gui.ComponentBasedUI.ComponentFeature;
import org.visnow.vn.lib.gui.ComponentBasedUI.PreferredExtentChangeBehavior;
import static org.visnow.vn.lib.gui.ComponentBasedUI.PreferredExtentChangeBehavior.ALWAYS_RESET;
import static org.visnow.vn.lib.gui.ComponentBasedUI.PreferredExtentChangeBehavior.KEEP_IF_INSIDE;
import org.visnow.vn.lib.gui.ComponentBasedUI.PreferredSubrangeBehavior;
import static org.visnow.vn.lib.gui.ComponentBasedUI.PreferredSubrangeBehavior.RESET_LOW_UP;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ComponentSubrange extends ComponentFeature implements Cloneable
{
    private float physLow = 0, physUp = 1;
    private double physRange = 1;
    private double compRange = 1;
    protected ComponentSubrangeUI rangeUI = null;
    /**
     * indicates how the low / up values are set when component preferredMinValue/preferredMaxValue change
     */
    protected PreferredSubrangeBehavior preferredSubrangeBehavior = RESET_LOW_UP;

    protected FloatPairModificationListener uiValueChangedListener =
           new FloatPairModificationListener()
           {

               @Override
               public void floatPairValueChanged(FloatPairModificationEvent e)
               {
                   if (componentSchema == null || e.isAdjusting() && !continuousUpdate)
                       return;
                   physLow = (float) e.getValX();
                   physUp =  (float) e.getValY();
                   fireStateChanged();
               }
           };


    public ComponentSubrange()
    {
       super();
       preferredExtentChangeBehavior = KEEP_IF_INSIDE;
    }

    public ComponentSubrange(boolean pseudoComponentsAllowed, String[] preferedItemNames,
                          boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                          boolean addNull, boolean prefereNull, boolean continuousUpdate,
                          PreferredExtentChangeBehavior preferredExtentChangeBehavior)
    {
        super(pseudoComponentsAllowed, preferedItemNames,
              numericsOnly, scalarsOnly, vectorsOnly, addNull, prefereNull,
              continuousUpdate, preferredExtentChangeBehavior);
    }

    public ComponentSubrange(boolean pseudoComponentsAllowed,
                            boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate,
                            PreferredExtentChangeBehavior preferredExtentChangeBehavior)
    {
        this(pseudoComponentsAllowed, new String[]{}, numericsOnly, scalarsOnly, vectorsOnly,
             addNull, prefereNull, continuousUpdate, preferredExtentChangeBehavior);
    }

    public ComponentSubrange(boolean pseudoComponentsAllowed,
                            boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate)
    {
        this(pseudoComponentsAllowed, new String[]{}, numericsOnly, scalarsOnly, vectorsOnly,
             addNull, prefereNull, continuousUpdate, ALWAYS_RESET);
    }

    public ComponentSubrange(boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate)
    {
        this(false, numericsOnly, scalarsOnly, vectorsOnly,
             addNull, prefereNull, continuousUpdate);
    }

    public ComponentSubrange(boolean pseudoComponentsAllowed, String[] preferedItemNames,
                            boolean numericsOnly, boolean scalarsOnly, boolean vectorsOnly,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate)
    {
        this(pseudoComponentsAllowed, preferedItemNames, numericsOnly, scalarsOnly, vectorsOnly,
             addNull, prefereNull, continuousUpdate, ALWAYS_RESET);
    }

    /**
     * Standard constructor with all behavior definable
     * @param schemaAcceptor           lambda schema -> schema acceptable containing all component selection details
     * @param pseudoComponentsAllowed  true if indices and coordinates are allowed (see e.g. texture coordinates
     * @param preferedItemNames        some general component names can be preferred as initial selection
     * @param addNull                  true if null component is admissible (e.g. null as data mapping component)
     * @param prefereNull              true if initial selection is null component
     * @param continuousUpdate         if true, sliders in subclasses will notify listeners dynamically 
     * @param preferredExtentChangeBehavior subclass action taken if data daynamically change
     */
    public ComponentSubrange(Predicate<DataArraySchema> schemaAcceptor,
                            boolean pseudoComponentsAllowed, String[] preferedItemNames,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate,
                            PreferredExtentChangeBehavior preferredExtentChangeBehavior)
    {
        super(schemaAcceptor, pseudoComponentsAllowed, preferedItemNames, addNull, prefereNull, continuousUpdate, preferredExtentChangeBehavior);
    }

    /**
     * Simplified constructor without preferred component names selection and without pseudocomponents
     * @param schemaAcceptor           lambda schema -> schema acceptable containing all component selection details
     * @param addNull                  true if null component is admissible (e.g. null as data mapping component)
     * @param prefereNull              true if initial selection is null component
     * @param continuousUpdate         if true, sliders in subclasses will notify listeners dynamically 
     * @param preferredExtentChangeBehavior subclass action taken if data daynamically change
     */
    public ComponentSubrange(Predicate<DataArraySchema> schemaAcceptor,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate,
                            PreferredExtentChangeBehavior preferredExtentChangeBehavior)
    {
        this(schemaAcceptor, false, null, addNull, prefereNull, continuousUpdate, preferredExtentChangeBehavior);
    }
    
     /**
     * Simplified constructor with proper components, no preferred names and default range reset behavior in subclasses
     * @param schemaAcceptor           lambda schema -> schema acceptable containing all component selection details
     * @param addNull                  true if null component is admissible (e.g. null as data mapping component)
     * @param prefereNull              true if initial selection is null component
     * @param continuousUpdate         if true, sliders in subclasses will notify listeners dynamically 
     */
    public ComponentSubrange(Predicate<DataArraySchema> schemaAcceptor,
                            boolean addNull, boolean prefereNull, boolean continuousUpdate)
    {
        this(schemaAcceptor, addNull, prefereNull, continuousUpdate, ALWAYS_RESET);
    }
    
     /**
     * Simplified constructor with proper components, no null component allowed, no preferred names and default range reset behavior in subclasses
     * @param schemaAcceptor           lambda schema -> schema acceptable containing all component selection details
     * @param continuousUpdate         if true, sliders in subclasses will notify listeners dynamically 
     */
    public ComponentSubrange(Predicate<DataArraySchema> schemaAcceptor, 
                            boolean continuousUpdate)
    {
        this(schemaAcceptor, false, false, false);
    }

      /**
     * Simplest constructor setting lambda only; sliders in subclasses will remain silent when dragged
     * @param schemaAcceptor  lambda schema -> schema acceptable containing all component selection details
     */
   public ComponentSubrange(Predicate<DataArraySchema> schemaAcceptor)
    {
        this(schemaAcceptor, false);
    }
    

    @Override
    public ComponentSubrange clone()
    {
        ComponentSubrange cloned = new ComponentSubrange(numericsOnly, scalarsOnly, vectorsOnly,
                                                         addNull, prefereNull, continuousUpdate);
        cloned.preferredExtentChangeBehavior = preferredExtentChangeBehavior;
        cloned.setContainerSchema(containerSchema);
        cloned.setComponentSchema(componentSchema);
        cloned.physLow = physLow;
        cloned.physUp = physUp;
        return cloned;
    }

    public void copyValuesFrom(ComponentSubrange src)
    {
        super.copyValuesFrom(src);
        updateComponentSubrange(false);
        setPhysicalLowUp(src.physLow, src.physUp);
        if (ui != null && ui instanceof ComponentSubrangeUI) {
            SwingInstancer.swingRunLater(new Runnable() {
                @Override
                public void run() {
                    ((ComponentSubrangeUI) ui).updateRange();
                    ((ComponentSubrangeUI) ui).updateSubrange();
                }
            });
        }
    }

    @Override
    protected void localUpdateUI()
    {
        if (ui != null && ui instanceof ComponentSubrangeUI)
            SwingInstancer.swingRunLater(new Runnable() {
                @Override
                public void run() {
                    rangeUI = (ComponentSubrangeUI) ui;
                    rangeUI.setListener(uiValueChangedListener);
                }
            });
    }

    public void setLowUp(float low, float up)
    {
        if (componentSchema == null)
            return;
        physLow = (float) componentSchema.dataRawToPhys(low);
        physUp =  (float) componentSchema.dataRawToPhys(up);

        if (rangeUI != null)
            SwingInstancer.swingRunLater(new Runnable() {
                @Override
                public void run() {
                    rangeUI.updateSubrange();
                }
            });
        fireStateChanged();
    }

    public void setPhysicalLowUp(float physLow, float physUp)
    {
        if (componentSchema == null)
            return;
        this.physLow = physLow;
        this.physUp = physUp;

        if (rangeUI != null)
            SwingInstancer.swingRunLater(new Runnable() {
                @Override
                public void run() {
                    rangeUI.updateSubrange();
                }
            });
        fireStateChanged();
    }

    public float getLow()
    {
        return physToVal(physLow);
    }

    public float getUp()
    {
        return physToVal(physUp);
    }

    public float getPhysicalLow()
    {
        return physLow;
    }

    public float getPhysicalUp()
    {
        return physUp;
    }

    public void updateComponentSubrange()
    {
        updateComponentSubrange(true);
    }

    public void updateComponentSubrange(boolean fire)
    {

        if (oldComponentNameInvalid) {
            physLow = componentPhysMin;
            physUp = componentPhysMax;
        }
//        switch (policy)
//        {
//        case ALWAYS_RESET:
//            physLow = componentPhysMin;
//            physUp = componentPhysMax;
//            break;
//        case KEEP_IF_INSIDE:
//            if (physLow < componentPhysMin)
//                physLow = componentPhysMin;
//            if (physUp > componentPhysMax)
//                physUp = componentPhysMax;
//            break;
//        case KEEP_IF_BRACES:
//            if (physLow > componentPhysMin)
//                physLow = componentPhysMin;
//            if (physUp < componentPhysMax)
//                physUp = componentPhysMax;
//            break;
//        case EXTEND_IF_INTERSECTS:
//            if (physLow > componentPhysMin ||
//                physUp < componentPhysMax){
//                physLow = componentPhysMin;
//                physUp = componentPhysMax;
//            }
//            break;
//        case ALWAYS_KEEP:
//            break;
//        }
        if (componentSchema != null) {
            physRange = componentSchema.getPreferredPhysMaxValue() - componentSchema.getPreferredPhysMinValue();
            compRange = componentSchema.getPreferredMaxValue() - componentSchema.getPreferredMinValue();
            if (physRange <= 0) physRange = 1;
            if (compRange <= 0) compRange = 1;
            if (componentSchema.isAutoResetMapRange() || oldComponentNameInvalid) {
//                if (lowUpPolicy == RESET_LOW_UP)
                setPhysicalLowUp(componentPhysMin, componentPhysMax);
//                else
//                    setPhysicalLowUp(Math.max(physLow, componentPhysMin), Math.min(physUp, componentPhysMax));
            }
        } else {
            physLow = 0;
            physUp = 1;
        }
        if (physLow >= physUp) {
            float m = .5f * (physLow + physUp);
            physLow = m - 1;
            physUp = m + 1;
        }
        if (fire && active)
            fireStateChanged();
    }

    @Override
    public void reset()
    {
        if (componentSchema != null) {
            physLow = (float)componentSchema.getPhysMinValue();
            physUp  = (float)componentSchema.getPhysMaxValue();
            if (rangeUI != null) {
                SwingInstancer.swingRunLater(new Runnable() {
                    @Override
                    public void run() {
                        rangeUI.updateRange();
                        rangeUI.updateSubrange();
                    }
                });
            }
        }
    }

    @Override
    public void localUpdateComponent()
    {
        if (componentSchema != null) {
            updateComponentSubrange();
            if (rangeUI != null) {
                SwingInstancer.swingRunLater(new Runnable() {
                    @Override
                    public void run() {
                        rangeUI.updateRange();
                        rangeUI.updateSubrange();
                    }
                });
            }
        }
    }

    @Override
    public String toString()
    {
        if (componentSchema == null)
            return "";
        return componentSchema.getName() +
            String.format(": %7.3f %7.3f", physLow, physUp);
    }

    @Override
    public void updateFromString(String s)
    {
        s = s.trim();
        if (s == null || s.isEmpty()) {
            componentSchema = null;
            return;
        }
        String[] c = s.trim().split(" *:* +");
        setComponentSchema(c[0]);
        setPhysicalLowUp(Float.parseFloat(c[1]), Float.parseFloat(c[2]));
    }

    public String[] valuesToStringArray()
    {
        if (componentSchema == null)
            return new String[]{"null"};
        return new String[]{componentSchema.exportSchemaToString(),
                            String.format("selected range: %7.3f %7.3f", physLow, physUp)};
    }

    public void restoreFromStringArray(String[] s)
    {
        setComponentSchema((s[0].trim().split(" *:* +")[0]));
        if (s.length > 1) {
            String[] c = s[1].trim().split(" *:* +");
            setPhysicalLowUp(Float.parseFloat(c[2]), Float.parseFloat(c[3]));
        }
    }

    public void setLowUpPolicy(PreferredSubrangeBehavior lowUpPolicy)
    {
        this.preferredSubrangeBehavior = lowUpPolicy;
    }

    public boolean isAdjusting()
    {
        if (ui == null)
            return false;
        return ((ComponentSubrangeUI) ui).isAdjusting();
    }

    public boolean isUnmodified()
    {
        return isComponentDefault() && physLow == componentPhysMin && physUp == componentPhysMax;
    }
}
