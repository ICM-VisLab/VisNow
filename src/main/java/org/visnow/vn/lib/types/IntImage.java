/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.lib.types;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class IntImage
{

    private int width;

    public int getWidth()
    {
        return width;
    }

    private int height;

    public int getHeight()
    {
        return height;
    }

    private Integer[][] tab;

    public int getCell(int x, int y)
    {
        //System.out.println(x%width);
        //System.out.println(y%height);
        return tab[(x + width) % width][(y + height) % height];
    }

    public Integer[][] getContent()
    {
        return tab;
    }

    public void setValueAt(int x, int y, int value)
    {
        tab[(x + width) % width][(y + height) % height] = value;
    }

    public IntImage(IntImage from)
    {
        this(from.getWidth(), from.getHeight());
        for (int i = 0; i < width; ++i)
            for (int j = 0; j < height; ++j)
                tab[i][j] = from.getCell(i, j);
    }

    /**
     * Creates a new instance of IntImage
     */
    public IntImage()
    {
        this(32, 32);
    }

    public IntImage(int width, int height)
    {
        this.width = width;
        this.height = height;
        tab = new Integer[width][height];
        for (int i = 0; i < width; ++i)
            for (int j = 0; j < height; ++j)
                tab[i][j] = 0;
    }

    @Override
    public String toString()
    {
        return "IntImage[" + width + "," + height + "]" + "((" + super.toString() + "))\n";
    }
}
