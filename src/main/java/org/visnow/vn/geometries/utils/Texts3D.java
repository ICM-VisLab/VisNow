/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.utils;

import org.jogamp.java3d.Billboard;
import org.jogamp.java3d.BoundingSphere;
import org.visnow.vn.geometries.objects.TextBillboard;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.parameters.FontParams;
import static org.apache.commons.math3.util.FastMath.*;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Texts3D extends OpenBranchGroup
{

    protected float[] textCoords;
    protected String[] texts;
    protected FontParams fontParams;
    protected float[][] extents = {{Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE},
                                   {-Float.MAX_VALUE, -Float.MAX_VALUE, -Float.MAX_VALUE}};

    public Texts3D(float[] textCoords, String[] texts, FontParams fontParams)
    {
        this.textCoords = textCoords;
        this.texts = texts;
        this.fontParams = fontParams;
        BoundingSphere bSphere = new BoundingSphere();
        int nGlyphs = texts.length;
        float[] c = new float[3];
        for (int i = 0; i < nGlyphs; i++) {
            System.arraycopy(textCoords, 3 * i, c, 0, 3);
            for (int j = 0; j < 3; j++) {
                extents[0][j] = min(c[j], extents[0][j]);
                extents[1][j] = max(c[j], extents[1][j]);
            }
            addChild(TextBillboard.createBillboard(texts[i],
                                                   fontParams, c, Billboard.ROTATE_ABOUT_POINT, bSphere));
        }
    }

    public float[][] getExtents()
    {
        return extents;
    }

}
