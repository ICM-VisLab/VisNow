/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.parameters;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.engine.core.ParameterName;

/**
 *
 * @author norkap
 */


public class FontParamsShared {
    
    public static final ParameterName<Boolean> FONT_PARAMS_THREE_DIMENSIONAL = new ParameterName("three dimensional");
    
    // % from 0.1 to 100
    public static final ParameterName<Float> FONT_PARAMS_GLYPHS_SIZE = new ParameterName("glyphs size");
    
    public static final ParameterName<Float> FONT_PARAMS_PRECISION = new ParameterName("precision");
    
    public static final ParameterName<String> FONT_PARAMS_FONT_NAME = new ParameterName("font name");
    
    public static final ParameterName<Integer> FONT_PARAMS_FONT_TYPE = new ParameterName("font type");
    
    public static final ParameterName<Integer> FONT_PARAMS_COLOR = new ParameterName("color");
    
    public static final ParameterName<Integer> FONT_PARAMS_BRIGHTNESS = new ParameterName("brightness");
    
    public static List<Parameter> createDefaultParametersAsList()
    {
        
        List<Parameter> l = new ArrayList<>();
        l.add(new Parameter<>(FONT_PARAMS_THREE_DIMENSIONAL, false));
        l.add(new Parameter<>(FONT_PARAMS_GLYPHS_SIZE, .012f));
        l.add(new Parameter<>(FONT_PARAMS_PRECISION, 3.0f));
        l.add(new Parameter<>(FONT_PARAMS_FONT_NAME, "sans-serif"));
        l.add(new Parameter<>(FONT_PARAMS_FONT_TYPE, Font.PLAIN));
        l.add(new Parameter<>(FONT_PARAMS_COLOR, Integer.MAX_VALUE));
        l.add(new Parameter<>(FONT_PARAMS_BRIGHTNESS, 50));
        
        return l;
    }
}
