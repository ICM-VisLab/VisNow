/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.parameters;

import java.util.ArrayList;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.vn.geometries.gui.TransparencyEditor.TransparencyEditor;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEvent;
import org.visnow.vn.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrange;
import static java.lang.Math.*;
import java.util.Arrays;
import java.util.Vector;
import org.apache.log4j.Logger;
import org.visnow.vn.lib.utils.StringUtils;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class TransparencyParams implements Cloneable
{
    public static final int RAMP         = 0;
    public static final int REVERSE_RAMP = 1;
    public static final int VALLEY       = 2;
    public static final int PEAK         = 3;
    public static final int COSINE       = 4;
    public static final int SINE         = 5;
    public static final int FLAT         = 6;
    
    
    private static final int HISTO_LENGTH = 256;
    private static final float HISTO_DELTA = 1.f / HISTO_LENGTH;
    
    private final ComponentSubrange componentRange = new ComponentSubrange();

    private int mapType = RAMP;
    private float exponent = 1;
    private int nPeriods = 3;
    private float[] prototypeMap = new float[256];
    private float multiplier = .5f;
    private int[] bgrRange = new int[]{0, 255};
    private final int[] map = new int[HISTO_LENGTH];
    protected boolean active = true;
    protected boolean lastActive = true;
    protected RenderEventListener listener = null;
    private boolean adjusting = false;
    private TransparencyEditor ui = null;
    private boolean transparencyManuallyEdited = false;
    private static final Logger LOGGER = Logger.getLogger(TransparencyParams.class);

    public TransparencyParams()
    {
        for (int i = 0; i < prototypeMap.length; i++)
            prototypeMap[i] = i * HISTO_DELTA;
        updateMap();
        componentRange.setContinuousUpdate(false);
        componentRange.setAddNull(true);
        componentRange.setComponentSchema(-1);
    }
    
    public void copyValuesFrom(TransparencyParams src)
    {
        boolean tmpActive = active;
        active = false;
        mapType = src.mapType;
        exponent = src.exponent;
        nPeriods = src.nPeriods;
        multiplier = src.multiplier;
        bgrRange = src.bgrRange.clone();
        System.arraycopy(src.getPrototypeMap(), 0, prototypeMap, 0, prototypeMap.length);
        componentRange.copyValuesFrom(src.getComponentRange());
        if (ui != null)
            ui.updateDataValuesFromParams(true);
        active = tmpActive;
    }
    
    public String[] valuesToStringArray()
    {
        Vector<String> res = new Vector<>();
        String[] trComp = componentRange.valuesToStringArray();
        res.add("componentRange {");
        for (String s : trComp) 
            res.add("    " + s);
        res.add("}");
        res.add("mapType: " + mapType);
        res.add("exponent: " + exponent);
        res.add("nPeriods: " + nPeriods);
        res.add("multiplier: " + multiplier);
        res.add("range: " + bgrRange[0] + " " + bgrRange[1]);
        if (transparencyManuallyEdited) {
            res.add("raw_transparency {");
            for (int i = 0, k = 0; i < (prototypeMap.length + 15) / 16; i++) {
                StringBuilder strB = new StringBuilder("    ");
                for (int j = 0; j < 16 && k < prototypeMap.length; j++, k++) 
                    strB.append(String.format("%5.3f ", prototypeMap[k]));
                res.add(strB.toString());
            }
            res.add("}");
        }
        
        String[] r = new String[res.size()];
        for (int i = 0; i < r.length; i++) 
           r[i] = res.get(i);
        return r;
    }
    
    public void restoreValuesFrom(String[] saved)
    {
        LOGGER.debug("restoring transparencyParams");
        for (int i = 0; i < saved.length; i++) {
            if (saved[i].trim().equals("componentRange {")) {
                String[] currentBlock = StringUtils.findBlock(saved, i);
                componentRange.restoreFromStringArray(currentBlock);
                i += currentBlock.length + 1;
                continue;
            }
            if (saved[i].trim().startsWith("mapType:")) {
                try {
                    mapType = Integer.parseInt(saved[i].trim().split(" *:* +")[1]);
                    createPrototypeMap();
                } catch (NumberFormatException e) {
                }
                continue;
            }
            if (saved[i].trim().startsWith("exponent:")) {
                try {
                    exponent = Float.parseFloat(saved[i].trim().split(" *:* +")[1]);
                    createPrototypeMap();
                } catch (NumberFormatException e) {
                }
                continue;
            }
            if (saved[i].trim().startsWith("nPeriods:")) {
                try {
                    nPeriods = Integer.parseInt(saved[i].trim().split(" *:* +")[1]);
                    createPrototypeMap();
                } catch (NumberFormatException e) {
                }
                continue;
            }
            if (saved[i].trim().startsWith("multiplier:")) {
                try {
                    multiplier = Float.parseFloat(saved[i].trim().split(" *:* +")[1]);
                    updateMap();
                } catch (NumberFormatException e) {
                }
                continue;
            }
            if (saved[i].trim().startsWith("range:")) {
                try {
                    String[] sp = saved[i].trim().split(" *:* +");
                    bgrRange[0] = Integer.parseInt(sp[1]);
                    bgrRange[1] = Integer.parseInt(sp[2]);
                    updateMap();
                } catch (NumberFormatException e) {
                }
                continue;
            }
            if (saved[i].trim().startsWith("raw_transparency {")) {
                String[] currentBlock = StringUtils.findBlock(saved, i);
                for (int k = 0, l = 0; k < currentBlock.length; k++) {
                    String[] items = currentBlock[k].trim().split(" *:* +");
                    for (int m = 0; m < items.length; m++, l++)
                        prototypeMap[l] = Float.parseFloat(items[m]);
                }
                i += currentBlock.length + 1;
            }
        }
        if (ui != null)
            ui.updateDataValuesFromParams(true);
        fireStateChanged();
    }


    public void setUi(TransparencyEditor ui)
    {
        this.ui = ui;
    }

    public float[] getPrototypeMap()
    {
        return prototypeMap;
    }
    
    public int getMapType()
    {
        return mapType;
    }

    public void setMapType(int mapType)
    {
        this.mapType = mapType;
        createPrototypeMap();
    }

    public int[] getBgrRange()
    {
        return bgrRange;
    }

    public void setBgrRange(int[] bgrRange)
    {
        this.bgrRange = bgrRange;
        bgrRange[0] = Math.max(0, bgrRange[0]);
        bgrRange[1] = Math.min(255, bgrRange[1]);
        if (bgrRange[0] >= bgrRange[1]) {
            int t = (bgrRange[0] + bgrRange[1]) / 2;
            bgrRange[0] = Math.max(0,   t - 1);
            bgrRange[1] = Math.min(255, t + 1);
        }
        updateMap();
    }
    
    public float getExponent()
    {
        return exponent;
    }

    public int getnPeriods()
    {
        return nPeriods;
    }

    public void setExponent(float exp)
    {
        this.exponent = exp;
        createPrototypeMap();
    }

    public float getMultiplier()
    {
        return multiplier;
    }

    public void setMultiplier(float multiplier)
    {
        this.multiplier = multiplier;
        updateMap();
    }

    public void setPrototypeMap(float[] prototypeMap)
    {
        this.prototypeMap = prototypeMap;
        updateMap();
    }

    public void setNPeriods(int nPeriods)
    {
        this.nPeriods = nPeriods;
        createPrototypeMap();
    }

    public void setTransparencyManuallyEdited(boolean transparencyManuallyEdited)
    {
        this.transparencyManuallyEdited = transparencyManuallyEdited;
    }
    
    public void createPrototypeMap()
    {
        switch (mapType) {
            case 0:
                for (int i = 0; i < 256; i++)
                    prototypeMap[i] = (float) Math.pow(HISTO_DELTA * i, exponent);
                break;
            case 1:
                for (int i = 0; i < 256; i++)
                    prototypeMap[i] = (float) Math.pow(HISTO_DELTA * (255 - i), exponent);
                break;
            case 2:
                for (int i = 0; i < 128; i++)
                    prototypeMap[128 + i] = prototypeMap[127 - i] = (float) Math.pow(i / 128., exponent);
                break;
            case 3:
                for (int i = 0; i < 128; i++)
                    prototypeMap[i] = prototypeMap[255 - i] = (float) Math.pow(i / 128., exponent);
                break;
            case 4:
                double d = 2 * nPeriods * PI / (prototypeMap.length - 1.);
                for (int i = 0; i < prototypeMap.length; i++) {
                    double u = ((1 + cos(i * d)) / 2);
                    prototypeMap[i] = (float) Math.pow(u, exponent);
                }
                break;
            case 5:
                d = 2 * nPeriods * PI / (prototypeMap.length - 1.);
                for (int i = 0; i < prototypeMap.length; i++) {
                    double u = ((1 + sin(i * d)) / 2);
                    prototypeMap[i] = (float) Math.pow(u, exponent);
                }
                break;
            case 6:
                Arrays.fill(prototypeMap, 1.f);
                prototypeMap[0] = 0;
                break;
        }
        transparencyManuallyEdited = false;
        updateMap();
    }
    
    private void updateMap()
    {
        Arrays.fill(map, (byte)0);
        for (int i = bgrRange[0]; i <= bgrRange[1]; i++) 
            map[i] = (int)(255 * multiplier * prototypeMap[i]);
        if (ui != null)
            ui.updateDataValuesFromParams(false);
        fireStateChanged();
    }

    public int[] getMap()
    {
        return map;
    }

    public ComponentSubrange getComponentRange()
    {
        return componentRange;
    }

    public void setContainer(DataContainer container)
    {
        if (ui != null)
            ui.setDataContainer(container);
        setContainerSchema(container.getSchema());
    }
    
    public void setContainerSchema(DataContainerSchema containerSchema)
    {
        componentRange.setContainerSchema(containerSchema);
        if (active)
            fireStateChanged();
    }

    public boolean isActive()
    {
        return active;
    }

    
    public void setActive(boolean active)
    {
        lastActive = active;
        this.active = active;
    }
    
    public void restoreActive()
    {
        active = lastActive;
    }

    /**
     * Get the value of adjusting
     *
     * @return the value of adjusting
     */
    public boolean isAdjusting()
    {
        return adjusting;
    }

    /**
     * Set the value of adjusting
     *
     * @param adjusting new value of adjusting
     */
    public void setAdjusting(boolean adjusting)
    {
        this.adjusting = adjusting;
    }

    /**
     * Utility field holding list of RenderEventListeners.
     */
    private transient ArrayList<RenderEventListener> renderEventListenerList
        = new ArrayList<>();

    /**
     * Registers RenderEventListener to receive events.
     * <p>
     * @param listener The listener to register.
     */
    public synchronized void addListener(RenderEventListener listener)
    {
        renderEventListenerList.add(listener);
    }

    /**
     * Removes RenderEventListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */
    public synchronized void removeRenderEventListener(RenderEventListener listener)
    {
        renderEventListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     */
    public void fireStateChanged()
    {
        if (active) {
            RenderEvent e = new RenderEvent(this, RenderEvent.TRANSPARENCY);
            try {
                for (RenderEventListener renderEventListenerList1 : renderEventListenerList)
                    renderEventListenerList1.renderExtentChanged(e);
            } catch (Exception x) {
            }
            
        }
    }

}
