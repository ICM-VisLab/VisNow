/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.MoviesManager;

import java.util.ArrayList;
import org.jogamp.java3d.Transform3D;
import org.visnow.vn.geometries.objects.generics.OpenTransformGroup;

/**
 * @author Norbert Kapiński (norkap@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * 
 * This class extends the OpenTranfromGroup class to allow addition of the listeners for monitoring transformation change.
 * 
 * 
 */


public class TransformCameraGroup extends OpenTransformGroup{
    
    
    ArrayList<Listener> listeners = new ArrayList<>();
    
    /**
     * creates a new transform camera group
     */
    public TransformCameraGroup(){
        
    }
    
    /**
     * creates a new transform camera group with a specific name
     * @param name specific name for the camera transform group.
     */
    
    public TransformCameraGroup(String name){
        super.setName(name);
    }
    
    /**
     * interface for camera group transform changed listeners
     */
    public interface Listener{
        public void transformCameraGroupChanged(Transform3D newTransform);
    }
    
    /**
     * sets new transform of the transform camera group
     * @param newTransform - new transform
     */
    
    @Override
    public void setTransform(Transform3D newTransform){
        super.setTransform(newTransform);
        fireTransformChanged(newTransform);
    }
    
    
    private void fireTransformChanged(Transform3D newTransform) {
        for (Listener l : listeners)
            l.transformCameraGroupChanged(newTransform);
    }
    
    /**
     * adds a transform change listener to the list
     * @param l listener
     * @return true (as specified by Collection.add)
     */
    
    public boolean addTransformCameraGroupListener(Listener l){
        return listeners.add(l);
    }
    
    /**
     * removes a transform changed listener from the list
     * @param l listener
     * @return true if this list contained the specified element, false otherwise.
     */
    
    public boolean removeTransformCameraGroupListener(Listener l){
        return listeners.remove(l);
    }
    
}
