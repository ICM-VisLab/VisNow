/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.eventslisteners.render;

import javax.swing.event.ChangeEvent;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class RenderEvent extends ChangeEvent
{

    public static final int COORDS = 1;
    public static final int COLORS = 2;
    public static final int TEXTURE = 4;
    public static final int APPEARANCE = 8;
    public static final int GEOMETRY = 16;
    public static final int TRANSPARENCY = 32;
    public static final int DATA_MAP = COLORS | TEXTURE | TRANSPARENCY;

    protected boolean adjusting = false;
    protected int updateExtent = 0;

    public RenderEvent(Object source, int extent)
    {
        super(source);
        this.updateExtent = extent;
        this.adjusting = false;
    }

    public RenderEvent(Object source, int extent, boolean adjusting)
    {
        super(source);
        this.updateExtent = extent;
        this.adjusting = adjusting;
    }

    public int getUpdateExtent()
    {
        return updateExtent;
    }

    public boolean isAdjusting()
    {
        return adjusting;
    }

}
