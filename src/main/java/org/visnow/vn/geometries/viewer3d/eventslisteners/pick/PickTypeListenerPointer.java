/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.eventslisteners.pick;

import java.util.EnumMap;
import org.visnow.vn.lib.basic.viewers.HapticViewer3D.controller.pointer3d.IViewPointer;

/**
 * Implemented by pointers that want to be notified about changes in active pick types.
 * <p/>
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public interface PickTypeListenerPointer extends IViewPointer
{

    /**
     * Called when a pointer is attached to the viewer and should get the first list of active
     * picks.
     * <p/>
     * @param pickTypes all pick types with counters meaning how many modules has those picks
     *                  enabled
     */
    public void onPicksInit(EnumMap<PickType, Integer> pickTypes);

    /**
     * Called when a module has enabled
     * <code>pickType</code> picking type. It DOES NOT mean that this type of pick hadn't been
     * enabled earlier by another module.
     * <p/>
     * @param pickType  just enabled pick type
     * @param pickTypes all pick types with counters meaning how many modules has those picks
     *                  enabled
     */
    public void onPickTurnedOn(PickType pickType, EnumMap<PickType, Integer> pickTypes);

    /**
     * Called when a module has disabled
     * <code>pickType</code> picking type. It DOES NOT mean that this type of pick isn't still
     * active - it can be active in another module.
     * <p/>
     * @param pickType  just disabled pick type
     * @param pickTypes all pick types with counters meaning how many modules has those picks
     *                  enabled
     */
    public void onPickTurnedOff(PickType pickType, EnumMap<PickType, Integer> pickTypes);
}
