/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d;

import java.util.ArrayList;
import org.jogamp.java3d.Transform3D;
import org.visnow.vn.geometries.objects.generics.OpenTransformGroup;

/**
 *
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class ObjRotate extends OpenTransformGroup
{

    public ObjRotate()
    {
    }

    public ObjRotate(String name)
    {
        super(name);
    }

    @Override
    public void setTransform(Transform3D t)
    {
        super.setTransform(t);
        fireSceneTransformChanged(t);
    }

    //<editor-fold desc=" Listener class and methods ">
    public interface Listener
    {

        void sceneTransformChanged(Transform3D newTransform);
    }

    ArrayList<Listener> listeners = new ArrayList<Listener>();

    boolean addTransformListener(Listener l)
    {
        return listeners.add(l);
    }

    boolean removeTransformListener(Listener l)
    {
        return listeners.remove(l);
    }

    void fireSceneTransformChanged(Transform3D newTransform)
    {
        for (Listener l : listeners)
            l.sceneTransformChanged(newTransform);
    }

    //</editor-fold>
}
