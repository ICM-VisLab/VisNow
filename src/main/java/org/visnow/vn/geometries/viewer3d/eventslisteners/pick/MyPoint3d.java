/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.eventslisteners.pick;

import org.jogamp.vecmath.Point3d;
import org.jogamp.vecmath.Point3f;
import org.jogamp.vecmath.Tuple3d;
import org.jogamp.vecmath.Tuple3f;

/**
 *
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class MyPoint3d extends Point3d
{

    public MyPoint3d()
    {
    }

    public MyPoint3d(Point3d p1)
    {
        super(p1);
    }

    public MyPoint3d(double x, double y, double z)
    {
        super(x, y, z);
    }

    public MyPoint3d(double[] p)
    {
        super(p);
    }

    public MyPoint3d(Point3f p1)
    {
        super(p1);
    }

    public MyPoint3d(Tuple3f t1)
    {
        super(t1);
    }

    public MyPoint3d(Tuple3d t1)
    {
        super(t1);
    }

    public void set(int index, double value)
    {
        switch (index) {
            case 0:
                setX(value);
                break;
            case 1:
                setY(value);
                break;
            case 2:
                setZ(value);
                break;
            default:
                throw new IllegalArgumentException("index in Tuple3d can be only 0, 1 or 2");
        }
    }

    public double get(int index)
    {
        double result;
        switch (index) {
            case 0:
                result = getX();
                break;
            case 1:
                result = getY();
                break;
            case 2:
                result = getZ();
                break;
            default:
                throw new IllegalArgumentException("index in Tuple3d can be only 0, 1 or 2");
        }
        return result;
    }

    public float[] toFloat()
    {
        float[] p;
        p = new float[]{(float) x,
                        (float) y,
                        (float) z
        };
        return p;
    }

    public double[] toDouble()
    {
        double[] p;
        p = new double[]{x, y, z};
        return p;
    }
}
