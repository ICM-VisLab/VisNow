/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.lights;

import org.jogamp.java3d.Bounds;
import org.jogamp.java3d.Light;
import org.jogamp.java3d.PointLight;
import org.jogamp.vecmath.Color3f;
import org.jogamp.vecmath.Point3f;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class EditablePointLight extends EditableLight
{
    protected static final Point3f DEFAULT_ATTENUATION = new Point3f(1, 1, 1);
    protected final Color3f stdLightCol;
    protected Point3f position;

    public EditablePointLight(Color3f lightColor, Bounds bounds, Point3f position,
                              String name, boolean enabled)
    {
        type = LightType.POINT;
        lightName = name;
        stdLightCol = lightColor == null ? new Color3f(DEFAULT_LIGHT_COLOR) : lightColor;
        this.lightColor = stdLightCol;
        if (position != null)
            this.position = position;
        else
            this.position = new Point3f(0, 0, 0);
        this.enabled = enabled;
        light = new PointLight(enabled, this.lightColor, this.position, DEFAULT_ATTENUATION);
        light.setCapability(Light.ALLOW_STATE_WRITE);
        light.setCapability(Light.ALLOW_COLOR_WRITE);
        light.setCapability(PointLight.ALLOW_ATTENUATION_WRITE);
        light.setCapability(PointLight.ALLOW_POSITION_WRITE);
        light.setInfluencingBounds(bounds);
    }

    public EditablePointLight(Bounds bounds, String name)
    {
        this(DEFAULT_LIGHT_COLOR, bounds, new Point3f(), name, true);
    }

    public void setAttenuation(Point3f attenuation)
    {
        ((PointLight)light).setAttenuation(attenuation);
    }

    public PointLight getLight()
    {
        return (PointLight)light;
    }

    @Override
    public void reset()
    {
        ((PointLight)light).setAttenuation(DEFAULT_ATTENUATION);
        setLightColor(new Color3f(stdLightCol));
        ((PointLight)light).setPosition(0, 0, 0);
    }
}
