//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.lights;

import java.util.ArrayList;
import org.jogamp.java3d.BoundingSphere;
import org.jogamp.java3d.Canvas3D;
import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.AxisAngle4f;
import org.jogamp.vecmath.Color3f;
import org.jogamp.vecmath.Vector3d;
import org.jogamp.vecmath.Vector3f;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;

/**
 *
 * @author know
 */
public class LightsInitializers
{
    private static final boolean[] DIR_LIGHT_ACTIVE = {true, true, true, false};
    private static final boolean[] LIGHT_BIDIRECTIONAL = {true, false, false, false};
    private static final Color3f[] DIRECTIONAL_LIGHTS_COLORS = new Color3f[]
            {new Color3f(0.7f, 0.7f, 0.7f),
             new Color3f(0.3f, 0.25f, 0.2f),
             new Color3f(0.23f, 0.2f, 0.25f),
             new Color3f(0.1f, 0.1f, 0.15f)};
    private static final Vector3f INIT_LIGHT_DIRECTION = new Vector3f(0, 0, -1);
    private static final Vector3f[] LIGHT_DIRECTIONS = new Vector3f[]
            {new Vector3f(.1f, .08f, -.5f),
             new Vector3f(-1.f, -0.4f, -.5f),
             new Vector3f(0.f, 0.6f, -1.f),
             new Vector3f(0.f, -0.6f, -1.f)};
    private static final Color3f[] POINT_LIGHT_COLORS = new Color3f[]
            {new Color3f(0.3f, 0.3f, 0.3f),
             new Color3f(0.3f, 0.3f, 0.3f),
             new Color3f(0.3f, 0.3f, 0.3f)};

    public static void createDirectionalLights(BoundingSphere bounds,
                                               OpenBranchGroup windowRootObject,
                                               ArrayList<DirectionalLightObject> directionalLightObjects)
    {
        directionalLightObjects.clear();
        for (int i = 0; i < DIRECTIONAL_LIGHTS_COLORS.length; i++) {
            float angle = (float)LIGHT_DIRECTIONS[i].angle(INIT_LIGHT_DIRECTION);
            Vector3f axis = new Vector3f();
            axis.cross(LIGHT_DIRECTIONS[i],INIT_LIGHT_DIRECTION);
            AxisAngle4f rotationAngle = new AxisAngle4f(axis.x, axis.y, axis.z, angle);
            Transform3D transform = new Transform3D();
            transform.setRotation(rotationAngle);
            DirectionalLightObject lightObject = new DirectionalLightObject(
                                                     new EditableDirectionalLight(DIRECTIONAL_LIGHTS_COLORS[i],
                                                                                  bounds,
                                                                                  "directional light " + i, DIR_LIGHT_ACTIVE[i], LIGHT_BIDIRECTIONAL[i]),
                                                     transform);
            lightObject.getLight().setGlyph(lightObject.getGlyph());
            directionalLightObjects.add(lightObject);
            windowRootObject.addChild(lightObject);
        }
    }


    public static void createPointLights(BoundingSphere bounds,
                                         OpenBranchGroup rootObject,
                                         float radius, Vector3d sceneCenter,
                                         ArrayList<PointLightObject> pointLightObjects,
                                         Canvas3D canvas)
    {
        pointLightObjects.clear();
        for (int i = 0; i < POINT_LIGHT_COLORS.length; i++) {
            PointLightObject lightObject = new PointLightObject(
                                                     new EditablePointLight(POINT_LIGHT_COLORS[i], bounds,
                                                                            null, "point light " + i, false));
            pointLightObjects.add(lightObject);
            rootObject.addChild(lightObject);
            lightObject.reset();
            lightObject.updateScale(radius, sceneCenter);
            lightObject.reset();
        }
    }

    private LightsInitializers()
    {
    }
}
