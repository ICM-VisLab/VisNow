//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.lights;

import org.jogamp.vecmath.Color3f;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenTransformGroup;

/**
 *
 * @author know
 */
abstract public class LightObject extends OpenBranchGroup
{
    protected final OpenTransformGroup lightTransform = new OpenTransformGroup();
    protected final OpenBranchGroup lightGroup = new OpenBranchGroup();
    protected final OpenBranchGroup transGroup = new OpenBranchGroup();
    protected EditableLight light;
    protected boolean enabled = true;

    public LightObject(EditableLight light)
    {
        this.light = light;
    }

    public EditableLight getLight()
    {
        return light;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
        light.setEnabled(enabled);
    }

    public Color3f getLightColor()
    {
        return light.getLightColor();
    }

    public void setLightColor(Color3f lightCol)
    {
        light.setLightColor(lightCol);
    }

    public int getState()
    {
        if (!light.isEnabled())
            return 0;
        else if (light instanceof EditableDirectionalLight &&
                 ((EditableDirectionalLight)light).isBiDirectional())
            return 2;
        else
            return 1;
    }

    public void setState(int state)
    {
        light.setEnabled(state > 0);
        if (light instanceof EditableDirectionalLight)
            ((EditableDirectionalLight)light).setBiDirectional(state == 2);
    }

    abstract public void reset();

}
