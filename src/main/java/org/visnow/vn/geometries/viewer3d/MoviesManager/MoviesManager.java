/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.MoviesManager;

import java.util.ArrayList;
import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedLineStripArray;
import org.jogamp.vecmath.Point3d;
import org.apache.log4j.Logger;
import org.visnow.vn.geometries.objects.GeometryObject;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;
import org.visnow.vn.lib.utils.curves.Cubic;

/**
 * This class holds and manage information about key frames of a movie.
 * 
 * @author Norbert Kapiński (norkap@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * /**
 * This class holds and manage information about key frames of a movie.
 * 
 * @author Norbert Kapiński (norkap@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * 
 * 
 */


public class MoviesManager {

    protected static Logger LOGGER = Logger.getLogger(MoviesManager.class);
    
    private ArrayList<MovieFrame> keyFrames;
    private GeometryObject trajOutObj;
    
    /**
     * This method creates geometry object representing camera trajectory. Trajectory is interpolated based on key frames.
     * @param isClosed if true than the starting point is also the ending point and the trajectory is closed.
     * @param nKnots number of interpolation points on every segment between key frames. Must be greater than 0
     * @return camera trajectory or null if number of key frames is lower than 4
     */
    
    public GeometryObject createTrajectorieGeometry(boolean isClosed, int nKnots)
    {
        if(!(nKnots>0)){
            throw new IllegalArgumentException("Interpolation knots number = "+nKnots+", Interpolation knots number must be greater than 0");
        }

        int nKeyFrames = keyFrames.size();
        if(nKeyFrames<4){
            return null;
        }
        
        Point3d[] vertsl;
        if(isClosed){
            vertsl = new Point3d[nKeyFrames + 1];
            vertsl[nKeyFrames] = new Point3d(keyFrames.get(0).getCameraPosition());
        }else{
            vertsl = new Point3d[nKeyFrames];
        }
        
        for (int i = 0; i < nKeyFrames; i++) {
            vertsl[i] = new Point3d(keyFrames.get(i).getCameraPosition());
        }

        ArrayList<Point3d> interpolatedPoints = interpolateCameraTrajectory(vertsl, nKnots);
        int nFrames = interpolatedPoints.size();
        
        Point3d[] nVertsl = new Point3d[nFrames];
        
        for (int i = 0; i < nFrames; i++) {
            nVertsl[i] = new Point3d(interpolatedPoints.get(i));
        }
        
        int[] pIndexl = new int[2*(nFrames - 1)];
        
        for (int i = 0; i < nFrames - 1; i++) {
            pIndexl[2*i] = i;
            pIndexl[2*i + 1] = i+1;
        }
        
        trajOutObj = new GeometryObject("trajectory");
        
        int[] stripsl = new int[nFrames - 1];
        for (int i = 0; i < stripsl.length; i++) {
            stripsl[i] = 2;
        }

        IndexedLineStripArray glyph = new IndexedLineStripArray(nFrames,
                                          GeometryArray.COORDINATES,
                                          2 * nFrames, stripsl);
        
        glyph.setCoordinates(0, nVertsl);
        glyph.setCoordinateIndices(0, pIndexl);
        OpenShape3D pickLine = new OpenShape3D();
        pickLine.addGeometry(glyph);
        OpenBranchGroup pLine = new OpenBranchGroup();
        pLine.addChild(pickLine);
        trajOutObj.addNode(pLine);
        
        return trajOutObj;
    }
    
    private static ArrayList<Point3d> interpolateCameraTrajectory(Point3d[] points, int nKnots)
    {
        ArrayList<Point3d> newPoints = new ArrayList<>();
        
        int nSplines = points.length - 1;
        
        float[][][] splines = Cubic.calcCubic3D(nSplines, points);
        
        float[][] splinesX = splines[0];
        float[][] splinesY = splines[1];
        float[][] splinesZ = splines[2];
        float t;
        
        for (int i = 0; i < nSplines; i++) {
            for (int j = 0; j < nKnots; j++) {
                t=j/(float)nKnots;
                newPoints.add(new Point3d(Cubic.eval(splinesX[i], t), Cubic.eval(splinesY[i], t), Cubic.eval(splinesZ[i], t)));
            }
        }
        newPoints.add(new Point3d(Cubic.eval(splinesX[nSplines - 1], 1), Cubic.eval(splinesY[nSplines - 1], 1), Cubic.eval(splinesZ[nSplines - 1], 1)));
        
      return newPoints;
    }
    
    /**
     * init key frame list
     */
    public MoviesManager(){
        keyFrames = new ArrayList<>();
    }   
    
    /**
     * adds new frame to the key frames list
     * @param frame new frame
     */
    public void addKeyFrame(MovieFrame frame)
    {
        keyFrames.add(frame);
    }
    /**
     * removes frame from the key frames list
     * @param idx index of the frame to be removed
     */
    public void removeKeyFrame(int idx)
    {
        if(idx>=0 || idx<keyFrames.size()){
            keyFrames.remove(idx);
        }else{
            LOGGER.error(new IndexOutOfBoundsException("idx"+idx+"; size of keyFrames list = "+keyFrames.size()));
        }
    }

    /**
     * Key frames list.
     * @return key frames list 
     */
    
    public ArrayList<MovieFrame> getKeyFrames() {
        return keyFrames;
    }
    
    /**
     * This method returns camera trajectory geometry object
     * @return geometry object representing camera trajectory 
     */
    public GeometryObject getTrajectoryGeometryObject() {
        return trajOutObj;
    }
    
}
