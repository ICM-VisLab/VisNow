/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.eventslisteners.render;

import java.util.*;

/**
 * This event is fired from the AnimationPanel class when the time range is changed by timeRangeSlider
 * <p>
 * @author Jędrzej M. Nowosielski (jnow@icm.edu.pl), University of Warsaw ICM
 */
public class TimeRangeModificationEvent extends EventObject
{
    /**
     * minimum value (of the timeRangeSlider from AnimationPanel)
     */
    private float tMin;

    /**
     * maximum value
     */
    private float tMax;

    /**
     * current low value
     */
    private float tLow;

    /**
     * current high value
     */
    private float tHigh;

    /**
     * Creates a new instance of TimeRangeModificationEvent
     * <p>
     * @param source
     * @param tMin   minimum value (of the timeRangeSlider from AnimationPanel)
     * @param tMax   maximum value
     * @param tLow   current low value
     * @param tHigh  current high value
     * <p>
     */
    public TimeRangeModificationEvent(Object source, float tMin, float tMax, float tLow, float tHigh)
    {
        super(source);
        this.tMin = tMin;
        this.tMax = tMax;
        this.tLow = tLow;
        this.tHigh = tHigh;

    }

    /**
     * Returns minimum value
     * <p>
     * @return minimum value
     */
    public float getTMin()
    {
        return tMin;
    }

    /**
     * Returns maximum value
     * <p>
     * @return maximum value
     */
    public float getTMax()
    {
        return tMax;
    }

    /**
     * Returns current low value
     * <p>
     * @return current low value
     */
    public float getTLow()
    {
        return tLow;
    }

    /**
     * Returns current high value
     * <p>
     * @return current high value
     */
    public float getTHigh()
    {
        return tHigh;
    }

    @Override
    public String toString()
    {
        return "TimeRangeModificationEvent";
    }
}
