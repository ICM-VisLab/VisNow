/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d.lights;

import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedTriangleStripArray;
import org.jogamp.java3d.Material;
import org.jogamp.java3d.PolygonAttributes;
import org.jogamp.vecmath.Color3f;
import org.visnow.vn.geometries.geometryTemplates.Glyph;
import org.visnow.vn.geometries.geometryTemplates.ScalarGlyphTemplates;
import org.visnow.vn.geometries.geometryTemplates.VectorGlyphTemplates;
import org.visnow.vn.geometries.objects.generics.OpenAppearance;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenColoringAttributes;
import org.visnow.vn.geometries.objects.generics.OpenMaterial;
import org.visnow.vn.geometries.objects.generics.OpenPolygonAttributes;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;

/**
 *
 * @author Know
 */
public class LightIndicator extends OpenBranchGroup
{

    protected EditableLight light;

    protected IndexedTriangleStripArray triangleStrips;

    protected OpenAppearance appearance = new OpenAppearance();
    protected OpenBranchGroup glyph = new OpenBranchGroup();

    protected OpenShape3D surfaces = new OpenShape3D();

    protected boolean visible = false;

    protected float[] tVerts;
    protected float[] verts;
    protected float scale = 1;
    protected float[] scaledVerts;

    public LightIndicator()
    {
    }

    public LightIndicator(EditableLight light)
    {
        super(light.getLightName());
        this.light = light;
        setLight();
        glyph.addChild(surfaces);
    }

    private Color3f bright(Color3f lightColor)
    {
        float[] clr = new float[3];
        lightColor.get(clr);
        float u = 0;
        for (int i = 0; i < clr.length; i++)
            if (clr[i] > u)
                u = clr[i];
        if (u > 0) {
            u = .5f / u + .5f;
            for (int i = 0; i < clr.length; i++)
                clr[i] *= u;
        }
        return new Color3f(clr);
    }

    public void setLight()
    {
        surfaces.removeAllGeometries();
        if (light.getType() != EditableLight.LightType.DIRECTIONAL &&
            light.getType() != EditableLight.LightType.POINT)
            return;
        Glyph gt = light.getType() == EditableLight.LightType.DIRECTIONAL ?
                   VectorGlyphTemplates.glyph("arrow 3d", 2) :
                   ScalarGlyphTemplates.glyph("sphere", 3);
        int nTVerts = gt.getNverts();
        int nVerts;
        int nTInds =  gt.getNinds();
        int nInds;
        int[] tStrips    = gt.getStrips();
        int[] tInds      = gt.getPntsIndex();
        tVerts   = gt.getVerts();
        float[] tNormals = gt.getNormals();
        float[] normals;
        int[] strips;
        int[] indices    = tInds;
        int off = 3 * nTVerts;
        switch (light.getType()) {
            case DIRECTIONAL:
                if (((EditableDirectionalLight)light).isBiDirectional()) {
                    nTVerts = gt.getNverts();
                    nVerts = 2 * nTVerts;
                    nInds  = 2 * nTInds;
                    strips = new int[2 * tStrips.length];
                    indices = new int[2 * nTInds];
                    System.arraycopy(tStrips, 0, strips, 0, tStrips.length);
                    for (int i = 0; i < tStrips.length; i++)
                        strips[i + tStrips.length] = strips[i];
                    System.arraycopy(tInds, 0, indices, 0, tInds.length);
                    for (int i = 0; i < tInds.length; i++)
                        indices[i + tInds.length] = indices[i] + nTVerts;
                }
                else
                {
                    nVerts = nTVerts;
                    nInds  = nTInds;
                    strips = tStrips;
                }
                verts = new float[3 * nVerts];
                normals = new float[3 * nVerts];
                for (int i = 0; i < 3 * nTVerts; i += 3){
                        verts[i]       =  .1f * tVerts[i];
                        verts[i + 1]   =  .1f * tVerts[i + 1];
                        verts[i + 2]   = 1.0f - tVerts[i + 2];
                        for (int j = 0; j < 3; j++)
                            normals[i + j] =  tNormals[i + j];
                    }
                if (((EditableDirectionalLight)light).isBiDirectional())
                    for (int i = 0; i < off; i ++) {
                        verts[i + off]    =  -verts[i];
                        normals[i + off] = -normals[i];
                    }
                scaledVerts = verts;
                break;
            case POINT:
                nVerts = nTVerts;nInds  = nTInds;
                strips = tStrips;
                verts = new float[3 * nVerts];
                normals = new float[3 * nVerts];
                for (int i = 0; i < 3 * nVerts; i += 3)
                    for (int j = 0; j < 3; j++) {
                        verts[i + j]   =  .02f * tVerts[i + j];
                        normals[i + j] =  tNormals[i + j];
                    }
                scaledVerts = new float[3 * nVerts];
                System.arraycopy(verts, 0, scaledVerts, 0, 3 * nVerts);
                break;
            default:
                return;
        }
        triangleStrips = new IndexedTriangleStripArray(nVerts,
                                                       GeometryArray.COORDINATES | GeometryArray.NORMALS,
                                                       nInds, strips);
        triangleStrips.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        triangleStrips.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        triangleStrips.setCoordinateIndices(0, indices);
        triangleStrips.setCoordinates(0, scaledVerts);
        triangleStrips.setNormals(0, normals);
        surfaces.addGeometry(triangleStrips);
        appearance.setUserData(this);
        Color3f color = light.getLightColor();
        OpenColoringAttributes colorAttributes =
                new OpenColoringAttributes(color);
        appearance.setColoringAttributes(colorAttributes);
        OpenPolygonAttributes polygonAttributes =
                new OpenPolygonAttributes(PolygonAttributes.POLYGON_FILL,
                                          PolygonAttributes.CULL_NONE, 0, true, 0);
        appearance.setPolygonAttributes(polygonAttributes);
        Color3f dark = new Color3f(.2f * color.getX(), .2f * color.getY(),  .2f * color.getZ());
        OpenMaterial material =
                new OpenMaterial(dark, bright(color), dark, dark, .1f, Material.SPECULAR);
        appearance.setMaterial(material);
        surfaces.setAppearance(appearance);
    }

    public void updateLight()
    {
        Color3f color = light.getLightColor();
        Color3f dark = new Color3f(.2f * color.getX(), .2f * color.getY(),  .2f * color.getZ());
        appearance.getMaterial().setEmissiveColor(bright(color));
        appearance.getMaterial().setAmbientColor(dark);
        appearance.getMaterial().setDiffuseColor(dark);
    }

    public void setVisible(boolean visible)
    {
        this.visible = visible;
        if (visible && light.isEnabled()) {
            if (!getAllChildren().hasNext())
                addChild(glyph);
        }
        else
           removeAllChildren();
    }

    public void updateEnabled()
    {
        if (visible && light.isEnabled()) {
            if (!getAllChildren().hasNext())
                addChild(glyph);
        }
        else
           removeAllChildren();
    }

    public void updateScale(float scale)
    {
        if (light.getType() != EditableLight.LightType.POINT)
            return;
        this.scale = scale;
        for (int i = 0; i < verts.length; i++)
            scaledVerts[i] = scale * verts[i];
        triangleStrips.setCoordinates(0, scaledVerts);

    }

}
