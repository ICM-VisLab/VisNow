/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.visnow.vn.geometries.viewer3d.controls;

import org.jogamp.java3d.Transform3D;

/**
 *
 * @author know
 */
public class SavedTransforms
{
    public final Transform3D rotation;
    public final Transform3D translation;
    public final Transform3D scaling;
    public final Transform3D universe;

    public SavedTransforms(Transform3D rotation, Transform3D translation, Transform3D scaling, Transform3D universe)
    {
        this.rotation = rotation;
        this.translation = translation;
        this.scaling = scaling;
        this.universe = universe;
    }
}
