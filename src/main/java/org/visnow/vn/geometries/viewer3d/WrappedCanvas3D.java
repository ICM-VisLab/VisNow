/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.viewer3d;

import java.awt.Container;
import java.awt.GraphicsConfiguration;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.HierarchyBoundsListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.lang.reflect.Method;
import org.jogamp.java3d.Canvas3D;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.visnow.vn.system.main.VisNow;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * 
 * This calss is an override to keep Java3D stero rendering working on newer JREs.
 * 
 * For Java3D 1.5.2:
 * - on Java versions 1.6 or older stereo rendering was working fine.
 * - in Java 1.7 due to some GraphicsConfiguration hierarchy changes the wrapper below is required. With the wrapper enabled stereo is working fine.
 * - in Java 1.8 so far no solution was found to properly run stereo.
 * 
 * For Java3D 1.6-pre:
 * 
 */
public class WrappedCanvas3D extends Canvas3D {
    
    

    private GraphicsConfiguration initialGC;

    public WrappedCanvas3D(GraphicsConfiguration graphicsConfiguration) {
        super(graphicsConfiguration);
        initialGC = this.getGraphicsConfiguration();
        if ((VisNow.getJava3DVersion() == 5 && VisNow.getJavaVersion() == 7) || (VisNow.getJava3DVersion() >= 6 && VisNow.getJavaVersion() >= 7) ) {
            this.addHierarchyListener(new HierarchyListener() {
                @Override
                public void hierarchyChanged(HierarchyEvent e) {
                    if (e.getChangeFlags() == HierarchyEvent.PARENT_CHANGED) {
                        Container parent = e.getChangedParent();
                        try {
                            Class<?> c = java.awt.Component.class;
                            Method method = c.getDeclaredMethod("setGraphicsConfiguration", GraphicsConfiguration.class);
                            if (method == null) {
                                return;
                            }
                            method.setAccessible(true);
                            if (parent == null) {
                                return;
                            }
                            Window parentWindow = SwingUtilities.getWindowAncestor(parent);
                            if (parentWindow != null && initialGC != null) {
                                method.invoke(parentWindow, initialGC);
                            }
                        } catch (NoSuchMethodException ex) {
                            //in case of Java 6 ignore this exception
                        } catch (Exception ex) {
                            throw new RuntimeException(ex.getMessage());
                        }
                        if (parent instanceof JComponent) {
                            ((JComponent) parent).revalidate();
                        }
                    }
                }
            });
        }

        this.addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(ComponentEvent e) {
                updateLocation();                
            }

            @Override
            public void componentMoved(ComponentEvent e) {
                updateLocation();                
            }

            @Override
            public void componentShown(ComponentEvent e) {
                updateLocation();                
            }

            @Override
            public void componentHidden(ComponentEvent e) {
            }
        });
        
        this.addHierarchyBoundsListener(new HierarchyBoundsListener() {
            @Override
            public void ancestorMoved(HierarchyEvent e) {
                updateLocation();                
            }

            @Override
            public void ancestorResized(HierarchyEvent e) {
                updateLocation();                
            }
        });
        
        
        updateLocation();
    }
    
    private Point locationOnScreen = null;
    
    private void updateLocation() {
        locationOnScreen = this.getLocationOnScreen();                
    }
    
    public Point getWrappedLocationOnScreen() {
        return locationOnScreen;
    }
    
    @Override
    public boolean getStereoAvailable() {    
        return (super.getStereoAvailable() && ((VisNow.getJava3DVersion() == 5 && VisNow.getJavaVersion() < 8) || VisNow.getJava3DVersion() == 6));        
    }
}
