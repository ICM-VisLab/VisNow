/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects.generics;

import org.jogamp.java3d.ColoringAttributes;
import org.jogamp.vecmath.Color3f;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class OpenColoringAttributes extends ColoringAttributes
{

    /**
     * Creates a new instance of OpenColoringAttributes
     */
    public OpenColoringAttributes()
    {
        setCapability(ColoringAttributes.ALLOW_COLOR_READ);
        setCapability(ColoringAttributes.ALLOW_COLOR_WRITE);
        setCapability(ColoringAttributes.ALLOW_SHADE_MODEL_READ);
        setCapability(ColoringAttributes.ALLOW_SHADE_MODEL_WRITE);
    }

    public OpenColoringAttributes(Color3f color)
    {
        super(color, ColoringAttributes.NICEST);
    }

    @Override
    public OpenColoringAttributes cloneNodeComponent(boolean forceDuplicate)
    {
        OpenColoringAttributes openColoringAttributes = new OpenColoringAttributes();
        openColoringAttributes.duplicateNodeComponent(this, forceDuplicate);
        return openColoringAttributes;
    }
    
    public void copyValuesFrom(ColoringAttributes src)
    {
        Color3f col = new Color3f();
        src.getColor(col);
        setColor(col);
    }
}
