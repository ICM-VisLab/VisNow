/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects;

import java.util.ArrayList;
import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.Matrix4f;
import org.visnow.vn.geometries.events.TransformEvent;
import org.visnow.vn.geometries.events.TransformListener;

/**
 *
 * @author Krzysztof S. Nowinski
 * University of Warsaw, ICM
 */
public class SignalingTransform3D
{

    protected Transform3D transform = new Transform3D();

    /**
     * Get the value of transform
     *
     * @return the value of transform
     */
    public Transform3D getTransform()
    {
        return transform;
    }

    /**
     * Set the value of transform
     *
     * @param transform new value of transform
     */
    public void setTransform(Transform3D transform)
    {
        this.transform = transform;
    }

    public float[][] getMatrix()
    {
        Matrix4f tr = new Matrix4f();
        transform.get(tr);
        float[][] trMatrix = new float[4][4];
        for (int i = 0; i < trMatrix.length; i++)
            tr.getRow(i, trMatrix[i]);
        return trMatrix;
    }

    /**
     * Utility field holding list of TransformListeners.
     */
    private transient ArrayList<TransformListener> TransformListenerList
        = new ArrayList<TransformListener>();

    /**
     * Registers TransformListener to receive events.
     * <p>
     * @param listener The listener to register.
     */
    public synchronized void addTransformListener(TransformListener listener)
    {
        TransformListenerList.add(listener);
    }

    /**
     * Removes TransformListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */
    public synchronized void removeTransformListener(TransformListener listener)
    {
        TransformListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param object Parameter #1 of the <CODE>TransformEvent<CODE> constructor.
     */
    public void fireTransformChanged()
    {
        TransformEvent e = new TransformEvent(this, transform);
        for (TransformListener listener : TransformListenerList)
            listener.transformChanged(e);
    }
}
