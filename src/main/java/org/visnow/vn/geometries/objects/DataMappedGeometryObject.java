/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects;

import org.visnow.jlargearrays.ByteLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jscic.Field;
import org.visnow.vn.datamaps.colormap1d.DefaultColorMap1D;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.parameters.DataMappingParams;

/**
 *
 * @author Krzysztof S. Nowinski, Warsaw University, ICM
 *
 */
public class DataMappedGeometryObject extends GeometryObject
{

    public static final int SURFACE = 1;
    public static final int EDGES = 2;
    public static final int EXTEDGES = 4;

    protected boolean structureChanged = true;
    protected boolean dataChanged = true;
    protected boolean rangeChanged = true;
    protected boolean isTextureChanged = true;
    protected boolean coordsChanged = true;
    protected boolean colorsChanged = true;
    protected boolean uvCoordsChanged = true;
    protected boolean debug = false;
    protected boolean newParams = true;

    protected long nNodes = 0;
    protected long nTriangleIndices = 0;
    protected long nLineIndices = 0;
    protected IntLargeArray coordIndices = null;
    protected FloatLargeArray coords = null;
    protected FloatLargeArray normals = null;
    protected FloatLargeArray uvData = null;
    protected ByteLargeArray colors = null;
    protected DefaultColorMap1D colorMap = null;
    protected ColormapLegend colormapLegend = new ColormapLegend();

    /**
     * Transient place holder for all geometries created by the module - detached
     * and re-attached for all structural changes
     */
    protected OpenBranchGroup geometry = new OpenBranchGroup("data mapped geom");

    /**
     * Creates a new instance of DataMappedGeometryObject
     */
    public DataMappedGeometryObject()
    {
        dataMappingParams = new DataMappingParams();
        colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
        dataMappingParams.getTransparencyParams().addListener(renderingParams.getTransparencyChangeListener());
        addGeometry2D(colormapLegend);
    }

    public DataMappedGeometryObject(DataMappingParams dataMappingParams)
    {
        this.dataMappingParams = dataMappingParams;
        colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
        dataMappingParams.getTransparencyParams().addListener(renderingParams.getTransparencyChangeListener());
        addGeometry2D(colormapLegend);
    }

    public DataMappedGeometryObject(String name)
    {
        super(name);
        dataMappingParams = new DataMappingParams();
        colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
        dataMappingParams.getTransparencyParams().addListener(renderingParams.getTransparencyChangeListener());
        addGeometry2D(colormapLegend);
    }
    
    public DataMappedGeometryObject(String name, DataMappingParams dataMappingParams)
    {
        super(name);
        this.dataMappingParams = dataMappingParams;
        colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
        dataMappingParams.getTransparencyParams().addListener(renderingParams.getTransparencyChangeListener());
        addGeometry2D(colormapLegend);
    }

    public DataMappedGeometryObject(String name, int timestamp)
    {
        super(name, timestamp);
        dataMappingParams = new DataMappingParams();
        colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
        dataMappingParams.getTransparencyParams().addListener(renderingParams.getTransparencyChangeListener());
        addGeometry2D(colormapLegend);
    }

    public DataMappedGeometryObject(String name, int timestamp, DataMappingParams dataMappingParams)
    {
        super(name, timestamp);
        this.dataMappingParams = dataMappingParams;
        colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
        dataMappingParams.getTransparencyParams().addListener(renderingParams.getTransparencyChangeListener());
        addGeometry2D(colormapLegend);
    }

    public DataMappingParams getDataMappingParams()
    {
        return dataMappingParams;
    }

    public ColormapLegend getColormapLegend()
    {
        return colormapLegend;
    }

    public boolean setField(Field inField)
    {
        return false;
    }
}
