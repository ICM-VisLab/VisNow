/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.jogamp.java3d.J3DGraphics2D;
import org.jogamp.java3d.MultipleParentException;
import org.jogamp.java3d.Transform3D;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.apache.commons.math3.util.FastMath;
import org.apache.log4j.Logger;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jscic.CellSet;
import org.visnow.jscic.Field;
import org.visnow.jscic.IrregularField;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.parameters.PresentationParams;
import org.visnow.vn.geometries.utils.transform.LocalToWindow;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class IrregularFieldGeometry extends FieldGeometry
{

    protected PresentationParams presentationParams = null;
    protected ArrayList<CellSetGeometry> cellSetGeometries = new ArrayList<CellSetGeometry>();
    protected IrregularField field = null;
    static Logger logger = Logger.getLogger(IrregularFieldGeometry.class);
    protected ColormapLegend fldColormapLegend = colormapLegend;

    public IrregularFieldGeometry(String name, PresentationParams presentationParams)
    {
        super(name, presentationParams);
        this.presentationParams = presentationParams;
        transformedGeometries.addChild(geometries);
        geometry.addChild(transformedGeometries);
    }

    public IrregularFieldGeometry(PresentationParams presentationParams)
    {
        super(presentationParams);
        this.presentationParams = presentationParams;
        transformedGeometries.addChild(geometries);
        geometry.addChild(transformedGeometries);
    }

    private void updateCellSetGeometries()
    {
        boolean detach = geometry.postdetach();
        cellSetGeometries.clear();
        clearAllGeometry();
        geometries.removeAllChildren();
        for (int iSet = 0; iSet < field.getNCellSets(); iSet++) {
            CellSet cSet = field.getCellSet(iSet);
            CellSetGeometry cSetGeometry = new CellSetGeometry(cSet.getName());
            cSetGeometry.setParams(presentationParams.getChild(iSet));
            presentationParams.getChild(iSet).setActive(true);
            cSetGeometry.setInData(field, cSet);
            cellSetGeometries.add(cSetGeometry);
            geometries.addChild(cSetGeometry);
            addBgrColorListener(cSetGeometry.getBgrColorListener());
        }
//        for (int iSet = 0; iSet < field.getNCellSets(); iSet++)
//           cellSetGeometries.get(iSet).setInData(field, field.getCellSet(iSet));
        if (detach)
            geometry.postattach();
    }

    public void setData(IrregularField inField)
    {
        setData(inField, "");
    }

    public void setData(IrregularField inField, String presentationParamString)
    {
        if (inField == null || (int) inField.getNNodes() < 1)
            return;
        setIgnoreUpdate(true);

        presentationParams.setInField(inField);
        if (!presentationParamString.isEmpty())
            presentationParams.restorePassivelyValuesFrom(presentationParamString);
        structureChanged = (field == null || !inField.isStructureCompatibleWith(this.field));
        dataChanged = (field == null || !inField.isDataCompatibleWith(this.field));
        this.field = inField;
        name = field.getName();
        if (cellSetGeometries != null) {
            for (CellSetGeometry cellSetGeometry : cellSetGeometries)
                cellSetGeometry.getDataMappingParams().clearRenderEventListeners();
            updateCellSetGeometries();
            transformParams = presentationParams.getTransformParams();
            colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
            presentationParams.getTransformParams().addChangeListener(new ChangeListener()
            {
                @Override
                public void stateChanged(ChangeEvent evt)
                {
                    transformedGeometries.setTransform(new Transform3D(transformParams.getTransform()));
                }
            });
            transformedGeometries.setTransform(new Transform3D(transformParams.getTransform()));
            if (field.getNCellSets() == 1)
                colormapLegend = cellSetGeometries.get(0).getColormapLegend();
            else
                colormapLegend = fldColormapLegend;
        }
        setIgnoreUpdate(false);
    }

    @Override
    public void setIgnoreUpdate(boolean ignoreUpdate)
    {
        this.ignoreUpdate = ignoreUpdate;
        for (CellSetGeometry cellSetGeometrie : cellSetGeometries)
            cellSetGeometrie.setIgnoreUpdate(ignoreUpdate);
    }

    public ArrayList<CellSetGeometry> getCellSetGeometries()
    {
        return cellSetGeometries;
    }

    public CellSetGeometry getCellSetGeometry(int i)
    {
        if (cellSetGeometries != null && i >= 0 && i < cellSetGeometries.size())
            return cellSetGeometries.get(i);
        return null;
    }

    @Override
    public void updateCoords(FloatLargeArray coords)
    {
        boolean detach = geometry.postdetach();
        this.coords = coords;
        if (field != null) {
            int nthreads = FastMath.min(cellSetGeometries.size(), VisNow.availableProcessors());
            int k = cellSetGeometries.size() / nthreads;
            final float[] coords_data = coords.getData();
            Future[] threads = new Future[nthreads];
            for (int j = 0; j < nthreads; j++) {
                final int firstIdx = j * k;
                final int lastIdx = (j == nthreads - 1) ? cellSetGeometries.size() : firstIdx + k;
                threads[j] = ConcurrencyUtils.submit(() -> {
                    for (int i = firstIdx; i < lastIdx; i++) {
                        cellSetGeometries.get(i).updateCoords(coords_data, false);
                    }
                });
            }
            try {
                ConcurrencyUtils.waitForCompletion(threads);
            } catch (InterruptedException | ExecutionException ex) {
                cellSetGeometries.forEach(cellSetGeometry -> {
                    cellSetGeometry.updateCoords(coords_data, true);
                });
            }
        }
        if (detach)
            geometry.postattach();
    }

    @Override
    public void updateCoords(boolean force)
    {
        if (field == null || !force)
            return;
        boolean detach = geometry.postdetach();
        coords = field.getCurrentCoords() == null ? null : field.getCurrentCoords();
        int nthreads = FastMath.min(cellSetGeometries.size(), VisNow.availableProcessors());
        int k = cellSetGeometries.size() / nthreads;
        Future[] threads = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final int firstIdx = j * k;
            final int lastIdx = (j == nthreads - 1) ? cellSetGeometries.size() : firstIdx + k;
            threads[j] = ConcurrencyUtils.submit(() -> {
                for (int i = firstIdx; i < lastIdx; i++) {
                    cellSetGeometries.get(i).updateCoords(force);
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(threads);
        } catch (InterruptedException | ExecutionException ex) {
            cellSetGeometries.forEach(cellSetGeometry -> {
                cellSetGeometry.updateCoords(force);
            });
        }

        if (detach)
            geometry.postattach();
    }

    @Override
    public void updateCoords()
    {
        if (field == null || ignoreUpdate)
            return;
        coords = field.getCurrentCoords() == null ? null : field.getCurrentCoords();

        int nthreads = FastMath.min(cellSetGeometries.size(), VisNow.availableProcessors());
        int k = cellSetGeometries.size() / nthreads;
        Future[] threads = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final int firstIdx = j * k;
            final int lastIdx = (j == nthreads - 1) ? cellSetGeometries.size() : firstIdx + k;
            threads[j] = ConcurrencyUtils.submit(() -> {
                for (int i = firstIdx; i < lastIdx; i++) {
                    cellSetGeometries.get(i).updateCoords(false);
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(threads);
        } catch (InterruptedException | ExecutionException ex) {
            cellSetGeometries.forEach(cellSetGeometry -> {
                cellSetGeometry.updateCoords(true);
            });
        }

    }

    public void updateTextureCoords()
    {
        if (field != null) {
            boolean detach = geometry.postdetach();
            int nthreads = FastMath.min(cellSetGeometries.size(), VisNow.availableProcessors());
            int k = cellSetGeometries.size() / nthreads;
            Future[] threads = new Future[nthreads];
            for (int j = 0; j < nthreads; j++) {
                final int firstIdx = j * k;
                final int lastIdx = (j == nthreads - 1) ? cellSetGeometries.size() : firstIdx + k;
                threads[j] = ConcurrencyUtils.submit(() -> {
                    for (int i = firstIdx; i < lastIdx; i++) {
                        cellSetGeometries.get(i).updateTextureCoords();
                    }
                });
            }
            try {
                ConcurrencyUtils.waitForCompletion(threads);
            } catch (InterruptedException | ExecutionException ex) {
                cellSetGeometries.forEach(cellSetGeometry -> {
                    cellSetGeometry.updateTextureCoords();
                });
            }
            if (detach)
                geometry.postattach();
        }
    }

    @Override
    public void updateDataMap()
    {
        if (field != null) {
            boolean detach = geometry.postdetach();
            int nthreads = FastMath.min(cellSetGeometries.size(), VisNow.availableProcessors());
            int k = cellSetGeometries.size() / nthreads;
            Future[] threads = new Future[nthreads];
            for (int j = 0; j < nthreads; j++) {
                final int firstIdx = j * k;
                final int lastIdx = (j == nthreads - 1) ? cellSetGeometries.size() : firstIdx + k;
                threads[j] = ConcurrencyUtils.submit(() -> {
                    for (int i = firstIdx; i < lastIdx; i++) {
                        cellSetGeometries.get(i).updataDataMap(false);
                    }
                });
            }
            try {
                ConcurrencyUtils.waitForCompletion(threads);
            } catch (InterruptedException | ExecutionException ex) {
                cellSetGeometries.forEach(cellSetGeometry -> {
                    cellSetGeometry.updataDataMap(true);
                });
            }
            if (detach)
                geometry.postattach();
        }
    }

    public void updateColors()
    {
        if (field != null) {
            boolean detach = geometry.postdetach();
            int nthreads = FastMath.min(cellSetGeometries.size(), VisNow.availableProcessors());
            int k = cellSetGeometries.size() / nthreads;
            Future[] threads = new Future[nthreads];
            for (int j = 0; j < nthreads; j++) {
                final int firstIdx = j * k;
                final int lastIdx = (j == nthreads - 1) ? cellSetGeometries.size() : firstIdx + k;
                threads[j] = ConcurrencyUtils.submit(() -> {
                    for (int i = firstIdx; i < lastIdx; i++) {
                        cellSetGeometries.get(i).updateColors(false);
                    }
                });
            }
            try {
                ConcurrencyUtils.waitForCompletion(threads);
            } catch (InterruptedException | ExecutionException ex) {
                cellSetGeometries.forEach(cellSetGeometry -> {
                    cellSetGeometry.updateColors(true);
                });
            }
            if (detach)
                geometry.postattach();
        }
    }

    @Override
    public void createGeometry(Field inField)
    {
        updateGeometry();
    }

    @Override
    public OpenBranchGroup getGeometry()
    {
        updateGeometry();
        return geometry;
    }

    @Override
    public void updateGeometry()
    {
        if (ignoreUpdate || field == null)
            return;
        boolean detach = geometry.postdetach();
        try {
            transformedGeometries.removeAllChildren();
        } catch (Exception e) {
        }
        if (cellSetGeometries != null) {
            int nthreads = FastMath.min(cellSetGeometries.size(), VisNow.availableProcessors());
            int k = cellSetGeometries.size() / nthreads;
            Future[] threads = new Future[nthreads];
            for (int j = 0; j < nthreads; j++) {
                final int firstIdx = j * k;
                final int lastIdx = (j == nthreads - 1) ? cellSetGeometries.size() : firstIdx + k;
                threads[j] = ConcurrencyUtils.submit(() -> {
                    for (int i = firstIdx; i < lastIdx; i++) {
                        cellSetGeometries.get(i).updateGeometry(false);
                    }
                });
            }
            try {
                ConcurrencyUtils.waitForCompletion(threads);
            } catch (InterruptedException | ExecutionException ex) {
                cellSetGeometries.forEach(cellSetGeometry -> {
                    cellSetGeometry.updateGeometry(true);
                });
            }
        }
        if (geometries.getParent() != null) {
            geometries.detach();
        }
        try {
            transformedGeometries.addChild(geometries);
        } catch (MultipleParentException e) {
            logger.error("multiple parent in " + this + " updateGeometry");
        }
        if (detach) {
            geometry.postattach();
        }
    }

    @Override
    public void drawLocal2D(J3DGraphics2D vGraphics, LocalToWindow ltw, int w, int h)
    {
//        System.out.println("IF drawLocal2D");
        for (CellSetGeometry cellSetGeom : cellSetGeometries)
            cellSetGeom.draw2D(vGraphics, ltw, w, h);
    }

    @Override
    public OpenBranchGroup getGeometry(Field inField)
    {
        return null;
    }

    @Override
    public void updateGeometry(Field inField)
    {
    }

    public PresentationParams getFieldDisplayParams()
    {
        newParams = false;
        return presentationParams;
    }

    public ColormapLegend getColormapLegend(int i)
    {
        if (cellSetGeometries == null || i < 0 ||
            i >= cellSetGeometries.size() ||
            cellSetGeometries.get(i) == null)
            return null;
        return cellSetGeometries.get(i).getColormapLegend();
    }

    @Override
    public boolean setField(Field inField)
    {
        if (inField == null || !(inField instanceof IrregularField))
            return false;
        setData((IrregularField) inField);
        return true;
    }

    public void updateCellSetData(int n)
    {
        if (n >= 0 && n < field.getNCellSets()) {
            cellSetGeometries.get(n).setName(field.getCellSet(n).getName());
            cellSetGeometries.get(n).setPicked(field.getCellSet(n).isSelected());
        }
    }

    @Override
    public Field getField()
    {
        return field;
    }

}
