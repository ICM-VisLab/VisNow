/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects.generics;

import org.jogamp.java3d.Node;
import org.jogamp.java3d.Shape3D;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class OpenShape3D extends Shape3D implements Debuggable
{

    protected OpenAppearance appearance;

    /**
     * Creates a new instance of OpenShape3D
     */
    public OpenShape3D()
    {
        setCapability(Shape3D.ALLOW_APPEARANCE_READ);
        setCapability(Shape3D.ALLOW_APPEARANCE_WRITE);
        setCapability(Shape3D.ALLOW_GEOMETRY_READ);
        setCapability(Shape3D.ALLOW_GEOMETRY_WRITE);
        setCapability(Shape3D.ENABLE_PICK_REPORTING);
        setCapability(Node.ALLOW_LOCAL_TO_VWORLD_READ);
    }
    
    /**
     * Creates a new instance of OpenShape3D
     */
    public OpenShape3D(String name)
    {
        setCapability(Shape3D.ALLOW_APPEARANCE_READ);
        setCapability(Shape3D.ALLOW_APPEARANCE_WRITE);
        setCapability(Shape3D.ALLOW_GEOMETRY_READ);
        setCapability(Shape3D.ALLOW_GEOMETRY_WRITE);
        setCapability(Shape3D.ENABLE_PICK_REPORTING);
        setCapability(Node.ALLOW_LOCAL_TO_VWORLD_READ);
        setName(name);
    }
    
    public void printDebugInfo(int level)
    {
        for (int i = 0; i < level; i++) 
            System.out.print("  ");
        int n = this.numGeometries();
        System.out.println(("" + this + ": " + n + " geom(s)").replaceFirst("org.visnow.vn.geometries.", ""));
    }

    public void printDebugInfo()
    {
        printDebugInfo(0);
    }

    public OpenAppearance getAppearance()
    {
        return appearance;
    }

    public void setAppearance(OpenAppearance appearance)
    {
        this.appearance = appearance;
        super.setAppearance(appearance);
    }

    public Node cloneNode(boolean forceDuplicate)
    {
        OpenShape3D openShape3D = new OpenShape3D();
        openShape3D.duplicateNode(this, forceDuplicate);
        return openShape3D;
    }
}
