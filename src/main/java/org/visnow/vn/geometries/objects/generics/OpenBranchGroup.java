/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.objects.generics;

import org.jogamp.java3d.BranchGroup;
import org.jogamp.java3d.Node;
import org.jogamp.java3d.TransparencyAttributes;
import org.visnow.vn.geometries.viewer3d.Display3DPanel;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class OpenBranchGroup extends BranchGroup implements Debuggable
{

    /**
     * Creates a new instance of OpenBranchGroup
     */
    public OpenBranchGroup()
    {
        setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
        setCapability(BranchGroup.ALLOW_CHILDREN_READ);
        setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
        setCapability(BranchGroup.ALLOW_DETACH);
        setCapability(BranchGroup.ALLOW_LOCAL_TO_VWORLD_READ);
    }

    public OpenBranchGroup(String name)
    {
        super.setName(name);
        setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
        setCapability(BranchGroup.ALLOW_CHILDREN_READ);
        setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
        setCapability(BranchGroup.ALLOW_DETACH);
        setCapability(BranchGroup.ENABLE_PICK_REPORTING);
        setCapability(BranchGroup.ALLOW_LOCAL_TO_VWORLD_READ);
    }

    public void printDebugInfo()
    {
        printDebugInfo(0);
    }

    public void printDebugInfo(int level)
    {
        int n = this.numChildren();
        for (int i = 0; i < level; i++) 
            System.out.print("  ");
        System.out.println(("" + this + ": " + n + ((n==1)?" child":" children  ")).
                replaceFirst("org.visnow.vn.geometries.", ""));
        for (int i = 0; i < n; i++) {
            Node child = this.getChild(i);
            if (child instanceof Debuggable) 
                ((Debuggable) child).printDebugInfo(level + 1);
        }
    }

    @Override
    public Node cloneNode(boolean forceDuplicate)
    {
        OpenBranchGroup openBranchGroup = new OpenBranchGroup();
        openBranchGroup.duplicateNode(this, forceDuplicate);
        return openBranchGroup;
    }

    private Node postparent = null;

    public boolean postdetach()
    {
        //System.out.println("object "+getName()+" postdetach @"+System.currentTimeMillis());
        if (this.postparent != null) {
            return false;
        }

        if (!isNodeAttached(this)) {
            return false;
        }

        if (this.getParent() == null) {
            return false;
        }

        if (myViewer != null && !myViewer.isStoringFrames()) {
            return false;
        }

        if (myViewer != null && myViewer.isWaitingForExternalTrigger()) {
            return false;
        }

        if (myViewer != null) {
            myViewer.setPostRenderSilent(true);
        }

        this.postparent = this.getParent();
        if (postparent != null)
            this.detach();
        return true;
    }

    public void postattach()
    {
        if (this.postparent == null) {
            return;
        }

        //System.out.println("object "+getName()+" postattach @"+System.currentTimeMillis());
        if (myViewer != null) {
            myViewer.setPostRenderSilent(false);
        }

        if (postparent instanceof OpenBranchGroup && this.getParent() == null) {
            ((OpenBranchGroup) postparent).addChild(this);
        } else if (postparent instanceof OpenTransformGroup && this.getParent() == null) {
            ((OpenTransformGroup) postparent).addChild(this);
        }
        this.postparent = null;
    }

    protected Display3DPanel myViewer = null;

    public void setCurrentViewer(Display3DPanel panel)
    {
        this.myViewer = panel;
        //        if (panel != null) {
        //            System.out.println("object " + getName() + " set viewer to " + panel.getName());
        //        } else {
        //            System.out.println("object " + getName() + " set viewer to NULL");
        //        }

        for (int i = 0; i < this.numChildren(); i++) {
            Node n = this.getChild(i);
            if (n instanceof OpenBranchGroup) {
                ((OpenBranchGroup) n).setCurrentViewer(panel);
            } else if (n instanceof OpenTransformGroup) {
                ((OpenTransformGroup) n).setCurrentViewer(panel);
            }
        }
    }

    public Display3DPanel getCurrentViewer()
    {
        return myViewer;
    }

    public static boolean isNodeAttached(Node n)
    {
        if (n == null)
            return false;

        if ("root_object".equals(n.getName()))
            return true;

        return isNodeAttached(n.getParent());
    }

    /**
     * Factory for OpenAppearance. Creates appearance that have lighting set and transparency (if
     * <code>transparent</code> is true)
     *
     * @return a new Appearance object
     */
    public static OpenAppearance createAppearance(boolean transparent)
    {

        OpenAppearance a = new OpenAppearance();
        a.getMaterial().setLightingEnable(true);
        if (transparent)
            a.getTransparencyAttributes().setTransparencyMode(TransparencyAttributes.NICEST);
        return a;

    }
}
