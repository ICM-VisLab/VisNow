/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.geometryTemplates;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
abstract public class Glyph
/**
 * Abstract class specifying templates for small geometric objects that can be used as glyphs.<p>
 * Each subclass must have a constructor XxGlyph(Integer lod) providing data for building a glyph
 * with a specified level of detail lod. The glyph is a set of indexed geometry arrays of the type
 * indicated by the getType() method
 * <p>
 */
{

    /**
     * glyph is formed by a series of line strips
     */
    public static final int LINE_STRIPS = 0;
    /**
     * glyph is formed by a series of triangle strips
     */
    public static final int TRIANGLE_STRIPS = 1;
    /**
     * glyph is formed by a series of triangle fans
     */
    public static final int TRIANGLE_FANS = 2;
    /**
     * number of vertices (coords triples)
     */

    protected String name;
    protected int type;
    protected boolean refinable;

    protected int nverts;
    /**
     * number of indices (vertices of rendered triangles/lines
     */
    protected int ninds;
    /**
     * number of polytriangle strips/fans
     */
    protected int nstrips;
    /**
     * numbers of vertices in each triangle strip/fan
     */
    protected int[] strips;
    /**
     * number of polylines
     */
    protected int nlstrips;
    /**
     * numbers of vertices in each polyline
     */
    protected int[] lstrips;
    /**
     * vertex coordinates
     * <p>
     * (recommended normalization to -1...1, (0,0,0) will be used as grid origin,
     * z - coordinate is will be used as vector direction
     *
     */
    protected float verts[];
    /**
     * normals coordinates
     */
    protected float normals[];
    /**
     * indices to points in all strips
     */
    protected int pntsIndex[];
    /**
     * indices to colors (array of length ninds filled by zeros)
     */
    protected int clrsIndex[];

    /**
     *
     * @return glyph name for guis
     */
    public String getName()
    {
        return name;
    }

    /**
     *
     * @return glyph type (one of LINE_STRIPS, TRIANGLE_STRIPS or TRIANGLE_FANS)
     */
    public int getType()
    {
        return type;
    }

    /**
     *
     * @return true if various levels of detail (smoothness) are possible
     */
    public boolean isRefinable()
    {
        return refinable;
    }

    /**
     *
     * @return indices to colors
     */
    public int[] getClrsIndex()
    {
        return clrsIndex;
    }

    /**
     *
     * @return numbers of vertices in each polyline
     */
    public int[] getLstrips()
    {
        return lstrips;
    }

    /**
     *
     * @return number of indices
     */
    public int getNinds()
    {
        return ninds;
    }

    /**
     *
     * @return number of polylines
     */
    public int getNlstrips()
    {
        return nlstrips;
    }

    /**
     *
     * @return array of normals
     */
    public float[] getNormals()
    {
        return normals;
    }

    /**
     *
     * @return number of triangle strips
     */
    public int getNstrips()
    {
        return nstrips;
    }

    /**
     *
     * @return number of vertices
     */
    public int getNverts()
    {
        return nverts;
    }

    /**
     *
     * @return
     */
    public int[] getPntsIndex()
    {
        return pntsIndex;
    }

    /**
     *
     * @return
     */
    public int[] getStrips()
    {
        return strips;
    }

    /**
     *
     * @return coordinates of vertices
     */
    public float[] getVerts()
    {
        return verts;
    }

}
