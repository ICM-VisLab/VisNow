/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.geometryTemplates;

import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 * All constructors of member classes have the lod (level of detail) parameter
 *
 */
public class VectorTemplates
{

    //add new glyphs here
    public static class SegmentTemplate extends Glyph
    {
        public SegmentTemplate(Integer lod)
        {
            name = "segment";
            type = LINE_STRIPS;
            refinable = false;
            nstrips = 1;
            nverts = 2;
            ninds = 2;

            strips = new int[]{2};
            verts = new float[]{0, 0, 0, 0, 0, 1};
            normals = new float[6];
            pntsIndex = new int[]{0, 1};
            clrsIndex = new int[]{0, 0};
        }
    }

    public static class SymmetricSegmentTemplate extends Glyph
    {
        public SymmetricSegmentTemplate(Integer lod)
        {
            name = "symmetric segment";
            type = LINE_STRIPS;
            refinable = false;
            nstrips = 1;
            nverts = 2;
            ninds = 2;
            strips = new int[]{2};
            verts = new float[]{0, 0, -1, 0, 0, 1};
            normals = new float[6];
            pntsIndex = new int[]{0, 1};
            clrsIndex = new int[]{0, 0};
        }

    }

    public static class SimpleArrowTemplate extends Glyph
    {
        public SimpleArrowTemplate(Integer lod)
        {
            name = "simple arrow";
            type = LINE_STRIPS;
            refinable = false;
            nstrips = 2;
            nverts = 5;
            ninds = 6;

            strips = new int[]{3, 3};
            verts = new float[]{0, 0, 0, 0, 0, 1, .1f, 0, .8f,
                                -.05f, -.08f, .8f, -.05f, .08f, .8f};
            normals = new float[15];
            pntsIndex = new int[]{0, 1, 2, 3, 1, 4};
            clrsIndex = new int[]{0, 0, 0, 0, 0, 0};
        }
    }

    public static class CircleTemplate extends Glyph
    {
        public CircleTemplate(Integer lod)
        {
            name = "circle";
            type = LINE_STRIPS;
            refinable = true;
            nstrips = 1;
            nverts = 4 * lod + 1;
            ninds = 4 * lod + 1;
            strips = new int[1];

            verts = new float[3 * nverts];
            pntsIndex = new int[nverts];
            clrsIndex = new int[nverts];
            normals = new float[3 * nverts];

            strips[0] = nverts;
            for (int i = 0; i < verts.length; i++)
                verts[i] = 0;
            for (int i = 0; i <= 4 * lod; i++) {
                double phi = (PI * i) / (2. * lod);
                verts[3 * i] = (float) (cos(phi) / 6.);
                verts[3 * i + 1] = (float) (sin(phi) / 6.);
            }
            for (int i = 0; i < pntsIndex.length; i++) {
                pntsIndex[i] = i;
                clrsIndex[i] = 0;
            }
        }

        public CircleTemplate()
        {
            this(5);
        }
    }

    public static class TubeTemplate extends Glyph
    {

        public TubeTemplate(Integer lod)
        {
            name = "tube";
            type = TRIANGLE_STRIPS;
            refinable = true;
            int n = 4 * lod + 5;
            nstrips = 1;
            nverts = 2 * n + 2;
            ninds = 2 * n + 2;
            strips = new int[1];

            verts = new float[3 * nverts];
            normals = new float[3 * nverts];
            pntsIndex = new int[nverts];
            clrsIndex = new int[nverts];

            strips[0] = nverts;
            for (int i = 0; i < verts.length; i++)
                verts[i] = normals[i] = 0;
            for (int i = 0; i < n + 1; i++) {
                double phi = -2. * (PI * i) / n;
                verts[6 * i] = verts[6 * i + 3] = (float) (cos(phi));
                verts[6 * i + 1] = verts[6 * i + 3 + 1] = (float) (sin(phi));
                verts[6 * i + 2] = 0;
                verts[6 * i + 3 + 2] = 1;
                normals[6 * i] = normals[6 * i + 3] = (float) (cos(phi));
                normals[6 * i + 1] = normals[6 * i + 4] = (float) (sin(phi));
                normals[6 * i + 2] = normals[6 * i + 5] = 0;
            }
            for (int i = 0; i < pntsIndex.length; i++) {
                pntsIndex[i] = i;
                clrsIndex[i] = 0;
            }
        }

        public TubeTemplate()
        {
            this(10);
        }
    }

    public static class ConeTemplate extends Glyph
    {

        /**
         * Creates a new instance of SphereTemplate
         */
        public ConeTemplate(Integer lod)
        {
            name = "cone";
            type = TRIANGLE_FANS;
            refinable = true;
            nstrips = 1;
            nverts = 4 * lod + 2;
            ninds = 4 * lod + 2;
            strips = new int[1];

            verts = new float[3 * nverts];
            normals = new float[3 * nverts];
            pntsIndex = new int[nverts];
            clrsIndex = new int[nverts];

            strips[0] = nverts;
            for (int i = 0; i < verts.length; i++)
                verts[i] = normals[i] = 0;
            verts[2] = 1;
            normals[2] = 1;
            double s = sqrt(37. / 36.);
            for (int i = 0; i < 4 * lod + 1; i++) {
                double phi = -(PI * i) / (2. * lod);
                verts[3 * (i + 1)] = (float) (cos(phi) / 6.);
                verts[3 * (i + 1) + 1] = (float) (sin(phi) / 6.);
                normals[3 * (i + 1)] = (float) (cos(phi));
                normals[3 * (i + 1) + 1] = (float) (sin(phi));
                normals[3 * (i + 1) + 2] = 1 / (float) (6 * s);
            }
            for (int i = 0; i < pntsIndex.length; i++) {
                pntsIndex[i] = i;
                clrsIndex[i] = 0;
            }
        }

        public ConeTemplate()
        {
            this(10);
        }
    }

    public static class Arrow3dTemplate extends Glyph
    {
        public Arrow3dTemplate(Integer lod)
        {
            name = "arrow 3d";
            type = TRIANGLE_STRIPS;
            refinable = true;
            int n = 4 * lod + 5;
            nstrips = 2;
            nverts = 4 * n + 4;
            ninds = 4 * n + 4;
            strips = new int[]{2 * n + 2, 2 * n + 2};

            verts = new float[3 * nverts];
            normals = new float[3 * nverts];
            pntsIndex = new int[nverts];
            clrsIndex = new int[nverts];
            int m = 6 * n + 6;

            for (int i = 0; i < verts.length; i++) {
                verts[i] = normals[i] = 0;
            }
            for (int i = 0; i < n + 1; i++) {
                double phi = -2. * (PI * i) / n;
                verts[6 * i] =     verts[6 * i + 3] = (float) (.06 * cos(phi));
                verts[6 * i + 1] = verts[6 * i + 4] = (float) (.06 * sin(phi));
                verts[6 * i + 2] = 0;
                verts[6 * i + 5] = .8f;
                normals[6 * i] =     normals[6 * i + 3] = -(float) (cos(phi));
                normals[6 * i + 1] = normals[6 * i + 4] = -(float) (sin(phi));
                normals[6 * i + 2] = normals[6 * i + 5] = 0;
                verts[m + 6 * i] =     (float) (.15 * cos(phi));
                verts[m + 6 * i + 1] = (float) (.15 * sin(phi));
                verts[m + 6 * i + 2] = .6f;
                verts[m + 6 * i + 3] = verts[m + 6 * i + 4] = 0;
                verts[m + 6 * i + 5] = 1f;
                normals[m + 6 * i] =     normals[m + 6 * i + 3] = -(float) (cos(phi));
                normals[m + 6 * i + 1] = normals[m + 6 * i + 4] = -(float) (sin(phi));
                normals[m + 6 * i + 2] = normals[m + 6 * i + 5] = 0;
            }
            for (int i = 0; i < pntsIndex.length; i++) {
                pntsIndex[i] = i;
                clrsIndex[i] = 0;
            }
        }

        public Arrow3dTemplate()
        {
            this(10);
        }
    }

    public static class ArrowTemplate extends Glyph
    {

        /**
         * Creates a new instance of DiamondTemplate
         */
        public ArrowTemplate(Integer lod)
        {
            name = "arrow";
            type = LINE_STRIPS;
            refinable = false;
            nstrips = 1;
            nverts = 6;
            ninds = 8;

            strips = new int[1];

            float[] v = {0, 0, 0, 0, 0, 1.f,
                         0, .1f, .8f, 0, -.1f, .8f,
                         .1f, 0, .8f, -.1f, 0, .8f};
            verts = v;
            normals = new float[18];
            int[] pInd = {0, 1, 2, 3, 1, 4, 5, 1};
            pntsIndex = pInd;
            clrsIndex = new int[ninds];
            strips[0] = ninds;
            for (int i = 0; i < clrsIndex.length; i++)
                clrsIndex[i] = 0;
        }
    }
}
