/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.interactiveGlyphs;

import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedLineArray;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.RECTANGLE;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams.*;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class RectangleGlyph extends Glyph
{
    protected float[] lineColors = { 0, 0, 1, 1, 0, 0};
    
    public RectangleGlyph(InteractiveGlyphParams params)
    {
        super(params);
        type = RECTANGLE;
        setName("rectangle glyph");
        visibleWidgets = W_ROT_VIS | 
                         U_TRANS_VIS | V_TRANS_VIS | 
                         U_RANGE_VIS | V_RANGE_VIS | 
                         SCALE_VIS   | VOLUME_VIS;
        params.setVisibleWidgets(visibleWidgets);
        glyphVerts = new float[] {-1, -1, 0,   -1,  1, 0,
                                   1, -1, 0,    1,  1, 0, 
                                  -1, -1, 0,    1, -1, 0,
                                  -1,  1, 0,    1,  1, 0};
        lines = new IndexedLineArray(8,  GeometryArray.COORDINATES | GeometryArray.COLOR_3, 8);
        lines.setCoordinateIndices(0, new int[] { 0,  1,  2,  3,  4,  5,  6,  7});
        lines.setColorIndices(0, new int[]      { 0,  0,  0,  0,  1,  1,  1,  1});
        lines.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        lines.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        lines.setCapability(GeometryArray.ALLOW_COLOR_READ);
        lines.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
        lines.setCoordinates(0, glyphVerts);
        lines.setColors(0, lineColors);
        lineShape.addGeometry(lines);
        glyphGroup.addChild(lineShape);
        reper = new Reper2D(params);
        addChild(reper);
    }
    
    @Override
    public void update()
    {
        for (int i = 0; i < 2; i++) {
            float c = params.center[i];
            float u0 = params.uRange[0] * params.u[i];
            float u1 = params.uRange[1] * params.u[i];
            float v0 = params.vRange[0] * params.v[i];
            float v1 = params.vRange[1] * params.v[i];
            
            glyphVerts[     i] = glyphVerts[12 + i] = c + u0 + v0;
            glyphVerts[ 3 + i] = glyphVerts[18 + i] = c + u0 + v1;
            glyphVerts[ 6 + i] = glyphVerts[15 + i] = c + u1 + v0;
            glyphVerts[ 9 + i] = glyphVerts[21 + i] = c + u1 + v1;
            
            System.arraycopy(currentColors[2 - i], 0, lineColors, 3 * i, 3);
        }
        lines.setCoordinates(0, glyphVerts);
        lines.setColors(0, lineColors);
        float s = params.getScale();
        if (showReper && reper.getParent() == null)
            addChild(reper);
        if (!showReper && reper.getParent() != null)
            removeChild(reper);
        if (reper.getParent() != null)
            reper.update(new float[] {s, s, s});
    }
}
