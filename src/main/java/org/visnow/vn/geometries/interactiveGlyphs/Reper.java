/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.interactiveGlyphs;

import org.jogamp.java3d.IndexedLineStripArray;
import org.visnow.vn.geometries.objects.generics.OpenBranchGroup;
import org.visnow.vn.geometries.objects.generics.OpenShape3D;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
abstract public class Reper extends OpenBranchGroup
{
    protected InteractiveGlyphParams params;
    protected IndexedLineStripArray glyphLines;
    protected OpenShape3D lineShape    = new OpenShape3D("lines");
    protected float[]   scale;
    protected float[]   glyphVerts;
    protected float[][] currentColors = InteractiveGlyphParams.LIGHT_COLORS;
    protected float[]   lineColors = {0, .6f, .6f, .6f, .6f, 0, .6f, 0, .6f};
    
    public Reper(InteractiveGlyphParams params)
    {
        super();
        this.params = params;
        scale = new float[] {1, 1, 1};
    }
    
    public Reper(InteractiveGlyphParams params, float[] scale)
    {
        super();
        this.params = params;
        this.scale = scale;
    }
    
    public void updateColors(float[][] currentColors)
    {
        for (int i = 0; i < 3; i++)
            System.arraycopy(currentColors[(i + 1) % 3], 0, lineColors, 3 * i, 3);
    }
    
    abstract public void update(float[] scale);
    
    public final void update()
    {
        update(scale);
    }
}
