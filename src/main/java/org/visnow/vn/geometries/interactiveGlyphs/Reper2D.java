/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.interactiveGlyphs;

import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedLineStripArray;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class Reper2D extends Reper
{
    
    public Reper2D(InteractiveGlyphParams params)
    {
        super(params);
        glyphVerts = new float[24];
        glyphLines = new IndexedLineStripArray(8,  GeometryArray.COORDINATES | GeometryArray.COLOR_3, 
                                               10, new int[] {2, 3, 2, 3});
        update();
        glyphLines.setCoordinateIndices(0, new int[] {0,  1,  2,  1,  3,  
                                                      4,  5,  6,  5, 7});
        glyphLines.setColorIndices(0, new int[] {0, 0, 0, 0, 0,
                                                 1, 1, 1, 1, 1});
        glyphLines.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        glyphLines.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        glyphLines.setCapability(GeometryArray.ALLOW_COLOR_READ);
        glyphLines.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
        glyphLines.setCoordinates(0, glyphVerts);
        glyphLines.setColors(0, lineColors);
        lineShape.addGeometry(glyphLines);
        addChild(lineShape);
        currentColors = params.getCurrentColors();
        for (int i = 0; i < 3; i++)
            System.arraycopy(currentColors[(i + 1) % 3], 0, lineColors, 3 * i, 3);
    }
    
    private void addArrow(float[] center, float scale, 
                          float[] uu, float[] vv, int s)
    {
        for (int i = 0; i < 3; i++) {
            float c = center[i];
            float u = scale * uu[i];
            float v = scale * vv[i];
            glyphVerts[s + i]       = c - u;
            glyphVerts[s + 3 + i]   = c + u;
            glyphVerts[s + 6 + i]   = c + .85f * u - .07f * v;
            glyphVerts[s + 9 + i]   = c + .85f * u + .07f * v;
        }
    }
    
    public final void update(float[] scale)
    {
        float[][] r = new float[][] {params.getU(), params.getV()};
        for (int i = 0; i < 2; i++) 
            addArrow(params.getCenter(), scale[i], r[i], r[(i + 1) % 2], 12 * i);
        glyphLines.setCoordinates(0, glyphVerts);
        glyphLines.setColors(0, lineColors);
    }
    
}
