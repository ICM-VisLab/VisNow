/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.interactiveGlyphs;

import org.jogamp.java3d.GeometryArray;
import org.jogamp.java3d.IndexedLineArray;
import org.jogamp.java3d.IndexedQuadArray;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.PLANE;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams.*;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class PlaneGlyph extends Glyph
{
    protected float[] planeVerts = {-1, -1, 0,
                                     1, -1, 0,
                                     1,  1, 0,
                                    -1,  1, 0};
    
    public PlaneGlyph(InteractiveGlyphParams params)
    {
        super(params);
        reper = new Reper3D(params);
        type = PLANE;
        setName("plane glyph");
        params.setRadiusSliderTitle("planar area dimension (in % of field size)");
        visibleWidgets = U_ROT_VIS   | V_ROT_VIS   | W_ROT_VIS |
                         U_TRANS_VIS | V_TRANS_VIS | W_TRANS_VIS | 
                         U_RANGE_VIS | V_RANGE_VIS | 
                         SCALE_VIS   | AXES_VIS;
        params.setVisibleWidgets(visibleWidgets);
        glyphVerts = new float[] {-1, -1, 0,   -1,  1, 0,
                                   1, -1, 0,    1,  1, 0, 
                                  -1, -1, 0,    1, -1, 0,
                                  -1,  1, 0,    1,  1, 0};
        glyphRects = new IndexedQuadArray(4, GeometryArray.COORDINATES, 4);
        glyphRects.setCoordinateIndices(0, new int[] {0, 1, 2, 3});
        glyphRects.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        glyphRects.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        glyphRects.setCoordinates(0, planeVerts);
        surfShape.setAppearance(surfApp);
        surfShape.addGeometry(glyphRects);
        glyphGroup.addChild(surfShape);
        lines = new IndexedLineArray(8,  GeometryArray.COORDINATES | GeometryArray.COLOR_3, 8);
        lines.setCoordinateIndices(0, new int[] { 0, 1, 2, 3, 4, 5, 6, 7});
        lines.setColorIndices(0, new int[]      { 2, 2, 2, 2, 1, 1, 1, 1});
        lines.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        lines.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        lines.setCapability(GeometryArray.ALLOW_COLOR_READ);
        lines.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
        lines.setCoordinates(0, glyphVerts);
        lines.setColors(0, lineColors);
        lineShape.addGeometry(lines);
        glyphGroup.addChild(lineShape);
        currentColors = params.getCurrentColors();
        addChild(reper);
    }
    
    @Override
    public void updateColors()
    {
        reper.updateColors(currentColors);
    }
    
    @Override
    public void update()
    {
        for (int i = 0; i < 3; i++) {
            float c = params.center[i];
            float u0 = params.uRange[0] * params.u[i];
            float u1 = params.uRange[1] * params.u[i];
            float v0 = params.vRange[0] * params.v[i];
            float v1 = params.vRange[1] * params.v[i];
            planeVerts[i]      = c + u0 + v0;
            planeVerts[3 + i]  = c + u1 + v0;
            planeVerts[6 + i]  = c + u1 + v1;
            planeVerts[9 + i]  = c + u0 + v1;
            
            glyphVerts[     i] = glyphVerts[12 + i] = c + u0 + v0;
            glyphVerts[ 3 + i] = glyphVerts[18 + i] = c + u0 + v1;
            glyphVerts[ 6 + i] = glyphVerts[15 + i] = c + u1 + v0;
            glyphVerts[ 9 + i] = glyphVerts[21 + i] = c + u1 + v1;
        }
        glyphRects.setCoordinates(0, planeVerts);
        lines.setCoordinates(0, glyphVerts);
        float s = params.getScale();
        reper.update(new float[]{.3f * s, .3f * s, s});
    }
}
