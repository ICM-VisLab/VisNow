/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.interactiveGlyphs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.gui.events.FloatValueModificationEvent;
import org.visnow.vn.gui.events.FloatValueModificationListener;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyphParams.*;
import org.visnow.vn.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider;
import org.visnow.vn.gui.widgets.UnboundedRoller.UnboundedRoller;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 * @author know
 */
public class InteractiveGlyphGUI extends javax.swing.JPanel
{
    protected boolean rangeGUIVisible = true;
    protected boolean centerGUIVisible = true;
    InteractiveGlyphParams params;
    
    protected int axis = 2;
    protected float scaleFactor = 1;
    protected float[][] colors = InteractiveGlyphParams.LIGHT_COLORS;
    protected Color wColor = new Color(colors[0][0], colors[0][1], colors[0][2]);
    protected Color uColor = new Color(colors[1][0], colors[1][1], colors[1][2]);
    protected Color vColor = new Color(colors[2][0], colors[2][1], colors[2][2]);
    /**
     * Creates new form SimplePlanarSliceGUI
     */
    public InteractiveGlyphGUI()
    {
        initComponents();
        uRotationRoller.addFloatValueModificationListener(new FloatValueModificationListener()
        {
            @Override
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                params.setShow(true);
                params.setAdjusting(uRotationRoller.isAdjusting() ? 0 : -1);
                params.setRotation(new float[] {uRotationRoller.getValue(), 
                                                vRotationRoller.getValue(), 
                                                wRotationRoller.getValue()});
            }
        });
        uRotationRoller.setLargeIncrement(90);
        vRotationRoller.addFloatValueModificationListener(new FloatValueModificationListener()
        {
            @Override
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                params.setShow(true);
                params.setAdjusting(vRotationRoller.isAdjusting() ? 1 : -1);
                params.setRotation(new float[] {uRotationRoller.getValue(), 
                                                vRotationRoller.getValue(), 
                                                wRotationRoller.getValue()});
            }
        });
        vRotationRoller.setLargeIncrement(90);
        wRotationRoller.addFloatValueModificationListener(new FloatValueModificationListener()
        {
            @Override
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                params.setShow(true);
                params.setAdjusting(wRotationRoller.isAdjusting() ? 2 : -1);
                params.setRotation(new float[] {uRotationRoller.getValue(), 
                                                vRotationRoller.getValue(), 
                                                wRotationRoller.getValue()});
            }
        });
        wRotationRoller.setLargeIncrement(90);
        uTranslationRoller.addFloatValueModificationListener(new FloatValueModificationListener()
        {
            @Override
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                params.setShow(true);
                params.setAdjusting(uTranslationRoller.isAdjusting() ? 3 : -1);
                params.setShiftU(uTranslationRoller.getValue());
                if (!uTranslationRoller.isAdjusting())
                    uTranslationRoller.setOutValue(0);
            }
        });
        vTranslationRoller.addFloatValueModificationListener(new FloatValueModificationListener()
        {
            @Override
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                params.setShow(true);
                params.setAdjusting(vTranslationRoller.isAdjusting() ? 4 : -1);
                params.setShiftV(vTranslationRoller.getValue());
                if (!vTranslationRoller.isAdjusting())
                    vTranslationRoller.setOutValue(0);
            }
        });
        wTranslationRoller.addFloatValueModificationListener(new FloatValueModificationListener()
        {
            @Override
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                params.setShow(true);
                params.setAdjusting(wTranslationRoller.isAdjusting() ? 5 : -1);
                params.setShiftW(wTranslationRoller.getValue());
                if (!wTranslationRoller.isAdjusting())
                    wTranslationRoller.setOutValue(0);
            }
        });
        updateSliderBorders(InteractiveGlyphParams.LIGHT_COLORS);
        xLeftButton.setVisible(false);
        xRightButton.setVisible(false);
        yLeftButton.setVisible(false);
        yRightButton.setVisible(false);
        zLeftButton.setVisible(false);
        zRightButton.setVisible(false);
    }
    
    public void updateSliderBorders(float[][] colors)
    {
        wColor = new Color(colors[0][0], colors[0][1], colors[0][2]);
        uColor = new Color(colors[1][0], colors[1][1], colors[1][2]);
        vColor = new Color(colors[2][0], colors[2][1], colors[2][2]);
        uRotationRoller.setBackground(uColor);
        vRotationRoller.setBackground(vColor);
        wRotationRoller.setBackground(wColor);
        uTranslationRoller.setBackground(uColor);
        vTranslationRoller.setBackground(vColor);
        wTranslationRoller.setBackground(wColor);
        uRangeSlider.setBorder(BorderFactory.createLineBorder(uColor, 2));
        vRangeSlider.setBorder(BorderFactory.createLineBorder(vColor, 2));
        wRangeSlider.setBorder(BorderFactory.createLineBorder(wColor, 2));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        GridBagConstraints gridBagConstraints;

        buttonGroup1 = new ButtonGroup();
        jPanel1 = new JPanel();
        centerButton = new JButton();
        hideGlyphButton = new JButton();
        initOrientationPanel = new JPanel();
        xButton = new JRadioButton();
        yButton = new JRadioButton();
        zButton = new JRadioButton();
        rotationsPanel = new JPanel();
        uRotationRoller = new UnboundedRoller();
        vRotationRoller = new UnboundedRoller();
        wRotationRoller = new UnboundedRoller();
        xLeftButton = new JButton();
        yLeftButton = new JButton();
        zLeftButton = new JButton();
        xRightButton = new JButton();
        yRightButton = new JButton();
        zRightButton = new JButton();
        glyphScaleSlider = new JSlider();
        rangePanel = new JPanel();
        uRangeSlider = new ExtendedFloatSubRangeSlider();
        vRangeSlider = new ExtendedFloatSubRangeSlider();
        wRangeSlider = new ExtendedFloatSubRangeSlider();
        radiusSlider = new JSlider();
        volLabel = new JLabel();
        autoUpdateBox = new JCheckBox();
        jPanel2 = new JPanel();
        translationsPanel = new JPanel();
        uTranslationRoller = new UnboundedRoller();
        vTranslationRoller = new UnboundedRoller();
        wTranslationRoller = new UnboundedRoller();

        setPreferredSize(new Dimension(220, 844));
        setLayout(new GridBagLayout());

        jPanel1.setLayout(new GridLayout(1, 0));

        centerButton.setText("center");
        centerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                centerButtonActionPerformed(evt);
            }
        });
        jPanel1.add(centerButton);

        hideGlyphButton.setText("hide ");
        hideGlyphButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                hideGlyphButtonActionPerformed(evt);
            }
        });
        jPanel1.add(hideGlyphButton);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        add(jPanel1, gridBagConstraints);

        initOrientationPanel.setBorder(BorderFactory.createTitledBorder("initial orientation"));
        initOrientationPanel.setPreferredSize(new Dimension(220, 50));
        initOrientationPanel.setLayout(new GridLayout(1, 0));

        buttonGroup1.add(xButton);
        xButton.setText("x"); // NOI18N
        xButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                xButtonActionPerformed(evt);
            }
        });
        initOrientationPanel.add(xButton);

        buttonGroup1.add(yButton);
        yButton.setText("y"); // NOI18N
        yButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                yButtonActionPerformed(evt);
            }
        });
        initOrientationPanel.add(yButton);

        buttonGroup1.add(zButton);
        zButton.setSelected(true);
        zButton.setText("z"); // NOI18N
        zButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                zButtonActionPerformed(evt);
            }
        });
        initOrientationPanel.add(zButton);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(initOrientationPanel, gridBagConstraints);

        rotationsPanel.setBorder(BorderFactory.createTitledBorder("rotations (in local coordinate system)"));
        rotationsPanel.setMinimumSize(new Dimension(190, 50));
        rotationsPanel.setPreferredSize(new Dimension(220, 60));
        rotationsPanel.setLayout(new GridBagLayout());

        uRotationRoller.setBackground(uColor);
        uRotationRoller.setMinimumSize(new Dimension(60, 20));
        uRotationRoller.setPreferredSize(new Dimension(100, 30));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(2, 2, 2, 2);
        rotationsPanel.add(uRotationRoller, gridBagConstraints);

        vRotationRoller.setBackground(vColor);
        vRotationRoller.setMinimumSize(new Dimension(60, 20));
        vRotationRoller.setName(""); // NOI18N
        vRotationRoller.setPreferredSize(new Dimension(100, 30));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(1, 7, 1, 7);
        rotationsPanel.add(vRotationRoller, gridBagConstraints);

        wRotationRoller.setBackground(wColor);
        wRotationRoller.setMinimumSize(new Dimension(60, 20));
        wRotationRoller.setName(""); // NOI18N
        wRotationRoller.setPreferredSize(new Dimension(100, 30));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(1, 1, 1, 1);
        rotationsPanel.add(wRotationRoller, gridBagConstraints);

        xLeftButton.setText("<");
        xLeftButton.setMargin(new Insets(2, 2, 2, 2));
        xLeftButton.setMinimumSize(new Dimension(40, 20));
        xLeftButton.setPreferredSize(new Dimension(40, 20));
        xLeftButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                xLeftButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = GridBagConstraints.LINE_START;
        rotationsPanel.add(xLeftButton, gridBagConstraints);

        yLeftButton.setText("<");
        yLeftButton.setMargin(new Insets(2, 2, 2, 2));
        yLeftButton.setMinimumSize(new Dimension(40, 20));
        yLeftButton.setPreferredSize(new Dimension(40, 20));
        yLeftButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                yLeftButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new Insets(0, 8, 0, 0);
        rotationsPanel.add(yLeftButton, gridBagConstraints);

        zLeftButton.setText("<");
        zLeftButton.setMargin(new Insets(2, 2, 2, 2));
        zLeftButton.setMinimumSize(new Dimension(40, 20));
        zLeftButton.setOpaque(false);
        zLeftButton.setPreferredSize(new Dimension(40, 20));
        zLeftButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                zLeftButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = GridBagConstraints.LINE_START;
        rotationsPanel.add(zLeftButton, gridBagConstraints);

        xRightButton.setText(">");
        xRightButton.setMargin(new Insets(2, 2, 2, 2));
        xRightButton.setMinimumSize(new Dimension(40, 20));
        xRightButton.setPreferredSize(new Dimension(40, 20));
        xRightButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                xRightButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = GridBagConstraints.LINE_END;
        rotationsPanel.add(xRightButton, gridBagConstraints);

        yRightButton.setText(">");
        yRightButton.setMargin(new Insets(2, 2, 2, 2));
        yRightButton.setMinimumSize(new Dimension(40, 20));
        yRightButton.setPreferredSize(new Dimension(40, 20));
        yRightButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                yRightButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new Insets(0, 0, 0, 7);
        rotationsPanel.add(yRightButton, gridBagConstraints);

        zRightButton.setText(">");
        zRightButton.setMargin(new Insets(2, 2, 2, 2));
        zRightButton.setMinimumSize(new Dimension(40, 20));
        zRightButton.setPreferredSize(new Dimension(40, 20));
        zRightButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                zRightButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = GridBagConstraints.LINE_END;
        rotationsPanel.add(zRightButton, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(4, 0, 4, 0);
        add(rotationsPanel, gridBagConstraints);

        glyphScaleSlider.setMajorTickSpacing(20);
        glyphScaleSlider.setMinorTickSpacing(5);
        glyphScaleSlider.setPaintLabels(true);
        glyphScaleSlider.setPaintTicks(true);
        glyphScaleSlider.setBorder(BorderFactory.createTitledBorder("glyph scale"));
        glyphScaleSlider.setMaximumSize(new Dimension(32767, 65));
        glyphScaleSlider.setMinimumSize(new Dimension(36, 55));
        glyphScaleSlider.setOpaque(false);
        glyphScaleSlider.setPreferredSize(new Dimension(200, 60));
        glyphScaleSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent evt) {
                glyphScaleSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 5, 0, 5);
        add(glyphScaleSlider, gridBagConstraints);

        rangePanel.setBorder(BorderFactory.createEtchedBorder());
        rangePanel.setLayout(new GridBagLayout());

        uRangeSlider.setBorder(BorderFactory.createEtchedBorder());
        uRangeSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent evt) {
                uRangeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 3, 0, 3);
        rangePanel.add(uRangeSlider, gridBagConstraints);

        vRangeSlider.setBorder(BorderFactory.createEtchedBorder());
        vRangeSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent evt) {
                vRangeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 3, 0, 3);
        rangePanel.add(vRangeSlider, gridBagConstraints);

        wRangeSlider.setBorder(BorderFactory.createEtchedBorder());
        wRangeSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent evt) {
                wRangeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 2, 0, 2);
        rangePanel.add(wRangeSlider, gridBagConstraints);

        radiusSlider.setMajorTickSpacing(20);
        radiusSlider.setMinorTickSpacing(5);
        radiusSlider.setPaintLabels(true);
        radiusSlider.setPaintTicks(true);
        radiusSlider.setBorder(BorderFactory.createTitledBorder("circle/sphere radius"));
        radiusSlider.setMaximumSize(new Dimension(32767, 65));
        radiusSlider.setMinimumSize(new Dimension(36, 55));
        radiusSlider.setOpaque(false);
        radiusSlider.setPreferredSize(new Dimension(200, 60));
        radiusSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent evt) {
                radiusSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        rangePanel.add(radiusSlider, gridBagConstraints);

        volLabel.setText("jLabel1");
        volLabel.setMaximumSize(new Dimension(300, 25));
        volLabel.setPreferredSize(new Dimension(220, 20));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(0, 6, 0, 6);
        rangePanel.add(volLabel, gridBagConstraints);

        autoUpdateBox.setSelected(true);
        autoUpdateBox.setText("autoupdate box dimensions");
        autoUpdateBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                autoUpdateBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(0, 7, 0, 0);
        rangePanel.add(autoUpdateBox, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(rangePanel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        add(jPanel2, gridBagConstraints);

        translationsPanel.setBorder(BorderFactory.createTitledBorder("<html>glyph translations <p>(in local coordinate system)"));
        translationsPanel.setMinimumSize(new Dimension(190, 60));
        translationsPanel.setOpaque(false);
        translationsPanel.setPreferredSize(new Dimension(220, 65));
        translationsPanel.setLayout(new GridBagLayout());

        uTranslationRoller.setBackground(uColor);
        uTranslationRoller.setMinimumSize(new Dimension(60, 20));
        uTranslationRoller.setPreferredSize(new Dimension(200, 30));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(2, 2, 2, 2);
        translationsPanel.add(uTranslationRoller, gridBagConstraints);

        vTranslationRoller.setBackground(vColor);
        vTranslationRoller.setMinimumSize(new Dimension(60, 20));
        vTranslationRoller.setName(""); // NOI18N
        vTranslationRoller.setPreferredSize(new Dimension(200, 30));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(1, 1, 1, 1);
        translationsPanel.add(vTranslationRoller, gridBagConstraints);

        wTranslationRoller.setBackground(wColor);
        wTranslationRoller.setMinimumSize(new Dimension(60, 20));
        wTranslationRoller.setName(""); // NOI18N
        wTranslationRoller.setPreferredSize(new Dimension(200, 30));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(1, 1, 1, 1);
        translationsPanel.add(wTranslationRoller, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(4, 0, 4, 0);
        add(translationsPanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void xButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_xButtonActionPerformed
    {//GEN-HEADEREND:event_xButtonActionPerformed
        setAxis();
    }//GEN-LAST:event_xButtonActionPerformed

    private void yButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_yButtonActionPerformed
    {//GEN-HEADEREND:event_yButtonActionPerformed
        setAxis();
    }//GEN-LAST:event_yButtonActionPerformed

    private void zButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_zButtonActionPerformed
    {//GEN-HEADEREND:event_zButtonActionPerformed
        setAxis();
    }//GEN-LAST:event_zButtonActionPerformed

    private void glyphScaleSliderStateChanged(ChangeEvent evt)//GEN-FIRST:event_glyphScaleSliderStateChanged
    {//GEN-HEADEREND:event_glyphScaleSliderStateChanged
        params.setShow(true);
        params.setScale((float)Math.exp((glyphScaleSlider.getValue() - 60) / 20.f)); 
    }//GEN-LAST:event_glyphScaleSliderStateChanged

    
    private void uRangeSliderStateChanged(ChangeEvent evt)//GEN-FIRST:event_uRangeSliderStateChanged
    {//GEN-HEADEREND:event_uRangeSliderStateChanged
        params.setShow(true);
        params.setAdjusting(uRangeSlider.isAdjusting() ? 3 : -1);
        params.setURange(new float[] {uRangeSlider.getLow(), uRangeSlider.getUp()});
    }//GEN-LAST:event_uRangeSliderStateChanged

    private void vRangeSliderStateChanged(ChangeEvent evt)//GEN-FIRST:event_vRangeSliderStateChanged
    {//GEN-HEADEREND:event_vRangeSliderStateChanged
        params.setShow(true);
        params.setAdjusting(vRangeSlider.isAdjusting() ? 4 : -1);
        params.setVRange(new float[] {vRangeSlider.getLow(), vRangeSlider.getUp()});
    }//GEN-LAST:event_vRangeSliderStateChanged

    private void wRangeSliderStateChanged(ChangeEvent evt)//GEN-FIRST:event_wRangeSliderStateChanged
    {//GEN-HEADEREND:event_wRangeSliderStateChanged
        params.setShow(true);
        params.setAdjusting(wRangeSlider.isAdjusting() ? 5 : -1);
        params.setWRange(new float[] {wRangeSlider.getLow(), wRangeSlider.getUp()});
    }//GEN-LAST:event_wRangeSliderStateChanged

    private void radiusSliderStateChanged(ChangeEvent evt)//GEN-FIRST:event_radiusSliderStateChanged
    {//GEN-HEADEREND:event_radiusSliderStateChanged
        params.setShow(true);
        params.setAdjusting(radiusSlider.getValueIsAdjusting() ? 6 : -1);
        params.setRadius(scaleFactor * (float)Math.exp((radiusSlider.getValue() - 60) / 20.f));  
    }//GEN-LAST:event_radiusSliderStateChanged

    private void hideGlyphButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_hideGlyphButtonActionPerformed
    {//GEN-HEADEREND:event_hideGlyphButtonActionPerformed
        params.setShow(false);
    }//GEN-LAST:event_hideGlyphButtonActionPerformed

    private void autoUpdateBoxActionPerformed(ActionEvent evt)//GEN-FIRST:event_autoUpdateBoxActionPerformed
    {//GEN-HEADEREND:event_autoUpdateBoxActionPerformed
        params.setAutoUpdateBox(autoUpdateBox.isSelected());
    }//GEN-LAST:event_autoUpdateBoxActionPerformed

    private void centerButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_centerButtonActionPerformed
    {//GEN-HEADEREND:event_centerButtonActionPerformed
        params.setActive(false);
        params.centerBox();
        params.setActive(true);
    }//GEN-LAST:event_centerButtonActionPerformed

    private void xLeftButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_xLeftButtonActionPerformed
        uRotationRoller.setValue(uRotationRoller.getValue() + 90);
    }//GEN-LAST:event_xLeftButtonActionPerformed

    private void xRightButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_xRightButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_xRightButtonActionPerformed

    private void yLeftButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_yLeftButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_yLeftButtonActionPerformed

    private void yRightButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_yRightButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_yRightButtonActionPerformed

    private void zLeftButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_zLeftButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_zLeftButtonActionPerformed

    private void zRightButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_zRightButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_zRightButtonActionPerformed

    public void updateSliderBounds(boolean resetVal)
    {
        params.setActive(false);
        ExtendedFloatSubRangeSlider[] rangeSliders = new ExtendedFloatSubRangeSlider[] 
               {uRangeSlider, vRangeSlider, wRangeSlider};
        scaleFactor = 0;
        for (int i = 0; i < rangeSliders.length; i++) {
            if (resetVal)
                rangeSliders[i].setParams(params.getMinMax()[i][0], params.getMinMax()[i][1],
                                          params.getMinMax()[i][0], params.getMinMax()[i][1]);
            else 
                rangeSliders[i].setMinMax(params.getMinMax()[i][0], params.getMinMax()[i][1]);
            scaleFactor += (params.getMinMax()[i][1] - params.getMinMax()[i][0]) * 
                           (params.getMinMax()[i][1] - params.getMinMax()[i][0]);
        }
        scaleFactor = (float)Math.sqrt(scaleFactor /3);
        params.setActive(true);
    }
    
    public void clearRotationRollers()
    {
        params.setShow(true);
        uRotationRoller.setValue(0);
        vRotationRoller.setValue(0);
        wRotationRoller.setValue(0); 
    }

    public void clearTranslationRollers()
    {
        params.setShow(true);
        uTranslationRoller.setValue(0);
        vTranslationRoller.setValue(0);
        wTranslationRoller.setValue(0); 
    }

    private void setAxis()
    {
        params.setShow(true);
        if (params.getDimension() == 2) {
            if (xButton.isSelected())
                axis = 0;
            else if (yButton.isSelected())
                axis = 1;
        }
        else {
            if (xButton.isSelected())
                axis = 1;
            else if (yButton.isSelected())
                axis = 2;
            else if (zButton.isSelected())
                axis = 0;
        }
        params.setActive(false);
        updateSliderBounds(true);
        clearRotationRollers();
        params.setActive(true);
        params.setAxis(axis); 
    }

    public void setParams(InteractiveGlyphParams params)
    {
        this.params = params;
        params.setGUI(this);
        updateWidgetVisibility();
    }
    
    public void updateWidgetVisibility()
    {
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                int dim = params.getDimension();
                int vis = params.getVisibleWidgets();
                rangeGUIVisible = (vis & (U_RANGE_VIS | V_RANGE_VIS | W_RANGE_VIS)) != 0;
                centerGUIVisible = (vis & (U_TRANS_VIS | V_TRANS_VIS | W_TRANS_VIS)) != 0;
                uRotationRoller.setVisible((vis & U_ROT_VIS) != 0);
                vRotationRoller.setVisible((vis & V_ROT_VIS) != 0);
                wRotationRoller.setVisible((vis & W_ROT_VIS) != 0);
                uRangeSlider.setVisible((vis & U_RANGE_VIS) != 0);
                vRangeSlider.setVisible((vis & V_RANGE_VIS) != 0);
                wRangeSlider.setVisible((vis & W_RANGE_VIS) != 0);
                uTranslationRoller.setVisible((vis & U_TRANS_VIS) != 0);
                vTranslationRoller.setVisible((vis & V_TRANS_VIS) != 0);
                wTranslationRoller.setVisible((vis & W_TRANS_VIS) != 0);
                glyphScaleSlider.setVisible((vis & SCALE_VIS) != 0);
                volLabel.setVisible((vis & VOLUME_VIS) != 0);
                radiusSlider.setBorder(BorderFactory.createTitledBorder(params.getRadiusSliderTitle()));
                radiusSlider.setVisible((vis & RADIUS_VIS) != 0);
                initOrientationPanel.setVisible((vis & AXES_VIS) != 0);
                zButton.setVisible(dim == 3);
                zButton.setSelected(dim == 3);
                xButton.setSelected(dim == 2);
            }
        });
    }
    
    public void updateRangeSliders(float[][]minmax, float[][] range)
    {
        uRangeSlider.setParams(minmax[0][0], minmax[0][1], range[0][0], range[0][1]);
        vRangeSlider.setParams(minmax[1][0], minmax[1][1], range[1][0], range[1][1]);
        wRangeSlider.setParams(minmax[2][0], minmax[2][1], range[2][0], range[2][1]);
    }
    
    public void updateRadiusSliderTitle(String title)
    {
        ((TitledBorder)radiusSlider.getBorder()).setTitle(title);
    }
    
    public void setTranslationRollersSensitivity(float sensitivity)
    {
        uTranslationRoller.setSensitivity(sensitivity);
        vTranslationRoller.setSensitivity(sensitivity);
        wTranslationRoller.setSensitivity(sensitivity);
    }
    
    public void displayVolume(float volume)
    {
        if (volume < .001)
            volLabel.setText(String.format("volume = %e", volume));
        else
            volLabel.setText(String.format("volume = %f", volume));
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected JCheckBox autoUpdateBox;
    protected ButtonGroup buttonGroup1;
    protected JButton centerButton;
    protected JSlider glyphScaleSlider;
    protected JButton hideGlyphButton;
    protected JPanel initOrientationPanel;
    protected JPanel jPanel1;
    protected JPanel jPanel2;
    protected JSlider radiusSlider;
    protected JPanel rangePanel;
    protected JPanel rotationsPanel;
    protected JPanel translationsPanel;
    protected ExtendedFloatSubRangeSlider uRangeSlider;
    protected UnboundedRoller uRotationRoller;
    protected UnboundedRoller uTranslationRoller;
    protected ExtendedFloatSubRangeSlider vRangeSlider;
    protected UnboundedRoller vRotationRoller;
    protected UnboundedRoller vTranslationRoller;
    protected JLabel volLabel;
    protected ExtendedFloatSubRangeSlider wRangeSlider;
    protected UnboundedRoller wRotationRoller;
    protected UnboundedRoller wTranslationRoller;
    protected JRadioButton xButton;
    protected JButton xLeftButton;
    protected JButton xRightButton;
    protected JRadioButton yButton;
    protected JButton yLeftButton;
    protected JButton yRightButton;
    protected JRadioButton zButton;
    protected JButton zLeftButton;
    protected JButton zRightButton;
    // End of variables declaration//GEN-END:variables
}
