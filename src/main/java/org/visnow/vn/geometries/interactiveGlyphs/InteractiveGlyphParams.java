/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.interactiveGlyphs;

import java.util.ArrayList;
import java.util.Arrays;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.vn.engine.core.ParameterChangeListener;
import org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType;
import static org.visnow.vn.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.BOX;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class InteractiveGlyphParams
{
    public static final int U_ROT_VIS   = 1;
    public static final int V_ROT_VIS   = 1 << 1;
    public static final int W_ROT_VIS   = 1 << 2;
    public static final int U_TRANS_VIS = 1 << 3;
    public static final int V_TRANS_VIS = 1 << 4;
    public static final int W_TRANS_VIS = 1 << 5;
    public static final int U_RAD_VIS   = 1 << 6;
    public static final int V_RAD_VIS   = 1 << 7;
    public static final int W_RAD_VIS   = 1 << 8;
    public static final int U_RANGE_VIS = 1 << 9;
    public static final int V_RANGE_VIS = 1 << 10;
    public static final int W_RANGE_VIS = 1 << 11;
    public static final int SCALE_VIS   = 1 << 12;
    public static final int AXES_VIS    = 7 << 13;
    public static final int X_AXIS_VIS  = 1 << 13;
    public static final int Y_AXIS_VIS  = 1 << 14;
    public static final int Z_AXIS_VIS  = 1 << 15;
    public static final int RADIUS_VIS  = 1 << 16;
    public static final int VOLUME_VIS  = 1 << 17;
    public static final float[][] LIGHT_COLORS = {{0,  1,  1},   {1,  0,  1},   {1,  1,  0}};
    public static final float[][] DARK_COLORS  = {{0,.6f,.6f}, {.6f,  0,.6f}, {.6f,.6f,  0}};

    protected InteractiveGlyph glyph;
    protected InteractiveGlyphGUI gui;
    protected int visibleWidgets = 0;
    protected String scaleSliderTitle = "glyph scale";
    protected String radiusSliderTitle = "glyph radius";

    protected FloatLargeArray coords;
    protected float[][] currentColors = LIGHT_COLORS;

    protected boolean active = true;

    protected int     dimension = 3;
    protected int     axis      = 0;

    protected float[] fieldCenter = {0, 0, 0};  // center of local coords system
    protected float[] center      = {0, 0, 0};  // rotation center of the glyph
    protected float[] centerOld   = {0, 0, 0};
    protected float[] baseCenter  = {0, 0, 0};  // rotation center of the glyph retained when center is adjusted
    protected float[] baseU       = {1, 0, 0};  // u reper versor retained when rotation is adjusted
    protected float[] baseV       = {0, 1, 0};
    protected float[] baseW       = {0, 0, 1};

    protected float[] u           = {1, 0, 0};  // current u reper versor
    protected float[] uOld        = {1, 0, 0};
    protected float   shiftU      = 0;          // current center shift in the u direction
    protected float   shiftUOld   = 0;
    protected float[] uMinMax     = {-1, 1};     // extreme values of the u coordinate of the field nodes
    protected float[] uMinMaxOld  = {-1, 1};
    protected float[] uRange      = {-1, 1};    // u range of the selected box
    protected float[] uRangeOld   = {-1, 1};

    protected float[] v           = {0, 1, 0};
    protected float[] vOld        = {0, 1, 0};
    protected float   shiftV      = 0;
    protected float   shiftVOld   = 0;
    protected float[] vMinMax     = {-1, 1};
    protected float[] vMinMaxOld  = {-1, 1};
    protected float[] vRange      = {-1, 1};
    protected float[] vRangeOld   = {-1, 1};

    protected float[] w           = {0, 0, 1};
    protected float[] wOld        = {0, 0, 1};
    protected float   shiftW      = 0;
    protected float   shiftWOld   = 0;
    protected float[] wMinMax     = {-1, 1};
    protected float[] wMinMaxOld  = {-1, 1};
    protected float[] wRange      = {-1, 1};
    protected float[] wRangeOld   = {-1, 1};

    protected float[] rotations   = {0, 0, 0};
    protected float   radius      = .5f;
    protected float   glyphScale  = 1.f;
    protected float   glyphDim    = 1.f;
    protected boolean show        = true;
    protected int adjusting = -1;

    protected boolean essentialChange = false;
    protected boolean autoUpdateBox   = true;
    protected boolean fireWhenAdjusting = false;

    protected float volume = 1;

    public InteractiveGlyphParams()
    {
    }

    public InteractiveGlyphParams(InteractiveGlyph parent)
    {
        this.glyph = parent;
    }

    public void checkEssentialChange()
    {
        if (shiftUOld != shiftU || shiftVOld != shiftV|| shiftWOld != shiftW)
            essentialChange = true;
        shiftUOld      = shiftU;
        shiftVOld      = shiftV;
        shiftWOld      = shiftW;

        for (int i = 0; i < 3; i++)  {
            if (centerOld[i] != center[i] || uOld[i] != u[i] || vOld[i] != v[i] || wOld[i] != w[i])
                essentialChange = true;
            centerOld[i] = center[i];
            uOld[i] = u[i];
            vOld[i] = v[i];
            wOld[i] = w[i];
        }
        for (int i = 0; i < 2; i++) {
            if (uMinMaxOld[i] != uMinMax[i] || vMinMaxOld[i] != vMinMax[i] || wMinMaxOld[i] != wMinMax[i])
                essentialChange = true;
            uMinMaxOld[i] = uMinMax[i];
            vMinMaxOld[i] = vMinMax[i];
            wMinMaxOld[i] = wMinMax[i];
        }
    }

    public void setGUI(InteractiveGlyphGUI gui) {
        this.gui = gui;
    }

    public void setGlyph(InteractiveGlyph glyph)
    {
        this.glyph = glyph;
    }

    public GlyphType getGlyphType()
    {
       return glyph.getGlyph().type;
    }

    public float[][] getCurrentColors() {
        return currentColors;
    }

    public void setCurrentColors(float[][] currentColors) {
        this.currentColors = currentColors;
        if (glyph != null)
            glyph.update();
    }

    public void darkColors(boolean dark)
    {
        setCurrentColors(dark ? DARK_COLORS : LIGHT_COLORS);
        if (gui != null)
            gui.updateSliderBorders(currentColors);
    }

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
        if (gui != null)
            gui.updateWidgetVisibility();
    }

    public void setCoords(FloatLargeArray coords)
    {
        this.coords = coords;
        if (coords == null)
            return;
        glyphDim = 0;
        for (int d = 0; d < 3; d++) {
            float rmin = Float.MAX_VALUE;
            float rmax = -Float.MAX_VALUE;
            for (long i = d; i < coords.length(); i += 3) {
                if (coords.get(i) < rmin)
                    rmin = coords.get(i);
                if (coords.get(i) > rmax)
                    rmax = coords.get(i);
            }
            center[d] = (rmax + rmin) / 2;
            glyphDim += (rmax - rmin) * (rmax - rmin);
        }
        glyphDim = (float)Math.sqrt(glyphDim);
        if (gui != null)
            gui.setTranslationRollersSensitivity(glyphDim / 1000);
        setAxis(0);
    }

    /**
     * adjusts glyph (reper position and orientation) according to GUI
     * manipulations
     *
     * @param adjusting tells which glyph positioning widget is currently used:
     * 0, 1, 2 denote rotation rollers, 3,4,5 - translation rollers
     */
    public void updateReper(int adjusting)
    {
        float[][] reper = {u, v, w};
        switch (adjusting) {
            case 0:
            case 1:
            case 2:
                float[][] baseReper = {baseU, baseV, baseW};
                float[] ux = reper[(adjusting + 1) % 3];
                float[] vx = reper[(adjusting + 2) % 3];
                float[] ub = baseReper[(adjusting + 1) % 3];
                float[] vb = baseReper[(adjusting + 2) % 3];
                double angle = rotations[adjusting] * Math.PI / 180;
                float c0 = (float) Math.cos(angle);
                float s0 = (float) Math.sin(angle);
                for (int i = 0; i < 3; i++) {
                    ux[i] = c0 * ub[i] - s0 * vb[i];
                    vx[i] = s0 * ub[i] + c0 * vb[i];
                }
                break;
            case 3:
            case 4:
            case 5:
                int ax = adjusting - 3;
                float[] shift = {shiftU, shiftV, shiftW};
                if (dimension == 2)
                    shiftW = 0; // plane glyphs can be translated only in xy plane
                if (shift[ax] != 0)
                    for (int i = 0; i < 3; i++)
                        center[i] = baseCenter[i] + shift[ax] * reper[ax][i];
                break;
            default:
                storeReper();
                checkEssentialChange();
        }
        updateAxesExtents();
        glyph.update();
    }

    public void setReper(float[] center, float[] u, float[] v, float[] w)
    {
//        boolean reperChanged = false;
//        for (int i = 0; i < 3; i++) {
//            if (center[i] != this.center[i]) {
//                reperChanged = true;
//                break;
//            }
//            if (u[i] != this.u[i]) {
//                reperChanged = true;
//                break;
//            }
//            if (v[i] != this.v[i]) {
//                reperChanged = true;
//                break;
//            }
//            if (w[i] != this.w[i]) {
//                reperChanged = true;
//                break;
//            }
//        }
//        if (!reperChanged)
//            return;
        System.arraycopy(center, 0, this.center, 0, 3);
        System.arraycopy(u, 0, this.u, 0, 3);
        System.arraycopy(v, 0, this.v, 0, 3);
        System.arraycopy(w, 0, this.w, 0, 3);
        storeReper();
        updateAxesExtents();
        glyph.update();
        checkEssentialChange();
        fireParameterChanged("glyph");
    }

    public void storeReper()
    {
        System.arraycopy(center, 0, baseCenter, 0, 3);
        System.arraycopy(w, 0, baseW, 0, 3);
        System.arraycopy(u, 0, baseU, 0, 3);
        System.arraycopy(v, 0, baseV, 0, 3);
    }

    public void updateAxesExtents()
    {
        if (coords == null || !autoUpdateBox)
            return;
        updateMinMax();
        if (gui != null)
            SwingInstancer.swingRunAndWait(new Runnable()
            {
                @Override
                public void run()
                {
                    gui.updateSliderBounds(true);
                }
            });
        glyph.update();
    }



    private float computeVolume()
    {
        volume = 0;
        float[][] range  = {uRange,  vRange, wRange};
        switch (glyph.getGlyph().getType()) {
            case BOX:
                volume = 1;
                for (float[] range1 : range)
                    volume *= (range1[1] - range1[0]);
                break;
            case RECTANGLE:
                volume = (range[1][1] - range[1][0]) * (range[0][1] - range[0][0]);
                break;
            case SPHERE:
                volume = (float)(4 * Math.PI * radius * radius * radius / 3);
                break;
            case CIRCLE:
                volume = (float)(Math.PI * radius * radius);
                break;
        }
        if (gui != null)
            gui.displayVolume(volume);
        return volume;
    }

    public void setAxis(int axis)
    {
        float[][] dir = {u, v, w};
        float[][] baseDir = {baseU, baseV, baseW};
        for (int i = 0; i < 3; i++) {
            Arrays.fill(dir[i], 0);
        }
        if (dimension == 3)
            for (int i = 0; i < 3; i++)
                dir[i][(i + axis) % 3] = baseDir[i][(i + axis) % 3] = 1;
        else
            for (int i = 0; i < 2; i++)
                dir[i][(i + axis) % 2] = baseDir[i][(i + axis) % 2] = 1;
        storeReper();
        checkEssentialChange();
        updateAxesExtents();
        glyph.update();
        fireParameterChanged("glyph");
    }

    private void updateMinMax()
    {
        long n = coords.length();
        float[][] dir = {u, v, w};
        float[][] minmax = {uMinMax, vMinMax, wMinMax};
        float[][] range  = {uRange,  vRange, wRange};
        for (int d = 0; d < 3; d++) {
            float rmin = Float.MAX_VALUE;
            float rmax = -Float.MAX_VALUE;
            for (long i = 0; i < n; i += 3) {
                float f = dir[d][0] * (coords.get(i)     - center[0]) +
                          dir[d][1] * (coords.get(i + 1) - center[1]) +
                          dir[d][2] * (coords.get(i + 2) - center[2]);
                if (f < rmin)
                    rmin = f;
                if (f > rmax)
                    rmax = f;
            }
            minmax[d][0] = rmin;
            minmax[d][1] = rmax;
        }
        for (int i = 0; i < range.length; i++)
            System.arraycopy(minmax[i], 0, range[i], 0, 2);
        computeVolume();
    }

    public void centerBox()
    {
        float[][] dir = {u, v, w};
        float[][] range  = {uRange,  vRange, wRange};
        float[][] minmax = {uMinMax, vMinMax, wMinMax};
        float r, s;
        for (int d = 0; d < 3; d++)  {
            if (autoUpdateBox) {
                s = (minmax[d][0] + minmax[d][1]) / 2;
                r = (minmax[d][1] - minmax[d][0]) / 2;
            }
            else {
                s = (range[d][0] + range[d][1]) / 2;
                r = (range[d][1] - range[d][0]) / 2;
            }
            for (int i = 0; i < 3; i++)
                center[i] += s * dir[d][i];
            range[d][0] = -r;
            range[d][1] =  r;
            minmax[d][0] -= s;
            minmax[d][1] -= s;
        }
        setReper(center, u, v, w);
        glyph.update();
    }

    public int getAdjusting() {
        return adjusting;
    }

    public void setAdjusting(int adjusting) {
        this.adjusting = adjusting;
    }

    public float getScale()
    {
        return glyphDim * glyphScale;
    }

    public void setScale(float scale)
    {
        glyphScale = scale;
        glyph.update();
    }

    public float[] getCenter()
    {
        return center;
    }

    public float[] getU()
    {
        return u;
    }

    public float[] getV()
    {
        return v;
    }

    public float[] getW()
    {
        return w;
    }

    public float[] getuRange()
    {
        return uRange;
    }

    public float[] getvRange()
    {
        return vRange;
    }

    public float[] getwRange()
    {
        return wRange;
    }

    public float[][] getMinMax()
    {
        return new float[][] {uMinMax, vMinMax, wMinMax};
    }

    public void setMinMax(float[][] minMax)
    {
        if (minMax[0] != null && minMax[0].length == 2) {
            uMinMax = minMax[0];
            if (uRange[0] < uMinMax[0])
                uRange[0] = uMinMax[0];
            if (uRange[1] > uMinMax[1])
                uRange[1] = uMinMax[1];
        }
        if (minMax[1] != null && minMax[1].length == 2) {
            vMinMax = minMax[1];
            if (vRange[0] < vMinMax[0])
                vRange[0] = vMinMax[0];
            if (vRange[1] > vMinMax[1])
                vRange[1] = vMinMax[1];
        }
        if (minMax[2] != null && minMax[2].length == 2) {
            wMinMax = minMax[2];
            if (wRange[0] < wMinMax[0])
                wRange[0] = wMinMax[0];
            if (wRange[1] > wMinMax[1])
                wRange[1] = wMinMax[1];
        }
        if (gui != null)
            SwingInstancer.swingRunAndWait(new Runnable()
            {
                @Override
                public void run()
                {
                    gui.updateSliderBounds(true);
                }
            });
        glyph.update();
    }

    public float getRadius()
    {
        return radius;
    }

    public void setRadius(float radius)
    {
        this.radius = radius;
        computeVolume();
        glyph.update();
        fireParameterChanged("glyph");
    }

    public boolean isShow()
    {
        return show;
    }

    public void setShow(boolean show)
    {
        this.show = show;
        glyph.update();
    }

    public void setCenter(float[] center)
    {
        System.arraycopy(center, 0, this.center, 0, center.length);
        updateReper(-1);
        fireParameterChanged("glyph");
    }
    public void moveCenter(float uShift, float vShift)
    {
        for (int i = 0; i < center.length; i++)
            center[i] += uShift * u[i] + vShift * v[i];
        if (autoUpdateBox)
            updateReper(-1);
        else {
            for (int i = 0; i < 2; i++) {
                uRange[i] -= uShift;
                uMinMax[i] -= uShift;
                vRange[i] -= uShift;
                vMinMax[i] -= uShift;
            }
        }
        if (gui != null)
            SwingInstancer.swingRunAndWait(new Runnable()
            {
                @Override
                public void run()
                {
                    gui.updateSliderBounds(true);
                }
            });
        glyph.update();
        fireParameterChanged("glyph");
    }

    public float getShiftU()
    {
        return shiftU;
    }

    public float getShiftV()
    {
        return shiftV;
    }

    public float getShiftW()
    {
        return shiftW;
    }

    public void setShiftU(float shiftU)
    {
        this.shiftU = shiftU;
        updateReper(adjusting);
        if (adjusting < 0)
            fireParameterChanged("glyph");
    }

    public void setShiftV(float shiftV)
    {
        this.shiftV = shiftV;
        updateReper(adjusting);
        if (adjusting < 0)
            fireParameterChanged("glyph");
    }

    public void setShiftW(float shiftW)
    {
        this.shiftW = shiftW;
        updateReper(adjusting);
        if (adjusting < 0)
            fireParameterChanged("glyph");
    }

    public float getVolume()
    {
        return volume;
    }

    public void setRotation(float[] rotations)
    {

        this.rotations = rotations;
        updateReper(adjusting);
        if (adjusting < 0) {
            updateAxesExtents();
            if (gui != null)
                gui.clearRotationRollers();
            fireParameterChanged("glyph");
        }
    }

    public void setURange(float[] uRange)
    {
        System.arraycopy(uRange, 0, this.uRange, 0, 2);
        computeVolume();
        essentialChange = true;
        if (adjusting < 0)
            fireParameterChanged("glyph");
    }

    public void setVRange(float[] vRange)
    {
        System.arraycopy(vRange, 0, this.vRange, 0, 2);
        computeVolume();
        essentialChange = true;
        if (adjusting < 0)
            fireParameterChanged("glyph");
    }

    public void setWRange(float[] wRange)
    {
        System.arraycopy(wRange, 0, this.wRange, 0, 2);
        computeVolume();
        essentialChange = true;
        if (adjusting < 0)
            fireParameterChanged("glyph");
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public int getVisibleWidgets()
    {
        return visibleWidgets;
    }

    public void setVisibleWidgets(int visibleWidgets)
    {
        this.visibleWidgets = visibleWidgets;
    }

    public String getScaleSliderTitle()
    {
        return scaleSliderTitle;
    }

    public void setScaleSliderTitle(String scaleSliderTitle)
    {
        this.scaleSliderTitle = scaleSliderTitle;
    }

    public String getRadiusSliderTitle()
    {
        return radiusSliderTitle;
    }

    public void setRadiusSliderTitle(String radiusSliderTitle)
    {
        this.radiusSliderTitle = radiusSliderTitle;
    }

    public boolean isAutoUpdateBox()
    {
        return autoUpdateBox;
    }

    public void setAutoUpdateBox(boolean autoUpdateBox)
    {
        this.autoUpdateBox = autoUpdateBox;
        if (autoUpdateBox)
            updateAxesExtents();
    }

    public void setFireWhenAdjusting(boolean fireWhenAdjusting)
    {
        this.fireWhenAdjusting = fireWhenAdjusting;
    }

    /**
     * Utility field holding list of listeners of particular parameters change
     * events
     */
    private ArrayList<ParameterChangeListener> listeners = new ArrayList<>();

    /**
     * Registers parameter change listener to receive events.
     *
     * @param listener The listener to register.
     */
    public synchronized void addParameterChangelistener(ParameterChangeListener listener)
    {
        listeners.add(listener);
    }

    /**
     * Removes parameter change listener from the list.
     *
     * @param listener The listener to remove.
     */
    public synchronized void removeParameterChangeListener(ParameterChangeListener listener)
    {
        listeners.remove(listener);
    }

    /**
     * Clears ChangeListenerList.
     */
    public synchronized void clearParameterChangeListeners()
    {
        listeners.clear();
    }

    public void fireParameterChanged(String parameter)
    {
        if (active && essentialChange && (adjusting == -1 || fireWhenAdjusting))
            for (ParameterChangeListener listener : listeners)
                listener.parameterChanged(parameter);
        essentialChange = false;
    }

    public void forceFireParameterChanged(String parameter)
    {
        for (ParameterChangeListener listener : listeners)
            listener.parameterChanged(parameter);
        essentialChange = false;
    }

    public GlyphGeometryParams getGeometryParams()
    {
        return new GlyphGeometryParams(center,
                                       u, shiftU, uMinMax, uRange,
                                       v, shiftV, vMinMax, vRange,
                                       w, shiftW, wMinMax, wRange,
                                       radius, glyphScale);
    }
}
