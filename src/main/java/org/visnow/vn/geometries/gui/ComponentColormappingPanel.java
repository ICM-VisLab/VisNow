/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.gui;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.visnow.vn.geometries.parameters.ComponentColorMap;
import org.visnow.vn.geometries.utils.ColorMapper;
import org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrange;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ComponentColormappingPanel extends javax.swing.JPanel
{

    private ComponentColorMap map = null;
    /**
     * Creates new form ComponentColormappingPanel
     */
    public ComponentColormappingPanel()
    {
        initComponents();
        colorEditor.setVisible(false);
        colorEditor.setEnabled(false);
    }

    public void setMap(ComponentColorMap map)
    {
        this.map = map;
        colormapChooser.setMap(map);
        dataRangeUI.setComponentValue(map.getComponentRange());
        ComponentSubrange s = map.getComponentRange();
        colormapChooser.setComponentRange(s);
        map.getComponentRange().addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                SwingInstancer.swingRunLater(() -> {
                    String s1 = ((ComponentSubrange)e.getSource()).getComponentName();
                    setColorMapEdit(s1 != null && !s1.equals("null"));
                });
            }
        });
        String name = map.getComponentRange().getComponentName();
        setColorMapEdit(name != null && !name.equals("null"));
    }

    public void setColorMapEdit(boolean edit)
    {
        colormapChooser.setVisible(edit);
        wrapBox.setVisible(edit);
        colorEditor.setVisible(!edit);
        colorEditor.setEnabled(!edit);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        dataRangeUI = new org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrangeUI();
        wrapBox = new javax.swing.JCheckBox();
        colorEditor = new org.visnow.vn.gui.widgets.ColorEditor();
        colormapChooser = new org.visnow.vn.geometries.gui.ColormapChooser();

        setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(dataRangeUI, gridBagConstraints);

        wrapBox.setText("wrap colormap");
        wrapBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                wrapBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        add(wrapBox, gridBagConstraints);

        colorEditor.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                colorEditorStateChanged(evt);
            }
        });

        javax.swing.GroupLayout colorEditorLayout = new javax.swing.GroupLayout(colorEditor);
        colorEditor.setLayout(colorEditorLayout);
        colorEditorLayout.setHorizontalGroup(
            colorEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
        colorEditorLayout.setVerticalGroup(
            colorEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 21, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        add(colorEditor, gridBagConstraints);

        colormapChooser.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        add(colormapChooser, gridBagConstraints);

        getAccessibleContext().setAccessibleName("");
    }// </editor-fold>//GEN-END:initComponents

    private void wrapBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_wrapBoxActionPerformed
    {//GEN-HEADEREND:event_wrapBoxActionPerformed
        map.setWrap(wrapBox.isSelected());
    }//GEN-LAST:event_wrapBoxActionPerformed

    private void colorEditorStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_colorEditorStateChanged
    {//GEN-HEADEREND:event_colorEditorStateChanged
        map.setDefaultColor(ColorMapper.convertColorToColor3f(colorEditor.getColor()));
    }//GEN-LAST:event_colorEditorStateChanged

    public void updateComponent()
    {
        if (map == null)
            return;
        map.setActive(ComponentColorMap.Active.SLEEP);
        int selectedComponent = map.getDataComponentIndex();
        if (selectedComponent >= 0) {
            colorEditor.setVisible(false);
            colorEditor.setEnabled(false);
            colormapChooser.setVisible(true);
            colormapChooser.setComponentRange(map.getComponentRange());
            wrapBox.setEnabled(true);
        } else {
            colorEditor.setVisible(true);
            colorEditor.setEnabled(true);
            colormapChooser.setVisible(false);
            wrapBox.setEnabled(false);
        }
        map.setActive(ComponentColorMap.Active.ONETIME);
        map.fireStateChanged(ComponentColorMap.Active.ONETIME, 0);
    }

    public void setColorMapIndex(int map)
    {
        colormapChooser.setSelectedIndex(map);
    }

    public void updateDataValuesFromParams()
    {
        map.setActive(ComponentColorMap.Active.SLEEP);
        colormapChooser.setSelectedIndex(map.getMapType());
        colormapChooser.setComponentRange(map.getComponentRange());
        wrapBox.setSelected(map.isWrap());
        dataRangeUI.updateSubrange();
        map.setActive(ComponentColorMap.Active.ONETIME);
    }

    public boolean isAdjusting()
    {
        return dataRangeUI.isAdjusting() || colormapChooser.isAdjusting();
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
        dataRangeUI.setEnabled(enabled);
        wrapBox.setEnabled(enabled);
        colormapChooser.setEnabled(enabled);
        repaint();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.visnow.vn.gui.widgets.ColorEditor colorEditor;
    private org.visnow.vn.geometries.gui.ColormapChooser colormapChooser;
    private org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrangeUI dataRangeUI;
    private javax.swing.JCheckBox wrapBox;
    // End of variables declaration//GEN-END:variables

    public void updateGUI()
    {
    }
}
