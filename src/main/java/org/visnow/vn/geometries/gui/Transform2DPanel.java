/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.gui;

import org.jogamp.java3d.Transform3D;
import org.jogamp.vecmath.Matrix3f;
import org.jogamp.vecmath.Vector3f;
import org.visnow.vn.geometries.objects.SignalingTransform3D;
import org.visnow.vn.geometries.parameters.TransformParams;
import org.visnow.vn.lib.utils.SwingInstancer;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Transform2DPanel extends javax.swing.JPanel
{

    private TransformParams transformParams = new TransformParams();
    private SignalingTransform3D sigTrans = null;
    private Transform3D trans = null;
    private Matrix3f rotMatrix = new Matrix3f(1, 0, 0, 0, 1, 0, 0, 0, 1);
    private Vector3f transVector = new Vector3f();
    private float scale = 1.f;
    private float xScale = 1.f;
    private float yScale = 1.f;
    private float zScale = 1.f;
    private boolean active = true;

    /**
     * Creates new form TransformPanel
     */
    public Transform2DPanel()
    {
        initComponents();
        exportTransformButton.setVisible(false); //awaiting further development (norkap)
    }

    public void updateGUI(float rotation, float[] transValues, float[] scaleValues, float scale)
    {
        active = false;
        rotationRoller.setValue(rotation);
        xTransRoller.setValue(transValues[0]);
        yTransRoller.setValue(transValues[1]);
        scaleRoller.setValue(scale);
        xScaleRoller.setValue(scaleValues[0]);
        yScaleRoller.setValue(scaleValues[1]);
        active = true;
        makeRotMatrix();
    }
    
    public double[] getTransformMatrix() {
        double[] matrix = new double[]{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1};
        if(trans != null) {
            trans.get(matrix);
        }
        return matrix;
    }
    
    public float getRotation() {
        return rotationRoller.getValue();
    }

    public float[] getTranslationValues() {
        return new float[] {xTransRoller.getValue(), yTransRoller.getValue()};
    }

    public float[] getScaleValues() {
        return new float[] {xScaleRoller.getValue(), yScaleRoller.getValue()};
    }
    
    public float getScale() {
        return scaleRoller.getValue();
    }
    
    public boolean isAdjusting() {
        return rotationRoller.isAdjusting() || scaleRoller.isAdjusting() || xScaleRoller.isAdjusting() || yScaleRoller.isAdjusting();
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        transformResetButton = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        scaleRoller = new org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedUnboundedRoller();
        rotationRoller = new org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedUnboundedRoller();
        xTransRoller = new org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedUnboundedRoller();
        yTransRoller = new org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedUnboundedRoller();
        exportTransformButton = new javax.swing.JButton();
        xScaleRoller = new org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedUnboundedRoller();
        yScaleRoller = new org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedUnboundedRoller();

        setPreferredSize(new java.awt.Dimension(206, 285));
        setLayout(new java.awt.GridBagLayout());

        transformResetButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        transformResetButton.setText("reset/inherit");
        transformResetButton.setMargin(new java.awt.Insets(2, 1, 2, 1));
        transformResetButton.setName("transformResetButton"); // NOI18N
        transformResetButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                transformResetButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 4, 0);
        add(transformResetButton, gridBagConstraints);

        jPanel3.setName("jPanel3"); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 206, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 8, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        add(jPanel3, gridBagConstraints);

        scaleRoller.setLabel("scale");
        scaleRoller.setName("scaleRoller"); // NOI18N
        scaleRoller.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                scaleRollerStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(6, 0, 3, 0);
        add(scaleRoller, gridBagConstraints);

        rotationRoller.setLabel("rotate");
        rotationRoller.setName("rotationRoller"); // NOI18N
        rotationRoller.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                rotationRollerStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(rotationRoller, gridBagConstraints);

        xTransRoller.setLabel("translate x");
        xTransRoller.setName("xTransRoller"); // NOI18N
        xTransRoller.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                xTransRollerStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        add(xTransRoller, gridBagConstraints);

        yTransRoller.setLabel("translate y");
        yTransRoller.setName("yTransRoller"); // NOI18N
        yTransRoller.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                yTransRollerStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(yTransRoller, gridBagConstraints);

        exportTransformButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        exportTransformButton.setText("export transformation");
        exportTransformButton.setMargin(new java.awt.Insets(2, 1, 2, 1));
        exportTransformButton.setMinimumSize(new java.awt.Dimension(100, 25));
        exportTransformButton.setName("exportTransformButton"); // NOI18N
        exportTransformButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                exportTransformButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(6, 0, 0, 0);
        add(exportTransformButton, gridBagConstraints);

        xScaleRoller.setLabel("scale x");
        xScaleRoller.setName("xScaleRoller"); // NOI18N
        xScaleRoller.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                xScaleRollerStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(6, 0, 0, 0);
        add(xScaleRoller, gridBagConstraints);

        yScaleRoller.setLabel("scale y");
        yScaleRoller.setName("yScaleRoller"); // NOI18N
        yScaleRoller.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                yScaleRollerStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(yScaleRoller, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void reset()
    {
        active = false;
        //       System.out.println("resetting transform");
        rotationRoller.reset();
        yTransRoller.reset();
        xTransRoller.reset();
        scaleRoller.reset();
        xScaleRoller.reset();
        yScaleRoller.reset();
        rotMatrix = new Matrix3f(1, 0, 0, 0, 1, 0, 0, 0, 1);
        transVector = new Vector3f();
        scale = xScale = yScale = zScale = 1.f;
        transformParams.resetTransform();
        active = true;
    }

    private void transformResetButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_transformResetButtonActionPerformed
    {//GEN-HEADEREND:event_transformResetButtonActionPerformed
        reset();
    }//GEN-LAST:event_transformResetButtonActionPerformed

    private void scaleRollerStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_scaleRollerStateChanged
    {//GEN-HEADEREND:event_scaleRollerStateChanged
        scale = (float) pow(1.01, scaleRoller.getValue());
        makeRotMatrix();
    }//GEN-LAST:event_scaleRollerStateChanged

    private void rotationRollerStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_rotationRollerStateChanged
    {//GEN-HEADEREND:event_rotationRollerStateChanged
        makeRotMatrix();
    }//GEN-LAST:event_rotationRollerStateChanged

    private void xTransRollerStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_xTransRollerStateChanged
    {//GEN-HEADEREND:event_xTransRollerStateChanged
        transVector.x = xTransRoller.getValue();
        trans = new Transform3D(rotMatrix, transVector, scale);
        if (!active)
            return;
        transformParams.setAdjusting(xTransRoller.isAdjusting());
        double[] matrix = new double[16];
        trans.get(matrix);
        transformParams.setTransform(matrix);
        if (sigTrans != null)
            sigTrans.setTransform(trans);
    }//GEN-LAST:event_xTransRollerStateChanged

    private void yTransRollerStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_yTransRollerStateChanged
    {//GEN-HEADEREND:event_yTransRollerStateChanged
        transVector.y = yTransRoller.getValue();
        trans = new Transform3D(rotMatrix, transVector, scale);
        if (!active)
            return;
        transformParams.setAdjusting(yTransRoller.isAdjusting());
        double[] matrix = new double[16];
        trans.get(matrix);
        transformParams.setTransform(matrix);
        if (sigTrans != null)
            sigTrans.setTransform(trans);
    }//GEN-LAST:event_yTransRollerStateChanged

    private void exportTransformButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_exportTransformButtonActionPerformed
    {//GEN-HEADEREND:event_exportTransformButtonActionPerformed
        sigTrans.fireTransformChanged();
    }//GEN-LAST:event_exportTransformButtonActionPerformed

    private void xScaleRollerStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_xScaleRollerStateChanged
    {//GEN-HEADEREND:event_xScaleRollerStateChanged
        xScale = (float) pow(1.01, xScaleRoller.getValue());
        makeRotMatrix();
    }//GEN-LAST:event_xScaleRollerStateChanged

    private void yScaleRollerStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_yScaleRollerStateChanged
    {//GEN-HEADEREND:event_yScaleRollerStateChanged
        yScale = (float) pow(1.01, yScaleRoller.getValue());
        makeRotMatrix();
    }//GEN-LAST:event_yScaleRollerStateChanged

    private void makeRotMatrix()
    {
        transformParams.setAdjusting(rotationRoller.isAdjusting() ||
            scaleRoller.isAdjusting() || xScaleRoller.isAdjusting() || yScaleRoller.isAdjusting());
        rotMatrix = new Matrix3f(xScale, 0, 0,
                                 0, yScale, 0,
                                 0, 0, zScale);
        Matrix3f tmp = new Matrix3f();
        tmp.rotZ((float) (rotationRoller.getValue() * PI / 180));
        tmp.mul(rotMatrix);
        trans = new Transform3D(tmp, transVector, scale);
        if (!active)
            return;
        double[] matrix = new double[16];
        trans.get(matrix);
        transformParams.setTransform(matrix);
        if (sigTrans != null)
            sigTrans.setTransform(trans);
    }

    public void setTransformParams(TransformParams transformParams)
    {
        this.transformParams = transformParams;
    }

    public void setSigTrans(SignalingTransform3D sigTrans)
    {
        this.sigTrans = sigTrans;
    }

    public void resetTransform()
    {
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                reset();
            }
        });
    }

    public void setTransSensitivity(float s)
    {
        xTransRoller.setSensitivity(s);
        yTransRoller.setSensitivity(s);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JButton exportTransformButton;
    protected javax.swing.JPanel jPanel3;
    protected org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedUnboundedRoller rotationRoller;
    protected org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedUnboundedRoller scaleRoller;
    protected javax.swing.JButton transformResetButton;
    protected org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedUnboundedRoller xScaleRoller;
    protected org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedUnboundedRoller xTransRoller;
    protected org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedUnboundedRoller yScaleRoller;
    protected org.visnow.vn.gui.widgets.UnboundedRoller.ExtendedUnboundedRoller yTransRoller;
    // End of variables declaration//GEN-END:variables
}
