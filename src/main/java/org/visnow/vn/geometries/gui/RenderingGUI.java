/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import org.jogamp.java3d.LineAttributes;
import org.jogamp.java3d.PolygonAttributes;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.plaf.basic.BasicComboPopup;
import org.jogamp.vecmath.Color3f;
import org.apache.log4j.Logger;
import org.visnow.jscic.FieldType;
import org.visnow.vn.geometries.parameters.PointFieldRenderingParams;
import org.visnow.vn.geometries.parameters.RegularField3DParams;
import org.visnow.vn.geometries.parameters.RenderingParams;
import org.visnow.vn.gui.icons.IconsContainer;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class RenderingGUI extends javax.swing.JPanel
{
    private static final Logger LOGGER = Logger.getLogger(RenderingGUI.class);
    
    protected static ImageIcon[] modeIcons = null;

    protected RenderingParams renderingParams = new RenderingParams();
    protected PointFieldRenderingParams pointFieldRenderingParams = null;
    protected FieldType type;
    protected int nDims = 3;
    protected int cellDim = 2;
    protected boolean renderingRegularField = true;
    ComboBoxRenderer shadingBoxRenderer = new ComboBoxRenderer();

    class IconString
    {

        ImageIcon icon;
        String string;

        public IconString(ImageIcon icon, String string)
        {
            this.icon = icon;
            this.string = string;
        }

        public ImageIcon getIcon()
        {
            return icon;
        }

        public String getString()
        {
            return string;
        }

        @Override
        public String toString()
        {
            return string;
        }
    }

    protected IconString[] elements = {
        new IconString(new ImageIcon(IconsContainer.getGouraud()), "smooth"),
        new IconString(new ImageIcon(IconsContainer.getFlat()), "flat"),
        new IconString(new ImageIcon(IconsContainer.getUnshaded()), "unshaded"),
        new IconString(new ImageIcon(IconsContainer.getBackground()), "background")};

    class ComplexCellRenderer implements ListCellRenderer
    {

        protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index,
                                                      boolean isSelected, boolean cellHasFocus)
        {
            Icon theIcon = null;
            String theText = null;

            JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index,
                                                                                    isSelected, cellHasFocus);

            if (value instanceof IconString) {
                theIcon = ((IconString) value).getIcon();
                theText = ((IconString) value).getString();
            } else
                theText = "";
            if (theIcon != null)
                renderer.setIcon(theIcon);
            renderer.setText(theText);
            return renderer;
        }
    }
    
    class ComboBoxRenderer extends JLabel implements ListCellRenderer {
        ComboBoxRenderer() {
            setOpaque(true);
            setHorizontalAlignment(LEFT);
            setVerticalAlignment(CENTER);
        }

        /*
         * This method finds the image and text corresponding
         * to the selected value and returns the label, set up
         * to display the text and image.
         */
        @Override
        public Component getListCellRendererComponent(
                                           JList list,
                                           Object value,
                                           int index,
                                           boolean isSelected,
                                           boolean cellHasFocus) {
            //Get the selected index. (The index param isn't
            //always valid, so just use the value.)
            IconString val = (IconString)value;
            //Set the icon and text.  If icon was null, say so.
            setIcon(val.getIcon());
            setText(val.getString());
            return this;
        }
    }


    /**
     * Creates new form DisplayPropertiesGUI
     */
    public RenderingGUI()
    {
        initComponents();
        if (renderingRegularField) {
           lineCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "no lines", "lines" }));
           pointCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "no points", "points" }));
        }
        imageBox.setVisible(renderingRegularField);
        outlineBox.setVisible(!renderingRegularField);
        faceButton.setInit(new String[]{"<html>all<p>faces</html>", 
                                        "<html>front<p>faces</html>", 
                                        "<html>back<p>faces</html>"}, 
                           new Icon[] {new ImageIcon(IconsContainer.getAll()),
                                       new ImageIcon(IconsContainer.getFront()),
                                       new ImageIcon(IconsContainer.getBack())
                           });
        faceButton.setTexts(new String[]{"all faces", 
                                        "front faces", 
                                        "back faces"});
        specularColorEditor.setTitle("specular color");
        specularColorEditor.setBrightness(20);
        featSlider.setShowingFields(false);
        
        Object popup = surfaceModeCombo.getUI().getAccessibleChild(surfaceModeCombo, 0);
        if (popup instanceof BasicComboPopup) {
            BasicComboPopup modesPopup = (BasicComboPopup) popup;
            JList modesList = modesPopup.getList();
            modesPopup.removeAll();
            modesList.setCellRenderer(new ComplexCellRenderer());
            modesList.setSize(200, 192);
            modesList.setMinimumSize(new Dimension(180, 192));
            modesList.setPreferredSize(new Dimension(180, 192));
            modesList.setMaximumSize(new Dimension(180, 192));
            modesList.setFixedCellHeight(48);
            modesPopup.setSize(200, 200);
            modesPopup.setMinimumSize(new Dimension(184, 200));
            modesPopup.setPreferredSize(new Dimension(184, 200));
            modesPopup.setMaximumSize(new Dimension(184, 200));
            modesPopup.add(modesList, BorderLayout.CENTER);
        }
        shadingBoxRenderer.setPreferredSize(new Dimension(140, 48));
        surfaceModeCombo.setRenderer(shadingBoxRenderer);
    }

    public void updateDataValuesFromParams()
    {
        if (renderingParams == null) 
            return;
        boolean ssis = renderingParams.isSilentInheritingStatus();
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                renderingParams.setSilentInheritingStatus(true);
                int dm = renderingParams.getDisplayMode();
                boxBox.setSelected((dm & RenderingParams.OUTLINE_BOX) != 0);
                imageBox.setSelected((dm & RenderingParams.IMAGE) != 0);
                renderingRegularField = renderingParams.isRenderingRegularField();
                if (renderingRegularField) {
                   lineCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "no lines", "lines" }));
                   pointCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "no points", "points" }));
                }
                else {
                   lineCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "no edges", "line cells", "all edges" }));
                   pointCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "no nodes", "point cells", "all nodes" }));
                }
                if (renderingRegularField) {
                    if ((dm & (RenderingParams.NODES)) != 0) 
                        pointCombo.setSelectedIndex(1);
                    else 
                        pointCombo.setSelectedIndex(0);
                        
                    if ((dm & (RenderingParams.EDGES)) != 0) 
                        lineCombo.setSelectedIndex(1);
                    else 
                        lineCombo.setSelectedIndex(0);
                }
                else {
                    if ((dm & RenderingParams.NODES) != 0)
                        pointCombo.setSelectedIndex(2);
                    else if ((dm & RenderingParams.POINT_CELLS) != 0)
                        pointCombo.setSelectedIndex(1);
                    else
                        pointCombo.setSelectedIndex(0);
                    if ((dm & RenderingParams.EDGES) != 0)
                        lineCombo.setSelectedIndex(2);
                    else if ((dm & RenderingParams.SEGMENT_CELLS) != 0)
                        lineCombo.setSelectedIndex(1);
                    else
                        lineCombo.setSelectedIndex(0);

                }
                surfaceBox.setSelected((dm & RenderingParams.SURFACE) != 0);
                for (int i = 0; i < SHADING.length; i++) 
                    if (SHADING[i] == renderingParams.getShadingMode()) {
                        surfaceModeCombo.setSelectedIndex(i);
                        break;
                    }
                lightBackBox.setSelected(renderingParams.isLightedBackside());
                flipToggle.setSelected(renderingParams.getSurfaceOrientation() == 0);
                faceButton.setState(renderingParams.getCullMode());
                toFrontBox.setSelected(renderingParams.getSurfaceOffset() < 0);
                specularColorEditor.setColor(renderingParams.getColor());
                transparencySlider.setValue((int)(100 * renderingParams.getTransparency()));
                shininessSlider.setValue((int)(3000 / renderingParams.getShininess()) - 1);
                lineWidthSlider.setValue(renderingParams.getLineThickness());
                featSlider.setValue(renderingParams.getMinEdgeDihedral() + .1);
                surfaceBox.setSelected((dm & RenderingParams.SURFACE) == RenderingParams.SURFACE);
                updateActiveControls();
            }
        });
        renderingParams.setSilentInheritingStatus(ssis);
    }

    /**
     * Enables/disables controls according to checkboxes state (surface, image, lines, points, box)
     */
    private void updateActiveControls()
    {
        if (renderingParams == null)
            return;
        boolean lines =     (renderingParams.getDisplayMode() & 
                            (RenderingParams.EDGES | RenderingParams.SEGMENT_CELLS |
                             RenderingParams.NODES | RenderingParams.POINT_CELLS |
                             RenderingParams.OUTLINE_BOX)) != 0;
        boolean linesFull = (renderingParams.getDisplayMode() & 
                             RenderingParams.EDGES) != 0;
        boolean surfaces = (surfaceBox.isEnabled() && surfaceBox.isSelected() && !outlineBox.isSelected());
        boolean image = (imageBox.isEnabled() && imageBox.isVisible()&& imageBox.isSelected());
        surfaceBox.setEnabled(!outlineBox.isSelected());
        pointCombo.setEnabled(!outlineBox.isSelected());
        lineCombo.setEnabled(!outlineBox.isSelected());
        surfacePanel.setEnabled(surfaces);
        specularColorEditor.setEnabled(surfaces);
        shininessSlider.setEnabled(surfaces);
        transparencySlider.setEnabled(surfaces || image);
        faceButton.setEnabled(surfaces);
        flipToggle.setEnabled(surfaces);
        lightBackBox.setEnabled(surfaces);
        surfaceModeCombo.setEnabled(surfaces);
        toFrontBox.setEnabled(surfaces);
        jPanel5.setEnabled(surfaces);

        edgesPanel.setEnabled(lines);
        jLabel4.setEnabled(lines);
        lineStyleCombo.setEnabled(lines);
        lineWidthSlider.setEnabled(lines);

        featSlider.setEnabled(linesFull);
    }

    public void setNDims(int nDims)
    {
        this.nDims = nDims;
    }

    public void setCellDim(int cellDim)
    {
        this.cellDim = cellDim;
        surfaceBox.setEnabled(cellDim > 1);
    }

    public void setRegularField3D(boolean rf3d)
    {
        regularField3DMapPanel.setVisible(rf3d);
        displayContentPanel.setVisible(!rf3d);
        surfacePanel.setVisible(!rf3d);
        edgesPanel.setVisible(!rf3d);
        maskPanel.setVisible(!rf3d && type != FieldType.FIELD_POINT);
    }
    
    public void setFieldType(FieldType type, int dim)
    {
        this.type = type;
        renderingRegularField = type == FieldType.FIELD_REGULAR;
        if (renderingRegularField) {
           lineCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "no lines", "lines" }));
           pointCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "no points", "points" }));
        }
        else {
           lineCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "no edges", "line cells", "all edges" }));
           pointCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "no nodes", "point cells", "all nodes" }));
        }
        imageBox.setVisible(renderingRegularField);
        outlineBox.setVisible(!renderingRegularField);
        setCellDim(dim);
        generalRenderingPanel.setVisible(type == FieldType.FIELD_IRREGULAR || type == FieldType.FIELD_REGULAR && dim < 3);
        surfacePanel.setVisible(type == FieldType.FIELD_IRREGULAR || type == FieldType.FIELD_REGULAR && dim == 2);
        edgesPanel.setVisible(type == FieldType.FIELD_IRREGULAR);
        pointFieldPanel.setVisible(type == FieldType.FIELD_POINT);
        maskPanel.setVisible(type != FieldType.FIELD_POINT);
        setRegularField3D(type == FieldType.FIELD_REGULAR && dim ==3);
    }
    
    public void setRenderingParams(RenderingParams renderingParams)
    {
        this.renderingParams = renderingParams;
        updateDataValuesFromParams();
    }
    
    public void setPointFieldRenderingParams(PointFieldRenderingParams renderingParams)
    {
        this.pointFieldRenderingParams = renderingParams;
        pointFieldPanel.setParams(renderingParams);
    }
    
    public void setContent3DParams(RegularField3DParams params)
    {
        regularField3DMapPanel.setParams(params);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        cullFaceGroup = new javax.swing.ButtonGroup();
        shadingGroup = new javax.swing.ButtonGroup();
        helpFrame = new javax.swing.JFrame();
        helpText = new javax.swing.JLabel();
        buttonGroup1 = new javax.swing.ButtonGroup();
        regularField3DMapPanel = new org.visnow.vn.geometries.gui.RegularField3DMapPanel();
        uIStyleFakeInitializer2 = new org.visnow.vn.system.swing.UIStyleFakeInitializer();
        maskPanel = new javax.swing.JPanel();
        omitMaskedNodesBox = new javax.swing.JRadioButton();
        transparentMaskedNodesBox = new javax.swing.JRadioButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        pointFieldPanel = new org.visnow.vn.geometries.gui.PointFieldRenderingGUI();
        generalRenderingPanel = new javax.swing.JPanel();
        displayContentPanel = new javax.swing.JPanel();
        surfaceBox = new javax.swing.JCheckBox();
        imageBox = new javax.swing.JCheckBox();
        outlineBox = new javax.swing.JCheckBox();
        lineCombo = new javax.swing.JComboBox();
        boxBox = new javax.swing.JCheckBox();
        pointCombo = new javax.swing.JComboBox();
        resetButton = new javax.swing.JButton();
        surfacePanel = new javax.swing.JPanel();
        transparencySlider = new javax.swing.JSlider();
        shininessSlider = new javax.swing.JSlider();
        jPanel5 = new javax.swing.JPanel();
        surfaceModeCombo = new javax.swing.JComboBox();
        flipToggle = new javax.swing.JToggleButton();
        faceButton = new org.visnow.vn.gui.widgets.MultistateButton();
        toFrontBox = new javax.swing.JCheckBox();
        lightBackBox = new javax.swing.JCheckBox();
        specularColorEditor = new org.visnow.vn.gui.widgets.ColorEditor();
        edgesPanel = new javax.swing.JPanel();
        lineStyleCombo = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        featSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();
        lineWidthSlider = new org.visnow.vn.gui.widgets.ExtendedSlider();

        helpFrame.setUndecorated(true);

        helpText.setText("<html>This slider controls relative visibility of the object surface part<p>\nwith respect to other geometries<p>\nIf there are lines on the surface, increase slider value (drag right)<p>\nDrag left to see the surface through other object (\"object in the box\" effect)</html>"); // NOI18N
        helpFrame.getContentPane().add(helpText, java.awt.BorderLayout.CENTER);

        setLayout(new java.awt.GridBagLayout());

        regularField3DMapPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(regularField3DMapPanel, gridBagConstraints);
        add(uIStyleFakeInitializer2, new java.awt.GridBagConstraints());

        maskPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("masked out nodes"));
        maskPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 0));

        buttonGroup1.add(omitMaskedNodesBox);
        omitMaskedNodesBox.setSelected(true);
        omitMaskedNodesBox.setText("not rendered");
        omitMaskedNodesBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                omitMaskedNodesBoxActionPerformed(evt);
            }
        });
        maskPanel.add(omitMaskedNodesBox);

        buttonGroup1.add(transparentMaskedNodesBox);
        transparentMaskedNodesBox.setText("transparent");
        transparentMaskedNodesBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                transparentMaskedNodesBoxActionPerformed(evt);
            }
        });
        maskPanel.add(transparentMaskedNodesBox);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(maskPanel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.weighty = 1.0;
        add(filler1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(pointFieldPanel, gridBagConstraints);

        generalRenderingPanel.setLayout(new java.awt.GridBagLayout());

        displayContentPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("show"));
        displayContentPanel.setName(""); // NOI18N
        displayContentPanel.setLayout(new java.awt.GridBagLayout());

        surfaceBox.setSelected(true);
        surfaceBox.setText("surfaces"); // NOI18N
        surfaceBox.setToolTipText("show surfaces (if present) in 3D view");
        surfaceBox.setMaximumSize(new java.awt.Dimension(88, 16));
        surfaceBox.setMinimumSize(new java.awt.Dimension(80, 16));
        surfaceBox.setPreferredSize(new java.awt.Dimension(80, 16));
        surfaceBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                surfaceBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        displayContentPanel.add(surfaceBox, gridBagConstraints);

        imageBox.setText("image");
        imageBox.setToolTipText("show image (if present) in 2D view");
        imageBox.setMaximumSize(new java.awt.Dimension(63, 16));
        imageBox.setMinimumSize(new java.awt.Dimension(63, 16));
        imageBox.setPreferredSize(new java.awt.Dimension(63, 16));
        imageBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                imageBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.9;
        displayContentPanel.add(imageBox, gridBagConstraints);

        outlineBox.setText("outline");
        outlineBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                outlineBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        displayContentPanel.add(outlineBox, gridBagConstraints);

        lineCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "no edges", "line cells", "all edges" }));
        lineCombo.setSelectedIndex(1);
        lineCombo.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                lineComboItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        displayContentPanel.add(lineCombo, gridBagConstraints);

        boxBox.setText("box");
        boxBox.setToolTipText("show geometry outline (if geometry present)");
        boxBox.setMaximumSize(new java.awt.Dimension(47, 16));
        boxBox.setMinimumSize(new java.awt.Dimension(47, 16));
        boxBox.setPreferredSize(new java.awt.Dimension(47, 16));
        boxBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                boxBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        displayContentPanel.add(boxBox, gridBagConstraints);

        pointCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "no nodes", "point cells", "all nodes" }));
        pointCombo.setSelectedIndex(1);
        pointCombo.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                pointComboItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        displayContentPanel.add(pointCombo, gridBagConstraints);

        resetButton.setText("reset");
        resetButton.setMaximumSize(new java.awt.Dimension(67, 24));
        resetButton.setMinimumSize(new java.awt.Dimension(67, 24));
        resetButton.setOpaque(false);
        resetButton.setPreferredSize(new java.awt.Dimension(67, 24));
        resetButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                resetButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        displayContentPanel.add(resetButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        generalRenderingPanel.add(displayContentPanel, gridBagConstraints);

        surfacePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("surfaces"));
        surfacePanel.setLayout(new java.awt.GridBagLayout());

        transparencySlider.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        transparencySlider.setMajorTickSpacing(20);
        transparencySlider.setMinorTickSpacing(5);
        transparencySlider.setValue(0);
        transparencySlider.setBorder(javax.swing.BorderFactory.createTitledBorder("transparency"));
        transparencySlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                transparencySliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        surfacePanel.add(transparencySlider, gridBagConstraints);

        shininessSlider.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        shininessSlider.setMajorTickSpacing(20);
        shininessSlider.setMinorTickSpacing(5);
        shininessSlider.setValue(15);
        shininessSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("shininess"));
        shininessSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                shininessSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        surfacePanel.add(shininessSlider, gridBagConstraints);

        jPanel5.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPanel5.setLayout(new java.awt.GridBagLayout());

        surfaceModeCombo.setModel(new DefaultComboBoxModel(elements));
        surfaceModeCombo.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                surfaceModeComboItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 4, 0);
        jPanel5.add(surfaceModeCombo, gridBagConstraints);

        flipToggle.setText("flip sides");
        flipToggle.setMargin(new java.awt.Insets(2, 4, 2, 4));
        flipToggle.setMaximumSize(new java.awt.Dimension(89, 28));
        flipToggle.setMinimumSize(new java.awt.Dimension(89, 18));
        flipToggle.setName(""); // NOI18N
        flipToggle.setPreferredSize(new java.awt.Dimension(89, 28));
        flipToggle.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                flipToggleActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        jPanel5.add(flipToggle, gridBagConstraints);

        faceButton.setText("multistateButton1");
        faceButton.setMaximumSize(new java.awt.Dimension(140, 60));
        faceButton.setMinimumSize(new java.awt.Dimension(140, 60));
        faceButton.setName(""); // NOI18N
        faceButton.setPreferredSize(new java.awt.Dimension(140, 60));
        faceButton.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                faceButtonStateChanged(evt);
            }
        });
        faceButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                faceButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel5.add(faceButton, gridBagConstraints);

        toFrontBox.setText("pull to front ");
        toFrontBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                toFrontBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        jPanel5.add(toFrontBox, gridBagConstraints);

        lightBackBox.setText("<html>lighted<p> back</html>");
        lightBackBox.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lightBackBox.setMargin(new java.awt.Insets(0, 2, 2, 0));
        lightBackBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                lightBackBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel5.add(lightBackBox, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        surfacePanel.add(jPanel5, gridBagConstraints);

        specularColorEditor.setBasicColor(java.awt.Color.white);
        specularColorEditor.setBrightness(20);
        specularColorEditor.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                specularColorEditorStateChanged(evt);
            }
        });
        specularColorEditor.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 3, 3);
        surfacePanel.add(specularColorEditor, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 0);
        generalRenderingPanel.add(surfacePanel, gridBagConstraints);

        edgesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("points and lines"));
        edgesPanel.setRequestFocusEnabled(false);
        edgesPanel.setLayout(new java.awt.GridBagLayout());

        lineStyleCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "solid", "dashed", "dotted", "dashdot" }));
        lineStyleCombo.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                lineStyleComboItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 8, 0, 0);
        edgesPanel.add(lineStyleCombo, gridBagConstraints);

        jLabel4.setText("line style");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 0, 0);
        edgesPanel.add(jLabel4, gridBagConstraints);

        featSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.FLOAT);
        featSlider.setGlobalMax(1000
        );
        featSlider.setGlobalMin(.01);
        featSlider.setMax(100);
        featSlider.setMin(0.1);
        featSlider.setScaleType(org.visnow.vn.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        featSlider.setSubmitOnAdjusting(false);
        featSlider.setValue(0.1);
        featSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("feature angle"));
        featSlider.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                featSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        edgesPanel.add(featSlider, gridBagConstraints);

        lineWidthSlider.setFieldType(org.visnow.vn.gui.components.NumericTextField.FieldType.FLOAT);
        lineWidthSlider.setMax(15);
        lineWidthSlider.setMin(0.1);
        lineWidthSlider.setScaleType(org.visnow.vn.gui.widgets.ExtendedSlider.ScaleType.LOGARITHMIC);
        lineWidthSlider.setValue(0.5);
        lineWidthSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("line/point width"));
        lineWidthSlider.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                lineWidthSliderUserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        edgesPanel.add(lineWidthSlider, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        generalRenderingPanel.add(edgesPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(generalRenderingPanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void setGeometryMode()
    {
        int mode = 0;
        if (outlineBox.isSelected()) {
            mode |= RenderingParams.EDGES;
            renderingParams.setMinEdgeDihedral(10);
            renderingParams.setDisplayMode(mode);
            updateActiveControls();
            return;
        }
        if (surfaceBox.isSelected())
            mode |= RenderingParams.SURFACE;
        switch ((String)lineCombo.getSelectedItem()) {
            case "lines":
            case "all edges":
                mode |= RenderingParams.EDGES;
                break;
            case "line cells":
                mode |= RenderingParams.SEGMENT_CELLS;
                break;
        }
        switch ((String)pointCombo.getSelectedItem()) {
            case "points":
            case "all nodes":
                mode |= RenderingParams.NODES;
                break;
            case "point cells":
                mode |= RenderingParams.POINT_CELLS;
                break;
        }
        if (imageBox.isSelected())
            mode |= RenderingParams.IMAGE;
        if (boxBox.isSelected())
            mode |= RenderingParams.OUTLINE_BOX;
        renderingParams.setDisplayMode(mode);
        updateActiveControls();
    }

    private void lineStyleComboItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_lineStyleComboItemStateChanged
    {//GEN-HEADEREND:event_lineStyleComboItemStateChanged
        switch (lineStyleCombo.getSelectedIndex()) {
            case 0:
                renderingParams.setLineStyle(LineAttributes.PATTERN_SOLID);
                break;
            case 1:
                renderingParams.setLineStyle(LineAttributes.PATTERN_DASH);
                break;
            case 2:
                renderingParams.setLineStyle(LineAttributes.PATTERN_DOT);
                break;
            case 3:
                renderingParams.setLineStyle(LineAttributes.PATTERN_DASH_DOT);
                break;
        }
    }//GEN-LAST:event_lineStyleComboItemStateChanged

    private void transparencySliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_transparencySliderStateChanged
    {//GEN-HEADEREND:event_transparencySliderStateChanged
        float transp = .01f * transparencySlider.getValue();
        if (renderingParams.getTransparency() == transp)
            return;

        renderingParams.setTransparency(transp);
    }//GEN-LAST:event_transparencySliderStateChanged

    private void shininessSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_shininessSliderStateChanged
    {//GEN-HEADEREND:event_shininessSliderStateChanged
        renderingParams.setShininess(3000.f / (1 + shininessSlider.getValue()));
    }//GEN-LAST:event_shininessSliderStateChanged

    private void surfaceBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_surfaceBoxActionPerformed
    {//GEN-HEADEREND:event_surfaceBoxActionPerformed
        setGeometryMode();
        updateActiveControls();
    }//GEN-LAST:event_surfaceBoxActionPerformed

    private void flipToggleActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_flipToggleActionPerformed
    {//GEN-HEADEREND:event_flipToggleActionPerformed
        renderingParams.setSurfaceOrientation(flipToggle.isSelected() ? 0 : (byte)1);
    }//GEN-LAST:event_flipToggleActionPerformed

    private void specularColorEditorStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_specularColorEditorStateChanged
    {//GEN-HEADEREND:event_specularColorEditorStateChanged
        renderingParams.setSpecularColor(new Color3f(specularColorEditor.getColorComponents()));
    }//GEN-LAST:event_specularColorEditorStateChanged

    private void featSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_featSliderStateChanged
    {//GEN-HEADEREND:event_featSliderStateChanged
        renderingParams.setMinEdgeDihedral((float)(featSlider.getValue()));
    }//GEN-LAST:event_featSliderStateChanged

    private void lineWidthSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_lineWidthSliderStateChanged
    {//GEN-HEADEREND:event_lineWidthSliderStateChanged
        renderingParams.setLineThickness((float)(lineWidthSlider.getValue()));
    }//GEN-LAST:event_lineWidthSliderStateChanged

    private void toFrontBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_toFrontBoxActionPerformed
    {//GEN-HEADEREND:event_toFrontBoxActionPerformed
        renderingParams.setSurfaceOffset(toFrontBox.isSelected() ? -1000000 : VisNow.get().getMainConfig().getRenderingSurfaceOffset());
    }//GEN-LAST:event_toFrontBoxActionPerformed

    private void imageBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_imageBoxActionPerformed
        setGeometryMode();
        updateActiveControls();
    }//GEN-LAST:event_imageBoxActionPerformed

    private void boxBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_boxBoxActionPerformed
    {//GEN-HEADEREND:event_boxBoxActionPerformed
        setGeometryMode();
        updateActiveControls();
    }//GEN-LAST:event_boxBoxActionPerformed

    private static final int[] SHADING =  {RenderingParams.GOURAUD_SHADED, 
                                           RenderingParams.FLAT_SHADED,
                                           RenderingParams.UNSHADED,
                                           RenderingParams.BACKGROUND};

    private void surfaceModeComboItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_surfaceModeComboItemStateChanged
    {//GEN-HEADEREND:event_surfaceModeComboItemStateChanged
        renderingParams.setShadingMode(SHADING[surfaceModeCombo.getSelectedIndex()]);
    }//GEN-LAST:event_surfaceModeComboItemStateChanged
    
    private static final int[] CULL_MODE = {PolygonAttributes.CULL_NONE,
                                            PolygonAttributes.CULL_BACK,
                                            PolygonAttributes.CULL_FRONT
    };
    
    public void setShadingMode(int shading)
    {
        surfaceModeCombo.setSelectedIndex(shading);
    }

    public void setCullMode(int cullFace)
    {
        faceButton.setState(cullFace);
    }
    
    private void faceButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_faceButtonActionPerformed
    {//GEN-HEADEREND:event_faceButtonActionPerformed
        renderingParams.setCullMode(CULL_MODE[faceButton.getState()]);
    }//GEN-LAST:event_faceButtonActionPerformed

    private void faceButtonStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_faceButtonStateChanged
    {//GEN-HEADEREND:event_faceButtonStateChanged
        renderingParams.setCullMode(CULL_MODE[faceButton.getState()]);
    }//GEN-LAST:event_faceButtonStateChanged

    private void lightBackBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_lightBackBoxActionPerformed
    {//GEN-HEADEREND:event_lightBackBoxActionPerformed
        renderingParams.setLightedBackside(lightBackBox.isSelected());
    }//GEN-LAST:event_lightBackBoxActionPerformed

    private void omitMaskedNodesBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_omitMaskedNodesBoxActionPerformed
    {//GEN-HEADEREND:event_omitMaskedNodesBoxActionPerformed
        renderingParams.setTransparentlyRenderedMaskedNodes(transparentMaskedNodesBox.isSelected());
    }//GEN-LAST:event_omitMaskedNodesBoxActionPerformed

    private void transparentMaskedNodesBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_transparentMaskedNodesBoxActionPerformed
    {//GEN-HEADEREND:event_transparentMaskedNodesBoxActionPerformed
        renderingParams.setTransparentlyRenderedMaskedNodes(transparentMaskedNodesBox.isSelected());
    }//GEN-LAST:event_transparentMaskedNodesBoxActionPerformed

    private void lineWidthSliderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_lineWidthSliderUserChangeAction
    {//GEN-HEADEREND:event_lineWidthSliderUserChangeAction
        float lineThickness = (Float) lineWidthSlider.getValue();
        if (renderingParams.getLineThickness() == lineThickness)
            return;
        renderingParams.setLineThickness(lineThickness);
    }//GEN-LAST:event_lineWidthSliderUserChangeAction

    private void featSliderUserChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_featSliderUserChangeAction
    {//GEN-HEADEREND:event_featSliderUserChangeAction
        renderingParams.setMinEdgeDihedral((Float)featSlider.getValue() - .1f);
    }//GEN-LAST:event_featSliderUserChangeAction

    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_resetButtonActionPerformed
    {//GEN-HEADEREND:event_resetButtonActionPerformed
        surfaceBox.setSelected(true);
        lineCombo.setSelectedIndex(1);
        pointCombo.setSelectedIndex(1);
    }//GEN-LAST:event_resetButtonActionPerformed

    private void lineComboItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_lineComboItemStateChanged
    {//GEN-HEADEREND:event_lineComboItemStateChanged
        setGeometryMode();
    }//GEN-LAST:event_lineComboItemStateChanged

    private void pointComboItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_pointComboItemStateChanged
    {//GEN-HEADEREND:event_pointComboItemStateChanged
        setGeometryMode();
    }//GEN-LAST:event_pointComboItemStateChanged

    private void outlineBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_outlineBoxActionPerformed
        if (outlineBox.isSelected()) {
            featSlider.setValue(10);
            surfaceBox.setSelected(false);
            lineCombo.setSelectedIndex(2);
        }
        setGeometryMode();
        updateActiveControls();
    }//GEN-LAST:event_outlineBoxActionPerformed

    public void setOutlineMode(boolean mode) {
        outlineBoxActionPerformed(null);
        outlineBox.setSelected(mode);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JCheckBox boxBox;
    protected javax.swing.ButtonGroup buttonGroup1;
    protected javax.swing.ButtonGroup cullFaceGroup;
    protected javax.swing.JPanel displayContentPanel;
    protected javax.swing.JPanel edgesPanel;
    protected org.visnow.vn.gui.widgets.MultistateButton faceButton;
    protected org.visnow.vn.gui.widgets.ExtendedSlider featSlider;
    protected javax.swing.Box.Filler filler1;
    protected javax.swing.JToggleButton flipToggle;
    protected javax.swing.JPanel generalRenderingPanel;
    protected javax.swing.JFrame helpFrame;
    protected javax.swing.JLabel helpText;
    protected javax.swing.JCheckBox imageBox;
    protected javax.swing.JLabel jLabel4;
    protected javax.swing.JPanel jPanel5;
    protected javax.swing.JCheckBox lightBackBox;
    protected javax.swing.JComboBox lineCombo;
    protected javax.swing.JComboBox lineStyleCombo;
    protected org.visnow.vn.gui.widgets.ExtendedSlider lineWidthSlider;
    protected javax.swing.JPanel maskPanel;
    protected javax.swing.JRadioButton omitMaskedNodesBox;
    protected javax.swing.JCheckBox outlineBox;
    protected javax.swing.JComboBox pointCombo;
    protected org.visnow.vn.geometries.gui.PointFieldRenderingGUI pointFieldPanel;
    protected org.visnow.vn.geometries.gui.RegularField3DMapPanel regularField3DMapPanel;
    protected javax.swing.JButton resetButton;
    protected javax.swing.ButtonGroup shadingGroup;
    protected javax.swing.JSlider shininessSlider;
    protected org.visnow.vn.gui.widgets.ColorEditor specularColorEditor;
    protected javax.swing.JCheckBox surfaceBox;
    protected javax.swing.JComboBox surfaceModeCombo;
    protected javax.swing.JPanel surfacePanel;
    protected javax.swing.JCheckBox toFrontBox;
    protected javax.swing.JSlider transparencySlider;
    protected javax.swing.JRadioButton transparentMaskedNodesBox;
    protected org.visnow.vn.system.swing.UIStyleFakeInitializer uIStyleFakeInitializer2;
    // End of variables declaration//GEN-END:variables
    
}
