/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.geometries.gui.TransparencyEditor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicComboPopup;
import org.visnow.jscic.DataContainer;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.vn.geometries.events.ColorListener;
import org.visnow.vn.geometries.events.ColorMapChangeListener;
import org.visnow.vn.geometries.parameters.TransparencyParams;
import org.visnow.jscic.DataContainerSchema;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.vn.gui.icons.IconsContainer;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
//TODO opis osi poziomej w edytorze
//todo range slider obcinania 
//remap histogramu
public class TransparencyEditor extends javax.swing.JPanel implements Serializable
{

    class IconString
    {

        ImageIcon icon;
        String string;

        public IconString(ImageIcon icon, String string)
        {
            this.icon = icon;
            this.string = string;
        }

        public ImageIcon getIcon()
        {
            return icon;
        }

        public String getString()
        {
            return string;
        }

        @Override
        public String toString()
        {
            return string;
        }
    }

    final IconString[] mapTypeIcons = {
        new IconString(new ImageIcon(IconsContainer.getRamp()), "v"),
        new IconString(new ImageIcon(IconsContainer.getReverseRamp()), "-v"),
        new IconString(new ImageIcon(IconsContainer.getValley()), "|v|"),
        new IconString(new ImageIcon(IconsContainer.getPeak()), "-|v|"),
        new IconString(new ImageIcon(IconsContainer.getComb1()), "cos v"),
        new IconString(new ImageIcon(IconsContainer.getComb2()), "sin v"),
        new IconString(new ImageIcon(IconsContainer.getNot0()), "v!=0")
    };

    class ComplexCellRenderer implements ListCellRenderer
    {
        protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index,
                                                      boolean isSelected, boolean cellHasFocus)
        {
            Icon theIcon = null;
            String theText = "";
            JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index,
                                                                                    isSelected, cellHasFocus);

            if (value instanceof IconString) {
                theIcon = ((IconString) value).getIcon();
                theText = ((IconString) value).getString();
            } 
            if (theIcon != null)
                renderer.setIcon(theIcon);
            renderer.setText(theText);
            return renderer;
        }
    }
    
    private int mapType = 0;
    private static final int HISTO_LENGTH = 256;
    private static final float HISTO_DELTA = 1.f / HISTO_LENGTH;
    private float[] transp = new float[256];
    private float scale = 1.f;
    private int bgrThreshold = 0;
    private long[] histo;
    private float cmpMin = 0, cmpMax = 100;
    private TransparencyParams params = new TransparencyParams();
    private DataArraySchema daSchema = null;
    private DataContainer container = null;
    private boolean active = true;
    private boolean skipHistogramUpdate = true;

    /**
     * Creates new form PrefsDataGraph
     */
    public TransparencyEditor()
    {
        initComponents();
        Object popup = predefinedTransparenciesCombo.getUI().getAccessibleChild(predefinedTransparenciesCombo, 0);
        if (popup instanceof BasicComboPopup) {
            BasicComboPopup modesPopup = (BasicComboPopup) popup;
            JList modesList = modesPopup.getList();
            modesPopup.removeAll();
            modesList.setCellRenderer(new ComplexCellRenderer());
            modesList.setSize(200, 192);
            modesList.setMinimumSize(new Dimension(200, 188));
            modesList.setPreferredSize(new Dimension(200, 188));
            modesList.setMaximumSize(new Dimension(200, 188));
            modesList.setFixedCellHeight(27);
            modesPopup.setSize(200, 200);
            modesPopup.setMinimumSize(new Dimension(110, 190));
            modesPopup.setPreferredSize(new Dimension(110, 190));
            modesPopup.setMaximumSize(new Dimension(110, 190));
            modesPopup.add(modesList, BorderLayout.CENTER);
        }
        histogramGraphPanel.setTransp(transp);
        transparencyDisplayPanel.setTransp(transp);
        histogramGraphPanel.setChangeListener((ChangeEvent e) -> {
            params.setTransparencyManuallyEdited(true);
            params.setPrototypeMap(transp);
        });
        histogramGraphPanel.setImmediateChangeListener(transparencyDisplayPanel.getImmediateRepaintListener());
        nPeriodsSpinner.setVisible(mapType == 4 || mapType == 5);
        expSlider.setVisible(mapType < mapTypeIcons.length - 1);
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this
     * code. The content of this method is always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        componentSubrangeUI = new org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrangeUI();
        lookupTableEditorPanel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        scaleSlider = new javax.swing.JSlider();
        transparencyDisplayPanel = new org.visnow.vn.geometries.gui.TransparencyEditor.TransparencyDisplayPanel();
        histogramGraphPanel = new org.visnow.vn.geometries.gui.TransparencyEditor.HistogramGraphPanel();
        bgrSlider = new org.visnow.vn.gui.widgets.FloatSubRangeSlider.FloatSubRangeSlider();
        jPanel2 = new javax.swing.JPanel();
        expSlider = new javax.swing.JSlider();
        predefinedTransparenciesCombo = new javax.swing.JComboBox();
        nPeriodsSpinner = new javax.swing.JSpinner();

        setPreferredSize(new java.awt.Dimension(240, 420));
        setLayout(new java.awt.BorderLayout());

        componentSubrangeUI.setMinimumSize(new java.awt.Dimension(180, 129));
        componentSubrangeUI.setPreferredSize(new java.awt.Dimension(200, 129));
        add(componentSubrangeUI, java.awt.BorderLayout.NORTH);

        lookupTableEditorPanel.setPreferredSize(new java.awt.Dimension(307, 270));
        lookupTableEditorPanel.setLayout(new java.awt.GridBagLayout());

        jPanel1.setLayout(new java.awt.GridBagLayout());

        scaleSlider.setOrientation(javax.swing.JSlider.VERTICAL);
        scaleSlider.setToolTipText("<html>overall opacity slider<p>drag to increase/decrease global transparency</html>");
        scaleSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                scaleSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(scaleSlider, gridBagConstraints);

        transparencyDisplayPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                transparencyDisplayPanelMouseClicked(evt);
            }
        });

        org.jdesktop.layout.GroupLayout transparencyDisplayPanelLayout = new org.jdesktop.layout.GroupLayout(transparencyDisplayPanel);
        transparencyDisplayPanel.setLayout(transparencyDisplayPanelLayout);
        transparencyDisplayPanelLayout.setHorizontalGroup(
            transparencyDisplayPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 267, Short.MAX_VALUE)
        );
        transparencyDisplayPanelLayout.setVerticalGroup(
            transparencyDisplayPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 24, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 13, 0, 14);
        jPanel1.add(transparencyDisplayPanel, gridBagConstraints);

        histogramGraphPanel.setPreferredSize(new java.awt.Dimension(180, 150));

        org.jdesktop.layout.GroupLayout histogramGraphPanelLayout = new org.jdesktop.layout.GroupLayout(histogramGraphPanel);
        histogramGraphPanel.setLayout(histogramGraphPanelLayout);
        histogramGraphPanelLayout.setHorizontalGroup(
            histogramGraphPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 267, Short.MAX_VALUE)
        );
        histogramGraphPanelLayout.setVerticalGroup(
            histogramGraphPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 109, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 13, 0, 14);
        jPanel1.add(histogramGraphPanel, gridBagConstraints);

        bgrSlider.setPaintLabels(true);
        bgrSlider.setPaintTicks(true);
        bgrSlider.setPreferredSize(new java.awt.Dimension(180, 55));
        bgrSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                bgrSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(bgrSlider, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.3;
        lookupTableEditorPanel.add(jPanel1, gridBagConstraints);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Predefined maps"));
        jPanel2.setPreferredSize(new java.awt.Dimension(200, 69));
        jPanel2.setRequestFocusEnabled(false);
        jPanel2.setLayout(new java.awt.GridBagLayout());

        expSlider.setMaximum(20);
        expSlider.setMinimum(1);
        expSlider.setPaintTrack(false);
        expSlider.setValue(2);
        expSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "exponent", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("SansSerif.plain", 0, 12))); // NOI18N
        expSlider.setMinimumSize(new java.awt.Dimension(40, 40));
        expSlider.setPreferredSize(new java.awt.Dimension(100, 40));
        expSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                expSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanel2.add(expSlider, gridBagConstraints);

        predefinedTransparenciesCombo.setModel(new DefaultComboBoxModel(mapTypeIcons));
        predefinedTransparenciesCombo.setMinimumSize(new java.awt.Dimension(100, 23));
        predefinedTransparenciesCombo.setPreferredSize(new java.awt.Dimension(100, 40));
        predefinedTransparenciesCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                predefinedTransparenciesComboActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel2.add(predefinedTransparenciesCombo, gridBagConstraints);

        nPeriodsSpinner.setModel(new javax.swing.SpinnerNumberModel(3, 1, 9, 1));
        nPeriodsSpinner.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "periods", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("SansSerif.plain", 0, 12))); // NOI18N
        nPeriodsSpinner.setMinimumSize(new java.awt.Dimension(28, 35));
        nPeriodsSpinner.setPreferredSize(new java.awt.Dimension(45, 40));
        nPeriodsSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                nPeriodsSpinnerStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel2.add(nPeriodsSpinner, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        lookupTableEditorPanel.add(jPanel2, gridBagConstraints);

        add(lookupTableEditorPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void scaleSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_scaleSliderStateChanged
        if (!active || scaleSlider.getValueIsAdjusting())
            return;
        scale = .01f * scaleSlider.getValue();
        histogramGraphPanel.setScale(scale);
        transparencyDisplayPanel.setScale(scale);
        params.setMultiplier(scale);
    }//GEN-LAST:event_scaleSliderStateChanged


    private void nPeriodsSpinnerStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_nPeriodsSpinnerStateChanged
    {//GEN-HEADEREND:event_nPeriodsSpinnerStateChanged
        if (active) {
            params.setNPeriods((Integer) nPeriodsSpinner.getValue());
            transparencyDisplayPanel.setTransp(params.getPrototypeMap());
        }
    }//GEN-LAST:event_nPeriodsSpinnerStateChanged

    private void expSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_expSliderStateChanged
    {//GEN-HEADEREND:event_expSliderStateChanged
        if (active || !expSlider.getValueIsAdjusting()) {
            params.setExponent(expSlider.getValue() / 2f);
            transparencyDisplayPanel.setTransp(params.getPrototypeMap());
        }
    }//GEN-LAST:event_expSliderStateChanged


    private void transparencyDisplayPanelMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_transparencyDisplayPanelMouseClicked
    {//GEN-HEADEREND:event_transparencyDisplayPanelMouseClicked
        if (evt.getClickCount() == 2 || (evt.getModifiersEx() & MouseEvent.BUTTON3) != 0) {
            mapType = predefinedTransparenciesCombo.getSelectedIndex();
            nPeriodsSpinner.setVisible(mapType == 4 || mapType == 5);
            expSlider.setVisible(mapType < mapTypeIcons.length - 1);
            params.setMapType(mapType);
        }
        else
            transparencyDisplayPanel.switchPattern();
    }//GEN-LAST:event_transparencyDisplayPanelMouseClicked

    private void predefinedTransparenciesComboActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_predefinedTransparenciesComboActionPerformed
    {//GEN-HEADEREND:event_predefinedTransparenciesComboActionPerformed
        if (!active )
            return;
        mapType = predefinedTransparenciesCombo.getSelectedIndex();
        nPeriodsSpinner.setVisible(mapType == 4 || mapType == 5);
        expSlider.setVisible(mapType < 6);
        params.setMapType(mapType);
    }//GEN-LAST:event_predefinedTransparenciesComboActionPerformed

    private void bgrSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_bgrSliderStateChanged
                                                        
        if (!bgrSlider.isAdjusting() && params != null) {
            float min = bgrSlider.getMinimum();
            float max = bgrSlider.getMaximum();
            params.setBgrRange(new int[] {(int)(255 * (bgrSlider.getBottomValue() - min) / (max - min)),
                                          (int)(255 * (bgrSlider.getTopValue()    - min) / (max - min)) });
            histogramGraphPanel.setBgrRange(params.getBgrRange());
        }
    }//GEN-LAST:event_bgrSliderStateChanged

    public void setDataRange(float dmin, float dmax)
    {
        if (!active )
            return;
        updateBgHistogram();
        histogramGraphPanel.setMinMax(cmpMin, cmpMax);
        histogramGraphPanel.repaint();
    }

    public void setScale(int scale)
    {
        scaleSlider.setValue(scale);
    }

    public ColorListener getColorListener()
    {
        return transparencyDisplayPanel.getColorListener();
    }

    public ColorMapChangeListener getColorMapChangeListener()
    {
        return transparencyDisplayPanel.getColorMapChangeListener();
    }

    public int getBgrThreshold()
    {
        return bgrThreshold;
    }

    private void updateBgHistogram()
    {
        if (skipHistogramUpdate)
            return;
        if (daSchema == null || container == null)
            return;
        if (cmpMin >= cmpMax - .001f) {
            float med = .5f * (cmpMin + cmpMax);
            cmpMin = med - .0005f;
            cmpMax = med + .0005f;
        }
        float dlow = (float) ((cmpMin - daSchema.getPreferredPhysMinValue()) * (daSchema.getPreferredMaxValue() - daSchema.getPreferredMinValue()) /
                (daSchema.getPreferredPhysMaxValue() - daSchema.getPreferredPhysMinValue()) + daSchema.getPreferredMinValue());
        float dup = (float) ((cmpMax - daSchema.getPreferredPhysMinValue()) * (daSchema.getPreferredMaxValue() - daSchema.getPreferredMinValue()) /
                (daSchema.getPreferredPhysMaxValue() - daSchema.getPreferredPhysMinValue()) + daSchema.getPreferredMinValue());

        DataArray da = container.getComponent(daSchema.getName());
        histo = da.getCurrentHistogram(dlow, dup, HISTO_LENGTH, false, null);  
        histogramGraphPanel.setBgHist(histo);
    }

    public void setStartNullTransparencyComponent(boolean startNull)
    {
        params.getComponentRange().setComponentSchema(-1);
    }

    ChangeListener componentRangeChangeListener = null;

    public void setParams(TransparencyParams params)
    {
        if (this.params != null && this.params.getComponentRange() != null) 
            this.params.getComponentRange().removeChangeListener(componentRangeChangeListener);
        this.params = params;
        transp = params.getPrototypeMap();
        params.setUi(this);
        histogramGraphPanel.setTransp(transp);
        transparencyDisplayPanel.setTransp(transp);
        componentSubrangeUI.setComponentValue(params.getComponentRange());
        componentRangeChangeListener = (ChangeEvent e) -> {
            SwingInstancer.swingRunLater(() -> {
                updateComponent();
            });
        };
        params.getComponentRange().addChangeListener(componentRangeChangeListener);
    }

    public void setDataContainerSchema(DataContainerSchema inContainerSchema)
    {
        skipHistogramUpdate = true;
        params.getComponentRange().setContainerSchema(inContainerSchema);
        updateComponent();
        skipHistogramUpdate = false;
    }

    public void setDataContainer(DataContainer inContainer)
    {
        skipHistogramUpdate = true;
        container = inContainer;
        params.getComponentRange().setContainerSchema(inContainer.getSchema());
        updateComponent();
        skipHistogramUpdate = false;
    }

    private void updateComponent()
    {
        boolean oldActive = params.isActive();
        params.setActive(false);
        if (params.getComponentRange().getComponentSchema() == null) {
            daSchema = null;
            lookupTableEditorPanel.setVisible(false);
        } else {
            lookupTableEditorPanel.setVisible(true);
            daSchema = params.getComponentRange().getComponentSchema();
            cmpMin = params.getComponentRange().getPhysicalLow();
            cmpMax = params.getComponentRange().getPhysicalUp();
            histogramGraphPanel.setCompName(daSchema.getName());
            histogramGraphPanel.setMinMax(cmpMin, cmpMax);
            bgrSlider.setParams(cmpMin, cmpMax);
            updateBgHistogram();
            repaint();
        }
        params.setActive(oldActive);
        params.fireStateChanged();
    }
    
    
    public void updateDataValuesFromParams(boolean setSubrangeUI)
    {
        boolean oldActive = params.isActive();
        params.setActive(false);
        active = false;
        if (setSubrangeUI) {
            componentSubrangeUI.setComponentValue(params.getComponentRange());
            daSchema = params.getComponentRange().getComponentSchema();
        }
        predefinedTransparenciesCombo.setSelectedIndex(params.getMapType());
        expSlider.setValue((int)(2 * params.getExponent()));
        nPeriodsSpinner.setValue(params.getnPeriods());
        scaleSlider.setValue((int)(100 * params.getMultiplier()));
        cmpMin = params.getComponentRange().getPhysicalLow();
        cmpMax = params.getComponentRange().getPhysicalUp();
        if (daSchema != null)
            histogramGraphPanel.setCompName(daSchema.getName());
        histogramGraphPanel.setMinMax(cmpMin, cmpMax);
        bgrSlider.setMinMax(cmpMin, cmpMax);
        int[] bgRange = params.getBgrRange();
        if (setSubrangeUI)
            bgrSlider.setLowUp(cmpMin + bgRange[0] * (cmpMax - cmpMin) / 255.f, 
                               cmpMin + bgRange[1] * (cmpMax - cmpMin) / 255.f);
        transp = params.getPrototypeMap();
        histogramGraphPanel.repaint();
        active = true;
        params.setActive(oldActive);
        //params.fireStateChanged();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.visnow.vn.gui.widgets.FloatSubRangeSlider.FloatSubRangeSlider bgrSlider;
    private org.visnow.vn.lib.gui.ComponentBasedUI.range.ComponentSubrangeUI componentSubrangeUI;
    private javax.swing.JSlider expSlider;
    private org.visnow.vn.geometries.gui.TransparencyEditor.HistogramGraphPanel histogramGraphPanel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel lookupTableEditorPanel;
    private javax.swing.JSpinner nPeriodsSpinner;
    private javax.swing.JComboBox predefinedTransparenciesCombo;
    private javax.swing.JSlider scaleSlider;
    private org.visnow.vn.geometries.gui.TransparencyEditor.TransparencyDisplayPanel transparencyDisplayPanel;
    // End of variables declaration//GEN-END:variables
    public static void main(String[] args)
    {
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        final TransparencyEditor p = new TransparencyEditor();
        f.add(p);
        f.pack();
        f.setVisible(true);
    }

}
