/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.datamaps.utils;

import java.awt.Point;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.util.Vector;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Dist
{

    public static Point getClosestPoint(Point p, Path2D path)
    {

        Vector<Point> pts = new Vector<Point>();
        PathIterator fpi = path.getPathIterator(null, 0.25f);

        double[] coords = new double[2];
        Point pp1 = new Point(0, 0), pp2 = new Point(0, 0);
        while (!fpi.isDone()) {
            switch (fpi.currentSegment(coords)) {
                case PathIterator.SEG_CLOSE:
                    break;
                case PathIterator.SEG_LINETO:
                    pp2 = new Point((int) coords[0], (int) coords[1]);
                    Point p4 = Dist.getClosestPoint(pp1, pp2, p);
                    pts.add(p4);
                    pp1 = pp2;
                    break;
                case PathIterator.SEG_MOVETO:
                    pp1 = new Point((int) coords[0], (int) coords[1]);
                    break;

                case PathIterator.SEG_CUBICTO:
                case PathIterator.SEG_QUADTO:
                default:

            }
            fpi.next();
        }

        double minDist = Double.MAX_VALUE;
        Point minP = new Point(0, 0);
        for (Point pp : pts) {
            if (p.distance(pp) < minDist) {
                minP = pp;
                minDist = p.distance(pp);
            }
        }

        return minP;
    }

    public static Point getClosestPoint(Point pt1, Point pt2, Point p)
    {
        double u = ((p.x - pt1.x) * (pt2.x - pt1.x) + (p.y - pt1.y) * (pt2.y - pt1.y)) / (sqr(pt2.x - pt1.x) + sqr(pt2.y - pt1.y));
        if (u > 1.0) {
            return (Point) pt2.clone();
        } else if (u <= 0.0) {
            return (Point) pt1.clone();
        } else {
            return new Point((int) (pt2.x * u + pt1.x * (1.0 - u) + 0.5), (int) (pt2.y * u + pt1.y * (1.0 - u) + 0.5));
        }
    }

    private static double sqr(double x)
    {
        return x * x;
    }

    private Dist()
    {
    }
}
