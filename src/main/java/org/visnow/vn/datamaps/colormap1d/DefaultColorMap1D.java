/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.datamaps.colormap1d;

import org.visnow.vn.datamaps.DefaultColorMap;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl) University of Warsaw, Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 */
public abstract class DefaultColorMap1D extends DefaultColorMap implements ColorMap1D
{
    protected boolean symmetric = false;
    protected boolean defaultRange  = false;
    protected float   defaultLow    = -Float.MAX_VALUE;
    protected float   defaultUp     =  Float.MAX_VALUE;

    public DefaultColorMap1D()
    {
    }

    public DefaultColorMap1D(String name, boolean buildin)
    {
        super(name, buildin);
    }

    public boolean isSymmetric()
    {
        return symmetric;
    }

    public void setSymmetric(boolean symmetric)
    {
        this.symmetric = symmetric;
    }

    public boolean isDefaultRange()
    {
        return defaultRange;
    }

    public void setDefaultRange(boolean defaultRange)
    {
        this.defaultRange = defaultRange;
    }

    public float getDefaultLow()
    {
        return defaultLow;
    }

    public void setDefaultLow(float defaultLow)
    {
        this.defaultLow = defaultLow;
    }

    public float getDefaultUp()
    {
        return defaultUp;
    }

    public void setDefaultUp(float defaultUp)
    {
        this.defaultUp = defaultUp;
    }
    
}
