/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.datamaps.colormap2d;

import org.visnow.vn.datamaps.colormap2d.shape.EllipseDrawModel;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Vector;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import org.visnow.vn.datamaps.ColorMapManager;
import org.visnow.vn.datamaps.colormap2d.shape.ShapeDrawModel;
import org.visnow.vn.lib.basic.utilities.ShapeColorMapEditor.PaintMode;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ShapeColorMap2D implements TableModel, ColorMap2D
{

    private String[] colNames = {
        "Visible", "Name", "Outline", "ControlPoints", "Color", "Alpha", "X", "A", "B", "Width"
    };
    private Class[] colClass = {
        Boolean.class, String.class, Boolean.class,
        Boolean.class, Color.class, Integer.class,
        Integer.class, Integer.class, Integer.class,
        Integer.class
    };
    private BufferedImage bufferedImage;
    private int[] rgbInt;
    private Vector<ShapeDrawModel> shapes;
    public final PropertyChangeSupport propertyChangeSupport;
    private final PropertyChangeListener propertyChangeListener = new PropertyChangeListener()
    {

        public void propertyChange(PropertyChangeEvent evt)
        {
            fireTableModelListeners();
            propertyChangeSupport.firePropertyChange(evt);
            updateColorMap();
        }
    };

    public ShapeColorMap2D()
    {
        this.shapes = new Vector<ShapeDrawModel>();
        this.propertyChangeSupport = new PropertyChangeSupport(this);
        this.bufferedImage = new BufferedImage(ColorMapManager.SAMPLING_TABLE, ColorMapManager.SAMPLING_TABLE, BufferedImage.TYPE_INT_ARGB);
    }

    private void updateColorMap()
    {
        Graphics2D g2d = (Graphics2D) this.bufferedImage.getGraphics();
        g2d.setColor(Color.black);
        g2d.fillRect(0, 0, ColorMapManager.SAMPLING, ColorMapManager.SAMPLING);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        if (shapes != null) {
            for (ShapeDrawModel ellipse : shapes) {
                ellipse.paintShape(g2d, PaintMode.COLOR_ALPHA);
            }
        }

        rgbInt = new int[ColorMapManager.SAMPLING_TABLE * ColorMapManager.SAMPLING_TABLE];
        bufferedImage.getRGB(0, 0, ColorMapManager.SAMPLING_TABLE, ColorMapManager.SAMPLING_TABLE, rgbInt, 0, ColorMapManager.SAMPLING);
    }

    public void add(ShapeDrawModel shape)
    {
        shapes.add(shape);

        shape.propertyChangeSupport.addPropertyChangeListener(propertyChangeListener);
        fireTableModelListeners();
        propertyChangeSupport.firePropertyChange("add", null, shape);
        updateColorMap();
    }

    public void remove(ShapeDrawModel shape)
    {
        shapes.remove(shape);
        shape.propertyChangeSupport.removePropertyChangeListener(propertyChangeListener);
        fireTableModelListeners();
        propertyChangeSupport.firePropertyChange("remove", null, shape);
        updateColorMap();
    }

    private void fireTableModelListeners()
    {
        for (TableModelListener tableModelListener : tableModelListeners) {
            tableModelListener.tableChanged(new TableModelEvent(this));
        }
    }

    public int getRowCount()
    {
        return shapes.size();
    }

    public int getColumnCount()
    {
        return colNames.length;
    }

    public String getColumnName(int columnIndex)
    {
        return colNames[columnIndex];
    }

    public Class<?> getColumnClass(int columnIndex)
    {
        return colClass[columnIndex];
    }

    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        return true;
    }

    public Object getValueAt(int rowIndex, int columnIndex)
    {
        EllipseDrawModel ellipse = (EllipseDrawModel) shapes.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return ellipse.isShow();
            case 1:
                return ellipse.getName();
            case 2:
                return ellipse.isShowOutline();
            case 3:
                return ellipse.isShowCP();
            case 4:
                return ellipse.getColor();
            case 5:
                return ellipse.getColor().getAlpha();
            case 6:
                return ellipse.getCenterX();
            case 7:
                return ellipse.getRadiusA();
            case 8:
                return ellipse.getRadiusB();
            case 9:
                return ellipse.getWidth();
            default:
                return null;
        }
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex)
    {
        EllipseDrawModel ellipse = (EllipseDrawModel) shapes.get(rowIndex);
        switch (columnIndex) {
            case 0:
                ellipse.setShow((Boolean) aValue);
                break;
            case 1:
                ellipse.setName((String) aValue);
                break;
            case 2:
                ellipse.setShowOutline((Boolean) aValue);
                break;
            case 3:
                ellipse.setShowCP((Boolean) aValue);
                break;
            case 4:
                ellipse.setColor((Color) aValue);
                break;
            case 5:
                Color c = ellipse.getColor();
                ellipse.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), (Integer) aValue));
                break;
            case 6:
                ellipse.setCenterX((Integer) aValue);
                break;
            case 7:
                ellipse.setRadiusA((Integer) aValue);
                break;
            case 8:
                ellipse.setRadiusB((Integer) aValue);
                break;
            case 9:
                ellipse.setWidth((Integer) aValue);
                break;
            default:
        }
        //
    }

    private Vector<TableModelListener> tableModelListeners = new Vector<TableModelListener>();

    public void addTableModelListener(TableModelListener l)
    {
        tableModelListeners.add(l);
    }

    public void removeTableModelListener(TableModelListener l)
    {
        tableModelListeners.remove(l);
    }

    public Vector<ShapeDrawModel> getShapes()
    {
        return shapes;
    }

    public int[] getRGBColorTable()
    {
        return rgbInt;
    }

    public int[] getARGBColorTable()
    {
        return rgbInt;
    }

    public byte[] getRGBByteColorTable()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public byte[] getARGBByteColorTable()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener)
    {
        propertyChangeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener)
    {
        propertyChangeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    public int[] getDefaultRGBColorTable()
    {
        return getRGBColorTable();
    }

    public byte[] getDefaultRGBByteColorTable()
    {
        return getDefaultRGBByteColorTable();
    }

    public int[] getDims()
    {
        int[] dims = {ColorMapManager.SAMPLING_TABLE, ColorMapManager.SAMPLING_TABLE};
        return dims;
    }

    public boolean isBuildin()
    {
        return false;
    }

    public String getName()
    {
        return "Ellipse";
    }
}
