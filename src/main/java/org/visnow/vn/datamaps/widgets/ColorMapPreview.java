/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.datamaps.widgets;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import org.visnow.vn.datamaps.ColorMap;
import org.visnow.vn.datamaps.ColorMapManager;
import org.visnow.vn.datamaps.colormap1d.ColorMap1D;
import org.visnow.vn.datamaps.colormap2d.ColorMap2D;
import org.visnow.vn.datamaps.utils.Orientation;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ColorMapPreview extends JComponent
{

    protected boolean editor;
    protected ColorMap colorMap;
    protected Orientation orientation = Orientation.VERTICAL;
    protected PropertyChangeListener propertyChangeListener = new PropertyChangeListener()
    {

        public void propertyChange(PropertyChangeEvent evt)
        {
            repaint();
        }
    };

    public boolean isHorizontal()
    {
        return this.orientation == Orientation.HORIZONTAL;
    }

    public boolean isEditor()
    {
        return editor;
    }

    public void setEditor(boolean editor)
    {
        this.editor = editor;
    }

    public Orientation getOrientation()
    {
        return orientation;
    }

    public void setOrientation(Orientation orientation)
    {
        this.orientation = orientation;
    }

    public ColorMapPreview()
    {
        this.orientation = Orientation.VERTICAL;
        this.editor = false;
        setBorder(new CompoundBorder(new LineBorder(Color.gray), new LineBorder(Color.white)));
    }

    public ColorMap getColorMap()
    {
        return colorMap;
    }

    public void setColorMap(ColorMap colorMap)
    {
        if (this.colorMap != null) {
            this.colorMap.removePropertyChangeListener(propertyChangeListener);
        }
        this.colorMap = colorMap;
        this.colorMap.addPropertyChangeListener(propertyChangeListener);
        repaint();
    }

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        Insets insets;
        if (getBorder() != null) {
            insets = getBorder().getBorderInsets(this);
        } else {
            insets = new Insets(0, 0, 0, 0);
        }
        Dimension dimension = new Dimension(getWidth() - insets.left - insets.right, getHeight() - insets.top - insets.bottom);

        g.setColor(getBackground());
        g.fillRect(insets.left, insets.top, dimension.width, dimension.height);
        if (colorMap != null && isEnabled()) {
            int[] colorTable = colorMap.getRGBColorTable();

            if (colorMap instanceof ColorMap1D) {
                if (isHorizontal()) {
                    BufferedImage bi = new BufferedImage(ColorMapManager.SAMPLING, dimension.height, BufferedImage.TYPE_INT_ARGB);
                    for (int y = 0; y < dimension.height; y++) {
                        bi.setRGB(0, y, ColorMapManager.SAMPLING, 1, colorTable, 0, ColorMapManager.SAMPLING);
                    }
                    g.drawImage(bi, insets.left, insets.top, dimension.width, dimension.height, null);
                } else {
                    BufferedImage bi = new BufferedImage(dimension.width, ColorMapManager.SAMPLING, BufferedImage.TYPE_INT_ARGB);
                    for (int x = 0; x < dimension.width; x++) {
                        bi.setRGB(x, 0, 1, ColorMapManager.SAMPLING, colorTable, 0, 1);
                    }
                    g.drawImage(bi, insets.left, insets.top, dimension.width, dimension.height, null);
                }
            } else if (colorMap instanceof ColorMap2D) {
                BufferedImage bi = new BufferedImage(ColorMapManager.SAMPLING, ColorMapManager.SAMPLING, BufferedImage.TYPE_INT_ARGB);
                bi.setRGB(0, 0, ColorMapManager.SAMPLING, ColorMapManager.SAMPLING, colorTable, 0, ColorMapManager.SAMPLING);
                g.drawImage(bi, insets.left, insets.top, dimension.width, dimension.height, null);
            }

        }
    }
}
