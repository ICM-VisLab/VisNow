/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.autohelp;

/**
 * fast, testing prototype of web browser to test java html rendering engine
 */
import javax.swing.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;

public class TestWebBrowser
{

    public static void main(String[] args)
    {

        // get the first URL
        String initialPage = "https://www.visnow.org/";
        if (args.length > 0) {
            initialPage = args[0];
        }

        // set up the editor pane
        final JEditorPane jep = new JEditorPane();
        jep.setEditable(false);

        try {
            jep.setPage(initialPage);
        } catch (IOException ex) {

        }

        // set up the window
        JScrollPane scrollPane = new JScrollPane(jep);
        JFrame f = new JFrame();
        JPanel p = new JPanel();
        p.setLayout(new BorderLayout());
        final JTextField t = new JTextField();
        t.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                try {
                    System.out.println("Loading : " + t.getText());
                    jep.setPage(t.getText());
                    System.out.println("Loading : " + t.getText());
                } catch (Exception ex) {
                }
                ;
            }
        });

        p.add(t, BorderLayout.NORTH);
        p.add(scrollPane, BorderLayout.CENTER);

        f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        f.setContentPane(p);
        f.setSize(512, 342);
        EventQueue.invokeLater(new FrameShower(f));

    }

    // Helps avoid a really obscure deadlock condition.
    // See http://java.sun.com/developer/JDCTechTips/2003/tt1208.html#1
    private static class FrameShower implements Runnable
    {

        private final Frame frame;

        FrameShower(Frame frame)
        {
            this.frame = frame;
        }

        public void run()
        {
            frame.setVisible(true);
        }

    }

}
