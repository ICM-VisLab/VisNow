/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.applications;

import java.awt.BorderLayout;
import org.visnow.vn.engine.core.CoreName;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.geometries.viewer3d.Display3DPanel;
import org.visnow.vn.lib.basic.viewers.Viewer3D.Viewer3D;
import org.visnow.vn.lib.templates.visualization.modules.VisualizationModule;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class DefaultApplication3DTemplate extends ApplicationTemplate
{
    
    protected DefaultApplication3DTemplate()
    {
        super();
        Viewer3D viewer3D = new Viewer3D(true);
        initModule("viewer 3D", viewer3D);
        Display3DPanel displayPanel = viewer3D.getDisplayPanel();
        displayPanel.setSize(mainPanel.getWidth(), mainPanel.getHeight());
        mainPanel.add(displayPanel, BorderLayout.CENTER);
    }
    
    @Override
    protected ModuleCore addModule(String name, String classDesc)
    {
        try {
            ModuleCore core = application.getLibraries().generateCore(new CoreName("internal", classDesc));
            initModule(name, core);
            if (core instanceof VisualizationModule)
                addLink(name, "outObj", "viewer 3D", "inObject");
            return core;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        VisNow.mainBlocking(args, false);
        new DefaultApplication3DTemplate().setVisible(true);
    }

}
