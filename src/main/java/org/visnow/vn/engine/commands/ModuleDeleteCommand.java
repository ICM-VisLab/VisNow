/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.commands;

import java.awt.Point;
import org.visnow.vn.engine.core.CoreName;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class ModuleDeleteCommand extends Command
{

    private String name;
    private CoreName coreName;
    private Point position;

    public String getName()
    {
        return name;
    }

    public CoreName getCoreName()
    {
        return coreName;
    }

    public Point getPosition()
    {
        return position;
    }

    public ModuleDeleteCommand(String name, CoreName coreName, Point position)
    {
        this(name, coreName, position, true);
    }

    public ModuleDeleteCommand(String name, CoreName coreName, Point position, boolean active)
    {
        super(Command.DELETE_MODULE);
        this.name = name;
        this.coreName = coreName;
        this.position = position;
        this.active = active;
    }

    protected ModuleDeleteCommand(ModuleAddCommand reverse)
    {
        super(Command.DELETE_MODULE);
        this.name = reverse.getName();
        this.coreName = reverse.getCoreName();
        this.position = reverse.getPosition();
    }

    @Override
    public String toString()
    {
        return "DELETE_MODULE " + name;
    }
}
