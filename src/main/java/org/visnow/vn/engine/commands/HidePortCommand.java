/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.commands;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class HidePortCommand extends Command
{

    private String moduleName;

    public String getModuleName()
    {
        return moduleName;
    }

    private String portName;

    public String getPortName()
    {
        return portName;
    }

    private boolean input;

    public boolean isInput()
    {
        return input;
    }

    public HidePortCommand(String moduleName, boolean input, String portName)
    {
        super(Command.UI_HIDE_PORT);
        this.moduleName = moduleName;
        this.input = input;
        this.portName = portName;
        this.reverse = new ShowPortCommand(this);
    }

    public HidePortCommand(ShowPortCommand reverse)
    {
        super(Command.UI_HIDE_PORT);
        this.moduleName = reverse.getModuleName();
        this.input = reverse.isInput();
        this.portName = reverse.getPortName();
        this.reverse = reverse;
    }

}
