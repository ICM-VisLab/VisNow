/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.commands;

import org.visnow.vn.engine.core.LinkName;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class LinkAddCommand extends Command
{

    private LinkName name;

    public LinkName getName()
    {
        return name;
    }

    public LinkAddCommand(LinkName name, boolean active)
    {
        super(Command.ADD_LINK);
        this.name = name;
        this.reverse = new LinkDeleteCommand(this);
        this.active = active;
    }

    protected LinkAddCommand(LinkDeleteCommand reverse)
    {
        super(Command.ADD_LINK);
        this.name = reverse.getName();
        this.reverse = reverse;
        // Scenario:
        // A link between two modules has been removed,
        // then user undo this operation.
        // The default behavior of the receiving module
        // is set below.
        // Current value:
        // false
        // Module does not start action by default.
        this.active = false;
    }
}
