/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.commands;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class Command
{

    //<editor-fold defaultstate="collapsed" desc=" FINAL types ">
    public static final int BLOCK = 0x1;
    public static final int BLOCK_REDO = 0x2;
    //public static final int BEGIN = 0x1;
    //public static final int END = 0x2;

    public static final int ADD_LIBRARY = 0x11;
    public static final int RENAME_LIBRARY = 0x12;
    public static final int DELETE_LIBRARY = 0x13;

    public static final int ADD_MODULE = 0x21;
    public static final int RENAME_MODULE = 0x22;
    public static final int DELETE_MODULE = 0x23;

    public static final int ADD_LINK = 0x31;
    public static final int DELETE_LINK = 0x33;
    public static final int SPLIT_LINK = 0x35;

    //public static final int UI_MOVE_MODULE = 0x101;
    public static final int UI_MOVE_MULTIPLE_MODULES = 0x102;
    public static final int UI_SHOW_PORT = 0x111;
    public static final int UI_HIDE_PORT = 0x112;
    public static final int UI_MOVE_LINK_BAR = 0x131;

    public static final int UI_SCENE_SELECTED_MODULE = 0x151;
    public static final int UI_FRAME_SELECTED_MODULE = 0x152;
    //</editor-fold>

    protected int type;

    public int getType()
    {
        return type;
    }

    protected boolean active;

    /**
     * If this flag is true than after executing this command another action is taken (like running module after executing ModuleAddCommand)
     * @return true if command is active
     */
    public boolean isActive()
    {
        return active;
    }        
    
    protected Command reverse;

    public Command getReverseCommand()
    {
        return reverse;
    }

    protected Command(int type)
    {
        this.type = type;
    }

}
