/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.core;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public interface ParameterProxy
{
    //trying to simplify runButton usage 
    //trying to make better abstraction for current Parameter mechanism.

    public <T> T get(ParameterName<T> signature);

    /**
     * If value is an array (also multidimensional), than array should be cloned. Single element within an array is not cloned.
     * For primitives and String (which are immutable classes) it works just fine.
     * <p>
     * Assuming that every set is one call - that's why these methods should be renamed to "send".
     * This is to avoid multiple call on one GUI action.
     */
    public <T> void set(ParameterName<T> signature, T value);

    public <T1, T2> void set(ParameterName<T1> signature1, T1 value1, ParameterName<T2> signature2, T2 value2);

    public <T1, T2, T3> void set(ParameterName<T1> signature1, T1 value1, ParameterName<T2> signature2, T2 value2, ParameterName<T3> signature3, T3 value3);

    public <T1, T2, T3, T4> void set(ParameterName<T1> signature1, T1 value1, ParameterName<T2> signature2, T2 value2, ParameterName<T3> signature3, T3 value3, ParameterName<T4> signature4, T4 value4);

    public <T1, T2, T3, T4> void set(ParameterName<T1> signature1, T1 value1, ParameterName<T2> signature2, T2 value2, ParameterName<T3> signature3, T3 value3, ParameterName<T4> signature4, T4 value4, Object... signatureAndValues);
}
