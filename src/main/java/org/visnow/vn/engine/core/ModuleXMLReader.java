/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.core;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import org.visnow.vn.lib.types.VNDataAcceptor;
import org.visnow.vn.lib.types.VNDataSchema;
import org.visnow.vn.lib.types.VNDataSchemaComparator;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ModuleXMLReader
{

    public static InputEgg[] getInputEggsFromModuleXML(String packageName, ClassLoader loader) throws URISyntaxException, ParserConfigurationException, SAXException, IOException, ClassNotFoundException
    {
        if (packageName == null) {
            return null;
        }

        InputStream is = null;
        if (loader != null) {
            is = loader.getResourceAsStream(packageName.replace(".", "/") + "/module.xml");
        }

        if (is == null)
            is = ModuleXMLReader.class.getResourceAsStream("/" + packageName.replace(".", "/") + "/module.xml");

        if (is == null) {
            return null;
        }

        InputEgg[] out = getInputEggsFromStream(packageName, is, loader);
        is.close();

        return out;
    }

    public static InputEgg[] getInputEggsFromStream(String packageName, InputStream is, ClassLoader loader) throws URISyntaxException, ParserConfigurationException, SAXException, IOException, ClassNotFoundException
    {
        if (packageName == null || is == null) return null;
        else {

            Node node = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is).getDocumentElement();
            InputEgg[] inputEggs = null;
            Node inputsNode = null;

            if (node.getNodeName().equals("module")) {
                ArrayList<InputEgg> inputEggList = new ArrayList<InputEgg>();

                for (int i = 0; i < node.getChildNodes().getLength(); i++) {
                    Node childNode = node.getChildNodes().item(i);
                    String nodeName = childNode.getNodeName();
                    if (nodeName.equalsIgnoreCase("inputs")) {
                        inputsNode = node.getChildNodes().item(i);
                        Node inputNode;
                        for (int j = 0; j < inputsNode.getChildNodes().getLength(); j++) {
                            inputNode = inputsNode.getChildNodes().item(j);
                            if (inputNode.getNodeName().equals("input")) {

                                String inputName = null;
                                String inputType = null;
                                String inputModifiers = null;
                                String minConnections = null;
                                String maxConnections = null;
                                String inputDescription = null;

                                if (inputNode.getAttributes().getNamedItem("name") != null) {
                                    inputName = inputNode.getAttributes().getNamedItem("name").getNodeValue();
                                }
                                if (inputNode.getAttributes().getNamedItem("type") != null) {
                                    inputType = inputNode.getAttributes().getNamedItem("type").getNodeValue();
                                }
                                if (inputNode.getAttributes().getNamedItem("modifiers") != null) {
                                    inputModifiers = inputNode.getAttributes().getNamedItem("modifiers").getNodeValue();
                                }
                                if (inputNode.getAttributes().getNamedItem("minConnections") != null) {
                                    minConnections = inputNode.getAttributes().getNamedItem("minConnections").getNodeValue();
                                }
                                if (inputNode.getAttributes().getNamedItem("maxConnections") != null) {
                                    maxConnections = inputNode.getAttributes().getNamedItem("maxConnections").getNodeValue();
                                }

                                if (inputName != null && inputType != null) {
                                    Class typeClass = null;
                                    if (loader != null)
                                        typeClass = loader.loadClass(inputType);
                                    if (typeClass == null)
                                        typeClass = ModuleXMLReader.class.getClassLoader().loadClass(inputType);

                                    int modifiers = InputEgg.NORMAL;
                                    if (inputModifiers != null) {
                                        String[] mods = inputModifiers.split(":");
                                        for (int k = 0; k < mods.length; k++) {
                                            if (mods[k].equals("TRIGGERING"))
                                                modifiers = modifiers | InputEgg.TRIGGERING;
                                            else if (mods[k].equals("HIDDEN"))
                                                modifiers = modifiers | InputEgg.HIDDEN;
                                            else if (mods[k].equals("NECESSARY"))
                                                modifiers = modifiers | InputEgg.NECESSARY;
                                        }
                                    }

                                    int minC = 0;
                                    if (minConnections != null) {
                                        try {
                                            minC = Integer.parseInt(minConnections);
                                        } catch (NumberFormatException ex) {
                                            minC = 0;
                                        }
                                    }

                                    int maxC = 1;
                                    if (maxConnections != null) {
                                        try {
                                            maxC = Integer.parseInt(maxConnections);
                                        } catch (NumberFormatException ex) {
                                            maxC = 1;
                                        }
                                    }

                                    //acceptors
                                    VNDataAcceptor[] acceptors = null;
                                    ArrayList<VNDataAcceptor> acceptorList = new ArrayList<VNDataAcceptor>();

                                    //parse acceptors
                                    for (int k = 0; k < inputNode.getChildNodes().getLength(); k++) {
                                        Node acceptorNode = inputNode.getChildNodes().item(k);
                                        if (acceptorNode.getNodeType() == Node.TEXT_NODE || acceptorNode.getNodeType() == Node.COMMENT_NODE || acceptorNode.getNodeName().equalsIgnoreCase("description")) {
                                            ; // ignore (this case should be removed in validator anyway) TODO: implement schema validation
                                        } else if (!acceptorNode.getNodeName().equalsIgnoreCase("acceptor")) {
                                            throw new IllegalStateException("Incorrect node: " + acceptorNode + " for package: " + packageName);
                                        } else {

                                            ArrayList<String[]> params = new ArrayList<String[]>();
                                            for (int l = 0; l < acceptorNode.getChildNodes().getLength(); l++) {

                                                Node paramNode = acceptorNode.getChildNodes().item(l);
                                                if (paramNode.getNodeType() == Node.TEXT_NODE || paramNode.getNodeType() == Node.COMMENT_NODE) {
                                                    ; // ignore (this case should be removed in validator anyway) TODO: implement schema validation
                                                } else if (!paramNode.getNodeName().equalsIgnoreCase("param")) {
                                                    throw new IllegalStateException("Incorrect node: " + paramNode + " for package: " + packageName);
                                                } else {

                                                    String[] paramStr = new String[2];
                                                    if (paramNode.getAttributes().getNamedItem("name") != null) {
                                                        paramStr[0] = paramNode.getAttributes().getNamedItem("name").getNodeValue();
                                                    }
                                                    if (paramNode.getAttributes().getNamedItem("value") != null) {
                                                        paramStr[1] = paramNode.getAttributes().getNamedItem("value").getNodeValue();
                                                    }
                                                    if (paramStr[0] == null || paramStr[0].length() == 0 || paramStr[1] == null || paramStr[1].length() == 0) {
                                                        continue;
                                                    }

                                                    params.add(paramStr);
                                                }
                                            }

                                            //create schema and comparator
                                            VNDataSchema schm = new VNDataSchema(params);
                                            long comp = VNDataSchemaComparator.createComparatorFromSchemaParams(params);
                                            acceptorList.add(new VNDataAcceptor(schm, comp));
                                        }
                                    }

                                    if (acceptorList.size() > 0) {
                                        acceptors = new VNDataAcceptor[acceptorList.size()];
                                        for (int k = 0; k < acceptors.length; k++) {
                                            acceptors[k] = acceptorList.get(k);
                                        }
                                    }

                                    //parse description
                                    Node descNode;
                                    for (int k = 0; k < inputNode.getChildNodes().getLength(); k++) {
                                        descNode = inputNode.getChildNodes().item(k);
                                        if (!descNode.getNodeName().equals("description")) {
                                            continue;
                                        }

                                        if (descNode.getAttributes().getNamedItem("value") != null) {
                                            inputDescription = descNode.getAttributes().getNamedItem("value").getNodeValue();
                                        }
                                    }

                                    inputEggList.add(new InputEgg(inputName, typeClass, modifiers, minC, maxC, inputDescription, acceptors));
                                } else {
                                    System.err.println("illegal input entry in package: " + packageName);
                                }
                            }
                        }
                    }
                }

                inputEggs = new InputEgg[inputEggList.size()];
                for (int i = 0; i < inputEggs.length; i++) {
                    inputEggs[i] = inputEggList.get(i);
                }
            }
            return inputEggs;
        }
    }

    public static OutputEgg[] getOutputEggsFromModuleXML(String packageName, ClassLoader loader) throws URISyntaxException, ParserConfigurationException, SAXException, IOException, ClassNotFoundException
    {
        if (packageName == null) {
            return null;
        }

        InputStream is = null;
        if (loader != null) {
            is = loader.getResourceAsStream(packageName.replace(".", "/") + "/module.xml");
        }
        if (is == null) {
            is = ModuleXMLReader.class.getResourceAsStream("/" + packageName.replace(".", "/") + "/module.xml");
        }
        if (is == null) {
            return null;
        }

        OutputEgg[] out = getOutputEggsFromStream(packageName, is, loader);
        is.close();

        return out;
    }

    public static OutputEgg[] getOutputEggsFromStream(String packageName, InputStream is, ClassLoader loader) throws URISyntaxException, ParserConfigurationException, SAXException, IOException, ClassNotFoundException
    {
        if (packageName == null || is == null) {
            return null;
        }

        Node node = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is).getDocumentElement();
        OutputEgg[] outputEggs = null;
        Node outputsNode = null;

        if (node.getNodeName().equals("module")) {
            String moduleClass = "";
            if (node.getAttributes().getNamedItem("class") != null) {
                moduleClass = node.getAttributes().getNamedItem("class").getNodeValue();
            }

            ArrayList<OutputEgg> outputEggList = new ArrayList<OutputEgg>();

            for (int i = 0; i < node.getChildNodes().getLength(); i++) {
                String nodeName = node.getChildNodes().item(i).getNodeName();

                if (nodeName.equalsIgnoreCase("outputs")) {
                    outputsNode = node.getChildNodes().item(i);
                    Node outputNode;
                    for (int j = 0; j < outputsNode.getChildNodes().getLength(); j++) {
                        outputNode = outputsNode.getChildNodes().item(j);

                        if (outputNode.getNodeName().equals("output")) {

                            String outputName = null;
                            String outputType = null;
                            String outputDescription = null;
                            String maxConnections = null;

                            if (outputNode.getAttributes().getNamedItem("name") != null) {
                                outputName = outputNode.getAttributes().getNamedItem("name").getNodeValue();
                            }
                            if (outputNode.getAttributes().getNamedItem("type") != null) {
                                outputType = outputNode.getAttributes().getNamedItem("type").getNodeValue();
                            }
                            if (outputNode.getAttributes().getNamedItem("maxConnections") != null) {
                                maxConnections = outputNode.getAttributes().getNamedItem("maxConnections").getNodeValue();
                            }

                            int maxC = -1;
                            if (maxConnections != null) {
                                try {
                                    maxC = Integer.parseInt(maxConnections);
                                } catch (NumberFormatException ex) {
                                    maxC = -1;
                                }
                            }

                            //output schemas
                            VNDataSchema[] schemas = null;
                            ArrayList<VNDataSchema> schemasList = new ArrayList<VNDataSchema>();

                            //parse schemas
                            Node schemaNode;
                            for (int k = 0; k < outputNode.getChildNodes().getLength(); k++) {
                                schemaNode = outputNode.getChildNodes().item(k);
                                if (!schemaNode.getNodeName().equals("schema")) {
                                    continue;
                                }

                                Node paramNode;
                                ArrayList<String[]> params = new ArrayList<String[]>();
                                for (int l = 0; l < schemaNode.getChildNodes().getLength(); l++) {
                                    paramNode = schemaNode.getChildNodes().item(l);
                                    if (!paramNode.getNodeName().equals("param")) {
                                        continue;
                                    }

                                    String[] paramStr = new String[2];
                                    if (paramNode.getAttributes().getNamedItem("name") != null) {
                                        paramStr[0] = paramNode.getAttributes().getNamedItem("name").getNodeValue();
                                    }
                                    if (paramNode.getAttributes().getNamedItem("value") != null) {
                                        paramStr[1] = paramNode.getAttributes().getNamedItem("value").getNodeValue();
                                    }
                                    if (paramStr[0] == null || paramStr[0].length() == 0 || paramStr[1] == null || paramStr[1].length() == 0) {
                                        continue;
                                    }

                                    params.add(paramStr);
                                }

                                //create schema
                                schemasList.add(new VNDataSchema(params));
                            }

                            if (schemasList.size() > 0) {
                                schemas = new VNDataSchema[schemasList.size()];
                                for (int k = 0; k < schemas.length; k++) {
                                    schemas[k] = schemasList.get(k);
                                }
                            }

                            //parse description
                            Node descNode;
                            for (int k = 0; k < outputNode.getChildNodes().getLength(); k++) {
                                descNode = outputNode.getChildNodes().item(k);
                                if (!descNode.getNodeName().equals("description")) {
                                    continue;
                                }

                                if (descNode.getAttributes().getNamedItem("value") != null) {
                                    outputDescription = descNode.getAttributes().getNamedItem("value").getNodeValue();
                                }
                            }

                            if (outputName != null && outputType != null) {
                                Class typeClass = null;
                                if (loader != null)
                                    typeClass = loader.loadClass(outputType);
                                if (typeClass == null)
                                    typeClass = ModuleXMLReader.class.getClassLoader().loadClass(outputType);
                                outputEggList.add(new OutputEgg(outputName, typeClass, maxC, outputDescription, schemas));
                            } else {
                                System.err.println("illegal output entry in package: " + packageName);
                            }
                        } else if (outputNode.getNodeName().equals("geometryOutput")) {
                            //parse description
                            Node descNode;
                            String goDesc = null;
                            for (int k = 0; k < outputNode.getChildNodes().getLength(); k++) {
                                descNode = outputNode.getChildNodes().item(k);
                                if (!descNode.getNodeName().equals("description")) {
                                    continue;
                                }

                                if (descNode.getAttributes().getNamedItem("value") != null) {
                                    goDesc = descNode.getAttributes().getNamedItem("value").getNodeValue();
                                }
                            }

                            Class mc = null;
                            try {
                                if (loader != null)
                                    mc = loader.loadClass(packageName + "." + moduleClass);
                                if (mc == null)
                                    mc = ModuleXMLReader.class.getClassLoader().loadClass(packageName + "." + moduleClass);

                                if (mc.getField("geometryOutput") != null) {
                                    OutputEgg goe = (OutputEgg) mc.getField("geometryOutput").get(null);
                                    if (goDesc != null)
                                        goe.setDescription(goDesc);
                                    if (goe != null) {
                                        outputEggList.add(goe);
                                    }
                                }
                            } catch (ClassNotFoundException ex) {
                                System.err.println("ERROR creating geometry output in " + packageName + "." + moduleClass + ": class not found: " + ex.getMessage());
                            } catch (NoSuchFieldException ex) {
                                System.err.println("ERROR creating geometry output in " + packageName + "." + moduleClass + ": no such field: " + ex.getMessage());
                            } catch (IllegalAccessException ex) {
                                System.err.println("ERROR creating geometry output in " + packageName + "." + moduleClass + ": illegal access: " + ex.getMessage());
                            } catch (IllegalArgumentException ex) {
                                System.err.println("ERROR creating geometry output in " + packageName + "." + moduleClass + ": illegal argument: " + ex.getMessage());
                            } catch (SecurityException ex) {
                                System.err.println("ERROR creating geometry output in " + packageName + "." + moduleClass + ": security exception: " + ex.getMessage());
                            }
                        }
                    }
                }
            }

            outputEggs = new OutputEgg[outputEggList.size()];
            for (int i = 0; i < outputEggs.length; i++) {
                outputEggs[i] = outputEggList.get(i);
            }
        }
        return outputEggs;
    }

    public static String[] getModuleInfo(String packageName, ClassLoader loader) throws URISyntaxException, ParserConfigurationException, SAXException, IOException
    {
        if (packageName == null) {
            return null;
        }

        InputStream is = null;
        if (loader != null) {
            is = loader.getResourceAsStream(packageName.replace(".", "/") + "/module.xml");
        }

        if (is == null)
            is = ModuleXMLReader.class.getResourceAsStream("/" + packageName.replace(".", "/") + "/module.xml");

        if (is == null) {
            return null;
        }

        String[] out = getModuleInfoFromStream(packageName, is, loader);
        is.close();

        return out;
    }

    public static String[] getModuleInfoFromStream(String packageName, InputStream is, ClassLoader loader) throws URISyntaxException, ParserConfigurationException, SAXException, IOException
    {
        if (packageName == null || is == null) {
            return null;
        }

        Node node = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is).getDocumentElement();
        Node descNode;
        Node readerNode;
        String moduleName = null;
        String moduleClass = null;
        String moduleDescription = null;
        String moduleLongDescription = null;
        String moduleKeyWords = null;
        String readerDataType = null;
        String testData = "false";

        if (node.getNodeName().equals("module")) {
            if (node.getAttributes().getNamedItem("name") != null) {
                moduleName = node.getAttributes().getNamedItem("name").getNodeValue();
            }

            if (node.getAttributes().getNamedItem("class") != null) {
                moduleClass = node.getAttributes().getNamedItem("class").getNodeValue();
                moduleClass = packageName + "." + moduleClass;
            }

            for (int i = 0; i < node.getChildNodes().getLength(); i++) {
                String nodeName = node.getChildNodes().item(i).getNodeName();
                if (nodeName.equalsIgnoreCase("description")) {
                    descNode = node.getChildNodes().item(i);
                    moduleDescription = descNode.getAttributes().getNamedItem("value").getNodeValue();
                }

                if (nodeName.equalsIgnoreCase("longDescription")) {
                    descNode = node.getChildNodes().item(i);
                    moduleLongDescription = descNode.getAttributes().getNamedItem("value").getNodeValue();
                }

                if (nodeName.equalsIgnoreCase("keyWords")) {
                    descNode = node.getChildNodes().item(i);
                    moduleKeyWords = descNode.getAttributes().getNamedItem("value").getNodeValue();
                }

                if (nodeName.equalsIgnoreCase("reader")) {
                    readerNode = node.getChildNodes().item(i);
                    String tmp = readerNode.getAttributes().getNamedItem("datatype").getNodeValue();
                    if (tmp != null && tmp.length() > 0)
                        readerDataType = tmp;
                }

                if (nodeName.equalsIgnoreCase("testdata")) {
                    testData = "true";
                }
            }

        }

        if (moduleName != null && moduleClass != null) {
            String[] out = new String[8];
            out[0] = moduleName;
            out[1] = moduleClass;
            out[2] = moduleDescription;
            out[3] = packageName;
            out[4] = readerDataType;
            out[5] = testData;
            out[6] = moduleLongDescription;
            out[7] = moduleKeyWords;
            return out;
        }
        return null;
    }

    private ModuleXMLReader()
    {
    }

}
