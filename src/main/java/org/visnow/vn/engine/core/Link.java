/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.core;

import org.visnow.vn.engine.commands.SplitLinkCommand;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class Link implements LinkFace
{

    private Input input;
    private Output output;
    protected LinkName name;

    public Input getInput()
    {
        return input;
    }

    public Output getOutput()
    {
        return output;
    }

    public LinkName getName()
    {
        return name;
    }

    public Link(Output output, Input input)
    {
        this.output = output;
        this.input = input;
        this.name = new LinkName(
            output.getModuleBox().getName(),
            output.getName(),
            input.getModuleBox().getName(),
            input.getName());
        output.addLink(this);
        input.addLink(this);
    }

    public void splitRequested()
    {
        this.getOutput().getModuleBox().getEngine().getApplication().getReceiver().receive(
            new SplitLinkCommand(this.getName()));
    }

    public void splitToOutput(Output output)
    {
        Output oldOutput = this.output;
        this.output = output;
        this.name = new LinkName(
            output.getModuleBox().getName(),
            output.getName(),
            input.getModuleBox().getName(),
            input.getName());
        output.addLink(this);
        oldOutput.removeLink(this, true);
    }

    public void updateName(LinkName ln)
    {
        this.name = ln;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return output.isActive();
    }

}
