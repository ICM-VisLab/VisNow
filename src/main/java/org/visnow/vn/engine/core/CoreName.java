/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.core;

/**
 * Lightweight class containing all the data necessary to save and restore
 * a module core.
 * <p>
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class CoreName
{

    private String libraryName;
    private String libraryCoreName = null;
    private String className;

    /**
     * Gets the name of the library containing the module core.
     * <p>
     * @return The name of the library containing the module core.
     */
    public String getLibraryName()
    {
        return libraryName;
    }

    /**
     * Gets the name of the module core.
     * <p>
     * @return The name of the module core.
     */
    public String getLibraryCoreName()
    {
        return libraryCoreName;
    }

    /**
     * Gets the name of the module core class.
     * <p>
     * @return Full pathname of the module core class.
     */
    public String getClassName()
    {
        return className;
    }

    /**
     * Creates new coreName with given library and class names.
     * <p>
     * @param libraryName Name of the library containing the module core.
     * @param className   Full pathname of the module core class.
     */
    public CoreName(String libraryName, String className)
    {
        this.libraryName = libraryName;
        this.className = className;
    }

    /**
     * Creates new coreName with given library and class names.
     * <p>
     * @param libraryName     Name of the library containing the module core.
     * @param libraryCoreName Module core name in the library.
     * @param className       Full pathname of the module core class.
     */
    public CoreName(String libraryName, String libararyCoreName, String className)
    {
        this.libraryName = libraryName;
        this.libraryCoreName = libararyCoreName;
        this.className = className;
    }

    public String toString()
    {
        return libraryName + "." + libraryCoreName + "." + className;
    }

}
