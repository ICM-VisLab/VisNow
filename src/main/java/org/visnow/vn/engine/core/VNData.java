/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.core;

//import org.visnow.vn.engine.main.ModuleBox;

import org.apache.log4j.Logger;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class VNData
{
    private static final Logger LOGGER = Logger.getLogger(VNData.class);
    
    private static boolean debug = false;

    protected Object value;
    protected Class type;
    protected ModuleBoxFace sourceModule;
    protected Output sourceOutput;

    protected boolean valueChanged;

    public Object getValue()
    {
        return value;
    }

    private void checkSaturation()
    {
        sourceOutput.checkSaturation();
        for (LinkFace link : sourceOutput.getLinks()) {
            link.getInput().checkSaturation();
        }
    }

    public boolean setValue(Object value)
    {
        if (debug)
            LOGGER.debug("set value");
        valueChanged = true;
        if (value == null) {
            this.value = null;
            checkSaturation();
            return true;
        }
        if (type.isInstance(value)) {
            this.value = value;
            checkSaturation();
            return true;
        }
        //LOGGER.debug("vns | type = "+type);
        //LOGGER.debug("vns | object = "+value);
        if (debug)
            LOGGER.debug("value not set;");
        checkSaturation();
        return false;
    }

    //    public VNData() {
    //        this(null, null, null, null);
    //    }
    //
    //    public VNData(ModuleBox module, Output output) {
    //        this(module, output, output.getType());
    //    }
    //
    public VNData(ModuleBoxFace module, Output output, Class type)
    {
        this(module, output, type, null);
    }

    public VNData(ModuleBoxFace module, Output output, Class type, Object value)
    {
        this.sourceModule = module;
        this.sourceOutput = output;
        this.type = type;
        if (value == null || !(type.isInstance(value)))
            this.value = null;
        else
            this.value = value;
        this.valueChanged = true;
    }

    //<editor-fold defaultstate="collapsed" desc=" Getters/Setters ">
    public boolean isValueChanged()
    {
        return valueChanged;
    }

    public void markValueOld()
    {
        valueChanged = false;
    }

    public Class getType()
    {
        return type;
    }

    //public void setType(Class type) {
    //    this.type = type;
    //}
    public ModuleBoxFace getSourceModule()
    {
        return sourceModule;
    }

    //public void setSourceModule(ModuleBox sourceModule) {
    //    this.sourceModule = sourceModule;
    //}
    public Output getSourceOutput()
    {
        return sourceOutput;
    }

    public void restart()
    {
        this.valueChanged = true;
    }

    //public void setSourceOutput(Output sourceOutput) {
    //    this.sourceOutput = sourceOutput;
    //}
    //</editor-fold>
}
