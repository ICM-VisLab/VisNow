/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import org.apache.log4j.Logger;
import org.visnow.vn.application.application.Application;
import org.visnow.vn.engine.core.LinkName;
import org.visnow.vn.engine.core.Link;
import org.visnow.vn.engine.element.Element;
import org.visnow.vn.engine.element.ElementKiller;
import org.visnow.vn.engine.element.ElementState;
import org.visnow.vn.engine.error.Displayer;
import org.visnow.vn.engine.exception.VNApplicationNetLoopException;
import org.visnow.vn.engine.exception.VNSystemEngineException;
import org.visnow.vn.engine.core.Input;
import org.visnow.vn.engine.main.ModuleBox;
import org.visnow.vn.engine.main.ModuleElement;
import org.visnow.vn.engine.main.ModuleSaturation;
import org.visnow.vn.engine.messages.Message;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class Engine extends Element implements ElementKiller
{

    private static final Logger LOGGER = Logger.getLogger(Engine.class);

    private static boolean debug = true;

    private ConcurrentHashMap<String, ModuleBox> modules;
    private ConcurrentHashMap<LinkName, Link> links;

    private boolean killed = false;

    public void kill()
    {
        killed = true;
        //VNLogger.log("KILLED!", this, "engine");
        //getApplication().stopProgress(); //TODO - progressBar
        getApplication().releaseAccess();
    }

    public void startModules()
    {
        for (ModuleBox module : this.getModules().values()) {
            module.run();
        }
    }

    public boolean isKilled()
    {
        return killed;
    }

    public boolean getPermission(ModuleElement element)
    {
        return true;
    }

    private int moduleNumber;

    public int nextModuleNumber()
    {
        ++moduleNumber;
        return moduleNumber - 1;
    }

    public ConcurrentHashMap<String, ModuleBox> getModules()
    {
        return modules;
    }

    public ConcurrentHashMap<LinkName, Link> getLinks()
    {
        return links;
    }

    public ModuleBox getModule(String name)
    {
        return modules.get(name);
    }

    public Link getLink(LinkName name)
    {
        return links.get(name);
    }

    private EngineCommandExecutor executor;
    private Application application;

    public EngineCommandExecutor getExecutor()
    {
        return executor;
    }

    public Application getApplication()
    {
        return application;
    }

    public Engine(Application application)
    {
        super("Engine");
        this.killer = this;
        this.modules = new ConcurrentHashMap<>();
        this.links = new ConcurrentHashMap<>();
        this.executor = new EngineCommandExecutor(this);
        this.application = application;
        this.moduleNumber = 1;
    }

    //    @Override
    //    public LinkedBlockingQueue<Message> getQueue() {
    //        VNLogger.log("MESSAGE PUT", this, "engine");
    //        return super.getQueue();
    //    }
    public boolean isNewActionPossible()
    {
        return true;
    }

    public boolean isModuleActionPossible()
    {
        return true;
    }

    @Override
    protected void onNotifyMessage(Message message) throws VNSystemEngineException
    {
    }

    @Override
    protected void onActionMessage(Message message) throws VNSystemEngineException
    {
    }

    @Override
    protected void onInactionMessage(Message message) throws VNSystemEngineException
    {
    }

    @Override
    protected void onKillMessage() throws VNSystemEngineException
    {
    }

    //private Message lastMessage;
    //protected Message getLastMessage() {
    //    return lastMessage;
    //}
    protected void gotException(VNSystemEngineException ex)
    {
        Displayer.display(200907100945L, ex, this, "Exception in engine queue.");
    }

    @Override
    public boolean nextMessage() throws InterruptedException, VNSystemEngineException
    {
        final Message lastMessage = takeFromQueue();//getQueue().take();
        //XXX: (szpak) why create many threads to dispach messages???
        //     probably locking mechanism is incorrectly designed as messages are not started in "correct" order and multiple engine sub-threads are started here.
        //     That was proably some kind of quick fix to prevent deadlock.
        new Thread(new Runnable()
        {
            public void run()
            {
                try {
//                    Message lastMessage = takeFromQueue(); //getQueue().take();

                    switch (lastMessage.getType()) {
                        //XXX: (szpak) apparently empty handlers
                        case Message.KILL:
                        case Message.NOTIFY:
                        case Message.ACTION:
                        case Message.INACTION:
//                    onKillMessage();
//                    onNotifyMessage(lastMessage);
//                    onInactionMessage(lastMessage);
//                    onActionMessage(lastMessage);
                            break;
                        case Message.READY:
                            onReadyMessage(lastMessage);
                            break;
                        case Message.DONE:
                            onDoneMessage(lastMessage);
                            break;
                        case Message.START_ACTION:
                            onStartActionMessage(lastMessage);
                            break;
                        case Message.START_ACTION_IF_NOT_IN_QUEUE:
                            onStartActionIfNotInQueueMessage(lastMessage);
                            break;
                        default:
//                            onOtherMessage(lastMessage);
//                            throw new IllegalStateException("Message " + lastMessage + " not supported");
                            LOGGER.warn("There should be no other message. Message: " + lastMessage);
                            break;
                    }
                } catch (VNSystemEngineException ex) {
                    gotException(ex);
//                }catch (InterruptedException exception) {
//                    LOGGER.error("Interrupted in takeFromQueue", exception);
                }

            }
        }, "VN-Engine-message").start();
        return true;
    }

    @Override
    protected void onOtherMessage(Message message) throws VNSystemEngineException
    {
        switch (message.getType()) {
            case Message.START_ACTION:
                onStartActionMessage(message);
                break;
//            case Message.START_ACTION_IF_NOT_IN_QUEUE:
//                onStartActionIfNotInQueueMessage(message);
//                break;
        }

    }

    private Semaphore lockup = new Semaphore(1, true);

    protected void onStartActionMessage(Message message)
    {
        try {
//            LOGGER.debug("ACQUIRING LOCK... " + message);
            lockup.acquire();
//            LOGGER.debug("ACQUIRING LOCK... SUCCESS " + message);
        } catch (InterruptedException e) {
            LOGGER.error("Interrupted while acquiring semaphore ", e);
            return;
        }

        try {
            getApplication().getAccess("Start Action (Engine-0) " + message.getSender().toString());
//            VNLogger.debugFlow(true, message.getSender());//.log("START! from module ["+message.getSender()+"]\n\n", this, "engine");
            killed = false;
            //            getApplication().getScene().getScenePanel().getProgress().init(); //TODO - progressBar
            ///XXX: (szpak) onWaveStarting(); not implemented
//            if (message.getSender() instanceof ModuleElement) {
//                try {
//                    ((ModuleElement) message.getSender()).getModuleBox().getCore().onWaveStarting();
//                } catch (Exception e) {
//                    Displayer.ddisplay(42, e, this,
//                                       "An error occured in function \"onWaveStarting\" " +
//                        "of module \"" + message.getSender().getName() + "\".\n" +
//                        "Please report this exception to the module core developer.\n" +
//                        "The application flow will be terminated."
//                    );
//                    killed = true;
//                    getApplication().releaseAccess();
//                    return;
//                }
//            }
            message.getSender().putToQueue(new Message(this, Message.NOTIFY));
            //if (debug) {
            //    System.out.println("sent message to " + message.getSender());
            //}
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

//    private int startActionID = 0;
    private final Set<Element> inQueueStartActionElements = new HashSet<Element>();

    protected void onStartActionIfNotInQueueMessage(Message message)
    {
        boolean inQueue = false;
        synchronized (inQueueStartActionElements) {
            if (inQueueStartActionElements.contains(message.getSender())) {
                inQueue = true;
            } else {
                inQueueStartActionElements.add(message.getSender());
        }
    }
        if (!inQueue) {
            try {
                LOGGER.debug("ACQUIRING LOCK...");
                lockup.acquire();
                LOGGER.debug("ACQUIRING LOCK... SUCCESS");
            } catch (InterruptedException e) {
                LOGGER.error("Interrupted while acquiring semaphore ", e);
                return;
            }

            try {
                getApplication().getAccess("Start Action (Engine-0)");
                inQueueStartActionElements.remove(message.getSender());
//            VNLogger.debugFlow(true, message.getSender());//.log("START! from module ["+message.getSender()+"]\n\n", this, "engine");
                killed = false;
                Message m = new Message(this, Message.NOTIFY);
//            LOGGER.debug(startActionID + " " + message.getSender() + " queue: " + message.getSender().toStringQueue());
                if (!message.getSender().isInQueue(m)) {
                    message.getSender().putToQueue(m);
        }
            } catch (InterruptedException ex) {
//            ex.printStackTrace();
                LOGGER.warn("Error while running start action", ex);
                //remove element just in case
                inQueueStartActionElements.remove(message.getSender());
    }
        }

        }

    @Override
    protected void onReadyMessage(Message message) throws VNSystemEngineException
    {
        try {
            //      getApplication().startProgress(); //TODO - progressBar
            message.getSender().putToQueue(new Message(this, Message.ACTION));
        } catch (InterruptedException ex) {
            throw new VNSystemEngineException(200910260000L, "Ready message propagation interrupted", ex, this, Thread.currentThread());
        }
    }

    @Override
    protected void onDoneMessage(Message message) throws VNSystemEngineException
    {
        //getApplication().stopProgress(); //TODO - progressBar
//        VNLogger.debugFlow(false, message.getSender());//.log("FINISH!\n\n", this, "engine");
        //XXX: (szpak) onWaveFinalizing() apparently empty
//        if (message.getSender() instanceof ModuleElement) {
//            try {
//                ((ModuleElement) message.getSender()).getModuleBox().getCore().onWaveFinalizing();
//            } catch (Exception e) {
//                Displayer.ddisplay(42, e, this,
//                                   "An error occured in function \"onWaveFinalizing\" " +
//                                   "of module \"" + message.getSender().getName() + "\".\n" +
//                                   "Please report this exception to the module core developer.\n" +
//                                   "The application flow will be terminated."
//                );
//                killed = true;
//                getApplication().releaseAccess();
//                return;
//            }
//        }
        getApplication().releaseAccess();
        lockup.release();
//        LOGGER.debug("RELEASE");
        // TODO: finish action

        engineSaturationCheck();
    }

    private void onFinishActionMessage(Message message)
    {

    }

    public void unkill()
    {
        killed = false;
    }

    public Collection<ModuleBox> getTopologicalModules() throws VNApplicationNetLoopException
    {
        //if(true) return this.getModules().values();

        Vector<ModuleBox> ret = new Vector<ModuleBox>();
        HashMap<ModuleBox, ModuleNode> nodes = new HashMap<ModuleBox, ModuleNode>();
        for (ModuleBox m : this.getModules().values()) {
            nodes.put(m, new ModuleNode(m));
        }
        for (Map.Entry<ModuleBox, ModuleNode> en : nodes.entrySet()) {
            Iterator<Link> i = en.getKey().iterator(false, true);
            while (i.hasNext()) {
                en.getValue().edges.add(nodes.get(i.next().getInput().getModuleBox()));
            }
        }

        Vector<ModuleNode> ready = new Vector<ModuleNode>();
        for (ModuleNode mn : nodes.values()) {
            if (mn.inValue == 0) {
                ready.add(mn);
            }
        }
        while (!ready.isEmpty()) {
            ModuleNode next = ready.remove(ready.size() - 1);
            ret.add(next.node);
            for (ModuleNode mn : next.edges) {
                mn.inValue--;
                if (mn.inValue == 0) {
                    ready.add(mn);
                }
            }
        }
        if (ret.size() < nodes.size()) {
            throw new VNApplicationNetLoopException();
        }

        return ret;
    }

    void updateLinkName(LinkName old, LinkName ln)
    {
        Link l = links.get(old);
        links.remove(old);
        links.put(ln, l);
    }

    void updateModuleName(String name, String newName)
    {
        ModuleBox mb = modules.get(name);
        modules.remove(name);
        modules.put(newName, mb);
    }

    public Thread doTheMainReset(Thread oldThread)
    {

        this.setElementState(ElementState.passive);
        this.clearQueue();
        oldThread.interrupt();

        for (ModuleBox module : getModules().values()) {
            module.doTheMainResetKillAll();
        }

        killed = false;
        if (lockup.availablePermits() <= 0) {
            lockup.release();
        }

        for (ModuleBox module : getModules().values()) {
            module.doTheMainResetWakeUp();
        }

        return new Thread(this, "VN-Engine-new");
    }

    public void correctModuleCount(int c)
    {
        if (moduleNumber <= c) {
            moduleNumber = c + 1;
        }
    }

    public void engineSaturationCheck()
    {
        Iterator<Entry<String, ModuleBox>> it = modules.entrySet().iterator();
        boolean wrongData = false;
        boolean noData = false;
        while (it.hasNext()) {
            Entry<String, ModuleBox> entry = it.next();
            ModuleSaturation sat = entry.getValue().getElement().getSaturation();
            if (!wrongData && sat == ModuleSaturation.wrongData) {
                wrongData = true;
            }
            if (!noData && sat == ModuleSaturation.noData) {
                noData = true;
            }
        }

        if (!wrongData && !noData) {
            application.setStatus(Application.ApplicationStatus.OK);
        } else if (wrongData) {
            application.setStatus(Application.ApplicationStatus.ERROR);
        } else if (noData) {
            application.setStatus(Application.ApplicationStatus.WARNING);
        }

    }

}

class ModuleNode
{

    protected ModuleBox node;
    protected int inValue;

    public ModuleNode(ModuleBox box)
    {
        this.node = box;
        int i = 0;
        for (Input inp : box.getInputs().getInputs().values()) {
            i += inp.getLinks().size();
        }
        this.inValue = i;
        this.edges = new Vector<ModuleNode>();
    }

    protected Vector<ModuleNode> edges;
}
