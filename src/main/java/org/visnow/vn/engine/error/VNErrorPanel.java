/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.error;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import org.visnow.vn.engine.exception.VNException;
import org.visnow.vn.engine.exception.VNRuntimeException;

/**
 *
 * @author gacek
 */
public class VNErrorPanel extends javax.swing.JPanel
{

    private Exception e;

    public Exception getException()
    {
        return e;
    }

    /**
     * Creates new form VNErrorPanel
     */
    public VNErrorPanel(VNException e)
    {
        initComponents();
        this.e = e;
        typeField.setText(e.getClass().getSimpleName());
        codeField.setText("" + e.getCode());
        messageArea.setText(e.getMessage());
        throwerField.setText((e.getThrower() == null) ? "null" : e.getThrower().toString());
        threadField.setText((e.getThread() == null) ? "null" : e.getThread().getName());
        detailsArea.setText((e.getDetails() == null) ? "" : e.getDetails().toString());
    }

    public VNErrorPanel(VNRuntimeException e)
    {
        initComponents();
        this.e = e;
        typeField.setText(e.getClass().getSimpleName());
        codeField.setText("" + e.getCode());
        messageArea.setText(e.getMessage());
        throwerField.setText((e.getThrower() == null) ? "null" : e.getThrower().toString());
        threadField.setText((e.getThread() == null) ? "null" : e.getThread().getName());
        detailsArea.setText((e.getDetails() == null) ? "" : e.getDetails().toString());
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new JLabel();
        jLabel2 = new JLabel();
        jScrollPane1 = new JScrollPane();
        messageArea = new JTextArea();
        typeField = new JTextField();
        jLabel3 = new JLabel();
        throwerField = new JTextField();
        jLabel4 = new JLabel();
        threadField = new JTextField();
        jLabel5 = new JLabel();
        codeField = new JTextField();
        jLabel6 = new JLabel();
        jScrollPane2 = new JScrollPane();
        detailsArea = new JTextArea();

        jLabel1.setHorizontalAlignment(SwingConstants.RIGHT);
        jLabel1.setText("type:");

        jLabel2.setHorizontalAlignment(SwingConstants.RIGHT);
        jLabel2.setText("message:");

        messageArea.setColumns(20);
        messageArea.setEditable(false);
        messageArea.setRows(5);
        jScrollPane1.setViewportView(messageArea);

        typeField.setEditable(false);

        jLabel3.setHorizontalAlignment(SwingConstants.RIGHT);
        jLabel3.setText("thrower:");

        throwerField.setEditable(false);

        jLabel4.setHorizontalAlignment(SwingConstants.RIGHT);
        jLabel4.setText("thread:");

        threadField.setEditable(false);

        jLabel5.setHorizontalAlignment(SwingConstants.RIGHT);
        jLabel5.setText("code:");

        codeField.setEditable(false);

        jLabel6.setHorizontalAlignment(SwingConstants.RIGHT);
        jLabel6.setText("details:");

        detailsArea.setColumns(20);
        detailsArea.setEditable(false);
        detailsArea.setRows(2);
        jScrollPane2.setViewportView(detailsArea);

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(Alignment.LEADING)
                                        .addComponent(jLabel6, GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE)
                                        .addGroup(Alignment.TRAILING, layout.createParallelGroup(Alignment.TRAILING, false)
                                                .addComponent(jLabel1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabel2, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabel5, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addComponent(jLabel3, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE)
                                        .addComponent(jLabel4, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE))
                                .addPreferredGap(ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(Alignment.LEADING)
                                        .addComponent(jScrollPane2, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
                                        .addComponent(throwerField, GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
                                        .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
                                        .addComponent(typeField, GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
                                        .addComponent(threadField, GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
                                        .addComponent(codeField, GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE))
                                .addContainerGap())
                );
        layout.setVerticalGroup(
                layout.createParallelGroup(Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                                        .addComponent(jLabel1)
                                        .addComponent(typeField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                                        .addComponent(jLabel5)
                                        .addComponent(codeField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(Alignment.LEADING)
                                        .addComponent(jLabel2)
                                        .addComponent(jScrollPane1))
                                .addPreferredGap(ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                                        .addComponent(throwerField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel3))
                                .addPreferredGap(ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                                        .addComponent(threadField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel4))
                                .addPreferredGap(ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(Alignment.LEADING)
                                        .addComponent(jScrollPane2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel6))
                                .addContainerGap())
                );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JTextField codeField;
    private JTextArea detailsArea;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JTextArea messageArea;
    private JTextField threadField;
    private JTextField throwerField;
    private JTextField typeField;
    // End of variables declaration//GEN-END:variables

}
