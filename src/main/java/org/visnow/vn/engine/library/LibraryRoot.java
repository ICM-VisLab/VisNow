/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.library;

import java.util.HashMap;
import java.util.Vector;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.exception.VNException;
import org.visnow.vn.lib.types.VNDataAcceptor;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public interface LibraryRoot
{

    public final static int INTERNAL = 0;
    public final static int JAR = 1;
    public final static int XML = 2;
    public final static int OTHER = 254;
    public final static int NONE = 255;

    public abstract int getType();

    public abstract String getFilePath();

    public abstract String getName();

    public abstract LibraryFolder getRootFolder();

    public abstract ModuleCore loadCore(String className) throws VNException;

    public abstract HashMap<String, String> getInputTypes(String className);

    public abstract TypesMap getTypesMap();

    public abstract HashMap<String, VNDataAcceptor[]> getInputVNDataAcceptors(String className);

    public abstract Vector<LibraryCore> getAllCores();

}
