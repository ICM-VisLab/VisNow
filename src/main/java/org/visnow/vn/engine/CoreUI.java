/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine;

import org.visnow.vn.engine.core.ParameterProxy;

/**
 * This class is a simple try to make better (yet very simple) abstraction between user interfaces and computational core of modules.
 * User interfaces considered here are: standard GUI, Viewers as interface to modules, batch mode.
 * <p>
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public interface CoreUI
{

    /**
     * This method makes (possible) connection between UI and computational core of module.
     * @param targetParameterProxy 
     */
    public void setParameterProxy(ParameterProxy targetParameterProxy);

    /**
     * XXX: This should be resolved in some other way - possibly also by using parameterProxy - to avoid direct calls from logic to UI (and vice versa)
     * 
     * This is update method for UI - needed for consistency between module state and UI state.
     * @param p (read only) parameter proxy necessary for update - main assumption here is that parameters within this parameter proxy are correct, consistent 
     * and can be (and should be) pushed directly into corresponding GUI controls
     * @param resetGUIOnlyControls if true than controls that are not directly bound to module parameters should be reset as well 
     * (such controls may be considered as additional 'GUI parameters')
     * @param setRunButtonPending if true than run button should be set into pending state (as when new input field arrives)
     */
    public void updateGUI(final ParameterProxy p, boolean resetGUIOnlyControls, boolean setRunButtonPending);

}
