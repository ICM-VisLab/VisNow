/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.main;

import org.visnow.vn.engine.core.Output;
import org.visnow.vn.engine.Engine;
import org.visnow.vn.engine.core.OutputEgg;
import org.visnow.vn.engine.core.VNData;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class DataModule extends ModuleBox
{

    private Output output;

    //TODO: Output nie powinien być normalnym elementem.
    //Po pierwsze, tworzy niepotrzebny wątek i kolejkę.
    //Po drugie, musimy obchodzić cały system podłączający output do moduleBox i engine.
    //Może Output powinien być interfejsem?
    public DataModule(Engine engine, String name, VNData data)
    {
        super(engine, name, new DataCore());
        //        this.data = data;
        this.output = new Output(new OutputEgg("data", data.getType()));
        output.setModuleBox(this);
        output.setValue(data.getValue());
        ((DataCore) this.getCore()).init(this);
    }

    public Output getOutput()
    {
        return output;
    }
}
