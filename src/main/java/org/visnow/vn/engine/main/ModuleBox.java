/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.engine.main;

import org.visnow.vn.engine.core.Output;
import org.visnow.vn.engine.core.Link;
import org.visnow.vn.engine.core.Input;
import java.util.Iterator;
import java.util.Vector;
import org.apache.log4j.Logger;
import org.visnow.vn.engine.core.Inputs;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.core.Outputs;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.engine.Engine;
import org.visnow.vn.engine.core.ModuleBoxFace;
import org.visnow.vn.engine.messages.Message;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class ModuleBox extends ModuleBoxFace implements Iterable<Link>
{
    private static final Logger LOGGER = Logger.getLogger(ModuleBox.class);

    private static boolean debug = false;

    //    private ModuleSaturation saturation;
    //    public ModuleSaturation getSaturation() {
    //        return saturation;
    //    }
    //<editor-fold defaultstate="collapsed" desc=" [VAR] Name | Engine | internal blocks ">
    private String name;
    private Engine engine;
    private ModuleElement element;
    private ModuleCore core;

    //private Inputs inputs;
    //private Outputs outputs;
    //private Parameters parameters;
    @Override
    public String getName()
    {
        return name;
    }

    /**
     * @return the application
     */
    public Engine getEngine()
    {
        return engine;
    }

    /**
     * @return the inputs
     */
    public Inputs getInputs()
    {
        return core.getInputs();
    }

    /**
     * @return the outputs
     */
    public Outputs getOutputs()
    {
        return core.getOutputs();
    }

    /**
     * @return the parameters
     */
    public Parameters getParameters()
    {
        return core.getParameters();
    }

    /**
     * @return the element
     */
    public ModuleElement getElement()
    {
        return element;
    }

    /**
     * @return the core
     */
    public ModuleCore getCore()
    {
        return core;
    }

    public Input getInput(String name)
    {
        return core.getInputs().getInput(name);
    }

    public Output getOutput(String name)
    {
        return core.getOutputs().getOutput(name);
    }

    public Port getPort(boolean isInput, String name)
    {
        if (isInput)
            return getInput(name);
        return getOutput(name);
    }

    //</editor-fold>
    @SuppressWarnings("static-access")
    public ModuleBox(Engine engine, String name, ModuleCore core)
    {
        this.engine = engine;
        this.name = name;
        this.core = core;
        if (core != null) {
            this.element = new ModuleElement(this);
            core.setModuleBoxEgg(this);
            this.element.checkSaturation();
        }
    }

    Thread elementThread;

    public void doTheMainResetKillAll()
    {
        this.element.doTheMainResetKillAll();
        elementThread.interrupt();

        for (Input input : getInputs()) {
            input.doTheMainResetKillAll();
        }
        for (Output output : getOutputs()) {
            output.doTheMainResetKillAll();
        }

        for (Thread t : portThreads)
            t.interrupt();

        portThreads.clear();
    }

    public void doTheMainResetWakeUp()
    {
        run();
    }

    private Vector<Thread> portThreads = new Vector<Thread>();

    //public Vector<Thread> getThreads() {return threads;}
    public void run()
    {

        if (debug)
            System.out.println("run {" + this + "}");
        elementThread = new Thread(getElement(), "VNM-" + this.getName());
        elementThread.start();
        //threads.add(elementThread);
        for (Input input : getInputs()) {
            Thread t = new Thread(input, "VNI-" + input.getName());
            t.start();
            portThreads.add(t);
        }
        for (Output output : getOutputs()) {
            Thread t = new Thread(output, "VNO-" + output.getName());
            t.start();
            portThreads.add(t);
        }

    }

    public Iterator<Link> iterator()
    {
        return new ModuleLinkIterator(this);
    }

    public Iterator<Link> iterator(boolean in, boolean out)
    {
        return new ModuleLinkIterator(this, in, out);
    }

    @Override
    public void startAction()
    {
        try {
            if (getEngine() == null)
                return;

            getEngine().putToQueue(new Message(this.getElement(), Message.START_ACTION));
            //this.getElement().getQueue().put(new Message(null, Message.START_ACTION));
        } catch (InterruptedException ex) {
            LOGGER.error(ex);
        }
    }

//    private final Object engineQueueLocalLock = new Object();
//  
    @Override
    public void startIfNotInQueue()
    {
        LOGGER.debug("");
        try {
            Message message = new Message(this.getElement(), Message.START_ACTION_IF_NOT_IN_QUEUE);
//            LOGGER.debug(getEngine().isInQueue(message));

//            synchronized (engineQueueLocalLock) {
            //if not in queue
//                LOGGER.debug("engine queue: " + getEngine().toStringQueue());
//                LOGGER.debug(getElement() + " queue: " + getEngine().toStringQueue());
            if (getEngine() != null)//) && !getEngine().isInQueue(message))
                    getEngine().putToQueue(message);
//            }
        } catch (InterruptedException ex) {
            LOGGER.error(ex);
        }
    }

    public void setProgress(float progress)
    {
        getEngine()
                .getApplication()
                .getArea()
                .getInput()
                .setModuleProgress(this.getName(), progress);
    }

    public void updateName(String newName)
    {
        this.name = newName;
        fireModuleBoxFaceNameChanged();
    }

    public boolean hasIndirectDownstreamConnectionTo(ModuleBox mod)
    {
        if (mod == null)
            return false;

        if (mod == this)
            return true;

        Iterator<Output> outs = getOutputs().iterator();
        Output out;
        Vector<Link> links;
        boolean tmp;
        while (outs.hasNext()) {
            out = outs.next();
            links = out.getLinks();
            for (int i = 0; i < links.size(); i++) {
                tmp = links.get(i).getInput().getModuleBox().hasIndirectDownstreamConnectionTo(mod);
                if (tmp)
                    return true;
            }
        }
        return false;
    }

    public boolean hasIndirectUpstreamConnectionTo(ModuleBox mod)
    {
        if (mod == null)
            return false;

        if (mod == this)
            return true;

        Iterator<Input> ins = getInputs().iterator();
        Input in;
        Vector<Link> links;
        boolean tmp;
        while (ins.hasNext()) {
            in = ins.next();
            links = in.getLinks();
            for (int i = 0; i < links.size(); i++) {
                tmp = links.get(i).getOutput().getModuleBox().hasIndirectUpstreamConnectionTo(mod);
                if (tmp)
                    return true;
            }
        }
        return false;
    }

}
