/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.main;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import static org.visnow.vn.system.main.VisNow.getOsType;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class VisNowJava3DInit
{

    private static final Logger LOGGER = Logger.getLogger(VisNowJava3DInit.class);

    @SuppressWarnings({"unchecked"})
    public static void initJava3D(boolean debug, Logger logger) 
    {

        //org.jogamp.java3d.VirtualUniverse vu = new org.jogamp.java3d.VirtualUniverse();
        Map<String, String> vuMap = org.jogamp.java3d.VirtualUniverse.getProperties();
        Set<String> vuKeys = vuMap.keySet();
        Iterator<String> vuKeysI = vuKeys.iterator();

        logger.info("-------- Java3D startup info --------");
        logger.info(" * Virtual Universe properties:");

        if (debug) {
            String key;
            while (vuKeysI.hasNext()) {
                key = vuKeysI.next();
                logger.info("    " + key + " = " + vuMap.get(key));
            }         
        } else {
            logger.info("    version = " + vuMap.get("j3d.version"));
            logger.info("    vendor = " + vuMap.get("j3d.vendor"));
            logger.info("    specification.version = " + vuMap.get("j3d.specification.version"));
            logger.info("    specification.vendor = " + vuMap.get("j3d.specification.vendor"));
            logger.info("    renderer = " + vuMap.get("j3d.renderer"));
        }


        if (debug) {
            org.jogamp.java3d.GraphicsConfigTemplate3D template = new org.jogamp.java3d.GraphicsConfigTemplate3D();
            template.setStereo(org.jogamp.java3d.GraphicsConfigTemplate3D.PREFERRED);
            template.setSceneAntialiasing(org.jogamp.java3d.GraphicsConfigTemplate3D.PREFERRED);

            GraphicsConfiguration gcfg = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getBestConfiguration(template);
            org.jogamp.java3d.Canvas3D c3d = new org.jogamp.java3d.Canvas3D(gcfg);
            Map c3dMap = c3d.queryProperties();
            Set<String> c3dMapKeys = c3dMap.keySet();
            Iterator<String> c3dMapKeysI = c3dMapKeys.iterator();

            logger.info(" * Canvas3D properties:");
            String key;
            while (c3dMapKeysI.hasNext()) {
                key = c3dMapKeysI.next();
                logger.info("    " + key + " = " + c3dMap.get(key));
            }
            logger.info("");
        }
        logger.info("------------------------------------------");
        logger.info("");
    }

    private VisNowJava3DInit()
    {
    }

}
