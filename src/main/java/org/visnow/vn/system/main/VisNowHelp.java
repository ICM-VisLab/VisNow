/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.visnow.vn.application.application.Application;
import org.visnow.vn.application.area.Area;
import org.visnow.vn.application.area.widgets.ModulePanel;
import org.visnow.vn.engine.core.CoreName;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.exception.VNException;
import org.visnow.vn.engine.main.ModuleBox;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class VisNowHelp
{

    public static void main(String[] args)
    {
        //String moduleClass = "org.visnow.vn.lib.basic.mappers.Isosurface.Isosurface";
        //String moduleName = "isosurface [0]";

        String moduleClass = "org.visnow.vn.lib.basic.viewers.Viewer3D.Viewer3D";
        String moduleName = "viewer 3d [0]";

        BufferedImage img = null;
        HashMap<String, Point> inputHandles = new HashMap<String, Point>();
        HashMap<String, Point> outputHandles = new HashMap<String, Point>();

        img = renderModuleBox(moduleName, moduleClass, inputHandles, outputHandles);

        showImage(img, inputHandles, outputHandles);

    }

    private static void showImage(final BufferedImage img, final HashMap<String, Point> inputHandles, final HashMap<String, Point> outputHandles)
    {
        JFrame frame = new JFrame("image");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(0, 0, 640, 480);
        frame.setLayout(new BorderLayout());

        JPanel panel = new JPanel()
        {

            @Override
            public void paint(Graphics g)
            {
                Graphics2D g2d = (Graphics2D) g;
                g2d.setPaint(Color.WHITE);
                g2d.fillRect(0, 0, getWidth(), getHeight());

                if (img == null)
                    return;

                int xpos = (getWidth() - img.getWidth()) / 2;
                int ypos = (getHeight() - img.getHeight()) / 2;
                g2d.drawImage(img, null, xpos, ypos);

                String[] inputPorts = inputHandles.keySet().toArray(new String[0]);
                for (int i = 0; i < inputHandles.size(); i++) {
                    Point p = (Point) inputHandles.get(inputPorts[i]);
                    g2d.setPaint(Color.MAGENTA);
                    //g2d.fillOval((int) (xpos + p.getX() - 2), (int) (ypos + p.getY() - 2), 4, 4);                    
                    g2d.fillRect((int) (xpos + p.getX()), (int) (ypos + p.getY()), 1, 1);
                }

                String[] outputPorts = outputHandles.keySet().toArray(new String[0]);
                for (int i = 0; i < outputHandles.size(); i++) {
                    Point p = (Point) outputHandles.get(outputPorts[i]);
                    g2d.setPaint(Color.MAGENTA);
                    //g2d.fillOval((int) (xpos + p.getX() - 2), (int) (ypos + p.getY() - 2), 4, 4);
                    g2d.fillRect((int) (xpos + p.getX()), (int) (ypos + p.getY()), 1, 1);
                }
            }
        };
        panel.setPreferredSize(new Dimension(640, 480));

        frame.add(panel, BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);

    }

    public static BufferedImage renderModuleBox(String moduleName, String moduleClass, HashMap<String, Point> inputHandles, HashMap<String, Point> outputHandles)
    {
        if (moduleClass == null) {
            return null;
        }

        String[] args = new String[0];
        VisNow.mainBlocking(args, false);
        VisNow vn = VisNow.get();

        BufferedImage img = null;
        Application app = vn.getMainWindow().getApplicationsPanel().getCurrentApplication();
        Area ai = app.getArea().getInput().getArea();
        ModuleCore core = null;
        ModulePanel mp = null;
        String[] inputPorts;
        String[] outputPorts;

        try {
            core = app.getLibraries().generateCore(new CoreName("internal", moduleClass));

            inputPorts = core.getInputs().getInputs().keySet().toArray(new String[0]);
            outputPorts = core.getOutputs().getOutputs().keySet().toArray(new String[0]);

        } catch (VNException ex) {
            System.err.println("ERROR: cannot create module " + moduleClass);
            return null;
        }

        ModuleBox mb = new ModuleBox(app.getEngine(), moduleName, core);
        mp = new ModulePanel(ai.getPanel(), mb);

        int w = mp.getWidth();
        int h = mp.getHeight();

        mp.setPreferredSize(new Dimension(w, h));
        JPanel tmpPanel = new JPanel(new BorderLayout());
        tmpPanel.setPreferredSize(mp.getPreferredSize());
        tmpPanel.add(mp, BorderLayout.CENTER);

        JFrame tmpFrame = new JFrame("tmp");
        tmpFrame.setLayout(new BorderLayout());
        tmpFrame.add(tmpPanel, BorderLayout.CENTER);
        tmpFrame.setBounds(0, 0, 480, 320);
        tmpFrame.setVisible(true);

        img = new BufferedImage(w - 12, h - 12, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = img.createGraphics();
        g2d.translate(-6, -6);
        g2d.setPaint(Color.BLACK);
        g2d.fillRect(0, 0, w - 12, h - 12);
        mp.paintAll(g2d);
        img.flush();

        tmpFrame.dispose();

        if (inputPorts != null) {
            for (int i = 0; i < inputPorts.length; i++) {
                inputHandles.put(inputPorts[i], new Point(16 + 24 * i, 0));
            }
        }

        if (outputPorts != null) {
            for (int i = 0; i < outputPorts.length; i++) {
                outputHandles.put(outputPorts[i], new Point(16 + 24 * i, h - 13));
            }
        }

        return img;
    }

    private VisNowHelp()
    {
    }
}
