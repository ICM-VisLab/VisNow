/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

//UTIL
/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class FileLinesIterator implements Iterator<String>
{

    private final FileReader reader;
    private String next;
    private boolean finished;

    public FileLinesIterator(File file) throws FileNotFoundException
    {
        reader = new FileReader(file);
        finished = false;
        next = null;
    }

    private void getNext()
    {
        try {
            String ret = "";
            int r = reader.read();
            while (r != -1 && r != '\n') {
                ret += (char) r;
                r = reader.read();
            }
            if (r == -1) {
                finished = true;
                reader.close();
                if (ret.length() == 0) {
                    next = null;
                    return;
                }
            }
            next = ret;
        } catch (IOException e) {
            next = null;
            finished = true;
        }
    }

    @Override
    public boolean hasNext()
    {
        if (finished)
            return false;
        if (next == null)
            getNext();
        if (next == null)
            return false;
        return true;
    }

    @Override
    public String next()
    {
        if (next == null) {
            if (finished)
                return null;
            getNext();
        }
        String ret = next;
        next = null;
        return ret;
    }
    
    @Override
    public void remove() {
    }    
}
