/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.framework;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Point;
import java.io.File;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.visnow.vn.application.application.Application;
import org.visnow.vn.application.frames.tabs.ModuleAdder;
import org.visnow.vn.application.io.VNReader;
import org.visnow.vn.application.io.XMLReader;
import org.visnow.vn.engine.error.Displayer;
import org.visnow.vn.engine.exception.VNOuterException;
import org.visnow.vn.engine.library.LibraryCore;
import org.visnow.vn.engine.library.LibraryFolder;
import org.visnow.vn.lib.utils.SwingInstancer;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.swing.filechooser.VNFileChooser;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class MainMenu
{

    //<editor-fold defaultstate="collapsed" desc=" [VAR] window, application ">
    protected MainWindow window;

    protected Application getApplication()
    {
        return window.getApplicationsPanel().getCurrentApplication();
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" [VAR] Untitled count ">
    private int untitledCount;

    public int getUntitledCount()
    {
        return untitledCount;
    }

    public void setUntitledCount(int untitledCount)
    {
        this.untitledCount = untitledCount;
    }

    public int nextUntitled()
    {
        this.untitledCount = untitledCount + 1;
        return untitledCount - 1;
    }

    //</editor-fold>
    protected MainMenu(MainWindow window)
    {
        this.window = window;
        this.untitledCount = 1;
    }

    //<editor-fold defaultstate="collapsed" desc=" FileFilters ">
    private final static FileNameExtensionFilter vnaFilter = new FileNameExtensionFilter("VisNow Application", "vna", "VNA");

    private final static FileFilter vnfFilter = new FileFilter()
    {
        @Override
        public boolean accept(File f)
        {
            if (f.isDirectory())
                return true;
            if (!f.isFile())
                return false;
            return (f.getName().toLowerCase().endsWith(".vnf"));
        }

        @Override
        public String getDescription()
        {
            return "VisNow application data file ( .vnf)";
        }
    };

    private final static FileFilter vnFilter = new FileFilter()
    {
        @Override
        public boolean accept(File f)
        {
            if (f.isDirectory())
                return true;
            if (!f.isFile())
                return false;
            if (f.getName().toLowerCase().endsWith(".vna"))
                return true;
            if (f.getName().toLowerCase().endsWith(".vnf"))
                return true;
            if (f.getName().toLowerCase().endsWith(".vns"))
                return true;
            return false;
        }

        @Override
        public String getDescription()
        {
            return "All VisNow Applications ( .vna, .vnf, .vns)";
        }
    };

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" File ">
    protected void newApplication()
    {
        window.getApplicationsPanel().addApplication(new Application("Untitled(" + nextUntitled() + ")", false));
    }

    protected void openApplication()
    {
        //        try {
        VNFileChooser chooser = new VNFileChooser(); /* TODO: foxtrot file chooser */

        //chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.addFileFilter(vnFilter);
        chooser.addFileFilter(vnaFilter);
        chooser.addFileFilter(vnfFilter);
        chooser.setFileFilter(vnFilter);
        //            chooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getVN2UserFolder()));
        boolean chosen = chooser.showDialog();//window);
        if (chosen) {
            File file = chooser.getSelectedFile();
            Application application;
            try {
                application = XMLReader.readXML(chooser.getSelectedFile());
                // TODO: Zamienic na wersje tablicowa - dodac obsluge tablicy wywolujaca odczytywanie pojedynczych plikow.
                /* TODO: load application */
                // add Application
            } catch (VNOuterException ex) {
                Displayer.display(200907100601L, ex, this, "Could not read application from XML.");
                return;
            }

            window.getApplicationsPanel().addApplication(application);
        }

    }

    public void openApplication(File file)
    {
        try {
            Application application = XMLReader.readXML(file);
            window.getApplicationsPanel().addApplication(application);
        } catch (VNOuterException ex) {
            Displayer.display(200907311400L, ex, this, "Could not read application from XML.");
        }
    }

    protected boolean saveApplication()
    {
        //        if(window.getApplicationsPanel().getApplication().getFilePath() == null)
        //            return saveAsApplication();
        //        VisNow.get().getMainConfig().addRecentApplication(window.getApplicationsPanel().getApplication().getFilePath());
        //        return window.getApplicationsPanel().getApplication().save();

        return betaSave();
    }

    protected boolean saveAsApplication()
    {
        //        JFileChooser chooser = new JFileChooser(); /* TODO: foxtrot file chooser */
        //        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        //        chooser.addChoosableFileFilter(vnFilter);
        //        chooser.addChoosableFileFilter(vnaFilter);
        //        chooser.addChoosableFileFilter(vnfFilter);
        //        chooser.setFileFilter(vnFilter);
        //        chooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getVN2UserFolder()));
        //        int fileInt = chooser.showSaveDialog(window);
        //
        VNFileChooser chooser = new VNFileChooser(); /* TODO: foxtrot file chooser */

        //chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.addFileFilter(vnFilter);
        chooser.addFileFilter(vnaFilter);
        chooser.addFileFilter(vnfFilter);
        chooser.setFileFilter(vnFilter);
        //chooser.setCurrentDirectory(new File(VisNow.get().getMainConfig().getVN2UserFolder()));
        boolean chosen = chooser.showDialog();

        if (chosen) {
            return window.getApplicationsPanel().getCurrentApplication().saveAs(chooser.getSelectedFile());
        }
        return false;
    }

    protected void readTemplate()
    {

    }

    protected void closeApplication()
    {
        window.getApplicationsPanel().removeCurrentApplication();
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Edit ">
    protected void undo()
    {

    }

    protected void redo()
    {

    }

    protected void showHistory()
    {

    }

    protected void delete()
    {
        getApplication().deleteSelected();
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Action ">
    protected void interrupt()
    {

    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Help ">
    protected void about()
    {
        JOptionPane.showMessageDialog(
                null,
                VisNow.TITLE + " v" + VisNow.getVersionNumber()  + "\n\nCopyright 2014-2019 ICM University of Warsaw\nCopyright 2020 onward visnow.org",
                VisNow.TITLE + " v" + VisNow.getVersionNumber(),
                JOptionPane.INFORMATION_MESSAGE,
                new ImageIcon(getClass()
                        .getResource("/org/visnow/vn/gui/icons/big/visnow.png"))
        );
    }

    protected void help()
    {
        VisNow.get().showHelp("top");
    }

    //</editor-fold>
    void clearState()
    {
        window.getApplicationsPanel().getCurrentApplication().doTheMainReset();
    }

    JFileChooser chooser = new JFileChooser(VisNow.get().getMainConfig().getUsableApplicationsPath());

    boolean betaSave()
    {
        if (window.getApplicationsPanel().getCurrentApplication() != null) {
            chooser.setFileFilter(new FileNameExtensionFilter("VisNow network file", "vna", "VNA"));
            int result = chooser.showSaveDialog(window);

            if (result == JFileChooser.APPROVE_OPTION) {
                File file = VNFileChooser.fileWithExtensionAddedIfNecessary(chooser.getSelectedFile(),
                                                                            vnaFilter);
                boolean overwriteFile = true;
                if (file.exists() && JOptionPane.YES_OPTION != JOptionPane.showConfirmDialog(window, "File " + file.getAbsolutePath() + " already exists. Overwrite?", "Overwrite file?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE))
                    overwriteFile = false;
                if (overwriteFile) {
                    VisNow.get().getMainConfig().setLastApplicationsPath(file.getAbsolutePath());
                    String newAppName = file.getName();
                    //trim extension (string part after dot ".") 
                    if (newAppName.lastIndexOf('.') > 0) //but only if there is anything else (so no trimming for filenames like ".xyz")
                        newAppName = newAppName.substring(0, newAppName.lastIndexOf('.'));
                    window.getApplicationsPanel().getCurrentApplication().setTitle(newAppName);
                    return window.getApplicationsPanel().getCurrentApplication().betaSaveAs(file);
                } else return false;
            } else return false;
        } else return false;
    }

    void betaOpen()
    {
        chooser.setMultiSelectionEnabled(false);
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.setFileFilter(new FileNameExtensionFilter("VisNow network file", "vna", "VNA"));
        int result = chooser.showOpenDialog(window);
        if (result == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile();

            if (!file.exists()) file = new File(file.getAbsolutePath() + "." + vnaFilter.getExtensions()[0]);
            betaOpenFile(file);
        }
    }

    public void betaOpenFile(final File file)
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                final Application application = VNReader.readApplication(file);
                if (application != null) {
                    VisNow.get().getMainConfig().setLastApplicationsPath(file.getAbsolutePath());
                    SwingInstancer.swingRunLater(() -> {
                        window.getApplicationsPanel().addApplication(application);
                    });
                } else {
                    JOptionPane.showMessageDialog(window, "ERROR: cannot load VNA application\n" + file.getAbsolutePath(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        },"VN-READ-APPLICATION-B").start();
    }

    public ArrayList<Component> getReadersMenu(Point p)
    {
        ArrayList<Component> readersList = new ArrayList<Component>();
        LibraryFolder rootFolder = VisNow.get().getMainLibraries().getInternalLibrary().getRootFolder();
        processLibraryFolderForReaders(rootFolder, readersList, p);

        if (readersList.isEmpty())
            return null;

        if (readersList.get(readersList.size() - 1) instanceof JSeparator) {
            readersList.remove(readersList.size() - 1);
        }
        return readersList;
    }

    private boolean processLibraryFolderForReaders(LibraryFolder folder, ArrayList<Component> readersList, final Point p)
    {
        Vector<LibraryFolder> folders = folder.getSubFolders();
        Vector<LibraryCore> cores = folder.getCores();
        for (int i = 0; i < folders.size(); i++) {
            boolean added = processLibraryFolderForReaders(folders.get(i), readersList, p);
            if (added)
                readersList.add(new JSeparator());
        }

        boolean added = false;
        for (int i = 0; i < cores.size(); i++) {
            final LibraryCore core = cores.get(i);
            if (core == null)
                return false;

            if (core.isReader()) {
                JMenuItem item = new JMenuItem(core.getReaderDataType() + "...");
                item.addActionListener(new java.awt.event.ActionListener()
                {

                    @Override
                    public void actionPerformed(java.awt.event.ActionEvent evt)
                    {
                        ModuleAdder.doAddModule(core, getApplication(), p, true);
                    }
                });
                readersList.add(item);
                added = true;
            }
        }
        return added;
    }

    public ArrayList<Component> getTestdataMenu(Point p)
    {
        ArrayList<Component> out = new ArrayList<Component>();
        LibraryFolder rootFolder = VisNow.get().getMainLibraries().getInternalLibrary().getRootFolder();
        processLibraryFolderForTestData(rootFolder, out, p);

        if (out.isEmpty())
            return null;

        if (out.get(out.size() - 1) instanceof JSeparator) {
            out.remove(out.size() - 1);
        }
        return out;

    }

    private boolean processLibraryFolderForTestData(LibraryFolder folder, ArrayList<Component> list, final Point p)
    {
        Vector<LibraryFolder> folders = folder.getSubFolders();
        Vector<LibraryCore> cores = folder.getCores();
        for (int i = 0; i < folders.size(); i++) {
            boolean added = processLibraryFolderForTestData(folders.get(i), list, p);
            if (added)
                list.add(new JSeparator());
        }

        boolean added = false;
        for (int i = 0; i < cores.size(); i++) {
            final LibraryCore core = cores.get(i);
            if (core == null) {
                return false;
            }
            if (core.isTestData()) {
                JMenuItem item = new JMenuItem(core.getName());
                item.addActionListener(new java.awt.event.ActionListener()
                {
                    @Override
                    public void actionPerformed(java.awt.event.ActionEvent evt)
                    {
                        ModuleAdder.doAddModule(core, getApplication(), p, false);
                    }
                });
                list.add(item);
                added = true;
            }
        }
        return added;
    }

}
