/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.utils.log;

/**
 * Class to represent single line of Log file with corresponding log Level.
 * It's based on org.apache.log4j.Level and additionally Unknown_level is added.
 * <p>
 * @author szpak
 */
public class LogLine
{

    private Level level;
    private String line;

    /**
     * Tries to read level information from the line; If no level string is found than defaultLevel is taken.
     * <p>
     * @param line
     */
    public LogLine(String line, Level defaultLevel)
    {
        this.line = line;
        level = defaultLevel;
        for (Level level : Level.values()) {
            if (line.indexOf(level.name()) >= 0) {
                this.level = level;
                break;
            }
        }
    }

    public String getLine()
    {
        return line;
    }

    public Level getLevel()
    {
        return level;
    }

    /**
     * Levels based on log4j levels + unknown level.
     * This class describes level of the line (not every line includes Level string and
     * there are entries with multiple lines - like printStackTrace lines)
     * <p>
     * !! Used also as a name for string comparision !!
     */
    public enum Level
    {

        FATAL, ERROR, WARN, INFO, DEBUG, TRACE, UNKNOWN_LEVEL;
    }
}
