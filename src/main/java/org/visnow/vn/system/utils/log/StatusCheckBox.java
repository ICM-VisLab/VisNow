/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.utils.log;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import org.apache.log4j.Logger;

/**
 * Checkbox with color rectangle in place of standard "tick" mark.
 * <p>
 * @author szpak
 */
public class StatusCheckBox extends JCheckBox
{

    private static final Logger LOGGER = Logger.getLogger(StatusCheckBox.class);

    public StatusCheckBox(String text, boolean selected, Color color)
    {
        super(text, selected);
        setIcon(new StatusCheckBoxIcon(color));
    }

    private class StatusCheckBoxIcon implements Icon
    {

        //default size (just to not return 0 on getIconHeight/Width)
        private int size = 16;
        private Color color;

        public StatusCheckBoxIcon(Color color)
        {
            this.color = color;
        }

        public int getIconHeight()
        {
            return size;
        }

        public int getIconWidth()
        {
            return size;
        }

        public void paintIcon(Component c, Graphics g, int x, int y)
        {
            JCheckBox cb = (JCheckBox) c;
            int margin = 2;
            this.size = cb.getHeight() - cb.getMargin().top - cb.getMargin().bottom - margin * 2;
            if (cb.isSelected()) {
                g.setColor(color);
                g.fillRect(cb.getMargin().left, cb.getMargin().top + margin, size, size);
                g.setColor(Color.WHITE);
                g.drawRect(cb.getMargin().left, cb.getMargin().top + margin, size, size);
            } else {
                g.setColor(cb.getBackground().darker());
                g.fillRect(cb.getMargin().left, cb.getMargin().top + margin, size, size);
            }
        }
    }
}
