/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.utils.log;

import java.awt.Color;
import java.awt.Component;
import java.util.HashMap;
import java.util.Map;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

/**
 * Simple color renderer; It renders JList Items in color according to Level value in LogLine (as an item in the list).
 * Additionally it contains levels color map. This map can be used to render buttons responsible
 * for turning on/off some particular level of logging.
 * <p>
 * @author szpak
 */
public class LogWindowColorRenderer extends DefaultListCellRenderer
{

    public static final Color defaultColor = new Color(223, 223, 255);
    public static final Map<LogLine.Level, Color> backgroundColors;
    public static final Map<LogLine.Level, Color> foregroundColors;

    static {
        backgroundColors = new HashMap<LogLine.Level, Color>();
        backgroundColors.put(LogLine.Level.FATAL, Color.BLACK);
        backgroundColors.put(LogLine.Level.ERROR, Color.RED);
        backgroundColors.put(LogLine.Level.WARN, Color.YELLOW);
        backgroundColors.put(LogLine.Level.INFO, new Color(223, 255, 223));
        backgroundColors.put(LogLine.Level.DEBUG, Color.WHITE);
        backgroundColors.put(LogLine.Level.TRACE, new Color(223, 223, 255));
        backgroundColors.put(LogLine.Level.UNKNOWN_LEVEL, new Color(223, 223, 255));

        foregroundColors = new HashMap<LogLine.Level, Color>();
        foregroundColors.put(LogLine.Level.FATAL, Color.WHITE);
        foregroundColors.put(LogLine.Level.ERROR, Color.BLACK);
        foregroundColors.put(LogLine.Level.WARN, Color.BLACK);
        foregroundColors.put(LogLine.Level.INFO, Color.BLACK);
        foregroundColors.put(LogLine.Level.DEBUG, Color.BLACK);
        foregroundColors.put(LogLine.Level.TRACE, Color.BLACK);
        foregroundColors.put(LogLine.Level.UNKNOWN_LEVEL, Color.BLACK);
    }

    public Component getListCellRendererComponent(
        JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

        LogLine line = (LogLine) value;
        setText(line.getLine());
        Color backgroundColor = backgroundColors.get(line.getLevel());
        Color foregroundColor = foregroundColors.get(line.getLevel());
        setBackground(backgroundColor);
        //set black to avoid default white color for selected items
        setForeground(foregroundColor);
        return this;
    }
}
