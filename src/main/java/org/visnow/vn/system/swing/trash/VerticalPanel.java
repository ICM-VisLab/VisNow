/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.swing.trash;

import java.awt.Color;
import java.awt.Component;
import java.util.Stack;
import java.util.Vector;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.visnow.vn.system.swing.VNSwingUtils;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class VerticalPanel extends JPanel
{

    private int horizontalSpace = 5;

    private Vector<JComponent> components;

    public void addComponent(JComponent sp)
    {
        VNSwingUtils.setConstantHeight(sp, 80);
        components.add(sp);
        sp.setAlignmentX(Component.CENTER_ALIGNMENT);
    }

    public VerticalPanel()
    {
        this.components = new Vector<JComponent>();
        BoxLayout bl = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(bl);
        unusedSeparators = new Stack<JPanel>();
        usedSeparators = new Stack<JPanel>();
        this.parent = null;
    }

    //<editor-fold defaultstate="collapsed" desc=" Separators ">
    private Stack<JPanel> unusedSeparators;
    private Stack<JPanel> usedSeparators;

    private JPanel getSeparator()
    {
        if (!unusedSeparators.isEmpty()) {
            JPanel ret = unusedSeparators.pop();
            usedSeparators.push(ret);
            return ret;
        }
        JPanel ret = new JPanel();
        ret.setBackground(Color.LIGHT_GRAY);
        //System.out.println(this.getWidth());
        VNSwingUtils.setConstantHeight(ret, horizontalSpace);
        usedSeparators.push(ret);
        return ret;
    }

    //</editor-fold>
    private DuoPanel parent;
    private int height;

    public void setParentDuoPanel(DuoPanel panel)
    {
        this.parent = panel;
    }

    public void repack()
    {
        this.removeAll();
        height = horizontalSpace;
        unusedSeparators.addAll(usedSeparators);
        usedSeparators.clear();
        this.add(getSeparator());
        for (Component panel : components) {
            this.add(panel);
            height += panel.getPreferredSize().height;
            this.add(getSeparator());
            height += horizontalSpace;
        }
        VNSwingUtils.setConstantHeight(this, height);
        //this.doLayout();
        if (parent == null)
            repaint();
        else
            parent.repack();
    }

}
