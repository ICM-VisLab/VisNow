/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.swing.split_1;

import java.awt.BorderLayout;
import java.awt.Color;

/**
 *
 * @author gacek
 */
public class FAreaSingle extends FAreaSplittable
{

    protected boolean isMajor()
    {
        return false;
    }

    protected boolean isSplit()
    {
        return false;
    }

    protected boolean isSingle()
    {
        return true;
    }

    private FPlace place = new FPlace(this);

    public FPlace getPlace()
    {
        return place;
    }

    public FPlace getSomePlace()
    {
        return place;
    }

    private boolean remove = false;

    public boolean isRemoved()
    {
        return remove;
    }

    public FAreaSingle(FArea parent)
    {
        super(parent);
        getBottomLayer().add(place, BorderLayout.CENTER);
        this.setBackground(Color.red);
    }

    void markForRemoval()
    {
        remove = true;
        getMajor().markPlaceForRemoval(this);

    }

    protected void replaceChild(FAreaSplittable oldChild, FAreaSplittable newChild)
    {
    }

    @Override
    public void addBox(FBox box, int direction)
    {
        if (direction == centerD) {
            place.addBox(box);
            return;
        }

        FAreaSingle brother = new FAreaSingle(null);
        brother.addBox(box);
        new FAreaSplit(getParentArea(), this, brother, direction);

    }

    @Override
    protected void resize()
    {
        super.resize();
        place.resize();
        //TODO WHAT?
    }

    // protected void doDrop(FBox box, int dir) {
    //     addBox(box, dir);
    // }
    protected void unmarkForRemoval()
    {
        if (this.remove) {
            this.remove = false;
            this.getMajor().markPlaceForRemoval(null);
        }
    }

    protected void performRemoval()
    {
        //System.out.println("Perform removal");
        FArea par = this.getParentArea();
        if (!par.isSplit())
            return;
        FAreaSplit parent = (FAreaSplit) par;
        FArea grand = parent.getParentArea();

        FAreaSplittable brother;
        if (parent.getSon().equals(this))
            brother = parent.getDaughter();
        else
            brother = parent.getSon();

        brother.setParentArea(grand);
        grand.replaceChild(parent, brother);
        grand.validate();
    }

    protected String writeXML(int i)
    {
        String d = "";
        for (int j = 0; j < i; ++j)
            d += "  ";
        String ret = d + "<single>\n";
        ret += this.getPlace().writeInnerXML(i + 1);
        ret += d + "</single>\n";
        return ret;
    }

}
