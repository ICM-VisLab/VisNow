/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.swing;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagLayoutInfo;
import java.awt.LayoutManager;
import javax.swing.JPanel;

/**
 * Panel that "cheats" backing GridBagLayout to prevent it from using minimumSize of components when there is not enough space for render them using preferredSize.
 * Cheating means passing preferredSize as minimumSize.
 * 
 * @author szpak
 */
public class FixedGridBagLayoutPanel extends JPanel
{
    private GridBagLayout fixedGridBagLayoutManager;

    public FixedGridBagLayoutPanel()
    {
        fixedGridBagLayoutManager = new GridBagLayout()
        {

            @Override
            protected GridBagLayoutInfo getLayoutInfo(Container parent, int sizeflag)
            {
                return super.getLayoutInfo(parent, PREFERREDSIZE);
            }

            public Dimension minimumLayoutSize(Container parent)
            {
                GridBagLayoutInfo info = super.getLayoutInfo(parent, MINSIZE);
                return getMinSize(parent, info);
            }
        };

        setLayout(fixedGridBagLayoutManager);
    }

    /**
     * This method should not be called as this JPanel is dedicated for use with customized GridBagLayout. Though, it is called in initComponents (in NB GUI Builder).
     * This method should have empty body but NB GUI Builder doesn't work properly then.
     * @param mgr
     * @deprecated
     */
    @Deprecated
    @Override
    public void setLayout(LayoutManager mgr)
    {
        super.setLayout(mgr); //To change body of generated methods, choose Tools | Templates.
        super.setLayout(fixedGridBagLayoutManager);
    }

}
