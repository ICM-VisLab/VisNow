/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.swing.filechooser;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class ButtonList extends JScrollPane
{

    private JPanel bigPanel;
    private JPanel panel;
    private JPanel restPanel;

    public ButtonList()
    {
        setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        this.getHorizontalScrollBar().setPreferredSize(new Dimension(9, 9));

        bigPanel = new JPanel();
        panel = new JPanel();
        restPanel = new JPanel();

        panel.setLayout(new GridBagLayout());
        bigPanel.setLayout(new BorderLayout());
        restPanel.setPreferredSize(new Dimension(1, 1));

        bigPanel.add(panel, BorderLayout.WEST);
        bigPanel.add(restPanel, BorderLayout.CENTER);

        this.setViewportView(bigPanel);
    }

    public void startAdding()
    {
        panel.removeAll();
    }

    public void stopAdding()
    {
        panel.doLayout();
        bigPanel.doLayout();
        panel.doLayout();
        bigPanel.repaint();

        getViewport().doLayout();
        doLayout();
        getHorizontalScrollBar().setValue(getHorizontalScrollBar().getMaximum());
        repaint();
    }

    public void addButton(Component comp)
    {
        panel.add(comp);
    }

    public void quickAddButton(Component comp)
    {
        panel.add(comp);
        stopAdding();
    }
}
