/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.swing.filechooser;

import java.awt.Color;
import java.awt.Component;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.LineBorder;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class FileNameRenderer implements ListCellRenderer
{

    private final static ImageIcon folder
        = new ImageIcon(FileNameRenderer.class.getResource("/org/visnow/vn/gui/icons/small/fileopen.png"));

    private final static LineBorder border
        = new LineBorder(Color.DARK_GRAY, 1);

    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
        JLabel ret;
        if (value instanceof File) {
            ret = new JLabel(((File) value).getName() + "  ");
            if (((File) value).isDirectory())
                ret.setIcon(folder);
        } else {
            ret = new JLabel("-- ERROR --");
            ret.setForeground(Color.RED);
        }
        ret.setOpaque(true);
        if (isSelected)
            ret.setBackground(Color.LIGHT_GRAY);
        else
            ret.setBackground(Color.WHITE);
        if (cellHasFocus)
            ret.setBorder(border);
        return ret;
    }
}
