/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.swing;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import org.visnow.vn.engine.library.LibraryCore;
import org.visnow.vn.engine.library.LibraryFolder;
import org.visnow.vn.engine.library.LibraryRoot;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class LibTreeCellRenderer extends DefaultTreeCellRenderer
{

    private final static Color selectedBgColor = new Color(200, 200, 200);

    public LibTreeCellRenderer()
    {
    }

    @Override
    public Component getTreeCellRendererComponent(
        JTree tree,
        Object value,
        boolean selected,
        boolean expanded,
        boolean leaf,
        int row,
        boolean hasFocus)
    {
        DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) value;
        Object val = dmtn.getUserObject();
        if (val == null) {
            return new JLabel();
        }

        String name;
        JLabel label = new JLabel(val.toString());

        //////////////////////////////ROOT
        if (LibraryRoot.class.isAssignableFrom(val.getClass())) {
            name = ((LibraryRoot) val).getName();
            label.setText(name);
        }

        //////////////////////////////FOLDER
        if (LibraryFolder.class.isAssignableFrom(val.getClass())) {
            name = ((LibraryFolder) val).getName();
            label.setText(name);
            LibraryFolder lf = (LibraryFolder) val;
            if (lf.getRoot().getRootFolder() == lf) {
                if (lf.getRoot().getType() == LibraryRoot.INTERNAL) {
                    label.setIcon(Icons.getLibraryRootIcon(LibraryRoot.INTERNAL, 1));
                } else {
                    label.setIcon(Icons.getLibraryRootIcon(LibraryRoot.JAR, 1));
                }
            } else {
                label.setIcon(Icons.getLibraryFolderIcon(1));
            }
        }

        ///////////////////////////////CORE
        if (LibraryCore.class.isAssignableFrom(val.getClass())) {
            name = ((LibraryCore) val).getName();
            label.setText(name);
            label.setIcon(Icons.getLibraryCoreIcon(1));
            label.setToolTipText(((LibraryCore) val).getShortDescription());
        }

        if (selected) {
            label.setOpaque(true);
            label.setBackground(/*TODO: swing color*/selectedBgColor);
        }
        //if (val instanceof ModuleCore)
        //    setToolTipText(((ModuleCore)val).getShortDesc());

        if (dmtn.getAllowsChildren()) {
            label.setForeground(Color.BLACK);
        } else {
            //temporary state for conditional accept
            label.setForeground(Color.ORANGE);
        }

        return label;
    }
}
