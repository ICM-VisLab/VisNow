/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.swing.split;

import java.awt.BorderLayout;
import java.awt.Container;

/**
 *
 * @author gacek
 */
public abstract class FArea extends Container
{

    public final static int nullD = -1;
    public final static int centerD = 0;
    public final static int topD = 1;
    public final static int bottomD = 2;
    public final static int leftD = 3;
    public final static int rightD = 4;
    public final static int lastD = rightD + 1;

    protected abstract boolean isMajor();

    protected abstract boolean isSplit();

    protected abstract boolean isSingle();

    protected abstract FAreaMajor getMajor();

    protected abstract FPlace getSomePlace();

    protected abstract void replaceChild(FAreaSplittable oldChild, FAreaSplittable newChild);

    public void addBox(FBox box)
    {
        addBox(box, FAreaSplittable.centerD);
    }

    public abstract void addBox(FBox box, int direction);

    protected abstract void notifySplitListeners();

    //protected abstract void resize();
    protected FArea()
    {
        setLayout(new BorderLayout());
    }

}
