/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.config;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import org.visnow.vn.engine.exception.VNOuterDataException;
import org.visnow.vn.system.main.VisNow;
import static org.visnow.vn.system.main.VisNow.getOsType;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl), University of Warsaw, ICM
 *
 */
public class VNPlugin
{

    private static final Logger LOGGER = Logger.getLogger(VNPlugin.class);
    private String name = "plugin";
    private String libraryName = null;
    private String jarPath = null;
    private String[] libs = null;
    private String nativeLibsDir = null;
    private URLClassLoader loader = null;
    private boolean active = false;

    public VNPlugin(String name, String jarPath, String[] libs, String nativeLibsDir)
    {
        this.name = name;
        this.jarPath = jarPath;
        this.libs = libs;
        this.nativeLibsDir = nativeLibsDir;
        createClassLoader();
        readLibraryName();
    }

    public VNPlugin(String name, String jarPath)
    {
        this(name, jarPath, null, null);
    }

    private void createClassLoader()
    {
        if (libs == null) {
            loader = null;
            return;
        }

        ArrayList<URL> urlList = new ArrayList<>();
        try {
            urlList.add((new File(jarPath)).toURI().toURL());
        } catch (MalformedURLException ex) {
            return;
        }

        File f;
        for (String lib : libs) {
            f = new File(lib);
            if (f.exists()) {
                try {
                    URL url = f.toURI().toURL();
                    urlList.add(url);
                } catch (MalformedURLException ex) {
                    LOGGER.warn("WARNING: malformed url for plugin " + name + ": " + lib);
                }
            }
        }
        URL[] urls = new URL[urlList.size()];
        for (int i = 0; i < urls.length; i++) {
            urls[i] = urlList.get(i);
        }
        loader = new URLClassLoader(urls);
    }

    private void readLibraryName()
    {
        if (jarPath == null) {
            libraryName = null;
            return;
        }

        try {
            File file = new File(jarPath);
            if (!file.exists()) {
                libraryName = null;
                return;
            }
            JarFile jar = new JarFile(file);
            Enumeration<JarEntry> enumeration;
            JarEntry tmpEntry;
            JarEntry libraryEntry = null;
            enumeration = jar.entries();

            while (enumeration.hasMoreElements()) {
                tmpEntry = enumeration.nextElement();
                if (tmpEntry.getName().toLowerCase().equals("library.xml")) {
                    libraryEntry = tmpEntry;
                    break;
                }
            }

            if (libraryEntry == null) {
                libraryName = null;
                return;
            }
            InputStream is = jar.getInputStream(libraryEntry);
            Node main = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is).getDocumentElement();
            if (!main.getNodeName().equalsIgnoreCase("library")) {
                throw new VNOuterDataException(200903271350L, "Main node is not a library node.", null, null, Thread.currentThread());
            }
            String tmp = file.getPath();
            if (tmp.contains("\\")) {
                tmp = file.getPath().replace('\\', '/');
                tmp = "/" + tmp;
            }

            libraryName = main.getAttributes().getNamedItem("name").getNodeValue();
        } catch (IOException | ParserConfigurationException | SAXException | VNOuterDataException | DOMException ex) {
            libraryName = null;
        }

    }

    public boolean loadNative()
    {
        if (nativeLibsDir == null) {
            return true;
        }

        File libF = new File(nativeLibsDir);
        if (!libF.exists() || !libF.isDirectory()) {
            LOGGER.warn("Plugin " + name + " cannot find native libraries directory");
            return false;
        }

        String jlp = System.getProperty("java.library.path");
        jlp = jlp + File.pathSeparator + nativeLibsDir;
        try {
            System.setProperty("java.library.path", jlp);
            Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
            fieldSysPath.setAccessible(true);
            fieldSysPath.set(null, null);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            //ex.printStackTrace();
            LOGGER.warn("Plugin " + name + " failed to load native libraries");
            return false;
        }

        String[] libraryFiles = libF.list(new FilenameFilter()
        {
            @Override
            public boolean accept(File dir, String name)
            {
                return (name.toLowerCase().endsWith(".dll") || name.toLowerCase().endsWith(".so"));
            }
        });

        boolean successOnAllFiles = true;
        for (String libToRead : libraryFiles) {
            if (libToRead.startsWith("lib"))
                libToRead = libToRead.substring(3);
            if (libToRead.endsWith(".so"))
                libToRead = libToRead.substring(0, libToRead.lastIndexOf(".so"));
            else if (libToRead.endsWith(".dll"))
                libToRead = libToRead.substring(0, libToRead.lastIndexOf(".dll"));
            try {
                System.loadLibrary(libToRead);
            } catch (UnsatisfiedLinkError err) {
                successOnAllFiles = false;
            }
        }

        if (successOnAllFiles) {
            LOGGER.info("Plugin " + name + " loaded native libraries");
            return true;
        } else {
            LOGGER.warn("Plugin " + name + " failed to load native libraries");
            return false;
        }
    }

    public boolean unloadNative()
    {
        if (nativeLibsDir == null) {
            return true;
        }

        File libF = new File(nativeLibsDir);
        if (!libF.exists() || !libF.isDirectory()) {
            return true;
        }

        String jlp = System.getProperty("java.library.path");
        if (!jlp.contains(nativeLibsDir))
            return true;

        String newJlp = jlp.substring(0, jlp.indexOf(nativeLibsDir) - 1) + jlp.substring(jlp.indexOf(nativeLibsDir) + nativeLibsDir.length());
        try {
            System.setProperty("java.library.path", newJlp);
            Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
            fieldSysPath.setAccessible(true);
            fieldSysPath.set(null, null);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            LOGGER.warn("Plugin " + name + " failed to unload native libraries");
            return false;
        }
        return true;
    }

    public boolean check()
    {
        return testJar(jarPath);
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the jarPath
     */
    public String getJarPath()
    {
        return jarPath;
    }

    /**
     * @param jarPath the jarPath to set
     */
    public void setJarPath(String jarPath)
    {
        this.jarPath = jarPath;
    }

    /**
     * @return the libs
     */
    public String[] getLibs()
    {
        return libs;
    }

    /**
     * @param libs the libs to set
     */
    public void setLibs(String[] libs)
    {
        this.libs = libs;
        createClassLoader();
    }

    /**
     * @return the nativeLibsDir
     */
    public String getNativeLibsDir()
    {
        return nativeLibsDir;
    }

    /**
     * @param nativeLibsDir the nativeLibsDir to set
     */
    public void setNativeLibsDir(String nativeLibsDir)
    {
        this.nativeLibsDir = nativeLibsDir;
    }

    /**
     * @return the loader
     */
    public URLClassLoader getLoader()
    {
        return loader;
    }

    public static ArrayList<VNPlugin> pluginsFactory(File pluginsDir)
    {
        ArrayList<VNPlugin> plugins = new ArrayList<>();
        if (pluginsDir == null || !pluginsDir.exists())
            return plugins;

        String[] ls = pluginsDir.list();
        for (String l : ls) {
            File f = new File(pluginsDir.getAbsolutePath() + File.separator + l);
            if (f.exists() && f.isDirectory()) {
                VNPlugin plugin = pluginFactory(f);
                if (plugin != null && plugin.check())
                    plugins.add(plugin);
            }
        }
        return plugins;
    }

    public static VNPlugin pluginFactory(File pluginDir)
    {
        if (pluginDir == null || !pluginDir.exists()) {
            return null;
        }
        String pluginPath = pluginDir.getAbsolutePath();

        String pluginJarWithDependenciesPath = null;
        String pluginJarPath = null;
        String[] pluginLibs = null;
        String pluginNativeLibsDir = null;

        boolean jarWithDependenciesFound = false;
        boolean jarFound = false;
        String[] ls = pluginDir.list();
        Arrays.sort(ls);
        for (String l : ls) {
            if (l.equals("lib")) {
                File libDir = new File(pluginPath + File.separator + l);
                String[] libLs = libDir.list(new FilenameFilter()
                {
                    @Override
                    public boolean accept(File dir, String name)
                    {
                        return (name.toLowerCase().endsWith(".jar"));
                    }
                });
                if (libLs != null) {
                    pluginLibs = new String[libLs.length];
                    for (int j = 0; j < libLs.length; j++) {
                        pluginLibs[j] = pluginPath + File.separator + "lib" + File.separator + libLs[j];
                    }
                }
                String nativeLibDirPath = pluginPath + File.separator + "lib" + File.separator + "native";
                if (new File(nativeLibDirPath).exists()) {
                    //read native libraries list
                    boolean isLinux = (getOsType() == VisNow.OsType.OS_LINUX);
                    boolean isWindows = (getOsType() == VisNow.OsType.OS_WINDOWS);
                    boolean is64 = VisNow.isJreArch64();
                    if (isLinux && (new File(nativeLibDirPath + File.separator + "linux")).exists()) {
                        nativeLibDirPath += File.separator + "linux";
                        if (is64) {
                            nativeLibDirPath += File.separator + "x86_64";
                        } else {
                            nativeLibDirPath += File.separator + "x86";
                        }
                    } else if (isWindows && (new File(nativeLibDirPath + File.separator + "windows")).exists()) {
                        nativeLibDirPath += File.separator + "windows";
                        if (is64) {
                            nativeLibDirPath += File.separator + "win64";
                        } else {
                            nativeLibDirPath += File.separator + "win32";
                        }
                    }

                    File nativeLibDir = new File(nativeLibDirPath);
                    if (nativeLibDir.exists()) {
                        pluginNativeLibsDir = nativeLibDirPath;
                    }
                }
            } else if ((l.endsWith(".jar") || l.endsWith(".JAR")) && l.contains("-with-dependencies.") && !jarWithDependenciesFound) {
                    pluginJarWithDependenciesPath = pluginPath + File.separator + l;
                    jarWithDependenciesFound = true;
            } else if ((l.endsWith(".jar") || l.endsWith(".JAR")) && !l.contains("-with-dependencies.") && !l.contains("-javadoc.") && !l.contains("-sources.") && !jarFound) {
                    pluginJarPath = pluginPath + File.separator + l;
                    jarFound = true;
            }
        }

        if (jarWithDependenciesFound) {
            String pluginName = pluginJarWithDependenciesPath.substring(pluginJarWithDependenciesPath.lastIndexOf(File.separator) + 1);
            if (pluginName.endsWith("-with-dependencies.jar") || pluginName.endsWith("-with-dependencies.JAR")) {
                pluginName = pluginName.substring(0, pluginName.length() - 4);
            }
            return new VNPlugin(pluginName, pluginJarWithDependenciesPath, pluginLibs, pluginNativeLibsDir);
        } else if (jarFound) {
            String pluginName = pluginJarPath.substring(pluginJarPath.lastIndexOf(File.separator) + 1);
            if (pluginName.endsWith(".jar") || pluginName.endsWith(".JAR")) {
                pluginName = pluginName.substring(0, pluginName.length() - 4);
            }
            return new VNPlugin(pluginName, pluginJarPath, pluginLibs, pluginNativeLibsDir);
        } else {
            return null;
        }
    }

    public static boolean testPluginFolder(String pluginPath)
    {
        File pluginDir = new File(pluginPath);
        if (!pluginDir.exists())
            return false;

        String[] ls = pluginDir.list();
        Arrays.sort(ls);
        for (String l : ls) {
            if (l.toLowerCase().endsWith(".jar")) {
                //found first jar, test it
                return testJar(pluginPath + File.separator + l);
            }
        }
        //no jar found in dir
        return false;
    }

    public static boolean testJar(String jarPath)
    {
        try {
            File file = new File(jarPath);
            if (!file.exists()) {
                return false;
            }
            JarFile jar = new JarFile(file);
            Enumeration<JarEntry> enumeration;
            JarEntry tmpEntry;
            JarEntry libraryEntry = null;
            enumeration = jar.entries();

            while (enumeration.hasMoreElements()) {
                tmpEntry = enumeration.nextElement();
                if (tmpEntry.getName().toLowerCase().equals("library.xml")) {
                    libraryEntry = tmpEntry;
                    break;
                }
            }

            return (libraryEntry != null);
        } catch (IOException ex) {
            return false;
        }
    }

    /**
     * @return the active
     */
    public boolean isActive()
    {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active)
    {
        this.active = active;
    }

    public void activate()
    {
        setActive(true);
    }

    public void deactivate()
    {
        setActive(false);
    }

    /**
     * @return the libraryName
     */
    public String getLibraryName()
    {
        return libraryName;
    }
}
