/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.system.libraries;

import java.awt.Color;
import java.util.Collection;
import org.apache.log4j.Logger;
import org.visnow.vn.engine.library.LibraryRoot;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class MainTypes
{
    private static final Logger LOGGER = Logger.getLogger(MainTypes.class);

    private MainTypes()
    {

    }

    public static TypeStyle getTypeStyle(String className)
    {
        TypeStyle out;
        if ((out = VisNow.get().getMainLibraries().getInternalLibrary().getTypesMap().getStyle(className)) != null)
            return out;
        for (LibraryRoot library : VisNow.get().getMainLibraries().getLibraries())
            if (library.getTypesMap() != null && (out = library.getTypesMap().getStyle(className)) != null)
                return out;
        LOGGER.warn("no predefined port color for " + className);
        return new TypeStyle(Color.yellow);
    }

    public  static Collection<String> getTypes()
    {
        return VisNow.get().getMainLibraries().getInternalLibrary().getTypesMap().getTypes();
    }
}
