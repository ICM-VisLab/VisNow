/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.events;

import java.util.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class FloatPairModificationEvent extends EventObject
{

    private double valX;
    private double valY;
    private boolean adjusting;

    /**
     * Creates a new instance of DynamicsModificationEvent
     */
    public FloatPairModificationEvent(Object source, double valX, double valY, boolean adjusting)
    {
        super(source);
        this.valX = valX;
        this.valY = valY;
        this.adjusting = adjusting;
    }

    public double getValX()
    {
        return valX;
    }

    public double getValY()
    {
        return valY;
    }

   public boolean isAdjusting()
   {
      return adjusting;
   }

    @Override
    public String toString()
   {
      return "FloatPairModificationEvent " + valX + " " + valY + " " + adjusting;
   }
}
