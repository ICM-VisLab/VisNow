/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.components;

import java.beans.*;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class NumericTextFieldBeanInfo extends SimpleBeanInfo
{

    // Bean descriptor//GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( org.visnow.vn.gui.components.NumericTextField.class , null ); // NOI18N//GEN-HEADEREND:BeanDescriptor
    // Here you can add code for customizing the BeanDescriptor.

        return beanDescriptor;     }//GEN-LAST:BeanDescriptor


    // Property identifiers//GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_action = 1;
    private static final int PROPERTY_actionCommand = 2;
    private static final int PROPERTY_actionListeners = 3;
    private static final int PROPERTY_actionMap = 4;
    private static final int PROPERTY_actions = 5;
    private static final int PROPERTY_alignmentX = 6;
    private static final int PROPERTY_alignmentY = 7;
    private static final int PROPERTY_ancestorListeners = 8;
    private static final int PROPERTY_autoscrolls = 9;
    private static final int PROPERTY_autoToolTip = 10;
    private static final int PROPERTY_background = 11;
    private static final int PROPERTY_backgroundSet = 12;
    private static final int PROPERTY_baselineResizeBehavior = 13;
    private static final int PROPERTY_border = 14;
    private static final int PROPERTY_bounds = 15;
    private static final int PROPERTY_caret = 16;
    private static final int PROPERTY_caretColor = 17;
    private static final int PROPERTY_caretListeners = 18;
    private static final int PROPERTY_caretPosition = 19;
    private static final int PROPERTY_colorModel = 20;
    private static final int PROPERTY_columns = 21;
    private static final int PROPERTY_component = 22;
    private static final int PROPERTY_componentCount = 23;
    private static final int PROPERTY_componentListeners = 24;
    private static final int PROPERTY_componentOrientation = 25;
    private static final int PROPERTY_componentPopupMenu = 26;
    private static final int PROPERTY_components = 27;
    private static final int PROPERTY_containerListeners = 28;
    private static final int PROPERTY_cursor = 29;
    private static final int PROPERTY_cursorSet = 30;
    private static final int PROPERTY_debugGraphicsOptions = 31;
    private static final int PROPERTY_disabledTextColor = 32;
    private static final int PROPERTY_displayable = 33;
    private static final int PROPERTY_document = 34;
    private static final int PROPERTY_doubleBuffered = 35;
    private static final int PROPERTY_dragEnabled = 36;
    private static final int PROPERTY_dropLocation = 37;
    private static final int PROPERTY_dropMode = 38;
    private static final int PROPERTY_dropTarget = 39;
    private static final int PROPERTY_editable = 40;
    private static final int PROPERTY_enabled = 41;
    private static final int PROPERTY_fieldType = 42;
    private static final int PROPERTY_focusable = 43;
    private static final int PROPERTY_focusAccelerator = 44;
    private static final int PROPERTY_focusCycleRoot = 45;
    private static final int PROPERTY_focusCycleRootAncestor = 46;
    private static final int PROPERTY_focusListeners = 47;
    private static final int PROPERTY_focusOwner = 48;
    private static final int PROPERTY_focusTraversable = 49;
    private static final int PROPERTY_focusTraversalKeys = 50;
    private static final int PROPERTY_focusTraversalKeysEnabled = 51;
    private static final int PROPERTY_focusTraversalPolicy = 52;
    private static final int PROPERTY_focusTraversalPolicyProvider = 53;
    private static final int PROPERTY_focusTraversalPolicySet = 54;
    private static final int PROPERTY_font = 55;
    private static final int PROPERTY_fontSet = 56;
    private static final int PROPERTY_foreground = 57;
    private static final int PROPERTY_foregroundSet = 58;
    private static final int PROPERTY_formatter = 59;
    private static final int PROPERTY_graphics = 60;
    private static final int PROPERTY_graphicsConfiguration = 61;
    private static final int PROPERTY_height = 62;
    private static final int PROPERTY_hierarchyBoundsListeners = 63;
    private static final int PROPERTY_hierarchyListeners = 64;
    private static final int PROPERTY_highlighter = 65;
    private static final int PROPERTY_horizontalAlignment = 66;
    private static final int PROPERTY_horizontalVisibility = 67;
    private static final int PROPERTY_ignoreRepaint = 68;
    private static final int PROPERTY_inheritsPopupMenu = 69;
    private static final int PROPERTY_inputContext = 70;
    private static final int PROPERTY_inputMap = 71;
    private static final int PROPERTY_inputMethodListeners = 72;
    private static final int PROPERTY_inputMethodRequests = 73;
    private static final int PROPERTY_inputVerifier = 74;
    private static final int PROPERTY_insets = 75;
    private static final int PROPERTY_keyListeners = 76;
    private static final int PROPERTY_keymap = 77;
    private static final int PROPERTY_layout = 78;
    private static final int PROPERTY_lightweight = 79;
    private static final int PROPERTY_locale = 80;
    private static final int PROPERTY_location = 81;
    private static final int PROPERTY_locationOnScreen = 82;
    private static final int PROPERTY_managingFocus = 83;
    private static final int PROPERTY_margin = 84;
    private static final int PROPERTY_max = 85;
    private static final int PROPERTY_maximumSize = 86;
    private static final int PROPERTY_maximumSizeSet = 87;
    private static final int PROPERTY_min = 88;
    private static final int PROPERTY_minimumSize = 89;
    private static final int PROPERTY_minimumSizeSet = 90;
    private static final int PROPERTY_mouseListeners = 91;
    private static final int PROPERTY_mouseMotionListeners = 92;
    private static final int PROPERTY_mousePosition = 93;
    private static final int PROPERTY_mouseWheelListeners = 94;
    private static final int PROPERTY_name = 95;
    private static final int PROPERTY_navigationFilter = 96;
    private static final int PROPERTY_nextFocusableComponent = 97;
    private static final int PROPERTY_opaque = 98;
    private static final int PROPERTY_optimizedDrawingEnabled = 99;
    private static final int PROPERTY_paintingForPrint = 100;
    private static final int PROPERTY_paintingTile = 101;
    private static final int PROPERTY_parent = 102;
    private static final int PROPERTY_peer = 103;
    private static final int PROPERTY_preferredScrollableViewportSize = 104;
    private static final int PROPERTY_preferredSize = 105;
    private static final int PROPERTY_preferredSizeSet = 106;
    private static final int PROPERTY_propertyChangeListeners = 107;
    private static final int PROPERTY_registeredKeyStrokes = 108;
    private static final int PROPERTY_requestFocusEnabled = 109;
    private static final int PROPERTY_revertIfOutOfBounds = 110;
    private static final int PROPERTY_rootPane = 111;
    private static final int PROPERTY_scrollableTracksViewportHeight = 112;
    private static final int PROPERTY_scrollableTracksViewportWidth = 113;
    private static final int PROPERTY_scrollOffset = 114;
    private static final int PROPERTY_selectedText = 115;
    private static final int PROPERTY_selectedTextColor = 116;
    private static final int PROPERTY_selectionColor = 117;
    private static final int PROPERTY_selectionEnd = 118;
    private static final int PROPERTY_selectionStart = 119;
    private static final int PROPERTY_showing = 120;
    private static final int PROPERTY_size = 121;
    private static final int PROPERTY_submitOnLostFocus = 122;
    private static final int PROPERTY_text = 123;
    private static final int PROPERTY_toolkit = 124;
    private static final int PROPERTY_toolTipText = 125;
    private static final int PROPERTY_topLevelAncestor = 126;
    private static final int PROPERTY_transferHandler = 127;
    private static final int PROPERTY_treeLock = 128;
    private static final int PROPERTY_UI = 129;
    private static final int PROPERTY_UIClassID = 130;
    private static final int PROPERTY_valid = 131;
    private static final int PROPERTY_validateRoot = 132;
    private static final int PROPERTY_value = 133;
    private static final int PROPERTY_verifyInputWhenFocusTarget = 134;
    private static final int PROPERTY_vetoableChangeListeners = 135;
    private static final int PROPERTY_visible = 136;
    private static final int PROPERTY_visibleRect = 137;
    private static final int PROPERTY_width = 138;
    private static final int PROPERTY_x = 139;
    private static final int PROPERTY_y = 140;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[141];
    
        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", org.visnow.vn.gui.components.NumericTextField.class, "getAccessibleContext", null ); // NOI18N
            properties[PROPERTY_action] = new PropertyDescriptor ( "action", org.visnow.vn.gui.components.NumericTextField.class, "getAction", "setAction" ); // NOI18N
            properties[PROPERTY_actionCommand] = new PropertyDescriptor ( "actionCommand", org.visnow.vn.gui.components.NumericTextField.class, null, "setActionCommand" ); // NOI18N
            properties[PROPERTY_actionListeners] = new PropertyDescriptor ( "actionListeners", org.visnow.vn.gui.components.NumericTextField.class, "getActionListeners", null ); // NOI18N
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", org.visnow.vn.gui.components.NumericTextField.class, "getActionMap", "setActionMap" ); // NOI18N
            properties[PROPERTY_actions] = new PropertyDescriptor ( "actions", org.visnow.vn.gui.components.NumericTextField.class, "getActions", null ); // NOI18N
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", org.visnow.vn.gui.components.NumericTextField.class, "getAlignmentX", "setAlignmentX" ); // NOI18N
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", org.visnow.vn.gui.components.NumericTextField.class, "getAlignmentY", "setAlignmentY" ); // NOI18N
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", org.visnow.vn.gui.components.NumericTextField.class, "getAncestorListeners", null ); // NOI18N
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", org.visnow.vn.gui.components.NumericTextField.class, "getAutoscrolls", "setAutoscrolls" ); // NOI18N
            properties[PROPERTY_autoToolTip] = new PropertyDescriptor ( "autoToolTip", org.visnow.vn.gui.components.NumericTextField.class, "isAutoToolTip", "setAutoToolTip" ); // NOI18N
            properties[PROPERTY_autoToolTip].setPreferred ( true );
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", org.visnow.vn.gui.components.NumericTextField.class, "getBackground", "setBackground" ); // NOI18N
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", org.visnow.vn.gui.components.NumericTextField.class, "isBackgroundSet", null ); // NOI18N
            properties[PROPERTY_baselineResizeBehavior] = new PropertyDescriptor ( "baselineResizeBehavior", org.visnow.vn.gui.components.NumericTextField.class, "getBaselineResizeBehavior", null ); // NOI18N
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", org.visnow.vn.gui.components.NumericTextField.class, "getBorder", "setBorder" ); // NOI18N
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", org.visnow.vn.gui.components.NumericTextField.class, "getBounds", "setBounds" ); // NOI18N
            properties[PROPERTY_caret] = new PropertyDescriptor ( "caret", org.visnow.vn.gui.components.NumericTextField.class, "getCaret", "setCaret" ); // NOI18N
            properties[PROPERTY_caretColor] = new PropertyDescriptor ( "caretColor", org.visnow.vn.gui.components.NumericTextField.class, "getCaretColor", "setCaretColor" ); // NOI18N
            properties[PROPERTY_caretListeners] = new PropertyDescriptor ( "caretListeners", org.visnow.vn.gui.components.NumericTextField.class, "getCaretListeners", null ); // NOI18N
            properties[PROPERTY_caretPosition] = new PropertyDescriptor ( "caretPosition", org.visnow.vn.gui.components.NumericTextField.class, "getCaretPosition", "setCaretPosition" ); // NOI18N
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", org.visnow.vn.gui.components.NumericTextField.class, "getColorModel", null ); // NOI18N
            properties[PROPERTY_columns] = new PropertyDescriptor ( "columns", org.visnow.vn.gui.components.NumericTextField.class, "getColumns", "setColumns" ); // NOI18N
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", org.visnow.vn.gui.components.NumericTextField.class, null, null, "getComponent", null ); // NOI18N
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", org.visnow.vn.gui.components.NumericTextField.class, "getComponentCount", null ); // NOI18N
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", org.visnow.vn.gui.components.NumericTextField.class, "getComponentListeners", null ); // NOI18N
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", org.visnow.vn.gui.components.NumericTextField.class, "getComponentOrientation", "setComponentOrientation" ); // NOI18N
            properties[PROPERTY_componentPopupMenu] = new PropertyDescriptor ( "componentPopupMenu", org.visnow.vn.gui.components.NumericTextField.class, "getComponentPopupMenu", "setComponentPopupMenu" ); // NOI18N
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", org.visnow.vn.gui.components.NumericTextField.class, "getComponents", null ); // NOI18N
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", org.visnow.vn.gui.components.NumericTextField.class, "getContainerListeners", null ); // NOI18N
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", org.visnow.vn.gui.components.NumericTextField.class, "getCursor", "setCursor" ); // NOI18N
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", org.visnow.vn.gui.components.NumericTextField.class, "isCursorSet", null ); // NOI18N
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", org.visnow.vn.gui.components.NumericTextField.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" ); // NOI18N
            properties[PROPERTY_disabledTextColor] = new PropertyDescriptor ( "disabledTextColor", org.visnow.vn.gui.components.NumericTextField.class, "getDisabledTextColor", "setDisabledTextColor" ); // NOI18N
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", org.visnow.vn.gui.components.NumericTextField.class, "isDisplayable", null ); // NOI18N
            properties[PROPERTY_document] = new PropertyDescriptor ( "document", org.visnow.vn.gui.components.NumericTextField.class, "getDocument", "setDocument" ); // NOI18N
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", org.visnow.vn.gui.components.NumericTextField.class, "isDoubleBuffered", "setDoubleBuffered" ); // NOI18N
            properties[PROPERTY_dragEnabled] = new PropertyDescriptor ( "dragEnabled", org.visnow.vn.gui.components.NumericTextField.class, "getDragEnabled", "setDragEnabled" ); // NOI18N
            properties[PROPERTY_dropLocation] = new PropertyDescriptor ( "dropLocation", org.visnow.vn.gui.components.NumericTextField.class, "getDropLocation", null ); // NOI18N
            properties[PROPERTY_dropMode] = new PropertyDescriptor ( "dropMode", org.visnow.vn.gui.components.NumericTextField.class, "getDropMode", "setDropMode" ); // NOI18N
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", org.visnow.vn.gui.components.NumericTextField.class, "getDropTarget", "setDropTarget" ); // NOI18N
            properties[PROPERTY_editable] = new PropertyDescriptor ( "editable", org.visnow.vn.gui.components.NumericTextField.class, "isEditable", "setEditable" ); // NOI18N
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", org.visnow.vn.gui.components.NumericTextField.class, "isEnabled", "setEnabled" ); // NOI18N
            properties[PROPERTY_fieldType] = new PropertyDescriptor ( "fieldType", org.visnow.vn.gui.components.NumericTextField.class, "getFieldType", "setFieldType" ); // NOI18N
            properties[PROPERTY_fieldType].setPreferred ( true );
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", org.visnow.vn.gui.components.NumericTextField.class, "isFocusable", "setFocusable" ); // NOI18N
            properties[PROPERTY_focusAccelerator] = new PropertyDescriptor ( "focusAccelerator", org.visnow.vn.gui.components.NumericTextField.class, "getFocusAccelerator", "setFocusAccelerator" ); // NOI18N
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", org.visnow.vn.gui.components.NumericTextField.class, "isFocusCycleRoot", "setFocusCycleRoot" ); // NOI18N
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", org.visnow.vn.gui.components.NumericTextField.class, "getFocusCycleRootAncestor", null ); // NOI18N
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", org.visnow.vn.gui.components.NumericTextField.class, "getFocusListeners", null ); // NOI18N
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", org.visnow.vn.gui.components.NumericTextField.class, "isFocusOwner", null ); // NOI18N
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", org.visnow.vn.gui.components.NumericTextField.class, "isFocusTraversable", null ); // NOI18N
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", org.visnow.vn.gui.components.NumericTextField.class, null, null, null, "setFocusTraversalKeys" ); // NOI18N
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", org.visnow.vn.gui.components.NumericTextField.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", org.visnow.vn.gui.components.NumericTextField.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicyProvider] = new PropertyDescriptor ( "focusTraversalPolicyProvider", org.visnow.vn.gui.components.NumericTextField.class, "isFocusTraversalPolicyProvider", "setFocusTraversalPolicyProvider" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", org.visnow.vn.gui.components.NumericTextField.class, "isFocusTraversalPolicySet", null ); // NOI18N
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", org.visnow.vn.gui.components.NumericTextField.class, "getFont", "setFont" ); // NOI18N
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", org.visnow.vn.gui.components.NumericTextField.class, "isFontSet", null ); // NOI18N
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", org.visnow.vn.gui.components.NumericTextField.class, "getForeground", "setForeground" ); // NOI18N
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", org.visnow.vn.gui.components.NumericTextField.class, "isForegroundSet", null ); // NOI18N
            properties[PROPERTY_formatter] = new PropertyDescriptor ( "formatter", org.visnow.vn.gui.components.NumericTextField.class, null, "setFormatter" ); // NOI18N
            properties[PROPERTY_formatter].setPreferred ( true );
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", org.visnow.vn.gui.components.NumericTextField.class, "getGraphics", null ); // NOI18N
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", org.visnow.vn.gui.components.NumericTextField.class, "getGraphicsConfiguration", null ); // NOI18N
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", org.visnow.vn.gui.components.NumericTextField.class, "getHeight", null ); // NOI18N
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", org.visnow.vn.gui.components.NumericTextField.class, "getHierarchyBoundsListeners", null ); // NOI18N
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", org.visnow.vn.gui.components.NumericTextField.class, "getHierarchyListeners", null ); // NOI18N
            properties[PROPERTY_highlighter] = new PropertyDescriptor ( "highlighter", org.visnow.vn.gui.components.NumericTextField.class, "getHighlighter", "setHighlighter" ); // NOI18N
            properties[PROPERTY_horizontalAlignment] = new PropertyDescriptor ( "horizontalAlignment", org.visnow.vn.gui.components.NumericTextField.class, "getHorizontalAlignment", "setHorizontalAlignment" ); // NOI18N
            properties[PROPERTY_horizontalVisibility] = new PropertyDescriptor ( "horizontalVisibility", org.visnow.vn.gui.components.NumericTextField.class, "getHorizontalVisibility", null ); // NOI18N
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", org.visnow.vn.gui.components.NumericTextField.class, "getIgnoreRepaint", "setIgnoreRepaint" ); // NOI18N
            properties[PROPERTY_inheritsPopupMenu] = new PropertyDescriptor ( "inheritsPopupMenu", org.visnow.vn.gui.components.NumericTextField.class, "getInheritsPopupMenu", "setInheritsPopupMenu" ); // NOI18N
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", org.visnow.vn.gui.components.NumericTextField.class, "getInputContext", null ); // NOI18N
            properties[PROPERTY_inputMap] = new PropertyDescriptor ( "inputMap", org.visnow.vn.gui.components.NumericTextField.class, "getInputMap", null ); // NOI18N
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", org.visnow.vn.gui.components.NumericTextField.class, "getInputMethodListeners", null ); // NOI18N
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", org.visnow.vn.gui.components.NumericTextField.class, "getInputMethodRequests", null ); // NOI18N
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", org.visnow.vn.gui.components.NumericTextField.class, "getInputVerifier", "setInputVerifier" ); // NOI18N
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", org.visnow.vn.gui.components.NumericTextField.class, "getInsets", null ); // NOI18N
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", org.visnow.vn.gui.components.NumericTextField.class, "getKeyListeners", null ); // NOI18N
            properties[PROPERTY_keymap] = new PropertyDescriptor ( "keymap", org.visnow.vn.gui.components.NumericTextField.class, "getKeymap", "setKeymap" ); // NOI18N
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", org.visnow.vn.gui.components.NumericTextField.class, "getLayout", "setLayout" ); // NOI18N
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", org.visnow.vn.gui.components.NumericTextField.class, "isLightweight", null ); // NOI18N
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", org.visnow.vn.gui.components.NumericTextField.class, "getLocale", "setLocale" ); // NOI18N
            properties[PROPERTY_location] = new PropertyDescriptor ( "location", org.visnow.vn.gui.components.NumericTextField.class, "getLocation", "setLocation" ); // NOI18N
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", org.visnow.vn.gui.components.NumericTextField.class, "getLocationOnScreen", null ); // NOI18N
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", org.visnow.vn.gui.components.NumericTextField.class, "isManagingFocus", null ); // NOI18N
            properties[PROPERTY_margin] = new PropertyDescriptor ( "margin", org.visnow.vn.gui.components.NumericTextField.class, "getMargin", "setMargin" ); // NOI18N
            properties[PROPERTY_max] = new PropertyDescriptor ( "max", org.visnow.vn.gui.components.NumericTextField.class, "getMax", "setMax" ); // NOI18N
            properties[PROPERTY_max].setPreferred ( true );
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", org.visnow.vn.gui.components.NumericTextField.class, "getMaximumSize", "setMaximumSize" ); // NOI18N
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", org.visnow.vn.gui.components.NumericTextField.class, "isMaximumSizeSet", null ); // NOI18N
            properties[PROPERTY_min] = new PropertyDescriptor ( "min", org.visnow.vn.gui.components.NumericTextField.class, "getMin", "setMin" ); // NOI18N
            properties[PROPERTY_min].setPreferred ( true );
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", org.visnow.vn.gui.components.NumericTextField.class, "getMinimumSize", "setMinimumSize" ); // NOI18N
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", org.visnow.vn.gui.components.NumericTextField.class, "isMinimumSizeSet", null ); // NOI18N
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", org.visnow.vn.gui.components.NumericTextField.class, "getMouseListeners", null ); // NOI18N
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", org.visnow.vn.gui.components.NumericTextField.class, "getMouseMotionListeners", null ); // NOI18N
            properties[PROPERTY_mousePosition] = new PropertyDescriptor ( "mousePosition", org.visnow.vn.gui.components.NumericTextField.class, "getMousePosition", null ); // NOI18N
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", org.visnow.vn.gui.components.NumericTextField.class, "getMouseWheelListeners", null ); // NOI18N
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", org.visnow.vn.gui.components.NumericTextField.class, "getName", "setName" ); // NOI18N
            properties[PROPERTY_navigationFilter] = new PropertyDescriptor ( "navigationFilter", org.visnow.vn.gui.components.NumericTextField.class, "getNavigationFilter", "setNavigationFilter" ); // NOI18N
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", org.visnow.vn.gui.components.NumericTextField.class, "getNextFocusableComponent", "setNextFocusableComponent" ); // NOI18N
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", org.visnow.vn.gui.components.NumericTextField.class, "isOpaque", "setOpaque" ); // NOI18N
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", org.visnow.vn.gui.components.NumericTextField.class, "isOptimizedDrawingEnabled", null ); // NOI18N
            properties[PROPERTY_paintingForPrint] = new PropertyDescriptor ( "paintingForPrint", org.visnow.vn.gui.components.NumericTextField.class, "isPaintingForPrint", null ); // NOI18N
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", org.visnow.vn.gui.components.NumericTextField.class, "isPaintingTile", null ); // NOI18N
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", org.visnow.vn.gui.components.NumericTextField.class, "getParent", null ); // NOI18N
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", org.visnow.vn.gui.components.NumericTextField.class, "getPeer", null ); // NOI18N
            properties[PROPERTY_preferredScrollableViewportSize] = new PropertyDescriptor ( "preferredScrollableViewportSize", org.visnow.vn.gui.components.NumericTextField.class, "getPreferredScrollableViewportSize", null ); // NOI18N
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", org.visnow.vn.gui.components.NumericTextField.class, "getPreferredSize", "setPreferredSize" ); // NOI18N
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", org.visnow.vn.gui.components.NumericTextField.class, "isPreferredSizeSet", null ); // NOI18N
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", org.visnow.vn.gui.components.NumericTextField.class, "getPropertyChangeListeners", null ); // NOI18N
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", org.visnow.vn.gui.components.NumericTextField.class, "getRegisteredKeyStrokes", null ); // NOI18N
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", org.visnow.vn.gui.components.NumericTextField.class, "isRequestFocusEnabled", "setRequestFocusEnabled" ); // NOI18N
            properties[PROPERTY_revertIfOutOfBounds] = new PropertyDescriptor ( "revertIfOutOfBounds", org.visnow.vn.gui.components.NumericTextField.class, "isRevertIfOutOfBounds", "setRevertIfOutOfBounds" ); // NOI18N
            properties[PROPERTY_revertIfOutOfBounds].setPreferred ( true );
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", org.visnow.vn.gui.components.NumericTextField.class, "getRootPane", null ); // NOI18N
            properties[PROPERTY_scrollableTracksViewportHeight] = new PropertyDescriptor ( "scrollableTracksViewportHeight", org.visnow.vn.gui.components.NumericTextField.class, "getScrollableTracksViewportHeight", null ); // NOI18N
            properties[PROPERTY_scrollableTracksViewportWidth] = new PropertyDescriptor ( "scrollableTracksViewportWidth", org.visnow.vn.gui.components.NumericTextField.class, "getScrollableTracksViewportWidth", null ); // NOI18N
            properties[PROPERTY_scrollOffset] = new PropertyDescriptor ( "scrollOffset", org.visnow.vn.gui.components.NumericTextField.class, "getScrollOffset", "setScrollOffset" ); // NOI18N
            properties[PROPERTY_selectedText] = new PropertyDescriptor ( "selectedText", org.visnow.vn.gui.components.NumericTextField.class, "getSelectedText", null ); // NOI18N
            properties[PROPERTY_selectedTextColor] = new PropertyDescriptor ( "selectedTextColor", org.visnow.vn.gui.components.NumericTextField.class, "getSelectedTextColor", "setSelectedTextColor" ); // NOI18N
            properties[PROPERTY_selectionColor] = new PropertyDescriptor ( "selectionColor", org.visnow.vn.gui.components.NumericTextField.class, "getSelectionColor", "setSelectionColor" ); // NOI18N
            properties[PROPERTY_selectionEnd] = new PropertyDescriptor ( "selectionEnd", org.visnow.vn.gui.components.NumericTextField.class, "getSelectionEnd", "setSelectionEnd" ); // NOI18N
            properties[PROPERTY_selectionStart] = new PropertyDescriptor ( "selectionStart", org.visnow.vn.gui.components.NumericTextField.class, "getSelectionStart", "setSelectionStart" ); // NOI18N
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", org.visnow.vn.gui.components.NumericTextField.class, "isShowing", null ); // NOI18N
            properties[PROPERTY_size] = new PropertyDescriptor ( "size", org.visnow.vn.gui.components.NumericTextField.class, "getSize", "setSize" ); // NOI18N
            properties[PROPERTY_submitOnLostFocus] = new PropertyDescriptor ( "submitOnLostFocus", org.visnow.vn.gui.components.NumericTextField.class, "isSubmitOnLostFocus", "setSubmitOnLostFocus" ); // NOI18N
            properties[PROPERTY_text] = new PropertyDescriptor ( "text", org.visnow.vn.gui.components.NumericTextField.class, "getText", "setText" ); // NOI18N
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", org.visnow.vn.gui.components.NumericTextField.class, "getToolkit", null ); // NOI18N
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", org.visnow.vn.gui.components.NumericTextField.class, "getToolTipText", "setToolTipText" ); // NOI18N
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", org.visnow.vn.gui.components.NumericTextField.class, "getTopLevelAncestor", null ); // NOI18N
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", org.visnow.vn.gui.components.NumericTextField.class, "getTransferHandler", "setTransferHandler" ); // NOI18N
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", org.visnow.vn.gui.components.NumericTextField.class, "getTreeLock", null ); // NOI18N
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", org.visnow.vn.gui.components.NumericTextField.class, "getUI", "setUI" ); // NOI18N
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", org.visnow.vn.gui.components.NumericTextField.class, "getUIClassID", null ); // NOI18N
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", org.visnow.vn.gui.components.NumericTextField.class, "isValid", null ); // NOI18N
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", org.visnow.vn.gui.components.NumericTextField.class, "isValidateRoot", null ); // NOI18N
            properties[PROPERTY_value] = new PropertyDescriptor ( "value", org.visnow.vn.gui.components.NumericTextField.class, "getValue", "setValue" ); // NOI18N
            properties[PROPERTY_value].setPreferred ( true );
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", org.visnow.vn.gui.components.NumericTextField.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" ); // NOI18N
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", org.visnow.vn.gui.components.NumericTextField.class, "getVetoableChangeListeners", null ); // NOI18N
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", org.visnow.vn.gui.components.NumericTextField.class, "isVisible", "setVisible" ); // NOI18N
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", org.visnow.vn.gui.components.NumericTextField.class, "getVisibleRect", null ); // NOI18N
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", org.visnow.vn.gui.components.NumericTextField.class, "getWidth", null ); // NOI18N
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", org.visnow.vn.gui.components.NumericTextField.class, "getX", null ); // NOI18N
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", org.visnow.vn.gui.components.NumericTextField.class, "getY", null ); // NOI18N
        }
        catch(IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Properties
    // Here you can add code for customizing the properties array.

        return properties;     }//GEN-LAST:Properties

    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_actionListener = 0;
    private static final int EVENT_ancestorListener = 1;
    private static final int EVENT_caretListener = 2;
    private static final int EVENT_componentListener = 3;
    private static final int EVENT_containerListener = 4;
    private static final int EVENT_focusListener = 5;
    private static final int EVENT_hierarchyBoundsListener = 6;
    private static final int EVENT_hierarchyListener = 7;
    private static final int EVENT_inputMethodListener = 8;
    private static final int EVENT_keyListener = 9;
    private static final int EVENT_mouseListener = 10;
    private static final int EVENT_mouseMotionListener = 11;
    private static final int EVENT_mouseWheelListener = 12;
    private static final int EVENT_propertyChangeListener = 13;
    private static final int EVENT_userActionListener = 14;
    private static final int EVENT_vetoableChangeListener = 15;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[16];
    
        try {
            eventSets[EVENT_actionListener] = new EventSetDescriptor ( org.visnow.vn.gui.components.NumericTextField.class, "actionListener", java.awt.event.ActionListener.class, new String[] {"actionPerformed"}, "addActionListener", "removeActionListener" ); // NOI18N
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( org.visnow.vn.gui.components.NumericTextField.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorAdded", "ancestorRemoved", "ancestorMoved"}, "addAncestorListener", "removeAncestorListener" ); // NOI18N
            eventSets[EVENT_caretListener] = new EventSetDescriptor ( org.visnow.vn.gui.components.NumericTextField.class, "caretListener", javax.swing.event.CaretListener.class, new String[] {"caretUpdate"}, "addCaretListener", "removeCaretListener" ); // NOI18N
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( org.visnow.vn.gui.components.NumericTextField.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentResized", "componentMoved", "componentShown", "componentHidden"}, "addComponentListener", "removeComponentListener" ); // NOI18N
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( org.visnow.vn.gui.components.NumericTextField.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" ); // NOI18N
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( org.visnow.vn.gui.components.NumericTextField.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusGained", "focusLost"}, "addFocusListener", "removeFocusListener" ); // NOI18N
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( org.visnow.vn.gui.components.NumericTextField.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" ); // NOI18N
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( org.visnow.vn.gui.components.NumericTextField.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" ); // NOI18N
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( org.visnow.vn.gui.components.NumericTextField.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"inputMethodTextChanged", "caretPositionChanged"}, "addInputMethodListener", "removeInputMethodListener" ); // NOI18N
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( org.visnow.vn.gui.components.NumericTextField.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyTyped", "keyPressed", "keyReleased"}, "addKeyListener", "removeKeyListener" ); // NOI18N
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( org.visnow.vn.gui.components.NumericTextField.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mousePressed", "mouseReleased", "mouseEntered", "mouseExited"}, "addMouseListener", "removeMouseListener" ); // NOI18N
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( org.visnow.vn.gui.components.NumericTextField.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" ); // NOI18N
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( org.visnow.vn.gui.components.NumericTextField.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" ); // NOI18N
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( org.visnow.vn.gui.components.NumericTextField.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" ); // NOI18N
            eventSets[EVENT_userActionListener] = new EventSetDescriptor ( org.visnow.vn.gui.components.NumericTextField.class, "userActionListener", org.visnow.vn.gui.swingwrappers.UserActionListener.class, new String[] {"userChangeAction", "userAction"}, "addUserActionListener", "removeUserActionListener" ); // NOI18N
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( org.visnow.vn.gui.components.NumericTextField.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" ); // NOI18N
        }
        catch(IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Events
    // Here you can add code for customizing the event sets array.

        return eventSets;     }//GEN-LAST:Events

    // Method identifiers//GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_add2 = 2;
    private static final int METHOD_add3 = 3;
    private static final int METHOD_add4 = 4;
    private static final int METHOD_add5 = 5;
    private static final int METHOD_add6 = 6;
    private static final int METHOD_addKeymap7 = 7;
    private static final int METHOD_addNotify8 = 8;
    private static final int METHOD_addPropertyChangeListener9 = 9;
    private static final int METHOD_applyComponentOrientation10 = 10;
    private static final int METHOD_areFocusTraversalKeysSet11 = 11;
    private static final int METHOD_bounds12 = 12;
    private static final int METHOD_checkImage13 = 13;
    private static final int METHOD_checkImage14 = 14;
    private static final int METHOD_compare15 = 15;
    private static final int METHOD_compareStrict16 = 16;
    private static final int METHOD_compareTo17 = 17;
    private static final int METHOD_computeVisibleRect18 = 18;
    private static final int METHOD_contains19 = 19;
    private static final int METHOD_contains20 = 20;
    private static final int METHOD_convertToDouble21 = 21;
    private static final int METHOD_copy22 = 22;
    private static final int METHOD_countComponents23 = 23;
    private static final int METHOD_createDoubleFromObject24 = 24;
    private static final int METHOD_createDoubleFromObject25 = 25;
    private static final int METHOD_createImage26 = 26;
    private static final int METHOD_createImage27 = 27;
    private static final int METHOD_createLongFromObject28 = 28;
    private static final int METHOD_createLongFromObject29 = 29;
    private static final int METHOD_createToolTip30 = 30;
    private static final int METHOD_createVolatileImage31 = 31;
    private static final int METHOD_createVolatileImage32 = 32;
    private static final int METHOD_cut33 = 33;
    private static final int METHOD_deliverEvent34 = 34;
    private static final int METHOD_disable35 = 35;
    private static final int METHOD_dispatchEvent36 = 36;
    private static final int METHOD_doLayout37 = 37;
    private static final int METHOD_enable38 = 38;
    private static final int METHOD_enable39 = 39;
    private static final int METHOD_enableInputMethods40 = 40;
    private static final int METHOD_findComponentAt41 = 41;
    private static final int METHOD_findComponentAt42 = 42;
    private static final int METHOD_firePropertyChange43 = 43;
    private static final int METHOD_firePropertyChange44 = 44;
    private static final int METHOD_firePropertyChange45 = 45;
    private static final int METHOD_firePropertyChange46 = 46;
    private static final int METHOD_firePropertyChange47 = 47;
    private static final int METHOD_firePropertyChange48 = 48;
    private static final int METHOD_firePropertyChange49 = 49;
    private static final int METHOD_firePropertyChange50 = 50;
    private static final int METHOD_getActionForKeyStroke51 = 51;
    private static final int METHOD_getBaseline52 = 52;
    private static final int METHOD_getBounds53 = 53;
    private static final int METHOD_getClientProperty54 = 54;
    private static final int METHOD_getComponentAt55 = 55;
    private static final int METHOD_getComponentAt56 = 56;
    private static final int METHOD_getComponentZOrder57 = 57;
    private static final int METHOD_getConditionForKeyStroke58 = 58;
    private static final int METHOD_getDefaultLocale59 = 59;
    private static final int METHOD_getFocusTraversalKeys60 = 60;
    private static final int METHOD_getFontMetrics61 = 61;
    private static final int METHOD_getInsets62 = 62;
    private static final int METHOD_getKeymap63 = 63;
    private static final int METHOD_getListeners64 = 64;
    private static final int METHOD_getLocation65 = 65;
    private static final int METHOD_getMinMax66 = 66;
    private static final int METHOD_getMousePosition67 = 67;
    private static final int METHOD_getPopupLocation68 = 68;
    private static final int METHOD_getPrintable69 = 69;
    private static final int METHOD_getPropertyChangeListeners70 = 70;
    private static final int METHOD_getScrollableBlockIncrement71 = 71;
    private static final int METHOD_getScrollableUnitIncrement72 = 72;
    private static final int METHOD_getSize73 = 73;
    private static final int METHOD_getText74 = 74;
    private static final int METHOD_getToolTipLocation75 = 75;
    private static final int METHOD_getToolTipText76 = 76;
    private static final int METHOD_getType77 = 77;
    private static final int METHOD_gotFocus78 = 78;
    private static final int METHOD_grabFocus79 = 79;
    private static final int METHOD_handleEvent80 = 80;
    private static final int METHOD_hasFocus81 = 81;
    private static final int METHOD_hide82 = 82;
    private static final int METHOD_imageUpdate83 = 83;
    private static final int METHOD_insets84 = 84;
    private static final int METHOD_inside85 = 85;
    private static final int METHOD_invalidate86 = 86;
    private static final int METHOD_isAncestorOf87 = 87;
    private static final int METHOD_isFocusCycleRoot88 = 88;
    private static final int METHOD_isLightweightComponent89 = 89;
    private static final int METHOD_keyDown90 = 90;
    private static final int METHOD_keyUp91 = 91;
    private static final int METHOD_layout92 = 92;
    private static final int METHOD_list93 = 93;
    private static final int METHOD_list94 = 94;
    private static final int METHOD_list95 = 95;
    private static final int METHOD_list96 = 96;
    private static final int METHOD_list97 = 97;
    private static final int METHOD_loadKeymap98 = 98;
    private static final int METHOD_locate99 = 99;
    private static final int METHOD_location100 = 100;
    private static final int METHOD_lostFocus101 = 101;
    private static final int METHOD_minimumSize102 = 102;
    private static final int METHOD_modelToView103 = 103;
    private static final int METHOD_mouseDown104 = 104;
    private static final int METHOD_mouseDrag105 = 105;
    private static final int METHOD_mouseEnter106 = 106;
    private static final int METHOD_mouseExit107 = 107;
    private static final int METHOD_mouseMove108 = 108;
    private static final int METHOD_mouseUp109 = 109;
    private static final int METHOD_move110 = 110;
    private static final int METHOD_moveCaretPosition111 = 111;
    private static final int METHOD_nextFocus112 = 112;
    private static final int METHOD_paint113 = 113;
    private static final int METHOD_paintAll114 = 114;
    private static final int METHOD_paintComponents115 = 115;
    private static final int METHOD_paintImmediately116 = 116;
    private static final int METHOD_paintImmediately117 = 117;
    private static final int METHOD_paste118 = 118;
    private static final int METHOD_postActionEvent119 = 119;
    private static final int METHOD_postEvent120 = 120;
    private static final int METHOD_preferredSize121 = 121;
    private static final int METHOD_prepareImage122 = 122;
    private static final int METHOD_prepareImage123 = 123;
    private static final int METHOD_print124 = 124;
    private static final int METHOD_print125 = 125;
    private static final int METHOD_print126 = 126;
    private static final int METHOD_print127 = 127;
    private static final int METHOD_printAll128 = 128;
    private static final int METHOD_printComponents129 = 129;
    private static final int METHOD_putClientProperty130 = 130;
    private static final int METHOD_read131 = 131;
    private static final int METHOD_registerKeyboardAction132 = 132;
    private static final int METHOD_registerKeyboardAction133 = 133;
    private static final int METHOD_remove134 = 134;
    private static final int METHOD_remove135 = 135;
    private static final int METHOD_remove136 = 136;
    private static final int METHOD_removeAll137 = 137;
    private static final int METHOD_removeKeymap138 = 138;
    private static final int METHOD_removeNotify139 = 139;
    private static final int METHOD_removePropertyChangeListener140 = 140;
    private static final int METHOD_repaint141 = 141;
    private static final int METHOD_repaint142 = 142;
    private static final int METHOD_repaint143 = 143;
    private static final int METHOD_repaint144 = 144;
    private static final int METHOD_repaint145 = 145;
    private static final int METHOD_replaceSelection146 = 146;
    private static final int METHOD_requestDefaultFocus147 = 147;
    private static final int METHOD_requestFocus148 = 148;
    private static final int METHOD_requestFocus149 = 149;
    private static final int METHOD_requestFocusInWindow150 = 150;
    private static final int METHOD_resetKeyboardActions151 = 151;
    private static final int METHOD_reshape152 = 152;
    private static final int METHOD_resize153 = 153;
    private static final int METHOD_resize154 = 154;
    private static final int METHOD_revalidate155 = 155;
    private static final int METHOD_scrollRectToVisible156 = 156;
    private static final int METHOD_select157 = 157;
    private static final int METHOD_selectAll158 = 158;
    private static final int METHOD_setBounds159 = 159;
    private static final int METHOD_setComponentZOrder160 = 160;
    private static final int METHOD_setDefaultLocale161 = 161;
    private static final int METHOD_setValue162 = 162;
    private static final int METHOD_show163 = 163;
    private static final int METHOD_show164 = 164;
    private static final int METHOD_size165 = 165;
    private static final int METHOD_toString166 = 166;
    private static final int METHOD_transferFocus167 = 167;
    private static final int METHOD_transferFocusBackward168 = 168;
    private static final int METHOD_transferFocusDownCycle169 = 169;
    private static final int METHOD_transferFocusUpCycle170 = 170;
    private static final int METHOD_unregisterKeyboardAction171 = 171;
    private static final int METHOD_update172 = 172;
    private static final int METHOD_updateUI173 = 173;
    private static final int METHOD_validate174 = 174;
    private static final int METHOD_viewToModel175 = 175;
    private static final int METHOD_write176 = 176;

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[177];
    
        try {
            methods[METHOD_action0] = new MethodDescriptor(java.awt.Component.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_action0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor(java.awt.Component.class.getMethod("add", new Class[] {java.awt.PopupMenu.class})); // NOI18N
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_add2] = new MethodDescriptor(java.awt.Container.class.getMethod("add", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_add2].setDisplayName ( "" );
            methods[METHOD_add3] = new MethodDescriptor(java.awt.Container.class.getMethod("add", new Class[] {java.lang.String.class, java.awt.Component.class})); // NOI18N
            methods[METHOD_add3].setDisplayName ( "" );
            methods[METHOD_add4] = new MethodDescriptor(java.awt.Container.class.getMethod("add", new Class[] {java.awt.Component.class, int.class})); // NOI18N
            methods[METHOD_add4].setDisplayName ( "" );
            methods[METHOD_add5] = new MethodDescriptor(java.awt.Container.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_add5].setDisplayName ( "" );
            methods[METHOD_add6] = new MethodDescriptor(java.awt.Container.class.getMethod("add", new Class[] {java.awt.Component.class, java.lang.Object.class, int.class})); // NOI18N
            methods[METHOD_add6].setDisplayName ( "" );
            methods[METHOD_addKeymap7] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("addKeymap", new Class[] {java.lang.String.class, javax.swing.text.Keymap.class})); // NOI18N
            methods[METHOD_addKeymap7].setDisplayName ( "" );
            methods[METHOD_addNotify8] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("addNotify", new Class[] {})); // NOI18N
            methods[METHOD_addNotify8].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener9] = new MethodDescriptor(java.awt.Container.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class})); // NOI18N
            methods[METHOD_addPropertyChangeListener9].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation10] = new MethodDescriptor(java.awt.Container.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class})); // NOI18N
            methods[METHOD_applyComponentOrientation10].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet11] = new MethodDescriptor(java.awt.Container.class.getMethod("areFocusTraversalKeysSet", new Class[] {int.class})); // NOI18N
            methods[METHOD_areFocusTraversalKeysSet11].setDisplayName ( "" );
            methods[METHOD_bounds12] = new MethodDescriptor(java.awt.Component.class.getMethod("bounds", new Class[] {})); // NOI18N
            methods[METHOD_bounds12].setDisplayName ( "" );
            methods[METHOD_checkImage13] = new MethodDescriptor(java.awt.Component.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_checkImage13].setDisplayName ( "" );
            methods[METHOD_checkImage14] = new MethodDescriptor(java.awt.Component.class.getMethod("checkImage", new Class[] {java.awt.Image.class, int.class, int.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_checkImage14].setDisplayName ( "" );
            methods[METHOD_compare15] = new MethodDescriptor(org.visnow.vn.gui.components.NumericTextField.class.getMethod("compare", new Class[] {java.lang.Number.class, java.lang.Number.class})); // NOI18N
            methods[METHOD_compare15].setDisplayName ( "" );
            methods[METHOD_compareStrict16] = new MethodDescriptor(org.visnow.vn.gui.components.NumericTextField.class.getMethod("compareStrict", new Class[] {java.lang.Number.class, java.lang.Number.class})); // NOI18N
            methods[METHOD_compareStrict16].setDisplayName ( "" );
            methods[METHOD_compareTo17] = new MethodDescriptor(org.visnow.vn.gui.components.NumericTextField.class.getMethod("compareTo", new Class[] {java.lang.Object.class})); // NOI18N
            methods[METHOD_compareTo17].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect18] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_computeVisibleRect18].setDisplayName ( "" );
            methods[METHOD_contains19] = new MethodDescriptor(java.awt.Component.class.getMethod("contains", new Class[] {java.awt.Point.class})); // NOI18N
            methods[METHOD_contains19].setDisplayName ( "" );
            methods[METHOD_contains20] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("contains", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_contains20].setDisplayName ( "" );
            methods[METHOD_convertToDouble21] = new MethodDescriptor(org.visnow.vn.gui.components.NumericTextField.class.getMethod("convertToDouble", new Class[] {org.visnow.vn.gui.components.NumericTextField.FieldType.class, java.lang.Number.class})); // NOI18N
            methods[METHOD_convertToDouble21].setDisplayName ( "" );
            methods[METHOD_copy22] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("copy", new Class[] {})); // NOI18N
            methods[METHOD_copy22].setDisplayName ( "" );
            methods[METHOD_countComponents23] = new MethodDescriptor(java.awt.Container.class.getMethod("countComponents", new Class[] {})); // NOI18N
            methods[METHOD_countComponents23].setDisplayName ( "" );
            methods[METHOD_createDoubleFromObject24] = new MethodDescriptor(org.visnow.vn.gui.components.NumericTextField.class.getMethod("createDoubleFromObject", new Class[] {java.lang.Number.class})); // NOI18N
            methods[METHOD_createDoubleFromObject24].setDisplayName ( "" );
            methods[METHOD_createDoubleFromObject25] = new MethodDescriptor(org.visnow.vn.gui.components.NumericTextField.class.getMethod("createDoubleFromObject", new Class[] {org.visnow.vn.gui.components.NumericTextField.FieldType.class, java.lang.Number.class})); // NOI18N
            methods[METHOD_createDoubleFromObject25].setDisplayName ( "" );
            methods[METHOD_createImage26] = new MethodDescriptor(java.awt.Component.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class})); // NOI18N
            methods[METHOD_createImage26].setDisplayName ( "" );
            methods[METHOD_createImage27] = new MethodDescriptor(java.awt.Component.class.getMethod("createImage", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_createImage27].setDisplayName ( "" );
            methods[METHOD_createLongFromObject28] = new MethodDescriptor(org.visnow.vn.gui.components.NumericTextField.class.getMethod("createLongFromObject", new Class[] {java.lang.Number.class})); // NOI18N
            methods[METHOD_createLongFromObject28].setDisplayName ( "" );
            methods[METHOD_createLongFromObject29] = new MethodDescriptor(org.visnow.vn.gui.components.NumericTextField.class.getMethod("createLongFromObject", new Class[] {org.visnow.vn.gui.components.NumericTextField.FieldType.class, java.lang.Number.class})); // NOI18N
            methods[METHOD_createLongFromObject29].setDisplayName ( "" );
            methods[METHOD_createToolTip30] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("createToolTip", new Class[] {})); // NOI18N
            methods[METHOD_createToolTip30].setDisplayName ( "" );
            methods[METHOD_createVolatileImage31] = new MethodDescriptor(java.awt.Component.class.getMethod("createVolatileImage", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_createVolatileImage31].setDisplayName ( "" );
            methods[METHOD_createVolatileImage32] = new MethodDescriptor(java.awt.Component.class.getMethod("createVolatileImage", new Class[] {int.class, int.class, java.awt.ImageCapabilities.class})); // NOI18N
            methods[METHOD_createVolatileImage32].setDisplayName ( "" );
            methods[METHOD_cut33] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("cut", new Class[] {})); // NOI18N
            methods[METHOD_cut33].setDisplayName ( "" );
            methods[METHOD_deliverEvent34] = new MethodDescriptor(java.awt.Container.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_deliverEvent34].setDisplayName ( "" );
            methods[METHOD_disable35] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("disable", new Class[] {})); // NOI18N
            methods[METHOD_disable35].setDisplayName ( "" );
            methods[METHOD_dispatchEvent36] = new MethodDescriptor(java.awt.Component.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class})); // NOI18N
            methods[METHOD_dispatchEvent36].setDisplayName ( "" );
            methods[METHOD_doLayout37] = new MethodDescriptor(java.awt.Container.class.getMethod("doLayout", new Class[] {})); // NOI18N
            methods[METHOD_doLayout37].setDisplayName ( "" );
            methods[METHOD_enable38] = new MethodDescriptor(java.awt.Component.class.getMethod("enable", new Class[] {boolean.class})); // NOI18N
            methods[METHOD_enable38].setDisplayName ( "" );
            methods[METHOD_enable39] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("enable", new Class[] {})); // NOI18N
            methods[METHOD_enable39].setDisplayName ( "" );
            methods[METHOD_enableInputMethods40] = new MethodDescriptor(java.awt.Component.class.getMethod("enableInputMethods", new Class[] {boolean.class})); // NOI18N
            methods[METHOD_enableInputMethods40].setDisplayName ( "" );
            methods[METHOD_findComponentAt41] = new MethodDescriptor(java.awt.Container.class.getMethod("findComponentAt", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_findComponentAt41].setDisplayName ( "" );
            methods[METHOD_findComponentAt42] = new MethodDescriptor(java.awt.Container.class.getMethod("findComponentAt", new Class[] {java.awt.Point.class})); // NOI18N
            methods[METHOD_findComponentAt42].setDisplayName ( "" );
            methods[METHOD_firePropertyChange43] = new MethodDescriptor(java.awt.Component.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, byte.class, byte.class})); // NOI18N
            methods[METHOD_firePropertyChange43].setDisplayName ( "" );
            methods[METHOD_firePropertyChange44] = new MethodDescriptor(java.awt.Component.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, short.class, short.class})); // NOI18N
            methods[METHOD_firePropertyChange44].setDisplayName ( "" );
            methods[METHOD_firePropertyChange45] = new MethodDescriptor(java.awt.Component.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, long.class, long.class})); // NOI18N
            methods[METHOD_firePropertyChange45].setDisplayName ( "" );
            methods[METHOD_firePropertyChange46] = new MethodDescriptor(java.awt.Component.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, float.class, float.class})); // NOI18N
            methods[METHOD_firePropertyChange46].setDisplayName ( "" );
            methods[METHOD_firePropertyChange47] = new MethodDescriptor(java.awt.Component.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, double.class, double.class})); // NOI18N
            methods[METHOD_firePropertyChange47].setDisplayName ( "" );
            methods[METHOD_firePropertyChange48] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, boolean.class, boolean.class})); // NOI18N
            methods[METHOD_firePropertyChange48].setDisplayName ( "" );
            methods[METHOD_firePropertyChange49] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, int.class, int.class})); // NOI18N
            methods[METHOD_firePropertyChange49].setDisplayName ( "" );
            methods[METHOD_firePropertyChange50] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, char.class, char.class})); // NOI18N
            methods[METHOD_firePropertyChange50].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke51] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_getActionForKeyStroke51].setDisplayName ( "" );
            methods[METHOD_getBaseline52] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getBaseline", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_getBaseline52].setDisplayName ( "" );
            methods[METHOD_getBounds53] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_getBounds53].setDisplayName ( "" );
            methods[METHOD_getClientProperty54] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class})); // NOI18N
            methods[METHOD_getClientProperty54].setDisplayName ( "" );
            methods[METHOD_getComponentAt55] = new MethodDescriptor(java.awt.Container.class.getMethod("getComponentAt", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_getComponentAt55].setDisplayName ( "" );
            methods[METHOD_getComponentAt56] = new MethodDescriptor(java.awt.Container.class.getMethod("getComponentAt", new Class[] {java.awt.Point.class})); // NOI18N
            methods[METHOD_getComponentAt56].setDisplayName ( "" );
            methods[METHOD_getComponentZOrder57] = new MethodDescriptor(java.awt.Container.class.getMethod("getComponentZOrder", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_getComponentZOrder57].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke58] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_getConditionForKeyStroke58].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale59] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getDefaultLocale", new Class[] {})); // NOI18N
            methods[METHOD_getDefaultLocale59].setDisplayName ( "" );
            methods[METHOD_getFocusTraversalKeys60] = new MethodDescriptor(java.awt.Container.class.getMethod("getFocusTraversalKeys", new Class[] {int.class})); // NOI18N
            methods[METHOD_getFocusTraversalKeys60].setDisplayName ( "" );
            methods[METHOD_getFontMetrics61] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class})); // NOI18N
            methods[METHOD_getFontMetrics61].setDisplayName ( "" );
            methods[METHOD_getInsets62] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getInsets", new Class[] {java.awt.Insets.class})); // NOI18N
            methods[METHOD_getInsets62].setDisplayName ( "" );
            methods[METHOD_getKeymap63] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("getKeymap", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_getKeymap63].setDisplayName ( "" );
            methods[METHOD_getListeners64] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getListeners", new Class[] {java.lang.Class.class})); // NOI18N
            methods[METHOD_getListeners64].setDisplayName ( "" );
            methods[METHOD_getLocation65] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getLocation", new Class[] {java.awt.Point.class})); // NOI18N
            methods[METHOD_getLocation65].setDisplayName ( "" );
            methods[METHOD_getMinMax66] = new MethodDescriptor(org.visnow.vn.gui.components.NumericTextField.class.getMethod("getMinMax", new Class[] {org.visnow.vn.gui.components.NumericTextField.FieldType.class})); // NOI18N
            methods[METHOD_getMinMax66].setDisplayName ( "" );
            methods[METHOD_getMousePosition67] = new MethodDescriptor(java.awt.Container.class.getMethod("getMousePosition", new Class[] {boolean.class})); // NOI18N
            methods[METHOD_getMousePosition67].setDisplayName ( "" );
            methods[METHOD_getPopupLocation68] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getPopupLocation", new Class[] {java.awt.event.MouseEvent.class})); // NOI18N
            methods[METHOD_getPopupLocation68].setDisplayName ( "" );
            methods[METHOD_getPrintable69] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("getPrintable", new Class[] {java.text.MessageFormat.class, java.text.MessageFormat.class})); // NOI18N
            methods[METHOD_getPrintable69].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners70] = new MethodDescriptor(java.awt.Component.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_getPropertyChangeListeners70].setDisplayName ( "" );
            methods[METHOD_getScrollableBlockIncrement71] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("getScrollableBlockIncrement", new Class[] {java.awt.Rectangle.class, int.class, int.class})); // NOI18N
            methods[METHOD_getScrollableBlockIncrement71].setDisplayName ( "" );
            methods[METHOD_getScrollableUnitIncrement72] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("getScrollableUnitIncrement", new Class[] {java.awt.Rectangle.class, int.class, int.class})); // NOI18N
            methods[METHOD_getScrollableUnitIncrement72].setDisplayName ( "" );
            methods[METHOD_getSize73] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getSize", new Class[] {java.awt.Dimension.class})); // NOI18N
            methods[METHOD_getSize73].setDisplayName ( "" );
            methods[METHOD_getText74] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("getText", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_getText74].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation75] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class})); // NOI18N
            methods[METHOD_getToolTipLocation75].setDisplayName ( "" );
            methods[METHOD_getToolTipText76] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class})); // NOI18N
            methods[METHOD_getToolTipText76].setDisplayName ( "" );
            methods[METHOD_getType77] = new MethodDescriptor(org.visnow.vn.gui.components.NumericTextField.class.getMethod("getType", new Class[] {java.lang.Number.class})); // NOI18N
            methods[METHOD_getType77].setDisplayName ( "" );
            methods[METHOD_gotFocus78] = new MethodDescriptor(java.awt.Component.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_gotFocus78].setDisplayName ( "" );
            methods[METHOD_grabFocus79] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("grabFocus", new Class[] {})); // NOI18N
            methods[METHOD_grabFocus79].setDisplayName ( "" );
            methods[METHOD_handleEvent80] = new MethodDescriptor(java.awt.Component.class.getMethod("handleEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_handleEvent80].setDisplayName ( "" );
            methods[METHOD_hasFocus81] = new MethodDescriptor(java.awt.Component.class.getMethod("hasFocus", new Class[] {})); // NOI18N
            methods[METHOD_hasFocus81].setDisplayName ( "" );
            methods[METHOD_hide82] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("hide", new Class[] {})); // NOI18N
            methods[METHOD_hide82].setDisplayName ( "" );
            methods[METHOD_imageUpdate83] = new MethodDescriptor(java.awt.Component.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, int.class, int.class, int.class, int.class, int.class})); // NOI18N
            methods[METHOD_imageUpdate83].setDisplayName ( "" );
            methods[METHOD_insets84] = new MethodDescriptor(java.awt.Container.class.getMethod("insets", new Class[] {})); // NOI18N
            methods[METHOD_insets84].setDisplayName ( "" );
            methods[METHOD_inside85] = new MethodDescriptor(java.awt.Component.class.getMethod("inside", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_inside85].setDisplayName ( "" );
            methods[METHOD_invalidate86] = new MethodDescriptor(java.awt.Container.class.getMethod("invalidate", new Class[] {})); // NOI18N
            methods[METHOD_invalidate86].setDisplayName ( "" );
            methods[METHOD_isAncestorOf87] = new MethodDescriptor(java.awt.Container.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_isAncestorOf87].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot88] = new MethodDescriptor(java.awt.Container.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class})); // NOI18N
            methods[METHOD_isFocusCycleRoot88].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent89] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_isLightweightComponent89].setDisplayName ( "" );
            methods[METHOD_keyDown90] = new MethodDescriptor(java.awt.Component.class.getMethod("keyDown", new Class[] {java.awt.Event.class, int.class})); // NOI18N
            methods[METHOD_keyDown90].setDisplayName ( "" );
            methods[METHOD_keyUp91] = new MethodDescriptor(java.awt.Component.class.getMethod("keyUp", new Class[] {java.awt.Event.class, int.class})); // NOI18N
            methods[METHOD_keyUp91].setDisplayName ( "" );
            methods[METHOD_layout92] = new MethodDescriptor(java.awt.Container.class.getMethod("layout", new Class[] {})); // NOI18N
            methods[METHOD_layout92].setDisplayName ( "" );
            methods[METHOD_list93] = new MethodDescriptor(java.awt.Component.class.getMethod("list", new Class[] {})); // NOI18N
            methods[METHOD_list93].setDisplayName ( "" );
            methods[METHOD_list94] = new MethodDescriptor(java.awt.Component.class.getMethod("list", new Class[] {java.io.PrintStream.class})); // NOI18N
            methods[METHOD_list94].setDisplayName ( "" );
            methods[METHOD_list95] = new MethodDescriptor(java.awt.Component.class.getMethod("list", new Class[] {java.io.PrintWriter.class})); // NOI18N
            methods[METHOD_list95].setDisplayName ( "" );
            methods[METHOD_list96] = new MethodDescriptor(java.awt.Container.class.getMethod("list", new Class[] {java.io.PrintStream.class, int.class})); // NOI18N
            methods[METHOD_list96].setDisplayName ( "" );
            methods[METHOD_list97] = new MethodDescriptor(java.awt.Container.class.getMethod("list", new Class[] {java.io.PrintWriter.class, int.class})); // NOI18N
            methods[METHOD_list97].setDisplayName ( "" );
            methods[METHOD_loadKeymap98] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("loadKeymap", new Class[] {javax.swing.text.Keymap.class, javax.swing.text.JTextComponent.KeyBinding[].class, javax.swing.Action[].class})); // NOI18N
            methods[METHOD_loadKeymap98].setDisplayName ( "" );
            methods[METHOD_locate99] = new MethodDescriptor(java.awt.Container.class.getMethod("locate", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_locate99].setDisplayName ( "" );
            methods[METHOD_location100] = new MethodDescriptor(java.awt.Component.class.getMethod("location", new Class[] {})); // NOI18N
            methods[METHOD_location100].setDisplayName ( "" );
            methods[METHOD_lostFocus101] = new MethodDescriptor(java.awt.Component.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_lostFocus101].setDisplayName ( "" );
            methods[METHOD_minimumSize102] = new MethodDescriptor(java.awt.Container.class.getMethod("minimumSize", new Class[] {})); // NOI18N
            methods[METHOD_minimumSize102].setDisplayName ( "" );
            methods[METHOD_modelToView103] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("modelToView", new Class[] {int.class})); // NOI18N
            methods[METHOD_modelToView103].setDisplayName ( "" );
            methods[METHOD_mouseDown104] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, int.class, int.class})); // NOI18N
            methods[METHOD_mouseDown104].setDisplayName ( "" );
            methods[METHOD_mouseDrag105] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, int.class, int.class})); // NOI18N
            methods[METHOD_mouseDrag105].setDisplayName ( "" );
            methods[METHOD_mouseEnter106] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, int.class, int.class})); // NOI18N
            methods[METHOD_mouseEnter106].setDisplayName ( "" );
            methods[METHOD_mouseExit107] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, int.class, int.class})); // NOI18N
            methods[METHOD_mouseExit107].setDisplayName ( "" );
            methods[METHOD_mouseMove108] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, int.class, int.class})); // NOI18N
            methods[METHOD_mouseMove108].setDisplayName ( "" );
            methods[METHOD_mouseUp109] = new MethodDescriptor(java.awt.Component.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, int.class, int.class})); // NOI18N
            methods[METHOD_mouseUp109].setDisplayName ( "" );
            methods[METHOD_move110] = new MethodDescriptor(java.awt.Component.class.getMethod("move", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_move110].setDisplayName ( "" );
            methods[METHOD_moveCaretPosition111] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("moveCaretPosition", new Class[] {int.class})); // NOI18N
            methods[METHOD_moveCaretPosition111].setDisplayName ( "" );
            methods[METHOD_nextFocus112] = new MethodDescriptor(java.awt.Component.class.getMethod("nextFocus", new Class[] {})); // NOI18N
            methods[METHOD_nextFocus112].setDisplayName ( "" );
            methods[METHOD_paint113] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("paint", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paint113].setDisplayName ( "" );
            methods[METHOD_paintAll114] = new MethodDescriptor(java.awt.Component.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paintAll114].setDisplayName ( "" );
            methods[METHOD_paintComponents115] = new MethodDescriptor(java.awt.Container.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paintComponents115].setDisplayName ( "" );
            methods[METHOD_paintImmediately116] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("paintImmediately", new Class[] {int.class, int.class, int.class, int.class})); // NOI18N
            methods[METHOD_paintImmediately116].setDisplayName ( "" );
            methods[METHOD_paintImmediately117] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("paintImmediately", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_paintImmediately117].setDisplayName ( "" );
            methods[METHOD_paste118] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("paste", new Class[] {})); // NOI18N
            methods[METHOD_paste118].setDisplayName ( "" );
            methods[METHOD_postActionEvent119] = new MethodDescriptor(javax.swing.JTextField.class.getMethod("postActionEvent", new Class[] {})); // NOI18N
            methods[METHOD_postActionEvent119].setDisplayName ( "" );
            methods[METHOD_postEvent120] = new MethodDescriptor(java.awt.Component.class.getMethod("postEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_postEvent120].setDisplayName ( "" );
            methods[METHOD_preferredSize121] = new MethodDescriptor(java.awt.Container.class.getMethod("preferredSize", new Class[] {})); // NOI18N
            methods[METHOD_preferredSize121].setDisplayName ( "" );
            methods[METHOD_prepareImage122] = new MethodDescriptor(java.awt.Component.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_prepareImage122].setDisplayName ( "" );
            methods[METHOD_prepareImage123] = new MethodDescriptor(java.awt.Component.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, int.class, int.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_prepareImage123].setDisplayName ( "" );
            methods[METHOD_print124] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("print", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_print124].setDisplayName ( "" );
            methods[METHOD_print125] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("print", new Class[] {})); // NOI18N
            methods[METHOD_print125].setDisplayName ( "" );
            methods[METHOD_print126] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("print", new Class[] {java.text.MessageFormat.class, java.text.MessageFormat.class})); // NOI18N
            methods[METHOD_print126].setDisplayName ( "" );
            methods[METHOD_print127] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("print", new Class[] {java.text.MessageFormat.class, java.text.MessageFormat.class, boolean.class, javax.print.PrintService.class, javax.print.attribute.PrintRequestAttributeSet.class, boolean.class})); // NOI18N
            methods[METHOD_print127].setDisplayName ( "" );
            methods[METHOD_printAll128] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("printAll", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_printAll128].setDisplayName ( "" );
            methods[METHOD_printComponents129] = new MethodDescriptor(java.awt.Container.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_printComponents129].setDisplayName ( "" );
            methods[METHOD_putClientProperty130] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_putClientProperty130].setDisplayName ( "" );
            methods[METHOD_read131] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("read", new Class[] {java.io.Reader.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_read131].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction132] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, int.class})); // NOI18N
            methods[METHOD_registerKeyboardAction132].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction133] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, javax.swing.KeyStroke.class, int.class})); // NOI18N
            methods[METHOD_registerKeyboardAction133].setDisplayName ( "" );
            methods[METHOD_remove134] = new MethodDescriptor(java.awt.Component.class.getMethod("remove", new Class[] {java.awt.MenuComponent.class})); // NOI18N
            methods[METHOD_remove134].setDisplayName ( "" );
            methods[METHOD_remove135] = new MethodDescriptor(java.awt.Container.class.getMethod("remove", new Class[] {int.class})); // NOI18N
            methods[METHOD_remove135].setDisplayName ( "" );
            methods[METHOD_remove136] = new MethodDescriptor(java.awt.Container.class.getMethod("remove", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_remove136].setDisplayName ( "" );
            methods[METHOD_removeAll137] = new MethodDescriptor(java.awt.Container.class.getMethod("removeAll", new Class[] {})); // NOI18N
            methods[METHOD_removeAll137].setDisplayName ( "" );
            methods[METHOD_removeKeymap138] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("removeKeymap", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_removeKeymap138].setDisplayName ( "" );
            methods[METHOD_removeNotify139] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("removeNotify", new Class[] {})); // NOI18N
            methods[METHOD_removeNotify139].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener140] = new MethodDescriptor(java.awt.Component.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class})); // NOI18N
            methods[METHOD_removePropertyChangeListener140].setDisplayName ( "" );
            methods[METHOD_repaint141] = new MethodDescriptor(java.awt.Component.class.getMethod("repaint", new Class[] {})); // NOI18N
            methods[METHOD_repaint141].setDisplayName ( "" );
            methods[METHOD_repaint142] = new MethodDescriptor(java.awt.Component.class.getMethod("repaint", new Class[] {long.class})); // NOI18N
            methods[METHOD_repaint142].setDisplayName ( "" );
            methods[METHOD_repaint143] = new MethodDescriptor(java.awt.Component.class.getMethod("repaint", new Class[] {int.class, int.class, int.class, int.class})); // NOI18N
            methods[METHOD_repaint143].setDisplayName ( "" );
            methods[METHOD_repaint144] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("repaint", new Class[] {long.class, int.class, int.class, int.class, int.class})); // NOI18N
            methods[METHOD_repaint144].setDisplayName ( "" );
            methods[METHOD_repaint145] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("repaint", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_repaint145].setDisplayName ( "" );
            methods[METHOD_replaceSelection146] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("replaceSelection", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_replaceSelection146].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus147] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("requestDefaultFocus", new Class[] {})); // NOI18N
            methods[METHOD_requestDefaultFocus147].setDisplayName ( "" );
            methods[METHOD_requestFocus148] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("requestFocus", new Class[] {})); // NOI18N
            methods[METHOD_requestFocus148].setDisplayName ( "" );
            methods[METHOD_requestFocus149] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("requestFocus", new Class[] {boolean.class})); // NOI18N
            methods[METHOD_requestFocus149].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow150] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("requestFocusInWindow", new Class[] {})); // NOI18N
            methods[METHOD_requestFocusInWindow150].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions151] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("resetKeyboardActions", new Class[] {})); // NOI18N
            methods[METHOD_resetKeyboardActions151].setDisplayName ( "" );
            methods[METHOD_reshape152] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("reshape", new Class[] {int.class, int.class, int.class, int.class})); // NOI18N
            methods[METHOD_reshape152].setDisplayName ( "" );
            methods[METHOD_resize153] = new MethodDescriptor(java.awt.Component.class.getMethod("resize", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_resize153].setDisplayName ( "" );
            methods[METHOD_resize154] = new MethodDescriptor(java.awt.Component.class.getMethod("resize", new Class[] {java.awt.Dimension.class})); // NOI18N
            methods[METHOD_resize154].setDisplayName ( "" );
            methods[METHOD_revalidate155] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("revalidate", new Class[] {})); // NOI18N
            methods[METHOD_revalidate155].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible156] = new MethodDescriptor(javax.swing.JTextField.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_scrollRectToVisible156].setDisplayName ( "" );
            methods[METHOD_select157] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("select", new Class[] {int.class, int.class})); // NOI18N
            methods[METHOD_select157].setDisplayName ( "" );
            methods[METHOD_selectAll158] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("selectAll", new Class[] {})); // NOI18N
            methods[METHOD_selectAll158].setDisplayName ( "" );
            methods[METHOD_setBounds159] = new MethodDescriptor(java.awt.Component.class.getMethod("setBounds", new Class[] {int.class, int.class, int.class, int.class})); // NOI18N
            methods[METHOD_setBounds159].setDisplayName ( "" );
            methods[METHOD_setComponentZOrder160] = new MethodDescriptor(java.awt.Container.class.getMethod("setComponentZOrder", new Class[] {java.awt.Component.class, int.class})); // NOI18N
            methods[METHOD_setComponentZOrder160].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale161] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class})); // NOI18N
            methods[METHOD_setDefaultLocale161].setDisplayName ( "" );
            methods[METHOD_setValue162] = new MethodDescriptor(org.visnow.vn.gui.components.NumericTextField.class.getMethod("setValue", new Class[] {java.lang.Object.class, java.lang.String.class})); // NOI18N
            methods[METHOD_setValue162].setDisplayName ( "" );
            methods[METHOD_show163] = new MethodDescriptor(java.awt.Component.class.getMethod("show", new Class[] {})); // NOI18N
            methods[METHOD_show163].setDisplayName ( "" );
            methods[METHOD_show164] = new MethodDescriptor(java.awt.Component.class.getMethod("show", new Class[] {boolean.class})); // NOI18N
            methods[METHOD_show164].setDisplayName ( "" );
            methods[METHOD_size165] = new MethodDescriptor(java.awt.Component.class.getMethod("size", new Class[] {})); // NOI18N
            methods[METHOD_size165].setDisplayName ( "" );
            methods[METHOD_toString166] = new MethodDescriptor(java.awt.Component.class.getMethod("toString", new Class[] {})); // NOI18N
            methods[METHOD_toString166].setDisplayName ( "" );
            methods[METHOD_transferFocus167] = new MethodDescriptor(java.awt.Component.class.getMethod("transferFocus", new Class[] {})); // NOI18N
            methods[METHOD_transferFocus167].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward168] = new MethodDescriptor(java.awt.Component.class.getMethod("transferFocusBackward", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusBackward168].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle169] = new MethodDescriptor(java.awt.Container.class.getMethod("transferFocusDownCycle", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusDownCycle169].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle170] = new MethodDescriptor(java.awt.Component.class.getMethod("transferFocusUpCycle", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusUpCycle170].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction171] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_unregisterKeyboardAction171].setDisplayName ( "" );
            methods[METHOD_update172] = new MethodDescriptor(javax.swing.JComponent.class.getMethod("update", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_update172].setDisplayName ( "" );
            methods[METHOD_updateUI173] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("updateUI", new Class[] {})); // NOI18N
            methods[METHOD_updateUI173].setDisplayName ( "" );
            methods[METHOD_validate174] = new MethodDescriptor(java.awt.Container.class.getMethod("validate", new Class[] {})); // NOI18N
            methods[METHOD_validate174].setDisplayName ( "" );
            methods[METHOD_viewToModel175] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("viewToModel", new Class[] {java.awt.Point.class})); // NOI18N
            methods[METHOD_viewToModel175].setDisplayName ( "" );
            methods[METHOD_write176] = new MethodDescriptor(javax.swing.text.JTextComponent.class.getMethod("write", new Class[] {java.io.Writer.class})); // NOI18N
            methods[METHOD_write176].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
    // Here you can add code for customizing the methods array.

        return methods;     }//GEN-LAST:Methods

    private static java.awt.Image iconColor16 = null;//GEN-BEGIN:IconsDef
    private static java.awt.Image iconColor32 = null;
    private static java.awt.Image iconMono16 = null;
    private static java.awt.Image iconMono32 = null;//GEN-END:IconsDef
    private static String iconNameC16 = null;//GEN-BEGIN:Icons
    private static String iconNameC32 = null;
    private static String iconNameM16 = null;
    private static String iconNameM32 = null;//GEN-END:Icons

    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx


//GEN-FIRST:Superclass
    // Here you can add code for customizing the Superclass BeanInfo.

//GEN-LAST:Superclass
    /**
     * Gets the bean's <code>BeanDescriptor</code>s.
     * <p>
     * @return BeanDescriptor describing the editable
     *         properties of this bean. May return null if the
     *         information should be obtained by automatic analysis.
     */
    @Override
    public BeanDescriptor getBeanDescriptor()
    {
        return getBdescriptor();
    }

    /**
     * Gets the bean's <code>PropertyDescriptor</code>s.
     * <p>
     * @return An array of PropertyDescriptors describing the editable
     *         properties supported by this bean. May return null if the
     *         information should be obtained by automatic analysis.
     * <p>
     * If a property is indexed, then its entry in the result array will
     * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
     * A client of getPropertyDescriptors can use "instanceof" to check
     * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
     */
    @Override
    public PropertyDescriptor[] getPropertyDescriptors()
    {
        return getPdescriptor();
    }

    /**
     * Gets the bean's <code>EventSetDescriptor</code>s.
     * <p>
     * @return An array of EventSetDescriptors describing the kinds of
     *         events fired by this bean. May return null if the information
     *         should be obtained by automatic analysis.
     */
    @Override
    public EventSetDescriptor[] getEventSetDescriptors()
    {
        return getEdescriptor();
    }

    /**
     * Gets the bean's <code>MethodDescriptor</code>s.
     * <p>
     * @return An array of MethodDescriptors describing the methods
     *         implemented by this bean. May return null if the information
     *         should be obtained by automatic analysis.
     */
    @Override
    public MethodDescriptor[] getMethodDescriptors()
    {
        return getMdescriptor();
    }

    /**
     * A bean may have a "default" property that is the property that will
     * mostly commonly be initially chosen for update by human's who are
     * customizing the bean.
     * <p>
     * @return Index of default property in the PropertyDescriptor array
     *         returned by getPropertyDescriptors.
     * <P>
     * Returns -1 if there is no default property.
     */
    @Override
    public int getDefaultPropertyIndex()
    {
        return defaultPropertyIndex;
    }

    /**
     * A bean may have a "default" event that is the event that will
     * mostly commonly be used by human's when using the bean.
     * <p>
     * @return Index of default event in the EventSetDescriptor array
     *         returned by getEventSetDescriptors.
     * <P>
     * Returns -1 if there is no default event.
     */
    @Override
    public int getDefaultEventIndex()
    {
        return defaultEventIndex;
    }

    /**
     * This method returns an image object that can be used to
     * represent the bean in toolboxes, toolbars, etc. Icon images
     * will typically be GIFs, but may in future include other formats.
     * <p>
     * Beans aren't required to provide icons and may return null from
     * this method.
     * <p>
     * There are four possible flavors of icons (16x16 color,
     * 32x32 color, 16x16 mono, 32x32 mono). If a bean choses to only
     * support a single icon we recommend supporting 16x16 color.
     * <p>
     * We recommend that icons have a "transparent" background
     * so they can be rendered onto an existing background.
     *
     * @param iconKind The kind of icon requested. This should be
     *                 one of the constant values ICON_COLOR_16x16, ICON_COLOR_32x32,
     *                 ICON_MONO_16x16, or ICON_MONO_32x32.
     * <p>
     * @return An image object representing the requested icon. May
     *         return null if no suitable icon is available.
     */
    @Override
    public java.awt.Image getIcon(int iconKind)
    {
        switch (iconKind) {
            case ICON_COLOR_16x16:
                if (iconNameC16 == null)
                    return null;
                else {
                    if (iconColor16 == null)
                        iconColor16 = loadImage(iconNameC16);
                    return iconColor16;
                }
            case ICON_COLOR_32x32:
                if (iconNameC32 == null)
                    return null;
                else {
                    if (iconColor32 == null)
                        iconColor32 = loadImage(iconNameC32);
                    return iconColor32;
                }
            case ICON_MONO_16x16:
                if (iconNameM16 == null)
                    return null;
                else {
                    if (iconMono16 == null)
                        iconMono16 = loadImage(iconNameM16);
                    return iconMono16;
                }
            case ICON_MONO_32x32:
                if (iconNameM32 == null)
                    return null;
                else {
                    if (iconMono32 == null)
                        iconMono32 = loadImage(iconNameM32);
                    return iconMono32;
                }
            default:
                return null;
        }
    }
    
}
