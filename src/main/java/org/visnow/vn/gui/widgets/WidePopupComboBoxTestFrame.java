/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets;

import org.apache.log4j.Logger;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author szpak
 */
public class WidePopupComboBoxTestFrame extends javax.swing.JFrame
{
    private static final Logger LOGGER = Logger.getLogger(WidePopupComboBoxTestFrame.class);

    /**
     * Creates new form WidePopupComboBoxTestFrame
     */
    public WidePopupComboBoxTestFrame()
    {
        initComponents();
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        widePopupComboBox1 = new org.visnow.vn.gui.widgets.WidePopupComboBox();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        widePopupComboBox1.setListData(new String[] {"asd", "fdfdf", "23423423423423"});
        widePopupComboBox1.addUserActionListener(new org.visnow.vn.gui.swingwrappers.UserActionListener()
        {
            public void userChangeAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
                widePopupComboBox1UserChangeAction(evt);
            }
            public void userAction(org.visnow.vn.gui.swingwrappers.UserEvent evt)
            {
            }
        });
        getContentPane().add(widePopupComboBox1, new java.awt.GridBagConstraints());

        jButton1.setText("set");
        jButton1.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new java.awt.GridBagConstraints());

        jButton2.setText("reset");
        jButton2.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new java.awt.GridBagConstraints());

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton2ActionPerformed
    {//GEN-HEADEREND:event_jButton2ActionPerformed
        widePopupComboBox1.setListData(new String[]{});
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton1ActionPerformed
    {//GEN-HEADEREND:event_jButton1ActionPerformed
        widePopupComboBox1.setListData(new String[]{"asd fas fdsad fklsad flksaj hfd", "flksaj hfd", "asdf", "sssssssssssssssssssssssssssssssssssss"});
    }//GEN-LAST:event_jButton1ActionPerformed

    private void widePopupComboBox1UserChangeAction(java.util.EventObject evt)//GEN-FIRST:event_widePopupComboBox1UserChangeAction
    {//GEN-HEADEREND:event_widePopupComboBox1UserChangeAction
        LOGGER.debug("User change: " + widePopupComboBox1.getSelectedItem());
    }//GEN-LAST:event_widePopupComboBox1UserChangeAction

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        VisNow.initLogging(true);
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new WidePopupComboBoxTestFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private org.visnow.vn.gui.widgets.WidePopupComboBox widePopupComboBox1;
    // End of variables declaration//GEN-END:variables
}
