/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jogamp.vecmath.Color3f;
import org.visnow.vn.gui.icons.IconsContainer;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class SinglePanelColorEditor extends JPanel
{

    private static final long serialVersionUID = -4924595748344960000L;
    protected Color basicColor = Color.WHITE;
    protected BufferedImage img;
    protected int brightness = 50;
    protected int lastBrightness = 0;
    protected int lastHue = 300;
    protected int lastSat = 0;
    protected int lastButton = 0;
    protected int lastX = 0, lastY = 0;
    protected String title = "";
    protected Color color = new Color(.71f, .71f, .71f);
    protected Color3f color3f = new Color3f(.71f, .71f, .71f);
    protected float hue = 3;
    protected float sat = 0;
    protected boolean adjusting = false;
    protected JPanel colorSelector = new JPanel()
    {
        @Override
        public void paint(Graphics g)
        {
            g.drawImage(img, 0, 0, getWidth(), getHeight(), null);
        }
    };

    /**
     * Creates new form ColorEditor
     */
    public SinglePanelColorEditor()
    {
        initComponents();
        img = IconsContainer.getLightColorTable();
        setBackground(color);
        colorSelector.addMouseListener(new java.awt.event.MouseAdapter()
        {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                int x = (evt.getX() * img.getWidth()) / colorSelector.getWidth();
                if (x >= img.getWidth())
                    x = img.getWidth() - 1;
                int y = (evt.getY() * img.getHeight()) / colorSelector.getHeight();
                if (y >= img.getHeight())
                    y = img.getHeight() - 1;
                int c = img.getRGB(x, y);
                basicColor = new Color((c & 0x00ff0000) >> 16, (c & 0x0000ff00) >> 8, c & 0x000000ff);
                brightness = max(min(100, brightness), 0);
                float b = (float) sqrt(brightness / 100.);
                color = new Color(basicColor.getRed() * b / 255,
                                  basicColor.getGreen() * b / 255,
                                  basicColor.getBlue() * b / 255);
                setBackground(color);
                color3f = new Color3f();
                fireStateChanged();
            }
        });
        hueSatPanel.add(colorSelector, BorderLayout.CENTER);
    }

    public void setTitle(String s)
    {
        title = s;
        repaint();
    }

    public void setBasicColor(Color c)
    {
        basicColor = c;
        float b = (float) (brightness / 100.);
        //      float b = (float) sqrt(brightness / 100.);
        color = new Color(c.getRed() * b / 255,
                          c.getGreen() * b / 255,
                          c.getBlue() * b / 255);
        editorPanel.setBackground(color);
        repaint();
    }

    public void setBrightness(int br)
    {
        brightness = br;
        float b = (float) (brightness / 100.);
        //      float b = (float) sqrt(brightness / 100.);
        color = new Color(basicColor.getRed() * b / 255,
                          basicColor.getGreen() * b / 255,
                          basicColor.getBlue() * b / 255);
        editorPanel.setBackground(color);
        repaint();
    }

    public void setColor(Color c)
    {
        float r = c.getRed();
        float g = c.getGreen();
        float b = c.getBlue();
        float m = max(r, max(g, b));
        if (m == 0)
            basicColor = new Color(1, 1, 1);
        else
            basicColor = new Color(r / m, g / m, b / m);
        brightness = Math.max(0, Math.min((int) (100 * m * m),100));
        color = c;
        repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        editorPanel = new javax.swing.JPanel()
        {
            @Override
            public void paint(Graphics g)
            {
                int w = getWidth();
                int h = getHeight();

                if (enabled)
                {
                    g.setColor(color);
                    g.fillRect(0, 0, w, h);
                    g.setColor(Color.LIGHT_GRAY);
                    g.fillRect(40, 2, w - 42, h - 4);
                    w -= 44;
                    h -= 6;
                    g.setColor(basicColor);
                    int r = (brightness * w) / 100;
                    g.fillRect(41, 3, r, h);
                    g.setColor(Color.GRAY);
                    g.fillRect(r + 41, 3, w - r, h);
                    g.setFont(this.getFont());
                    g.setColor(Color.BLACK);
                    g.drawString(title, 22, h / 2 + 10);
                } else
                {
                    g.setColor(Color.LIGHT_GRAY);
                    g.fillRect(0, 0, w, h);
                    g.setColor(new Color(238, 238, 238));
                    g.fillRect(40, 2, w - 42, h - 4);
                    w -= 44;
                    h -= 6;
                    g.setFont(this.getFont());
                    g.setColor(Color.LIGHT_GRAY);
                    g.drawString(title, 22, h / 2 + 10);
                }
            }
        }
        ;
        hueSatPanel = new javax.swing.JPanel();

        setToolTipText("<html>drag left MB to change brightness<p>drag right MB to change hue and saturation<p>click to open color chooser"); // NOI18N
        setMinimumSize(new java.awt.Dimension(200, 130));
        setPreferredSize(new java.awt.Dimension(255, 160));
        setLayout(new java.awt.BorderLayout());

        jPanel1.setName("jPanel1"); // NOI18N
        jPanel1.setLayout(new java.awt.GridBagLayout());

        editorPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        editorPanel.setMinimumSize(new java.awt.Dimension(200, 30));
        editorPanel.setName("editorPanel"); // NOI18N
        editorPanel.setPreferredSize(new java.awt.Dimension(200, 35));
        editorPanel.addMouseMotionListener(new java.awt.event.MouseMotionAdapter()
        {
            public void mouseDragged(java.awt.event.MouseEvent evt)
            {
                editorPanelMouseDragged(evt);
            }
        });
        editorPanel.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mousePressed(java.awt.event.MouseEvent evt)
            {
                editorPanelMousePressed(evt);
            }
        });
        editorPanel.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyReleased(java.awt.event.KeyEvent evt)
            {
                editorPanelKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel1.add(editorPanel, gridBagConstraints);

        hueSatPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        hueSatPanel.setMinimumSize(new java.awt.Dimension(252, 152));
        hueSatPanel.setName("hueSatPanel"); // NOI18N
        hueSatPanel.setPreferredSize(new java.awt.Dimension(252, 152));
        hueSatPanel.setLayout(new java.awt.BorderLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        jPanel1.add(hueSatPanel, gridBagConstraints);

        add(jPanel1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void editorPanelMouseDragged(java.awt.event.MouseEvent evt)//GEN-FIRST:event_editorPanelMouseDragged
    {//GEN-HEADEREND:event_editorPanelMouseDragged
        adjusting = true;
        if (lastButton == MouseEvent.BUTTON1) {
            brightness = lastBrightness + (int) (100. * (evt.getX() - lastX) / (getWidth() - 4.));
            if (brightness < 0)
                brightness = 0;
            else if (brightness > 100)
                brightness = 100;
        } else {
            hue = ((lastHue - evt.getX() + lastX + 2400) % 600) / 100.f;
            int s = lastSat + lastY - evt.getY();
            if (s < 0)
                s = 0;
            if (s > 100)
                s = 100;
            sat = s / 100.f;
            int i = (int) hue;
            float f = hue - i;
            float p = 1 - sat;
            float q = 1 - sat * f;
            float t = 1 - sat * (1 - f);
            if (i == 0)
                basicColor = new Color(1, t, p);
            else if (i == 1)
                basicColor = new Color(q, 1, p);
            else if (i == 2)
                basicColor = new Color(p, 1, t);
            else if (i == 3)
                basicColor = new Color(p, q, 1);
            else if (i == 4)
                basicColor = new Color(t, p, 1);
            else if (i == 5)
                basicColor = new Color(1, p, q);
        }
        float b = (float) (brightness / 100.);
        //      float b = (float) sqrt(brightness / 100.);
        color = new Color(basicColor.getRed() * b / 255,
                          basicColor.getGreen() * b / 255,
                          basicColor.getBlue() * b / 255);
        setBackground(color);
        fireStateChanged();
        repaint();
    }//GEN-LAST:event_editorPanelMouseDragged

    private void editorPanelMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_editorPanelMousePressed
    {//GEN-HEADEREND:event_editorPanelMousePressed
        if (!enabled)
            return;
        adjusting = true;
        lastX = evt.getX();
        lastY = evt.getY();
        lastButton = evt.getButton();
        if (evt.getButton() == MouseEvent.BUTTON1)
            lastBrightness = brightness;
        else {
            lastHue = (int) (100 * hue);
            lastSat = (int) (100 * sat);
        }

    }//GEN-LAST:event_editorPanelMousePressed

    private void editorPanelKeyReleased(java.awt.event.KeyEvent evt)//GEN-FIRST:event_editorPanelKeyReleased
    {//GEN-HEADEREND:event_editorPanelKeyReleased
                if (!enabled)
            return;
        adjusting = false;
        fireStateChanged();
    }//GEN-LAST:event_editorPanelKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JPanel editorPanel;
    protected javax.swing.JPanel hueSatPanel;
    protected javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
    public Color getColor()
    {
        return color;
    }

    public int getBrightness()
    {
        return brightness;
    }

    public float[] getColorComponents()
    {
        return new float[]{
            basicColor.getRed() * brightness / 25500.f,
            basicColor.getGreen() * brightness / 25500.f,
            basicColor.getBlue() * brightness / 25500.f
        };
    }

    public boolean isAdjusting()
    {
        return adjusting;
    }

    /**
     * Utility field holding list of ChangeListeners.
     */
    private transient ArrayList<ChangeListener> listeners = new ArrayList<ChangeListener>();

    public void addChangeListener(ChangeListener l)
    {
        listeners.add(l);
    }

    public void removeChangeListener(ChangeListener l)
    {
        listeners.remove(l);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param object Parameter #1 of the <CODE>ChangeEvent<CODE> constructor.
     */
    private void fireStateChanged()
    {
        this.repaint();
        ChangeEvent e = new ChangeEvent(this);
        for (ChangeListener l : listeners)
            l.stateChanged(e);
    }

    private boolean enabled = true;

    @Override
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
        this.enabled = enabled;
    }

    @Override
    public boolean isEnabled()
    {
        return enabled;
    }

    public static void main(String[] args)
    {
        JFrame f = new JFrame();
        f.add(new SinglePanelColorEditor());
        f.pack();
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.setVisible(true);
    }
}
