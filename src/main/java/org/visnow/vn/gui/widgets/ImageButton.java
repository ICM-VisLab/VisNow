/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;

public class ImageButton extends JComponent
{

    public enum State
    {

        NORMAL, CLICKED, SELECTED, SELECTED_CLICKED
    }

    protected State state;
    protected Image image;
    protected Rectangle normalRect;
    protected Rectangle selectedRect;
    protected Rectangle clickedRect;
    protected Rectangle selectedClickedRect;

    public boolean isSelected()
    {
        return (this.state == State.NORMAL) || (this.state == State.CLICKED);
    }

    public ImageButton()
    {
        this.state = State.NORMAL;

        addMouseListener(new MouseAdapter()
        {
            @Override
            public void mousePressed(MouseEvent e)
            {
                switch (state) {
                    case SELECTED:
                        state = State.SELECTED_CLICKED;
                        break;
                    default:
                        state = State.CLICKED;
                }
                repaint();
            }

            @Override
            public void mouseReleased(MouseEvent e)
            {
                switch (state) {
                    case SELECTED_CLICKED:
                        state = State.NORMAL;
                        break;
                    case CLICKED:
                    default:
                        state = State.SELECTED;
                        break;
                }
                repaint();
            }
        });
    }

    public void paintSubImage(Graphics g, Rectangle rect)
    {
        Insets insets;
        if (getBorder() != null) {
            insets = getBorder().getBorderInsets(this);
        } else {
            insets = new Insets(0, 0, 0, 0);
        }

        g.drawImage(image,
                    insets.left, insets.top,
                    rect.width - insets.left - insets.right,
                    rect.height - insets.top - insets.bottom,
                    rect.x, rect.y,
                    rect.x + rect.width, rect.y + rect.height,
                    getBackground(), null);
    }

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);

        if (image != null && normalRect != null) {

            switch (state) {
                case CLICKED:
                    if (clickedRect != null) {
                        paintSubImage(g, clickedRect);
                    } else {
                        paintSubImage(g, normalRect);
                    }
                    break;
                case SELECTED:
                    if (selectedRect != null) {
                        paintSubImage(g, selectedRect);
                    } else {
                        paintSubImage(g, normalRect);
                    }
                    break;
                case SELECTED_CLICKED:
                    if (selectedClickedRect != null) {
                        paintSubImage(g, selectedClickedRect);
                    } else if (selectedRect != null) {
                        paintSubImage(g, selectedRect);
                    } else {
                        paintSubImage(g, normalRect);
                    }
                    break;
                case NORMAL:
                default:
                    paintSubImage(g, normalRect);
            }

        }
    }

    public Rectangle getSelectedClickedRect()
    {
        return selectedClickedRect;
    }

    public void setSelectedClickedRect(Rectangle selectedClickedRect)
    {
        this.selectedClickedRect = selectedClickedRect;
    }

    public Rectangle getClickedRect()
    {
        return clickedRect;
    }

    public void setClickedRect(Rectangle clickedRect)
    {
        this.clickedRect = clickedRect;
    }

    public Rectangle getSelectedRect()
    {
        return selectedRect;
    }

    public void setSelectedRect(Rectangle selectedRect)
    {
        this.selectedRect = selectedRect;
    }

    public Rectangle getNormalRect()
    {
        return normalRect;
    }

    public void setNormalRect(Rectangle normalRect)
    {
        this.normalRect = normalRect;
    }

    public Image getImage()
    {
        return image;
    }

    public void setImage(Image image)
    {
        this.image = image;
    }
}
