/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets.UnboundedRoller;

/*
 * BasicUnboundedIntRollerUI.java
 *
 * Created on April 14, 2004, 10:42 AM
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.plaf.*;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class BasicUnboundedIntRollerUI extends UnboundedIntRollerUI
    implements MouseListener, MouseMotionListener, MouseWheelListener
{

    int rHeight;
    Insets d;
    boolean valueAdj = false;
    Graphics g;
    JComponent c;
    int width = 40, height = 12;

    int startPos;
    int startVal = 0;
    int lastVal = 0;
    int lastX, lastY;
    float scale;
    private UnboundedIntRoller roller;
    private ImageIcon[] imgs = new ImageIcon[12];

    /**
     * Creates a new instance of BasicSubRangeSliderUI
     */
    public BasicUnboundedIntRollerUI()
    {
        for (int i = 0; i < imgs.length; i++)
            imgs[i] = new javax.swing.ImageIcon(getClass().getResource("/org/visnow/vn/gui/icons/roller/" + i + ".jpg"));
    }

    public static ComponentUI createUI(JComponent c)
    {
        return new BasicUnboundedIntRollerUI();
    }

    @Override
    public void installUI(JComponent c)
    {
        roller = (UnboundedIntRoller) c;
        roller.addMouseListener(this);
        roller.addMouseMotionListener(this);
    }

    @Override
    public void uninstallUI(JComponent c)
    {
        roller = (UnboundedIntRoller) c;
        roller.removeMouseListener(this);
        roller.removeMouseMotionListener(this);
    }

    @Override
    public void paint(Graphics g, JComponent c)
    {
        //  We don't want to paint inside the insets or borders.
        this.g = g;
        this.c = c;
        d = c.getInsets();
        width = c.getWidth() - d.left - d.right - 12;
        height = c.getHeight() - d.top - d.bottom;
        g.setColor(c.getBackground());
        g.fillRect(0, 0, c.getWidth(), c.getHeight());
        g.translate(d.left, d.top);
        g.setColor(Color.DARK_GRAY);
        g.drawRect(0, 0, width, height);
        g.setColor(Color.GRAY);
        g.fillRect(2, 2, width - 4, height - 3);
        g.setColor(Color.LIGHT_GRAY);
        g.clipRect(1, 1, width - 2, height - 1);
        g.setColor(Color.LIGHT_GRAY);
        g.clipRect(1, 1, width - 2, height - 1);
        int i = (13 * roller.getValue());
        i = max(0, i % 12);
        g.drawImage(imgs[i].getImage(), 0, 0, width, height, 0, 0, 319, 31, null);
    }


    @Override
    public void mouseClicked(MouseEvent e)
    {
        if (e.isShiftDown()) {
            int x = e.getX();
            if (x - roller.getX() < roller.getWidth() / 2)
                lastVal -= roller.getModel().getLargeIncrement();
            else
                lastVal += roller.getModel().getLargeIncrement();
            roller.setValue(lastVal);
        }
        else   
            if (e.getButton() == MouseEvent.BUTTON1)
                roller.setSensitivity(Math.max(1, (int)(roller.getSensitivity() / 1.5)));
            else
                roller.setSensitivity((int)(roller.getSensitivity() * 1.5));
    }

    @Override
    public void mouseDragged(MouseEvent e)
    {
        int x = e.getX();
        lastVal += (e.getX() - lastX) * roller.getSensitivity();
        roller.setValue(lastVal);
        lastX = x;
        c.repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e)
    {
    }

    @Override
    public void mouseExited(MouseEvent e)
    {
    }

    @Override
    public void mouseMoved(MouseEvent e)
    {
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
        roller.setAdjusting(true);
        lastX = startPos = e.getX();
        lastY = e.getY();
        lastVal = startVal = roller.getValue();

    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
        roller.setAdjusting(false);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e)
    {
        lastVal += (e.getWheelRotation()) * roller.getSensitivity();
        roller.setValue(lastVal);
    }
}
