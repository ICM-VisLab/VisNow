/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Vector;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;
import javax.swing.plaf.metal.MetalComboBoxUI;
import org.visnow.vn.lib.utils.SwingInstancer;

/**
 * @author Nobuo Tamemasa
 * @version 1.0 12/12/98
 */
public class SteppedComboBox extends JComboBox implements ComponentListener
{

    protected int popupWidth;

    public SteppedComboBox()
    {
        this(new String[]{});
        addComponentListener(this);
    }

    @SuppressWarnings("unchecked")
    public SteppedComboBox(ComboBoxModel aModel)
    {
        super(aModel);
        setUI(new SuperComboBoxUI());
        updatePopupWidth();
        addComponentListener(this);
    }

    public SteppedComboBox(final Object[] items)
    {
        super(items);
        setUI(new SuperComboBoxUI());
        updatePopupWidth();
        addComponentListener(this);
    }

    public SteppedComboBox(Vector items)
    {
        super(items);
        setUI(new SuperComboBoxUI());
        updatePopupWidth();
        addComponentListener(this);
    }

    public void setPopupWidth(int width)
    {
        popupWidth = width;
    }

    private void updatePopupWidth()
    {
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                if (SteppedComboBox.this.getRenderer() != null &&
                    SteppedComboBox.this.getModel() != null &&
                    (SteppedComboBox.this.getUI() instanceof SuperComboBoxUI)) {

                    JList list = ((SuperComboBoxUI) SteppedComboBox.this.getUI()).createPopup().getList();

                    popupWidth = 10;
                    for (int i = 0; i < SteppedComboBox.this.getModel().getSize(); i++) {
                        int w = SteppedComboBox.this.getRenderer().getListCellRendererComponent(
                            list, SteppedComboBox.this.getModel().getElementAt(i),
                            i, true, true).getPreferredSize().width;
                        if (w > popupWidth) {
                            popupWidth = w;
                        }
                    }
                    popupWidth += 25;

                } else {

                    if (SteppedComboBox.this.getFont() == null) {
                        return;
                    }
                    FontMetrics fm = SteppedComboBox.this.getFontMetrics(SteppedComboBox.this.getFont());
                    if (fm == null) {
                        popupWidth = 100;
                        return;
                    }
                    popupWidth = 10;
                    for (int i = 0; i < SteppedComboBox.this.getModel().getSize(); i++) {
                        int w = 0;
                        if (SteppedComboBox.this.getModel() != null && SteppedComboBox.this.getModel().getElementAt(i) != null) {
                            w = fm.stringWidth(SteppedComboBox.this.getModel().getElementAt(i).toString());
                        }
                        if (w > popupWidth) {
                            popupWidth = w;
                        }
                    }
                    popupWidth += 80;

                }

                if (popupWidth < getWidth()) {
                    popupWidth = getWidth();
                }

            }
        });

    }

    public Dimension getPopupSize()
    {
        Dimension size = getSize();
        if (popupWidth < 1) {
            popupWidth = size.width;
        }
        return new Dimension(popupWidth, size.height);
    }

    @Override
    public void setModel(ComboBoxModel aModel)
    {
        super.setModel(aModel);
        updatePopupWidth();
    }

    public void componentResized(ComponentEvent e)
    {
        updatePopupWidth();
    }

    public void componentMoved(ComponentEvent e)
    {
    }

    public void componentShown(ComponentEvent e)
    {
    }

    public void componentHidden(ComponentEvent e)
    {
    }

    @Override
    public void setRenderer(ListCellRenderer aRenderer)
    {
        super.setRenderer(aRenderer);
        updatePopupWidth();
    }
}

/**
 * @author Nobuo Tamemasa
 * @version 1.0 12/12/98
 */
class SuperComboBoxUI extends MetalComboBoxUI
{

    protected ComboPopup createPopup()
    {
        BasicComboPopup popup = new BasicComboPopup(comboBox)
        {
            public void show()
            {
                Dimension popupSize = ((SteppedComboBox) comboBox).getPopupSize();
                popupSize.setSize(popupSize.width,
                                  getPopupHeightForRowCount(comboBox.getMaximumRowCount()));
                Rectangle popupBounds = computePopupBounds(0,
                                                           comboBox.getBounds().height, popupSize.width, popupSize.height);
                scroller.setMaximumSize(popupBounds.getSize());
                scroller.setPreferredSize(popupBounds.getSize());
                scroller.setMinimumSize(popupBounds.getSize());
                list.invalidate();
                int selectedIndex = comboBox.getSelectedIndex();
                if (selectedIndex == -1) {
                    list.clearSelection();
                } else {
                    list.setSelectedIndex(selectedIndex);
                }
                list.ensureIndexIsVisible(list.getSelectedIndex());
                setLightWeightPopupEnabled(comboBox.isLightWeightPopupEnabled());

                show(comboBox, popupBounds.x, popupBounds.y);
            }
        };
        popup.getAccessibleContext().setAccessibleParent(comboBox);
        return popup;
    }
}
