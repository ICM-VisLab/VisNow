/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class FontComboBox extends JComboBox
{

    private int size = 12;
    private Color foreground = Color.BLACK;
    private Color background = Color.LIGHT_GRAY;
    private List<String> fontNames;

    private class FontComboBoxRenderer extends BasicComboBoxRenderer
    {

        @Override
        public Component getListCellRendererComponent(
            JList list,
            Object value,
            int index,
            boolean isSelected,
            boolean cellHasFocus)
        {
            JLabel renderer = new JLabel();
            renderer.setForeground(foreground);
            renderer.setBackground(background);
            if (value instanceof String) {
                renderer.setFont(new Font((String) value, Font.PLAIN, size));
                renderer.setText((String) value);
                renderer.setMaximumSize(new Dimension(150, size + 4));
                renderer.setMinimumSize(new Dimension(150, size + 4));
                renderer.setPreferredSize(new Dimension(150, size + 4));
            } else
                renderer.setText("");
            return renderer;
        }
    }

    public FontComboBox()
    {
        String stdChars = "0123456789!@#$%^&*()-_=+qwertyuiop[]QWERTYUIOP{}asdfghjkl;'ASDFGHJKL:|`zxcvbnm,./~ZXCVBNM<>?";
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Font[] tmpFonts = ge.getAllFonts();
        List<String> tmpFontNames = new ArrayList<String>();
        fontNames = new ArrayList<String>();
        for (int i = 0; i < tmpFonts.length; i++) {
            boolean proper = true;
            for (int j = 0; j < stdChars.length(); j++)
                if (!tmpFonts[i].canDisplay(stdChars.charAt(j))) {
                    proper = false;
                    break;
                }
            if (proper)
                tmpFontNames.add(tmpFonts[i].getName());
        }
        Set<String> set = new HashSet<String>();
        fontNames.add("Dialog");
        for (String fontName : tmpFontNames) {
            if (!fontName.endsWith("10") && set.add(fontName))
                fontNames.add(fontName);
        }
        for (String fontName : fontNames)
            addItem(fontName);
        setRenderer(new FontComboBoxRenderer());
    }

    @Override
    public void setFont(Font font)
    {
        super.setFont(font);
        size = (int) (1.6 * font.getSize());
    }

}
