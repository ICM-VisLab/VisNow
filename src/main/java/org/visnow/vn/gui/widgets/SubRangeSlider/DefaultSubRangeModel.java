/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets.SubRangeSlider;

/*
 * DefaultSubRangeModel.java
 *
 * Created on April 10, 2004, 7:30 PM
 */
import javax.swing.*;
import javax.swing.event.*;
import java.io.Serializable;
import java.util.EventListener;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class DefaultSubRangeModel implements SubRangeModel, Serializable
{

    /**
     * Only one <code>ChangeEvent</code> is needed per model instance since the
     * event's only (read-only) state is the source property. The source
     * of events generated here is always "this".
     */
    protected transient ChangeEvent changeEvent = null;

    /**
     * The listeners waiting for model changes.
     */
    protected EventListenerList listenerList = new EventListenerList();

    private int bottomValue = 0;
    private int lastBottomValue = 0;
    private int topValue = 100;
    private int lastTopValue = 100;
    private int extent = 0;
    private int min = 0;
    private int max = 100;
    private int policy = STOP;
    private int extensionPolicy = IGNORE;

    private boolean adjusting = false;
    private boolean bottomValueChanged = false;
    private boolean topValueChanged = false;

    /**
     * Initializes all of the properties with default values.
     * Those values are:
     * <ul>
     * <li><code>bottomValue</code> = 0
     * <li><code>topValue</code> = 100
     * <li><code>extent</code> = 0
     * <li><code>minimum</code> = 0
     * <li><code>maximum</code> = 100
     * <li><code>policy</code> = STOP
     * <li><code>adjusting</code> = false
     * </ul>
     */
    public DefaultSubRangeModel()
    {
    }

    /**
     * Initializes value, extent, minimum and maximum. Adjusting is false.
     * Throws an <code>IllegalArgumentException</code> if the following
     * constraints aren't satisfied:
     * <pre>
     * min <= bottomValue <= value+extent <= topValue <= max
     * </pre>
     */
    public DefaultSubRangeModel(int bottomValue, int topValue, int extent,
                                int policy, int min, int max)
    {
        if ((max >= min) &&
            (bottomValue >= min) &&
            (extent >= 0) &&
            ((bottomValue + extent) <= topValue) &&
            (topValue <= max)) {
            this.bottomValue = bottomValue;
            this.topValue = topValue;
            this.extent = extent;
            this.policy = policy;
            this.min = min;
            this.max = max;
        }
        this.bottomValue = bottomValue;
        this.topValue = topValue;
        this.extent = extent;
        this.policy = policy;
        this.min = min;
        this.max = max;
    }

    /**
     * Returns the model's current value.
     * <p>
     * @return the model's current value
     * <p>
     * @see #setValue
     * @see BoundedRangeModel#getValue
     */
    public int getBottomValue()
    {
        return bottomValue;
    }

    /**
     * Returns the model's current value.
     * <p>
     * @return the model's current value
     * <p>
     * @see #setValue
     * @see BoundedRangeModel#getValue
     */
    public int getTopValue()
    {
        return topValue;
    }

    /**
     * Returns the model's extent.
     * <p>
     * @return the model's extent
     * <p>
     * @see #setExtent
     * @see BoundedRangeModel#getExtent
     */
    public int getExtent()
    {
        return extent;
    }

    /**
     * Returns the model's policy.
     * <p>
     * @return the model's policy
     * <p>
     * @see #setPolicy
     */
    public int getPolicy()
    {
        return policy;
    }

    /**
     * Returns the model's minimum.
     * <p>
     * @return the model's minimum
     * <p>
     * @see #setMinimum
     * @see BoundedRangeModel#getMinimum
     */
    public int getMinimum()
    {
        return min;
    }

    /**
     * Returns the model's maximum.
     * <p>
     * @return the model's maximum
     * <p>
     * @see #setMaximum
     * @see BoundedRangeModel#getMaximum
     */
    public int getMaximum()
    {
        return max;
    }

    public void setPolicy(int n)
    {
        if (n >= STOP && n <= PUSH)
            setRangeProperties(bottomValue, topValue, extent, n, extensionPolicy,
                               min, max, adjusting);
    }

    /**
     * Sets the current topValue of the model. For a slider, that
     * determines where the knob appears. Ensures that the new
     * value, <I>n</I> falls within the model's constraints:
     * <pre>
     *     minimum <= value <= value+extent <= maximum
     * </pre>
     *
     * @see BoundedRangeModel#setValue
     */
    public void setTopValue(int n)
    {
        int newTopValue = min(n, max);
        int newBottomValue = bottomValue;
        if (newTopValue - extent < bottomValue) {
            if (policy == STOP)
                newTopValue = bottomValue + extent;
            else {
                newBottomValue = newTopValue - extent;
                if (newBottomValue < min) {
                    newBottomValue = min;
                    newTopValue = newBottomValue + extent;
                }
            }
        }
        setRangeProperties(newBottomValue, newTopValue, extent, policy, extensionPolicy,
                           min, max, adjusting);
    }

    /**
     * Sets the current bottomValue of the model. For a slider, that
     * determines where the knob appears. Ensures that the new
     * value, <I>n</I> falls within the model's constraints:
     * <pre>
     *     minimum <= value <= value+extent <= maximum
     * </pre>
     *
     * @see BoundedRangeModel#setValue
     */
    public void setBottomValue(int n)
    {
        int newBottomValue = max(n, min);
        int newTopValue = topValue;
        if (newBottomValue + extent > topValue) {
            if (policy == STOP) {
                newBottomValue = topValue - extent;
            } else {
                newTopValue = newBottomValue + extent;
                if (newTopValue > max) {
                    newTopValue = max;
                    newBottomValue = newTopValue - extent;
                }
            }
        }
        setRangeProperties(newBottomValue, newTopValue, extent, policy, extensionPolicy,
                           min, max, adjusting);
    }

    /**
     * Sets the extent to <I>n</I> after ensuring that <I>n</I>
     * is greater than or equal to zero and falls within the model's
     * constraints:
     * <pre>
     *     minimum <= value <= value+extent <= maximum
     * </pre>
     * @see BoundedRangeModel#setExtent
     */
    public void setExtent(int n)
    {
        int newExtent = min(max - min, max(0, n));
        int newBottomValue = bottomValue;
        int newTopValue = topValue;
        if (bottomValue + newExtent > max) {
            newBottomValue = max - newExtent;
        }
        if (newTopValue < newBottomValue + newExtent)
            newTopValue = newBottomValue + newExtent;
        setRangeProperties(newBottomValue, newTopValue, newExtent, extensionPolicy,
                           policy, min, max, adjusting);
    }

    /**
     * Sets the minimum to <I>n</I> after ensuring that <I>n</I>
     * that the other three properties obey the model's constraints:
     * <pre>
     *     minimum <= value <= value+extent <= maximum
     * </pre>
     * @see #getMinimum
     * @see BoundedRangeModel#setMinimum
     */
    public void setMinimum(int n)
    {
        int newBottomValue;
        int newTopValue = topValue;
        int newMin = n;
        int newMax = max(newMin, max);
        int newExtent = min(newMax - newMin, extent);
        switch (extensionPolicy) {
            case RESET:
                newBottomValue = newMin;
                newTopValue = newMax;
                break;
            case IGNORE:
                newBottomValue = max(bottomValue, newMin);
                break;
            case STRETCH:
                if (bottomValue <= min)
                    newBottomValue = newMin;
                else
                    newBottomValue = max(bottomValue, newMin);
                break;
            case PULL:
                if (bottomValue <= min)
                    newBottomValue = newMin;
                else
                    newBottomValue = max(bottomValue, newMin);
                newTopValue = newBottomValue + topValue - bottomValue;
                break;
            default:
                newBottomValue = max(bottomValue, newMin);
                break;
        }
        if (newTopValue > newMax)
            newTopValue = newMax;

        if (newBottomValue + newExtent > newTopValue) {
            if (policy == STOP) {
                newBottomValue = max(newTopValue - newExtent, newMin);
                newTopValue = newBottomValue + newExtent;
            } else {
                newTopValue = newBottomValue + newExtent;
                if (newTopValue > max) {
                    newTopValue = max;
                    newBottomValue = newTopValue - newExtent;
                }
            }
        }
        setRangeProperties(newBottomValue, newTopValue, newExtent, policy, extensionPolicy,
                           newMin, newMax, adjusting);
    }

    /**
     * Sets the maximum to <I>n</I> and modifies <code>bottomValue, topValue</code>
     * according to the <code>extensionPolicy</code> ensuring that <I>n</I>
     * that the other three properties obey the model's constraints:
     * <pre>
     *     minimum <= value <= value+extent <= maximum
     * </pre>
     * @see BoundedRangeModel#setMaximum
     */
    public void setMaximum(int n)
    {
        int newTopValue;
        int newBottomValue = bottomValue;
        int newMax = n;
        int newMin = min(newMax, min);
        int newExtent = min(newMax - newMin, extent);
        switch (extensionPolicy) {
            case RESET:
                newBottomValue = newMin;
                newTopValue = newMax;
                break;
            case IGNORE:
                newTopValue = min(topValue, newMax);
                break;
            case STRETCH:
                if (topValue >= max)
                    newTopValue = newMax;
                else
                    newTopValue = min(topValue, newMax);
                break;
            case PULL:
                if (topValue >= max)
                    newTopValue = newMax;
                else
                    newTopValue = min(topValue, newMax);
                newBottomValue = newTopValue - topValue + bottomValue;
                break;
            default:
                newTopValue = min(topValue, newMax);
                break;
        }
        if (newBottomValue < newMin)
            newBottomValue = newMin;
        if (newBottomValue + newExtent > newTopValue) {
            if (policy == STOP) {
                newTopValue = min(newBottomValue + newExtent, newMax);
                newBottomValue = newTopValue - newExtent;
            } else {
                newBottomValue = newTopValue - newExtent;
                if (newBottomValue < min) {
                    newBottomValue = min;
                    newTopValue = newBottomValue + newExtent;
                }
            }
        }
        setRangeProperties(newBottomValue, newTopValue, newExtent, policy, extensionPolicy,
                           newMin, newMax, adjusting);
    }

    public void setMinMax(int m, int n)
    {
        if (m > n)
            return;
        int newBottomValue;
        int newTopValue;
        int newMin = m;
        int newMax = n;
        if (bottomValue == min)
            newBottomValue = newMin;
        else
            newBottomValue = max(bottomValue, newMin);
        if (topValue == max)
            newTopValue = newMax;
        else
            newTopValue = min(topValue, newMin);
        int newExtent = min(newMax - newMin, extent);
        if (newBottomValue + newExtent > newTopValue) {
            if (policy == STOP) {
                newBottomValue = max(newTopValue - newExtent, newMin);
                newTopValue = newBottomValue + newExtent;
            } else {
                newTopValue = newBottomValue + newExtent;
                if (newTopValue > max) {
                    newTopValue = max;
                    newBottomValue = newTopValue - newExtent;
                }
            }
        }

        if (newBottomValue + newExtent > newTopValue) {
            if (policy == STOP) {
                newTopValue = min(newBottomValue + newExtent, newMax);
                newBottomValue = newTopValue - newExtent;
            } else {
                newBottomValue = newTopValue - newExtent;
                if (newBottomValue < min) {
                    newBottomValue = min;
                    newTopValue = newBottomValue + newExtent;
                }
            }
        }
        setRangeProperties(newBottomValue, newTopValue, newExtent, policy, extensionPolicy,
                           newMin, newMax, adjusting);
    }

    /**
     * Sets all of the <code>BoundedRangeModel</code> properties after forcing
     * the arguments to obey the usual constraints:
     * <pre>
     *     minimum <= bo   ttomValue <= bottomValue+extent <= topValue <= maximum
     * </pre>
     * <p>
     * At most, one <code>ChangeEvent</code> is generated.
     *
     * @see BoundedRangeModel#setRangeProperties
     * @see #setValue
     * @see #setExtent
     * @see #setMinimum
     * @see #setMaximum
     * @see #setValueIsAdjusting
     */
    public void setRangeProperties(int newBottomValue, int newTopValue,
                                   int newExtent, int newPolicy, int newExtensionPolicy, int newMin, int newMax,
                                   boolean newAdjusting)
    {
        boolean isChange
            = (newBottomValue != bottomValue) ||
            (newTopValue != topValue) ||
            (newExtent != extent) ||
            (newPolicy != policy) ||
            (newExtensionPolicy != extensionPolicy) ||
            (newMin != min) ||
            (newMax != max) ||
            (newAdjusting != adjusting);

        bottomValueChanged = (newBottomValue != bottomValue);
        topValueChanged = (newTopValue != topValue);
        if (isChange) {
            lastBottomValue = bottomValue;
            bottomValue = newBottomValue;
            lastTopValue = topValue;
            topValue = newTopValue;
            extent = newExtent;
            policy = newPolicy;
            extensionPolicy = newExtensionPolicy;
            min = newMin;
            max = newMax;
            adjusting = newAdjusting;
            fireStateChanged();
        }
    }

    /**
     * Adds a <code>ChangeListener</code>. The change listeners are run each
     * time any one of the Bounded Range model properties changes.
     *
     * @param l the ChangeListener to add
     * <p>
     * @see #removeChangeListener
     * @see BoundedRangeModel#addChangeListener
     */
    public void addChangeListener(ChangeListener l)
    {
        listenerList.add(ChangeListener.class, l);
    }

    /**
     * Removes a <code>ChangeListener</code>.
     *
     * @param l the <code>ChangeListener</code> to remove
     * <p>
     * @see #addChangeListener
     * @see BoundedRangeModel#removeChangeListener
     */
    public void removeChangeListener(ChangeListener l)
    {
        listenerList.remove(ChangeListener.class, l);
    }

    /**
     * Returns an array of all the change listeners
     * registered on this <code>DefaultBoundedRangeModel</code>.
     *
     * @return all of this model's <code>ChangeListener</code>s
     *         or an empty
     *         array if no change listeners are currently registered
     *
     * @see #addChangeListener
     * @see #removeChangeListener
     *
     * @since 1.4
     */
    public ChangeListener[] getChangeListeners()
    {
        return (ChangeListener[]) listenerList.getListeners(
            ChangeListener.class);
    }

    /**
     * Runs each <code>ChangeListener</code>'s <code>stateChanged</code> method.
     *
     * @see #setRangeProperties
     * @see EventListenerList
     */
    protected void fireStateChanged()
    {
        Object[] listeners = listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == ChangeListener.class) {
                if (changeEvent == null) {
                    changeEvent = new ChangeEvent(this);
                }
                ((ChangeListener) listeners[i + 1]).stateChanged(changeEvent);
            }
        }
    }

    /**
     * Returns a string that displays all of the
     * <code>BoundedRangeModel</code> properties.
     */
    @Override
    public String toString()
    {
        String strPol = null;
        if (policy == STOP)
            strPol = "STOP";
        else
            strPol = "PUSH";
        String strExtPol = null;

        String modelString
            = "bottomValue=" + getBottomValue() + ", " +
            "topValue=" + getTopValue() + ", " +
            "extent=" + getExtent() + ", " +
            "policy=" + strPol + ", " +
            "extensionPolicy=" + strExtPol + ", " +
            "min=" + getMinimum() + ", " +
            "max=" + getMaximum() + ", " +
            "adj=" + isAdjusting();

        return getClass().getName() + "[" + modelString + "]";
    }

    /**
     * Returns an array of all the objects currently registered as
     * <code><em>Foo</em>Listener</code>s
     * upon this model.
     * <code><em>Foo</em>Listener</code>s
     * are registered using the <code>add<em>Foo</em>Listener</code> method.
     * <p>
     * You can specify the <code>listenerType</code> argument
     * with a class literal, such as <code><em>Foo</em>Listener.class</code>.
     * For example, you can query a <code>DefaultBoundedRangeModel</code>
     * instance <code>m</code>
     * for its change listeners
     * with the following code:
     *
     * <pre>ChangeListener[] cls = (ChangeListener[])(m.getListeners(ChangeListener.class));</pre>
     *
     * If no such listeners exist,
     * this method returns an empty array.
     *
     * @param listenerType the type of listeners requested;
     *                     this parameter should specify an interface
     *                     that descends from <code>java.util.EventListener</code>
     * <p>
     * @return an array of all objects registered as
     *         <code><em>Foo</em>Listener</code>s
     *         on this model,
     *         or an empty array if no such
     *         listeners have been added
     * <p>
     * @exception ClassCastException if <code>listenerType</code> doesn't
     *                               specify a class or interface that implements
     *                               <code>java.util.EventListener</code>
     *
     * @see #getChangeListeners
     *
     * @since 1.3
     */
    public <T extends EventListener> T[] getListeners(Class<T> listenerType)
    {
        return listenerList.getListeners(listenerType);
    }

    /**
     * Returns true if the value is in the process of changing
     * as a result of actions being taken by the user.
     *
     * @return the value of the <code>adjusting</code> property
     * <p>
     * @see #setValue
     * @see BoundedRangeModel#getValueIsAdjusting
     */
    public boolean isAdjusting()
    {
        return adjusting;
    }

    /**
     * Sets the <code>adjusting</code> property.
     *
     * @see #getValueIsAdjusting
     * @see #setValue
     * @see BoundedRangeModel#setValueIsAdjusting
     */
    public void setAdjusting(boolean b)
    {
        setRangeProperties(bottomValue, topValue, extent, policy, extensionPolicy, min, max, b);
    }

    public boolean isBottomValueChanged()
    {
        return bottomValueChanged;
    }

    public boolean isTopValueChanged()
    {
        return topValueChanged;
    }

    public int getLastBottomValue()
    {
        return lastBottomValue;
    }

    public int getLastTopValue()
    {
        return lastTopValue;
    }

    public int getExtensionPolicy()
    {
        return extensionPolicy;
    }

    public void setExtensionPolicy(int n)
    {
        if (n >= RESET && n <= PULL)
            setRangeProperties(bottomValue, topValue, extent, policy, n,
                               min, max, adjusting);
    }

}
