/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import org.apache.log4j.Logger;
import org.visnow.vn.gui.swingwrappers.UserActionListener;
import org.visnow.vn.gui.swingwrappers.UserEvent;
import org.visnow.vn.gui.widgets.utils.drag2D.DragBundle;
import org.visnow.vn.gui.widgets.utils.hyperrectangle.Hyperrectangle;
import org.visnow.vn.gui.widgets.utils.hyperrectangle.HyperrectangleUtils;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.swing.UIStyle;

/**
 *
 * @author szpak
 */
public class HyperrectangleSelectTool extends javax.swing.JPanel
{

    private static final Logger LOGGER = Logger.getLogger(HyperrectangleSelectTool.class);
    private static final int minimumMarginPx = 20;
    private static final int minimumContentPx = 40;
    private static final float cornerAttractionPc = 0.2f;
    private static final int maximumAttractionPx = 30;

    //SPEC: 1, 2 or 3
    private int numberOfDimensions = 3;

    private float minimumDimensionSize = 2;
    //SPEC: 3 x floats, each >= 2
    private double[] baseSize = new double[]{100, 100, 100};
    //    private float[] selectionSize = new float[]{360, 150, 60};
    //    private float[] selectionSize = new float[]{260, 50, 60};
    //    private float[] selectionSize = new float[]{60, 50, 60};
//    private float[] selectionSize = new float[]{80, 80, 80};
    private double[] selectionSize = new double[]{80, 20, 20};
    //SPEC(internal): base has no offset
    //SPEC: 3 x floats, any. This is offset of selection area in base coordinates (where 0,0 is upper left corner)
    //    private float[] selectionOffset = new float[]{-50, -80, 20};
//    private float[] selectionOffset = new float[]{-20, -30, 0};
    private double[] selectionOffset = new double[]{10, 30, 30};
    //    private float[] selectionOffset = new float[]{0, 0, 0};

    private Hyperrectangle base;
    private Hyperrectangle selection;

    private boolean allowOverflow = true;
    private boolean submitOnAdjusting = true;

    /**
     * Creates new form RectangleSelectTool
     */
    public HyperrectangleSelectTool()
    {
        updateHyperrectangles();
        cropToOverflow();

        MouseAdapter mouseAdapter = new MouseAdapter()
        {
            private boolean dragging = false;
            DragBundle dragBundle = null;
            Hyperrectangle selectionBeforeDrag;

            @Override
            public void mouseReleased(MouseEvent e)
            {
                dragging = false;

                //TODO: if value has changed
                if (!submitOnAdjusting) {
                    //TODO: test changed
                    fireValueChanged();
                }
                //TODO: if value has changed
                fireUserAction(UserEvent.VALUE_CHANGED);
                //TODO: handle stop adjusting here
            }

            //TODO: unify + test getHeight (make sure that it doesn't depend on parent scroll
            @Override
            public void mouseDragged(MouseEvent e)
            {
                //this is to avoid dragging by RMB
                if (dragging) {
                    selection = selectionBeforeDrag.clone();
                    dragBundle.applyDrag(selection, e.getX(), e.getY(), minimumDimensionSize);
                    if (!allowOverflow) selection.packInside(base);
                    selectionOffset = selection.getOffset();
                    selectionSize = selection.getSize();

//                if (valueChanged)
                    fireValueChanged();

//                if (userAction)
                    fireUserAction(UserEvent.VALUE_CHANGED);

                    repaint();
                }
            }

            @Override
            public void mousePressed(MouseEvent e)
            {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    selectionBeforeDrag = selection.clone();
                    dragBundle = HyperrectangleUtils.getSelectionDragBundle(base, selectionBeforeDrag, getWidth(), getHeight(), e.getX(), e.getY());
                    if (dragBundle != null) {
                        dragging = true;
                        setCursor(dragBundle.getCursor());
                    } else {
                        setCursor(Cursor.getDefaultCursor());
                    }
                }
            }

            @Override

            public void mouseEntered(MouseEvent e)
            {
                mouseMoved(e);
            }

            @Override
            public void mouseMoved(MouseEvent e)
            {
                if (!dragging) {
                    //                    DragPosition dragPosition = 
//                    preparePointerMapping(e);
//                    updateCursor();
//                }
                    DragBundle dragBundle = HyperrectangleUtils.getSelectionDragBundle(base, selection, getWidth(), getHeight(), e.getX(), e.getY());
                    if (dragBundle != null)
                        setCursor(dragBundle.getCursor());
                    else setCursor(Cursor.getDefaultCursor());
                }
            }
        };

        addMouseListener(mouseAdapter);
        addMouseMotionListener(mouseAdapter);

    }
//

    //XXX: this can be place to setPrecision(DOUBLE/INT)
    private void updateHyperrectangles()
    {
        base = new Hyperrectangle(numberOfDimensions, baseSize);
        selection = new Hyperrectangle(numberOfDimensions, selectionSize, selectionOffset);
//        base = new IntHyperrectangle(numberOfDimensions, baseSize);
//        selection = new IntHyperrectangle(numberOfDimensions, selectionSize, selectionOffset);
    }

    //TODO: find out in which moments validate overflow
    private void cropToOverflow()
    {
        if (!allowOverflow)
            selection.cropTo(base, minimumDimensionSize);
        selectionOffset = selection.getOffset();
        selectionSize = selection.getSize();
    }

    public void setAllowOverflow(boolean allowOverflow)
    {
        this.allowOverflow = allowOverflow;
        cropToOverflow();
        repaint();
    }

    public boolean isAllowOverflow()
    {
        return allowOverflow;
    }

    public double[] getBaseSize()
    {
        return baseSize.clone();
    }

    public void setBaseSize(double[] baseSize)
    {
        setBase(baseSize);
    }

    private void setBase(double[] baseSize)
    {
        if (baseSize.length < numberOfDimensions)
            throw new IllegalArgumentException("Incorrect number of dimensions " + baseSize.length + ". Should be at least " + numberOfDimensions);

        for (double f : baseSize)
            if (f < minimumDimensionSize)
                throw new IllegalArgumentException("Dimension too small " + f + ". Should be at least " + minimumDimensionSize);

        this.baseSize = baseSize.clone();
        updateHyperrectangles();
        cropToOverflow();
        repaint();
    }

    public double[] getSelectionSize()
    {
        return selectionSize.clone();
    }

    public void setSelectionSize(double[] selectionSize)
    {
        setSelection(selectionSize, selectionOffset);
    }

    public double[] getSelectionOffset()
    {
        return selectionOffset.clone();
    }

    public void setSelectionOffset(double[] selectionOffset)
    {
        setSelection(selectionSize, selectionOffset);
    }

    private void setSelection(double[] selectionSize, double[] selectionOffset)
    {
        if (selectionSize.length < numberOfDimensions)
            throw new IllegalArgumentException("Incorrect number of dimensions " + selectionSize.length + ". Should be at least " + numberOfDimensions);
        if (selectionOffset.length < numberOfDimensions)
            throw new IllegalArgumentException("Incorrect number of dimensions " + selectionOffset.length + ". Should be at least " + numberOfDimensions);

        for (double f : selectionSize)
            if (f < minimumDimensionSize)
                throw new IllegalArgumentException("Dimension too small " + f + ". Should be at least " + minimumDimensionSize);

        this.selectionSize = selectionSize.clone();
        this.selectionOffset = selectionOffset.clone();

        updateHyperrectangles();
        cropToOverflow();
        repaint();
    }

    public void setNumberOfDimensions(int numberOfDimensions)
    {
        if (numberOfDimensions < 1 || numberOfDimensions > 3)
            throw new IllegalArgumentException("Incorrect number of dimensions " + numberOfDimensions);
        this.numberOfDimensions = numberOfDimensions;

        updateHyperrectangles();
        cropToOverflow();
        repaint();
    }

    public int getNumberOfDimensions()
    {
        return numberOfDimensions;
    }

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);

        Graphics2D g2D = (Graphics2D) g;
        HyperrectangleUtils.draw(g2D, getWidth(), getHeight(), base, selection);
    }
 
    private boolean autoMinimumSize = true;
    private boolean autoPreferredSize = true;

    @Override
    public void setMinimumSize(Dimension minimumSize)
    {
        super.setMinimumSize(minimumSize); //To change body of generated methods, choose Tools | Templates.
        autoMinimumSize = false;
    }

    @Override
    public Dimension getMinimumSize()
    {
        if (autoMinimumSize) return getMinSize();
        else return super.getMinimumSize();
    }

    @Override
    public void setPreferredSize(Dimension preferredSize)
    {
        super.setPreferredSize(preferredSize); //To change body of generated methods, choose Tools | Templates.
        autoPreferredSize = false;
    }

    //TODO: fix repaint on scroll
    @Override
    public Dimension getPreferredSize()
    {
        if (autoPreferredSize) return getMinSize();
        else return super.getPreferredSize();
    }

    private Dimension getMinSize()
    {
        return new Dimension(minimumContentPx + minimumMarginPx * 2, minimumContentPx + minimumMarginPx * 2);
    }

    public boolean isSubmitOnAdjusting()
    {
        return submitOnAdjusting;
    }

    public void setSubmitOnAdjusting(boolean submitOnAdjusting)
    {
        this.submitOnAdjusting = submitOnAdjusting;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    public static void main(String[] args)
    {
        VisNow.initLogging(true);
        UIStyle.initStyle();

        JFrame frame = new JFrame();
        HyperrectangleSelectTool rectangleSelectTool = new HyperrectangleSelectTool();
        JPanel marginPanel = new JPanel();
        marginPanel.setBackground(new Color(0xddddff));
        marginPanel.setLayout(new BorderLayout());
        marginPanel.add(rectangleSelectTool, BorderLayout.CENTER);
        marginPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
//        JScrollPane scrollPane = new JScrollPane();
//        scrollPane.setBorder(null);
//        scrollPane.setViewportView(marginPanel);
//        frame.add(scrollPane);
        frame.add(marginPanel);
        frame.pack();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocation(400, 300);
        frame.setSize(300, 300);
        frame.setVisible(true);

    }

    private void fireValueChanged()
    {
        for (UserActionListener listener : userActionListeners)
            listener.userChangeAction(new org.visnow.vn.gui.swingwrappers.UserEvent(this));
    }

    private void fireUserAction(int flags)
    {
        UserEvent ue = new UserEvent(flags, this);
        for (UserActionListener listener : userActionListeners)
            listener.userAction(ue);
    }

    private List<UserActionListener> userActionListeners = new ArrayList<UserActionListener>();

    public void addUserActionListener(UserActionListener listener)
    {
        userActionListeners.add(listener);
    }

    public void removeUserActionListener(UserActionListener listener)
    {
        userActionListeners.remove(listener);
    }

}
