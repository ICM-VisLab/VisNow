/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets.UnboundedRoller;

/*
 * DefaultSubRangeModel.java
 *
 * Created on April 10, 2004, 7:30 PM
 */
import org.visnow.vn.gui.widgets.UnboundedRoller.UnboundedIntValueModel;
import javax.swing.*;
import javax.swing.event.*;
import java.io.Serializable;
import java.util.EventListener;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM/babor
 */
public class DefaultUnboundedIntValueModel implements UnboundedIntValueModel, Serializable
{

    /**
     * Only one <code>ChangeEvent</code> is needed per model instance since the
     * event's only (read-only) state is the source property. The source
     * of events generated here is always "this".
     */
    protected transient ChangeEvent changeEvent = null;

    /**
     * The listeners waiting for model changes.
     */
    protected EventListenerList listenerList = new EventListenerList();

    private int value = 0;
    private int sensitivity = 1;
    private int largeIncrement = 100;
    private boolean adjusting = false;

    /**
     * Initializes all of the properties with default values.
     * Those values are:
     * <ul>
     * <li><code>value</code> = 0
     * <li><code>sensitivity</code> = 1
     * <li><code>adjusting</code> = false
     * </ul>
     */
    public DefaultUnboundedIntValueModel()
    {
    }

    /**
     * Returns the model's current value.
     * <p>
     * @return the model's current value
     * <p>
     * @see #setValue
     * @see UnboundedRangeModel#getValue
     */
    public int getValue()
    {
        return value;
    }

    /**
     * Returns the model's sensitivity.
     * <p>
     * @return the model's sensitivity
     * <p>
     * @see #setSensitivity
     * @see UnboundedRangeModel#getVensitivity
     */
    public int getSensitivity()
    {
        return sensitivity;
    }

    /**
     * Sets the current value of the model.
     * <p>
     * @see UnboundedRangeModel#setValue
     */
    public void setValue(int newValue)
    {
        value = newValue;
    }

    /**
     * Sets the current sensitivity of the model.
     * <p>
     * @see UnboundedRangeModel#setSensitivity
     */
    public void setSensitivity(int newSensitivity)
    {
        sensitivity = newSensitivity;
    }

    /**
     * Adds a <code>ChangeListener</code>. The change listeners are run each
     * time any one of the Bounded Range model properties changes.
     *
     * @param l the ChangeListener to add
     * <p>
     * @see #removeChangeListener
     * @see BoundedRangeModel#addChangeListener
     */
    public void addChangeListener(ChangeListener l)
    {
        listenerList.add(ChangeListener.class, l);
    }

    /**
     * Removes a <code>ChangeListener</code>.
     *
     * @param l the <code>ChangeListener</code> to remove
     * <p>
     * @see #addChangeListener
     * @see BoundedRangeModel#removeChangeListener
     */
    public void removeChangeListener(ChangeListener l)
    {
        listenerList.remove(ChangeListener.class, l);
    }

    /**
     * Returns an array of all the change listeners
     * registered on this <code>DefaultBoundedRangeModel</code>.
     *
     * @return all of this model's <code>ChangeListener</code>s
     *         or an empty
     *         array if no change listeners are currently registered
     *
     * @see #addChangeListener
     * @see #removeChangeListener
     *
     * @since 1.4
     */
    public ChangeListener[] getChangeListeners()
    {
        return (ChangeListener[]) listenerList.getListeners(
            ChangeListener.class);
    }

    /**
     * Runs each <code>ChangeListener</code>'s <code>stateChanged</code> method.
     *
     * @see #setRangeProperties
     * @see EventListenerList
     */
    protected void fireStateChanged()
    {
        Object[] listeners = listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == ChangeListener.class) {
                if (changeEvent == null) {
                    changeEvent = new ChangeEvent(this);
                }
                ((ChangeListener) listeners[i + 1]).stateChanged(changeEvent);
            }
        }
    }

    /**
     * Returns a string that displays all of the
     * <code>BoundedRangeModel</code> properties.
     */
    @Override
    public String toString()
    {

        String modelString
            = "value=" + getValue() + ", " +
            "sensitivity=" + getSensitivity() + ", " +
            "adj=" + isAdjusting();

        return getClass().getName() + "[" + modelString + "]";
    }

    /**
     * Returns an array of all the objects currently registered as
     * <code><em>Foo</em>Listener</code>s
     * upon this model.
     * <code><em>Foo</em>Listener</code>s
     * are registered using the <code>add<em>Foo</em>Listener</code> method.
     * <p>
     * You can specify the <code>listenerType</code> argument
     * with a class literal, such as <code><em>Foo</em>Listener.class</code>.
     * For example, you can query a <code>DefaultBoundedRangeModel</code>
     * instance <code>m</code>
     * for its change listeners
     * with the following code:
     *
     * <pre>ChangeListener[] cls = (ChangeListener[])(m.getListeners(ChangeListener.class));</pre>
     *
     * If no such listeners exist,
     * this method returns an empty array.
     *
     * @param listenerType the type of listeners requested;
     *                     this parameter should specify an interface
     *                     that descends from <code>java.util.EventListener</code>
     * <p>
     * @return an array of all objects registered as
     *         <code><em>Foo</em>Listener</code>s
     *         on this model,
     *         or an empty array if no such
     *         listeners have been added
     * <p>
     * @exception ClassCastException if <code>listenerType</code> doesn't
     *                               specify a class or interface that implements
     *                               <code>java.util.EventListener</code>
     *
     * @see #getChangeListeners
     *
     * @since 1.3
     */
    public <T extends EventListener> T[] getListeners(Class<T> listenerType)
    {
        return listenerList.getListeners(listenerType);
    }

    /**
     * Returns true if the value is in the process of changing
     * as a result of actions being taken by the user.
     *
     * @return the value of the <code>adjusting</code> property
     * <p>
     * @see #setValue
     * @see BoundedRangeModel#getValueIsAdjusting
     */
    public boolean isAdjusting()
    {
        return adjusting;
    }

    /**
     * Sets the <code>adjusting</code> property.
     *
     * @see #getValueIsAdjusting
     * @see #setValue
     * @see BoundedRangeModel#setValueIsAdjusting
     */
    public void setAdjusting(boolean b)
    {
        adjusting = b;
    }

    @Override
    public int getLargeIncrement() {
        return largeIncrement;
    }
    @Override
    public void setLargeIncrement(int increment) {
        largeIncrement = increment;
    }

}
