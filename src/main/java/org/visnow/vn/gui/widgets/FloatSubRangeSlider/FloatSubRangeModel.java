/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets.FloatSubRangeSlider;

import javax.swing.event.*;

/**
 * Defines the data model used by SubRangeSlider component.
 * Defines five interrelated integer properties: minimum, maximum, extent,
 * bottomValue and topValue. These four integers define two nested ranges like this:
 * <pre>
 * minimum <= bottomValue <= bottomValue+extent <= topValue <= maximum
 * </pre>
 * The outer range is <code>minimum,maximum</code> and the inner
 * range is <code>bottomValue,topValue</code>.
 * <ul>
 * <li>
 * The minimum and maximum set methods "correct" the other
 * three properties to accommodate their new value argument. For
 * example setting the model's minimum may change its maximum, value,
 * and extent properties (in that order), to maintain the constraints
 * specified above.
 *
 * <li>
 * The value and extent set methods "correct" their argument to
 * fit within the limits defined by the other three properties.
 * For example if <code>value == maximum</code>, <code>setExtent(10)</code>
 * would change the extent (back) to zero.
 *
 * <li>
 * The five BoundedRangeModel values are defined as Java Beans properties
 * however Swing ChangeEvents are used to notify clients of changes rather
 * than PropertyChangeEvents. This was done to keep the overhead of monitoring
 * a BoundedRangeModel low. Changes are often reported at MouseDragged rates.
 * </ul>
 *
 * <p>
 *
 * For an example of specifying custom bounded range models used by sliders,
 * see <ahref="http://java.sun.com/docs/books/tutorial/uiswing/overview/anatomy.html">
 * The Anatomy of a Swing-Based Program
 * in <em>The Java Tutorial.</em>
 *
 * <p>
 * @see DefaultBoundedRangeModel
 */
public interface FloatSubRangeModel
{

    public static final int STOP = 0;
    public static final int PUSH = 1;

    public static final int RESET = 0;
    public static final int IGNORE = 1;
    public static final int STRETCH = 2;
    public static final int PULL = 3;

    /**
     * Returns the minimum acceptable value.
     *
     * @return the value of the minimum property
     * <p>
     * @see #setMinimum
     */
    float getMinimum();

    /**
     * Sets the model's minimum to <I>newMinimum</I>. The
     * other three properties may be changed as well, to ensure
     * that:
     * <pre><CODE>
     * minimum <= value <= value+extent <= maximum
     * </CODE> </pre>
     * <
     * p>
     * Notifies any listeners if the model changes.
     * <p>
     * @param newMinimum the model's new minimum
     * <p>
     * @see #getMinimum
     * @see #addChangeListener
     */
    void setMinimum(float newMinimum);

    /**
     * Returns the model's maximum. Note that the upper
     * limit on the model's value is (maximum - extent).
     *
     * @return the value of the maximum property.
     * <p>
     * @see #setMaximum
     * @see #setExtent
     */
    float getMaximum();

    /**
     * Sets the model's maximum to <I>newMaximum</I>. The other
     * three properties may be changed as well, to ensure that
     * <pre><CODE>
     * minimum <= value <= value+extent <= maximum
     * </CODE></pre>
     * <p>
     * Notifies any listeners if the model changes.
     * <p>
     * @param newMaximum the model's new maximum
     * <p>
     * @see #getMaximum
     * @see #addChangeListener
     */
    void setMaximum(float newMaximum);

    /**
     * Sets the model's minimum to <I>newMinimum</I> and maximum to <I>newMaximum</I>.The other
 three properties may be changed as well, to ensure that<pre><CODE>
     * minimum <= value <= value+extent <= maximum
     * </CODE></pre>
     * <p>
     * Notifies any listeners if the model changes.
     * <p>
     * @param newMinimum the model's new minimum
     * @param newMaximum the model's new maximum
     * <p>
     * @see #getMaximum
     * @see #addChangeListener
     */
    void setMinMax(float newMinimum, float newMaximum);

    /**
     * Returns the model's current bottomValue. Note that the upper
     * limit on the model's value is <code>maximum - extent</code>
     * and the lower limit is <code>minimum</code>.
     *
     * @return the model's value
     * <p>
     * @see #setValue
     */
    float getBottomValue();

    /**
     * Sets the model's currentbottomVvalue to <code>newValue</code> if <code>newValue</code>
     * satisfies the model's constraints. Those constraints are:
     * <pre>
     * minimum <= value <= value+extent <= maximum
     * </pre>
     * Otherwise, if <code>newValue</code> is less than <code>minimum</code>
     * it's set to <code>minimum</code>, if its greater than
     * <code>maximum</code> then it's set to <code>maximum</code>, and
     * if it's greater than <code>value+extent</code> then it's set to
     * <code>value+extent</code>.
     * <p>
     * Notifies any listeners if the model changes.
     *
     * @param newValue the model's new value
     */
    void setBottomValue(float newValue);

    /**
     * Returns the model's current topValue. Note that the upper
     * limit on the model's value is <code>maximum</code>
     * and the lower limit is <code>minimum + extent</code> .
     *
     * @return the model's topValue
     */
    float getTopValue();

    /**
     * Sets the model's current topValue to <code>newValue</code> if <code>newValue</code>
     * satisfies the model's constraints. Those constraints are:
     * <pre>
     * bottomValue+extent <= topValue <= maximum
     * </pre>
     * Notifies any listeners if the model changes.
     *
     * @param newValue the model's new value
     * <p>
     * @see #getValue
     */
    void setTopValue(float newValue);

    /**
     * Returns the model's extent, the length of the inner range that
     * begins at the model's value.
     *
     * @return the value of the model's extent property
     * <p>
     * @see #setExtent
     * @see #setValue
     */
    float getExtent();

    /**
     * Sets the model's extent. The <I>newExtent</I> is forced to
     * be greater than or equal to zero and less than or equal to
     * maximum - value.
     * <p>
     * Notifies any listeners if the model changes.
     *
     * @param newExtent the model's new extent
     * <p>
     * @see #getExtent
     * @see #setValue
     */
    void setExtent(float newExtent);

    /**
     * Returns the model's conflict resolving policy.
     * <p>
     * When policy==STOP updating bottomValue or topValue that would lead to
     * bottomValue+extent > topValue, change to currently modified value will be
     * constrained.
     * <p>
     * When policy==PUSH, the other value will be updated to preserve
     * bottomValue+extent <= topValue.
     * <p>
     * Notifies any listeners if the model changes.
     *
     * @return 
     * @see #getExtent
     * @see #setBottomValue
     * @see #setTopValue
     */
    int getPolicy();

    /**
     * Sets the model's conflict resolving policy.
     * <p>
     * When policy==STOP updating bottomValue or topValue that would lead to
     * bottomValue+extent > topValue, change to currently modified value will be
     * constrained.
     * <p>
     * When policy==PUSH, the other value will be updated to preserve
     * bottomValue+extent <= topValue.
     * <p>
     * Notifies any listeners if the model changes.
     *
     * @param newPolicy the model's new policy
     * <p>
     * @see #getExtent
     * @see #setBottomValue
     * @see #setTopValue
     */
    void setPolicy(int newPolicy);

    /**
     * <p>
     * <CODE>policy==RESET</CODE>: updating<CODE> minimum</CODE> or <CODE>maximum</CODE> causes setting of
     * <CODE>bottomValue</CODE> to <CODE>minimum</CODE> and <CODE>topValue</CODE> to <CODE>maximum</CODE>
     * <p>
     * <CODE>policy==IGNORE</CODE>: updating <CODE>minimum</CODE> or <CODE>maximum</CODE>modifies
     * <CODE>bottomValue</CODE> or <CODE>topValue</CODE> only in the case of constraints violation
     * <p>
     * <CODE>policy==STRETCH</CODE>: if <CODE>topValue=maximum</CODE> and <CODE>maximum</CODE> is changed,
     * <CODE>topValue</CODE> is set to  <CODE>maximum</CODE>, <CODE>bottomValue</CODE> is modified accordingly
     * <p>
     * <CODE>policy==PULL</CODE>: if <CODE>topValue=maximum</CODE> and <CODE>maximum</CODE> is changed,
     * <CODE>topValue</CODE> is set to  <CODE>maximum</CODE> and <CODE>bottomValue</CODE> is modified to preserve extent;
     * in the case of <CODE>bottomValue=minimum</CODE> and <CODE>mainimum</CODE> is changed,
     * <CODE>bottomValue</CODE> follows  <CODE>minimum</CODE> and <CODE>topValue</CODE> is modified to preserve extent;
     * <p>
     * @return current value of <CODE>extensionPolicy</CODE>
     */
    int getExtensionPolicy();

    /**
     * Sets the model's reaction on minimum/maximum change.
     * <p>
     * Notifies any listeners if the model changes.
     * <p>
     * @param newExtensionPolicy the model's new extension policy
     */
    void setExtensionPolicy(int newExtensionPolicy);

    /**
     * Returns true if a series of changes of
     * BottomValue or TopValue settings is in progress.
     * Returns false when both TopValue and BottomValue are finally set.
     * <p>
     * @return <CODE>true</CODE> if a series of changes of
     *         <CODE>bottomValue</CODE> or <CODE>topValue</CODE> settings is in progress
     * <p>
     * <CODE>false</CODE> when both <CODE>TopValue</CODE> and <CODE>BottomValue</CODE> are finally set.
     */
    boolean isAdjusting();

    /**
     * Set true if a series of changes of
     * BottomValue or TopValue settings is in progress.Set false when both TopValue and BottomValue are finally set.
     * @param newAdjusting new value of adjusting behavior
     */
    void setAdjusting(boolean newAdjusting);

    /**
     * This method sets all of the model's data with a single method call.
     * The method results in a single change event being generated. This is
     * convenient when you need to adjust all the model data simultaneously and
     * do not want individual change events to occur.
     *
     * @param bottomValue
     * @param topValue
     * @param extent          an float giving the amount by which the value can "jump"
     * @param policy
     * @param extensionPolicy
     * @param min             an float giving the minimum value
     * @param max             an float giving the maximum value
     * @param adjusting       a boolean, true if a series of changes are in
     *                        progress
     *
     * @see #setValue
     * @see #setExtent
     * @see #setMinimum
     * @see #setMaximum
     * @see #setAdjusting
     */
    void setRangeProperties(float bottomValue, float topValue, float extent,
                            int policy, int extensionPolicy, float min, float max, boolean adjusting);

    /**
     * Adds a ChangeListener to the model's listener list.
     *
     * @param x the ChangeListener to add
     * <p>
     * @see #removeChangeListener
     */
    void addChangeListener(ChangeListener x);

    /**
     * Removes a ChangeListener from the model's listener list.
     *
     * @param x the ChangeListener to remove
     * <p>
     * @see #addChangeListener
     */
    void removeChangeListener(ChangeListener x);

    boolean isBottomValueChanged();

    float getLastBottomValue();

    boolean isTopValueChanged();

    float getLastTopValue();
}
