/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets.UnboundedRoller;

/*
 * SubRangeSlider.java
 *
 * Created on April 13, 2004, 3:11 PM
 */
import org.visnow.vn.gui.events.IntValueModificationEvent;
import org.visnow.vn.gui.events.IntValueModificationListener;
import java.awt.*;
//import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;
import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class UnboundedIntRoller extends JComponent implements ChangeListener, Serializable
{

    private UnboundedIntValueModel model;
    private Insets insets = new Insets(3, 3, 3, 3);

    /**
     * Creates a new instance of SubRangeSlider
     */
    public UnboundedIntRoller()
    {
        init(new DefaultUnboundedIntValueModel());
        addMouseWheelListener(new java.awt.event.MouseWheelListener()
            {
                @Override
                public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt)
                {
                    UnboundedIntValueModel model = getModel();
                    setValue(model.getValue() + model.getSensitivity() * evt.getWheelRotation());
                }
            });
    }

    protected void init(UnboundedIntValueModel m)
    {
        setModel(m);
        setMinimumSize(new Dimension(60, 45));
        setPreferredSize(new Dimension(200, 55));
        UIManager.put(BasicUnboundedIntRollerUI.UI_CLASS_ID, "BasicUnboundedIntRollerUI");
        updateUI();
    }

    public void setUI(UnboundedIntRollerUI ui)
    {
        super.setUI(ui);
    }

    @Override
    public void updateUI()
    {
        setUI(new BasicUnboundedIntRollerUI());
        invalidate();
    }

    @Override
    public String getUIClassID()
    {
        return BasicUnboundedIntRollerUI.UI_CLASS_ID;
    }

    public void setModel(UnboundedIntValueModel m)
    {
        UnboundedIntValueModel old = model;
        if (old != null)
            old.removeChangeListener(this);

        if (m == null)
            model = new DefaultUnboundedIntValueModel();
        else
            model = m;
        model.addChangeListener(this);

        firePropertyChange("model", old, model);
    }

    public UnboundedIntValueModel getModel()
    {
        return model;
    }

    public void reset()
    {
        model.setValue(0);
        model.setSensitivity(1);
    }

    public void stateChanged(ChangeEvent e)
    {
        repaint();
    }

    public int getValue()
    {
        return model.getValue();
    }

    public void setValue(int v)
    {
        int old = getValue();
        if (v != old) {
            model.setValue(v);
            fireStateChanged();
        }
    }

    public void setOutValue(int v)
    {
        int old = getValue();
        if (v != old)
            model.setValue(v);
    }

    public boolean isAdjusting()
    {
        return model.isAdjusting();
    }

    public void setAdjusting(boolean b)
    {
        boolean old = isAdjusting();
        if (b != old) {
            model.setAdjusting(b);
            fireStateChanged();
        }
    }

    public int getSensitivity()
    {
        return model.getSensitivity();
    }

    public void setSensitivity(int m)
    {
        int old = getSensitivity();
        if (m != old)
            model.setSensitivity(m);
    }
    public int getLargeIncrement()
    {
        return model.getLargeIncrement();
    }
    
    public void setLargeIncrement(int increment)
    {
        model. setLargeIncrement(increment);
    }

    public void setInsets(Insets i)
    {
        insets = i;
    }

    public void setInsets(int top, int left, int bottom, int right)
    {
        insets = new Insets(top, left, bottom, right);
    }

    @Override
    public Insets getInsets()
    {
        return insets;
    }

    /**
     * Utility field holding list of ChangeListeners.
     */
    private transient ArrayList<IntValueModificationListener> intValueModificationListenerList = new ArrayList<IntValueModificationListener>();

    /**
     * Registers ChangeListener to receive events.
     * <p>
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(IntValueModificationListener listener)
    {
        intValueModificationListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     * <p>
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(IntValueModificationListener listener)
    {
        intValueModificationListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     * @param object Parameter #1 of the <CODE>ChangeEvent<CODE> constructor.
     */
    private void fireStateChanged()
    {
        IntValueModificationEvent e = new IntValueModificationEvent(this, model.getValue(), model.isAdjusting());
        for (IntValueModificationListener listener : intValueModificationListenerList) {
            listener.intValueChanged(e);
        }
    }

}
