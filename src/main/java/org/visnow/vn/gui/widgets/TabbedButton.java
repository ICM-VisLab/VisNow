/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;
import javax.swing.JComponent;

/**
 *
 * @author vis
 */
public class TabbedButton extends JComponent
{

    protected int clickedId;
    protected boolean clicked;
    protected Image image;
    protected Vector<Rectangle> clickAreas;
    protected Vector<Rectangle> selectedImageRect;
    protected Vector<Rectangle> clickedImageRect;

    public int getActiveTab()
    {
        return clickedId;
    }

    public TabbedButton()
    {
        this.clicked = false;
        this.clickedId = 0;
        this.clickAreas = new Vector<Rectangle>();
        this.selectedImageRect = new Vector<Rectangle>();
        this.clickedImageRect = new Vector<Rectangle>();
        addMouseListener(new MouseAdapter()
        {
            @Override
            public void mousePressed(MouseEvent e)
            {
                clicked = true;
                for (int i = 0; i < clickAreas.size(); i++) {
                    if (clickAreas.get(i).contains(e.getPoint())) {
                        clickedId = i;
                        break;
                    }
                }
                repaint();
            }

            @Override
            public void mouseReleased(MouseEvent e)
            {
                clicked = false;
                repaint();
            }
        });
    }

    public void addTab(Rectangle clickArea, Rectangle selectedArea, Rectangle clickedArea)
    {
        clickAreas.add(clickArea);
        selectedImageRect.add(selectedArea);
        clickedImageRect.add(clickedArea);
    }

    public void paintSubImage(Graphics g, Rectangle rect)
    {
        Insets insets;
        if (getBorder() != null) {
            insets = getBorder().getBorderInsets(this);
        } else {
            insets = new Insets(0, 0, 0, 0);
        }
        g.drawImage(image,
                    insets.left, insets.top,
                    rect.width - insets.left - insets.right,
                    rect.height - insets.top - insets.bottom,
                    rect.x, rect.y,
                    rect.x + rect.width, rect.y + rect.height,
                    getBackground(), null);
    }

    public boolean isClicked()
    {
        return clicked;
    }

    public void setClicked(boolean clicked)
    {
        this.clicked = clicked;
    }

    public Image getImage()
    {
        return image;
    }

    public void setImage(Image image)
    {
        this.image = image;
    }

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        if (image != null && clickAreas.size() > 0) {
            if (clicked) {
                paintSubImage(g, clickedImageRect.get(clickedId));
            } else {
                paintSubImage(g, selectedImageRect.get(clickedId));
            }

        }
    }
}
