/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.widgets;

import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

/**
 *
 * @author know
 */
public class RollSpinner extends JSpinner {
    
    private boolean adjusting = false;
    private long lastRollTime = 0;

    public RollSpinner(SpinnerModel model) {
        super(model);
        addMouseWheelListener(new java.awt.event.MouseWheelListener()
            {
                @Override
                public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt)
                {
                    if (!adjusting) {
                        adjusting = true;
                        new Thread(new Runnable(){
                            @Override
                            public void run() {
                                while (System.currentTimeMillis() - lastRollTime < 100)
                                    try {
                                        Thread.sleep(50);
                                    } catch (InterruptedException ex) {
                                    }
                                adjusting = false;
                            }
                        }).start();
                    }
                    SpinnerModel model = getModel();
                    if (!(model instanceof SpinnerNumberModel))
                        return;
                    SpinnerNumberModel nModel = (SpinnerNumberModel)model;
                    int newVal = (Integer)nModel.getValue() + evt.getWheelRotation()* nModel.getStepSize().intValue();
                    nModel.setValue(Math.max(Math.min(newVal, (Integer)nModel.getMaximum()),(Integer)nModel.getMinimum()));
                }
            });
    }
    
    public RollSpinner()
    {
        this(new SpinnerNumberModel());
    }

    public boolean isAdjusting() {
        return adjusting;
    }

    public void setAdjusting(boolean adjusting) {
        this.adjusting = adjusting;
    }
    
}
