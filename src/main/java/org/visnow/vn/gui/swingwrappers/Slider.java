/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.swingwrappers;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.apache.log4j.Logger;

//TODO: consider wrapping slider in JPanel - just like ComboBox. That could also help with preferredSize problems....
//TODO: this slider should supports 2 types of userChangeEvents: submit change while adjusting.
// (with two alternative behaviors: submit while adjusting (default) and submit on stop adjusting.
/**
 * JSlider wrapper: adds UserChange Action to this component.
 * It also supports more fine grained UserAction with types: UserEvent.VALUE_CHANGED and UserEvent.ADJUSTING (on stop adjusting only).
 * <p>
 * In basic case (when user clicks on a track) this Slider, unlike JSlider, fires UserAction events only twice (while JSlider fires stateChanged 3 times).
 * Let's say value = 10. User clicks track somewhere above value 10. Slider goes to 11.
 * Now, JSlider fires: 1. value==10, isAdjusting==true, 2. value==11, isAdjusting==true, 3. value==11, isAdjusting==false.
 * This Slider fires: 1. value==11, isAdjusting==true, 2. value==11, isAdjusting==false.
 * <p>
 * The same case is when user just drags the thumb (adjusts value) - there is no initial "start adjusting" event here.
 * <p>
 * <p>
 * @author szpak
 */
public class Slider extends JSlider
{

    private static final Logger LOGGER = Logger.getLogger(Slider.class);

    //    private boolean submitOnAdjusting = true;
    //internal flow flags/variables
    private boolean fromSetter = false;
    private Integer lastValue;
    private Boolean lastAdjusting = false;
    private boolean firstRun = true;

    public Slider()
    {
        lastValue = super.getValue();

        addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                firstRun = false;
                if (!fromSetter) {
                    int value = Slider.super.getValue();
                    boolean adjusting = Slider.super.getValueIsAdjusting();
                    if (!lastValue.equals(value)) { //value changed
                        lastValue = value;
                        lastAdjusting = adjusting;
                        fireValueChanged();
                        fireUserAction((adjusting ? UserEvent.ADJUSTING : 0) | UserEvent.VALUE_CHANGED);
                    } else if (!adjusting && lastAdjusting) { //no change in value but maybe stop adjusting?
                        lastAdjusting = adjusting;
                        //fire on stop adjusting
                        fireUserAction(0); //no adjusting, no change
                    }
                }
            }
        });
    }

    /**
     * @deprecated this setter fires itemStateChange event which is used to fire UserAction. Use {@code setVal} instead.
     */
    @Deprecated
    @Override
    public void setValue(int n)
    {
        super.setValue(n);
        if (firstRun)
            lastValue = super.getValue();
    }

    public void setVal(int n)
    {
        fromSetter = true;
        super.setValue(n);
        lastValue = super.getValue();
        fromSetter = false;
    }

    @Override
    public void setMinimum(int minimum)
    {
        fromSetter = true;
        super.setMinimum(minimum); //To change body of generated methods, choose Tools | Templates.
        lastValue = super.getValue();
        fromSetter = false;
    }

    @Override
    public void setMaximum(int maximum)
    {
        fromSetter = true;
        super.setMaximum(maximum); //To change body of generated methods, choose Tools | Templates.
        lastValue = super.getValue();
        fromSetter = false;
    }

    @Override
    public void setExtent(int extent)
    {
        fromSetter = true;
        super.setExtent(extent);
        lastValue = super.getValue();
        fromSetter = false;
    }

    @Override
    public void setSnapToTicks(boolean b)
    {
        fromSetter = true;
        super.setSnapToTicks(b); //To change body of generated methods, choose Tools | Templates.
        lastValue = super.getValue();
        fromSetter = false;
    }

    private void fireValueChanged()
    {
        for (UserActionListener listener : userActionListeners)
            listener.userChangeAction(new org.visnow.vn.gui.swingwrappers.UserEvent(this));
    }

    private void fireUserAction(int flags)
    {
        UserEvent ue = new UserEvent(flags, this);
        for (UserActionListener listener : userActionListeners)
            listener.userAction(ue);
    }

    private List<UserActionListener> userActionListeners = new ArrayList<UserActionListener>();

    public void addUserActionListener(UserActionListener listener)
    {
        userActionListeners.add(listener);
    }

    public void removeUserActionListener(UserActionListener listener)
    {
        userActionListeners.remove(listener);
    }
}
