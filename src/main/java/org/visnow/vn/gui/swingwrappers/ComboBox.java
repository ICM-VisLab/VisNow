/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.swingwrappers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.border.Border;
import org.apache.log4j.Logger;
import org.visnow.jscic.utils.ArrayUtils;
import org.visnow.vn.system.swing.CustomSizePanel;

//XXX: ?? preferred size??? -> BorderLayout.center ??)
/**
 * JComboBox wrapper with additional support for ValueChanged action. This ComboBox supports only DefaultComboBoxModel and String items - which is the most
 * common case anyway.
 * <p>
 * This wrapper wraps JComboBox up in JPanel - this is to avoid some serious problems with JComboBox events (itemStateChanged - fired multiple times, fired in
 * setters, fired when model changes its contents, not fired when changing model; actionPerformed - fired in setter!).
 * <p>
 * Typical style properties are overloaded and passed to inner JComboBox: setEnabled, setFont, setForeground, setBackground, setOpaque, setBorder,
 * setToolTipText.
 * <p>
 * It would be best to not keep model in separate field (but just in innerComboBox) but it's necessary to support custom labeling 
 * (using setListData(values,labels)).
 * <p>
 * @author szpak
 */
public class ComboBox extends CustomSizePanel
{
    private static final Logger LOGGER = Logger.getLogger(ComboBox.class);
    
    private boolean fromSetter = false;
    private final JComboBox<String> innerComboBox;
    private Object[] listValues;

    public ComboBox()
    {
        setCustomWidth(50);
        setOverrideMinSize(true);
        listValues = new Object[]{};
        innerComboBox = new JComboBox();
        innerComboBox.setModel(new DefaultComboBoxModel());
        super.setLayout(new BorderLayout());

        innerComboBox.setFont(super.getFont());
        innerComboBox.setForeground(super.getForeground());
        innerComboBox.setBackground(super.getBackground());
        innerComboBox.setOpaque(super.isOpaque());

        super.add(innerComboBox, BorderLayout.CENTER);

        // assuming that inner JComboBox always fires itemStateChange event on user change.
        innerComboBox.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                if (!fromSetter && e.getStateChange() == ItemEvent.SELECTED) {
                    fireValueChanged();
                }
            }
        });
    }

    /**
     * Returns number of items in this combo box list.
     */
    public int getLength()
    {
        return ((DefaultComboBoxModel) (innerComboBox.getModel())).getSize();
    }

    public void setListData(Object[] values)
    {
        listValues = values;
        innerComboBox.setModel(new DefaultComboBoxModel(values));
    }

    public void setListData(Object[] values, String[] labels)
    {
        listValues = values;
        innerComboBox.setModel(new DefaultComboBoxModel(labels));
    }

    
    /**
     * Removes all elements from this combo box list.
     */
    public void removeAllElements()
    {
        fromSetter = true;
        ((DefaultComboBoxModel) innerComboBox.getModel()).removeAllElements();
        fromSetter = false;
    }

    /**
     * Removes element at specified {@code index}.
     */
    public void removeElementAt(int index)
    {
        fromSetter = true;
        ((DefaultComboBoxModel) innerComboBox.getModel()).removeElementAt(index);
        fromSetter = false;
    }

    /**
     * Remove first {@code element} from this combo box list.
     */
    public void removeElement(String element)
    {
        fromSetter = true;
        ((DefaultComboBoxModel) innerComboBox.getModel()).removeElement(element);
        fromSetter = false;
    }

    /**
     * Selects {@code item} or does nothing if {@code item} not found.
     */
    public void setSelectedItem(Object item)
    {
        fromSetter = true;
        int index = ArrayUtils.indexOf(listValues, item);
        innerComboBox.setSelectedItem(item);
        fromSetter = false;
    }

    /**
     * Selects the item at index {@code index}
     *
     * @param index an integer specifying the list item to select,
     *              where 0 specifies the first item in the list and -1 indicates no selection
     * <p>
     * @exception IllegalArgumentException if <code>index</code> < -1 or
     *                                     <code>anIndex</code> is greater than or equal to size
     * @param index
     */
    public void setSelectedIndex(int index)
    {
        fromSetter = true;
        innerComboBox.setSelectedIndex(index);
        fromSetter = false;
    }

    /**
     * Returns selected item or null if no item is selected
     * <p>
     * @return
     */
    public Object getSelectedItem()
    {
        if (innerComboBox.getSelectedIndex() != -1) return listValues[innerComboBox.getSelectedIndex()];
        else return innerComboBox.getSelectedItem();
    }

    /**
     * Returns index of selected item or -1 if no item is selected.
     * <p>
     * @return
     */
    public int getSelectedIndex()
    {
        return innerComboBox.getSelectedIndex();
    }

    public void setEditable(boolean editable){
        innerComboBox.setEditable(editable);
    }
    
    public boolean isEditable() {
        return innerComboBox.isEditable();
    }
    
    private java.util.List<UserActionListener> userActionListeners = new ArrayList<UserActionListener>();

    public void addUserActionListener(UserActionListener listener)
    {
        userActionListeners.add(listener);
    }

    public void removeUserActionListener(UserActionListener listener)
    {
        userActionListeners.remove(listener);
    }

    private void fireValueChanged()
    {
        for (UserActionListener listener : userActionListeners)
            listener.userChangeAction(new org.visnow.vn.gui.swingwrappers.UserEvent(this));
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
        if (innerComboBox != null)
            innerComboBox.setEnabled(enabled);
    }

    @Override
    public void setFont(Font font)
    {
        super.setFont(font);
        if (innerComboBox != null)
            innerComboBox.setFont(font);
    }

    @Override
    public void setForeground(Color fg)
    {
        super.setForeground(fg);
        if (innerComboBox != null)
            innerComboBox.setForeground(fg);
    }

    @Override
    public void setBackground(Color bg)
    {
        super.setBackground(bg);
        if (innerComboBox != null)
            innerComboBox.setBackground(bg);
    }

    @Override
    public void setOpaque(boolean isOpaque)
    {
        super.setOpaque(isOpaque);
        if (innerComboBox != null)
            innerComboBox.setOpaque(isOpaque);
    }

    @Override
    public void setBorder(Border border)
    {
        if (innerComboBox != null)
            innerComboBox.setBorder(border);
    }

    @Override
    public Border getBorder()
    {
        if (innerComboBox != null)
            return innerComboBox.getBorder();
        else
            return null;
    }

    @Override
    public void setToolTipText(String text)
    {
        super.setToolTipText(text); //To change body of generated methods, choose Tools | Templates.
        innerComboBox.setToolTipText(text); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Returns inner combo box.
     * <p>
     * @deprecated - be aware that UserActions may not be reliable when developer is playing too much with innerComboBox model or setters.
     */
    @Deprecated
    public JComboBox getInnerComboBox()
    {
        return innerComboBox;
    }
}
