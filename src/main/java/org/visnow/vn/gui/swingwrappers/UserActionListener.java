/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.swingwrappers;

import java.util.EventListener;

/**
 * The listener interface for receiving user events only.
 * (so there should be no firing such events on setters or any other change of internal state).
 * <p>
 * @author szpak
 */
public interface UserActionListener extends EventListener
{

    /**
     * Simple action that notifies about real value change
     * (so pressing Enter on TextField when typing the same text or click on already selected RadioButton should not fire such action).
     * <p>
     * This action should be the most simple and natural understanding of "value changed".
     *
     * On the other hand some real change may not be notified using this action - and this should be implementation specific.
     * <p>
     * Examples:
     * <ul>
     * <li>typing into TextField and then losing focus without pressing ENTER can a) submit typed value b) revert previous value
     * <li>sliding/adjusting slider can a) fire event or b) event can be fired after adjusting is stopped
     * </ul>
     */
    public void userChangeAction(UserEvent event);

    /**
     * This action is fired when user performed some action
     * (including actions that don't change value of component - like adjusting sliders, confirming text field with enter, clicking already clicked radiobutton)
     */
    public void userAction(UserEvent event);
}
