/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.swingwrappers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.EventObject;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.DefaultButtonModel;
import javax.swing.JRadioButton;
import org.apache.log4j.Logger;

//TODO: write unit test of this control
/**
 * JRadioButton wrapper with additional ValueChanged action.
 * There could be 2 behaviors: submit on select only and submit on select/deselect, but it was dropped to avoid some problems related to button grouping (firing
 * events, inconsistent buttons within the group).
 * <p>
 * ValueChange is fired only on user action (no firing event on setter).
 * <p>
 * This wrapper is based ONLY on setSelected and actionPerformed. So any other reason of changing state of this button may incorrectly omit ValueChanged
 * action. Other reasons could be: getModel().setSelected, group.clearSelection.
 * <p>
 * setModel for different model than DefaultButtonModel is NOT supported (due to lack of getGroup method in ButtonModel).
 * <p>
 * All buttons within the group needs to be RadioButton (not JRadioButton), ClassCastException will be thrown otherwise.
 * <p>
 * Note: ButtonGroup can be in 2 states: A. none button is selected, B. one button is selected. If group is already in state B than it cannot go back to A
 * (neither manually nor by setter). It can be done only by buttonGroup.clearSelection.
 * <p>
 * Note: JRadioButton fires actionPerformed event on user click/press and fires itemStateChange event on selected change but order of these events is officially
 * unknown.
 * <p>
 * @author szpak
 */
public class RadioButton extends JRadioButton
{

    private static final Logger LOGGER = Logger.getLogger(RadioButton.class);

    private boolean storedSelected = false;

    public RadioButton()
    {
        initEvents();
    }

    private void initEvents()
    {
        //fire event if value changed        
        addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
//                boolean newSelected = RadioButton.super.isSelected();

                boolean fireEvent = RadioButton.super.isSelected() && !storedSelected;
//                    storeSelected(newSelected);
                //action only on selected
//                if (newSelected)
//                if (newSelected && newSelected != storedSelected) {
                updateGroup();
//                    fireValueChanged();
//                }
//                else updateGroup();
                if (fireEvent) fireValueChanged();
            }
        });
    }

    private void updateGroup()
    {
        ButtonGroup buttonGroup = ((DefaultButtonModel) getModel()).getGroup();
        //if within the group then deselect others
        if (buttonGroup != null)
            for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
                RadioButton button = (RadioButton) buttons.nextElement();
//                if (button != RadioButton.this)
                button.storeSelected(button.isSelected());
            }

    }

    @Override
    public void setModel(ButtonModel newModel)
    {
        if (!(newModel instanceof DefaultButtonModel))
            throw new IllegalArgumentException("Only DefaultButtonModel is supported here");
        super.setModel(newModel); //To change body of generated methods, choose Tools | Templates.
        updateGroup();
    }

    @Override
    public void setSelected(boolean selected)
    {
        super.setSelected(selected);
        storedSelected = super.isSelected();
        updateGroup();
    }

    private void storeSelected(boolean selected)
    {
        storedSelected = selected;
    }

    private List<UserActionListener> userActionListeners = new ArrayList<UserActionListener>();

    public void addUserActionListener(UserActionListener listener)
    {
        userActionListeners.add(listener);
    }

    public void removeUserActionListener(UserActionListener listener)
    {
        userActionListeners.remove(listener);
    }

    private void fireValueChanged()
    {
        for (UserActionListener listener : userActionListeners)
            listener.userChangeAction(new org.visnow.vn.gui.swingwrappers.UserEvent(this));
    }
}
