/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.gui.utils;

import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JTable;

/**
 *
 * @author babor
 */


public class TableUtilities {

    public static void addContentTooltipPopup(final JTable table, final int column) {
        table.addMouseMotionListener(new MouseAdapter() {
            
            @Override
            public void mouseMoved(MouseEvent e) {
                JComponent component = (JComponent) e.getSource();
                if(component == null)
                    return;
                
                int c = table.columnAtPoint(e.getPoint());
                if (c == column) {
                    int r = table.rowAtPoint(e.getPoint());
                    if(r == -1)
                        return;
                    Object obj = table.getModel().getValueAt(r, c);
                    if(obj == null)
                        return;
                    component.setToolTipText(obj.toString());
                    Action toolTipAction = component.getActionMap().get("postTip");
                    if (toolTipAction != null) {
                        ActionEvent postTip = new ActionEvent(component, ActionEvent.ACTION_PERFORMED, "");
                        toolTipAction.actionPerformed(postTip);
                    }
                } else {
                    component.setToolTipText(null);
                }
            }
        });

    }
    
    
    
}
