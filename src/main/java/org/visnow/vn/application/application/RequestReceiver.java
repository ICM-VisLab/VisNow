/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.application;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.visnow.vn.engine.commands.Command;
import org.visnow.vn.engine.commands.HidePortCommand;
import org.visnow.vn.engine.commands.LibraryAddCommand;
import org.visnow.vn.engine.commands.LibraryDeleteCommand;
import org.visnow.vn.engine.commands.LibraryRenameCommand;
import org.visnow.vn.engine.commands.LinkAddCommand;
import org.visnow.vn.engine.commands.LinkDeleteCommand;
import org.visnow.vn.engine.commands.ModuleAddCommand;
import org.visnow.vn.engine.commands.ModuleDeleteCommand;
import org.visnow.vn.engine.commands.ModuleRenameCommand;
import org.visnow.vn.engine.commands.MoveLinkBarCommand;
import org.visnow.vn.engine.commands.MoveModulesCommand;
import org.visnow.vn.engine.commands.SelectedModuleCommand;
import org.visnow.vn.engine.commands.ShowPortCommand;
import org.visnow.vn.engine.commands.SplitLinkCommand;
import org.visnow.vn.engine.core.Link;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class RequestReceiver
{
    private static final Logger LOGGER = Logger.getLogger(RequestReceiver.class);
    protected Application application;

    RequestReceiver(Application application)
    {
        this.application = application;
    }

    //<editor-fold defaultstate="collapsed" desc=" Receive (switch only) ">
    public void receive(Command command)
    {
        switch (command.getType()) {
            //case Command.BLOCK:
            //    beginGroup((BeginCommand) command);
            //    return;
            //case Command.END:
            //    endGroup((EndCommand) command);
            //    return;
            case Command.ADD_LIBRARY:
                addLibrary((LibraryAddCommand) command);
                return;
            case Command.RENAME_LIBRARY:
                renameLibrary((LibraryRenameCommand) command);
                return;
            case Command.DELETE_LIBRARY:
                deleteLibrary((LibraryDeleteCommand) command);
                return;
            case Command.ADD_MODULE:
                addModule((ModuleAddCommand) command);
                return;
            case Command.RENAME_MODULE:
                renameModule((ModuleRenameCommand) command);
                return;
            case Command.DELETE_MODULE:
                deleteModule((ModuleDeleteCommand) command);
                return;
            case Command.ADD_LINK:
                addLink((LinkAddCommand) command);
                return;
            case Command.DELETE_LINK:
                deleteLink((LinkDeleteCommand) command);
                return;
            case Command.SPLIT_LINK:
                splitLink((SplitLinkCommand) command);
                return;
            case Command.UI_MOVE_MULTIPLE_MODULES:
                moveModules((MoveModulesCommand) command);
                return;
            case Command.UI_MOVE_LINK_BAR:
                moveLinkBar((MoveLinkBarCommand) command);
                return;
            case Command.UI_SHOW_PORT:
                showPort((ShowPortCommand) command);
                return;
            case Command.UI_HIDE_PORT:
                hidePort((HidePortCommand) command);
                return;
            case Command.UI_SCENE_SELECTED_MODULE:
                sceneSelectedModule((SelectedModuleCommand) command);
                return;
            case Command.UI_FRAME_SELECTED_MODULE:
                frameSelectedModule((SelectedModuleCommand) command);
                return;
            default:
            /* TODO: exception */
        }
    }

    //</editor-fold>
    protected void defaultCommand(Command command)
    {
        application.getExecutor().execute(command);
        application.getHistory().pushToUndo(command);
    }

    //<editor-fold defaultstate="collapsed" desc=" [Rec] Library ">
    protected void addLibrary(LibraryAddCommand command)
    {
        defaultCommand(command);
    }

    protected void renameLibrary(LibraryRenameCommand command)
    {
        throw new UnsupportedOperationException("Hubert");
    }

    protected void deleteLibrary(LibraryDeleteCommand command)
    {
        /* TODO: prompt dla uzytkownika */
        defaultCommand(command);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" [Rec] Module ">
    protected void addModule(ModuleAddCommand command)
    {
        defaultCommand(command);
    }

    protected void renameModule(ModuleRenameCommand command)
    {
        defaultCommand(command);
    }

    protected void deleteModule(ModuleDeleteCommand command)
    {
        LOGGER.debug("");
        
        List<Link> inLinks = new ArrayList<>();
        for (Iterator<Link> links = application.getEngine().getModule(command.getName()).iterator(true, false); links.hasNext();)
            inLinks.add(links.next());
        List<Link> outLinks = new ArrayList<Link>();
        for (Iterator<Link> links = application.getEngine().getModule(command.getName()).iterator(false, true); links.hasNext();)
            outLinks.add(links.next());
        
        for (Link link: inLinks){
            LinkDeleteCommand ldc = new LinkDeleteCommand(link.getName(), false);//always passive input link removal while deleting module
            application.getExecutor().execute(ldc);
        }
        for (Link link: outLinks){
            LinkDeleteCommand ldc = new LinkDeleteCommand(link.getName(), command.isActive());
            application.getExecutor().execute(ldc);
        }
        
        application.getExecutor().execute(command);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" [Rec] Link ">
    protected void addLink(LinkAddCommand command)
    {
        defaultCommand(command);
    }

    protected void deleteLink(LinkDeleteCommand command)
    {
        defaultCommand(command);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" [Rec] UI ">
    protected void moveModules(MoveModulesCommand command)
    {
        defaultCommand(command);
    }

    protected void showPort(ShowPortCommand command)
    {
        defaultCommand(command);
    }

    protected void hidePort(HidePortCommand command)
    {
        if (application
                .getEngine()
                .getModule(command.getModuleName())
                .getPort(command.isInput(), command.getPortName())
                .isLinked()) {
            return; /* TODO: zabezpieczenie przed ta sytuacja? monit w tym miejscu? */

        }
        defaultCommand(command);
    }

    protected void moveLinkBar(MoveLinkBarCommand command)
    {
        throw new UnsupportedOperationException("Hubert");
    }

    private void frameSelectedModule(SelectedModuleCommand selectedModuleCommand)
    {
        application.getExecutor().execute(selectedModuleCommand);
    }

    private void sceneSelectedModule(SelectedModuleCommand selectedModuleCommand)
    {
        application.getExecutor().execute(selectedModuleCommand);
    }

    //</editor-fold>
    private void splitLink(SplitLinkCommand command)
    {
        application.getExecutor().execute(command);
        application.getHistory().clear();//TODO: add to history / redo implementation
    }
}
