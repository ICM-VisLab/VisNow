/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.application;

import java.awt.Point;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import javax.swing.SwingUtilities;
import org.apache.log4j.Logger;
import org.visnow.vn.application.area.Area;
import org.visnow.vn.application.frames.ApplicationFrame2;
import org.visnow.vn.application.frames.Frames;
import org.visnow.vn.application.io.VNWriter;
import org.visnow.vn.application.io.XMLWriter;
import org.visnow.vn.application.libraries.Libraries;
import org.visnow.vn.engine.Engine;
import org.visnow.vn.engine.commands.ModuleAddCommand;
import org.visnow.vn.engine.commands.ModuleDeleteCommand;
import org.visnow.vn.engine.core.CoreName;
import org.visnow.vn.engine.main.ModuleBox;
import org.visnow.vn.lib.basic.viewers.Viewer3D.Viewer3D;
import org.visnow.vn.system.config.MainConfig;
import org.visnow.vn.system.main.VisNow;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class Application
{
    private static final Logger LOGGER = Logger.getLogger(Application.class);

    public static enum ApplicationStatus
    {

        OK,
        WARNING,
        ERROR
    };

    private Libraries libraries;
    private Engine engine;
    private History history;
    private Frames frames;
    private Area area;
    private RequestReceiver receiver;
    private CommandExecutor executor;
    private Semaphore lockup;
    private String title;
    private String filePath;

    protected void setFilePath(String filePath)
    {
        this.filePath = filePath;
    }

    public boolean tryGetAccess(String reason)
    {
        LOGGER.debug("Getting access @" + Thread.currentThread() + " for: [" + reason + "]");
        boolean ret = lockup.tryAcquire();
        if (ret)
            LOGGER.debug("Acquired access @" + Thread.currentThread() + " for: [" + reason + "]");
        if (ret)
            getArea().getInput().showLock(true);//getScene().getScenePanel().getLockManager().setLocked(true);
        return ret;
    }

    public boolean getAccess(String reason) throws InterruptedException
    {
        LOGGER.debug("Getting access @" + Thread.currentThread() + " for: [" + reason + "]");
        lockup.acquire();
        LOGGER.debug("acquired access @" + Thread.currentThread() + " for: [" + reason + "]");
        getArea().getInput().showLock(true);//getScene().getScenePanel().getLockManager().setLocked(true);
        return true;
    }

    public void releaseAccess()
    {
        if (lockup.availablePermits() == 0)
            lockup.release();
        LOGGER.debug("released access @" + Thread.currentThread());
        getArea().getInput().showLock(false);
        //getScene().getScenePanel().getLockManager().setLocked(false);
    }

    //<editor-fold defaultstate="collapsed" desc=" [VAR] GETTERS ">
    public Libraries getLibraries()
    {
        return libraries;
    }

    public Engine getEngine()
    {
        return engine;
    }

    protected History getHistory()
    {
        return history;
    }

    public Area getArea()
    {
        return area;
    }

    public CommandExecutor getExecutor()
    {
        return executor;
    }

    public Frames getFrames()
    {
        return frames;
    }

    //</editor-fold>
    public ApplicationFrame2 getApplicationFrame()
    {
        return frames.getApplicationFrame();
    }

    public RequestReceiver getReceiver()
    {
        return receiver;
    }

    public Semaphore getLockup()
    {
        return lockup;
    }

    public boolean isLockupBusy()
    {
        return lockup.hasQueuedThreads() || lockup.availablePermits() == 0;
    }

    public String getTitle()
    {
        return title;
    }

    /**
     * Sets application title and corresponding application tab label to <code>title</code>
     */
    public void setTitle(String title)
    {
        this.title = title;

        final Application thisApplication = this;
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                VisNow.get().getMainWindow().getApplicationsPanel().updateApplicationTabTitle(thisApplication);
            }
        });
    }

    public String getFilePath()
    {
        return filePath;
    }

    private Thread engineThread;

    public Application(String title)
    {
        this(title, true);
    }

    public Application(String title, boolean disableStartupViewers)
    {
        this.libraries = new Libraries(this);
        this.engine = new Engine(this);
        this.history = new History(this);
        this.area = new Area(this);
        this.frames = new Frames(this);

        this.receiver = new RequestReceiver(this);
        this.executor = new CommandExecutor(this);

        this.lockup = new Semaphore(1, true);
        this.title = title;
        this.filePath = null;

        engineThread = new Thread(getEngine(), "VN-Engine");
        engineThread.start();

        if (!disableStartupViewers)
            initViewers();
    }

    public Application(String title, File file)
    {
        this(title, true);
        this.filePath = file.getPath();
    }

    private void initViewers()
    {
        MainConfig mainConfig = VisNow.get().getMainConfig();
        if (mainConfig.isStartupViewer3D()) {
            addInitViewerByName("viewer 3D", "org.visnow.vn.lib.basic.viewers.Viewer3D.Viewer3D");
        }
        if (mainConfig.isStartupOrthoViewer3D()) {
            addInitViewerByName("orthoviewer 3D", "org.visnow.vn.lib.basic.viewers.MultiViewer3D.Viewer3D");
        }
        if (mainConfig.isStartupViewer2D()) {
            addInitViewerByName("viewer 2D", "org.visnow.vn.lib.basic.viewers.Viewer2D.Viewer2D");
        }
        if (mainConfig.isStartupFieldViewer3D()) {
            addInitViewerByName("field viewer 3D", "org.visnow.vn.lib.basic.viewers.FieldViewer3D.FieldViewer3D");
        }
    }

    public boolean save()
    {
        return saveAs(new File(filePath));
    }

    private boolean changed = false;

    public void setChanged()
    {
        changed = true;
    }

    private void setNoChanged()
    {
        changed = false;
    }

    public boolean hasChanged()
    {
        return changed;
    }

    public boolean saveAs(File file)
    {
        try {
            this.getAccess("Saving application (Application-0)");
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
        boolean ret = XMLWriter.writeApplication(this, file);
        if (ret) {
            this.filePath = file.getPath();
            setNoChanged();
        }
        this.releaseAccess();
        return ret;
    }

    public void clearHistory()
    {
        history.clear();
    }

    public void deleteSelected()
    {
        area.getInput().requestedDeleteSelectedItems();
    }

    //FIXME: This method is called from Swing EDT, in the same time module onActive method calls Swing methods, and both onActive and this method
    //use application.getAccess(.) to acquire the lock which leads to deadlock (in Swing EDT)
    //new Thread .start() is just a quick fix solution - should be done in all Area event handlers!
    public void deleteAllModules()
    {
        ConcurrentHashMap<String, ModuleBox> tmp = this.getEngine().getModules();
        final ArrayList<ModuleBox> modules = new ArrayList(tmp.values());

        final RequestReceiver thisReceiver = this.receiver;
        
        if (!isLockupBusy())
            new Thread(new Runnable()
            {
                public void run()
                {
                    ModuleBox module;
                    for (int i = 0; i < modules.size(); i++) {
                        module = modules.get(i);
                        thisReceiver.receive(new ModuleDeleteCommand(
                                module.getName(),
                                module.getCore().getCoreName(),
                                null, false));
                    }
                }
            }).start();
    }

    public void doTheMainReset()
    {
        engineThread = engine.doTheMainReset(engineThread);
        this.releaseAccess();
        engineThread.start();
    }

    public boolean betaSaveAs(File file)
    {
        try {
            this.getAccess("Saving application (Application-1)");
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
        boolean ret = VNWriter.writeApplication(this, file);
        if (ret) {
            this.filePath = file.getPath();
            setNoChanged();
        }
        this.releaseAccess();
        return ret;
    }

    public void correctModuleCount(int c)
    {
        this.getEngine().correctModuleCount(c);
    }

    private Point initPoint = new Point(50, 440);
    int initCounter = 0;

    public void addInitViewerByName(String name, String module)
    {
        ModuleAddCommand viewerAddCommand
                = new ModuleAddCommand(name + "[" + initCounter + "]",
                                       new CoreName("internal", null, module),
                                       initPoint);
        this.executor.execute(viewerAddCommand);
        initPoint = new Point(initPoint.x - 80, initPoint.y - 70);
        initCounter++;
        this.setNoChanged();
    }

    public void addModuleByName(String name, String module, Point pt)
    {
        addModuleByName(name, module, pt, false);
    }

    public void addModuleByName(String name, String module, Point pt, boolean forceFlag)
    {
        ModuleAddCommand moduleAddCommand
                = new ModuleAddCommand(
                        name,
                        new CoreName("internal", null, module),
                        pt,
                        forceFlag);
        this.executor.execute(moduleAddCommand);
    }

    private final Point nextModuleLocation = new Point(50, 50);
    int newCounter = 0;
    
    public Point getNextModuleLocation() {
        Point out = (Point) nextModuleLocation.clone();
        nextModuleLocation.x += 50;
        nextModuleLocation.y += 50;
        newCounter++;
        if (newCounter == 5) {
            newCounter = 0;
            nextModuleLocation.x = 50;
            nextModuleLocation.y -= 50;
        }
        return out;
    }

    public void addModuleByName(String name, String module, boolean forceFlag)
    {
        addModuleByName(name, module, getNextModuleLocation(), forceFlag);
    }

    private ApplicationStatus applicationStatus = ApplicationStatus.OK;

    public ApplicationStatus getStatus()
    {
        return applicationStatus;
    }

    public void setStatus(ApplicationStatus status)
    {
        this.applicationStatus = status;
        fireStatusChanged();
    }

    //XXX: Should be changed to getViewer3DTopMost 
    /**
     * 
     * @return first Viewer3D on the list of modules
     */
    public Viewer3D getViewer3D()
    {
        for (ModuleBox m : getEngine().getModules().values()) {                   
            if (m.getCore().getCoreName().getClassName().equals(Viewer3D.class.getName()))
                return (Viewer3D) m.getCore();
        }
        return null;
    }
    
    protected transient ArrayList<ApplicationStatusChangeListener> applicationStatusChangeListenerList = new ArrayList<ApplicationStatusChangeListener>();

    public synchronized void addStatusChangeListener(ApplicationStatusChangeListener listener)
    {
        applicationStatusChangeListenerList.add(listener);
    }

    public synchronized void removeStatusChangeListener(ApplicationStatusChangeListener listener)
    {
        applicationStatusChangeListenerList.remove(listener);
    }

    public void fireStatusChanged()
    {
        ApplicationStatusChangeEvent e = new ApplicationStatusChangeEvent(applicationStatus, this);
        for (int i = 0; i < applicationStatusChangeListenerList.size(); i++) {
            applicationStatusChangeListenerList.get(i).applicationStatusChanged(e);
        }
    }

}
