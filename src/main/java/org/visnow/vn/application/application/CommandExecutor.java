/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.application;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.commands.Command;
import org.visnow.vn.engine.commands.HidePortCommand;
import org.visnow.vn.engine.commands.LibraryAddCommand;
import org.visnow.vn.engine.commands.LibraryDeleteCommand;
import org.visnow.vn.engine.commands.LibraryRenameCommand;
import org.visnow.vn.engine.commands.LinkAddCommand;
import org.visnow.vn.engine.commands.LinkDeleteCommand;
import org.visnow.vn.engine.commands.ModuleAddCommand;
import org.visnow.vn.engine.commands.ModuleDeleteCommand;
import org.visnow.vn.engine.commands.ModuleRenameCommand;
import org.visnow.vn.engine.commands.MoveLinkBarCommand;
import org.visnow.vn.engine.commands.MoveModulesCommand;
import org.visnow.vn.engine.commands.SelectedModuleCommand;
import org.visnow.vn.engine.commands.ShowPortCommand;
import org.visnow.vn.engine.commands.SplitLinkCommand;
import org.visnow.vn.engine.core.Link;
import org.visnow.vn.engine.core.LinkName;
import org.visnow.vn.engine.core.Output;
import org.visnow.vn.engine.error.Displayer;
import org.visnow.vn.engine.exception.VNException;
import org.visnow.vn.engine.main.ModuleBox;
import org.visnow.vn.lib.types.VNGeometryObject;
import org.visnow.vn.system.main.VisNow;
import org.visnow.vn.system.utils.usermessage.Level;
import org.visnow.vn.system.utils.usermessage.UserMessage;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class CommandExecutor
{

    private static final boolean profile = false;
    private Application application;

    public Application getApplication()
    {
        return application;
    }

    CommandExecutor(Application application)
    {
        this.application = application;
    }

    //<editor-fold defaultstate="collapsed" desc=" Receive (switch only) ">
    public void execute(Command command)
    {
        application.setChanged();
        switch (command.getType()) {
            case Command.ADD_LIBRARY:
                addLibrary((LibraryAddCommand) command);
                return;
            case Command.RENAME_LIBRARY:
                renameLibrary((LibraryRenameCommand) command);
                return;
            case Command.DELETE_LIBRARY:
                deleteLibrary((LibraryDeleteCommand) command);
                return;
            case Command.ADD_MODULE:
                addModule((ModuleAddCommand) command);
                return;
            case Command.RENAME_MODULE:
                renameModule((ModuleRenameCommand) command);
                return;
            case Command.DELETE_MODULE:
                selectedModule(new SelectedModuleCommand(Command.UI_FRAME_SELECTED_MODULE, null));
                deleteModule((ModuleDeleteCommand) command);
                return;
            case Command.ADD_LINK:
                addLink((LinkAddCommand) command);
                return;
            case Command.DELETE_LINK:
                deleteLink((LinkDeleteCommand) command);
                return;
            case Command.SPLIT_LINK:
                splitLink((SplitLinkCommand) command);
                return;
            case Command.UI_MOVE_MULTIPLE_MODULES:
                moveModules((MoveModulesCommand) command);
                return;
            case Command.UI_MOVE_LINK_BAR:
                moveLinkBar((MoveLinkBarCommand) command);
                return;
            case Command.UI_SHOW_PORT:
                showPort((ShowPortCommand) command);
                return;
            case Command.UI_HIDE_PORT:
                hidePort((HidePortCommand) command);
                return;
            case Command.UI_SCENE_SELECTED_MODULE:
                throw new UnsupportedOperationException("Deprecated");
            case Command.UI_FRAME_SELECTED_MODULE:
                selectedModule((SelectedModuleCommand) command);
                return;
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" [Rec] Library ">
    protected void addLibrary(LibraryAddCommand command)
    {
        try {
            application.getAccess("Add Library (ACE-0)");
        } catch (InterruptedException ex) {
            return;
        }
        application.getLibraries().addLibrary(command.getName(), VisNow.get().getMainLibraries().getLibrary(command.getName()));
        application.getFrames().getExecutor().refreshLibraries();
        application.releaseAccess();
    }

    protected void renameLibrary(LibraryRenameCommand command)
    {
        throw new UnsupportedOperationException("Hubert");
    }

    protected void deleteLibrary(LibraryDeleteCommand command)
    {
        try {
            application.getAccess("Add Library (ACE-0)");
        } catch (InterruptedException ex) {
            return;
        }
        application.getLibraries().deleteLibrary(command.getName());
        application.getFrames().getExecutor().refreshLibraries();
        application.releaseAccess();
    }

    //</editor-fold>
    private void addModuleProfile(String string)
    {
        if (profile)
            System.out.println(
                Displayer.timestamp() + " " +
                this.getClass().getName() + ": " +
                "addModule" +
                "\n\t" +
                string);
    }

    //<editor-fold defaultstate="collapsed" desc=" [Rec] Module ">
    protected void addModule(ModuleAddCommand command)
    {
        
        //1) create a module and release application access
        try {
            application.getAccess("Add module (ACE-4)");
        } catch (InterruptedException ex) {
            return;
        }
        ModuleCore core;
        try {
            core = application.getLibraries().generateCore(command.getCoreName());
        } catch (VNException ex) {
            Displayer.display(200907100600L, ex, this, "Cannot add module, unknown exception.");
            return; /* TODO: do something with this exception */

        }

        application.getEngine().getExecutor().addModule(command.getName(), core);
        application.getArea().getInput().addModuleWidget(command.getName(), command.getPosition());
        application.getFrames().getExecutor().refreshModules();
        application.getArea().getInput().selectModule(command.getName());

        application.releaseAccess();

        //2) create link
        LinkName newLinkName = null;
        if (VisNow.get().getMainConfig().isAutoconnectViewer())
            for (Output output : core.getOutputs().getSortedOutputs()) {
                if (output.getType() == VNGeometryObject.class)
                    try {
                        ConcurrentHashMap<String, ModuleBox> modules = application.getEngine().getModules();
                        Collection<ModuleBox> mbxs = modules.values();
                        Iterator<ModuleBox> it = mbxs.iterator();
                        ModuleBox mb;
                        int viewerMaxNo = -1;
                        String viewerName = null;
                        while (it.hasNext()) {
                            mb = it.next();
                            if (mb.getCore().getCoreName().getClassName().equals("org.visnow.vn.lib.basic.viewers.Viewer3D.Viewer3D") ||
                                mb.getCore().getCoreName().getClassName().equals("org.visnow.vn.lib.basic.viewers.Viewer2D.Viewer2D")) {
                                String name = mb.getName();
                                String no = name.substring(name.lastIndexOf("[") + 1, name.lastIndexOf("]"));
                                int n = Integer.parseInt(no);
                                if (n > viewerMaxNo) {
                                    viewerMaxNo = n;
                                    viewerName = name;
                                }
                            }
                        }

                        if (viewerName != null) {
                            newLinkName = new LinkName(core.getName(), output.getName(), viewerName, "inObject");
                        }
                    } catch (Exception e) {
                    }
            }
        if (VisNow.get().getMainConfig().isAutoconnectOrthoViewer3D())
            for (Output output : core.getOutputs().getSortedOutputs()) {
                if (output.getType() == VNGeometryObject.class)
                    try {
                        ConcurrentHashMap<String, ModuleBox> modules = application.getEngine().getModules();
                        Collection<ModuleBox> mbxs = modules.values();
                        Iterator<ModuleBox> it = mbxs.iterator();
                        ModuleBox mb;
                        int viewerMaxNo = -1;
                        String viewerName = null;
                        while (it.hasNext()) {
                            mb = it.next();
                            if (mb.getCore().getCoreName().getClassName().equals("org.visnow.vn.lib.basic.viewers.MultiViewer3D.Viewer3D")) {
                                String name = mb.getName();
                                String no = name.substring(name.lastIndexOf("[") + 1, name.lastIndexOf("]"));
                                int n = Integer.parseInt(no);
                                if (n > viewerMaxNo) {
                                    viewerMaxNo = n;
                                    viewerName = name;
                                }
                            }
                        }

                        if (viewerName != null) {
                            newLinkName = new LinkName(core.getName(), output.getName(), viewerName, "inObject");
                        }
                    } catch (Exception e) {
                    }
            }
        
        if (newLinkName != null)
            execute(new LinkAddCommand(newLinkName, true));  
        
        //3) initModule
        try {
            application.getEngine().getModule(command.getName()).getCore().setForceFlag(command.getForceFlag());
            if (command.isActive())
                application.getEngine().getModule(command.getName()).getCore().initModule(!command.getForceFlag());
        } catch (Exception e) {
            Displayer.ddisplay(200909302301L, e, this,
                               "ERROR IN MODULE FUNCTION:\n" +
                "An error has occured in the function \"onInitFinished\"" +
                "of module \"" + command.getName() + "\".\n" +
                "Please report this error to the module core developer.");
        }
    }

    protected void renameModule(ModuleRenameCommand command)
    {
        try {
            application.getAccess("Rename module (ACE-5)");
        } catch (InterruptedException ex) {
            return;
        }

        //check if rename is possible
        ModuleBox exists = application.getEngine().getModule(command.getNewName());
        if (exists != null) {
            VisNow.get().userMessageSend(new UserMessage(
                this.application.getTitle(), 
                command.getName(), 
                "Cannot rename module - name already exists", 
                "", 
                Level.ERROR));
            application.getArea().getInput().selectModule(command.getName());
            application.releaseAccess();
            return;
        }

        ModuleBox mb = application.getEngine().getModule(command.getName());
        for (Link link : mb) {
            LinkName oldLinkName = link.getName();
            String in = oldLinkName.getInputModule();
            String out = oldLinkName.getOutputModule();
            if (in.equals(command.getName()))
                in = command.getNewName();
            if (out.equals(command.getName()))
                out = command.getNewName();
            LinkName newLinkName = new LinkName(
                out,
                oldLinkName.getOutputPort(),
                in,
                oldLinkName.getInputPort());

            application.getEngine().getExecutor().renameLink(oldLinkName, newLinkName);
            application.getArea().getInput().renameLink(oldLinkName, newLinkName);
        }

        application.getEngine().getExecutor().renameModule(command.getName(), command.getNewName());
        application.getFrames().getExecutor().refreshModules();
        application.getArea().getInput().renameModuleWidget(command.getName(), command.getNewName());
        application.getArea().getInput().selectModule(command.getNewName());
        //application.getFrames().getExecutor().selectModule(command.getNewName());
        application.releaseAccess();
    }

    protected void deleteModule(ModuleDeleteCommand command)
    {
        try {
            application.getAccess(command.toString());
        } catch (InterruptedException ex) {
            return;
        }
        application.getEngine().getExecutor().deleteModule(command.getName());
        application.getFrames().getExecutor().refreshModules();
        application.getFrames().getExecutor().selectModule(null);
        application.getArea().getInput().deleteModuleWidget(command.getName());
        application.releaseAccess();
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" [Rec] Link ">
    protected void addLink(LinkAddCommand command)
    {
        //System.out.println("ADDING LINK!! Command is active: "+command.isActive());
        try {
            application.getAccess("Add Link (ACE-1)");
        } catch (InterruptedException ex) {
            return;
        }
        if (application.getEngine().getExecutor().addLink(command.getName(), command.isActive())) {
            application.getArea().getInput().addLinkWidget(command.getName());
            if (command.isActive()) {
                application.getEngine().getModule(command.getName().getInputModule()).startAction();
            }
        }
        application.releaseAccess();
    }

    protected void deleteLink(LinkDeleteCommand command)
    {
        try {
            application.getAccess("Delete Link (ACE-2)");
        } catch (InterruptedException ex) {
            return;
        }
        application.getEngine().getExecutor().deleteLink(command.getName(), command.isActive());
        application.getArea().getInput().deleteLinkWidget(command.getName());
        application.getArea().selectNull();
        application.releaseAccess();
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" [Rec] UI ">
    protected void moveModules(MoveModulesCommand command)
    {
    }

    protected void showPort(ShowPortCommand command)
    {
    }

    protected void hidePort(HidePortCommand command)
    {
    }

    protected void moveLinkBar(MoveLinkBarCommand command)
    {
    }

    private void selectedModule(SelectedModuleCommand command)
    {
        application.getArea().getInput().selectModule(command.getName());
    }

    //</editor-fold>
    private void splitLink(SplitLinkCommand command)
    {
        try {
            application.getAccess("Split Link (ACE-3)");
        } catch (InterruptedException ex) {
            return;
        }

        Link old = application.getEngine().getLink(command.getLinkName());

        String dataName = "" +
            old.getOutput().getType().getSimpleName() +
            "[" + application.getEngine().nextModuleNumber() + "]";

        application.getEngine().getExecutor().splitLink(command.getLinkName(), dataName);
        application.getArea().getInput().addDataWidget(dataName, command.getLinkName(), old.getName());

        application.getFrames().getExecutor().refreshModules();
        application.getArea().getInput().selectModule(dataName);
        application.getFrames().getExecutor().selectModule(dataName);

        application.releaseAccess();
    }
}
