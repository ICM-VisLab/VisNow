/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.frames;

import org.visnow.vn.application.frames.tabs.MLibrariesPanel;
import org.visnow.vn.application.frames.tabs.ModulesGUIPanel;
import org.visnow.vn.application.frames.tabs.MPortsPanel;
import org.visnow.vn.application.frames.tabs.MTitlePanel;
import org.visnow.vn.application.application.Application;

//import org.visnow.vn.application.frames.tabs.MMasterLibrariesPanel;
/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class Frames
{

    protected Application application;
    protected ApplicationFrame2 applicationFrame;
    protected ModulesGUIPanel guiPanel;
    protected MLibrariesPanel librariesPanel;
    protected MPortsPanel modulePanel;
    protected MTitlePanel titlePanel;
    //protected MRootsPanel rootsPanel;
    //    protected MMasterLibrariesPanel masterLibrariesPanel;
    //    protected ScenePanel scenePanel;

    protected FrameExecutor executor;

    public Frames(Application application)
    {
        this.application = application;
        this.guiPanel = new ModulesGUIPanel(this);
        this.librariesPanel = new MLibrariesPanel(this);
        this.modulePanel = new MPortsPanel(this);
        this.titlePanel = new MTitlePanel(this);
        this.executor = new FrameExecutor(this);
        //        this.masterLibrariesPanel = new MMasterLibrariesPanel(this);
        this.applicationFrame = new ApplicationFrame2(this);
        //this.rootsPanel = new MRootsPanel();
        this.selectingEnabled = true;
    }

    //<editor-fold defaultstate="collapsed" desc=" Getters ">
    /**
     * @return the application
     */
    public Application getApplication()
    {
        return application;
    }

    /**
     * @return the applicationFrame
     */
    public ApplicationFrame2 getApplicationFrame()
    {
        return applicationFrame;
    }

    /**
     * @return the guiPanel
     */
    public ModulesGUIPanel getGuiPanel()
    {
        return guiPanel;
    }

    /**
     * @return the librariesPanel
     */
    public MLibrariesPanel getLibrariesPanel()
    {
        return librariesPanel;
    }

    /**
     * @return the modulePanel
     */
    public MPortsPanel getModulePanel()
    {
        return modulePanel;
    }

    /**
     * @return the titlePanel
     */
    public MTitlePanel getTitlePanel()
    {
        return titlePanel;
    }

    /**
     * @return the executor
     */
    public FrameExecutor getExecutor()
    {
        return executor;
    }

    //    public MRootsPanel getRootsPanel() {
    //        return rootsPanel;
    //    }
    //</editor-fold>
    private boolean selectingEnabled;

    public boolean isSelectingEnabled()
    {
        return selectingEnabled;
    }

    public void setSelectingEnabled(boolean b)
    {
        selectingEnabled = b;
    }
}
