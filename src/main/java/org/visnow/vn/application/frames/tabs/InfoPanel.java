/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.frames.tabs;

import java.awt.Color;
import javax.swing.JLabel;
import org.visnow.vn.application.frames.portPanels.InfoInputPanel;
import org.visnow.vn.application.frames.portPanels.InfoOutputPanel;
import org.visnow.vn.application.frames.portPanels.InfoParameterPanel;
import org.visnow.vn.engine.core.Input;
import org.visnow.vn.engine.main.ModuleBox;
import org.visnow.vn.engine.core.Output;
import org.visnow.vn.engine.core.Parameter;
import org.visnow.vn.system.swing.BigPanel;
import org.visnow.vn.system.swing.JExpandableList;
import org.visnow.vn.system.swing.SmallPanel;
import org.visnow.vn.system.swing.VNSwingUtils;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class InfoPanel extends JExpandableList
{

    private final static Color ONE = new Color(165, 165, 165);//new Color(255,255,255);
    private final static Color TWO = new Color(205, 205, 205);//new Color(165,165,165);
    private final static Color THREE = new Color(255, 255, 255);//new Color(205,205,205);

    private ModuleBox module;

    JExpandableList inPanels;
    JExpandableList outPanels;
    JExpandableList paramPanels;

    public InfoPanel(ModuleBox module)
    {
        super(ONE);
        this.module = module;
        this.setBackground(VNSwingUtils.randomBrown());

        this.inPanels = new JExpandableList(TWO);
        this.outPanels = new JExpandableList(TWO);
        this.paramPanels = new JExpandableList(TWO);

        inPanels.setParentList(this);
        outPanels.setParentList(this);
        paramPanels.setParentList(this);

        SmallPanel inputSmall = new SmallPanel(ONE, TWO, "inputs");
        BigPanel inputBig = new BigPanel(ONE, TWO, "inputs");
        VNSwingUtils.setFillerComponent(inputBig.getPanel(), inPanels);

        SmallPanel outputSmall = new SmallPanel(ONE, TWO, "outputs");
        BigPanel outputBig = new BigPanel(ONE, TWO, "outputs");
        VNSwingUtils.setFillerComponent(outputBig.getPanel(), outPanels);

        SmallPanel paramSmall = new SmallPanel(ONE, TWO, "params");
        BigPanel paramBig = new BigPanel(ONE, TWO, "params");
        VNSwingUtils.setFillerComponent(paramBig.getPanel(), paramPanels);

        this.addPair(inputSmall, inputSmall.getButton(), inputBig, inputBig.getButton());
        this.addPair(outputSmall, outputSmall.getButton(), outputBig, outputBig.getButton());
        this.addPair(paramSmall, paramSmall.getButton(), paramBig, paramBig.getButton());

        if (module.getInputs().getInputs().isEmpty()) {
            inPanels.addEl(new JLabel("no inputs"));
        } else
            for (Input input : module.getInputs()) {
                SmallPanel sm = new SmallPanel(TWO, THREE, input.getName());
                InfoInputPanel bg = new InfoInputPanel(TWO, THREE, input);
                inPanels.addPair(sm, sm.getButton(), bg, bg.getButton());
            }

        if (module.getOutputs().getOutputs().isEmpty()) {
            outPanels.addEl(new JLabel("no outputs"));
        } else
            for (Output output : module.getOutputs()) {
                SmallPanel sm = new SmallPanel(TWO, THREE, output.getName());
                InfoOutputPanel bg = new InfoOutputPanel(TWO, THREE, output);
                outPanels.addPair(sm, sm.getButton(), bg, bg.getButton());
            }

        if (module.getParameters().getParameters().isEmpty()) {
            paramPanels.addEl(new JLabel("no parameters"));
        } else
            for (Parameter parameter : module.getParameters()) {
                SmallPanel sm = new SmallPanel(TWO, THREE, parameter.getName());
                InfoParameterPanel bg = new InfoParameterPanel(TWO, THREE, parameter);
                paramPanels.addPair(sm, sm.getButton(), bg, bg.getButton());
            }

    }
}
