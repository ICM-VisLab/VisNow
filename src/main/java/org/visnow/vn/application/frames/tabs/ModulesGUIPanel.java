/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved.
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.frames.tabs;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.util.Collection;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import org.apache.log4j.Logger;
import org.visnow.vn.application.frames.Frames;
import org.visnow.vn.engine.commands.Command;
import org.visnow.vn.engine.commands.SelectedModuleCommand;
import org.visnow.vn.engine.core.ModuleCore;
import org.visnow.vn.engine.main.ModuleBox;
import org.visnow.vn.gui.widgets.CoveringLayerPanel;
import org.visnow.vn.lib.basic.viewers.Viewer3D.Viewer3D;
import org.visnow.vn.system.swing.CustomSizePanel;

/**
 *
 * @author gacek
 */
public class ModulesGUIPanel extends javax.swing.JPanel
{

    private static final Logger LOGGER = Logger.getLogger(ModulesGUIPanel.class);

    private final JPanel noFieldVisualisationModulePanel;

    protected ModuleCore lastCore = null, currentCore = null;
    protected Frames frames;

    public Frames getFrames()
    {
        return frames;
    }

    /**
     * Creates new form ModulesGUIPanel
     */
    public ModulesGUIPanel(Frames frames)
    {
        initComponents();
        this.frames = frames;
        noFieldVisualisationModulePanel = new CustomSizePanel(new BorderLayout());
        scrollPane.setViewportView(noFieldVisualisationModulePanel);
        refreshModules();
    }

    public boolean selectModule(String name)
    {
//        LOGGER.debug("");
        if (name == null) {
            cardPanel.setVisible(false);
            scrollPane.setViewportView(null);
            if (comboBox.getItemCount() > 0) {
                comboBox.setSelectedIndex(comboBox.getItemCount() - 1);
            }
            currentCore = null;
            //szpak: don't know what's lastCore for, but just in case I do reset it to remove reference to just deleted module.
            lastCore = null;
        } else {
            if (getFrames().getApplication().getEngine().getModule(name) != null) {
                comboBox.setSelectedItem(name);
                currentCore = getFrames().getApplication().getEngine().getModule(name).getCore();
//                if (lastCore != null && lastCore instanceof Viewer3D) {
//                    ((Viewer3D) lastCore).getDisplayPanel().getControlsPanel().releaseLightEdit();
//                }

                Component modulePanel = currentCore.getPanel();

                LOGGER.debug(modulePanel);

                if (modulePanel instanceof CoveringLayerPanel && ((CoveringLayerPanel) modulePanel).isFieldVisualizationGUI()) {
                    noScrollPanel.removeAll();
                    noScrollPanel.add(modulePanel, BorderLayout.CENTER);
                    noScrollPanel.repaint();
                    CardLayout cl = (CardLayout) (cardPanel.getLayout());
                    cl.show(cardPanel, "noScrollCard");
                } else {
                    noFieldVisualisationModulePanel.removeAll();
                    noFieldVisualisationModulePanel.add(modulePanel, BorderLayout.CENTER);
                    scrollPane.setViewportView(noFieldVisualisationModulePanel);
                    CardLayout cl = (CardLayout) (cardPanel.getLayout());
                    cl.show(cardPanel, "scrollCard");
                }

                cardPanel.setVisible(true);
                lastCore = currentCore;
            }
        }
        revalidate();
        return true;
    }

    public ModuleCore getCurrentCore()
    {
        return currentCore;
    }

    public void refreshModules()
    {
        Collection<ModuleBox> modules = getFrames().getApplication().getEngine().getModules().values();
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        for (ModuleBox module : modules) {
            model.addElement(module.getName());
        }
        model.addElement("");
        comboBox.setModel(model);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        comboBox = new javax.swing.JComboBox();
        cardPanel = new javax.swing.JPanel();
        noScrollPanel = new javax.swing.JPanel();
        scrollPane = new javax.swing.JScrollPane();

        setLayout(new java.awt.BorderLayout());

        comboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        comboBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                comboBoxActionPerformed(evt);
            }
        });
        add(comboBox, java.awt.BorderLayout.NORTH);

        cardPanel.setLayout(new java.awt.CardLayout());

        noScrollPanel.setLayout(new java.awt.BorderLayout());
        cardPanel.add(noScrollPanel, "noScrollCard");
        cardPanel.add(scrollPane, "scrollCard");

        add(cardPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void comboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxActionPerformed
        if (getFrames().isSelectingEnabled()) {
            if (comboBox.getItemCount() > 0 && comboBox.getSelectedIndex() == comboBox.getItemCount() - 1) {
                getFrames().getApplication().getReceiver().receive(
                        new SelectedModuleCommand(Command.UI_FRAME_SELECTED_MODULE, null));
            } else {
                getFrames().getApplication().getReceiver().receive(
                        new SelectedModuleCommand(Command.UI_FRAME_SELECTED_MODULE, (String) comboBox.getSelectedItem()));
            }
//            if (lastCore != null && lastCore instanceof Viewer3D) {
//                ((Viewer3D) lastCore).getDisplayPanel().getControlsPanel().releaseLightEdit();
//            }
        }
    }//GEN-LAST:event_comboBoxActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel cardPanel;
    private javax.swing.JComboBox comboBox;
    private javax.swing.JPanel noScrollPanel;
    private javax.swing.JScrollPane scrollPane;
    // End of variables declaration//GEN-END:variables
}
