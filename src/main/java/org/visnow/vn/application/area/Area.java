/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.area;

import java.util.Vector;
import org.visnow.vn.application.application.Application;
import org.visnow.vn.application.area.widgets.ModulePanel;
import org.visnow.vn.engine.core.LinkName;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class Area
{

    private Application application;
    private AreaPanel areaPanel;
    private AreaInput areaInput;
    private AreaOutput areaOutput;

    protected Application getApplication()
    {
        return application;
    }

    public AreaPanel getPanel()
    {
        return areaPanel;
    }

    public AreaInput getInput()
    {
        return areaInput;
    }

    public AreaOutput getOutput()
    {
        return areaOutput;
    }

    public Area(Application application)
    {
        this.application = application;
        this.areaPanel = new AreaPanel(this);
        this.areaInput = new AreaInput(this);
        this.areaOutput = new AreaOutput(this);
    }

    private Vector<SelectableAreaItem> selection = new Vector<SelectableAreaItem>();

    public void select(SelectableAreaItem item)
    {
        selectNull();
        if (item != null) {
            selection.add(item);
            item.setSelected(true);
            if (item instanceof ModulePanel) {
                moduleSelectionChanged(((ModulePanel) item).getModule().getName());
                areaPanel.repaint();
                return;
            }
        }
        moduleSelectionChanged(null);
        areaPanel.repaint();
    }

    public void selectNull()
    {
        for (int i = 0; i < selection.size(); i++) {
            selection.get(i).setSelected(false);
        }
        selection.clear();
        areaPanel.repaint();
    }

    public void alterSelection(SelectableAreaItem item)
    {
        if (item.isSelected()) {
            item.setSelected(false);
            selection.remove(item);
        } else {
            item.setSelected(true);
            selection.add(item);
            if (item instanceof ModulePanel) {
                moduleSelectionChanged(((ModulePanel) item).getModule().getName());
            }
        }
        areaPanel.repaint();
    }

    private void moduleSelectionChanged(String name)
    {
        getApplication().getFrames().getExecutor().selectModule(name);
    }

    /**
     * @return the selection
     */
    public Vector<SelectableAreaItem> getSelection()
    {
        return selection;
    }

}
