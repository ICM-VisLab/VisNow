/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.area.widgets;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import org.visnow.vn.system.swing.JComponentViewer;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class BgPanel extends JPanel
{

    //<editor-fold defaultstate="collapsed" desc=" Back Images ">
    private static BufferedImage backImage = null;

    static BufferedImage getBackImage()
    {
        return backImage;
    }

    private static BufferedImage backLockImage = null;

    static BufferedImage getBackLockImage()
    {
        return backLockImage;
    }

    public static void initBackImages()
    {
        if (backImage == null) {
            int deepGrayInt = 20 * (1 + 256 + 256 * 256);
            int textureSize = 30;
            int textureSizeMinus = textureSize - 1;
            backImage = new java.awt.image.BufferedImage(textureSize, textureSize, java.awt.image.BufferedImage.TYPE_INT_RGB);
            for (int i = 0; i < textureSizeMinus; ++i)
                for (int j = 0; j < textureSizeMinus; ++j)
                    backImage.setRGB(i, j, 0);

            for (int i = 0; i <= textureSizeMinus; ++i) {
                backImage.setRGB(textureSizeMinus, i, deepGrayInt);
                backImage.setRGB(i, textureSizeMinus, deepGrayInt);
            }
        }
        if (backLockImage == null) {
            int bgBlue = 40 * 256 * 256; //oryginalnie: 30
            int deepBlue = 20 * (1 + 256 + 256 * 256) + bgBlue;
            int textureSize = 30;
            int textureSizeMinus = textureSize - 1;
            backLockImage = new java.awt.image.BufferedImage(textureSize, textureSize, java.awt.image.BufferedImage.TYPE_INT_RGB);
            for (int i = 0; i < textureSizeMinus; ++i)
                for (int j = 0; j < textureSizeMinus; ++j)
                    backLockImage.setRGB(i, j, bgBlue);

            for (int i = 0; i <= textureSizeMinus; ++i) {
                backLockImage.setRGB(textureSizeMinus, i, deepBlue);
                backLockImage.setRGB(i, textureSizeMinus, deepBlue);
            }
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" [VAR] - locked ">
    private boolean locked;

    public void setLocked(boolean b)
    {
        this.locked = b;
        repaint();
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" [Constructor] ">
    public BgPanel()
    {
        super();
        this.setBackground(new Color(0, 0, 0));
        BgPanel.initBackImages();
        locked = false;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Paint ">
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        Graphics2D gg = (Graphics2D) g;
        gg.setPaint(new TexturePaint(
            (locked) ? getBackLockImage() : getBackImage(),
            new java.awt.Rectangle(0, 0, 30, 30))
        );
        gg.fill(gg.getClip());
    }

    //</editor-fold>
    public static void main(String[] args)
    {
        JComponentViewer v = new JComponentViewer(new BgPanel(), 400, 400, true);
        v.setVisible(true);
    }

}
