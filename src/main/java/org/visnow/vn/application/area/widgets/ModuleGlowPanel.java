/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.area.widgets;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import org.visnow.vn.system.swing.VNSwingUtils;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
class ModuleGlowPanel extends JPanel
{

    private int getMinus(int a)
    {
        if (a > 0)
            return 0;
        else
            return -a;
    }

    private int d;

    /**
     * Creates a new instance of GlowPanel
     */
    public ModuleGlowPanel()
    {
        super();
        this.d = ModulePanel.GLOWBORDER;
    }

    @Override
    public void paint(Graphics g)
    {
        Graphics2D gg = (Graphics2D) g;
        gg.addRenderingHints(VNSwingUtils.getHints());
        gg.setColor(new Color(255, 255, 204, 34));
        //Dalej jest zle: podswietlenie znika, jesli modul przesunac za krawedz przescrollowanej sceny.
        int w = (int) gg.getClipBounds().getWidth() - d - d + getMinus(this.getParent().getParent().getX());
        int h = (int) gg.getClipBounds().getHeight() - d - d + getMinus(this.getParent().getParent().getY());
        int x = d;
        int y = d;
        for (int i = 1; i <= d; ++i)
            gg.fillRoundRect(x - i, y - i, 2 * i + w, 2 * i + h, d, d);
    }

}
