/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.area.widgets;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import javax.swing.JComponent;
import org.visnow.vn.application.area.AreaPanel;
import org.visnow.vn.system.swing.VNSwingUtils;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class PortGlowPanel extends JComponent
{

    public static final int w = 40;
    public static final int h = 40;
    public static final int r = 20;

    //<editor-fold defaultstate="collapsed" desc=" Kolory ">
    private static final java.awt.Color[] clearColors = {
        new java.awt.Color(255, 255, 204, 255),
        new java.awt.Color(255, 255, 204, 187),
        new java.awt.Color(255, 255, 204, 119),
        new java.awt.Color(255, 255, 204, 85),
        new java.awt.Color(255, 255, 204, 0)
    };
    private static final java.awt.Color[] okColors = {
        new java.awt.Color(55, 255, 4, 255),
        new java.awt.Color(55, 255, 4, 187),
        new java.awt.Color(55, 255, 4, 119),
        new java.awt.Color(55, 255, 4, 85),
        new java.awt.Color(55, 255, 4, 0)
    };
    private static final java.awt.Color[] warningColors = {
        new java.awt.Color(255, 220, 4, 255),
        new java.awt.Color(255, 220, 4, 187),
        new java.awt.Color(255, 220, 4, 119),
        new java.awt.Color(255, 220, 4, 85),
        new java.awt.Color(255, 220, 4, 0)
    };
    private static final java.awt.Color[] errorColors = {
        new java.awt.Color(255, 55, 4, 255),
        new java.awt.Color(255, 55, 4, 187),
        new java.awt.Color(255, 55, 4, 119),
        new java.awt.Color(255, 55, 4, 85),
        new java.awt.Color(255, 55, 4, 0)
    };

    private static final Paint clearSmooth
        = new java.awt.RadialGradientPaint(w / 2, h / 2, r, new float[]{0, 1},
                                           new java.awt.Color[]{clearColors[0], clearColors[4]}
        );
    private static final Paint okSmooth
        = new java.awt.RadialGradientPaint(w / 2, h / 2, r, new float[]{0, 1},
                                           new java.awt.Color[]{okColors[0], okColors[4]}
        );
    private static final Paint warningSmooth
        = new java.awt.RadialGradientPaint(w / 2, h / 2, r, new float[]{0, 1},
                                           new java.awt.Color[]{warningColors[0], warningColors[4]}
        );
    private static final Paint errorSmooth
        = new java.awt.RadialGradientPaint(w / 2, h / 2, r, new float[]{0, 1},
                                           new java.awt.Color[]{errorColors[0], errorColors[4]}
        );

    private static final Paint clearSmoothSmall
        = new java.awt.RadialGradientPaint(w / 2, h / 2, 3 * r / 4, new float[]{0, 1},
                                           new java.awt.Color[]{clearColors[0], clearColors[4]}
        );
    private static final Paint okSmoothSmall
        = new java.awt.RadialGradientPaint(w / 2, h / 2, 3 * r / 4, new float[]{0, 1},
                                           new java.awt.Color[]{okColors[0], okColors[4]}
        );
    private static final Paint warningSmoothSmall
        = new java.awt.RadialGradientPaint(w / 2, h / 2, 3 * r / 4, new float[]{0, 1},
                                           new java.awt.Color[]{warningColors[0], warningColors[4]}
        );
    private static final Paint errorSmoothSmall
        = new java.awt.RadialGradientPaint(w / 2, h / 2, 3 * r / 4, new float[]{0, 1},
                                           new java.awt.Color[]{errorColors[0], errorColors[4]}
        );

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Paint ">
    @Override
    public void paint(Graphics g)
    {
        Graphics2D gg = (Graphics2D) g;
        gg.addRenderingHints(VNSwingUtils.getHints());
        if (active) {
            switch (errorLevel) {
                case ERRORLEVEL_OK:
                    gg.setPaint(okSmooth);
                    break;
                case ERRORLEVEL_WARNING:
                    gg.setPaint(warningSmooth);
                    break;
                case ERRORLEVEL_ERROR:
                    gg.setPaint(errorSmooth);
                    break;
            }
            gg.fillOval(0, 0, w, h);
        } else {
            switch (errorLevel) {
                case ERRORLEVEL_OK:
                    gg.setPaint(okSmoothSmall);
                    break;
                case ERRORLEVEL_WARNING:
                    gg.setPaint(warningSmoothSmall);
                    break;
                case ERRORLEVEL_ERROR:
                    gg.setPaint(errorSmoothSmall);
                    break;
            }
            gg.fillOval(0, 0, w, h);
        }

    }

    //</editor-fold>
    private boolean active;

    public void setActive(boolean b)
    {
        active = b;
    }

    public static final int ERRORLEVEL_OK = 0;
    public static final int ERRORLEVEL_WARNING = 1;
    public static final int ERRORLEVEL_ERROR = 2;
    private int errorLevel = ERRORLEVEL_OK;

    public void setErrorLevel(int errLevel)
    {
        this.errorLevel = errLevel;
    }

    private AreaPanel ap;

    //<editor-fold defaultstate="collapsed" desc=" Konstruktor ">
    public PortGlowPanel(AreaPanel ap)
    {
        super();
        this.ap = ap;
        //animation.start();
    }
    //</editor-fold>

}
