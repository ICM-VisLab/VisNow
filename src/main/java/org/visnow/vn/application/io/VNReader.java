/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.io;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.log4j.Logger;
import org.visnow.vn.application.application.Application;
import org.visnow.vn.engine.core.CoreName;
import org.visnow.vn.engine.commands.LibraryAddCommand;
import org.visnow.vn.engine.commands.LinkAddCommand;
import org.visnow.vn.engine.commands.ModuleAddCommand;
import org.visnow.vn.engine.core.LinkName;
import org.visnow.vn.engine.core.ModuleCore;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class VNReader
{

    private static final Logger LOGGER = Logger.getLogger(VNReader.class);

    public static Application readApplication(File file)
    {
        try {
            return tryReadApplication(file);
        } catch (FileNotFoundException ex) {
            LOGGER.debug("ERR");
            return null;
        } catch (IOException e) {
            LOGGER.debug("ERR");
            return null;
        }
    }

    private static boolean isDigit(char c)
    {
        return (c >= '0') && (c <= '9');
    }

    private static int getLastInt(String s)
    {
        //LOGGER.debug("getLastInt["+s+"]");
        String ch = "";
        boolean started = false;
        int i = s.length() - 1;
        while (!isDigit(s.charAt(i))) {
            //    LOGGER.debug("i="+i);
            --i;
            if (i < 0) {
                return 0;
            }
        }
        while (isDigit(s.charAt(i))) {
            ch = "" + s.charAt(i) + ch;
            --i;
            if (i < 0) {
                break;
            }
        }
        return Integer.parseInt(ch);
    }

    /**
     * This reader assumes following rules of visnow .vna files: 1. Modules are
     * written from top to bottom (in terms of connection flow) 2. Module are
     * written in 3 ordered steps: a) @module module core b) @link+ links from
     * other modules to this module c) @params of this module
     */
    private static Application tryReadApplication(File file) throws FileNotFoundException, IOException
    {

        BufferedReader reader = new BufferedReader(new FileReader(file));

        Application application = new Application("read_app", true);
        String module = "";

        Set<ModuleCore> moduleCoresToInitAndRun = new HashSet<>();
        Set<ModuleCore> moduleCoresToInit = new HashSet<>();
        ModuleCore currentModuleCore = null;

        while (true) {
            String next = reader.readLine();
            if (next == null) {
                break;
            }
            VNReaderNode node = VNReaderNode.read(next);

            if (node != null) {
                LOGGER.debug(node.type);
                for (Entry<String, String> e : node.data.entrySet()) {
                    LOGGER.debug("[" + e.getKey() + "] => [" + e.getValue() + "]");
                }
                switch (node.type) {
                    case application:
                        application.setTitle(node.data.get("name"));
                        break;
                    case library:
                        application.getReceiver().receive(new LibraryAddCommand(node.data.get("name")));
                        break;
                    case module:
                        module = node.data.get("name");
                        application.getReceiver().receive(new ModuleAddCommand(
                                node.data.get("name"),
                                new CoreName(node.data.get("library"), node.data.get("class")),
                                new Point(Integer.parseInt(node.data.get("x")), Integer.parseInt(node.data.get("y"))), false, false
                        ));
                        application.correctModuleCount(getLastInt(module));
                        currentModuleCore = application.getEngine().getModule(module).getCore();
                        //run this module core (as source in flow graph); remove it if it is not a source (has links)
                        moduleCoresToInitAndRun.add(currentModuleCore);
                        //one "fromVNA" for module itself
                        currentModuleCore.increaseFromVNA();
                        break;
                    case link:
                        application.getReceiver().receive(new LinkAddCommand(new LinkName(
                                node.data.get("from"),
                                node.data.get("out"),
                                node.data.get("to"),
                                node.data.get("in")
                        ), false));

                        //remove module if it is not a source (has links)
                        moduleCoresToInitAndRun.remove(currentModuleCore);
                        //and add it to "init only" list
                        moduleCoresToInit.add(currentModuleCore);
                        //one "fromVNA" for each link
                        currentModuleCore.increaseFromVNA();
                        break;
                    case params:
                        //if there are links and module then fromVNA == 1 + number_of_links (but should be number_of_links only)
                        if (currentModuleCore.getFromVNA() > 1) {
                            currentModuleCore.decreaseFromVNA();
                        }
                        String dat = "";
                        String lin = "";
                        while (!lin.startsWith("@end")) {
                            dat += lin + "\n";
                            lin = reader.readLine();
                        }
                        application.getEngine().getModule(module).getParameters().setParameterActive(false);
                        application.getEngine().getModule(module).getParameters().readXML(dat);
                        application.getEngine().getModule(module).getParameters().setParameterActive(true);
                        break;
                    default:
                        break;
                }
                if(node.type == VNReaderNodeType.module || 
                        node.type == VNReaderNodeType.link || 
                        node.type == VNReaderNodeType.params) {
                    try {
                        Thread.sleep(500);
                    } catch (Exception e) {
                        LOGGER.debug("interrupted sllep in VNA read");
                    }
                }
            }
        }

        for (ModuleCore moduleCore : moduleCoresToInit) {
            moduleCore.initModule(false);
        }
        for (ModuleCore moduleCore : moduleCoresToInitAndRun) {
            moduleCore.initModule(true);
        }

        return application;
    }

    private VNReader()
    {
    }
}
