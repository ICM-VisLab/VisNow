/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 * 
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 *  This code is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 3, 
 *  or (at your option) any later version, as published by the Free Software Foundation.  
 *  The authors designate this particular file as subject to the "Classpath" exception 
 *  as provided in the LICENSE file that accompanied this code.
 *
 *  This code is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  version 3 for more details (a copy is included in the LICENSE file that
 *  accompanied this code).
 *
 *  You should have received a copy of the GNU General Public License version 3
 *  along with this work; if not, see http://www.gnu.org/licenses/gpl-3.0.txt 
 *
 *  Please contact visnow.org or visit www.visnow.org if you need additional information or
 *  have any questions.
 *
 */

package org.visnow.vn.application.io;

import java.awt.Point;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.StringTokenizer;
import org.apache.log4j.Logger;
import org.visnow.vn.application.application.Application;
import org.visnow.vn.engine.core.Link;
import org.visnow.vn.engine.library.LibraryRoot;
import org.visnow.vn.engine.core.Input;
import org.visnow.vn.engine.main.ModuleBox;
import org.visnow.vn.engine.core.Output;
import org.visnow.vn.engine.core.Parameters;
import org.visnow.vn.geometries.parameters.PresentationParams;
import org.visnow.vn.lib.templates.visualization.modules.VisualizationModule;

/**
 *
 * @author Hubert Orlik-Grzesik, University of Warsaw, ICM
 */
public class VNWriter
{
    private static final Logger LOGGER = Logger.getLogger(VNWriter.class);

    //<editor-fold defaultstate="collapsed" desc=" Utils ">
    private static void writeln(int n, FileWriter writer, String line) throws IOException
    {
        String ind = "";
        for (int i = 0; i < n; ++i) {
            ind = ind + "    ";
        }
        writer.write(ind + line + "\n");
    }

    //</editor-fold>
    public static boolean writeApplication(Application application, File file)
    {
        LOGGER.debug("WRITE");
        try {
            return tryWriteApplication(application, file);
        } catch (IOException ex) {
            LOGGER.debug("EXCEPTION");
            ex.printStackTrace();
        }
        return false;
    }

    private static boolean tryWriteApplication(Application application, File file) throws IOException
    {

        FileWriter writer = new FileWriter(file);

        writeln(0, writer, "@application name<" + application.getTitle() + ">");
        writeln(0, writer, "");
        for (LibraryRoot root : application.getLibraries()) {
            writeln(0, writer, "@library name<" + root.getName() + "> file<" + root.getFilePath() + ">");
        }
        writeln(0, writer, "");

        //Map<ModuleBox, Integer> modules = new HashMap<ModuleBox, Integer>();
        //Map<Link, Integer> links = new HashMap<Link, Integer>();
        PriorityQueue<ModulePriority> pq = new PriorityQueue<ModulePriority>();
        HashMap<ModuleBox, ModulePriority> hm = new HashMap<ModuleBox, ModulePriority>();

        for (ModuleBox mb : application.getEngine().getModules().values()) {
            ModulePriority m = new ModulePriority(mb);
            pq.add(m);
            hm.put(mb, m);
        }

        for (Link link : application.getEngine().getLinks().values()) {
            ModulePriority m = hm.get(link.getInput().getModuleBox());
            pq.remove(m);
            m.priority++;
            pq.add(m);
        }

        while (!pq.isEmpty()) {
            ModuleBox mb = pq.poll().module;
            LOGGER.debug("WRITING MODULE [" + mb.getName() + "]");
            writeModule(mb, application, writer);
            for (Output o : mb.getOutputs()) {
                for (Link link : o.getLinks()) {
                    ModulePriority m = hm.get(link.getInput().getModuleBox());
                    pq.remove(m);
                    m.priority--;
                    pq.add(m);
                }
            }
        }

        writer.close();
        return true;
    }

    private static void writeModule(ModuleBox module, Application application, FileWriter writer) throws IOException
    {
        Point p = application.getArea().getInput().getModulePosition(module.getName());
        writeln(0, writer, "@module" +
                " name<" + module.getName() + ">" +
                " class<" + module.getCore().getCoreName().getClassName() + ">" +
                " library<" + module.getCore().getCoreName().getLibraryName() + ">" +
                " x<" + p.x + "> y<" + p.y + ">");
        for (Input in : module.getInputs()) {
            for (Link link : in.getLinks()) {
                writeln(1, writer, "@link" +
                        " from<" + link.getOutput().getModuleBox().getName() + ">" +
                        " out<" + link.getOutput().getName() + ">" +
                        " to<" + link.getInput().getModuleBox().getName() + ">" +
                        " in<" + link.getInput().getName() + ">");
            }
        }
        writeln(0, writer, "@params");

        Parameters passiveParameters = module.getParameters().getPassiveClone();
        if (module.getCore() instanceof VisualizationModule){
            if (passiveParameters.getParameter(PresentationParams.VNA_PARAMETER_NAME.getName()) == null)
                LOGGER.warn("This module (" + module.getCore().getClass() + ") is based on its own Param class - cannot save PresentationParams");
            else
                passiveParameters.set(PresentationParams.VNA_PARAMETER_NAME, ((VisualizationModule) module.getCore()).getPresentationParameterString());
        }
        String paramString = passiveParameters.writeXML();

        if (paramString != null) {//TODO: trzeba uniknąć komend w parametrach
            //params = encode(params);
            paramString = paramString + "\n";
            StringTokenizer tokenizer = new StringTokenizer(paramString, "\n");
            while (tokenizer.hasMoreElements()) {
                writeln(3, writer, tokenizer.nextToken());
            }
        }
        writeln(0, writer, "@end");

        writeln(0, writer, "");
    }

    private VNWriter()
    {
    }

}

class ModulePriority implements Comparable
{

    public Integer priority;
    public ModuleBox module;

    public ModulePriority(ModuleBox mb)
    {
        module = mb;
        priority = 0;
    }

    public int compareTo(Object o)
    {
        if (o instanceof ModulePriority)
            return priority.compareTo(((ModulePriority) o).priority);
        return priority.compareTo(priority);
    }

}
