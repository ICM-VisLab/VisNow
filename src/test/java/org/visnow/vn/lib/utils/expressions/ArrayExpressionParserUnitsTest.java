/*
 *    VisNow
 *    Copyright (C) 2006-2019 University of Warsaw, ICM
 *    Copyright (C) 2020 onward visnow.org
 *    All rights reserved. 
 *
 *  This file is part of GNU Classpath.
 *
 *  GNU Classpath is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  GNU Classpath is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GNU Classpath; see the file COPYING.  If not, write to the
 *  University of Warsaw, Interdisciplinary Centre for Mathematical and
 *  Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.
 *
 *  Linking this library statically or dynamically with other modules is
 *  making a combined work based on this library.  Thus, the terms and
 *  conditions of the GNU General Public License cover the whole
 *  combination.
 *
 *  As a special exception, the copyright holders of this library give you
 *  permission to link this library with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on this library.  If you modify this library, you may extend
 *  this exception to your version of the library, but you are not
 *  obligated to do so.  If you do not wish to do so, delete this
 *  exception statement from your version.
 */
 
package org.visnow.vn.lib.utils.expressions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.math3.util.FastMath;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.utils.FloatingPointUtils;
import org.visnow.jscic.utils.UnitUtils;

/**
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
@RunWith(value = Parameterized.class)
public class ArrayExpressionParserUnitsTest
{

    private static final float DELTA_F = 1E-4f;
    private static final double DELTA_D = 1E-13;
    private static final int N = 10;
    private static final float FLOAT_VAL = 0.5f;
    private static final double DOUBLE_VAL = 2;
    private final DataArray[] data;

    @Parameterized.Parameters
    public static Collection<Object[]> getParameters()
    {

        //scalar constant
        DataArray scalarConstantFloat = DataArray.createConstant(DataArrayType.FIELD_DATA_FLOAT, N, FLOAT_VAL, "data array", "cm", null);
        DataArray scalarConstantDouble = DataArray.createConstant(DataArrayType.FIELD_DATA_DOUBLE, N, DOUBLE_VAL, "data array", "dm", null);

        //scalar
        LargeArray laFloat = new FloatLargeArray(N, false);
        LargeArray laDouble = new DoubleLargeArray(N, false);
        for (int i = 0; i < N; i++) {
            laFloat.setFloat(i, FLOAT_VAL);
            laDouble.setDouble(i, DOUBLE_VAL);
        }
        DataArray scalarFloat = DataArray.create(laFloat, 1, "float", "cm", null);
        DataArray scalarDouble = DataArray.create(laDouble, 1, "double", "dm", null);

        //scalar constant time data
        ArrayList<Float> timeSeries = new ArrayList<>(2);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        ArrayList<LargeArray> scalarConstantDataSeriesFloat = new ArrayList<>(2);
        scalarConstantDataSeriesFloat.add(scalarConstantFloat.getRawFloatArray());
        scalarConstantDataSeriesFloat.add(scalarConstantFloat.getRawFloatArray());
        TimeData constTdFloat = new TimeData(timeSeries, scalarConstantDataSeriesFloat, timeSeries.get(0));
        DataArray scalarConstantTimeDataFloat = DataArray.create(constTdFloat, 1, "data array", "cm", null);
        ArrayList<LargeArray> scalarConstantDataSeriesDouble = new ArrayList<>(2);
        scalarConstantDataSeriesDouble.add(scalarConstantDouble.getRawDoubleArray());
        scalarConstantDataSeriesDouble.add(scalarConstantDouble.getRawDoubleArray());
        TimeData constTdDouble = new TimeData(timeSeries, scalarConstantDataSeriesDouble, timeSeries.get(0));
        DataArray scalarConstantTimeDataDouble = DataArray.create(constTdDouble, 1, "data array", "dm", null);

        //scalar time data
        ArrayList<LargeArray> scalarDataSeriesFloat = new ArrayList<>(2);
        scalarDataSeriesFloat.add(scalarFloat.getRawFloatArray());
        scalarDataSeriesFloat.add(scalarFloat.getRawFloatArray());
        TimeData tdFloat = new TimeData(timeSeries, scalarDataSeriesFloat, timeSeries.get(0));
        DataArray scalarTimeDataFloat = DataArray.create(tdFloat, 1, "data array", "cm", null);
        ArrayList<LargeArray> scalarDataSeriesDouble = new ArrayList<>(2);
        scalarDataSeriesDouble.add(scalarDouble.getRawDoubleArray());
        scalarDataSeriesDouble.add(scalarDouble.getRawDoubleArray());
        TimeData tdDouble = new TimeData(timeSeries, scalarDataSeriesDouble, timeSeries.get(0));
        DataArray scalarTimeDataDouble = DataArray.create(tdDouble, 1, "data array", "dm", null);

        final DataArray[] inputs = new DataArray[]{scalarConstantFloat, scalarConstantDouble, scalarFloat, scalarDouble, scalarConstantTimeDataFloat, scalarConstantTimeDataDouble, scalarTimeDataFloat, scalarTimeDataDouble};

        final ArrayList<Object[]> parameters = new ArrayList<>();
        for (int i = 0; i < inputs.length; i++) {
            parameters.add(new Object[]{inputs, 1});
            parameters.add(new Object[]{inputs, 7});
        }
        return parameters;
    }

    public ArrayExpressionParserUnitsTest(DataArray[] inputs, int nthreads)
    {
        data = inputs;
        ConcurrencyUtils.setNumberOfThreads(nthreads);
        if (nthreads > 1) {
            ConcurrencyUtils.setConcurrentThreshold(1);
        }
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    private double processNaNs(double x)
    {
        return FloatingPointUtils.processNaNs(x, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
    }

    private float[] processNaNs(float[] x)
    {
        float[] res = new float[x.length];
        for (int i = 0; i < x.length; i++) {
            res[i] = FloatingPointUtils.processNaNs(x[i], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
        }
        return res;
    }

    private float processNaNs(float x)
    {
        return FloatingPointUtils.processNaNs(x, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
    }

    @Test
    public void testVECTOR() throws Exception
    {
        String expr = "(a,b)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, false, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(veclen_a + veclen_b, veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("m", da.getUnit());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                TimeData td_a = a.getTimeData().convertToDouble();
                TimeData td_b = b.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            if (v < veclen_a) {
                                assertEquals(UnitUtils.unitConvert(aa.getDouble(k * veclen_a + v), a.getUnit(), "m"), cc.getDouble(k * veclen + v), DELTA_D);
                            } else {
                                assertEquals(UnitUtils.unitConvert(bb.getDouble(k * veclen_b + (v - veclen_a)), b.getUnit(), "m"), cc.getDouble(k * veclen + v), DELTA_D);
                            }
                        }
                    }
                }

                //single precision
                parser = new ArrayExpressionParser(N, false, false, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(veclen_a + veclen_b, veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("m", da.getUnit());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                td_a = a.getTimeData().convertToFloat();
                td_b = b.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            if (v < veclen_a) {
                                assertEquals(UnitUtils.unitConvert(aa.getFloat(k * veclen_a + v), a.getUnit(), "m"), cc.getFloat(k * veclen + v), DELTA_F);
                            } else {
                                assertEquals(UnitUtils.unitConvert(bb.getFloat(k * veclen_b + (v - veclen_a)), b.getUnit(), "m"), cc.getFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                }

            }
        }
    }

    @Test
    public void testSUM() throws Exception
    {
        String expr = "a+b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, false, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("m", da.getUnit());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                TimeData td_a = a.getTimeData().convertToDouble();
                TimeData td_b = b.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(UnitUtils.unitConvert(aa.getDouble(k * veclen_a + v % veclen_a), a.getUnit(), "m") + UnitUtils.unitConvert(bb.getDouble(k * veclen_b + v % veclen_b), b.getUnit(), "m"), cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }

                //single precision
                parser = new ArrayExpressionParser(N, false, false, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("m", da.getUnit());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                td_a = a.getTimeData().convertToFloat();
                td_b = b.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(UnitUtils.unitConvert(aa.getFloat(k * veclen_a + v % veclen_a), a.getUnit(), "m") + UnitUtils.unitConvert(bb.getFloat(k * veclen_b + v % veclen_b), b.getUnit(), "m"), cc.getFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }

            }
        }
    }

    @Test
    public void testDIFF() throws Exception
    {
        String expr = "a-b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, false, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("m", da.getUnit());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                TimeData td_a = a.getTimeData().convertToDouble();
                TimeData td_b = b.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(UnitUtils.unitConvert(aa.getDouble(k * veclen_a + v % veclen_a), a.getUnit(), "m") - UnitUtils.unitConvert(bb.getDouble(k * veclen_b + v % veclen_b), b.getUnit(), "m"), cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, false, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("m", da.getUnit());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                td_a = a.getTimeData().convertToFloat();
                td_b = b.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(UnitUtils.unitConvert(aa.getFloat(k * veclen_a + v % veclen_a), a.getUnit(), "m") - UnitUtils.unitConvert(bb.getFloat(k * veclen_b + v % veclen_b), b.getUnit(), "m"), cc.getFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testMULT() throws Exception
    {
        String expr = "a*b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, false, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(veclen_a == veclen_b ? 1 : FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("m2", da.getUnit());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                TimeData td_a = a.getTimeData().convertToDouble();
                TimeData td_b = b.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    if (veclen_a == veclen_b) {
                        for (int k = 0; k < N; k++) {
                            double dot = 0;
                            for (int v = 0; v < veclen_a; v++) {
                                dot += UnitUtils.unitConvert(aa.getDouble(k * veclen_a + v), a.getUnit(), "m") * UnitUtils.unitConvert(bb.getDouble(k * veclen_b + v), b.getUnit(), "m");
                            }
                            assertEquals(processNaNs(dot), cc.getDouble(k), DELTA_D);
                        }
                    } else {
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(processNaNs(UnitUtils.unitConvert(aa.getDouble(k * veclen_a + v % veclen_a), a.getUnit(), "m") * UnitUtils.unitConvert(bb.getDouble(k * veclen_b + v % veclen_b), b.getUnit(), "m")), cc.getDouble(k * veclen + v), DELTA_D);
                            }
                        }
                    }
                }

                //single precision
                parser = new ArrayExpressionParser(N, false, false, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(veclen_a == veclen_b ? 1 : FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("m2", da.getUnit());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                td_a = a.getTimeData().convertToFloat();
                td_b = b.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    if (veclen_a == veclen_b) {
                        for (int k = 0; k < N; k++) {
                            float dot = 0;
                            for (int v = 0; v < veclen_a; v++) {
                                dot += UnitUtils.unitConvert(aa.getFloat(k * veclen_a + v), a.getUnit(), "m") * UnitUtils.unitConvert(bb.getFloat(k * veclen_b + v), b.getUnit(), "m");
                            }
                            assertEquals(processNaNs(dot), cc.getFloat(k), DELTA_F);
                        }
                    } else {
                        for (int k = 0; k < N; k++) {
                            for (int v = 0; v < veclen; v++) {
                                assertEquals(processNaNs(UnitUtils.unitConvert(aa.getFloat(k * veclen_a + v % veclen_a), a.getUnit(), "m") * UnitUtils.unitConvert(bb.getFloat(k * veclen_b + v % veclen_b), b.getUnit(), "m")), cc.getFloat(k * veclen + v), DELTA_F);
                            }
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testDIV() throws Exception
    {
        String expr = "a/b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, false, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("1", da.getUnit());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                TimeData td_a = a.getTimeData().convertToDouble();
                TimeData td_b = b.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(UnitUtils.unitConvert(aa.getDouble(k * veclen_a + v % veclen_a), a.getUnit(), "m") / UnitUtils.unitConvert(bb.getDouble(k * veclen_b + v % veclen_b), b.getUnit(), "m")), cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, false, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("1", da.getUnit());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                td_a = a.getTimeData().convertToFloat();
                td_b = b.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(processNaNs(UnitUtils.unitConvert(aa.getFloat(k * veclen_a + v % veclen_a), a.getUnit(), "m") / UnitUtils.unitConvert(bb.getFloat(k * veclen_b + v % veclen_b), b.getUnit(), "m")), cc.getFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testPOW() throws Exception
    {
        String expr = "a^2";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, false, vars);
            DataArray da = parser.evaluateExpr(expr);
            int veclen = da.getVectorLength();
            assertEquals(veclen_a, veclen);
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            assertEquals("m2", da.getUnit());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            TimeData td_a = a.getTimeData().convertToDouble();
            for (Float time : timeSeries) {
                LargeArray aa = td_a.getValue(time);
                LargeArray cc = td.getValue(time);
                for (int k = 0; k < N; k++) {
                    for (int v = 0; v < veclen; v++) {
                        assertEquals(processNaNs(FastMath.pow(UnitUtils.unitConvert(aa.getDouble(k * veclen_a + v % veclen_a), a.getUnit(), "m"), 2)), cc.getDouble(k * veclen + v), DELTA_D);
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, false, vars);
            da = parser.evaluateExpr(expr);
            veclen = da.getVectorLength();
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            assertEquals("m2", da.getUnit());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            td_a = a.getTimeData().convertToFloat();
            for (Float time : timeSeries) {
                LargeArray aa = td_a.getValue(time);
                LargeArray cc = td.getValue(time);
                for (int k = 0; k < N; k++) {
                    for (int v = 0; v < veclen; v++) {
                        assertEquals(processNaNs(FastMath.pow(UnitUtils.unitConvert(aa.getFloat(k * veclen_a + v % veclen_a), a.getUnit(), "m"), 2)), cc.getFloat(k * veclen + v), DELTA_F);
                    }
                }
            }
        }
    }

    @Test
    public void testNEG() throws Exception
    {
        String expr = "~a";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen = a.getVectorLength();
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, false, vars);
            DataArray da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            assertEquals(a.getUnit(), da.getUnit());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            TimeData td_a = a.getTimeData().convertToDouble();
            for (Float time : timeSeries) {
                LargeArray aa = td_a.getValue(time);
                LargeArray cc = td.getValue(time);
                for (int k = 0; k < N; k++) {
                    for (int v = 0; v < veclen; v++) {
                        assertEquals(-aa.getDouble(k * veclen + v), cc.getDouble(k * veclen + v), DELTA_D);
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, false, vars);
            da = parser.evaluateExpr(expr);
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
            assertEquals(a.getNFrames(), da.getNFrames());
            assertEquals(a.getUnit(), da.getUnit());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            td_a = a.getTimeData().convertToFloat();
            for (Float time : timeSeries) {
                LargeArray aa = td_a.getValue(time);
                LargeArray cc = td.getValue(time);
                for (int k = 0; k < N; k++) {
                    for (int v = 0; v < veclen; v++) {
                        assertEquals(-aa.getFloat(k * veclen + v), cc.getFloat(k * veclen + v), DELTA_F);
                    }
                }
            }
        }
    }

    @Test
    public void testSQRT() throws Exception
    {
        String expr = "sqrt(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
        }
    }

    @Test
    public void testLOG() throws Exception
    {
        String expr = "log(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
        }
    }

    @Test
    public void testLOG10() throws Exception
    {
        String expr = "log10(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
        }
    }

    @Test
    public void testEXP() throws Exception
    {
        String expr = "exp(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
        }
    }

    @Test
    public void testABS() throws Exception
    {
        String expr = "abs(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen = a.getVectorLength();
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, false, vars);
            DataArray da = parser.evaluateExpr(expr);
            assertEquals(1, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
            assertEquals(a.getNFrames(), da.getNFrames());
            assertEquals(a.getUnit(), da.getUnit());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            TimeData td_a = a.getTimeData().convertToDouble();
            for (Float time : timeSeries) {
                LargeArray aa = td_a.getValue(time);
                LargeArray cc = td.getValue(time);
                for (int k = 0; k < N; k++) {
                    double norm = 0;
                    for (int v = 0; v < veclen; v++) {
                        norm += aa.getDouble(k * veclen + v) * aa.getDouble(k * veclen + v);
                    }
                    assertEquals(processNaNs(FastMath.sqrt(norm)), cc.getDouble(k), DELTA_D);
                }
            }

            //single precision
            parser = new ArrayExpressionParser(N, false, false, vars);
            da = parser.evaluateExpr(expr);
            assertEquals(1, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT);
            assertEquals(a.getNFrames(), da.getNFrames());
            assertEquals(a.getUnit(), da.getUnit());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            td_a = a.getTimeData().convertToFloat();
            for (Float time : timeSeries) {
                LargeArray aa = td_a.getValue(time);
                LargeArray cc = td.getValue(time);
                for (int k = 0; k < N; k++) {
                    double norm = 0;
                    for (int v = 0; v < veclen; v++) {
                        norm += aa.getFloat(k * veclen + v) * aa.getFloat(k * veclen + v);
                    }
                    assertEquals(processNaNs((float) FastMath.sqrt(norm)), cc.getFloat(k), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testSIN() throws Exception
    {
        String expr = "sin(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
        }
    }

    @Test
    public void testCOS() throws Exception
    {
        String expr = "cos(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
        }
    }

    @Test
    public void testTAN() throws Exception
    {
        String expr = "tan(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
        }
    }

    @Test
    public void testASIN() throws Exception
    {
        String expr = "asin(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
        }
    }

    @Test
    public void testACOS() throws Exception
    {
        String expr = "acos(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
        }
    }

    @Test
    public void testATAN() throws Exception
    {
        String expr = "atan(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, false, vars);
            try {
                DataArray da = parser.evaluateExpr(expr);
                assert (false);
            } catch (IllegalArgumentException ex) {
                assert (true);
            }
        }
    }

    @Test
    public void testSIG() throws Exception
    {
        String expr = "signum(a)";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            //double precision
            parser = new ArrayExpressionParser(N, true, false, vars);
            DataArray da = parser.evaluateExpr(expr);
            int veclen = a.getVectorLength();
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
            assertEquals(a.getNFrames(), da.getNFrames());
            assertEquals("1", da.getUnit());
            ArrayList<Float> timeSeries = da.getTimeSeries();
            TimeData td = da.getTimeData();
            TimeData td_a = a.getTimeData().convertToDouble();
            for (Float time : timeSeries) {
                LargeArray aa = td_a.getValue(time);
                LargeArray cc = td.getValue(time);
                for (int k = 0; k < N; k++) {
                    for (int v = 0; v < veclen; v++) {
                        assertEquals(FastMath.signum(aa.getDouble(k * veclen_a + v)), cc.getDouble(k * veclen + v), DELTA_D);
                    }
                }
            }
            //single precision
            parser = new ArrayExpressionParser(N, false, false, vars);
            da = parser.evaluateExpr(expr);
            veclen = a.getVectorLength();
            assertEquals(veclen, da.getVectorLength());
            assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT);
            assertEquals(a.getNFrames(), da.getNFrames());
            assertEquals("1", da.getUnit());
            timeSeries = da.getTimeSeries();
            td = da.getTimeData();
            td_a = a.getTimeData().convertToFloat();
            for (Float time : timeSeries) {
                LargeArray aa = td_a.getValue(time);
                LargeArray cc = td.getValue(time);
                for (int k = 0; k < N; k++) {
                    for (int v = 0; v < veclen; v++) {
                        assertEquals(FastMath.signum(aa.getFloat(k * veclen_a + v)), cc.getFloat(k * veclen + v), DELTA_F);
                    }
                }
            }
        }
    }

    @Test
    public void testGT() throws Exception
    {
        String expr = "a>b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, false, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("1", da.getUnit());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                TimeData td_a = a.getTimeData().convertToDouble();
                TimeData td_b = b.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(UnitUtils.unitConvert(aa.getDouble(k * veclen_a + v % veclen_a), a.getUnit(), "m") > UnitUtils.unitConvert(bb.getDouble(k * veclen_b + v % veclen_b), b.getUnit(), "m") ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, false, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("1", da.getUnit());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                td_a = a.getTimeData().convertToFloat();
                td_b = b.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(UnitUtils.unitConvert(aa.getDouble(k * veclen_a + v % veclen_a), a.getUnit(), "m") > UnitUtils.unitConvert(bb.getDouble(k * veclen_b + v % veclen_b), b.getUnit(), "m") ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testLT() throws Exception
    {
        String expr = "a<b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, false, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("1", da.getUnit());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                TimeData td_a = a.getTimeData().convertToDouble();
                TimeData td_b = b.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(UnitUtils.unitConvert(aa.getDouble(k * veclen_a + v % veclen_a), a.getUnit(), "m") < UnitUtils.unitConvert(bb.getDouble(k * veclen_b + v % veclen_b), b.getUnit(), "m") ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, false, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("1", da.getUnit());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                td_a = a.getTimeData().convertToFloat();
                td_b = b.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(UnitUtils.unitConvert(aa.getDouble(k * veclen_a + v % veclen_a), a.getUnit(), "m") < UnitUtils.unitConvert(bb.getDouble(k * veclen_b + v % veclen_b), b.getUnit(), "m") ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testGET() throws Exception
    {
        String expr = "a>=b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, false, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("1", da.getUnit());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                TimeData td_a = a.getTimeData().convertToDouble();
                TimeData td_b = b.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(UnitUtils.unitConvert(aa.getDouble(k * veclen_a + v % veclen_a), a.getUnit(), "m") >= UnitUtils.unitConvert(bb.getDouble(k * veclen_b + v % veclen_b), b.getUnit(), "m") ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, false, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("1", da.getUnit());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                td_a = a.getTimeData().convertToFloat();
                td_b = b.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(UnitUtils.unitConvert(aa.getDouble(k * veclen_a + v % veclen_a), a.getUnit(), "m") >= UnitUtils.unitConvert(bb.getDouble(k * veclen_b + v % veclen_b), b.getUnit(), "m") ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testLET() throws Exception
    {
        String expr = "a<=b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, false, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("1", da.getUnit());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                TimeData td_a = a.getTimeData().convertToDouble();
                TimeData td_b = b.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(UnitUtils.unitConvert(aa.getDouble(k * veclen_a + v % veclen_a), a.getUnit(), "m") <= UnitUtils.unitConvert(bb.getDouble(k * veclen_b + v % veclen_b), b.getUnit(), "m") ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, false, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("1", da.getUnit());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                td_a = a.getTimeData().convertToFloat();
                td_b = b.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(UnitUtils.unitConvert(aa.getDouble(k * veclen_a + v % veclen_a), a.getUnit(), "m") <= UnitUtils.unitConvert(bb.getDouble(k * veclen_b + v % veclen_b), b.getUnit(), "m") ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testEQ() throws Exception
    {
        String expr = "a==b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, false, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("1", da.getUnit());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                TimeData td_a = a.getTimeData().convertToDouble();
                TimeData td_b = b.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(UnitUtils.unitConvert(aa.getDouble(k * veclen_a + v % veclen_a), a.getUnit(), "m") == UnitUtils.unitConvert(bb.getDouble(k * veclen_b + v % veclen_b), b.getUnit(), "m") ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }
                //single precision
                parser = new ArrayExpressionParser(N, false, false, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("1", da.getUnit());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                td_a = a.getTimeData().convertToFloat();
                td_b = b.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(UnitUtils.unitConvert(aa.getFloat(k * veclen_a + v % veclen_a), a.getUnit(), "m") == UnitUtils.unitConvert(bb.getFloat(k * veclen_b + v % veclen_b), b.getUnit(), "m") ? 1.0 : 0.0, cc.getFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testNEQ() throws Exception
    {
        String expr = "a!=b";
        ArrayExpressionParser parser;
        Map<String, DataArray> vars = new HashMap<>();
        for (int i = 0; i < data.length; i++) {
            DataArray a = data[i];
            int veclen_a = a.getVectorLength();
            vars.put("a", a.name("a"));
            for (int j = 0; j < data.length; j++) {
                DataArray b = data[j];
                int veclen_b = b.getVectorLength();
                vars.put("b", b.name("b"));
                //double precision
                parser = new ArrayExpressionParser(N, true, false, vars);
                DataArray da = parser.evaluateExpr(expr);
                int veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_DOUBLE);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("1", da.getUnit());
                ArrayList<Float> timeSeries = da.getTimeSeries();
                TimeData td = da.getTimeData();
                TimeData td_a = a.getTimeData().convertToDouble();
                TimeData td_b = b.getTimeData().convertToDouble();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(UnitUtils.unitConvert(aa.getDouble(k * veclen_a + v % veclen_a), a.getUnit(), "m") != UnitUtils.unitConvert(bb.getDouble(k * veclen_b + v % veclen_b), b.getUnit(), "m") ? 1.0 : 0.0, cc.getDouble(k * veclen + v), DELTA_D);
                        }
                    }
                }

                //single precision
                parser = new ArrayExpressionParser(N, false, false, vars);
                da = parser.evaluateExpr(expr);
                veclen = da.getVectorLength();
                assertEquals(FastMath.max(veclen_a, veclen_b), veclen);
                assertTrue(da.getType() == DataArrayType.FIELD_DATA_FLOAT || da.getType() == DataArrayType.FIELD_DATA_COMPLEX);
                assertEquals(FastMath.max(a.getNFrames(), b.getNFrames()), da.getNFrames());
                assertEquals("1", da.getUnit());
                timeSeries = da.getTimeSeries();
                td = da.getTimeData();
                td_a = a.getTimeData().convertToFloat();
                td_b = b.getTimeData().convertToFloat();
                for (Float time : timeSeries) {
                    LargeArray aa = td_a.getValue(time);
                    LargeArray bb = td_b.getValue(time);
                    LargeArray cc = td.getValue(time);
                    for (int k = 0; k < N; k++) {
                        for (int v = 0; v < veclen; v++) {
                            assertEquals(UnitUtils.unitConvert(aa.getFloat(k * veclen_a + v % veclen_a), a.getUnit(), "m") != UnitUtils.unitConvert(bb.getFloat(k * veclen_b + v % veclen_b), b.getUnit(), "m") ? 1.0 : 0.0, cc.getFloat(k * veclen + v), DELTA_F);
                        }
                    }
                }
            }
        }
    }
}
